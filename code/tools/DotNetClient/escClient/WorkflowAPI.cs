using System;
using System.Net;
using System.IO;
using System.Collections;

namespace Esc.Client
{
	public class WorkflowAPI : GenericClient
	{

		public WorkflowAPI(string hostname, int port, bool secure, string username, string password) : base ("/api/public/rest/v1/workflow", hostname, port, secure, username, password) {

		} 

		public EscWorkflow[] ListWorkflows(){
			ArrayList list = retrieveJsonArray ("/workflows");
			EscWorkflow[] results = new EscWorkflow[list.Count];
			for (int i = 0; i < results.Length; i++) {
				results [i] = new EscWorkflow ((Hashtable)list [i]);
			}
			return results;
		}

		public EscWorkflow GetWorkflow(string workflowId) {
			return new EscWorkflow (retrieveJson ("/workflows/" + workflowId));
		}

		public void DeleteWorkflow(String workflowId){
			postText ("/deleteworkflow", workflowId);
		}

		public EscWorkflowInvocation ExecuteWorkflow(string workflowId) {
			return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/invoke", "EXECUTE"));
		}

		public EscWorkflowInvocation ExecuteWorkflow(string workflowId, string versionId){
			return new EscWorkflowInvocation (postTextRetrieveJson ("/workflows/" + workflowId + "/" + versionId + "/invoke", "EXECUTE"));
		}

		public EscWorkflowInvocation ExecuteWorkflowOnDocument(string workflowId, string documentId){
			return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/docinvoke", documentId));
		}

		public EscWorkflowInvocation ExecuteWorkflowOnDocument(string workflowId, string versionId, string documentId){
			return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/" + versionId + "/docinvoke", documentId));
		}

		public EscWorkflowInvocation ExecuteWorkflowWithParameters(String workflowId, EscWorkflowParameterList parameters) {
			return new EscWorkflowInvocation(postJsonRetrieveJson("/workflows/" + workflowId + "/paraminvoke", parameters.ToJson()));
		}

		public EscWorkflowInvocation ExecuteWorkflowWithParameters(String workflowId, string versionId, EscWorkflowParameterList parameters) {
			return new EscWorkflowInvocation(postJsonRetrieveJson("/workflows/" + workflowId + "/" + versionId + "/paraminvoke", parameters.ToJson()));
		}

		public EscWorkflowInvocation[] ListInvocationsOfWorkflow(string workflowId){
			ArrayList list = retrieveJsonArray ("/workflows/" + workflowId + "/invocations");
			EscWorkflowInvocation[] results = new EscWorkflowInvocation[list.Count];
			for (int i = 0; i < results.Length; i++) {
				results [i] = new EscWorkflowInvocation ((Hashtable)list [i]);
			}
			return results;
		}

		public EscWorkflowInvocation GetInvocation(string invocationId) {
			return new EscWorkflowInvocation(retrieveJson("/invocations/" + invocationId));
		}

		public void TerminateInvocation(string invocationId){
			postText ("/invocations/" + invocationId + "/terminate", "STOP");
		}
	}
}


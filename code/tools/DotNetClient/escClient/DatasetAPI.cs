using System;
using System.Net;
using System.IO;
using System.Collections;

namespace Esc.Client
{
	public class DatasetAPI : GenericClient {
		public DatasetAPI (string hostname, int port, bool secure, string username, string password) : base ("/api/public/rest/v1/dataset", hostname, port, secure, username, password) {
		} 

		public EscDataset[] ListDatasets(){
			ArrayList list = retrieveJsonArray ("/list");
			EscDataset[] results = new EscDataset[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscDataset ((Hashtable)list [i]);
			}
			return results;
		}

		public EscDatasetItem[] ListDatasetContents(string id) {
			ArrayList list = retrieveJsonArray ("/sets/" + id + "/items");
			EscDatasetItem[] results = new EscDatasetItem[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscDatasetItem ((Hashtable)list [i]);
			}
			return results;
		}

		public string QuerySingleValueDatasetItemAsString(string datasetId, string itemName) {
			return retrieveJson("/sets/" + datasetId + "/singleitems/" + itemName + "/row");
		}

		public int GetMultipleValueDatasetItemSize(string datasetId, string itemName) {
			return int.Parse(retrieveJson("/sets/" + datasetId + "/items/" + itemName + "/size"));
		}   

		public string QueryMultipleValueDatasetItemAsString(string datasetId, string itemName, int startRow, int maxResults) {
			return retrieveJson("/sets/" + datasetId + "/items/" + itemName + "/rows/" + startRow + "/" + maxResults);
		}

		public string QueryMultipleValueDatasetItemAsString(string datasetId, string itemName, int startRow, int maxResults, EscDatasetKeyList keys) {
			return postJsonRetrieveJson ("/sets/" + datasetId + "/items/" + itemName + "/rows/" + startRow + "/" + maxResults, keys.ToJson ());
		}

		public EscDataset CreateDataset(string name){
			return new EscDataset (postTextRetrieveJson ("/new", name));
		}

		public void DeleteDataset(string datasetId){
			postText ("/deletedataset", datasetId);
		}

		public EscDatasetItem AddItemToDataset(string datasetId, EscDatasetItem item){
			return new EscDatasetItem (postTextRetrieveJson ("/sets/" + datasetId + "/ptitems", item.ToJson ()));
		}

		public void ResetDataset(string datasetId) {
			retrieveJson ("/sets/" + datasetId + "/reset");
		}

		public void DeleteItemFromDataset(string datasetId, String itemName){
			postText ("/deletedatasetitem/" + datasetId, itemName);
		}

		public EscDataset UpdateDataset(EscDataset ds){
			return new EscDataset(postTextRetrieveJson("/ptsets/" + ds.id, ds.ToJson()));
		}

		public string AppendToMultipleValueDatasetItem (string datasetId, string itemName, string jsonData) {
			return postTextRetrieveJson ("/sets/" + datasetId + "/items/" + itemName + "/json", jsonData);
		}

		public string UpdateSingleValueDatasetItem(string datasetId, string itemName, string jsonData){
			return postJsonRetrieveJson ("/sets/" + datasetId + "/singleitems/" + itemName + "/row", jsonData);
		}
	}
}

/*
String updateMultipleValueDatasetItem(String datasetId, String itemName, long rowId, String jsonData) throws Exception;
*/
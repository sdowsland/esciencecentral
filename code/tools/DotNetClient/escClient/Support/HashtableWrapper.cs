using System;
using System.Collections;

namespace Esc.Client
{
	public class HashtableWrapper
	{
		private Hashtable ht;
		public HashtableWrapper (Hashtable ht)
		{
			this.ht = ht;
		}

		public string getString(string name, string defaultValue){
			if (ht.ContainsKey (name)) {
				return (string)ht [name];
			} else {
				return defaultValue;
			}
		}

		public long getLong(string name, long defaultValue){
			if (ht.ContainsKey (name)) {
				return long.Parse (ht [name].ToString ());
			} else {
				return defaultValue;
			}
		}

		public int getInt(string name, int defaultValue){
			if (ht.ContainsKey (name)) {
				return int.Parse (ht [name].ToString ());
			} else {
				return defaultValue;
			}
		}
	}
}


﻿using System;
using System.Collections.Generic;

namespace Esc.Client
{
	public class MultipleSequentialOperation : EscOperation {
		private List<EscOperation> operations = new List<EscOperation>();
		private List<EscOperation> failedOperations = new List<EscOperation>();
		public List<EscOperation> FailedOperations {
			get{ return failedOperations; }
		}

		private List<EscOperation> succeededOperations = new List<EscOperation>();
		public List<EscOperation> SucceededOperations {
			get{ return succeededOperations; }
		}

		public MultipleSequentialOperation () {
		}

		public void AddOperation(EscOperation op){
			operations.Add (op);
			op.OperationFinished += InternalTaskFinshedHandler;
		}

		public void InternalTaskFinshedHandler(EscOperation op){
			succeededOperations.Add (op);
		}
			
		protected override void PerformOperation(){
			failedOperations.Clear ();
			int count = 0;
			foreach (EscOperation op in operations) {
				OperationStatus status = op.PerformSync();
				count++;
				UpdatePercentage ((double)operations.Count, (double)count);
				if(status!=OperationStatus.COMPLETED){
					failedOperations.Add(op);
				}
			}
			operations.Clear ();
		}

		public override void SetupFromManager(EscOperationManager manager){
			foreach (EscOperation op in operations) {
				op.SetupFromManager (manager);
			}
		}
	}
}


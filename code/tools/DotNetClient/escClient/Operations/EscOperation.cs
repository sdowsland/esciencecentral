﻿using System;
using System.Threading;

namespace Esc.Client {
	public enum OperationStatus {WAITING, RUNNING, COMPLETED, FAILED};
	public abstract class EscOperation{
		Thread myThread;

		/** Current progress of this event */
		private int currentProgress = 0;
		public int CurrentProgress{
			get{ return currentProgress; }
			set{
				if (value != currentProgress) {
					currentProgress = value;
					if (OperationProgress != null) {
						OperationProgress (this, currentProgress);
					}
				}
			}
		}

		/** Progress event */
		public delegate void OperationProgressHander(EscOperation source, int percent);
		public event OperationProgressHander OperationProgress;

		/** Event called when an operation completes */
		public delegate void OperationFinishedHandler(EscOperation source);
		public event OperationFinishedHandler OperationFinished;

		/** Operation status */
		protected OperationStatus status = OperationStatus.WAITING;
		public OperationStatus Status {
			get { return status; }
		}

		/** Error message if there was one */
		protected string errorMessage;
		public string ErrorMessage {
			get {return errorMessage;}
		}

		public EscOperation () {
		}

		public OperationStatus PerformSync(){
			try {
				status = OperationStatus.RUNNING;
				CurrentProgress = 0;
				PerformOperation ();
				status = OperationStatus.COMPLETED;
			} catch (Exception e){
				errorMessage = e.Message;
				status = OperationStatus.FAILED;
			}
			currentProgress = 100;
			if (OperationFinished != null) {
				OperationFinished (this);
			}
			return status;
		}

		public void StartOperation(){
			myThread = new Thread (new ThreadStart (delegate {
				try {
					CurrentProgress = 0;
					status = OperationStatus.RUNNING;
					PerformOperation();
					status = OperationStatus.COMPLETED;
				} catch (Exception e){
					Console.Out.Write(e.StackTrace);
					errorMessage = e.Message;
					status = OperationStatus.FAILED;
				}
				CurrentProgress = 100;
				if(OperationFinished!=null){
					OperationFinished(this);
				}
			}));

			myThread.Start ();
		}

		public void UpdatePercentage(double totalWork, double currentWork){
			int percent = (int)((currentWork / totalWork) * 100D);
			CurrentProgress = percent;
		}

		protected abstract void PerformOperation();
		public abstract void SetupFromManager (EscOperationManager manager);
	}

	// Operation involving storage API
	public abstract class StorageOperation : EscOperation {
		private StorageAPI client;
		public StorageAPI Client {
			get{return client; }
			set{ client = value; }
		}

		public StorageOperation() : base(){
		}

		public override void SetupFromManager(EscOperationManager manager){
			client = manager.CreateStorageClient ();
		}
	}

	// Operation involving workflow API
	/*
	public class WorkflowOperation : EscOperation {
	}
	*/

	// Operation involving dataset API
	/*
	public class DatasetOperation : EscOperation {
	}
	*/
}
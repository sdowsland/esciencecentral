﻿using System;

namespace Esc.Client
{
	public class DeleteFolderOperation : StorageOperation {
		private string folderId;

		public DeleteFolderOperation (EscFolder folder) {
			folderId = folder.id;
		}

		public DeleteFolderOperation(string folderId){
			this.folderId = folderId;
		}

		protected override void PerformOperation(){
			Client.DeleteFolder(folderId);
		}
	}
}
﻿using System;

namespace Esc.Client
{
	public class GetHomeDirectoryOperation : StorageOperation
	{
		private EscFolder home;
		public EscFolder Result{
			get{return home; }
		}

		public GetHomeDirectoryOperation () : base () {
		}

		protected override void PerformOperation(){
			home = Client.HomeFolder();
		}
	}
}


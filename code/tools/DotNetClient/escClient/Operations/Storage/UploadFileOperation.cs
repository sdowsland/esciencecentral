﻿using System;
using System.IO;
using System.Net;

namespace Esc.Client{
	public class UploadFileOperation : StorageOperation {
		private EscDocumentVersion result;
		public EscDocumentVersion Result {
			get{ return result; }
		}

		private string targetFolderId;
		private FileInfo sourceFile;

		public UploadFileOperation (FileInfo sourceFile, EscFolder targetFolder) {
			this.sourceFile = sourceFile;
			this.targetFolderId = targetFolder.id;
		}

		public UploadFileOperation(FileInfo sourceFile, string targetFolderId){
			this.sourceFile = sourceFile;
			this.targetFolderId = targetFolderId;
		}

		protected override void PerformOperation(){
			EscDocument document = Client.CreateDocumentInFolder (targetFolderId, sourceFile.Name);

			FileStream f = sourceFile.OpenRead ();
			HttpWebRequest request = Client.createRequest ("/data/" + document.id);

			request.Method = "POST";
			request.ContentType = "application/data";
			request.ContentLength = f.Length;
			Stream rs = request.GetRequestStream();
			double totalWork = f.Length;
			long bytesSent = 0;

			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = f.Read(buffer, 0, buffer.Length)) != 0) {
				rs.Write(buffer, 0, bytesRead);
				bytesSent += bytesRead;
				UpdatePercentage (totalWork, (double)bytesSent);
			}
			f.Close ();
			rs.Close ();

			WebResponse response = request.GetResponse ();
			Stream stream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (stream);
			string resultString = reader.ReadToEnd ();
			response.Close ();
			result = new EscDocumentVersion (resultString);
		}
	}
}
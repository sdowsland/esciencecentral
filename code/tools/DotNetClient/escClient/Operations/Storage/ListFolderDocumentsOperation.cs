﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace Esc.Client {
	public class ListFolderDocumentsOperation : StorageOperation {
		private string folderId;

		private List<EscDocument> results = new List<EscDocument>();
		public List<EscDocument> Results {
			get{ return results; }
		}

		public ListFolderDocumentsOperation (EscFolder folder) : base(){
			this.folderId = folder.id;
		}

		public ListFolderDocumentsOperation (string folderId) : base(){
			this.folderId = folderId;
		}

		protected override void PerformOperation(){
			EscDocument[] docs = Client.FolderDocuments (folderId);
			results = new List<EscDocument> ();
			foreach (EscDocument doc in docs) {
				results.Add (doc);
			}
		}
	}
}


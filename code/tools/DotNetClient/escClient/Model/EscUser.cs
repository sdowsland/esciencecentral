using System;
using Procurios.Public;
using System.Collections;

namespace Esc.Client
{
	public class EscUser : JSONSerializable
	{
		public String id;
		public String name;
		public String firstName;
		public String surname;
		public String organisationId;

		public EscUser ()
		{
		}

		public EscUser(string json){
			ParseJson(json);
		}

		public EscUser(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("firstName", firstName);
			ht.Add ("surname", surname);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) { 
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			firstName = ht.getString ("firstName", "");
			surname = ht.getString ("surname", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);

			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}
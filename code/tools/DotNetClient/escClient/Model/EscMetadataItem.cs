using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client
{
	public class EscMetadataItem : JSONSerializable
	{
		public enum METADATA_TYPE {
			TEXT,NUMERICAL,BOOLEAN,DATE
		};
		public METADATA_TYPE metadataType = METADATA_TYPE.TEXT;
		public string name;
		public string category;
		public string stringValue;
		public string objectId;
		public long id;

		public EscMetadataItem ()
		{
		}

		public EscMetadataItem(string json){
			ParseJson (json);
		}

		public EscMetadataItem(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("category", category);
			ht.Add ("stringValue", stringValue);

			if (metadataType == METADATA_TYPE.TEXT) {
				ht.Add ("metadataType", "text");
			} else if (metadataType == METADATA_TYPE.BOOLEAN) {
				ht.Add ("metadataType", "boolean");
			} else if (metadataType == METADATA_TYPE.NUMERICAL) {
				ht.Add ("metadataType", "numerical");
			} else if (metadataType == METADATA_TYPE.DATE) {
				ht.Add ("metadataType", "date");
			} else {
				ht.Add ("metadataType", "text");
			}
	
			ht.Add ("objectId", objectId);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getLong ("id", 0);
			name = ht.getString ("name", "");
			category = ht.getString ("category", "");
			stringValue = ht.getString ("stringValue", "");
			objectId = ht.getString ("objectId", "");
			string metadataTypeText = ht.getString ("type", "text");
			if (metadataTypeText.Equals ("text")) {
				metadataType = METADATA_TYPE.TEXT;

			} else if (metadataTypeText.Equals ("boolean")) {
				metadataType = METADATA_TYPE.BOOLEAN;

			} else if (metadataTypeText.Equals ("date")) {
				metadataType = METADATA_TYPE.DATE;

			} else if(metadataTypeText.Equals("numerical")){
				metadataType = METADATA_TYPE.NUMERICAL;

			} else {
				metadataType = METADATA_TYPE.TEXT;

			}
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);

			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}

	}
}


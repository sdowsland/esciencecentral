using System;
using Procurios.Public;
using System.Collections;

namespace Esc.Client {
	public class EscWorkflowInvocation : JSONSerializable {
		public string id;
		public string name;
		public string description;
		public string creatorId;
		public string containerId;
		public string status;
		public string workflowId;
		public string workflowVersionId;
		public string projectId;
		public long startTimestamp;
		public long endTimestamp;

		public EscWorkflowInvocation () {
		}

		public EscWorkflowInvocation (string json) {
			ParseJson (json);
		}

		public EscWorkflowInvocation (Hashtable ht) {
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("description", description);
			ht.Add ("creatorId", creatorId);
			ht.Add ("containerId", containerId);
			ht.Add ("status", status);
			ht.Add ("workflowId", workflowId);
			ht.Add ("workflowVersionId", workflowVersionId);
			ht.Add ("projectId", projectId);
			ht.Add ("startTimestamp", startTimestamp);
			ht.Add ("endTimestamp", endTimestamp);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			description = ht.getString ("description", "");
			creatorId = ht.getString ("creatorId", "");
			containerId = ht.getString ("containerId", "");
			status = ht.getString ("status", "");
			workflowId = ht.getString ("workflowId", "");
			projectId = ht.getString ("projectId", "");
			startTimestamp = ht.getLong ("startTimestamp", 0L);
			endTimestamp = ht.getLong ("endTimestamp", 0L);
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);

			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}
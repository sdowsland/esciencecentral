using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client
{
	public class EscFolder : JSONSerializable
	{
		public String id;
		public String name;
		public String description;
		public String organisationId;
		public String containerId;
		public String creatorId;

		public EscFolder ()
		{
		}

		public EscFolder(string json){
			ParseJson (json);
		}

		public EscFolder(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("description", description);
			ht.Add ("containerId", containerId);
			ht.Add ("organisationId", organisationId);
			ht.Add ("creatorId", creatorId);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			description = ht.getString ("description", "");
			containerId = ht.getString ("containerId", "");
			organisationId = ht.getString ("organisationId", "");
			creatorId = ht.getString ("creatorId", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);

			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}

		public override string ToString () {
			return name;
		}
	}
}


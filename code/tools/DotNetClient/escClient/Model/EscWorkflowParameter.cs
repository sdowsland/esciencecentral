using System;
using Procurios.Public;
using System.Collections;

namespace Esc.Client {
	public class EscWorkflowParameter : JSONSerializable {
		public string blockName;
		public string name;
		public string value;

		public EscWorkflowParameter () {
		}

		public EscWorkflowParameter (string json) {
			ParseJson (json);
		}

		public EscWorkflowParameter (Hashtable ht) {
			ParseHashtable (ht);
		}

		public string ToJson(){
			return JSON.JsonEncode (ToHashTable());
		}

		public Hashtable ToHashTable(){
			Hashtable ht = new Hashtable ();
			ht.Add ("blockName", blockName);
			ht.Add ("name", name);
			ht.Add ("value", value);
			return ht;
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			blockName = ht.getString ("blockName", "");
			name = ht.getString ("name", "");
			value = ht.getString ("value", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}
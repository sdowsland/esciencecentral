/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.net.ConnectException;


public class Init
{
    private static class MainOptions {
        @Parameter(names = { "-h", "--help" }, description = "show the help message", help = true)
        public boolean showHelp = false;

        @Parameter(names = { "--encodedPath" }, description = "if set, the assumption is that the path segment of URLs used as SOURCE or DEST are correctly encoded. Default [false] means they will be encoded during command execution")
        public boolean encodedPath = false;
    }


    public static void main(String[] args)
    {
        MainOptions opts = new MainOptions();
        JCommander jc = new JCommander(opts);

        try {
            Cp cp = new Cp(jc, false);
            Cp mv = new Cp(jc, true);
            Ls ls = new Ls(jc);

            jc.parse(args);

            if (opts.showHelp || jc.getParsedCommand() == null) {
                jc.usage();
                System.exit(1);
            }

            switch (jc.getParsedCommand()) {
                case "cp":
                    cp.encodedPath = opts.encodedPath;
                    cp.exec();
                    break;
                case "mv":
                    mv.encodedPath = opts.encodedPath;
                    mv.exec();
                    break;
                case "ls":
                    ls.encodedPath = opts.encodedPath;
                    ls.exec();
                    break;
                default:
                    throw new IllegalArgumentException("Unknown command: " + jc.getParsedCommand());
            }
        } catch (ParameterException x) {
            System.err.println("Invalid argument: " + x.getMessage());
        } catch (IllegalArgumentException x) {
            System.err.println("Invalid argument: " + x.getMessage());
            //jc.usage(jc.getParsedCommand());
        } catch (ToolException x) {
            System.err.println("Error running command " + jc.getParsedCommand() + ": " + x.getMessage());
        } catch (Exception x) {
            System.err.println("Error running command " + jc.getParsedCommand() + ": " + x);
        }
    }
}

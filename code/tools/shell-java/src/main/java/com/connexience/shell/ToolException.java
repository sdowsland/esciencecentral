/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell;

import java.text.MessageFormat;

/**
 * Created by Jacek on 13/09/2016.
 */
public class ToolException extends Exception
{
    public ToolException() { super(); }
    public ToolException(String message) { super(message); }
    public ToolException(String message, Throwable th) { super(message, th); }
    public ToolException(Throwable th) { super(th); }
    public ToolException(String message, Throwable th, boolean enableSuppression, boolean writableStackTrace) { super(message, th, enableSuppression, writableStackTrace); }

    public ToolException(String messagePattern, Object... messageArgs)
    {
        super(MessageFormat.format(messagePattern, messageArgs));
    }

    public ToolException(Throwable th, String messagePattern, Object... messageArgs)
    {
        super(MessageFormat.format(messagePattern, messageArgs), th);
    }

    public ToolException(Throwable th, boolean enableSuppression, boolean writableStackTrace, String messagePattern, Object... messageArgs)
    {
        super(MessageFormat.format(messagePattern, messageArgs), th, enableSuppression, writableStackTrace);
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell;

import java.io.File;
import java.net.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Pattern;

import com.beust.jcommander.JCommander;
import com.connexience.api.StorageClient;
import com.connexience.api.WorkflowClient;
import com.connexience.api.model.*;
import com.connexience.shell.misc.AdaptiveWriteProgress;
import com.connexience.shell.misc.Tokenizer;

import com.beust.jcommander.Parameter;


public class Cp implements IApplication
{
    private static final String _COMMAND_NAME_COPY = "cp";
    private static final String _COMMAND_NAME_MOVE = "mv";

    private static class AccountInfo {
        URI eSCuri;
        String path;
    }

    boolean _createDirIfNeeded = true;
    boolean _overwrite = false;
    boolean _allowDuplicatedNames = false;
    boolean _allowMissingFiles = true;
    boolean encodedPath = false;

    @Parameter(description = "SOURCE... DEST")
    public List<String> _files = new ArrayList<>();

    @Parameter(names = {"-q", "--quiet"}, description = "be quiet, do not show what is being done")
    public boolean _beQuiet = false;

    @Parameter(names = {"-v", "--verbose"}, description = "show progress for each individual file")
    public boolean _beVerbose = false;

    @Parameter(names = {"-h", "--help"}, description = "show help message", help = true)
    public boolean _showHelp = false;

    @Parameter(names = {"-u", "--username"}, description = "user name used for logging in to the e-SC system")
    public String _userName;

    @Parameter(names = {"-p", "--password"}, description = "user password used for logging in to the e-SC system")
    public String _userPass;

    @Parameter(names = {"-w", "--workflow"}, description = "copy in the files as workflows; ignored when files are only downloaded")
    boolean _asWorkflow;

    @Parameter(names = {"-r", "--recursive"}, description = "copy recursively")
    boolean _isRecursiveOn = false;

    @Parameter(names = {"-c", "--resolve-conflicts"}, description = "copy source files even if they have the same name in the source e-SC data storage")
    boolean _isResolveConflictOn = false;

    @Parameter(names = {"--public"}, description = "make the uploaded file public")
    boolean _asPublic;

    boolean _removeSource = false;

    AccountInfo[] _arguments;

    private final JCommander _jCommander;

    /**
     * Stores information whether a source path used during copy is exact or contains one or more wildcards.
     * Used once per source argument.
     */
    private boolean _isSourceWithWildcards;

    /**
     * Stores information whether a source path used during copy is valid and matched a path on the server.
     * Used once per source argument.
     */
    private boolean _isMatchMade;


    Cp(JCommander jCommander, boolean removeSource) {
        _jCommander = jCommander;
        _removeSource = removeSource;

        jCommander.addCommand(
                removeSource ? _COMMAND_NAME_MOVE : _COMMAND_NAME_COPY,
                this);
    }


    @Override
    public void exec() throws Exception {
        //_options = new Options();
        //new JCommander(_options, args);

        //_checkCommand();

        //_arguments = _parseArgs(args);

        //if (_arguments == null) {
        //    help(new String[0]);
        //}
        if (_showHelp) {
            _jCommander.usage(_removeSource ? _COMMAND_NAME_MOVE : _COMMAND_NAME_COPY);
            System.exit(1);
        }

        if (_files.size() < 2) {
            throw new IllegalArgumentException("Missing argument(s): at least two paths/file names are required.");
        }

        //assert (_arguments.length > 1);
        _arguments = _parseFiles();

        AccountInfo destination = _arguments[_arguments.length - 1];
        if (destination.eSCuri != null) {
            _copyToServer(destination);
        } else {
            _copyToLocal(destination);
        }
    }

    //private final static String helpMessage = 
    //        "Usage cp [OPTION]... [-T] SOURCE DEST\n" + 
    //        "  or: cp [OPTION]... SOURCE... DEST\n" +
    //        "  or: cp [OPTION]... -t DIRECTORY SOURCE...\n" +
    //        "Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.\n\n" +
    //        "  -t, --target-directory=DIRECTORY  copy all SOURCE arguments into DIRECTORY\n" +
    //        "  -T, --no-target-directory    treat DEST as normal file\n" +
    //        "  -q, --quiet                  be quiet, do not show what is being done\n" +
    //        "  -v, --verbose                show progress for each individual file\n" +
    //        "  -h, --help                   show this help message\n" + 
    //        "  -u, --username               user name to use for logging in to the e-SC system\n" + 
    //        "  -p, --password               user password for logging in to the e-SC system";


    private static final String ESC_SCHEMA = "esc://";


    private ArrayList<EscFolder> _listFolders(StorageClient client, EscFolder baseFolder, String parentPath, String nameWithWildcard, boolean isSingleTargetFile) throws ToolException {
        ArrayList<EscFolder> outputList = new ArrayList<>();

        try {
            if (Common.IsWildcard(nameWithWildcard)) {
                Pattern namePattern = Common.CreateRegexFromGlob(nameWithWildcard);
                _isSourceWithWildcards = true;

                if (isSingleTargetFile) {
                    throw new IllegalArgumentException("Source path that includes wildcard(s) cannot be copied to a single destination file.");
                }

                for (EscFolder f : client.listChildFolders(baseFolder.getId())) {
                    if (namePattern.matcher(f.getName()).matches()) {
                        outputList.add(f);
                    }
                }
            } else if (isSingleTargetFile) {
                for (EscFolder f : client.listChildFolders(baseFolder.getId())) {
                    if (nameWithWildcard.equals(f.getName())) {
                        if (outputList.size() > 0) {
                            throw new ToolException("Invalid destination path: source path: {0}/{1} indicates two folders with the same name: {2}, and so cannot be copied to a single destination file.",
                                    parentPath, nameWithWildcard, f.getName());
                        }
                        outputList.add(f);
                    }
                }
            } else {
                for (EscFolder f : client.listChildFolders(baseFolder.getId())) {
                    if (nameWithWildcard.equals(f.getName())) {
                        outputList.add(f);
                    }
                }
            }

            return outputList;
        } catch (IllegalArgumentException | ToolException x) {
            throw x;
        } catch (Exception x) {
            throw new ToolException(x, "Cannot access source path: {0}/{1}: {2}", parentPath, nameWithWildcard, x.getMessage());
        }
    }


    private int _listAndCopyToLocal(StorageClient client, ArrayList<EscFolder> baseFolders, String basePath, Tokenizer st, Path targetDir, boolean isSingleTargetFile) throws Exception
    {
        if (baseFolders.size() < 1) {
            return 0;
        } else if (isSingleTargetFile && baseFolders.size() > 1) {
            throw new ToolException("Internal error: multiple source folders for a single destination file; basePath = {0}", basePath);
        }

        if (st.hasMoreTokens()) {
            String nameWithWildcard = st.nextToken();
            if ("**".equals(nameWithWildcard)) {
                _isSourceWithWildcards = true;

                // [TODO]
                throw new UnsupportedOperationException("Support for the '**' wildcard has not yet been implemented.");
            }

            if (st.hasMoreTokens()) {
                // More tokens in the source path. Look further down...
                //
                int copyMade = 0;
                for (EscFolder srcFolder : baseFolders) {
                    ArrayList<EscFolder> subFolders = _listFolders(client, srcFolder, basePath, nameWithWildcard, isSingleTargetFile);
                    if (subFolders.size() > 0) {
                        copyMade += _listAndCopyToLocal(client, subFolders, basePath + "/" + srcFolder.getName(), new Tokenizer(st), targetDir, isSingleTargetFile);
                    }
                }
                return copyMade;
            } else {
                // No more tokens in the source path. Collect source objects and start copying...
                //
                if (Common.IsWildcard(nameWithWildcard)) {
                    Pattern namePattern = Common.CreateRegexFromGlob(nameWithWildcard);
                    _isSourceWithWildcards = true;

                    if (isSingleTargetFile) {
                        throw new IllegalArgumentException("A path with a wildcard cannot be copied to a single destination file.");
                    }

                    if (_isRecursiveOn) {
                        int copyMade = 0;

                        for (EscFolder baseFolder : baseFolders) {
                            for (EscFolder srcFolder : client.listChildFolders(baseFolder.getId())) {
                                if (namePattern.matcher(srcFolder.getName()).matches()) {
                                    _isMatchMade = true;
                                    copyMade += _copyFolderToLocal(client, srcFolder, targetDir);
                                }
                            }

                            for (EscDocument srcDoc : client.folderDocuments(baseFolder.getId())) {
                                if (namePattern.matcher(srcDoc.getName()).matches()) {
                                    _isMatchMade = true;
                                    _copyFileToLocal(client, srcDoc, targetDir);
                                    copyMade++;
                                }
                            }
                        }

                        return copyMade;
                    } else {
                        int copyMade = 0;

                        for (EscFolder baseFolder : baseFolders) {
                            for (EscFolder srcFolder : client.listChildFolders(baseFolder.getId())) {
                                if (namePattern.matcher(srcFolder.getName()).matches()) {
                                    _isMatchMade = true;
                                    System.out.println("Non-recursive copy: omitting source folder: " + basePath + "/" + baseFolder.getName() + "/" + srcFolder.getName());
                                }
                            }

                            for (EscDocument srcDoc : client.folderDocuments(baseFolder.getId())) {
                                if (namePattern.matcher(srcDoc.getName()).matches()) {
                                    _isMatchMade = true;
                                    _copyFileToLocal(client, srcDoc, targetDir);
                                    copyMade++;
                                }
                            }
                        }

                        return copyMade;
                    }
                } else {
                    if (_isRecursiveOn) {
                        int copyMade = 0;

                        for (EscFolder baseFolder : baseFolders) {
                            for (EscFolder srcFolder : client.listChildFolders(baseFolder.getId())) {
                                if (nameWithWildcard.equals(srcFolder.getName())) {
                                    _isMatchMade = true;
                                    copyMade += _copyFolderToLocal(client, srcFolder, targetDir);
                                }
                            }

                            for (EscDocument srcDoc : client.folderDocuments(baseFolder.getId())) {
                                if (nameWithWildcard.equals(srcDoc.getName())) {
                                    _isMatchMade = true;
                                    if (copyMade > 0) {
                                        throw new IllegalArgumentException("Name conflict at the source: two files with the same name exists at path " + basePath + "/" + baseFolder.getName() + "/" + nameWithWildcard + ". Use option --resolve-conflicts to download conflicting files.");
                                    } else {
                                        _copyFileToLocal(client, srcDoc, targetDir);
                                        copyMade++;
                                    }
                                }
                            }
                        }

                        return copyMade;
                    } else {
                        int copyMade = 0;

                        for (EscFolder baseFolder : baseFolders) {
                            for (EscFolder srcFolder : client.listChildFolders(baseFolder.getId())) {
                                if (nameWithWildcard.equals(srcFolder.getName())) {
                                    _isMatchMade = true;
                                    System.out.println("Non-recursive copy: omitting source folder: " + basePath + "/" + baseFolder.getName() + "/" + srcFolder.getName());
                                }
                            }

                            for (EscDocument srcDoc : client.folderDocuments(baseFolder.getId())) {
                                if (nameWithWildcard.equals(srcDoc.getName())) {
                                    _isMatchMade = true;
                                    if (copyMade > 0) {
                                        throw new IllegalArgumentException("Name conflict at the source: two files with the same name exists at path " + basePath + "/" + baseFolder.getName() + "/" + nameWithWildcard + ". Use option --resolve-conflicts to download conflicting files.");
                                    } else {
                                        _copyFileToLocal(client, srcDoc, targetDir);
                                        copyMade++;
                                    }
                                }
                            }
                        }

                        return copyMade;
                    }
                }
            }
        } else {
            throw new IllegalStateException(MessageFormat.format("Internal error: basePath = {0}, srcFolders.size = {1}, no more tokens", basePath, baseFolders.size()));
        }
    }

    private int _copyFolderToLocal(StorageClient client, EscFolder srcFolder, Path targetDir) throws Exception {
        int copiedFiles = 0;
        Path targetFolder = targetDir.resolve(srcFolder.getName());
        if (!_overwrite) {
            try {
                Files.createDirectory(targetFolder);
            } catch (FileAlreadyExistsException x) {
                throw new IllegalArgumentException("Cannot overwrite directory: " + targetFolder);
            }
        } else {
            Files.createDirectories(targetFolder);
        }
        if (!_beQuiet) {
            System.out.println("'" + srcFolder.getName() + "' --> '" + targetFolder + "'");
        }

        for (EscDocument doc : client.folderDocuments(srcFolder.getId())) {
            _copyFileToLocal(client, doc, targetFolder);
            copiedFiles++;
        }

        for (EscFolder folder : client.listChildFolders(srcFolder.getId())) {
            copiedFiles += _copyFolderToLocal(client, folder, targetFolder);
            if (_removeSource) {
                client.deleteFolder(folder.getId());
            }
        }

        return copiedFiles;
    }


    private void _copyFileToLocal(StorageClient client, EscDocument srcDocument, Path targetDir) throws Exception {
        Path targetFile = targetDir.resolve(srcDocument.getName());
        if (!_overwrite && Files.exists(targetFile)) {
            throw new IllegalArgumentException("Cannot overwrite file: " + targetFile);
        }
        if (!_beQuiet) {
            System.out.println("'" + srcDocument.getName() + "' --> '" + targetFile + "'");
        }
        client.download(srcDocument, targetFile.toFile());
        if (_removeSource) {
            client.deleteDocument(srcDocument.getId());
        }
    }


    private void _copyToLocal(AccountInfo destInfo) throws Exception
    {
        AccountInfo src = null;

        if (_isResolveConflictOn) {
            // [TODO]
            throw new UnsupportedOperationException("Copying with name conflict resolution has not yet been implemented.");
        }

        try {
            int sourceCount = _arguments.length - 1;
            Path targetDir = Paths.get(destInfo.path);

            // Check whether destination is a single file or directory
            //
            boolean singleFile;
            if (Files.isDirectory(targetDir)) {
                singleFile = false;
            } else {
                if (Files.exists(targetDir)) {
                    if (!_overwrite) {
                        throw new IllegalArgumentException("Invalid destination: cannot overwrite existing file: " + targetDir);
                    } else {
                        singleFile = true;
                    }
                } else {
                    Path parent = targetDir.getParent();
                    if (parent == null || Files.isDirectory(targetDir.getParent())) {
                        singleFile = true;
                    } else {
                        throw new IllegalArgumentException("Invalid destination directory: " + targetDir);
                    }
                }
            }

            if (singleFile && sourceCount > 1) {
                throw new IllegalArgumentException("Invalid destination: path '" + targetDir + "' is not a directory");
            }

            for (int i = 0; i < sourceCount; i++) {
                src = _arguments[i];

                if (src.eSCuri == null) {
                    throw new UnsupportedOperationException("Cannot copy a local file to a local destination. Use your regular OS copy tool to do this.");
                }

                String[] userPass = Common.PrepareCredentials(src.eSCuri, _userName, _userPass);
                StorageClient client = new StorageClient(src.eSCuri.getHost(), src.eSCuri.getPort(), false, userPass[0], userPass[1]);

                Tokenizer st = new Tokenizer(src.path, '/');
                if (st.hasMoreTokens()) {
                    String name = st.nextToken();
                    int copiedFiles;

                    _isSourceWithWildcards = false;

                    if ("home".equals(name)) {
                        ArrayList<EscFolder> folders = new ArrayList<>();
                        folders.add(client.homeFolder());
                        copiedFiles = _listAndCopyToLocal(client, folders, "", st, targetDir, singleFile);
                    } else if ("project".equals(name)) {
                        if (st.hasMoreTokens()) {
                            String projectName = st.nextToken();
                            EscProject srcProject = null;
                            for (EscProject project : client.listProjects()) {
                                if (projectName.equals(project.getName())) {
                                    srcProject = project;
                                    break;
                                }
                            }

                            if (srcProject == null) {
                                throw new IllegalArgumentException("Invalid source path: project '" + projectName + "' does not exist or is not available for user: " + userPass[0]);
                            } else {
                                ArrayList<EscFolder> folders = new ArrayList<>();
                                folders.add(client.getFolder(srcProject.getDataFolderId()));
                                copiedFiles = _listAndCopyToLocal(client, folders, "/project", st, targetDir, singleFile);
                            }
                        } else {
                            throw new IllegalArgumentException("Invalid source path: expected project name");
                        }
                    } else {
                        throw new IllegalArgumentException("Unexpected path element. Please use '/home/' or '/project/<PROJECT_NAME>/ to indicate user's home folder or a project they have access to.");
                    }

                    if (copiedFiles == 0) {
                        if (_isSourceWithWildcards || _isMatchMade) {
                            if (!_beQuiet) {
                                System.out.println("No files copied.");
                            }
                        } else {
                            throw new IllegalArgumentException("Invalid source path: " + src.path);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid source path: " + src.path + ". Please start with '/home/' or '/project/<PROJECT_NAME>/");
                }
            }
        } catch (ConnectException x) {
            throw new ToolException("Cannot access remote server: {0}: {1}", Common.ToSafeString(src.eSCuri), x.getMessage());
        }
    }


    private void _copyToServer(AccountInfo destInfo) throws Exception
    {
        assert (_arguments.length > 1);
        
        String[] userPass = Common.PrepareCredentials(destInfo.eSCuri, _userName, _userPass);

        StorageClient destStorageCli = new StorageClient(destInfo.eSCuri.getHost(), destInfo.eSCuri.getPort(), false, userPass[0], userPass[1]);
        WorkflowClient destWorkflowCli = _asWorkflow ? new WorkflowClient(destStorageCli) : null;

        AdaptiveWriteProgress progressCallback = _beVerbose ? new AdaptiveWriteProgress() : null;

        // Copy to a folder if there are more than 2 arguments or
        //      the last one ends with '/' (directory) or
        //      flag asWorkflows is set and all files will become workflows
        if (_asWorkflow || _arguments.length > 2 || destInfo.path.endsWith("/")) {
            EscFolder destFolder = _getFolder(destStorageCli, destInfo.path, _createDirIfNeeded);

            // Remember that the last argument is destInfo
            for (int i = 0; i < _arguments.length - 1; i++) {
                AccountInfo srcInfo = _arguments[i];
                if (srcInfo.eSCuri != null)
                    // [TODO]
                    throw new UnsupportedOperationException("Cross eSC-eSC copying not currently supported");
                else {
                    File f = new File(srcInfo.path);
                    if (!f.canRead()) {
                        String message = "Cannot read file '" + f + "'";
                        if (_allowMissingFiles) {
                            System.out.println(message);
                            continue;
                        } else {
                            throw new IllegalArgumentException(message);
                        }
                    }

                    if (progressCallback != null) {
                        progressCallback.setMessage(f.getName());
                    } else if (!_beQuiet) {
                        System.out.println(f.getName());
                    }

                    EscDocumentVersion v;
                    if (_asWorkflow) {
                        // Copy in a workflow.
                        // Note that setting workflow IDs and external block IDs is not supported.
                        EscWorkflow workflow = new EscWorkflow();
                        workflow.setName(f.getName());
                        workflow.setContainerId(destFolder.getId());
                        workflow.setDescription("Uploaded by e-SC shell tools");
                        workflow = destWorkflowCli.saveWorkflow(workflow);

                        v = destStorageCli.upload(workflow, f, progressCallback);
                    } else {
                        // Copy in a regular file.
                        v = destStorageCli.upload(destFolder, f, progressCallback);
                    }

                    if (v == null || "".equals(v.getId())) {
                        System.out.println("Upload error: server returned no version information");
                    }

                    if (_asPublic) {
                        System.out.println("Making the document public");
                        EscPermission p = new EscPermission();
                        p.setPermissionType(EscPermission.READ);
                        p.setPrincipalId(destStorageCli.publicUser().getId());
                        destStorageCli.grantPermission(v.getDocumentRecordId(), p);
                    }
                }
            }
        // Copy a file under a specified name
        } else { // if (_arguments.length == 2 && !destInfo.path.endsWith("/"))
            EscFolder destFolder;
            EscDocument destDoc;

            int lastSlash = destInfo.path.lastIndexOf('/');
            if (lastSlash > -1) {
                destFolder = _getFolder(destStorageCli, destInfo.path.substring(0, lastSlash), _createDirIfNeeded);
                if (!_allowDuplicatedNames && 
                    _getDocumentOrFolder(destStorageCli, destFolder, destInfo.path.substring(lastSlash + 1)) != null) {
                    throw new IllegalArgumentException("Target document or folder exists. Consider using option --allowDuplicates");
                }
                destDoc = destStorageCli.createDocumentInFolder(destFolder.getId(), destInfo.path.substring(lastSlash + 1));
            } else {
                destFolder = destStorageCli.homeFolder();
                destDoc = destStorageCli.createDocumentInFolder(destFolder.getId(), destInfo.path);
            }
            AccountInfo srcInfo = _arguments[0];
            if (srcInfo.eSCuri != null)
                throw new IllegalArgumentException("Cross eSC-eSC copying not currently supported");
            else {
                File f = new File(srcInfo.path);
                if (!f.canRead())
                    throw new IllegalArgumentException("Cannot read file '" + f + "'");

                progressCallback.setMessage(f.getName());
                destStorageCli.upload(destDoc, f, progressCallback);
            }
        }
    }

    private static Object _getDocumentOrFolder(StorageClient clientRef, EscFolder parent, String name) throws Exception
    {
        for (EscFolder f : clientRef.listChildFolders(parent.getId())) {
            if (name.equals(f.getName()))
                return f;
        }
        for (EscDocument d : clientRef.folderDocuments(parent.getId())) {
            if (name.equals(d.getName()))
                return d;
        }
        return null;
    }


    private static EscFolder _getFolder(StorageClient clientRef, String path, boolean createIfNeeded)
    throws Exception
    {
        EscFolder currentFolder;

        if (path.startsWith("/home/") || path.startsWith("home/")) {
            currentFolder = clientRef.homeFolder();
        } else {
            throw new UnsupportedOperationException("Uploading data to project is currently unsupported.");
        }
        StringTokenizer st = new StringTokenizer(path, "/");
        String fullPath = "/";
        while (st.hasMoreTokens()) {
            String folderName = st.nextToken();
            if ("".equals(folderName))
                continue;

            fullPath += folderName;
            boolean folderExists = false;
            for (EscFolder subdir : clientRef.listChildFolders(currentFolder.getId())) {
                if (folderName.equals(subdir.getName())) {
                    currentFolder = subdir;
                    folderExists = true;
                    break;
                }
            }
            
            if (folderExists)
                continue;
            else if (createIfNeeded)
                currentFolder = clientRef.createChildFolder(currentFolder.getId(), folderName);
            else
                throw new IllegalArgumentException("Can't locate folder '" + fullPath + "'");
        }

        return currentFolder;
    }


    private AccountInfo[] _parseFiles() throws MalformedURLException
    {
        // Parse arguments
        ArrayList<AccountInfo> output = new ArrayList<>(_files.size());

        try {
            if (_files.size() < 2) {
                throw new IllegalArgumentException("Missing argument");
            }

            for (String file : _files) {
                AccountInfo a = new AccountInfo();
                
                if (file.startsWith(ESC_SCHEMA)) {
                    if (encodedPath) {
                        a.eSCuri = new URI(file);
                    } else {
                        // Encoding trick based on the stackoverflow answer by M Abdul Sami at
                        // http://stackoverflow.com/questions/10786042/java-url-encoding-of-query-string-parameters
                        URL url = new URL("http://" + file.substring(ESC_SCHEMA.length()));
                        a.eSCuri = new URI(
                                "esc", url.getUserInfo(), url.getHost(), url.getPort(),
                                url.getPath(), url.getQuery(), url.getRef());
                    }
                    a.path = a.eSCuri.getPath();
                } else { // If not esc:// then assume local path
                    a.eSCuri = null; // filesystem
                    a.path = file;
                }

                output.add(a);
            }

            return output.toArray(new AccountInfo[output.size()]);
        } catch (URISyntaxException x) {
            throw new IllegalArgumentException(x.getMessage());
        }
    }
}

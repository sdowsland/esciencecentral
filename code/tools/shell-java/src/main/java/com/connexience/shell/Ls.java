/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class Ls implements IApplication
{
    @Parameter(names = { "-h", "--help" }, description = "show help message", help = true)
    public boolean _showHelp = false;

    boolean encodedPath = false;

    private final JCommander _jCommander;

    private static final String _COMMAND_NAME = "ls";


    public Ls(JCommander jCommander)
    {
        _jCommander = jCommander;
        jCommander.addCommand(_COMMAND_NAME, this);
    }


    @Override
    public void exec() throws Exception
    {
        if (_showHelp) {
            _jCommander.usage(_COMMAND_NAME);
            System.exit(1);
        }

        throw new UnsupportedOperationException("This operation has not yet been implemented.");
    }
}

<?php
/*
 * This file contains templates for the various data types supported by the
 * e-Science Central API.
 */

/**
 * This class provides a data structure for an EscFolder. 
 */
class EscFolder {
    public $id;
    public $name;
    public $creatorId;
    public $description;
    public $projectId;
}

/**
 * This class provides an EscDocument data structure
 */
class EscDocument {
    public $id;
    public $name;
    public $creatorId;
    public $description;
    public $projectId;
    public $currentVersionSize;
    public $currentVersionNumber;
    public $downloadPath;
    public $uploadPath;    
}
    
/**
 * EscUser class
 */
class EscUser {
    public $id;
    public $name;
    public $firstName;
    public $surname;
}

/**
 * This class provides a Document Version data structure
 */
class EscDocumentVersion {
    public $id;
    public $documentRecordId;
    public $comments;
    public $userId;
    public $versionNumber;
    public $size;
    public $timestamp;
    public $downloadPath;
    public $md5;    
}

/**
 * This class provides an EscProject object
 */
class EscProject {
    public $id;
    public $name;
    public $creatorId;
    public $description;
    public $workflowFolderId;
    public $dataFolderId;
}

/**
 * This class provides a metadata object
 */
class EscMetadataItem {
    public $metadataType = "text";
    public $name;
    public $category;
    public $stringValue;
    public $objectId;
    public $id;
}

/**
 * This class provides a workflow data type
 */
class EscWorkflow {
    public $id;
    public $name;
    public $creatorId;
    public $description;
    public $projectId;
    public $currentVersionSize;
    public $currentVersionNumber;    
}

/**
 * This class provides a workflow invocation data type
 */
class EscWorkflowInvocation {
    public $id;
    public $name;
    public $creatorId;
    public $description;
    public $projectId;
    public $status;
    public $workflowId;
    public $workflowVersionId;
    public $startTimestamp;
    public $endTimestamp;    
}
?>

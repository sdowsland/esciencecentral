﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using com.connexience.server.model;
using com.connexience.server.model.document;
using com.connexience.server.model.folder;
using com.connexience.server.model.metadata;
using com.connexience.server.model.workflow;
using com.connexience.server.model.social;
using com.connexience.server.model.properties;


namespace e_SC
{
    /// <summary>
    /// This class provides .NET binding for the public REST IStorage interface, revision 764.
    /// </summary>
    public partial class StorageAPI
    {
        public StorageAPI(string serverAddressURI)
        {
            _clientRef = new HttpClient();
            _clientRef.BaseAddress = new Uri(serverAddressURI);
            _clientRef.DefaultRequestHeaders.ExpectContinue = false;
        }


        public StorageAPI(string serverAddressURI, string userName, string userPassword)
            : this(serverAddressURI, new NetworkCredential(userName, userPassword))
        { }


        public StorageAPI(string serverAddressURI, System.Net.ICredentials credentials)
        {
            _clientRef = new HttpClient(
                new HttpClientHandler()
                {
                    Credentials = credentials,
                });
            _clientRef.BaseAddress = new Uri(serverAddressURI);
            _clientRef.DefaultRequestHeaders.ExpectContinue = false;
        }


        public async Task<long> GetTimestamp()
        {
            HttpResponseMessage response = await _clientRef.DeleteAsync("/api/rest/storage/timestamp");
            response.EnsureSuccessStatusCode();
            string s = await response.Content.ReadAsStringAsync();
            return long.Parse(s);
        }


        public async Task<Folder> GetHomeFolderAsync()
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/specialfolders/home");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<Folder>();
        }


        public async Task<Folder> GetFolderByIdAsync(string folderId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/folders/" + folderId);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<Folder>();
        }


        public async Task<T[]> GetFolderContentsByIdAsync<T>(string folderId) where T : ServerObject
        {
            string typePathId;

            if (typeof(T) == typeof(ServerObject))
                typePathId = "/contents";
            else if (typeof(T) == typeof(DocumentRecord))
                typePathId = "/documents";
            else if (typeof(T) == typeof(Folder))
                typePathId = "/children";
            else
                throw new ArgumentException("Unsupported type");

            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/folders/" + folderId + typePathId);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<T[]>();
        }


        public async Task<Folder> CreateChildFolderAsync(string parentFolderId, string folderName)
        {
            HttpResponseMessage response = await _clientRef.PostAsync(
                "/api/rest/storage/folders/" + parentFolderId + "/children",
                //new System.Net.Http.FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("name", folderName) }))
                // FIXME: Inconsistency in the IStorageAPI. Discuss it with Hugo.
                new StringContent(folderName));
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<Folder>();
        }


        public async Task DeleteFolder(string folderId)
        {
            HttpResponseMessage response = await _clientRef.DeleteAsync("/api/rest/storage/folders/" + folderId);
            response.EnsureSuccessStatusCode();
        }


        public async Task<DocumentRecord> CreateDocumentAsync(string parentFolderId, string documentName)
        {
            HttpResponseMessage response = await _clientRef.PostAsync(
                "/api/rest/storage/folders/" + parentFolderId + "/contents",
                new System.Net.Http.FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("name", documentName) }));
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<DocumentRecord>();
        }


        public async Task<DocumentRecord> GetDocumentRecordAsync(string documentId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/documents/" + documentId);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<DocumentRecord>();
        }


        public async Task<DocumentVersion[]> GetDocumentVersionsAsync(string documentId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/documents/" + documentId + "/versions");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<DocumentVersion[]>();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="versionId">A specific version id or <c>latest</c> to get the latest version of the document</param>
        /// <returns></returns>
        public async Task<DocumentVersion> GetDocumentVersionAsync(string documentId, string versionId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/documents/" + documentId + "/versions/" + versionId);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<DocumentVersion>();
        }


        public async Task DeleteDocumentAsync(string documentId)
        {
            HttpResponseMessage response = await _clientRef.DeleteAsync("/api/rest/storage/documents/" + documentId);
            response.EnsureSuccessStatusCode();
        }


        public async Task<ServerObject[]> MetadataSearch(MetadataQuery searchQuery)
        {
            HttpResponseMessage response = await _clientRef.PostContentAsJsonAsync("/api/rest/storage/search/metadata", searchQuery);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<ServerObject[]>();
        }

        
        public async Task<int> MetadataSearchSize(MetadataQuery searchQuery)
        {
            HttpResponseMessage response = await _clientRef.PostContentAsJsonAsync("/api/rest/storage/search/metadata/size", searchQuery);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<int>();
        }

        
        public async Task<MetadataSynonym[]> getSynonyms()
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/metadata/synonyms");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<MetadataSynonym[]>();
        }

        
        public async Task<MetadataSynonym> AddSynonym(MetadataSynonym synonym)
        {
            HttpResponseMessage response = await _clientRef.PostContentAsJsonAsync("/api/rest/storage/metadata/synonyms", synonym);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<MetadataSynonym>();
        }


        public async Task<MetadataItem> AddMetadataAsync(string documentId, MetadataItem metadata)
        {
            HttpResponseMessage response = await _clientRef.PostContentAsJsonAsync(
                "/api/rest/storage/documents/" + documentId + "/metadata",
                metadata);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<MetadataItem>();
        }


        public async Task<MetadataItem[]> GetDocumentMetadataAsync(string documentId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/documents/" + documentId + "/metadata");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<MetadataItem[]>();
        }


        public async Task DownloadDocumentAsync(string documentId, Stream outputStream)
        {
            Stream s = await _clientRef.GetStreamAsync("/api/data/" + documentId + "/latest");
            await s.CopyToAsync(outputStream);
        }


        public async Task UploadDocumentAsync(string documentId, Stream inputStream)
        {
            HttpResponseMessage response = await _clientRef.PostAsync("/api/data/" + documentId, new System.Net.Http.StreamContent(inputStream));
            response.EnsureSuccessStatusCode();
        }


        public async Task<PropertyGroup[]> GetAllPropertiesForObject(string objectId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/objects/" + objectId + "/properties");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<PropertyGroup[]>();
        }


        public async Task<PropertyGroup> GetPropertyGroupForObject(string objectId, string groupName)
        { 
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/objects/" + objectId + "/properties/groups/" + groupName);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<PropertyGroup>();
        }


        public async Task<Project[]> GetProjects()
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/projects");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<Project[]>();
        }
    }
}

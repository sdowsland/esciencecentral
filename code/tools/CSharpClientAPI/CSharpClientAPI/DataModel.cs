﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;


namespace com.connexience.server.model
{
    /// <summary>
    /// This attribute is meant to be equivalent to:
    /// <code>org.codehaus.jackson.annotate.JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")</code>
    /// to allow type information to be (de)serialized when communicating with an e-SC3 server.
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Struct, Inherited = true)]
    public class DecorateJsonWithClassAttribute : Attribute
    {
        public DecorateJsonWithClassAttribute()
        { }
    }


    [DecorateJsonWithClass]
    public class ServerObject
    {
        /** Database ID */
        public string id;

        /** Id of the object that contains this one if any */
        public string containerId;

        /** Organisation that this object belongs to */
        public string organisationId;

        /** Object name */
        public string name;

        /** Organisation description */
        public string description;

        /** ID of the user that created the object */
        public string creatorId;

        /** Date the object was created; the current time as UTC milliseconds from the epoch  */
        public long timeInMillis;

        /** ??? */
        public string objectType;

        /** The project which created this object */
        public string projectId;

        public override bool Equals(Object o)
        {
            if (o == null || GetType() != o.GetType())
                return false;

            ServerObject that = (ServerObject)o;
            return
                this.id == that.id &&
                this.containerId == that.containerId &&
                this.organisationId == that.organisationId &&
                this.name == that.name &&
                this.description == that.description &&
                this.creatorId == that.creatorId &&
                this.timeInMillis == that.timeInMillis &&
                this.objectType == that.objectType &&
                this.projectId == that.projectId;
        }

        public override int GetHashCode()
        {
            return 
                id.GetHashCode() ^ 
                containerId.GetHashCode() ^ 
                organisationId.GetHashCode() ^ 
                name.GetHashCode() ^ 
                description.GetHashCode() ^ 
                creatorId.GetHashCode() ^ 
                timeInMillis.GetHashCode() ^ 
                objectType.GetHashCode() ^ 
                projectId.GetHashCode();
        }
    }

    namespace document
    {
        public class DocumentRecord : ServerObject
        {
            /** ID of the document type for this record */
            public string documentTypeId;

            /** Does this record support versioning */
            public bool versioned;

            /** Set the maximum number of previous versions */
            public int maxVersions;

            /** Limit the maximum number of previous versions */
            public bool limitVersions;

            /** Current version number */
            public int currentVersionNumber;

            /** Current document size **/
            public long currentVersionSize;

            /** Current archive status **/
            public string currentArchiveStatus;

            public override bool Equals(Object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                DocumentRecord that = (DocumentRecord)o;
                return 
                    this.documentTypeId == that.documentTypeId &&
                    this.versioned == that.versioned &&
                    this.maxVersions == that.maxVersions &&
                    this.limitVersions == that.limitVersions &&
                    this.documentTypeId == that.documentTypeId &&
                    this.currentArchiveStatus == that.currentArchiveStatus &&
                    this.currentVersionNumber == that.currentVersionNumber &&
                    this.currentVersionSize == that.currentVersionSize;
            }

            public override int GetHashCode()
            {
                return
                    base.GetHashCode() ^
                    documentTypeId.GetHashCode() ^
                    versioned.GetHashCode() ^
                    maxVersions ^
                    limitVersions.GetHashCode() ^
                    currentVersionNumber ^
                    currentVersionSize.GetHashCode() ^
                    currentArchiveStatus.GetHashCode();
            }
        }

        public class DocumentVersion
        {
            /** Database ID of the version. This the same as the ID of the stored file */
            public string id;

            /** ID of the document record this file belongs to */
            public string documentRecordId;

            /** Version number of the file */
            public int versionNumber;

            /** ID of the User that created the file */
            public string userId;

            /** Timestamp for the file */
            public long timestamp;

            /** Size of data in bytes */
            public long size;

            /** Comments entered during upload */
            public string comments;

            public override bool Equals(Object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                DocumentVersion that = (DocumentVersion)o;
                return
                    this.id == that.id &&
                    this.documentRecordId == that.documentRecordId &&
                    this.versionNumber == that.versionNumber &&
                    this.userId == that.userId &&
                    this.timestamp == that.timestamp &&
                    this.size == that.size &&
                    this.comments == that.comments;
            }

            public override int GetHashCode()
            {
                return
                    id.GetHashCode() ^
                    documentRecordId.GetHashCode() ^
                    versionNumber ^
                    userId.GetHashCode() ^
                    timestamp.GetHashCode() ^
                    size.GetHashCode() ^
                    comments.GetHashCode();
            }
        }
    }

    namespace workflow
    {
        public class DynamicWorkflowLibrary : document.DocumentRecord
        {
            public string libraryName;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                DynamicWorkflowLibrary that = (DynamicWorkflowLibrary)o;
                return this.libraryName == that.libraryName;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode() ^ libraryName.GetHashCode();
            }
        }


        public class DynamicWorkflowService : document.DocumentRecord
        {
            /** Service category */
            public string category;

            /** ID of the block project file that created this block if there is one */
            public string projectFileId;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                DynamicWorkflowService that = (DynamicWorkflowService)o;
                return this.category == that.category && this.projectFileId == that.projectFileId;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode() ^ category.GetHashCode() ^ projectFileId.GetHashCode();
            }
        }


        public class WorkflowDocument : document.DocumentRecord
        {
            /** Should invocations store intermediate data */
            public bool intermedateDataStored;

            /** Does this workflow support an external data source */
            public bool externalDataSupported;

            /** Name of the block to send the external data to */
            public string externalDataBlockName;

            /** Workflow engine type to execute this workflow on */
            public string type;

            /** Should the invocation be deleted if successful */
            public bool deletedOnSuccess;

            /** Only upload output data for failed blocks */
            public bool onlyFailedOutputsUploaded;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                WorkflowDocument that = (WorkflowDocument)o;
                return
                    this.intermedateDataStored == that.intermedateDataStored &&
                    this.externalDataSupported == that.externalDataSupported &&
                    this.externalDataBlockName == that.externalDataBlockName &&
                    this.type == that.type &&
                    this.deletedOnSuccess == that.deletedOnSuccess &&
                    this.onlyFailedOutputsUploaded == that.onlyFailedOutputsUploaded;
            }

            public override int GetHashCode()
            {
                return
                    base.GetHashCode() ^
                    intermedateDataStored.GetHashCode() ^
                    externalDataSupported.GetHashCode() ^
                    externalDataBlockName.GetHashCode() ^
                    type.GetHashCode() ^
                    deletedOnSuccess.GetHashCode() ^
                    onlyFailedOutputsUploaded.GetHashCode();
            }
        }


        public class WorkflowInvocationFolder : folder.Folder
        {
            /** Invocation ID. This links into the workflow database */
            public string invocationId;

            /** ID of the workflow document associated with this invocation */
            public string workflowId;

            /** Version ID of the workflow document associated with this invocation */
            public string versionId;

            /** Timestamp of the invocation */
            public long invocationDate;

            /** Time this invocation was started */
            public long? queuedTime;

            /** Time this invocation was taken from the queue */
            public long? dequeuedTime;

            /** Time this invocation was started */
            public long? executionStartTime;

            /** Time this invocation was completed */
            public long? executionEndTime;

            /** Status of execution */
            public int invocationStatus;

            /** ID of the block currently being executed */
            public string currentBlockId;

            /** Is the streaming progress known for the current block */
            public bool streamingProgressKnown;

            /** Total number of bytes to stream */
            public long totalBytesToStream;

            /** Total number of bytes streamed */
            public long bytesStreamed;

            /** ID of the engine running the workflow */
            public string engineId;

            /** Status message */
            public string message;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                WorkflowInvocationFolder that = (WorkflowInvocationFolder)o;
                return
                    this.invocationId == that.invocationId &&
                    this.versionId == that.versionId &&
                    this.streamingProgressKnown == that.streamingProgressKnown &&
                    this.invocationDate == that.invocationDate &&
                    this.queuedTime == that.queuedTime &&
                    this.dequeuedTime == that.dequeuedTime &&
                    this.executionStartTime == that.executionStartTime &&
                    this.executionEndTime == that.executionEndTime &&
                    this.invocationStatus == that.invocationStatus &&
                    this.currentBlockId == that.currentBlockId &&
                    this.totalBytesToStream == that.totalBytesToStream &&
                    this.bytesStreamed == that.bytesStreamed &&
                    this.workflowId == that.workflowId &&
                    this.engineId == that.engineId &&
                    this.message == that.message;
            }

            public override int GetHashCode()
            {
                return
                    base.GetHashCode() ^
                    invocationId.GetHashCode() ^
                    workflowId.GetHashCode() ^
                    versionId.GetHashCode() ^
                    invocationDate.GetHashCode() ^
                    queuedTime.GetHashCode() ^
                    dequeuedTime.GetHashCode() ^
                    executionStartTime.GetHashCode() ^
                    executionEndTime.GetHashCode() ^
                    invocationStatus ^
                    currentBlockId.GetHashCode() ^
                    streamingProgressKnown.GetHashCode() ^
                    totalBytesToStream.GetHashCode() ^
                    bytesStreamed.GetHashCode() ^
                    engineId.GetHashCode() ^
                    message.GetHashCode();
            }
        }


        public class WorkflowParameter
        {
            public string blockName;
            public string name;
            public string value;

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                WorkflowParameter that = (WorkflowParameter)o;
                return
                    this.blockName == that.blockName &&
                    this.name == that.name &&
                    this.value == that.value;
            }

            public override int GetHashCode()
            {
                return blockName.GetHashCode() ^ name.GetHashCode() ^ value.GetHashCode();
            }
        }


        [DecorateJsonWithClass]
        public class WorkflowParameterList
        {
            public List<WorkflowParameter> parameters;

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                WorkflowParameterList that = (WorkflowParameterList)o;
                return this.parameters.SequenceEqual(that.parameters);
            }

            public override int GetHashCode()
            {
                int hash = 0;
                foreach (var p in parameters)
                {
                    hash ^= p.GetHashCode();
                }
                return hash;
            }
        }
    }

    namespace folder
    {
        public class Folder : ServerObject
        {
            /** Is this folder publicly visible bypassing the security checks */
            public bool publicVisible;

            /** Short name of folder */
            public string shortName;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                Folder that = (Folder)o;
                return this.publicVisible == that.publicVisible && this.shortName == that.shortName;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode() ^ publicVisible.GetHashCode() ^ shortName.GetHashCode();
            }

        }
    }

    namespace metadata
    {
        [DecorateJsonWithClass]
        public abstract class MetadataItem
        {
            /** Metadata database ID */
            public long id;

            /** ID of the object that this metadata refers to */
            public string objectId;

            /** Name of this metadata item */
            public string name;

            /** ID of the user that created the metadata */
            public string userId;

            /** Category name of the metadata */
            public string category;

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                MetadataItem that = (MetadataItem)o;
                return
                    this.id == that.id &&
                    this.objectId == that.objectId &&
                    this.name == that.name &&
                    this.userId == that.userId &&
                    this.category == that.category;
            }

            public override int GetHashCode()
            {
                return
                    id.GetHashCode() ^
                    objectId.GetHashCode() ^
                    name.GetHashCode() ^
                    userId.GetHashCode() ^
                    category.GetHashCode();
            }
        }

        namespace types
        {
            public class TextMetadata : MetadataItem
            {
                public string textValue;

                public override bool Equals(object o)
                {
                    if (!base.Equals(o))
                        return false;
                    // base.Equals == true ensures that o is typeof(this) so I can safely cast
                    TextMetadata that = (TextMetadata)o;
                    return this.textValue == that.textValue;
                }

                public override int GetHashCode()
                {
                    return base.GetHashCode() ^ textValue.GetHashCode();
                }
            }

            public class BooleanMetadata : MetadataItem
            {
                public bool booleanValue;

                public override bool Equals(object o)
                {
                    if (!base.Equals(o))
                        return false;
                    // base.Equals == true ensures that o is typeof(this) so I can safely cast
                    BooleanMetadata that = (BooleanMetadata)o;
                    return this.booleanValue == that.booleanValue;
                }

                public override int GetHashCode()
                {
                    return base.GetHashCode() ^ booleanValue.GetHashCode();
                }
            }

            public class NumericalMetadata : MetadataItem
            {
                public double doubleValue;

                public override bool Equals(object o)
                {
                    if (!base.Equals(o))
                        return false;
                    // base.Equals == true ensures that o is typeof(this) so I can safely cast
                    NumericalMetadata that = (NumericalMetadata)o;
                    return this.doubleValue == that.doubleValue;
                }

                public override int GetHashCode()
                {
                    return base.GetHashCode() ^ doubleValue.GetHashCode();
                }

            }

            public class DateMetadata : MetadataItem
            {
                public long dateValue;

                public override bool Equals(object o)
                {
                    if (!base.Equals(o))
                        return false;
                    // base.Equals == true ensures that o is typeof(this) so I can safely cast
                    DateMetadata that = (DateMetadata)o;
                    return this.dateValue == that.dateValue;
                }

                public override int GetHashCode()
                {
                    return base.GetHashCode() ^ dateValue.GetHashCode();
                }
            }
        }

        [DecorateJsonWithClass]
        public class MetadataQueryItem
        {
            public string name = "";

            public bool caseSensitiveName;

            public int positionCounter;

            public bool categoryIncluded;

            public string categorySearchText = "";

            public bool caseSensitiveCategoryName;

            public string label = "Search Item";

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                MetadataQueryItem that = (MetadataQueryItem)o;
                return
                    this.name == that.name &&
                    this.caseSensitiveName == that.caseSensitiveName &&
                    this.positionCounter == that.positionCounter &&
                    this.categoryIncluded == that.categoryIncluded &&
                    this.categorySearchText == that.categorySearchText &&
                    this.caseSensitiveCategoryName == that.caseSensitiveCategoryName &&
                    this.label == that.label;
            }

            public override int GetHashCode()
            {
                return
                    name.GetHashCode() ^
                    caseSensitiveName.GetHashCode() ^
                    positionCounter ^
                    categoryIncluded.GetHashCode() ^
                    categorySearchText.GetHashCode() ^
                    caseSensitiveCategoryName.GetHashCode() ^
                    label.GetHashCode();
            }
        }


        [DecorateJsonWithClass]
        public class MetadataQuery
        {
            public List<MetadataQueryItem> items = new List<MetadataQueryItem>();

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                MetadataQuery that = (MetadataQuery)o;
                return this.items.SequenceEqual(that.items);
            }

            public override int GetHashCode()
            {
                int hash = 0;
                foreach (var i in items)
                {
                    hash ^= i.GetHashCode();
                }
                return hash;
            }
        }

        [DecorateJsonWithClass]
        public class MetadataSynonym
        {
            public long id;

            public string category;

            public string name;

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                MetadataSynonym that = (MetadataSynonym)o;
                return
                    this.id == that.id &&
                    this.name == that.name &&
                    this.category == that.category;
            }

            public override int GetHashCode()
            {
                return id.GetHashCode() ^ name.GetHashCode() ^ category.GetHashCode();
            }

        }
    }

    namespace security
    {
        public class Group : ServerObject
        {
            /// <summary>
            /// Indicates whether this group is a special group or not.
            /// The default users and admin groups are protected and will not
            /// show in lists on the website
            /// </summary>
            public bool protectedGroup;

            /// <summary>
            /// ID of the profile for this group
            /// </summary>
            public string profileId;

            /// <summary>
            /// Should the group admin (creator) have to explicitly allow people to join the group?
            /// </summary>
            public bool adminApproveJoin = true;

            /// <summary>
            /// Can the members of this group be listed by non-members?
            /// </summary>
            public bool nonMembersList = true;

            /// <summary>
            /// Folder for the DocumentRecordLinks for this group
            /// </summary>
            public string dataFolder;

            /// <summary>
            /// Folder for any events stored in this group
            /// </summary>
            public string eventsFolder;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                Group that = (Group)o;
                return
                    this.protectedGroup == that.protectedGroup &&
                    this.profileId == that.profileId &&
                    this.adminApproveJoin == that.adminApproveJoin &&
                    this.nonMembersList == that.nonMembersList &&
                    this.dataFolder == that.dataFolder &&
                    this.eventsFolder == that.eventsFolder;
            }

            public override int GetHashCode()
            {
                return
                    base.GetHashCode() ^
                    protectedGroup.GetHashCode() ^
                    profileId.GetHashCode() ^
                    adminApproveJoin.GetHashCode() ^
                    nonMembersList.GetHashCode() ^
                    dataFolder.GetHashCode() ^
                    eventsFolder.GetHashCode();
            }
        }
    }

    namespace social
    {
        public class Project : security.Group
        {
            public string workflowFolderId;

            public override bool Equals(object o)
            {
                if (!base.Equals(o))
                    return false;
                // base.Equals == true ensures that o is typeof(this) so I can safely cast
                Project that = (Project)o;
                return this.workflowFolderId == that.workflowFolderId;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode() ^ workflowFolderId.GetHashCode();
            }
        }
    }

    namespace properties
    {
        [DecorateJsonWithClass]
        public class PropertyItem
        {
            public long id;

            public long groupId;

            public string name;

            public string value;

            public string type = "String";

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                PropertyItem that = (PropertyItem)o;
                return
                    this.id == that.id &&
                    this.groupId == that.groupId &&
                    this.name == that.name &&
                    this.value == that.value &&
                    this.type == that.type;
            }

            public override int GetHashCode()
            {
                return
                    id.GetHashCode() ^
                    groupId.GetHashCode() ^
                    name.GetHashCode() ^
                    value.GetHashCode() ^
                    type.GetHashCode();
            }
        }


        [DecorateJsonWithClass]
        public class PropertyGroup
        {
            public long id;

            /// <summary>
            /// Is this property group assigned to a server object
            /// </summary>
            public bool objectProperty;

            /// <summary>
            /// ID of the object that owns this property if there is one
            /// </summary>
            public string objectId;

            public string name;

            public string description;

            public string organisationId;

            /** Hashtable of properties for this group */
            public Dictionary<string, PropertyItem> properties = new Dictionary<string, PropertyItem>();

            public override bool Equals(object o)
            {
                if (o == null || GetType() != o.GetType())
                    return false;

                PropertyGroup that = (PropertyGroup)o;
                return
                    this.id == that.id &&
                    this.objectProperty == that.objectProperty &&
                    this.objectId == that.objectId &&
                    this.name == that.name &&
                    this.description == that.description &&
                    this.organisationId == that.organisationId &&
                    Equals(this, that);
            }

            public override int GetHashCode()
            {
                return
                    id.GetHashCode() ^
                    objectProperty.GetHashCode() ^
                    objectId.GetHashCode() ^
                    name.GetHashCode() ^
                    description.GetHashCode() ^
                    organisationId.GetHashCode() ^
                    GetHashCode<string, PropertyItem>(properties);
            }

            /// <summary>
            /// Thanks to LukeH
            /// http://stackoverflow.com/questions/3804367/testing-for-equality-between-dictionaries-in-c-sharp
            /// </summary>
            /// <typeparam name="TKey"></typeparam>
            /// <typeparam name="TValue"></typeparam>
            /// <param name="dict1"></param>
            /// <param name="dict2"></param>
            /// <returns></returns>
            public static bool Equals<TKey, TValue>(Dictionary<TKey, TValue> dict1, Dictionary<TKey, TValue> dict2)
            {
                if (dict1 == dict2)
                    return true;
                if ((dict1 == null) || (dict2 == null))
                    return false;
                if (dict1.Count != dict2.Count)
                    return false;

                var comparer = EqualityComparer<TValue>.Default;

                foreach (KeyValuePair<TKey, TValue> kvp in dict1)
                {
                    TValue value2;
                    if (!dict2.TryGetValue(kvp.Key, out value2))
                        return false;
                    if (!comparer.Equals(kvp.Value, value2))
                        return false;
                }

                return true;
            }

            public static int GetHashCode<TKey, TValue>(Dictionary<TKey, TValue> dict)
            {
                int hash = 0;
                foreach (KeyValuePair<TKey, TValue> kvp in dict)
                {
                    hash ^= kvp.Key.GetHashCode() ^ kvp.Value.GetHashCode();
                }
                return hash;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;
using Newtonsoft.Json.Converters;


namespace e_SC
{
    public static class JsonHelpers
    {
        public static Task<T> ReadContentAsAsync<T>(this HttpContent content)
        {
            return content.ReadAsStringAsync().ContinueWith<T>(
                readerTask =>
                {
                    var dObj = JsonConvert.DeserializeObject(readerTask.Result);
                    return (T)ReadContent(dObj, typeof(T));
                });
        }

        
        public static Task<HttpResponseMessage> PostContentAsJsonAsync<T>(this HttpClient client, string requestUri, T value)
        {
            JToken obj = WriteContent(value);
            return client.PostAsync(requestUri, new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(obj), System.Text.Encoding.UTF8, "application/json"));
        }


        public static Task<HttpResponseMessage> PutContentAsJsonAsync<T>(this HttpClient client, string requestUri, T value)
        {
            JToken obj = WriteContent(value);
            return client.PutAsync(requestUri, new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(obj), System.Text.Encoding.UTF8, "application/json"));
        }


        #region Implementation area 

        private static object ReadObject(JObject obj, Type objectType)
        {
            JToken className = obj["@class"];
            Type objType = (className == null ? objectType : Type.GetType(className.ToString()));
            if (objType == null || !objType.Equals(objectType) && !objType.IsSubclassOf(objectType))
            {
                throw new Exception("Cannot deserialize JSON object to type: " + objectType);
            }
            return obj.ToObject(objType);
        }

        private static object ReadArrayAsArray(JArray array, Type elementType)
        {
            Array outArr = Array.CreateInstance(elementType, array.Count);

            int i = 0;
            foreach (var obj in array)
            {
                outArr.SetValue(ReadContent(obj, elementType), i++);
            }

            return outArr;
        }

        private static object ReadArrayAsCollection(JArray array, Type elementType)
        {
            IList list = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementType));

            foreach (var obj in array)
            {
                list.Add(ReadContent(obj, elementType));
            }

            return list;
        }

        private static object ReadContent(object obj, Type objectType)
        {
            if (obj is JArray)
            {
                if (objectType.IsArray)
                {
                    return ReadArrayAsArray(obj as JArray, objectType.GetElementType());
                }
                else if (typeof(IEnumerable).IsAssignableFrom(objectType))
                {
                    Type[] elementTypes = objectType.GetGenericArguments();
                    if (elementTypes.Length == 0)
                        return obj;
                    else if (elementTypes.Length == 1)
                        return ReadArrayAsCollection(obj as JArray, objectType.GetGenericArguments()[0]);
                }

                throw new JsonSerializationException("Cannot deserialize JSON array to type: " + objectType);
            }
            else if (obj is JObject)
            {
                return ReadObject(obj as JObject, objectType);
            }
            else
            {
                throw new JsonSerializationException("Cannot deserialize JSON object: Unexpected type: " + objectType);
            }
        }


        private static JToken WriteContent(object value)
        {
            if (value is ICollection)
            {
                JArray container = new JArray();
                foreach (var elem in (value as ICollection))
                {
                    container.Add(WriteContent(elem));
                }
                return container;
            }
            else if (Attribute.IsDefined(value.GetType(), typeof(com.connexience.server.model.DecorateJsonWithClassAttribute)))
            {
                // ... add @class property if the type is decorated appropriately.
                JObject container = JObject.FromObject(value);
                container.AddFirst(new JProperty("@class", value.GetType().FullName));
                return container;
            }
            else
            {
                return JToken.FromObject(value);
            }
        }

        #endregion Implementation area
    }
}

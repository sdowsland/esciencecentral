﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using com.connexience.server.model;
using com.connexience.server.model.document;
using com.connexience.server.model.folder;
using com.connexience.server.model.metadata;
using com.connexience.server.model.workflow;

using Newtonsoft.Json;


namespace e_SC
{
    public class XStorageAPI
    {
        public XStorageAPI(string serverAddressURI)
        {
            _cookieContainer = new CookieContainer();
            _clientRef = new HttpClient(
                new HttpClientHandler()
                {
                    CookieContainer = _cookieContainer,
                });
            _clientRef.BaseAddress = new Uri(serverAddressURI);
            _clientRef.DefaultRequestHeaders.ExpectContinue = false;
        }

        public XStorageAPI(string serverAddressURI, string userName, string userPassword) : this(serverAddressURI)
        {
            _userName = userName;
            _userPassword = userPassword;
        }


        #region Public API

        public Cookie SessionCookie
        {
            get
            {
                var cookies = _cookieContainer.GetCookies(new Uri(_clientRef.BaseAddress, "/beta"));
                if (cookies["JSESSIONID"] == null)
                    return null;
                cookies["JSESSIONID"].Path = "";
                return cookies["JSESSIONID"];
            }
        }


        public string UploadBlock(string documentId, string versionId, int blockNo, Stream blockData)
        {
            return UploadBlockAsync(documentId, versionId, blockNo, blockData).Result;
        }


        public async Task<string> UploadBlockAsync(string documentId, string versionId, int blockNo, Stream blockData)
        {
            await VerifyAccessAsync();

            StreamContent content = new StreamContent(blockData);
            content.Headers.Add("Content-Type", "application/octet-stream");

            HttpResponseMessage response = await _clientRef.PutAsync(
                string.Format("/beta/rest/xstorage/documents/{0}/{1}/{2}", documentId, versionId, blockNo), content);
            // FIXME: Correct if the response is session timeout
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }


        /// <summary>
        /// This is a convenience method that commits a block list in range: [0-totalBlockNo)
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="versionId"></param>
        /// <param name="totalBlockNo"></param>
        /// <returns></returns>
        public DocumentVersion CommitBlockList(string documentId, string versionId, int totalBlockNo)
        {
            return CommitBlockListAsync(documentId, versionId, totalBlockNo).Result;
        }


        public async Task<DocumentVersion> CommitBlockListAsync(string documentId, string versionId, int totalBlockNo)
        {
            await VerifyAccessAsync();

            var blockList = new List<string>();
            for (int i = 0; i < totalBlockNo; i++)
                blockList.Add(i.ToString());

            HttpResponseMessage response = await _clientRef.PutContentAsJsonAsync(string.Format("/beta/rest/xstorage/documents/{0}/{1}/blocklist", documentId, versionId), blockList);
            // FIXME: Correct if the response is session timeout
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<DocumentVersion>();
        }

        #endregion

        #region Implementation Area

        HttpClient _clientRef;
        CookieContainer _cookieContainer;
        string _userName;
        string _userPassword;
        string _sessionId;


        private async Task VerifyAccessAsync()
        {
            if (_userName == null || _userPassword == null || "" == _userName || "" == _userPassword)
                // Continue using the public access
                return;
            // Otherwise try logging in...
            if (_sessionId == null)
                _sessionId = await AuthenticateAsync();
            // ensure it completed successfully...
            if (_sessionId == null)
                throw new ApplicationException("Login failed");
        }


        private async Task<string> AuthenticateAsync()
        {
            HttpResponseMessage response = await _clientRef.PostAsync("/beta/rest/account/login",
                new FormUrlEncodedContent(new KeyValuePair<string, string>[] { 
                                new KeyValuePair<string, string>("username", _userName),
                                new KeyValuePair<string, string>("password", _userPassword) }));

            response.EnsureSuccessStatusCode();

            string resultJson = await response.Content.ReadAsStringAsync();
            dynamic resultObj = JsonConvert.DeserializeObject(resultJson);
            return resultObj.sessionId;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using com.connexience.server.model;
using com.connexience.server.model.document;
using com.connexience.server.model.folder;
using com.connexience.server.model.metadata;
using com.connexience.server.model.workflow;

namespace e_SC
{
    /// <summary>
    /// An interface class for the IWorkflowAPI e-Science Central interface.
    /// </summary>
    public class WorkflowAPI
    {
        public WorkflowAPI(string serverAddressURI)
        {
            _clientRef = new HttpClient();
            _clientRef.BaseAddress = new Uri(serverAddressURI);
            _clientRef.DefaultRequestHeaders.ExpectContinue = false;
        }


        public WorkflowAPI(string serverAddressURI, string userName, string userPassword)
            : this(serverAddressURI, new NetworkCredential(userName, userPassword))
        { }


        public WorkflowAPI(string serverAddressURI, System.Net.ICredentials credentials)
        {
            _clientRef = new HttpClient(
                new HttpClientHandler()
                {
                    Credentials = credentials,
                });
            _clientRef.BaseAddress = new Uri(serverAddressURI);
            _clientRef.DefaultRequestHeaders.ExpectContinue = false;
        }


        #region Public API

        public async Task<WorkflowInvocationFolder> GetWorkflowStatusAsync(string invocationId)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/workflow/invocations/" + invocationId);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<WorkflowInvocationFolder>();
        }


        public async Task<WorkflowInvocationFolder> CallWorkflowAsync(string name, params Tuple<string, string>[] paramList)
        {
            return await CallWorkflowAsync(name, _TuplesToParam(paramList));
        }

        public async Task<WorkflowInvocationFolder> CallWorkflowAsync(string name, params Tuple<string, string, string>[] paramList)
        {
            return await CallWorkflowAsync(name, _TuplesToParam(paramList));
        }

        public async Task<WorkflowInvocationFolder> CallWorkflowAsync(string name, Dictionary<string, string> paramList)
        {
            return await CallWorkflowAsync(name, _DictToParam(paramList));
        }

        public async Task<WorkflowInvocationFolder> CallWorkflowAsync(string name, WorkflowParameterList paramList)
        {
            HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/workflow/list/all");
            response.EnsureSuccessStatusCode();
            WorkflowDocument[] wDocs = await response.Content.ReadContentAsAsync<WorkflowDocument[]>();
            WorkflowDocument wDoc = wDocs.FirstOrDefault(w => w.name.Equals(name));

            if (wDoc == null)
                return null;

            response = await _clientRef.PostContentAsJsonAsync<WorkflowParameterList>("/api/rest/workflow/" + wDoc.id + "/execute/parameters", paramList);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadContentAsAsync<WorkflowInvocationFolder>();
        }

        #endregion Public API

        #region Implementation

        private HttpClient _clientRef;


        private static WorkflowParameterList _TuplesToParam(params Tuple<string, string, string>[] paramList)
        {
            var list = new WorkflowParameterList();
            list.parameters = new List<WorkflowParameter>();

            foreach (Tuple<string, string, string> entry in paramList)
            {
                list.parameters.Add(new WorkflowParameter()
                {
                    blockName = entry.Item1,
                    name = entry.Item2,
                    value = entry.Item3
                });
            }

            return list;
        }


        private static WorkflowParameterList _TuplesToParam(params Tuple<string, string>[] paramList)
        {
            var list = new WorkflowParameterList();
            list.parameters = new List<WorkflowParameter>();

            foreach (Tuple<string, string> entry in paramList)
            {
                var p = new WorkflowParameter();
                int dot = entry.Item1.IndexOf('.');
                if (dot < 1)
                    throw new ArgumentException("Invalid item1: " + entry.Item1 + "; missing block name");
                if (dot > entry.Item1.Length - 2)
                    throw new ArgumentException("Invalid item1: " + entry.Item1 + "; missing property name");
                p.blockName = entry.Item1.Substring(0, dot);
                p.name = entry.Item1.Substring(dot + 1);
                p.value = entry.Item2;

                list.parameters.Add(p);
            }

            return list;
        }


        private static WorkflowParameterList _DictToParam(Dictionary<string, string> dict)
        {
            var list = new WorkflowParameterList();
            list.parameters = new List<WorkflowParameter>();

            foreach (KeyValuePair<string, string> entry in dict)
            {
                var p = new WorkflowParameter();
                int dot = entry.Key.IndexOf('.');
                if (dot < 1)
                    throw new ArgumentException("Invalid key: " + entry.Key + "; missing block name");
                if (dot > entry.Key.Length - 2)
                    throw new ArgumentException("Invalid key: " + entry.Key + "; missing property name");
                p.blockName = entry.Key.Substring(0, dot);
                p.name = entry.Key.Substring(dot + 1);
                p.value = entry.Value;

                list.parameters.Add(p);
            }

            return list;
        }

        #endregion Implementation
    }
}

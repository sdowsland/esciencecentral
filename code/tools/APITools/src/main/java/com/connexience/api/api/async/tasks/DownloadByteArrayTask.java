/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.api.api.async.tasks;

import com.connexience.api.api.async.ApiTask;
import com.connexience.api.model.EscDocument;
import java.io.ByteArrayOutputStream;

/**
 *
 * @author hugo
 */
public class DownloadByteArrayTask extends ApiTask {
    byte[] fileData = new byte[0];
    private EscDocument doc;
    private String name;
    private String folderId;

    public DownloadByteArrayTask(EscDocument doc) {
        this.doc = doc;
    }
    
    public DownloadByteArrayTask(String folderId, String name){
        this.doc = null;
        this.folderId = folderId;
        this.name = name;
    }
    
    @Override
    public void run() {
        try {
            if(doc==null){
                EscDocument[] docs = getManager().getStorageClient().folderDocuments(folderId);
                for(EscDocument d : docs){
                    if(d.getName().equals(name)){
                        doc = d;
                    }
                }
            }
                
            if(doc!=null){
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                getManager().getStorageClient().download(doc, stream);
                stream.flush();
                stream.close();
                fileData = stream.toByteArray();
                notifyCompleted();
            } else {
                notifyFailed(new Exception("Document not found"));
            }
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public byte[] getFileData() {
        return fileData;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common;
import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.model.EscFolder;
import com.connexience.api.StorageClient;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskManager;
import com.connexience.api.api.async.tasks.GetChildFoldersTask;
import com.connexience.api.api.async.tasks.GetHomeFolderTask;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Tree model for a set of folder
 * @author hugo
 */
public class FolderTreeModel implements TreeModel {
    private FolderNode topFolder;
    private ApiTaskManager manager;
    private ArrayList<TreeModelListener> listeners = new ArrayList<>();
    private ArrayList<FolderTreeModelListener> modelListeners = new ArrayList<>();

    public FolderTreeModel(ApiTaskManager manager, EscFolder topFolder) {
        this.manager = manager;
        this.topFolder = new FolderNode(topFolder);
        populateTopFolder();
    }

    public FolderTreeModel(ApiTaskManager manager) {
        this.topFolder = null;
        this.manager = manager;
        populateTopFolder();
    }
    
    public FolderTreeModel.FolderNode findNodeForFolder(EscFolder folder){
        if(topFolder!=null){
            if(topFolder.getFolder()==folder){
                return topFolder;
            } else {
                return topFolder.locateNodeForFolder(folder);
            }
        } else {
            return null;
        }
    }
    
    public FolderTreeModel.FolderNode findNodeForFolderId(String folderId){
        if(topFolder!=null){
            if(topFolder.getFolder().getId()==folderId){
                return topFolder;
            } else {
                return topFolder.locateNodeForFolderId(folderId);
            }
        } else {
            return null;
        }
    }
    public void populateTopFolder(){
        if(topFolder!=null){
            notifyTreeChanged();
            notifyRootNodeFetched(topFolder);
            topFolder.populate();
        } else {
            GetHomeFolderTask task = new GetHomeFolderTask();
            manager.performTask(task, new ApiTaskAdapter(){
                @Override
                public void taskCompleted(ApiTask task) {
                    topFolder = new FolderNode(((GetHomeFolderTask)task).getHomeFolder());
                    notifyTreeChanged();
                    notifyRootNodeFetched(topFolder);
                    topFolder.populate();
                }
            });
        }
    }
    
    public boolean isRoot(FolderTreeModel.FolderNode node){
        return node==topFolder;
    }
    
    @Override
    public Object getRoot() {
        if(topFolder!=null){
            return topFolder;
        } else {
            return "Fetching...";
        }
    }
    
    public void setTopFolder(EscFolder folder){
        
    }

    @Override
    public Object getChild(Object parent, int index) {
        if(parent instanceof FolderNode){
            return ((FolderNode)parent).getChild(index);
        } else {
            return null;
        }
    }

    @Override
    public int getChildCount(Object parent) {
        if(parent instanceof FolderNode){
            return ((FolderNode)parent).getChildCount();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isLeaf(Object node) {
        if(node instanceof FolderNode){
            return ((FolderNode)node).isLeaf();
        } else {
            return true;
        }
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if(parent instanceof FolderNode){
            return ((FolderNode)parent).getIndexOfChild(child);
        } else {
            return -1;
        }
    }

    public void addFolderTreeModelListener(FolderTreeModelListener l){
        modelListeners.add(l);
    }
    
    public void removeFolderTreeModelListener(FolderTreeModelListener l){
        modelListeners.remove(l);
    }
    
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        listeners.remove(l);
    }
    
    private void notifyNodeChanged(FolderNode node){
        ArrayList<FolderNode> pathNodes = new ArrayList<>();
        node.constructPathToHere(pathNodes);
        TreePath path;
        if(pathNodes.size()==1){
            path = new TreePath(node);
            TreeModelEvent evt = new TreeModelEvent(node, path);
            for(TreeModelListener l : listeners){
                l.treeStructureChanged(evt);
            }
        
        } else {
            path = new TreePath(pathNodes.toArray());           
            TreeModelEvent evt = new TreeModelEvent(node, path);
            for(TreeModelListener l : listeners){
                l.treeStructureChanged(evt);
            }            
        }


    }
    
    private void notifyRootNodeFetched(FolderNode node){
        for(FolderTreeModelListener l : modelListeners){
            l.rootNodeFetched(node);
        }
    }
    
    private void notifyTreeChanged(){
        TreePath path = new TreePath(topFolder);
        if(path.getLastPathComponent()!=null){
            for(TreeModelListener l : listeners){
                l.treeStructureChanged(new TreeModelEvent(this, path));
            }
        }
    }
    
    public class FolderNode {
        EscFolder folder;
        FolderNode parent;
        
        ArrayList<FolderNode> children = new ArrayList<>();
        private boolean populated = false;

        public FolderNode(EscFolder folder){
            this.folder = folder;
            this.parent = null;
        }
        
        public FolderNode(EscFolder folder, FolderNode parent) {
            this.folder = folder;
            this.parent = parent;
        }

        public EscFolder getFolder() {
            return folder;
        }

        
        public void constructPathToHere(ArrayList<FolderNode> path){
            path.add(0, this);
            if(parent!=null){
                parent.constructPathToHere(path);
            }
        }

        public boolean isPopulated() {
            return populated;
        }

        public void populate(){
            GetChildFoldersTask task = new GetChildFoldersTask(folder);
            final FolderNode node = this;
            manager.performTask(task, new ApiTaskAdapter() {

                @Override
                public void taskCompleted(ApiTask task) {
                    ArrayList<EscFolder> childFolders = ((GetChildFoldersTask)task).getChildrenArrayList();
                    children.clear();
                    for(EscFolder f : childFolders){
                        children.add(new FolderNode(f, node));
                    }
                    populated = true;
                    notifyNodeChanged(node);
                    //notifyTreeChanged();;
                }

                @Override
                public void taskFailed(ApiTask task, Exception cause) {
                    populated = false;
                    notifyNodeChanged(node);
                }

                @Override
                public void taskProgress(int percentage) {
                    
                }
            });
        }
        
        public int getChildCount(){
            if(populated){
                return children.size();
            } else {
                return 0;
            }
        }
        
        public Object getChild(int index){
            if(populated){
                return children.get(index);
            } else {
                return null;
            }
        }
        
        public boolean isLeaf(){
            return false;
        }

        @Override
        public String toString() {
            if(folder!=null){
                return folder.getName();
            } else {
                return "Fetching...";
            }
        }
        
        public int getIndexOfChild(Object child){
            return children.indexOf(child);
        }
        
        private FolderNode locateNodeForFolder(EscFolder folderToFind){
            if(folderToFind==folder){
                return this;
            } else {
                // Work through child nodes
                for(FolderNode node : children){
                    FolderNode n = node.locateNodeForFolder(folderToFind);
                    if(n!=null){
                        return n;
                    }
                }
                
                // Nothing found 
                return null;
            }
        }
        
        private FolderNode locateNodeForFolderId(String folderId){
            if(folder.getId().equals(folderId)){
                return this;
            } else {
                // Work through child nodes
                for(FolderNode node : children){
                    FolderNode n = node.locateNodeForFolderId(folderId);
                    if(n!=null){
                        return n;
                    }
                }
                
                // Nothing found 
                return null;
            }
        }        
    }
}
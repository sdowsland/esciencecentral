/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.api.api.async;
import com.connexience.api.*;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscProject;
import java.util.ArrayList;

/**
 * Task manager for running uploader tasks
 * @author hugo
 */
public class ApiTaskManager {
    private ArrayList<ApiTask> queue = new ArrayList<ApiTask>();
    private ArrayList<ApiTaskManagerListener> listeners = new ArrayList<>();
    private ArrayList<ApiTask> activeTasks = new ArrayList<>();
    private int maxActiveCount = 1;
    
    StorageClient storage;
    WorkflowClient workflow;
    DatasetClient dataset;
    
    public ApiTaskManager() {
    }

    public ApiTaskManager(ApiTaskManager existing) throws Exception {
        this.storage = new StorageClient(existing.getStorageClient());
        this.workflow = new WorkflowClient(existing.getWorkflowClient());
        this.dataset = new DatasetClient(existing.getDatasetClient());
    }

    public void clearPassword(){
        if(storage!=null){storage.setPassword("");}
        if(workflow!=null){workflow.setPassword("");}
        if(dataset!=null){dataset.setPassword("");}
    }
    
    public void addUploaderTaskManagerListener(ApiTaskManagerListener l){
        listeners.add(l);
    }
    
    public synchronized void taskFinished(ApiTask task){
        activeTasks.remove(task);
        notifyStatusChanged();
        checkQueue();
    }
    
    private synchronized void checkQueue(){
        // Look for another task
        if(activeTasks.size()<maxActiveCount){
            // Run another task
            if(queue.size()>0){
                ApiTask task = queue.remove(0);
                activeTasks.add(task);
                Thread t = new Thread(task);
                t.start();      
                notifyStatusChanged();
            }
        }        
    }
    
    public synchronized int getActiveTaskCount(){
        return activeTasks.size();
    }
    
    private synchronized void notifyStatusChanged(){
        for(ApiTaskManagerListener l : listeners){
            l.uploaderTaskManagerStatusChanged(this);
        }
    }
    
    public synchronized void logMessage(String message){
        for(ApiTaskManagerListener l : listeners){
            l.logMessageReceived(message);
        }
    }
    
    public synchronized void logErrorMessage(String message){
        for(ApiTaskManagerListener l : listeners){
            l.errorMessageReceived(message);
        }
    }
    
    public synchronized void performTask(ApiTask task, ApiTask.Callback callback){
        task.addCallback(callback);
        task.setManager(this);
        activeTasks.add(task);
        task.notifyStarted();
        Thread t = new Thread(task);
        t.start();             
        notifyStatusChanged();
    }
    
    public synchronized void performLowPriorityTask(ApiTask task, ApiTask.Callback callback){
        task.addCallback(callback);
        task.setManager(this);
        queue.add(task);
        task.notifyStarted();
        notifyStatusChanged();
        checkQueue();        
    }
    
    public DatasetClient getDatasetClient() throws Exception {
        if(dataset!=null){
            return dataset;
        } else {
            throw new Exception("Not logged in");
        }
    }
    
    public StorageClient getStorageClient() throws Exception {
        if(storage!=null){
            return storage;
        } else {
            throw new Exception("Not logged in");
        }
    }

    public WorkflowClient getWorkflowClient() throws Exception {
        if(workflow!=null){
            return workflow;
        } else {
            throw new Exception("Not logged in");
        }
    }    
    
    public void setStorageClient(StorageClient storage){
        this.storage = storage;
        try {
            workflow = new WorkflowClient(storage);
            dataset = new DatasetClient(storage);
        } catch (Exception e){
            displayError(e);
        }
    }
    
    public void displayError(Exception cause){
        System.out.println(cause.getMessage());
    }
    
   public  EscFolder getUploadFolder(String projectId) throws Exception {
        if(storage!=null){
            EscProject project = storage.getProject(projectId);
            if(project!=null){
                EscFolder targetFolder;
                String folderName = "uploads";

                EscFolder[] children = storage.listChildFolders(project.getDataFolderId());
                for(EscFolder f : children){
                    if(folderName.equals(f.getName())){
                        return f;
                    }
                }
                // Need to create the folder
                targetFolder = storage.createChildFolder(project.getDataFolderId(), folderName);
                return targetFolder;
            } else {
                throw new Exception("No such project");
            }            

        } else {
            throw new Exception("Not logged in");
        }
   }
        
    public boolean isClientFromSameInstallation(StorageClient externalClient){
        if(externalClient.getHostname().equals(storage.getHostname()) && 
          (externalClient.isSecure()==storage.isSecure()) && 
          (externalClient.getPort()==storage.getPort()) && 
          (externalClient.getUsername().equals(storage.getUsername()))){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ApiTaskManager){
            ApiTaskManager mgr = (ApiTaskManager)obj;
            if(storage!=null){
                try {
                    StorageClient mgrStorage = mgr.getStorageClient();
                    if(mgrStorage!=null){
                        if(mgrStorage.getHostname().equals(storage.getHostname()) &&
                           mgrStorage.getPort()==storage.getPort()&&
                           mgrStorage.isSecure()==storage.isSecure() &&
                           mgrStorage.getUsername().equals(storage.getUsername())){
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } catch (Exception e){
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if(storage!=null){
            return storage.getHostname() + ":" + storage.getUsername();
        } else {
            return "Unknown host";
        }
    }
}
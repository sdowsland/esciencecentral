
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDataset;
import com.connexience.api.model.EscDatasetItem;
import com.connexience.api.api.async.ApiTask;

/**
 * Fetches the items contained within a dataset
 * @author Hugo
 */
public class ListDatasetItemsTask extends ApiTask {
    private EscDataset dataset;
    private EscDatasetItem[] items;
    
    public ListDatasetItemsTask(EscDataset dataset) {
        this.dataset = dataset;
    }

    @Override
    public void run() {
        try {
            items = getManager().getDatasetClient().listDatasetContents(dataset.getId());
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscDatasetItem[] getItems() {
        return items;
    }
}
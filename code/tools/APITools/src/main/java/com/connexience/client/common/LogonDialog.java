/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.client.common;

import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.api.async.ApiTaskManager;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.tasks.LogonTask;
import java.awt.event.KeyEvent;
import java.util.prefs.Preferences;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author hugo
 */
public class LogonDialog extends javax.swing.JDialog {
    private boolean authenticated = false;
    private Class preferencesClass;
    private ApiTaskManager manager;
    
    /**
     * Creates new form LogonDialog
     */
    public LogonDialog(java.awt.Frame parent, boolean modal, ApiTaskManager mangager, Class preferencesClass) {
        super(parent, modal);
        this.preferencesClass = preferencesClass;
        this.manager = mangager;
        initComponents();
        setSize(450,350);
        setResizable(false);
        setLocationRelativeTo(null);
        populate();
    }

    private void populate(){
        try {
            Preferences prefs = Preferences.userNodeForPackage(preferencesClass);
            hostnameField.setText(prefs.get("hostname", "localhost"));
            
            // Try and logon
            boolean secure = false;
            int port = prefs.getInt("port", 8080);
     
            // Port inference
            if(port==80){
                portCombo.setSelectedIndex(0);
            } else if(port==8080){
                portCombo.setSelectedIndex(2);
            } else if(port==8181){
                portCombo.setSelectedIndex(3);
            } else if(port==443){
                portCombo.setSelectedIndex(1);
            } else {
                portCombo.setSelectedIndex(0);
            }
            
            usernameField.setText(prefs.get("username", ""));
            passwordField.setText("");
            passwordField.requestFocus();;
        prefs.sync();
        } catch (Exception e){
            
        }
    }
    
    private void save(){
        try {
            Preferences prefs = Preferences.userNodeForPackage(preferencesClass);
            
            if(portCombo.getSelectedIndex()==0){
                prefs.putInt("port", 80);
            } else if(portCombo.getSelectedIndex()==1){
                prefs.putInt("port", 443);
            } else if(portCombo.getSelectedIndex()==2){
                prefs.putInt("port", 8080);
            } else if(portCombo.getSelectedIndex()==3){
                prefs.putInt("port", 8181);
            }
            
            prefs.put("hostname", hostnameField.getText().trim());
            prefs.put("username", usernameField.getText().trim());
            prefs.sync();
        } catch (Exception e){
            
        }
    }

    public boolean isAuthenticated() {
        return authenticated;
    }
    
    private void doLogon(){
        save();
        Preferences prefs = Preferences.userNodeForPackage(preferencesClass);
            
        // Try and logon
        boolean secure = false;
        int port = prefs.getInt("port", 8080);
        if(port==8080 || port==80){
            secure = false;
        } else if (port==8181 || port==443){
            secure = true;
        } else {
            port = 80;
            secure = false;
        }
        
        disableUI();
        final LogonDialog dialog = this;
        LogonTask logon = new LogonTask(prefs.get("username", ""), new String(passwordField.getPassword()), prefs.get("hostname", "localhost"), prefs.getInt("port", 80), secure);
        manager.performTask(logon, new ApiTaskAdapter(){

            @Override
            public void taskCompleted(ApiTask task) {
                authenticated = true;
                SwingUtilities.invokeLater(new Runnable(){
                    public void run(){
                        dispose();
                    }
                });
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                manager.displayError(cause);
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        JOptionPane.showMessageDialog(dialog,"Logon Failed","Error",JOptionPane.ERROR_MESSAGE);   
                        passwordField.setText("");
                        passwordField.requestFocus();
                        enableUI();
                    }
                });
            }

            @Override
            public void taskProgress(int percentage) {
                
            }
        });        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        usernameField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        hostnameField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        portCombo = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Logon");
        setAlwaysOnTop(true);
        setLocationByPlatform(true);
        setModal(true);
        setResizable(false);

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/connexience/uploader/captions"); // NOI18N
        okButton.setText(bundle.getString("OK_TEXT")); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        jPanel1.add(okButton);

        cancelButton.setText(bundle.getString("CANCEL_TEXT")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel1.add(cancelButton);

        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        jLabel2.setText(bundle.getString("USERNAME_TEXT")); // NOI18N

        jLabel1.setText(bundle.getString("PASSWORD_TEXT")); // NOI18N

        passwordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                passwordFieldKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel3.setText(bundle.getString("LOGON_TITLE")); // NOI18N

        jLabel4.setText(bundle.getString("HOSTNAME_TEST")); // NOI18N

        jLabel5.setText(bundle.getString("PORT_TEXT")); // NOI18N
        jLabel5.setToolTipText("");

        portCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Standard", "Secure", "Development", "Secure Development" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5))
                        .addGap(76, 76, 76)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(passwordField)
                            .addComponent(usernameField)
                            .addComponent(hostnameField)
                            .addComponent(portCombo, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(hostnameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(portCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap(57, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        doLogon();
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        System.exit(0);
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void passwordFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_passwordFieldKeyPressed
        if(evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER){
            doLogon();
        }
    }//GEN-LAST:event_passwordFieldKeyPressed

    private void disableUI(){
        okButton.setEnabled(false);
        hostnameField.setEnabled(false);
        portCombo.setEnabled(false);
        usernameField.setEnabled(false);
        passwordField.setEnabled(false);
    }
    
    private void enableUI(){
        okButton.setEnabled(true);
        hostnameField.setEnabled(true);
        portCombo.setEnabled(true);
        usernameField.setEnabled(true);
        passwordField.setEnabled(true);        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField hostnameField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton okButton;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JComboBox portCombo;
    private javax.swing.JTextField usernameField;
    // End of variables declaration//GEN-END:variables
}

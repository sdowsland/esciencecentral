
package com.connexience.api.api.async.tasks;

import com.connexience.api.misc.IProgressInfo;
import com.connexience.api.api.async.ApiTask;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author hugo
 */
public abstract class TransferTask extends ApiTask {
    public abstract String getFileName();
    
    /** Copy the data from one stream to another */
    protected void copyInputStream(InputStream in, OutputStream out, IProgressInfo callback) throws IOException {
        byte[] buffer = new byte[16384];
        int len;
        long bytesSent = 0;

        if (callback != null) {
            while ((len = in.read(buffer)) >= 0) {
                out.write(buffer, 0, len);
                
                bytesSent += len;
                callback.reportProgress(bytesSent);
            }
            callback.reportEnd(bytesSent);
        } else {
            while ((len = in.read(buffer)) >= 0) {
                out.write(buffer, 0, len);
            }
        }
    }    
}

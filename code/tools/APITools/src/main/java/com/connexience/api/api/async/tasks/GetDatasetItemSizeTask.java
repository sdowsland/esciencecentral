package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDatasetItem;
import com.connexience.api.api.async.ApiTask;

/**
 * Gets the size of a dataset item
 * @author Hugo
 */
public class GetDatasetItemSizeTask extends ApiTask {
    private EscDatasetItem item;
    private int size = 0;

    public GetDatasetItemSizeTask(EscDatasetItem item) {
        this.item = item;
    }

    
    @Override
    public void run() {
        if(item.getItemType()==EscDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW){
            try {
                size = getManager().getDatasetClient().getMultipleValueDatasetItemSize(item.getDatasetId(), item.getName());
                notifyCompleted();
            } catch (Exception e){
                notifyFailed(e);
            }
                
        } else {
            notifyFailed(new Exception("No size information"));
        }
    }

    public int getSize() {
        return size;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscFolder;
import com.connexience.api.api.async.ApiTask;
import java.util.ArrayList;

/**
 * Lists child folders for a top level folder
 * @author hugo
 */
public class GetChildFoldersTask extends ApiTask {
    EscFolder folder;
    EscFolder[] children = new EscFolder[0];

    public GetChildFoldersTask(EscFolder folder) {
        this.folder = folder;
    }
    
    @Override
    public void run() {
        try {
            children = getManager().getStorageClient().listChildFolders(folder.getId());
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }
    
    public EscFolder[] getChildren() {
        return children;
    }
    
    public ArrayList<EscFolder> getChildrenArrayList(){
        ArrayList<EscFolder> results = new ArrayList<>();
        for(EscFolder f : children){
            results.add(f);
        }
        return results;
    }
}

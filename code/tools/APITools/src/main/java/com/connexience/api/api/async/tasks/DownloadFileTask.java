

package com.connexience.api.api.async.tasks;

import com.connexience.api.misc.IProgressInfo;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.api.api.async.ApiTask;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Downloads a file from the server
 * @author hugo
 */
public class DownloadFileTask extends TransferTask implements IProgressInfo {
    File localFile;
    EscDocument doc;
    long total;

    public DownloadFileTask(File localFile, EscDocument doc) {
        this.localFile = localFile;
        this.doc = doc;
    }
    
    @Override
    public void run() {
        FileOutputStream outStream = null;
        InputStream inStream = null;
        
        try {
            EscDocumentVersion version = getManager().getStorageClient().getLatestDocumentVersion(doc.getId());
            total = version.getSize();
            inStream = getManager().getStorageClient().openInputStream(version);
            
            if(total>0){
                outStream = new FileOutputStream(localFile);
                copyInputStream(inStream, outStream, this);
                notifyCompleted();
            } else {
                throw new Exception("File contains no data");
            }
            
        } catch (Exception e){
            notifyFailed(e);
        } finally {
            if(outStream!=null){
                try {
                    outStream.flush();
                    outStream.close();
                } catch (Exception e){}
            }
        }
    }

    @Override
    public String getFileName() {
        return localFile.getName();
    }

    @Override
    public void reportBegin(long totalLength) {
        
    }

    @Override
    public void reportProgress(long currentLength) {
        notifyProgress(currentLength, total);
    }

    @Override
    public void reportEnd(long currentLength) {
        
    }
}
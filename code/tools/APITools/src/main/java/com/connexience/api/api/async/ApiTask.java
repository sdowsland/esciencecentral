/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.api.api.async;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class is the base class for an asynchronous uploader task
 * @author hugo
 */
public abstract class ApiTask implements Runnable {
    public enum Status {
        WAITING, RUNNING, SUCCEEDED, FAILED
    }
    
    private Status status = Status.WAITING;
    private CopyOnWriteArrayList<ApiTask.Callback> listeners = new CopyOnWriteArrayList<Callback>();
    private ApiTaskManager manager;
    private int currentPercentComplete = 0;
    
    public ApiTask(){
    }

    public void addCallback(Callback callback) {
        this.listeners.add(callback);
    }

    public void removeCallback(Callback callback){
        this.listeners.remove(callback);
    }
    
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    public void setManager(ApiTaskManager manager) {
        this.manager = manager;
    }

    public ApiTaskManager getManager() {
        return manager;
    }
    
    public void notifyStarted(){
        for(Callback c : listeners){
            c.taskStarted(this);
        }        
    }
    
    protected void notifyCompleted(){
        manager.taskFinished(this);
        for(Callback c : listeners){
            c.taskCompleted(this);
        }
    }
    
    protected void notifyFailed(Exception cause) {
        manager.taskFinished(this);
        for(Callback c : listeners){
            c.taskFailed(this, cause);
        }
    }
    
    protected void notifyProgress(int percentage) {
        if(percentage!=currentPercentComplete){
            currentPercentComplete = percentage;
            for(Callback c : listeners){
                c.taskProgress(percentage);
            }
        }
    }
    
    protected void notifyProgress(long value, long max){
        int percentage = (int)(((double)value / (double)max) * 100.0);
        notifyProgress(percentage);
    }

    public int getCurrentPercentComplete() {
        return currentPercentComplete;
    }
    
    public void logError(String errorMessage){
        if(manager!=null){
            manager.logErrorMessage(errorMessage);
        } else {
            System.out.println("Error: " + errorMessage);
        }
    }
    
    public void logMessage(String message){
        if(manager!=null){
            manager.logMessage(message);
        } else {
            System.out.println(message);
        }
    }
    
    public interface Callback {
        public void taskStarted(ApiTask task);
        public void taskCompleted(ApiTask task);
        public void taskFailed(ApiTask task, Exception cause);
        public void taskProgress(int percentage);
    }
}
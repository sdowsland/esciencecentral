/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscFolder;
import com.connexience.api.api.async.ApiTask;

/**
 * This tasks gets a reference to the users home folder
 * @author hugo
 */
public class GetHomeFolderTask extends ApiTask {
    private EscFolder homeFolder;
    
    @Override
    public void run() {
        try {
            homeFolder = getManager().getStorageClient().homeFolder();
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscFolder getHomeFolder() {
        return homeFolder;
    }
}
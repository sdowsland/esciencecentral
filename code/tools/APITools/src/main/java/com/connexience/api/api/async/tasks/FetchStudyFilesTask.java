
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscProject;
import com.connexience.api.model.EscUser;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskManager;

/**
 * Fetch the list of files from a study
 * @author hugo
 */
public class FetchStudyFilesTask extends ApiTask {
    private EscProject study;
    private EscDocument[] contents;
    private EscUser currentUser = null;
    
    public FetchStudyFilesTask(EscProject study) {
        this.study = study;
    }
    
    @Override
    public void run() {
        try {
            currentUser = getManager().getStorageClient().currentUser();
            EscFolder uploadsFolder = getManager().getUploadFolder(study.getId());
            contents = getManager().getStorageClient().folderDocuments(uploadsFolder.getId());
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscDocument[] getContents() {
        return contents;
    }

    public EscUser getCurrentUser() {
        return currentUser;
    }
}
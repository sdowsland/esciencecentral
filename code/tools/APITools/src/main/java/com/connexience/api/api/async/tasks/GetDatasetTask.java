
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDataset;
import com.connexience.api.api.async.ApiTask;

/**
 * Returns a dataset definition
 * @author Hugo
 */
public class GetDatasetTask extends ApiTask {
    private String datasetId;
    private EscDataset dataset;

    public GetDatasetTask(String datasetId) {
        this.datasetId = datasetId;
        this.dataset = dataset;
    }

    public void run(){
        try {
            dataset = getManager().getDatasetClient().getDataset(datasetId);
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscDataset getDataset() {
        return dataset;
    }
}
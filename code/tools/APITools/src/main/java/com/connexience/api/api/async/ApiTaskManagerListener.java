package com.connexience.api.api.async;

/**
 * This interface defines a listener for the uploader task manager
 * @author hugo
 */
public interface ApiTaskManagerListener {
    public void uploaderTaskManagerStatusChanged(ApiTaskManager mgr);
    public void logMessageReceived(String message);
    public void errorMessageReceived(String message);
}
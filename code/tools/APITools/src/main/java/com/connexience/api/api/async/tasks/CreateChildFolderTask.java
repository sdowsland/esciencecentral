package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscFolder;
import com.connexience.api.api.async.ApiTask;

/**
 * This task creates a child folder in a parent
 * @author hugo
 */
public class CreateChildFolderTask extends ApiTask {
    EscFolder parent;
    EscFolder child;
    String childName;

    public CreateChildFolderTask(EscFolder parent, String childName) {
        this.parent = parent;
        this.childName = childName;
    }
    
    
    @Override
    public void run() {
        try {
            child = getManager().getStorageClient().createChildFolder(parent.getId(), childName);
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscFolder getChild() {
        return child;
    }
}
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.api.async.tasks.ui.UploadFolderProgressDialog;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JDesktopPane;
import javax.swing.SwingUtilities;

/**
 * This task downloads a folder and all of its sub-folders
 * @author hugo
 */
public class DownloadFolderTask extends ApiTask {
    File localFolder;
    EscFolder remoteFolder;
    ArrayList<DownloadFileTask> downloadTasks = new ArrayList<>();
    volatile int taskCount;
    volatile int finishedCount;
    private JDesktopPane desktop = null;
    ApiTask.Callback transferCallback;
    private UploadFolderProgressDialog dialog = null;    

    public DownloadFolderTask(File localFolder, EscFolder remoteFolder) {
        this.localFolder = localFolder;
        this.remoteFolder = remoteFolder;
    }
    
    public void setDesktop(JDesktopPane desktop){
        this.desktop = desktop;
    }    
    
    public void setTransferCallback(ApiTask.Callback transferCallback) {
        this.transferCallback = transferCallback;
    }
    
    @Override
    public void run() {
        if(desktop!=null){
            try {
                dialog = new UploadFolderProgressDialog();
                SwingUtilities.invokeAndWait(new Runnable(){
                    public void run(){
                        desktop.add(dialog);
                        dialog.setVisible(true);
                    }
                });
            } catch (Exception e){
                
            }

        }
        
        // Make a subfolder first
        File targetFolder = new File(localFolder, remoteFolder.getName());
        if(!targetFolder.exists()){
            targetFolder.mkdir();
        }
        
        if(dialog!=null){
            dialog.setExtraText("Creating folders...");
        }
        addDownloadsForFolder(remoteFolder, targetFolder);
        
        if(dialog!=null){
            dialog.setExtraText("Downloading files...");
        }
        
        DownloadFolderCallback downloadCallback = new DownloadFolderCallback();
        taskCount = downloadTasks.size();
        finishedCount = 0;
        for(DownloadFileTask t : downloadTasks){
            t.addCallback(downloadCallback);
            getManager().performLowPriorityTask(t, transferCallback);
        }
        notifyCompleted();
    }
    
    private void addDownloadsForFolder(EscFolder currentRemoteFolder, File currentLocalFolder){
        try {
            if(dialog!=null){
                dialog.setExtraText("Creating folders in: " + currentRemoteFolder.getName());
            }
        
            EscDocument[] documents = getManager().getStorageClient().folderDocuments(currentRemoteFolder.getId());
            for(EscDocument d : documents){
                DownloadFileTask task = new DownloadFileTask(new File(currentLocalFolder, d.getName()), d);
                downloadTasks.add(task);
            }
                
            // Now the folders
            EscFolder[] folders = getManager().getStorageClient().listChildFolders(currentRemoteFolder.getId());
            for(EscFolder f : folders){
                // Make the local directory
                File localFolder = new File(currentLocalFolder, f.getName());
                if(!localFolder.exists()){
                    localFolder.mkdir();
                }
                
                // Recursively add
                addDownloadsForFolder(f, localFolder);
            }
        } catch (Exception e){
            logError(e.getMessage());
        }
    }
    
    private class DownloadFolderCallback extends ApiTaskAdapter {

        @Override
        public void taskCompleted(ApiTask task) {
            finishedCount++;
            if(dialog!=null){
                dialog.setProgress(finishedCount, taskCount);
            }            
            if(finishedCount==taskCount){
                if(dialog!=null){
                    dialog.dispose();
                    dialog = null;
                }                
            }
        }

        @Override
        public void taskFailed(ApiTask task, Exception cause) {

            finishedCount++;
            if(dialog!=null){
                dialog.setProgress(finishedCount, taskCount);
            }            
            if(finishedCount==taskCount){
                System.out.println("All uploads done - fail");
                if(dialog!=null){
                    dialog.dispose();
                    dialog = null;
                }                
            }
        }
        
    }
}


package com.connexience.client.common.uitasks;

import com.connexience.api.api.async.ApiTask;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

/**
 * Sets the text of a component with the name of an object
 * @author hugo
 */
public class PopulateJComponentWithNameTask extends ApiTask {
    private JComponent component;
    private String objectId;

    public PopulateJComponentWithNameTask(JComponent component, String objectId) {
        this.component = component;
        this.objectId = objectId;
    }
    
    @Override
    public void run() {
        try {
            setText("Fetching...");
            if(objectId!=null && !objectId.isEmpty()){
                setText(getManager().getStorageClient().getObjectName(objectId));
            } else {
                setText("No ID");
            }
            notifyCompleted();
        } catch (Exception e){
            setText("Error getting name");
            notifyFailed(e);
        }
    }
    
    private void setText(final String text){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if(component instanceof JTextField){
                    ((JTextField)component).setText(text);

                } else if(component instanceof JTextArea){
                    ((JTextArea)component).setText(text);

                } else if(component instanceof JTextPane) {
                    ((JTextPane)component).setText(text);

                } else if(component instanceof JLabel){
                    ((JLabel)component).setText(text);

                }                
            }
        });
    }
}
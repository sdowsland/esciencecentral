
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import com.connexience.api.api.async.ApiTask;
import java.util.ArrayList;
import java.util.List;

/**
 * This task gets all of the contents for a folder including child folders
 * and documents
 * @author Hugo
 */
public class GetFolderContentsTask extends ApiTask {
    EscFolder folder;
    ArrayList<EscObject> contents = new ArrayList<>();

    public GetFolderContentsTask(EscFolder folder) {
        this.folder = folder;
    }
    
    
    public void run(){
        try {
            EscFolder[] folders = getManager().getStorageClient().listChildFolders(folder.getId());
            for(EscObject o : folders){
                contents.add(o);
            }
            
            EscDocument[] docs = getManager().getStorageClient().folderDocuments(folder.getId());
            for(EscObject o : docs){
                contents.add(o);
            }
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }
    
    public List<EscObject> getContents(){
        return contents;
    }

    public EscFolder getFolder() {
        return folder;
    }
    
}


package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscObject;
import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskManager;

/**
 * Lists related documents
 * @author hugo
 */
public class GetRelatedDocumentsTask extends ApiTask {
    private EscDocument doc;
    private EscObject[] related = new EscObject[0];
    
    public GetRelatedDocumentsTask(EscDocument doc) {
        this.doc = doc;
    }
   
    @Override
    public void run() {
        try {
            EscDocument[] docs = getManager().getStorageClient().getRelatedDocuments(doc.getId());
            EscWorkflowInvocation[] workflows = getManager().getWorkflowClient().listInvocationsRelatedToDocument(doc.getId());
            
            int count = 0;
            related = new EscObject[docs.length + workflows.length];
            for(EscDocument d : docs){
                related[count] = d;
                count++;
            }
            
            for(EscWorkflowInvocation w : workflows){
                related[count] = w;
                count++;
            }
            notifyCompleted();
        } catch (Exception e){
            getManager().displayError(e);
            notifyFailed(e);
        }
    }

    public EscObject[] getRelated() {
        return related;
    }    
}

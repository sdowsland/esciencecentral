/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common;

import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskManager;
import com.connexience.api.api.async.tasks.ChangeParentFolderTask;
import com.connexience.api.api.async.tasks.GetFolderContentsTask;
import com.connexience.api.api.async.tasks.GetFolderDocumentsTask;
import com.connexience.api.api.async.tasks.TransferTask;
import com.connexience.api.api.async.tasks.UploadFileToFolderTask;
import com.connexience.api.api.async.tasks.UploadFileToProjectTask;
import com.connexience.api.api.async.tasks.UploadFolderTask;
import com.connexience.client.common.dnd.EscObjectDataFlavor;
import com.connexience.client.common.dnd.EscObjectListTransferrable;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorMap;
import java.awt.datatransfer.SystemFlavorMap;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

/**
 *
 * @author hugo
 */
public class FolderView extends javax.swing.JPanel {

    EscFolder folder;
    final ApiTaskManager manager;
    FolderTree tree;
    //DefaultListModel model = new DefaultListModel();
    java.util.concurrent.CopyOnWriteArrayList<FolderViewListener> listeners = new CopyOnWriteArrayList<>();
    DragSource source;
    DropTarget target;
    private String windowGuid;
    
    
    /**
     * Creates new form FolderView
     */
    public FolderView(final ApiTaskManager manager) {
        this.manager = manager;
        initComponents();
        filesList.setCellRenderer(new DocumentListRenderer());
        filesList.setModel(new DefaultListModel());
        filesList.setDragEnabled(true);
        setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));

        filesList.setTransferHandler(new FolderViewTransferHandler());
        
        filesList.setDropMode(DropMode.INSERT);
        target = new DropTarget(filesList, new DropTargetAdapter() {

            @Override
            public void dragEnter(DropTargetDragEvent dtde) {
                super.dragEnter(dtde);
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){
                    setBorder(new LineBorder(Color.BLACK/*UIManager.getColor("controlHighlight")*/));
                    dtde.acceptDrag(DnDConstants.ACTION_COPY);
                    repaint();
                    
                } else if(dtde.isDataFlavorSupported(EscObjectDataFlavor.escObjectFlavor)){
                    setBorder(new LineBorder(Color.BLACK/*UIManager.getColor("controlHighlight")*/));
                    dtde.acceptDrag(DnDConstants.ACTION_MOVE);
                    repaint();
                            
                } else {
                    dtde.rejectDrag();
                    setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
                    repaint();
                }
            }

            @Override
            public void dragExit(DropTargetEvent dte) {
                super.dragExit(dte);
                setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
                repaint();
            }
            
            @Override
            public void drop(DropTargetDropEvent dtde) {
                setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
                repaint();
                
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){
                    dtde.acceptDrop(DnDConstants.ACTION_COPY);
                    // Files
                    try {
                        File f;
                        Object files = dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                        if(files instanceof List){
                            List uploads = (List)files;
                            for(Object o : uploads){
                                f = (File)o;
                                if(f.isFile()){
                                    uploadFile(f);
                                } else {
                                    uploadFolder(f);
                                }
                            }
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    
                } else if(dtde.isDataFlavorSupported(EscObjectDataFlavor.escObjectFlavor)){
                    // EscObjects
                    try {
                        // Make sure we're not dropping onto ourself
                        EscObjectListTransferrable objects = (EscObjectListTransferrable)dtde.getTransferable().getTransferData(EscObjectDataFlavor.escObjectFlavor);
                        if(!objects.getSourceComponentGuid().equals(windowGuid)){
                            dtde.acceptDrop(DnDConstants.ACTION_MOVE);
                            dropEscObjects(objects);
                        } else {
                            dtde.rejectDrop();
                        }

                        
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        
        final FolderView v = this;
        filesList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                notifyItemSelected(new ObjectSelection(v, filesList.getSelectedValuesList()));
            }
        });
        
    }

    public void setWindowGuid(String windowGuid) {
        this.windowGuid = windowGuid;
    }

    public String getWindowGuid() {
        return windowGuid;
    }

    
    public EscFolder getFolder() {
        return folder;
    }

    private void dropEscObjects(final EscObjectListTransferrable objects){
        if(manager.isClientFromSameInstallation(objects.getClient())){
            // Move within installation
            final ChangeParentFolderTask changeParentTask = new ChangeParentFolderTask(folder, objects.getObjects());
            manager.performTask(changeParentTask, new ApiTaskAdapter(){

                @Override
                public void taskStarted(ApiTask task) {
                    notifyTransferStarted(changeParentTask);   
                }

                @Override
                public void taskCompleted(ApiTask task) {
                    notifyTransferCompleted(changeParentTask);
                    fetchFolderFiles();
                    notifyFolderContentsChanged(folder);
                }

                @Override
                public void taskFailed(ApiTask task, Exception cause) {
                    notifyTransferCompleted(changeParentTask);
                    fetchFolderFiles();
                }
            });
            
        } else {
            // Move between installations
            System.out.println("Move between installations");
        }
    }
    
    private void notifyFolderContentsChanged(EscFolder folder){
        for(FolderViewListener l : listeners){
            l.folderContentsChanged(folder);
        }
    }
    
    private void uploadFolder(File f){
        UploadFolderTask task = new UploadFolderTask(f, folder);
        
        task.setTransferCallback(new ApiTaskAdapter(){

            @Override
            public void taskStarted(ApiTask task) {
                System.out.println("Task started: " + task.getClass().getSimpleName());
                if(task instanceof TransferTask){
                    notifyTransferStarted((TransferTask)task);
                }
            }

            @Override
            public void taskCompleted(ApiTask task) {
                /*
                if(task instanceof TransferTask){
                    notifyTransferCompleted((TransferTask)task);
                    if(task instanceof UploadFileToFolderTask){
                        if(folder!=null && ((UploadFileToFolderTask)task).getFolder()!=null && ((UploadFileToFolderTask)task).getFolder().getId().equals(folder.getId())){
                            // Need to refresh list
                            fetchFolderFiles();
                        }
                    }                    
                }
                */
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                if(task instanceof TransferTask){
                    notifyTransferCompleted((TransferTask)task);
                    
                    if(task instanceof UploadFileToFolderTask){
                        if(folder!=null && ((UploadFileToFolderTask)task).getFolder()!=null && ((UploadFileToFolderTask)task).getFolder().getId().equals(folder.getId())){
                            // Need to refresh list
                            fetchFolderFiles();
                        }
                    }
                }
            }
        });
        
        manager.performTask(task, new ApiTaskAdapter(){

            @Override
            public void taskCompleted(ApiTask task) {
                fetchFolderFiles();
                notifyFolderContentsChanged(folder);
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                fetchFolderFiles();
            }
        });
    }
    
    private void uploadFile(File f) {
        UploadFileToFolderTask task = new UploadFileToFolderTask(folder, f);
        manager.performTask(task, new ApiTaskAdapter(){

            @Override
            public void taskStarted(ApiTask task) {
                notifyTransferStarted((TransferTask)task);
            }
            
            @Override
            public void taskCompleted(ApiTask task) {
                fetchFolderFiles();
                notifyTransferCompleted((TransferTask)task);
                notifyFolderContentsChanged(folder);
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                fetchFolderFiles();
                notifyTransferCompleted((TransferTask)task);
            }

            @Override
            public void taskProgress(int percentage) {

            }
        });
    }

    private void notifyObjectDoubleClicked(EscObject object){
        for (FolderViewListener l : listeners) {
            l.objectDoubleClicked(object);
        }
    }
    
    private void notifyItemSelected(ObjectSelection item){
        for (FolderViewListener l : listeners) {
            l.itemSelected(item);
        }        
    }
    
    private void notifyTransferStarted(TransferTask transfer) {
        for (FolderViewListener l : listeners) {
            l.transferStarted(this, transfer);
        }
    }

    private void notifyTransferCompleted(TransferTask transfer) {
        for (FolderViewListener l : listeners) {
            l.transferCompleted(this, transfer);
        }
    }
    public void addFolderViewListener(FolderViewListener l) {
        listeners.add(l);
    }

    public void removeFolderViewListener(FolderViewListener l) {
        listeners.remove(l);
    }

    public void setFolder(EscFolder folder) {
        this.folder = folder;
        if (folder != null) {
            folderNameLabel.setText(folder.getName());
            fetchFolderFiles();
        } else {
            folderNameLabel.setText("Nothing selected");
            javax.swing.SwingUtilities.invokeLater(new Runnable(){
                public void run(){
                    filesList.setModel(new DefaultListModel());
                }
            });
        }
        
    }

    public void listenToTree(FolderTree tree) {
        this.tree = tree;
        tree.getTree().addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                if (e.getPath() != null && e.getPath().getLastPathComponent() instanceof FolderTreeModel.FolderNode) {
                    setFolder(((FolderTreeModel.FolderNode) e.getPath().getLastPathComponent()).getFolder());
                } else {
                    setFolder(null);
                }
            }
        });
        
        tree.getModel().addFolderTreeModelListener(new FolderTreeModelListener() {
            @Override
            public void rootNodeFetched(FolderTreeModel.FolderNode node) {
                setFolder(node.getFolder());
            }
        });
    }

    public void fetchFolderFilesIfFolderVisible(EscFolder updatedFolder){
        if(folder!=null && folder.getId()!=null && updatedFolder!=null && updatedFolder.getId()!=null){
            if(updatedFolder.getId().equals(folder.getId())){
                fetchFolderFiles();
            }
        }
    }
    
    public void fetchFolderFiles() {
        if (folder != null) {
            DefaultListModel model = new DefaultListModel();
            model.addElement("Fetching...");
            filesList.setModel(model);

            GetFolderContentsTask task = new GetFolderContentsTask(folder);
            manager.performTask(task, new ApiTaskAdapter() {

                @Override
                public void taskCompleted(ApiTask task) {
                    final GetFolderContentsTask t = (GetFolderContentsTask) task;
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            DefaultListModel updatedModel = new DefaultListModel();
                            if (folder != null && folder.getId().equals(t.getFolder().getId())) {
                                for (EscObject o : t.getContents()) {
                                    updatedModel.addElement(o);
                                }
                                filesList.setModel(updatedModel);
                                filesList.revalidate();
                                        
                            } else {
                                filesList.setModel(new DefaultListModel());
                            }
                        }

                    });

                }

                @Override
                public void taskFailed(ApiTask task, Exception cause) {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            filesList.setModel(new DefaultListModel());
                        }
                    });
                }

            });
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        folderNameLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        filesList = new javax.swing.JList();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout(2, 2));

        folderNameLabel.setBackground(new java.awt.Color(0, 102, 153));
        folderNameLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        folderNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        folderNameLabel.setText(" ");
        folderNameLabel.setOpaque(true);
        jPanel1.add(folderNameLabel, java.awt.BorderLayout.CENTER);

        add(jPanel1, java.awt.BorderLayout.NORTH);

        filesList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        filesList.setDragEnabled(true);
        filesList.setDropMode(javax.swing.DropMode.INSERT);
        filesList.setLayoutOrientation(javax.swing.JList.VERTICAL_WRAP);
        filesList.setVisibleRowCount(-1);
        filesList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                filesListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(filesList);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void filesListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_filesListMouseClicked
        if(evt.getClickCount()==2){
            Object selected = filesList.getSelectedValue();
            if(selected instanceof EscObject){
                notifyObjectDoubleClicked((EscObject)selected);
                
            }
        }
    }//GEN-LAST:event_filesListMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList filesList;
    private javax.swing.JLabel folderNameLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public class FolderViewTransferHandler extends TransferHandler {

        @Override
        public int getSourceActions(JComponent c) {
            return COPY_OR_MOVE;
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            if(c instanceof JList){
                try {
                    EscObjectListTransferrable transferrable = new EscObjectListTransferrable(windowGuid, manager.getStorageClient(), ((JList)c).getSelectedValuesList());
                    transferrable.setSourceComponentType(EscObjectListTransferrable.FOLDER_VIEW);
                    return transferrable;
                } catch (Exception e){
                    return super.createTransferable(c);
                }
            } else {
                return super.createTransferable(c); //To change body of generated methods, choose Tools | Templates.
            }
        }

        @Override
        protected void exportDone(JComponent source, Transferable data, int action) {
            super.exportDone(source, data, action); //To change body of generated methods, choose Tools | Templates.
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.client.common;

import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.tasks.TransferTask;
import com.connexience.api.api.async.tasks.UploadFileToProjectTask;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author hugo
 */
public class TransfersTableModel implements TableModel, ApiTask.Callback {
    ArrayList<TableModelListener> listeners = new ArrayList<TableModelListener>();
    ArrayList<TransferTask> uploads = new ArrayList<>();
    
    public void addTransfer(TransferTask task){
        uploads.add(task);
        task.addCallback(this);
        notifyChange();
    }

    @Override
    public synchronized void taskCompleted(ApiTask task) {
        task.removeCallback(this);
        uploads.remove(task);
        notifyChange();
    }

    @Override
    public synchronized void taskFailed(ApiTask task, Exception cause) {
        task.removeCallback(this);
        uploads.remove(task);
        notifyChange();
    }

    @Override
    public synchronized void taskStarted(ApiTask task) {

    }
    
    @Override
    public synchronized void taskProgress(int percentage) {
        notifyChange();
    }
    
    private synchronized void notifyChange(){
        final TableModelEvent evt = new TableModelEvent(this);
        for(final TableModelListener l : listeners){
            l.tableChanged(evt);
        }
    }
    
    @Override
    public int getRowCount() {
        return uploads.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex){
            case 0:
                return "Name";
            case 1:
                return "Progress";
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case 0:
                return String.class;
            case 1:
                return Integer.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public synchronized Object getValueAt(int rowIndex, int columnIndex) {
        try {
            TransferTask task = uploads.get(rowIndex);
            switch(columnIndex){
                case 0:
                    return task.getFileName();

                case 1:
                    return task.getCurrentPercentComplete();
                default:
                    return null;
            }
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}

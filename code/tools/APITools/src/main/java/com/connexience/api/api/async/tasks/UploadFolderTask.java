package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscFolder;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.api.async.tasks.ui.UploadFolderProgressDialog;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JDesktopPane;
import javax.swing.SwingUtilities;

/**
 * This tasks uploads an entire folder and any subfolders and files to the server
 * @author hugo
 */
public class UploadFolderTask extends TransferTask {
    EscFolder topFolder;
    File localFolder;
    ApiTask.Callback transferCallback;
    UploadFolderCallback globalCallback = new UploadFolderCallback();
    ArrayList<ApiTask> allMyTasks = new ArrayList<>();
    private JDesktopPane desktop = null;
    private UploadFolderProgressDialog dialog = null;
    private int totalTaskCount = 0;
    private int completedTaskCount = 0;
    
    public UploadFolderTask(File localFolder, EscFolder topFolder) {
        this.topFolder = topFolder;
        this.localFolder = localFolder;
    }

    public void setTransferCallback(ApiTask.Callback transferCallback) {
        this.transferCallback = transferCallback;
    }

    public void setDesktop(JDesktopPane desktop){
        this.desktop = desktop;
    }
    
    @Override
    public void run() {
        if(desktop!=null){
            try {
                dialog = new UploadFolderProgressDialog();
                SwingUtilities.invokeAndWait(new Runnable(){
                    public void run(){
                        desktop.add(dialog);
                        dialog.setVisible(true);
                    }
                });
            } catch (Exception e){
                
            }

        }
        
        uploadFolder(localFolder, topFolder);
        notifyCompleted();
    }

    private void uploadFolder(final File folder, final EscFolder escParent){
        CreateChildFolderTask createFolder = new CreateChildFolderTask(escParent, folder.getName());        
        getManager().performLowPriorityTask(createFolder, new ApiTaskAdapter(){

            @Override
            public void taskCompleted(ApiTask task) {
                for(File f : folder.listFiles()){
                    if(f.isDirectory()){
                        uploadFolder(f, ((CreateChildFolderTask)task).getChild());
                    }
                }
                
                // Upload the files
                ArrayList<UploadFileToFolderTask> list = new ArrayList<>();
                for(File f : folder.listFiles()){
                    
                    if(f.isFile()){
                        UploadFileToFolderTask uploadTask = new UploadFileToFolderTask(((CreateChildFolderTask)task).getChild(), f);
                        uploadTask.addCallback(globalCallback);
                        allMyTasks.add(uploadTask);
                        totalTaskCount++;
                        list.add(uploadTask);
                    }
                }
                
                for(UploadFileToFolderTask t : list){
                    getManager().performLowPriorityTask(t, transferCallback);                    
                }
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                cause.printStackTrace();
            }

        });
    }
    
    @Override
    public String getFileName() {
        return "Folder: " + localFolder.getName();
    }
    
    private class UploadFolderCallback extends ApiTaskAdapter {

        @Override
        public void taskStarted(ApiTask task) {
            if(dialog!=null){
                dialog.setProgress(completedTaskCount, totalTaskCount);
            } else {
                // Should we bring back the dialog
                if(desktop!=null){
                    try {
                        
                        dialog = new UploadFolderProgressDialog();
                        SwingUtilities.invokeAndWait(new Runnable(){
                            public void run(){
                                desktop.add(dialog);
                                dialog.setVisible(true);
                                dialog.setProgress(completedTaskCount, totalTaskCount);
                            }
                        });
                        
                    } catch (Exception e){

                    }                    
                }
            }
        }

        @Override
        public void taskCompleted(ApiTask task) {
            allMyTasks.remove(task);
            completedTaskCount++;
            if(dialog!=null){
                dialog.setProgress(completedTaskCount, totalTaskCount);
            }
            
            if(allMyTasks.size()==0){
                System.out.println("All uploads done");
                if(dialog!=null){
                    dialog.dispose();
                    dialog = null;
                }
            }
        }

        @Override
        public void taskFailed(ApiTask task, Exception cause) {
            allMyTasks.remove(task);
            completedTaskCount++;
            if(dialog!=null){
                dialog.setProgress(completedTaskCount, totalTaskCount);
            }            
            if(allMyTasks.size()==0){
                System.out.println("All uploads done - fail");
                if(dialog!=null){
                    dialog.dispose();
                    dialog = null;
                }                
            }
        }
        
    }
}
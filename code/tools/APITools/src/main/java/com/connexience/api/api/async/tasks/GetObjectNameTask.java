
package com.connexience.api.api.async.tasks;

import com.connexience.api.api.async.ApiTask;

/**
 * Fetches the name of an object
 * @author hugo
 */
public class GetObjectNameTask extends ApiTask {
    private String objectId;
    private String name = "";
    
    public GetObjectNameTask(String objectId) {
        this.objectId = objectId;
    }

    @Override
    public void run() {
        try {
            name = getManager().getStorageClient().getObjectName(objectId);
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public String getName() {
        return name;
    }
}

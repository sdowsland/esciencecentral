
package com.connexience.client.common;

import com.connexience.api.model.EscObject;
import com.connexience.api.api.async.tasks.TransferTask;
import com.connexience.api.model.EscFolder;
import java.awt.Component;

/**
 * Listener to a folder view panel
 * @author hugo
 */
public interface FolderViewListener {
    public void transferStarted(Component sourceComponent, TransferTask transfer);
    public void transferCompleted(Component sourceComponent, TransferTask transfer);
    public void objectDoubleClicked(EscObject object);
    public void itemSelected(ObjectSelection item);
    public void folderContentsChanged(EscFolder folder);
}

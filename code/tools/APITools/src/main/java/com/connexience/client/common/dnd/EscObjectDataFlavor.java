package com.connexience.client.common.dnd;

import com.connexience.api.model.EscObject;
import java.awt.datatransfer.DataFlavor;
import java.util.ArrayList;

/**
 * List of ESC objects
 * @author hugo
 */
public class EscObjectDataFlavor extends DataFlavor {
    public static DataFlavor escObjectFlavor;
    
    static {
        escObjectFlavor = new DataFlavor(EscObjectListTransferrable.class, "EscObjects");
    }
}


package com.connexience.api.api.async.tasks;

import com.connexience.api.StorageClient;
import com.connexience.api.api.async.ApiTask;

/**
 * Logs a user into the system
 * @author hugo
 */
public class LogonTask extends ApiTask {
    private String username;
    private String password;
    private String host;
    private int port;
    private boolean secure;

    public LogonTask(String username, String password, String host, int port, boolean secure) {
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.secure = secure;
    }
    
    @Override
    public void run() {
        try {
            StorageClient client = new StorageClient(host, port, secure, username, password);
            client.setUploadChunkingEnabled(false);
            long time = client.getTimestamp();
            getManager().setStorageClient(client);
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }
}

package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.api.api.async.ApiTask;

/**
 *
 * @author hugo
 */
public class GetLatestVersionTask extends ApiTask{
    EscDocument doc;
    EscDocumentVersion version;
    
    public GetLatestVersionTask(EscDocument doc) {
        this.doc = doc;
    }
    
    
    @Override
    public void run() {
        try {
            version = getManager().getStorageClient().getLatestDocumentVersion(doc.getId());
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscDocumentVersion getVersion() {
        return version;
    }
}

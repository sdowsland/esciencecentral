/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common;

import com.connexience.api.model.EscFolder;

import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.api.async.ApiTaskManager;
import com.connexience.api.api.async.tasks.ChangeParentFolderTask;
import com.connexience.api.api.async.tasks.TransferTask;
import com.connexience.api.api.async.tasks.UploadFileToFolderTask;
import com.connexience.client.common.dnd.EscObjectDataFlavor;
import com.connexience.client.common.dnd.EscObjectListTransferrable;
import java.awt.Color;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DropMode;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

/**
 * Displays a tree of folders
 * @author hugo
 */
public class FolderTree extends javax.swing.JPanel {
    ApiTaskManager manager;
    FolderTreeModel model;
    DropTarget target;
    ArrayList<FolderViewListener> listeners = new ArrayList<>();
    
    /**
     * Creates new form FolderTree
     */
    public FolderTree(ApiTaskManager manager) {
        this.manager = manager;
        model = new FolderTreeModel(manager);
        initComponents();
        folderTree.setModel(model);
        setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
        folderTree.addTreeExpansionListener(new TreeExpansionListener() {

            @Override
            public void treeExpanded(TreeExpansionEvent event) {
                TreePath path = event.getPath();
                
                if(path.getLastPathComponent() instanceof FolderTreeModel.FolderNode){
                    FolderTreeModel.FolderNode node = (FolderTreeModel.FolderNode)path.getLastPathComponent();
                    if(!node.isPopulated()){
                        node.populate();
                    }
                }
            }

            @Override
            public void treeCollapsed(TreeExpansionEvent event) {

            }
        });
        
        // Drop listener
        folderTree.setDropMode(DropMode.INSERT);
        target = new DropTarget(folderTree, new DropTargetAdapter() {

            @Override
            public void dragEnter(DropTargetDragEvent dtde) {
                super.dragEnter(dtde);
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){
                    setBorder(new LineBorder(Color.BLACK/*UIManager.getColor("controlHighlight")*/));  
                    //dtde.acceptDrag(DnDConstants.ACTION_COPY);
                    repaint();
                    
                } else if(dtde.isDataFlavorSupported(EscObjectDataFlavor.escObjectFlavor)){
                    setBorder(new LineBorder(Color.BLACK/*UIManager.getColor("controlHighlight")*/));
                    //dtde.acceptDrag(DnDConstants.ACTION_MOVE);
                    repaint();
                            
                } else {
                    dtde.acceptDrag(DnDConstants.ACTION_MOVE);
                    setBorder(new LineBorder(Color.BLACK/*UIManager.getColor("controlHighlight")*/));
                    //setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
                    repaint();
                }
            }

            @Override
            public void dragOver(final DropTargetDragEvent dtde) {
                super.dragEnter(dtde);
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){
                    TreePath path = folderTree.getClosestPathForLocation(dtde.getLocation().x, dtde.getLocation().y);
                    if(path!=null){
                        folderTree.expandPath(path);
                    }                    
                    
                } else if(dtde.isDataFlavorSupported(EscObjectDataFlavor.escObjectFlavor)){
                    
                    TreePath path = folderTree.getClosestPathForLocation(dtde.getLocation().x, dtde.getLocation().y);
                    if(path!=null){
                        folderTree.expandPath(path);
                    }                                   
                }
            }

            
            @Override
            public void dragExit(DropTargetEvent dte) {
                super.dragExit(dte);
                setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
                repaint();
            }
            
            @Override
            public void drop(DropTargetDropEvent dtde) {
                setBorder(new EmptyBorder(new Insets(1, 1, 1, 1)));
                repaint();
                dtde.acceptDrop(DnDConstants.ACTION_COPY);
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){
                    // Uploading File(s)
                    TreePath path = folderTree.getClosestPathForLocation(dtde.getLocation().x, dtde.getLocation().y);
                    if(path!=null){
                        folderTree.expandPath(path);

                        if(path.getLastPathComponent() instanceof FolderTreeModel.FolderNode){
                            
                            FolderTreeModel.FolderNode node = (FolderTreeModel.FolderNode)path.getLastPathComponent();
                            EscFolder target = node.getFolder();
                            if(target!=null){
                                try {
                                    File f;
                                    Object files = dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                                    if(files instanceof List){
                                        List uploads = (List)files;
                                        for(Object o : uploads){
                                            f = (File)o;
                                            if(f.isFile()){
                                                uploadFile(target, f);
                                            }
                                        }
                                    }
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } else {
                    // List of esc objects
                    try {
                        Object o = dtde.getTransferable().getTransferData(EscObjectDataFlavor.escObjectFlavor);
                        TreePath path = folderTree.getClosestPathForLocation(dtde.getLocation().x, dtde.getLocation().y);
                        if(path!=null){
                            folderTree.expandPath(path);

                            if(path.getLastPathComponent() instanceof FolderTreeModel.FolderNode){
                                FolderTreeModel.FolderNode node = (FolderTreeModel.FolderNode)path.getLastPathComponent();
                                EscObjectListTransferrable transferrable = (EscObjectListTransferrable)o;
                                dropEscObjects(node.getFolder(), transferrable);
                            }
                         }
                        
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        
        final FolderTree t = this;
        folderTree.addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                TreePath[] paths = folderTree.getSelectionPaths();
                if(paths!=null){
                    ObjectSelection selection = new ObjectSelection(t);
                    for(TreePath p : paths){
                        if(p.getLastPathComponent() instanceof FolderTreeModel.FolderNode){
                            selection.add(((FolderTreeModel.FolderNode)p.getLastPathComponent()).getFolder());
                        }
                    }

                    notifyItemSelected(selection);
                
                }
            }
        });
    }

    private void dropEscObjects(final EscFolder folder, final EscObjectListTransferrable objects){
        final ChangeParentFolderTask changeParentTask = new ChangeParentFolderTask(folder, objects.getObjects());
        manager.performTask(changeParentTask, new ApiTaskAdapter(){

            @Override
            public void taskStarted(ApiTask task) {
                notifyTransferStarted(changeParentTask);   
            }

            @Override
            public void taskCompleted(ApiTask task) {
                notifyTransferCompleted(changeParentTask);
                repopulateFolder(folder);
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                notifyTransferCompleted(changeParentTask);
            }
        });
    }
    
    public void repopulateSelected(){
        if(folderTree.getSelectionPath()!=null && folderTree.getSelectionPath().getLastPathComponent() instanceof FolderTreeModel.FolderNode){
            FolderTreeModel.FolderNode node = (FolderTreeModel.FolderNode)folderTree.getSelectionPath().getLastPathComponent();
            if(model.isRoot(node)){
                model.populateTopFolder();
            } else {
                node.populate();
            }
        }
    }
    
    public void repopulateFolder(EscFolder folder){
        FolderTreeModel.FolderNode node = model.findNodeForFolder(folder);
        if(node!=null){
            if(model.isRoot(node)){
                model.populateTopFolder();
            } else {
                node.populate();
            }
        }
    }
    
    public void selectFolder(String folderId){
        FolderTreeModel.FolderNode node = model.findNodeForFolderId(folderId);
        if(node!=null){
            ArrayList<FolderTreeModel.FolderNode> pathNodes = new ArrayList<>();
            node.constructPathToHere(pathNodes);
            TreePath path;
            if(pathNodes.size()==1){
                path = new TreePath(node);
                folderTree.setSelectionPath(path);

            } else {
                path = new TreePath(pathNodes.toArray());           
                folderTree.setSelectionPath(path);
            }          
        }
    }
    
    public void selectAndExpandFolder(EscFolder folder){
        FolderTreeModel.FolderNode node = model.findNodeForFolderId(folder.getId());
        if(node==null){
            node = model.findNodeForFolderId(folder.getContainerId());
        }
        
        if(node!=null){
            ArrayList<FolderTreeModel.FolderNode> pathNodes = new ArrayList<>();
            node.constructPathToHere(pathNodes);
            TreePath path;
            if(pathNodes.size()==1){
                path = new TreePath(node);
                folderTree.expandPath(path);
                folderTree.setSelectionPath(path);

            } else {
                path = new TreePath(pathNodes.toArray());           
                folderTree.expandPath(path);
                folderTree.setSelectionPath(path);
            }          
            folderTree.makeVisible(new TreePath(node));

            int row = folderTree.getRowForPath(path);
            folderTree.scrollRowToVisible(row);                  
            
        }
    }
    
    public void selectAndExpandFolder(String folderId){
        FolderTreeModel.FolderNode node = model.findNodeForFolderId(folderId);
        if(node!=null){
            ArrayList<FolderTreeModel.FolderNode> pathNodes = new ArrayList<>();
            node.constructPathToHere(pathNodes);
            TreePath path;
            if(pathNodes.size()==1){
                path = new TreePath(node);
                folderTree.expandPath(path);
                folderTree.setSelectionPath(path);

            } else {
                path = new TreePath(pathNodes.toArray());           
                folderTree.expandPath(path);
                folderTree.setSelectionPath(path);
            }          
        }
    }    
    
    public void repopulateFolder(String folderId){
        FolderTreeModel.FolderNode node = model.findNodeForFolderId(folderId);
        if(node!=null){
            if(model.isRoot(node)){
                model.populateTopFolder();
            } else {
                node.populate();
            }
        }
    }
    
    
    private void notifyItemSelected(ObjectSelection item){
        for (FolderViewListener l : listeners) {
            l.itemSelected(item);
        }        
    }
    
    private void notifyTransferStarted(TransferTask transfer) {
        for (FolderViewListener l : listeners) {
            l.transferStarted(this, transfer);
        }
    }
    
    private void notifyTransferCompleted(TransferTask transfer) {
        for (FolderViewListener l : listeners) {
            l.transferCompleted(this, transfer);
        }
    }    

    public void addFolderViewListener(FolderViewListener l) {
        listeners.add(l);
    }

    public void removeFolderViewListener(FolderViewListener l) {
        listeners.remove(l);
    }
    
    private void uploadFile(EscFolder folder, File f) {
        UploadFileToFolderTask task = new UploadFileToFolderTask(folder, f);
        notifyTransferStarted(task);
        manager.performTask(task, new ApiTaskAdapter() {

            @Override
            public void taskCompleted(ApiTask task) {
                
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {

            }

            @Override
            public void taskProgress(int percentage) {

            }
        });
    }
    
    public JTree getTree(){
        return folderTree;
    }
    
    public FolderTreeModel getModel(){
        return model;
    }
    
    public EscFolder getSelectedFolder(){
        if(folderTree.getSelectionPath()!=null && folderTree.getSelectionPath().getLastPathComponent() instanceof FolderTreeModel.FolderNode){
            return ((FolderTreeModel.FolderNode)folderTree.getSelectionPath().getLastPathComponent()).getFolder();
        } else {
            return null;
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        folderTree = new javax.swing.JTree();

        setLayout(new java.awt.BorderLayout());

        folderTree.setRowHeight(24);
        jScrollPane1.setViewportView(folderTree);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree folderTree;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

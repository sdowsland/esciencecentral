/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.api.api.async.tasks;

import com.connexience.api.misc.IProgressInfo;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscProject;
import java.io.File;

/**
 *
 * @author hugo
 */
public class UploadFileToFolderTask extends TransferTask implements IProgressInfo {
    EscFolder folder;
    private File localFile;
    private long total = 0;
    
    public UploadFileToFolderTask(EscFolder folder, File localFile) {
        this.localFile = localFile;
        this.folder = folder;
    }

    public File getLocalFile() {
        return localFile;
    }

    @Override
    public String getFileName() {
        return localFile.getName();
    }
    
    @Override
    public void run() {
        try {
            EscDocument serverDocument = getManager().getStorageClient().createDocumentInFolder(folder.getId(), localFile.getName());
            getManager().getStorageClient().upload(serverDocument, localFile, this);
            notifyCompleted();
        } catch (Exception e){
            getManager().displayError(e);
            notifyFailed(e);
        }
    }

    @Override
    public void reportBegin(long l) {
        total = l;
    }

    public EscFolder getFolder() {
        return folder;
    }

    
    @Override   
    public void reportProgress(long l) {
        notifyProgress(l, total);
    }

    @Override
    public void reportEnd(long l) {
    }    
}

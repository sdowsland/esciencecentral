
package com.connexience.client.common;

import com.connexience.api.model.EscDatasetItem;
import com.connexience.api.model.json.JSONArray;
import com.connexience.api.model.json.JSONObject;
import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.api.async.ApiTaskManager;
import com.connexience.api.api.async.tasks.GetDatasetItemSizeTask;
import com.connexience.api.api.async.tasks.QueryDatasetItemTask;
import java.util.ArrayList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Provides a table model for dataset item
 * @author Hugo
 */
public class DatasetItemTableModel implements TableModel {
    private ApiTaskManager manager;
    private EscDatasetItem item;
    private int startRow = 0;
    private int pageSize = 200;
    private int nameSearchSize = 100;
    private int itemSize = 0;
    
    private JSONArray resultSet = null;
    private ArrayList<String> columnNames = new ArrayList<>();
    private ArrayList<TableModelListener> listeners = new ArrayList<>();
    private ArrayList<Listener> modelListeners = new ArrayList<>();
    
    public DatasetItemTableModel(ApiTaskManager manager, EscDatasetItem item) {
        this.manager = manager;
        setItem(item);
    }
     
    public final void setItem(EscDatasetItem item){
        this.item = item;
        startRow = 0;
        pageSize = 200;
        resultSet = null;
        columnNames = new ArrayList<>();
        notifyDataChanged();
        notifyQueryPositionChanged();
    }
    
    public void fetchDetails(){
        if(item!=null){
            GetDatasetItemSizeTask task = new GetDatasetItemSizeTask(item);
            manager.performTask(task, new ApiTaskAdapter(){

                @Override
                public void taskCompleted(ApiTask task) {
                    itemSize = ((GetDatasetItemSizeTask)task).getSize();
                    nofifyDetailsFetched(itemSize);
                }

                @Override
                public void taskFailed(ApiTask task, Exception cause) {
                    itemSize = 0;
                    nofifyDetailsFetched(itemSize);
                }
            });
        }
    }
    
    public void performQuery(ApiTask.Callback callback) throws Exception {
        
        resultSet = null;
        columnNames = new ArrayList<>();
        javax.swing.SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                notifyDataChanged();
            }
        });
        
        
        QueryDatasetItemTask task = new QueryDatasetItemTask(item);
        task.setStartRow(startRow);
        task.setPageSize(pageSize);
        task.setNameSearchSize(nameSearchSize);
        task.addCallback(callback);
        
        
        manager.performTask(task, new ApiTaskAdapter(){

            @Override
            public void taskCompleted(ApiTask task) {
                columnNames = ((QueryDatasetItemTask)task).getColumnNames();
                resultSet = ((QueryDatasetItemTask)task).getResultSet();
                notifyQueryPositionChanged();
                notifyDataChanged();
                
            }

            @Override
            public void taskFailed(ApiTask task, Exception cause) {
                columnNames = new ArrayList<>();
                resultSet = null;
                notifyDataChanged();
            }
        });
        
    }

    public ArrayList<String> getColumnNames() {
        return columnNames;
    }

    public JSONArray getResultSet() {
        return resultSet;
    }
    
    private void notifyQueryPositionChanged(){
        if(resultSet!=null){
            for(Listener l : modelListeners){
                l.queryPositionChanged(startRow, startRow + resultSet.length(), pageSize);
            }
        }
    }
    
    private void nofifyDetailsFetched(int size){
        for(Listener l : modelListeners){
            l.detailsFetched(size);
        }
    }
    
    private void notifyDataChanged(){
        TableModelEvent evt = new TableModelEvent(this);
        for(TableModelListener l : listeners){
            l.tableChanged(evt);
        }
    }
    
    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    public int getColumnIndex(String columnName){
        if(resultSet!=null){
            return columnNames.indexOf(columnName);
        } else {
            return -1;
        }
    }
    
    @Override
    public int getRowCount() {
        if(resultSet==null){
            return 0;
        } else {
            return resultSet.length();
        }
    }

    @Override
    public int getColumnCount() {
        if(resultSet!=null && columnNames!=null){
            return columnNames.size();
        } else {
            return 0;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        if(resultSet!=null && columnNames!=null){
            return columnNames.get(columnIndex);
        } else {
            return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(resultSet!=null){
            String name = columnNames.get(columnIndex);
            JSONObject row = resultSet.getJSONObject(rowIndex);
            if(row.has(name)){
                return row.get(name).toString();
            } else {
                return "";
            }
        } else { 
            return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    public void addDatasetModelListener(Listener l){
        modelListeners.add(l);
    }
   
    public void removeDatasetModelListener(Listener l){
        modelListeners.remove(l); 
    }
    
    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    public int getItemSize() {
        return itemSize;
    }
   
    
    public interface Listener {
        public void queryPositionChanged(int startRow, int endRow, int pageSize);
        public void detailsFetched(int datasetSize);
    }
}

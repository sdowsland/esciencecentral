
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import java.util.ArrayList;

/**
 * This task deletes a set of objects from the server
 * @author Hugo
 */
public class DeleteObjectsTask extends TransferTask {
    private ArrayList<EscObject> objects;
    
    public DeleteObjectsTask() {
        objects = new ArrayList<>();
    }

    public void addObject(EscObject o){
        objects.add(o);
    }
    
    @Override
    public void run() {
        int count = 0;
        for(EscObject o : objects){
            try {
                if(o instanceof EscDocument){
                    getManager().getStorageClient().deleteDocument(o.getId());
                } else if(o instanceof EscFolder){
                    getManager().getStorageClient().deleteFolder(o.getId());
                }
            } catch (Exception e){
                logError("Error deleting object: " + e.getMessage());
            }
            count++;
            notifyProgress(count, objects.size());
        }
        notifyCompleted();
    }

    @Override
    public String getFileName() {
        if(objects.size()==1){
            return "Deleting: 1 object";
        } else{ 
            return "Deleting: " + objects.size() + " objects";
        }
    }
    
    
}

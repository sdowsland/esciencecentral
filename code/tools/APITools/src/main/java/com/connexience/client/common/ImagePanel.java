/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author hugo
 */
public class ImagePanel extends JPanel {

    private ImageIcon image = null;
    private Icon icon = null;
    
    public ImagePanel() {

    }

    public void setImage(ImageIcon image) {
        this.image = image;
        this.icon = null;
        repaint();
    }

    public void setIcon(Icon icon){
        this.icon = icon;
        this.image = null;
        repaint();
    }
    
    protected void paintComponent(Graphics g) {
        try {
            super.paintComponent(g);
            if(image!=null){
                g.drawImage(image.getImage(), 0, 0, null); 
            } else {
                BufferedImage image = new BufferedImage(24, 24, BufferedImage.TYPE_INT_RGB);
                image.getGraphics().setColor(Color.WHITE);
                image.getGraphics().fillRect(0, 0, 24, 24);
                icon.paintIcon(this, image.getGraphics(), 1, 4);
                
                g.drawImage(image, 0, 0, 32, 32, this);
                
            }
        } catch (Exception e) {
        }
    }

}

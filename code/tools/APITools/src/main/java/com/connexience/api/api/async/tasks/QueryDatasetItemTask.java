
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDatasetItem;
import com.connexience.api.model.json.JSONArray;
import com.connexience.api.model.json.JSONObject;
import com.connexience.api.api.async.ApiTask;
import java.util.ArrayList;

/**
 * This tasks performs a query on a DatasetItem
 * @author Hugo
 */
public class QueryDatasetItemTask extends ApiTask {
    private int startRow = 0;
    private int pageSize = 200;
    private JSONArray resultSet = new JSONArray();
    private int nameSearchSize = 100;
    private EscDatasetItem item;
    private ArrayList<String> columnNames = new ArrayList<>();

    public QueryDatasetItemTask(EscDatasetItem item) {
        this.item = item;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public JSONArray getResultSet() {
        return resultSet;
    }

    public ArrayList<String> getColumnNames() {
        return columnNames;
    }

    public void setNameSearchSize(int nameSearchSize) {
        this.nameSearchSize = nameSearchSize;
    }

    public int getNameSearchSize() {
        return nameSearchSize;
    }
    
    @Override
    public void run() {
        try {
            if(item.getItemType()==EscDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW){
                // Multi-row data
                resultSet = getManager().getDatasetClient().queryMultipleValueDatasetItemAsJson(item.getDatasetId(), item.getName(), startRow, pageSize);

                // search a number of rows to build a list of column names
                columnNames = buildColumnNames(resultSet, nameSearchSize);

            } else {
                // Single-row data
                JSONObject row = getManager().getDatasetClient().querySingleValueDatasetItemAsJson(item.getDatasetId(), item.getName());
                resultSet = new JSONArray();
                resultSet.put(row);

                ArrayList<String> namesList = new ArrayList<>();
                JSONArray names = row.names();
                for(int i=0;i<names.length();i++){
                    if(!names.get(i).toString().startsWith("_")){
                        if(!namesList.contains(names.get(i).toString())){
                            namesList.add(names.get(i).toString());
                        }
                    }
                }
                columnNames = namesList;
            }        

            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }
    
    public static ArrayList<String> buildColumnNames(JSONArray data, int searchSize) throws Exception {
        ArrayList<String> namesList = new ArrayList<>();
        int endPos;
        if(data.length()>searchSize){
            endPos = searchSize;
        } else {
            endPos = data.length();
        }

        JSONObject row;
        JSONArray names;
        String name;

        for(int i=0;i<endPos;i++){
            row = data.getJSONObject(i);
            names = row.names();
            for(int j=0;j<names.length();j++){
                name = names.getString(j);
                if(!name.startsWith("_")){
                    if(!namesList.contains(name)){
                        namesList.add(name);    
                    }
                }

            }
        }      
        return namesList;
    }
}

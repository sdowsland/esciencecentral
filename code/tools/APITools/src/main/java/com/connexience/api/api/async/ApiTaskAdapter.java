/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.api.api.async;

import com.connexience.api.api.async.ApiTask;

/**
 * Adapter to save code 
 * @author hugo
 */
public class ApiTaskAdapter implements ApiTask.Callback {

    @Override
    public void taskCompleted(ApiTask task) {
    }

    @Override
    public void taskFailed(ApiTask task, Exception cause) {
    }

    @Override
    public void taskProgress(int percentage) {
    }

    @Override
    public void taskStarted(ApiTask task) {
    }
}
package com.connexience.client;

import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.UIManager;

/**
 * Manages Icon loading for different DPI displays
 * @author hugo
 */
public class IconManager {
    public static String ICON_DIRECTORY = "/com/connexience/uploader/uiicons/16/";
    public static int ICON_SIZE = 16;
    public static boolean HIGH_DPI = false;
    static {
        IconManager.initialize();
    }
    
    public static void initialize(){
        int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
        
        if(dpi>=140){
            UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Arial", Font.PLAIN, 18));
            IconManager.ICON_DIRECTORY = "/com/connexience/uploader/uiicons/32/";
            IconManager.ICON_SIZE = 32;
            HIGH_DPI = true;
        }        
    }
    
    public static void forceLowDPI(){
        UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Arial", Font.PLAIN, 12));
        IconManager.ICON_DIRECTORY = "/com/connexience/uploader/uiicons/16/";
        IconManager.ICON_SIZE = 16;
        HIGH_DPI = false;        
    }
}

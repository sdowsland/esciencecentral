

package com.connexience.api.api.async.tasks;

import com.connexience.api.StorageClient;
import com.connexience.api.model.EscProject;
import com.connexience.api.api.async.ApiTask;

/**
 * Fetches a list of studies from the server
 * @author hugo
 */
public class ListStudiesTask extends ApiTask {
    private EscProject[] studies = new EscProject[0];
    
    @Override
    public void run() {
        try {
            StorageClient c = getManager().getStorageClient();
            studies = c.listProjects();
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscProject[] getStudies() {
        return studies;
    }
}
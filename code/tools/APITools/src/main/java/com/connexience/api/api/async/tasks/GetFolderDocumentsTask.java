package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import com.connexience.api.api.async.ApiTask;
import java.util.ArrayList;

/**
 * This task lists the documents in a folder
 * @author hugo
 */
public class GetFolderDocumentsTask extends ApiTask {
    EscFolder folder;
    ArrayList<EscObject> contents = new ArrayList<>();
    
    public GetFolderDocumentsTask(EscFolder folder) {
        this.folder = folder;
    }

    @Override
    public void run() {
        try {
            EscDocument[] docs = getManager().getStorageClient().folderDocuments(folder.getId());
            for(EscObject o : docs){
                contents.add(o);
            }
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public ArrayList<EscObject> getContents() {
        return contents;
    }

    public EscFolder getFolder() {
        return folder;
    }
    
    
}
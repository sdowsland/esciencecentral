package com.connexience.client.common;

import com.connexience.api.api.async.ApiTask;
import com.connexience.api.api.async.ApiTaskAdapter;
import com.connexience.api.api.async.ApiTaskManager;
import com.connexience.api.api.async.tasks.ListVersionsTask;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscDocumentVersion;
import javax.swing.DefaultListModel;

/**
 * Displays details of a document
 * @author hugo
 */
public class DocumentDetailsPanel extends javax.swing.JPanel {
    ApiTaskManager manager;
    EscDocument document;
    
    /**
     * Creates new form DocumentDetailsPanel
     */
    public DocumentDetailsPanel(ApiTaskManager manager) {
        initComponents();
        this.manager = manager;
    }

    public void setDocument(EscDocument document){
        this.document = document;
        displayDocumentDetails();
        fetchVersions();
    }
    
    private void displayDocumentDetails(){
        if(document!=null){
            
        } else {
            
        }
    }
    
    private void fetchVersions(){
        if(document!=null){
            final ListVersionsTask listTask = new ListVersionsTask(document);
            manager.performTask(listTask, new ApiTaskAdapter(){

                @Override
                public void taskCompleted(ApiTask task) {
                    EscDocumentVersion[] results = listTask.getVersions();
                    if(results.length>0){
                        DefaultListModel<EscDocumentVersion> model = new DefaultListModel<>();
                        for(EscDocumentVersion v : results){
                            model.addElement(v);
                        }
                        versionsList.setModel(model);
                    } else {
                        versionsList.setModel(new DefaultListModel());
                    }
                }

                @Override
                public void taskFailed(ApiTask task, Exception cause) {
                    versionsList.setModel(new DefaultListModel());
                }
            });
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        versionsList = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();

        setPreferredSize(new java.awt.Dimension(505, 400));
        setLayout(new java.awt.BorderLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Versions"));
        jPanel2.setLayout(new java.awt.BorderLayout());

        versionsList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(versionsList);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Details"));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 493, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        add(jPanel3, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList versionsList;
    // End of variables declaration//GEN-END:variables
}

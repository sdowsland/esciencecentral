
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.api.api.async.ApiTask;

/**
 * List the versions of a document
 * @author hugo
 */
public class ListVersionsTask extends ApiTask{
    EscDocumentVersion[] versions = new EscDocumentVersion[0];
    EscDocument doc;

    public ListVersionsTask(EscDocument doc) {
        this.doc = doc;
    }
    
    @Override
    public void run() {
        try {
            versions = getManager().getStorageClient().listDocumentVersions(doc.getId());
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public EscDocumentVersion[] getVersions() {
        return versions;
    }
}

package com.connexience.api.api.async.tasks;

import com.connexience.api.api.async.ApiTask;
import com.connexience.api.model.EscWorkflowService;
import java.util.ArrayList;

/**
 * Lists all of the workflow blocks the user has access to 
 * @author hugo
 */
public class ListWorkflowBlocksTask extends ApiTask {
    ArrayList<EscWorkflowService> services = new ArrayList<>();
    
    public void run(){
        try {
            EscWorkflowService[] results = getManager().getWorkflowClient().listSharedServices();
            services.clear();
            for(EscWorkflowService s : results){
                services.add(s);
            }
            notifyCompleted();
        } catch (Exception e){
            notifyFailed(e);
        }
    }

    public ArrayList<EscWorkflowService> getServices() {
        return services;
    }
}
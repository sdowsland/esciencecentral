
package com.connexience.api.api.async.tasks;

import com.connexience.api.misc.IProgressInfo;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscProject;
import com.connexience.api.api.async.ApiTaskManager;
import java.io.File;

/**
 * Uploads a file
 * @author hugo
 */
public class UploadFileToProjectTask extends TransferTask implements IProgressInfo {
    File localFile;
    EscProject project;
    private long total = 0;
    
    public UploadFileToProjectTask(EscProject project, File localFile) {
        this.localFile = localFile;
        this.project = project;
    }

    public File getLocalFile() {
        return localFile;
    }

    @Override
    public String getFileName() {
        return localFile.getName();
    }
    
    @Override
    public void run() {
        try {
            EscFolder targetFolder = getManager().getUploadFolder(project.getId());
            EscDocument serverDocument = getManager().getStorageClient().createDocumentInFolder(targetFolder.getId(), localFile.getName());
            getManager().getStorageClient().upload(serverDocument, localFile, this);
            Thread.sleep(2000);
            notifyCompleted();
        } catch (Exception e){
            getManager().displayError(e);
            notifyFailed(e);
        }
    }

    @Override
    public void reportBegin(long l) {
        total = l;
    }

    public EscProject getProject() {
        return project;
    }

    
    @Override   
    public void reportProgress(long l) {
        notifyProgress(l, total);
    }

    @Override
    public void reportEnd(long l) {
    }
}
package com.connexience.api.api.async.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import com.connexience.api.api.async.ApiTask;
import java.util.ArrayList;

/**
 * Change the parent folder for a set of objects
 * @author hugo
 */
public class ChangeParentFolderTask extends TransferTask {
    private ArrayList<EscObject> objects;
    private EscFolder newParentFolder;

    public ChangeParentFolderTask(EscFolder newParentFolder) {
        this.newParentFolder = newParentFolder;
        objects = new ArrayList<>();
    }
    
    public ChangeParentFolderTask(EscFolder newParentFolder, ArrayList<EscObject>objects) {
        this.objects = objects;
        this.newParentFolder = newParentFolder;
    }
    
    public ChangeParentFolderTask(EscFolder newParentFolder, EscObject[] objectsArray) {
        this.newParentFolder = newParentFolder;
        objects = new ArrayList<>();
        for(EscObject o : objectsArray){
            objects.add(o);
        }
    }    

    
    @Override
    public void run() {
        int count = 0;
        for(EscObject o : objects){
            if(!o.getContainerId().equals(newParentFolder.getId())){
                try {
                    if(o instanceof EscDocument){
                        o.setContainerId(newParentFolder.getId());
                        getManager().getStorageClient().updateDocument((EscDocument)o);
                    } else if(o instanceof EscFolder){
                        o.setContainerId(newParentFolder.getId());
                        getManager().getStorageClient().updateFolder((EscFolder)o);
                    }
                } catch (Exception e){
                    logError("Error moving object: " + o.getName() + ": " + e.getMessage());
                }
            }
            count++;
            notifyProgress(count, objects.size());
        }
        notifyCompleted();
    }

    @Override
    public String getFileName() {
        return "Moving: " + objects.size() + " objects";
    }
}

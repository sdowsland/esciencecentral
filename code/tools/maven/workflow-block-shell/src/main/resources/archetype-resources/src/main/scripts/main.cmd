@ECHO OFF

:: This is an example command-line script that implements an e-Science Central
:: workflow block.
::
:: Input, outputs and properties of the block are available under environment
:: variables named 'PROP__<PROPERTY_NAME>', 'INPUT__<INPUT_NAME>',
:: 'OUTPUT__<OUTPUT_NAME>'.
::
:: Note that all variable names are sanitized to make them valid in the batch
:: file. Thus, any characters that do not fit pattern [A-Z][a-z][0-9]_ will be
:: changed to underscore: '_'. For example, to access block property
:: 'My Property-1' you need to use variable %INPUT__My_Property_1%.
::
:: Properties may be of type: String, Integer, Long, Double, Boolean. String 
:: lists are currently not supported; please contact us if you need them.
::
:: Inputs of type:
::  -- data-wrapper (data tables) are passed as CSV files. The full path to the
::     input file is stored in variable %INPUT_<INPUT_NAME>%
::  -- file-wrapper (file lists) are a little more complicated to handle:
::      -- %INPUT__<INPUT_NAME>% -- is the first file on the list (for 
::         convenience)
::      -- %INPUT__<INPUT_NAME>_LIST[NNN]% -- is a simulated array variable with
::         all files from the list (cmd does not allow real arrays to be
::         defined, see examples below). You can set NNN to the file number you
::         want
::  -- other input types are not currently supported; please contact us if you
::     need any.
::
:: Outputs of type:
::  -- data-wrapper needs to be a CSV file with a header line. The full path
::     where to store the CSV file is indicated in %OUTPUT__<OUTPUT_NAME>%
::  -- file-wrapper needs to be a file which includes a list of files to return.
::     The output file path is indicated by the %OUTPUT__<OUTPUT_NAME>%
::     variable and each row in this file must include path to the file on the
::     list (see examples below).
::  -- again contact us if you need support for any other output type.
::
:: If your shell script refers to some external binary libraries, these can be
:: accessed with variables named 'DEP__<Library_Name>__<Command_Name>'.
:: There is also variable 'BLOCK_HOME' defined. It denotes the directory where
:: this block artifacts are located. You can use it to access any extra
:: resources or libraries that you added to your block.
::

::
:: Examples
::

:: This command will allow you to see the environment variables set by the
:: system. Simple run the block and you should see them in the output console
:: log.

SET

::
:: Example 1: Copy input CSV data from input x to output port y
::
copy %INPUT__x% %OUTPUT__y%

:: Example 2: Copy all files from input 'input-files' to the output list
::
:: Note that 'input-files' is an optional input, so first you need to check
:: whether it has been set. Note that delayed expansion is enabled by default
:: so referencing variables with !<variable name>! works fine.
::
IF NOT "%INPUT__input_files%" == "" (
  FOR /L %%G IN (1,1,%INPUT__input_files[#]%) DO (
    echo !INPUT__input_files[%%G]! >> %OUTPUT__output_files%
  )
)

::
:: Example 3: List the contents of the input file passed through input x to
:: the console log.
::
::type "%INPUT__x%"

::
:: Example 4: Search all input files (file-wrapper) for a user-provided pattern.
::
::FOR /L %%G IN (1,1,%INPUT__input_files[#]%) DO (
::    findstr "%PROP__MyProperty%" "!INPUT__input_files[%%G]!
::)

::
:: Example 5:  Output contents of the invocation directory through a
::      file-wrapper output.
::
::FOR /F %%G IN ('dir /B') DO (
::    echo %%G >> "%OUTPUT__output_files%"
::)

:: Example 6: Generate a file and return it via a file-wrapper port.
::      To make it working safely you need to create a temporary file name that
::      does not clash with any other name in the invocation directory. To get
::      such name you may use a helper function referenced by the 
::      FUNC__MkTempName variable. The output value is stored in the _MkTempName
::      variable.
::      Once you generate the data file, you need to add it to the output file
::      list (the last line in this example).
::
::call %FUNC__MkTempName%
::echo "My new file name is %_MkTempName%"
::echo "Generated test data" > %_MkTempName%
::echo %_MkTempName% >> %OUTPUT__output_files%

::
:: Example 7: Start command 'ACommand' from binary library 'MyDependency'.
:: Note that you need to add a suitable dependency library to
:: the dependencies.xml file.
::
:: %DEP__MyDependency__ACommand% /?

#!/bin/bash

# This is an example bash script that implements an e-Science Central workflow
# block.
#
# Input, outputs and properties of the block are available under environment
# variables named 'PROP__<Property_Name>', 'INPUT__<Input_Name>',
# 'OUTPUT__<Output_Name>'.
# If your shell script refers to some external binary libraries, these can be
# accessed with variables named 'DEP__<Library_Name>__<Command_Name>'.
# There is also variable 'BLOCK_HOME' defined. It denotes the directory where
# this block artifacts are located. You can use it access any extra resources
# or libraries you added to your block.
#
# Note that all variable names are sanitized to make them valid in bash.  Thus
# any characters that do not fit pattern [A-Z][a-z][0-9]_ will be changed to
# underscore: '_'.  For example, to access block property 'My Property-1' you
# need to use variable $INPUT__My_Property_1.  Alternatively, you can use
# function $(getProperty "My Property-1") which will do the sanitization for
# you.
#
#
# Inputs of type:
#  -- data-wrapper (data tables) are passed as CSV files. The full path to the
#     input file is stored in variable $INPUT__<Input_Name>,
#  -- file-wrapper (file lists) are passed as-is and you can refer to them
#     using:
#      -- $INPUT__<Input_Name> -- the first file on the list (for convenience)
#      -- $INPUT__<Input_Name>__LIST$ -- the array variable with all files from
#              the list (see examples below),
#  -- property-wrapper although they are not available directly, they set block
#     properties which you can access as usual -- using variables
#     $PROP__<Property_Name> or the getProperty function,
#  -- other input types are not currently supported; please contact us if you
#     need any.
#
# Outputs of type:
#  -- data-wrapper need to be CSV files with a header line. The full path of 
#     the output file is indicated in $OUTPUT__<OUTPUT_NAME>$
#  -- file-wrapper need to be a file which includes a list of files to return.
#     The output file path is indicated by the $OUTPUT__<OUTPUT_NAME>$ variable
#     and each row in this file must include path to the file on the list 
#     (see examples below)
#  -- property-wrapper need to be Java property files; see example below and
#     javadocs of java.util.Properties#load(Reader). Currently, only String
#     properties are supported. If you need to pass typed properties, please
#     contact the e-SC dev team.
#
# Properties may be of type: String, Integer, Long, Double, Boolean. String list
# are currently not supported; please contact us if you need them.
#

# This command will allow you to see the environment variables you can use to
# access the context of your block (inputs, properties, etc.). See below for
# more examples
set -o posix ; set

# Pass input CSV data from input x -> output y
#
cp $INPUT__x $OUTPUT__y

# Pass input files from input-files -> output-files. Literally, append file
# names from the input port to the file indicated by the output port variable.
# 
for f in "${INPUT__input_files__LIST[@]}" ; do
    echo "$f" >> $OUTPUT__output_files
done


#
# Example 1: List contents of the input file passed through the 'MyInput' block
# input (data-wrapper or the first file on the file-wrapper list).
#
# cat "$INPUT__MyInput"

#
# Example 2: Search input files (file-wrapper) for a user-provided pattern.
#
# for f in ${INPUT__MyInput__LIST[@]} ; do
#     grep "$PROP__MyPattern" "$f"
# done

#
# Example 3:  Output contents of the current directory through a file-wrapper
# output
#
# for f in $(ls *.txt) ; do
#     echo "$f" >> "$OUTPUT__MyOutput"
# done

#
# Example 4:  Working with the file-wrapper outputs you will need to create
# files that do not clash with any other in the current directory. You may
# use 'mktemp' command to generate files with random names. And then return them
# via the file-wrapper output file.
#
# OUTFILE=$( mktemp output-XXXX.txt )
# echo $OUTFILE >> $OUTPUT__MyFileWrapperOutput

#
# Example 5: Starting a command from a binary library which is declared
# as a dependency for your block
#
# $DEP__MyDependency__ACommand --help

#
# Example 6: Sending a property via a properties-wrapper output
#
#
# echo "MyProp = Property Value" >> $OUTPUT__MyPropertyWrapperOutput

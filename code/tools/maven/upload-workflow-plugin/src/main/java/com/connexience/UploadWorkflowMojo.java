/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import com.connexience.api.*;
import com.connexience.api.model.*;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


/**
 * Goal which upoads a workflow into e-Science Central
 *
 * @goal upload
 * @phase install
 */
public class UploadWorkflowMojo extends AbstractMojo
{
    /**
     * Location of the workflow files.
     *
     * @parameter property="upload.localWorkflowDirectory" default-value="${project.basedir}/src/main/resources" @required
     */
    private String localWorkflowDirectory;
    /**
     * Name of the artifact.
     *
     * @parameter property="project.artifactId" @required
     */
    private String artifactId;
    /**
     * Version of the artifact.
     *
     * @parameter property="project.version" @required
     */
    private String version;

    /**
     * @parameter property="upload.serverURL" default-value="http://localhost:8080"
     */
    private String serverURL;

    /**
     * Location of the upload servlet. This is added to the host and port
     * to give the upload url.
     *
     * @parameter property="upload.context" default-value="/workflow"
     */
    private String serverContext;

    /**
     * The username of the user uploading the workflow block
     *
     * @parameter property="upload.username" default-value=""
     */
    private String username;
    /**
     * The password of the user uploading the workflow block
     *
     * @parameter property="upload.password" default-value=""
     */
    private String password;

    /**
     * Should the upload plugin actually upload the block. This is used when
     * testing locally.
     *
     * @parameter property="upload.doUpload" default-value="true"
     */
    private String doUpload;
    /**
     * Should the upload plug-in make the uploaded item public. This should be
     * set to true for "production" blocks and libraries that everybody uses.
     * <p/>
     * If set to true  -- makes the item public
     * If set to false -- revokes all permissions from public user
     * If unset/empty  -- does not change permissions, i.e. a new item will be
     * private, whereas an updated item will retain its
     * current permissions.
     *
     * @parameter property="upload.makePublic" default-value=""
     */
    private String makePublic;

    /**
     * Should the plugin execute the workflows it uploads?
     * Should be set to "true" or "false"
     *
     * @parameter property="upload.executeWorkflows" default-value="false"
     */
    private String executeWorkflows;

    /**
     * Name of the mapping file in resources directory
     *
     * @parameter property="upload.mappingFilename" default-value="mapping.txt"
     */
    private String mappingFilename;

    //Map from filename to desired id for each workflow
    private ArrayList<MappingEntry> mappingList = new ArrayList<>();


    public UploadWorkflowMojo()
    { }


    public void execute() throws MojoExecutionException {
        if (!doUpload.equals("true")) {
            getLog().info("*** Skipping upload because doUpload is set to false in project pom.xml ***");
            return;
        }

        URI serverURI;
        try {
            serverURI = new URI(serverURL);
        } catch (URISyntaxException x) {
            throw new MojoExecutionException("Invalid serverURL", x);
        }

        boolean isSecure;
        int port;
        switch (serverURI.getScheme().toLowerCase()) {
            case "http":
                isSecure = false;
                port = 80;
                break;
            case "https":
                isSecure = true;
                port = 443;
                break;
            default:
                throw new MojoExecutionException("Invalid serverURL: Unsupported scheme: " + serverURI.getScheme());
        }

        if (serverURI.getPort() > 0) {
            port = serverURI.getPort();
        }

        WorkflowClient workflowApi = new WorkflowClient(serverURI.getHost(), port, isSecure, username, password);
        StorageClient storageApi = new StorageClient(serverURI.getHost(), port, isSecure, username, password);

        try {
            //read the mappings of ids
            File mappingFile = new File(localWorkflowDirectory, mappingFilename);
            readMappings(mappingFile);

            if (mappingList.size() == 0) {
                getLog().warn(String.format("*** Skipping upload. No mappings found in workflow mapping file: %s ***", mappingFile));
                return;
            }

            //authenticate the application - this checks that the application is valid on this server
            if (storageApi.getTimestamp() > 0) {
                //authenticate the user of the application - this enables users to give applications different privileges
                EscUser user = storageApi.currentUser();

                //for (Map.Entry<String, String> mappingEntry : namesMap.entrySet()) {
                for (MappingEntry entry : mappingList) {
                    File wfFile = new File(localWorkflowDirectory, entry.fileName);
                    if (!wfFile.exists()) {
                        getLog().warn(String.format("Invalid mapping for workflow file: '%s'. The file does not exist.", wfFile));
                        continue;
                    }

                    getLog().info("Starting to upload " + wfFile.getName());

                    //Try to set the id of the workflow if it's specified in the mapping file
                    String id = entry.blockId;
                    EscWorkflow workflow;
                    if (id != null && !id.isEmpty()) {
                        // There is an ID - is there an existing workflow
                        workflow = workflowApi.getWorkflow(id);

                        if (workflow == null) {
                            workflow = new EscWorkflow();
                        }
                    } else {
                        workflow = new EscWorkflow();
                    }
                    workflow.setName(wfFile.getName());
                    workflow.setContainerId(user.getWorkflowFolderId());
                    workflow.setDescription("Uploaded from Maven plugin");
                    workflow = workflowApi.saveWorkflow(workflow);

                    // Request an object id if there is one set in the pom
                    if (id != null && !id.isEmpty()) {
                        getLog().info("Requesting ID change: " + workflow.getId() + " -> " + id);
                        storageApi.changeObjectId(workflow.getId(), id);

                        getLog().info("Retrieving new object: " + id);
                        workflow = workflowApi.getWorkflow(id);
                    }

                    //try to set the external block id
                    String externalBlockName = entry.externalBlockId;
                    if (externalBlockName != null) {
                        try {
                            //Set the external block name via the workflow API
                            getLog().info("Setting ExternalBlockName to: " + externalBlockName);
                            workflowApi.enableWorkflowExternalDataSupport(workflow.getId(), externalBlockName);
                        } catch (Exception e) {
                            getLog().error("Unable to set the ExternalBlockName via the API.");
                        }
                    }

                    //upload the service
                    try (FileInputStream stream = new FileInputStream(wfFile)) {
                        storageApi.upload(workflow, stream, wfFile.length());
                    }
                    getLog().info("Uploaded " + wfFile + " to " + serverURI.getHost() + " in directory " + storageApi.getFolder(user.getWorkflowFolderId()).getName());

                    if ("true".equalsIgnoreCase(makePublic)) {
                        getLog().info("Making service Public readable");
                        EscPermission p = new EscPermission();
                        p.setPermissionType(EscPermission.READ);
                        p.setPrincipalId(storageApi.publicUser().getId());
                        storageApi.grantPermission(workflow.getId(), p);

                    } else if ("false".equalsIgnoreCase(makePublic)) {
                        getLog().warn("Revoking public permissions");
                        EscPermission p = new EscPermission();
                        p.setPermissionType(EscPermission.READ);
                        p.setPrincipalId(storageApi.publicUser().getId());
                        storageApi.revokePermission(workflow.getId(), p);
                    }

                    if ("true".equalsIgnoreCase(executeWorkflows)) {
                        workflowApi.executeWorkflow(workflow.getId());
                    }
                }
            }
        } catch (Exception e) {
            getLog().error(e);
        }
    }


    private void readMappings(File mappingFile)
    {
        if (mappingFile.exists()) {
            getLog().info("Reading mappings from " + mappingFile);
            try (LineNumberReader reader = new LineNumberReader(new FileReader(mappingFile))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    // Remove any whitespaces
                    line = line.trim();

                    // Skip empty and comment lines
                    if (line.length() == 0 || line.charAt(0) == '#') {
                        continue;
                    }

                    // Accept format: fileName = requestedId, [externalBlockId]
                    // Use the split limit to avoid parsing invalid lines quicker
                    String[] parts = line.split("=", 3);
                    if (parts.length == 2) {
                        String[] values = parts[1].split(",", 3);
                        if (values.length == 1) {
                            mappingList.add(new MappingEntry(parts[0].trim(), values[0].trim()));
                        } else if (values.length == 2) {
                            mappingList.add(new MappingEntry(parts[0].trim(), values[0].trim(), values[1].trim()));
                        } else {
                            getLog().error("Line " + reader.getLineNumber() + ": Unparsable value in mapping line: " + parts[1]);
                        }
                    } else {
                        getLog().error("Line " + reader.getLineNumber() + ": Unparsable mapping line: " + line);
                    }
                }
            } catch (Exception e) {
                getLog().error(e);
            }
        }
    }


    private static class MappingEntry
    {
        public String fileName;
        public String blockId;
        public String externalBlockId;

        public MappingEntry(String fileName, String blockId) {
            this.fileName = fileName;
            this.blockId = blockId;
        }

        public MappingEntry(String fileName, String blockId, String externalBlockId) {
            this.fileName = fileName;
            this.blockId = blockId;
            this.externalBlockId = externalBlockId;
        }
    }
}

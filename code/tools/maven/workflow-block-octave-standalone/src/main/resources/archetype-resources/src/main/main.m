# Entry point for users octave service.
# This file gets called once each time
# a chunk of data is passed through the
# service. It should therefore not modify
# the environment status. It is executed
# as a script and therefore can access and
# set any global variables that are needed.

# This displays the various variables in the
# workspace. It is just here to show how
# data is passed in to Octave and can
# be removed.
whos;

# Just copy the input numerical data to the output
y.numerical = x.numerical;

# To set file outputs, use:
# y.files = ['name1';'name2';...];

# To plot, make sure there is a gnuplot-bin
# dependency and use:
# plot(data);
# print -dpng graph.png;
# y.files=['graph.png'];

#
# To report progress from an ocatve block, use the following:
#
# disp(strcat(['escprogress:', num2str(percent)]));
#
# Where percent is a value 0-100 calculated by the
# code author.
#
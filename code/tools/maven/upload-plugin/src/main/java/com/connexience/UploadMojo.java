/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import com.connexience.api.*;
import com.connexience.api.model.*;
import com.connexience.server.model.workflow.XMLDataExtractor;
import com.connexience.server.model.workflow.LibraryXmlParser;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Goal which uploads a service or workflow into e-Science Central
 *
 * @goal upload 
 * @phase install
 */
public class UploadMojo extends AbstractMojo {

    /**
     * Type of object to upload - "service" or "workflow"
     *
     * @parameter property="upload.objectType" @required
     */
    private String objectType;

    /**
     * Location of the file.
     *
     * @parameter property="project.build.directory" @required
     */
    private File outputDirectory;
    /**
     * Name of the artifact.
     *
     * @parameter property="project.artifactId" @required
     */
    private String artifactId;
    /**
     * Name of the object
     *
     * @parameter property="project.name" @required
     */
    private String name;
    /**
     * Version of the artifact.
     *
     * @parameter property="project.version" @required
     */
    private String version;
    /**
     * Folder (fully qualified to place the service on the folder).
     *
     * @parameter property="upload.folderPath"
     */
    private String folderPath;

    /**
     * Category for the service
     *
     * @parameter property="upload.category" default-value="My Services"
     */
    private String category;

    /**
     * @parameter property="upload.serverURL" default-value="http://localhost:8080"
     */
    private String serverURL;

    /** 
     * Location of the upload servlet. This is added to the host and port 
     * to give the upload url.
     * 
     * @parameter property="upload.context" default-value="/workflow"
     */
    private String serverContext;

    /**
     * The username of the user uploading the workflow block
     *
     * @parameter property="upload.username" default-value=""
     */
    private String username;

    /**
     * The password of the user uploading the workflow block
     *
     * @parameter property="upload.password" default-value=""
     */
    private String password;
    /**
     * Requested ID for the uploaded block. If blank, this is auto generated
     *
     * @parameter property="upload.id" default-value=""
     */
    private String id;
    /**
     * Should the upload plugin actually upload the block. This is used when
     * testing locally. 
     * 
     * @parameter property="upload.doUpload" default-value="true"
     */
    private String doUpload;

    /**
     * Should the upload plug-in make the uploaded item public. This should be
     * set to true for "production" blocks and libraries that everybody uses.
     * 
     * If set to true  -- makes the item public
     * If set to false -- revokes all permissions from public user
     * If unset/empty  -- does not change permissions, i.e. a new item will be 
     *                    private, whereas an updated item will retain its 
     *                    current permissions.
     * 
     * @parameter property="upload.makePublic" default-value=""
     */
    private String makePublic;

    /**
     * Should the upload plug-in use chunked upload to store data.
     *
     * @parameter property="upload.useChunkedUploads" default-value="false";
     */
    private Boolean useChunkedUploads;


    public void execute() throws MojoExecutionException {
        if (doUpload.equals("true")) {
            URI serverURI;
            try {
                serverURI = new URI(serverURL);
            } catch (URISyntaxException x) {
                throw new MojoExecutionException("Invalid serverURL", x);
            }

            boolean isSecure;
            int port;
            switch (serverURI.getScheme().toLowerCase()) {
                case "http":
                    isSecure = false;
                    port = 80;
                    break;
                case "https":
                    isSecure = true;
                    port = 443;
                    break;
                default:
                    throw new MojoExecutionException("Invalid serverURL: Unsupported scheme: " + serverURI.getScheme());
            }

            if (serverURI.getPort() > 0) {
                port = serverURI.getPort();
            }

            WorkflowClient api = new WorkflowClient(serverURI.getHost(), port, isSecure, username, password);
            StorageClient storage = new StorageClient(serverURI.getHost(), port, isSecure, username, password);

            if (useChunkedUploads) {
                getLog().info("Uploading using chunks");
            }
            storage.setUploadChunkingEnabled(useChunkedUploads);
            try {
                getLog().info("Trying to access server: " + serverURI);
                //authenticate the application - this checks that the application is valid on this server
                if(storage.getTimestamp()>0){
                    //authenticate the user of the application - this enables users to give applications different priviledges
                    EscUser user = storage.currentUser();

                    String fileNameToUpload = outputDirectory + File.separator + artifactId + "-" + version + "-dist.zip";
                    File fileToUpload = new File(fileNameToUpload);
                    if(fileToUpload.exists()){
                        //Find the folder that we're going to put the service in
                        EscFolder f;
                        if (folderPath != null) {
                            f = getFolderForService(storage, user);
                        } else {
                            f = storage.homeFolder();
                        }
                        
                        if(f!=null){
                            getLog().info(f.getName());
                            f.setDescription("A description");
                            f = storage.updateFolder(f);
                        } else {
                            getLog().info("No folder");
                        }
                        
                        
                        // Do different things for services and libraries
                        if (objectType.equalsIgnoreCase("service")) {//Create and save the object
                            EscWorkflowService service = null;
                            if(id!=null && !id.isEmpty()){
                                // There is an ID - is there an existing service
                                service = api.getService(id);
                                if(service==null){
                                    service = new EscWorkflowService();
                                }
                            } else {
                                service = new EscWorkflowService();
                            }
                            service.setName(name);
                            service.setContainerId(f.getId());
                            service.setCategory(category);
                            service = api.saveService(service);

                            // Request an object id if there is one set in the pom
                            if (id != null && !id.isEmpty()) {
                                getLog().info("Requesting ID change: " + service.getId() + " -> " + id);
                                storage.changeObjectId(service.getId(), id);

                                getLog().info("Retrieving new object: " + id);
                                service = api.getService(id);
                            }

                            //upload the service
                            storage.upload(service, fileToUpload);
                            getLog().info("Uploaded " + fileToUpload + " to " + serverURI.getHost() + " in directory " + f.getName());

                            if ("true".equalsIgnoreCase(makePublic)) {
                                getLog().info("Making service Public readable");
                                EscPermission p = new EscPermission();
                                p.setPermissionType(EscPermission.READ);
                                EscUser publicUser = storage.publicUser();
                                p.setPrincipalId(publicUser.getId());
                                storage.grantPermission(service.getId(), p);

                            } else if ("false".equalsIgnoreCase(makePublic)) {
                                getLog().info("Revoking Public permissions to the service");
                                EscPermission p = new EscPermission();
                                p.setPermissionType(EscPermission.READ);
                                EscUser publicUser = storage.publicUser();
                                p.setPrincipalId(publicUser.getId());
                                storage.revokePermission(service.getId(), p);
                            }

                        } else if (objectType.equalsIgnoreCase("library")) {
                            // Try and extract the library.xml data
                            FileInputStream stream = new FileInputStream(fileToUpload);
                            XMLDataExtractor extractor = new XMLDataExtractor(new String[]{"library.xml"});
                            extractor.extractXmlData(stream);
                            stream.close();
                            
                            if(extractor.allDataPresent()){
                                getLog().info("Extracted library.xml file");
                                LibraryXmlParser parser = new LibraryXmlParser(extractor.getEntry("library.xml"));
                                parser.parse();
                                String libraryName = parser.getLibraryName();
                                getLog().info("Using library name: " + libraryName);
                                
                                // Is there a library with the same name
                                EscWorkflowLibrary library = api.getLibraryByName(libraryName);
                                if(library==null){
                                    //Create and save the object
                                    getLog().info("Library: " + libraryName + " does not exist. Creating a new one");
                                    library = new EscWorkflowLibrary();
                                    library.setName(libraryName);
                                    library.setLibraryName(libraryName);
                                    library.setContainerId(f.getId());
                                    library =  api.saveLibrary(library);
                                } else {
                                    getLog().info("Located existing version of library");
                                }

                                //upload the library
                                storage.upload(library, fileToUpload);
                                getLog().info("Uploaded " + fileToUpload + " to " + serverURI.getHost() + " in directory " + f.getName());

                                if ("true".equalsIgnoreCase(makePublic)) {
                                    getLog().info("Making library Public readable");
                                    EscUser publicUser = storage.publicUser();
                                    EscPermission p = new EscPermission();
                                    p.setPrincipalId(publicUser.getId());
                                    p.setPermissionType(EscPermission.READ);
                                    storage.grantPermission(library.getId(), p);
                                } else if ("false".equalsIgnoreCase(makePublic)) {
                                    getLog().info("Revoking Public permissions to the library");
                                    EscUser publicUser = storage.publicUser();
                                    EscPermission p = new EscPermission();
                                    p.setPrincipalId(publicUser.getId());
                                    p.setPermissionType(EscPermission.READ);
                                    storage.revokePermission(library.getId(), p);
                                }
                                
                            } else {
                                getLog().error("Library archive does not contain a library.xml file");
                            }
                        } else {
                            getLog().error("Cannot upload object of type: " + objectType);
                        }
                    } else {
                        getLog().info("Block Jar file does not exist. Skipping upload");
                    }
                }
            } catch (Exception e) {
                System.err.println("Exception when accessing server: " + serverURI);
                e.printStackTrace();
            }
        } else {
            getLog().info("*** Skipping Upload because doUpload is set to false in project pom.xml ***");
        }
    }

    /*
     * Get the folder that a service will be uploaded into. Will be the
     * folerPath if it exists in full and the home folder if not
     */
    private EscFolder getFolderForService(StorageClient api, EscUser user) throws Exception {
        EscFolder homeFolder = api.homeFolder();
        EscFolder serviceFolder = homeFolder;
        
        if (homeFolder != null) {
            //remove a starting /
            if (folderPath.subSequence(0, 1).equals("/")) {
                folderPath = folderPath.substring(1);
            }

            //try to find each folder in the path
            String[] path = folderPath.split("/");
            for (String folder : path) {
                serviceFolder = findFolder(api, serviceFolder, folder);
                if (serviceFolder == null) {
                    break;
                }
            }

            //if the path doesn't exist upload to the home folder
            if (serviceFolder == null) {
                serviceFolder = homeFolder;
                getLog().info("Cannot find directory: " + folderPath + " Uploading to home directory instead ");
            }
            
            return serviceFolder;
        } else {
            return null;
        }
    }

    /*
     * Find a subFolder based on name rather than Id
     */
    private EscFolder findFolder(StorageClient api, EscFolder parentFolder, String name) throws Exception {
        EscFolder[] contents = api.listChildFolders(parentFolder.getId());
        for (EscFolder f : contents) {
            if (f.getName().equalsIgnoreCase(name)) {
                return f;
            }
        }
        return null;
    }
}

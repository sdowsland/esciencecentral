#!/bin/sh

for arg in `ls -d */`
do
	cd ${arg}
	echo `pwd`
	mvn clean install
	cd ..
done
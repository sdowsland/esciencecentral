/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.rmi;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Link;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.api.impl.RMIService;
import com.connexience.server.workflow.types.WorkflowFilesystemScanner;
import com.connexience.server.workflow.types.WorkflowProject;
import com.connexience.server.workflow.types.WorkflowStudy;
import com.connexience.server.workflow.types.WorkflowStudyDeployment;
import com.connexience.server.workflow.types.WorkflowStudyLogger;
import com.connexience.server.workflow.types.WorkflowStudyObject;
import com.connexience.server.workflow.types.WorkflowStudyPhase;
import com.connexience.server.workflow.types.WorkflowStudySubject;
import com.connexience.server.workflow.types.WorkflowStudySubjectGroup;
import com.connexience.server.workflow.util.WorkflowManagerHelper;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.*;
import org.pipeline.core.data.Data;
import org.pipeline.core.xmlstorage.XmlDataStore;


/**
 * This class provides an RMI server that can manage workflow engines
 * @author hugo
 */
public class RMIServiceImpl extends UnicastRemoteObject implements RMIService {
    /**
     * Class version UID.
     * 
     * Please increment this value whenever your changes may cause 
     * incompatibility with the previous version of this class. If unsure, ask 
     * one of the core development team or read:
     *   http://docs.oracle.com/javase/6/docs/api/java/io/Serializable.html
     * and
     *   http://docs.oracle.com/javase/6/docs/platform/serialization/spec/version.html#6678
     */
    private static final long serialVersionUID = 2L;

    private static Logger logger = Logger.getLogger(RMIServiceImpl.class);
    private WorkflowManagerHelper helper;
    public RMIServiceImpl() throws RemoteException {
        try {
            Ticket ticket = EJBLocator.lookupTicketBean().createPublicWebTicket();
            helper = new WorkflowManagerHelper(ticket);
        } catch (ConnexienceException ce){
            throw new RemoteException("Error creating public ticket: " + ce.getMessage());
        }
    }
    
    public RMIServiceImpl(Ticket ticket) throws RemoteException {
        helper = new WorkflowManagerHelper(ticket);
    }
    
    @Override
    public void terminate(){
        try {
            UnicastRemoteObject.unexportObject(this, true);
        } catch(NoSuchObjectException nsoe){
            logger.error("Cannot remove RMI object: " + nsoe.getMessage());
        }
    }

    @Override
    public Ticket authenticate(String username, String password) throws ConnexienceException, RemoteException {
        return helper.authenticate(username, password);
    }

    @Override
    public int changeObjectId(String originalId, String requestedId) throws ConnexienceException, RemoteException {
        return helper.changeObjectId(originalId, requestedId);
    }

    @Override
    public DocumentVersion createNextVersion(String documentId) throws ConnexienceException, RemoteException {
        return helper.createNextVersion(documentId);
    }

    @Override
    public WorkflowLock createWorkflowLock(String invocationId, String contextId, boolean allowFailedSubworkflows, boolean pauseOnFailures) throws ConnexienceException, RemoteException {
        return helper.createWorkflowLock(invocationId, contextId, allowFailedSubworkflows, pauseOnFailures);
    }

    @Override
    public void attachInvocationToLock(String invocationId, long lockId) throws ConnexienceException, RemoteException {
        helper.attachInvocationToLock(invocationId, lockId);
    }

    @Override
    public void deleteFolder(String folderId) throws ConnexienceException, RemoteException {
        helper.deleteFolder(folderId);
    }

    @Override
    public void deleteDocument(String documentId) throws ConnexienceException, RemoteException {
        helper.deleteDocument(documentId);
    }

    @Override
    public WorkflowInvocationFolder executeWorkflow(String workflowId, WorkflowParameterList parameters,String parentInvocationId,  long lockId, String folderName) throws ConnexienceException, RemoteException {
        return helper.executeWorkflow(workflowId, parameters, parentInvocationId, lockId, folderName);
    }

    @Override
    public DocumentRecord[] getChildDocuments(String folderId) throws ConnexienceException, RemoteException {
        return helper.getChildDocuments(folderId);
    }

    @Override
    public Folder[] getChildFolders(String folderId) throws ConnexienceException, RemoteException {
        return helper.getChildFolders(folderId);
    }

    @Override
    public DocumentRecord getDocument(String id) throws ConnexienceException, RemoteException {
        return helper.getDocument(id);
    }

    @Override
    public DocumentVersion getDocumentVersion(String documentId, int versionNumber) throws ConnexienceException, RemoteException {
        return helper.getDocumentVersion(documentId, versionNumber);
    }

    @Override
    public DocumentVersion[] getDocumentVersions(String documentId) throws ConnexienceException, RemoteException {
        return helper.getDocumentVersions(documentId);
    }

    @Override
    public DynamicWorkflowLibrary getDynamicWorkflowLibraryByName(String libraryName) throws ConnexienceException, RemoteException {
        return helper.getDynamicWorkflowLibraryByName(libraryName);
    }

    @Override
    public DynamicWorkflowService getDynamicWorkflowService(String id) throws ConnexienceException, RemoteException {
        return helper.getDynamicWorkflowService(id);
    }

    @Override
    public Folder getFolder(String folderId) throws ConnexienceException, RemoteException {
        return helper.getFolder(folderId);
    }

    @Override
    public Folder getHomeFolder(String userId) throws ConnexienceException, RemoteException {
        return helper.getHomeFolder(userId);
    }

    @Override
    public DocumentVersion getLatestVersion(String documentId) throws ConnexienceException, RemoteException {
        return helper.getLatestVersion(documentId);
    }

    @Override
    public String getLatestVersionId(String documentId) throws ConnexienceException, RemoteException {
        return helper.getLatestVersionId(documentId);
    }

    @Override
    public MetadataCollection getMetadata(String objectId) throws ConnexienceException, RemoteException {
        return helper.getMetadata(objectId);
    }

    @Override
    public Folder getNamedSubdirectory(String id, String name) throws ConnexienceException, RemoteException {
        return helper.getNamedSubdirectory(id, name);
    }

    @Override
    public DocumentRecord getOrCreateDocumentRecord(String parentFolderId, String name) throws ConnexienceException, RemoteException {
        return helper.getOrCreateDocumentRecord(parentFolderId, name);
    }

    @Override
    public User getPublicUser() throws ConnexienceException, RemoteException {
        return helper.getPublicUser();
    }

    @Override
    public Date getServerTime() throws ConnexienceException, RemoteException {
        return helper.getServerTime();
    }

    @Override
    public String getServiceXml(String serviceId) throws ConnexienceException, RemoteException {
        return helper.getServiceXml(serviceId);
    }

    @Override
    public String getServiceXml(String serviceId, String versionId) throws ConnexienceException, RemoteException {
        return helper.getServiceXml(serviceId, versionId);
    }

    @Override
    public User getUser(String userId) throws ConnexienceException, RemoteException {
        return helper.getUser(userId);
    }

    @Override
    public WorkflowDocument getWorkflow(String id) throws ConnexienceException, RemoteException {
        return helper.getWorkflow(id);
    }

    @Override
    public WorkflowInvocationFolder getWorkflowInvocation(String invocationId) throws ConnexienceException, RemoteException {
        return helper.getWorkflowInvocation(invocationId);
    }

    @Override
    public List<WorkflowInvocationFolder> listWorkflowInvocations(String workflowId) throws ConnexienceException, RemoteException {
        return helper.listWorkflowInvocations(workflowId);
    }

    @Override
    public void grantObjectPermission(String objectId, String principalId, String permission) throws ConnexienceException, RemoteException {
        helper.grantObjectPermission(objectId, principalId, permission);
    }

    @Override
    public WorkflowDocument[] listWorkflows() throws ConnexienceException, RemoteException {
        return helper.listWorkflows();
    }

    @Override
    public DataStore loadDataStore() throws ConnexienceException, RemoteException {
        return helper.loadDataStore();
    }

    @Override
    public void logWorkflowComplete(String invocationId, String status) throws ConnexienceException, RemoteException {
        helper.logWorkflowComplete(invocationId, status);
    }

    @Override
    public void logWorkflowDequeued(String invocationId) throws ConnexienceException, RemoteException {
        helper.logWorkflowDequeued(invocationId);
    }

    @Override
    public void logWorkflowExecutionStarted(String invocationId) throws ConnexienceException, RemoteException {
        helper.logWorkflowExecutionStarted(invocationId);
    }

    @Override
    public void notifyEngineShutdown(String hostId) throws ConnexienceException, RemoteException {
        helper.notifyEngineShutdown(hostId);
    }

    @Override
    public void notifyEngineStartup(String hostId) throws ConnexienceException, RemoteException {
        helper.notifyEngineStartup(hostId);
    }

    @Override
    public JSONContainer queryDatasetItem(String datasetId, String itemName) throws ConnexienceException, RemoteException {
        return helper.queryDatasetItem(datasetId, itemName);
    }

    @Override
    public JSONContainer queryDatasetItem(DatasetQuery query) throws ConnexienceException, RemoteException {
        return helper.queryDatasetItem(query);
    }
    
    @Override
    public void refreshLockStatus(long lockId) throws ConnexienceException, RemoteException {
        helper.refreshLockStatus(lockId);
    }

    @Override
    public void removeWorkflowLock(long lockId) throws ConnexienceException, RemoteException {
        helper.removeWorkflowLock(lockId);
    }

    @Override
    public void resetDataset(String datasetId) throws ConnexienceException, RemoteException {
        helper.resetDataset(datasetId);
    }

    @Override
    public DocumentRecord saveDocument(String parentId, DocumentRecord doc) throws ConnexienceException, RemoteException {
        return helper.saveDocument(parentId, doc);
    }

    @Override
    public DynamicWorkflowLibrary saveDynamicWorkflowLibrary(DynamicWorkflowLibrary library) throws ConnexienceException, RemoteException {
        return helper.saveDynamicWorkflowLibrary(library);
    }

    @Override
    public DynamicWorkflowService saveDynamicWorkflowService(DynamicWorkflowService service) throws ConnexienceException, RemoteException {
        return helper.saveDynamicWorkflowService(service);
    }

    @Override
    public Folder saveFolder(Folder f) throws ConnexienceException, RemoteException {
        return helper.saveFolder(f);
    }

    @Override
    public WorkflowInvocationFolder saveWorkflowInvocation(WorkflowInvocationFolder invocation) throws ConnexienceException, RemoteException {
        return helper.saveWorkflowInvocation(invocation);
    }

    @Override
    public void setCurrentBlock(String invocationId, String contextId, int percentComplete) throws ConnexienceException, RemoteException {
        helper.setCurrentBlock(invocationId, contextId, percentComplete);
    }

    @Override
    public void setCurrentBlockStreamingProcess(String invocationId, String contextId, long totalBytesToStream, long bytesStreamed) throws ConnexienceException, RemoteException {
        helper.setCurrentBlockStreamingProcess(invocationId, contextId, totalBytesToStream, bytesStreamed);
    }

    @Override
    public void setInvocationEngineId(String invocationId, String engineId) throws ConnexienceException, RemoteException {
        helper.setInvocationEngineId(invocationId, engineId);
    }

    @Override
    public void setWorkflowLockStatus(long lockId, String status) throws ConnexienceException, RemoteException {
        helper.setWorkflowLockStatus(lockId, status);
    }

    @Override
    public void setWorkflowStatus(String invocationId, int status, String message) throws ConnexienceException, RemoteException {
        helper.setWorkflowStatus(invocationId, status, message); 
    }

    @Override
    public void updateDatasetItemWithJson(String datasetId, String itemName, long rowId, JSONContainer data) throws ConnexienceException, RemoteException {
        helper.updateDatasetItemWithJson(datasetId, itemName, rowId, data);
    }
    
    @Override
    public void updateDatasetItemWithJson(String datasetId, String itemName, JSONContainer data) throws ConnexienceException, RemoteException {
        helper.updateDatasetItemWithJson(datasetId, itemName, data);
    }

    @Override
    public void updateDatasetItemWithData(String datasetId, String itemName, Data data) throws ConnexienceException, RemoteException{
        helper.updateDatasetItemWithData(datasetId, itemName, data);
    }
    
    

    @Override
    public void updateDatasetItemWithNumber(String datasetId, String itemName, Number data) throws ConnexienceException, RemoteException {
        helper.updateDatasetItemWithNumber(datasetId, itemName, data);      
    }

    @Override
    public DocumentVersion updateDocumentVersion(DocumentVersion version) throws ConnexienceException, RemoteException {
        return helper.updateDocumentVersion(version);
    }

    @Override
    public void updateServiceLog(String invocationId, String contextId, String outputData, String statusText, String statusMessage) throws ConnexienceException, RemoteException {
        helper.updateServiceLog(invocationId, contextId, outputData, statusText, statusMessage);
    }

    @Override
    public void updateServiceLogMessage(String invocationId, String contextId, String statusText, String statusMessage) throws ConnexienceException, RemoteException {
        helper.updateServiceLogMessage(invocationId, contextId, statusText, statusMessage);
    }

    @Override
    public void uploadMetadata(String objectId, MetadataCollection metaData) throws ConnexienceException, RemoteException {
        helper.uploadMetadata(objectId, metaData);
    }

    @Override
    public Ticket getTicket() throws RemoteException {
        return helper.getTicket();
    }

    @Override
    public Link addDocumentLink(String sourceDocumentId, String targetDocumentId) throws ConnexienceException, RemoteException {
        return helper.addDocumentLink(sourceDocumentId, targetDocumentId);
    }

    @Override
    public DocumentRecord[] getDocumentLinks(String id) throws ConnexienceException, RemoteException {
        return helper.getDocumentLinks(id);
    }
    
    @Override
    public void workflowTerminatedByEngine(String invocationId) throws ConnexienceException, RemoteException {
        helper.workflowTerminatedByEngine(invocationId);
    }    

    @Override
    public WorkflowLock getWorkflowLock(long lockId) throws ConnexienceException, RemoteException {
        return helper.getWorkflowLock(lockId);
    }

    @Override
    public Folder getDeploymentFolder(Integer deploymentId) throws ConnexienceException, RemoteException {
        return helper.getDeploymentFolder(deploymentId);
    }

    @Override
    public Integer getDeploymentId(String studyCode, String phase, String loggerSerialNumber) throws ConnexienceException, RemoteException {
        return helper.getDeploymentId(studyCode, phase, loggerSerialNumber);
    }

    @Override
    public void addDataToDeployment(Integer deploymentId, String documentRecordId) throws ConnexienceException, RemoteException {
        helper.addDataToDeployment(deploymentId, documentRecordId);
    }

    @Override
    public Dataset saveDataset(Dataset ds) throws ConnexienceException, RemoteException {
        return helper.saveDataset(ds);
    }

    @Override
    public DatasetItem saveDatasetItem(DatasetItem item) throws ConnexienceException, RemoteException {
        return helper.saveDatasetItem(item);
    }

    @Override
    public User createAccount(String firstName, String surname, String logon, String password) throws ConnexienceException, RemoteException {
        return helper.createAccount(firstName, surname, logon, password);
    }

    @Override
    public Group createGroup(String groupName, boolean ownerApprovalRequired, boolean nonMembersView) throws ConnexienceException, RemoteException {
        return helper.createGroup(groupName, ownerApprovalRequired, nonMembersView);
    }
    
    @Override
    public Ticket createTicketForUser(String userId) throws ConnexienceException, RemoteException {
        return helper.createTicketForUser(userId);
    }

    @Override
    public DatasetItem lookupUserDatasetItemByName(String datasetName, String itemName) throws ConnexienceException, RemoteException {
        return helper.lookupUserDatasetItemByName(datasetName, itemName);
    }

    @Override
    public DatasetItem[] listDatasetItems(String datasetId) throws ConnexienceException, RemoteException {
        return helper.listDatasetItems(datasetId);
    }

    @Override
    public Dataset lookupUserDatasetByName(String datasetName) throws ConnexienceException, RemoteException {
        return helper.lookupUserDatasetByName(datasetName);
    }

    @Override
    public boolean userHasNamedDataset(String datasetName) throws ConnexienceException, RemoteException {
        return helper.userHasNamedDataset(datasetName);
    }
    
    @Override
    public Group getGroupByName(String name) throws ConnexienceException, RemoteException {
        return helper.getGroupByName(name);
    }
    
    @Override
    public void addUserToGroup(String groupId, String userId) throws ConnexienceException, RemoteException {
        helper.addUserToGroup(groupId, userId);
    }
    
    @Override
    public void removeUserFromGroup(String groupId, String userId) throws ConnexienceException, RemoteException {
        helper.removeUserFromGroup(groupId, userId);
    }

    @Override
    public ServerObject getServerObject(String objectId) throws ConnexienceException, RemoteException {
        return helper.getServerObject(objectId);
    }

    @Override
    public WorkflowProject getProject(int projectId) throws ConnexienceException, RemoteException {
        return helper.getProject(projectId);
    }

    @Override
    public WorkflowFilesystemScanner getScanner(long scannerId) throws ConnexienceException, RemoteException {
        return helper.getScanner(scannerId);
    }

    @Override
    public XmlDataStore getAllServerProperties() throws ConnexienceException, RemoteException {
        return helper.getAllServerProperties();
    }

    @Override
    public long lookupDatasetItemId(String datasetId, String itemName) throws ConnexienceException, RemoteException {
        return helper.lookupDatasetItemId(datasetId, itemName);
    }

    @Override
    public HashMap getStudyStats(int studyId) throws ConnexienceException, RemoteException {
        return helper.getStudyStats(studyId);
    }

    @Override
    public WorkflowStudy getStudy(int studyId) throws ConnexienceException, RemoteException {
        return helper.getStudy(studyId);
    }

    @Override
    public WorkflowStudyPhase[] listStudyPhases(int studyId) throws ConnexienceException, RemoteException {
        return helper.listStudyPhases(studyId);
    }

    @Override
    public WorkflowStudySubjectGroup[] listPhaseGroups(int phaseId) throws ConnexienceException, RemoteException {
        return helper.listPhaseGroups(phaseId);
    }

    @Override
    public WorkflowStudySubjectGroup[] listChildGroups(int parentGroupId) throws ConnexienceException, RemoteException {
        return helper.listChildGroups(parentGroupId);
    }

    @Override
    public WorkflowStudySubject[] listGroupSubjects(int parentGroupId) throws ConnexienceException, RemoteException {
        return helper.listGroupSubjects(parentGroupId);
    }

    @Override
    public WorkflowStudyDeployment[] listGroupDeployments(int parentGroupId) throws ConnexienceException, RemoteException {
        return helper.listGroupDeployments(parentGroupId);
    }

    @Override
    public WorkflowStudyLogger getLogger(int loggerId) throws ConnexienceException, RemoteException {
        return helper.getLogger(loggerId);
    }

    @Override
    public WorkflowStudyObject getStudyObjectAssociatedWithFolder(String folderId) throws ConnexienceException, RemoteException {
        return helper.getStudyObjectAssociatedWithFolder(folderId);
    }

    @Override
    public void updateStudyObjectProperties(WorkflowStudyObject object) throws ConnexienceException, RemoteException {
        helper.updateStudyObjectProperties(object);
    }
    
    
}

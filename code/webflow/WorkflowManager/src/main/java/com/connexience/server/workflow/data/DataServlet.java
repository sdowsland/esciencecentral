/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.data;

import com.connexience.server.ConnexienceException;
import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.provenance.model.logging.events.LibrarySaveOperation;
import com.connexience.provenance.model.logging.events.ServiceSaveOperation;
import com.connexience.provenance.model.logging.events.WorkflowSaveOperation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.util.WorkflowPreviewUploader;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author hugo
 */
public class DataServlet extends HttpServlet {
    /** Data store */
    private DataStore dataStore;
    
    /** Object mapper for this service */
    private ObjectMapper mapper;

    public DataServlet() {
        mapper = new ObjectMapper();
        mapper.enableDefaultTyping();        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String documentId = request.getHeader("documentid");
        String versionId = request.getHeader("versionid");  
        
        try {
            DocumentRecord doc = new DocumentRecord();
            doc.setId(documentId);
            doc.setOrganisationId(getDataStore().getOrganisationId());
            DocumentVersion version = new DocumentVersion();
            version.setId(versionId);
            version.setDocumentRecordId(documentId);        
            response.setHeader(HttpHeaders.CONTENT_LENGTH, Long.toString(getDataStore().getRecordSize(doc, version)));
            getDataStore().writeToStream(doc, version, response.getOutputStream());
            
        } catch (Exception e){
            if(!response.isCommitted()){
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error getting document: " + e.getMessage());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String documentId = request.getHeader("documentid");
        String versionId = request.getHeader("versionid");  
        String className = request.getHeader("classname");
        String userId = request.getHeader("userid");
        
        if(className.endsWith("DynamicWorkflowService")){
            // Uploading a service
            if(userId!=null){
                try {
                    Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    DocumentVersion v = StorageUtils.upload(ticket, request.getInputStream(), request.getContentLength(), doc, "Uploaded service");
                    WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceXml(ticket, documentId, v.getId());

                    //log that the library has been saved in the graphDb
                    DynamicWorkflowService service = (DynamicWorkflowService) doc;
                    DocumentVersion serviceVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, service.getId());
                    ServiceSaveOperation saveOp = new ServiceSaveOperation(service.getId(), serviceVersion.getId(),serviceVersion.getVersionNumber(), service.getName(),  ticket.getUserId(), new Date(System.currentTimeMillis()));
                    saveOp.setProjectId(ticket.getDefaultProjectId());
                    ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                    provClient.log(saveOp);                       
                    
                    // Return the version object 
                    mapper.writeValue(response.getOutputStream(), v);
                    
                } catch (Exception e){
                    if(!response.isCommitted()){
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error uploading: " + e.getMessage());
                    }                    
                }
            } else {
                if(!response.isCommitted()){
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "No user id present");
                }
            }
         
        } else if(className.endsWith("DynamicWorkflowLibrary")){
            // Uploading a library
            if(userId!=null){
                try {
                    Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    DocumentVersion v = StorageUtils.upload(ticket, request.getInputStream(), request.getContentLength(), doc, "Uploaded library");
                    WorkflowEJBLocator.lookupWorkflowManagementBean().updateLibraryXml(ticket, documentId, v.getId());

                    //log that the library has been saved in the graphDb
                    DynamicWorkflowLibrary library = (DynamicWorkflowLibrary) doc;
                    DocumentVersion libVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, library.getId());
                    LibrarySaveOperation saveOp = new LibrarySaveOperation(library.getId(), libVersion.getId(), library.getName(), libVersion.getVersionNumber(), ticket.getUserId(), new Date(System.currentTimeMillis()));
                    saveOp.setProjectId(ticket.getDefaultProjectId());
                    ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                    provClient.log(saveOp);                    
                    
                    // Return the version object 
                    mapper.writeValue(response.getOutputStream(), v);
                    
                } catch (Exception e){
                    if(!response.isCommitted()){
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error uploading: " + e.getMessage());
                    }                    
                }
            } else {
                if(!response.isCommitted()){
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "No user id present");
                }
            }            
        } else if(className.endsWith("WorkflowDocument")){
            // Uploading a workflow  
            if(userId!=null){
                try {
                    // Save data
                    Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    DocumentVersion v = StorageUtils.upload(ticket, request.getInputStream(), request.getContentLength(), doc, "Uploaded from workflow");
                    
                    WorkflowSaveOperation op = new WorkflowSaveOperation();
                    op.setName(doc.getName());
                    op.setTimestamp(new Date());
                    op.setUserId(ticket.getUserId());
                    op.setVersionId(v.getId());
                    op.setVersionNum(v.getVersionNumber());
                    op.setWorkflowId(doc.getId());
                    op.setProjectId(ticket.getDefaultProjectId());
                    ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                    provClient.log(op);    
                    
                    // Process the workflow preview
                    try {
                        WorkflowPreviewUploader preview = new WorkflowPreviewUploader(ticket, documentId, versionId);
                        preview.generatePreview();
                    } catch (ConnexienceException ce){
                        log("Error creating workflow preview image: " + ce.getMessage());
                    }
                    
                    // Send the version back
                    mapper.writeValue(response.getOutputStream(), v);

                } catch (Exception e){
                    if(!response.isCommitted()){
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error uploading: " + e.getMessage());
                    }                        
                }
            } else {
                if(!response.isCommitted()){
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "No user id present");
                }
            }
            
        } else {
            // Plain upload
            if(versionId!=null){
                // Already have a version assigned - direct save to storage driver
                try {
                    DocumentRecord doc = new DocumentRecord();
                    doc.setId(documentId);
                    doc.setOrganisationId(getDataStore().getOrganisationId());
                    DocumentVersion version = new DocumentVersion();
                    version.setId(versionId);
                    version.setDocumentRecordId(documentId);
                    version = getDataStore().readFromStream(doc, version, request.getInputStream(), request.getContentLength());
                    mapper.writeValue(response.getOutputStream(), version);
                } catch (Exception e){
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error getting document: " + e.getMessage());
                }        
                
            } else {
                // Need to do some extra work
                if(userId!=null){
                    try {
                        Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                        DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                        DocumentVersion v = StorageUtils.upload(ticket, request.getInputStream(), request.getContentLength(), doc, "Uploaded from workflow");
                        mapper.writeValue(response.getOutputStream(), v);
                        
                    } catch (Exception e){
                        if(!response.isCommitted()){
                            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error uploading: " + e.getMessage());
                        }                        
                    }
                } else {
                    if(!response.isCommitted()){
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "No user id present");
                    }
                }
                    
            }

        }
    }

    /** Get the data store */
    public DataStore getDataStore() throws ConnexienceException {
        if(dataStore==null){
            Ticket publicTicket = EJBLocator.lookupTicketBean().createPublicWebTicket();
            dataStore = EJBLocator.lookupStorageBean().getOrganisationDataStore(publicTicket, publicTicket.getOrganisationId());
            return dataStore;
        } else {
            return dataStore;
        }
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Data transfer servlet for workflow engine";
    }
    
}

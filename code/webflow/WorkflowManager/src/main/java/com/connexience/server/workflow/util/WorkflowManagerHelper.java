/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.util;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.WorkflowLockOperation;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.study.LoggerDeployment;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.scanner.filesystems.AzureScanner;
import com.connexience.server.model.scanner.filesystems.DiskScanner;
import com.connexience.server.model.scanner.filesystems.S3Scanner;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Link;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.util.SerializationUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.types.WorkflowDatasetQuery;
import com.connexience.server.workflow.types.WorkflowFilesystemScanner;
import com.connexience.server.workflow.types.WorkflowProject;
import com.connexience.server.workflow.types.WorkflowStudy;
import com.connexience.server.workflow.types.WorkflowStudyDeployment;
import com.connexience.server.workflow.types.WorkflowStudyLogger;
import com.connexience.server.workflow.types.WorkflowStudyObject;
import com.connexience.server.workflow.types.WorkflowStudyPhase;
import com.connexience.server.workflow.types.WorkflowStudySubject;
import com.connexience.server.workflow.types.WorkflowStudySubjectGroup;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pipeline.core.data.Data;
import org.pipeline.core.xmlstorage.XmlDataStore;

/**
 * This class performs all of the "business logic" for the workflow manager service.
 * The JSON service and RMI service delegate all tasks to this helper so that we
 * can keep all of the code sort of in one place.
 * @author hugo
 */
public class WorkflowManagerHelper {
    private Ticket ticket;
    private static Logger logger = Logger.getLogger(WorkflowManagerHelper.class);


    public WorkflowManagerHelper(Ticket ticket) {
        this.ticket = ticket;
    }

    public Ticket authenticate(String username, String password) throws ConnexienceException {
        Ticket t = EJBLocator.lookupTicketBean().createWebTicket(username, password);
        this.ticket = t;
        return t;
    }

    public int changeObjectId(String originalId, String requestedId) throws ConnexienceException {
        return EJBLocator.lookupObjectDirectoryBean().changeObjectId(ticket, originalId, requestedId);
    }

    public DocumentVersion createNextVersion(String documentId) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().createNextVersion(ticket, documentId);
    }

    public WorkflowLock createWorkflowLock(String invocationId, String contextId, boolean allowFailedSubworkflows, boolean pauseOnFailures) throws ConnexienceException {
        WorkflowInvocationFolder folder = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
        if(folder!=null){
            WorkflowLock lock = WorkflowEJBLocator.lookupWorkflowLockBean().createWorkflowLock(ticket, folder);
            lock.setContextId(contextId);
            lock.setEngineId(folder.getEngineId());
            lock.setUserId(ticket.getUserId());
            lock.setAllowFailedSubworkflows(allowFailedSubworkflows);
            lock.setPauseOnFailedSubworkflows(pauseOnFailures);
            lock = WorkflowEJBLocator.lookupWorkflowLockBean().saveWorkflowLock(ticket, lock);
            return lock;
        } else {
            throw new ConnexienceException("No such invocation: " + invocationId);
        }
    }

    public void attachInvocationToLock(String invocationId, long lockId) throws ConnexienceException
    {
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
        if (invocation == null) {
            throw new ConnexienceException("Invalid invocation id: " + invocationId);
        }

        WorkflowEJBLocator.lookupWorkflowLockBean().attachInvocationToLock(ticket, lockId, invocation);
    }


    public void deleteFolder(String folderId) throws ConnexienceException {
        EJBLocator.lookupStorageBean().removeFolderTree(ticket, folderId);
    }

    public void deleteDocument(String documentId) throws ConnexienceException {
        EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, documentId);
    }
    
    public WorkflowInvocationFolder executeWorkflow(String workflowId, WorkflowParameterList parameters,String parentInvocationId,  long lockId, String folderName) throws ConnexienceException {
            WorkflowInvocationMessage message = new WorkflowInvocationMessage(ticket, workflowId);
            try {
                message.setParameterXmlData(SerializationUtils.serialize(parameters));
            } catch (IOException ioe){
                throw new ConnexienceException("Error serializing workflow parameters: " + ioe.getMessage(), ioe);
            }
            message.setUseLatest(true);
            message.setWorkflowId(workflowId);
            message.setTicket(ticket);
            message.setResultsFolderName(folderName);       
            
            // Pass on the parent invocation ID if there is one
            if(parentInvocationId==null || parentInvocationId.trim().isEmpty()){
                message.setParentInvocationId(null);
            } else {
                message.setParentInvocationId(parentInvocationId.trim());
            }
            
            if(lockId!=-1){
                message.setLockMember(true);
                message.setLockId(lockId);
            } else {
                message.setLockMember(false);
            }
            
            String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, message);
            return WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
    }

    public DocumentRecord[] getChildDocuments(String folderId) throws ConnexienceException {
        try {
            List contents = EJBLocator.lookupStorageBean().getAllFolderContents(ticket, folderId);
            ArrayList<DocumentRecord> docs = new ArrayList<>();
            for(int i=0;i<contents.size();i++){
                if(contents.get(i) instanceof DocumentRecord){
                    docs.add((DocumentRecord)contents.get(i));
                }
            }
            return docs.toArray(new DocumentRecord[docs.size()]);
            
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in getChildDocuments: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in getChildDocuments: " + e.getMessage(), e);
        }
    }

    public Folder[] getChildFolders(String folderId) throws ConnexienceException {
        try {
            List results = EJBLocator.lookupStorageBean().getChildFolders(ticket, folderId);
            Folder[] folders = new Folder[results.size()];
            for(int i=0;i<results.size();i++){
                folders[i] = (Folder)results.get(i);
            }
            return folders;
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in getChildFolders: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in getChildFolders: " + e.getMessage(), e);
        }  
    }

    public DocumentRecord getDocument(String id) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().getDocumentRecord(ticket, id);
    }

    public DocumentVersion getDocumentVersion(String documentId, int versionNumber) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().getVersion(ticket, documentId, versionNumber);
    }

    public DocumentVersion[] getDocumentVersions(String documentId) throws ConnexienceException {
        try {
            List results = EJBLocator.lookupStorageBean().listVersions(ticket, documentId);
            DocumentVersion[] versions = new DocumentVersion[results.size()];
            for(int i=0;i<results.size();i++){
                versions[i] = (DocumentVersion)results.get(i);
            } 
            return versions;
            
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in getDocumentVersions: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in getDocumentVersions: " + e.getMessage(), e);
        }    
    }

    public DynamicWorkflowLibrary getDynamicWorkflowLibraryByName(String libraryName) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowLibraryByLibraryName(ticket, libraryName);
    }

    public DynamicWorkflowService getDynamicWorkflowService(String id) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowService(ticket, id);
    }

    public Folder getFolder(String folderId) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
    }

    public Folder getHomeFolder(String userId) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().getHomeFolder(ticket, userId);
    }

    public DocumentVersion getLatestVersion(String documentId) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().getLatestVersion(ticket, documentId);
    }

    public String getLatestVersionId(String documentId) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().getLatestVersionId(documentId);
    }

    public MetadataCollection getMetadata(String objectId) throws ConnexienceException {
        List results = EJBLocator.lookupMetaDataBean().getObjectMetadata(ticket, objectId);
        MetadataCollection mdc = new MetadataCollection(results);
        mdc.setObjectId(objectId);
        return mdc;
    }

    public Folder getNamedSubdirectory(String id, String name) throws ConnexienceException {
        Folder f = EJBLocator.lookupStorageBean().getNamedFolder(ticket, id, name);
        if(f==null){
            f = new Folder();
            f.setName(name);
            f = EJBLocator.lookupStorageBean().addChildFolder(ticket, id, f);
            return f;
        } else {
            return f;
        }
    }

    public DocumentRecord getOrCreateDocumentRecord(String parentFolderId, String name) throws ConnexienceException {
        return StorageUtils.getOrCreateDocumentRecord(ticket, parentFolderId, name);
    }

    public User getPublicUser() throws ConnexienceException {
        String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();
        return EJBLocator.lookupUserDirectoryBean().getUser(ticket, publicUserId); 
    }

    public Date getServerTime() throws ConnexienceException {
        return new Date();
    }

    public String getServiceXml(String serviceId) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowServiceXML(ticket, serviceId);
    }

    public String getServiceXml(String serviceId, String versionId) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowServiceXML(ticket, serviceId, versionId);
    }

    public User getUser(String userId) throws ConnexienceException {
        return EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
    }

    public WorkflowDocument getWorkflow(String id) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, id);
    }

    public WorkflowInvocationFolder getWorkflowInvocation(String invocationId) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
    }

    public List<WorkflowInvocationFolder> listWorkflowInvocations(String workflowId) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(ticket, workflowId);
    }

    public void grantObjectPermission(String objectId, String principalId, String permission) throws ConnexienceException {
        EJBLocator.lookupAccessControlBean().grantAccess(ticket, principalId, objectId, permission);
    }

    public WorkflowDocument[] listWorkflows() throws ConnexienceException {
        List workflows = WorkflowEJBLocator.lookupWorkflowManagementBean().listWorkflows(ticket);
        WorkflowDocument[] results = new WorkflowDocument[workflows.size()];
        for(int i=0;i<workflows.size();i++){
            results[i] = (WorkflowDocument)workflows.get(i);
        }
        return results;
    }

    public DataStore loadDataStore() throws ConnexienceException {
        DataStore ds = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());
        return ds;
    }

    public void logWorkflowComplete(String invocationId, String status) throws ConnexienceException {
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
        invocation.setExecutionEndTime(new Date());
        WorkflowEJBLocator.lookupWorkflowManagementBean().saveInvocationFolder(ticket, invocation); 
    }

    public void logWorkflowDequeued(String invocationId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().logWorkflowDequeued(ticket, invocationId);  
    }

    public void logWorkflowExecutionStarted(String invocationId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().logWorkflowExecutionStarted(ticket, invocationId);
    }

    public void notifyEngineShutdown(String hostId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().removeDyamicWorkflowEngineRecord(hostId);
    }

    public void notifyEngineStartup(String hostId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().createDyamicWorkflowEngineRecord(hostId);
    }
    
    public JSONContainer queryDatasetItem(DatasetQuery query) throws ConnexienceException {
        if(query instanceof WorkflowDatasetQuery){
            try {
                // Have to instantiate the query because a wrapper can hold queries
                // that may not be contained in the server-common project
                WorkflowDatasetQuery dsq = (WorkflowDatasetQuery)query;
                DatasetQuery unwrappedQuery = dsq.instantiateQuery();
                return EJBLocator.lookupDatasetsBean().performQuery(ticket, unwrappedQuery);
            } catch (Exception e){
                throw new ConnexienceException("Error performing query: " + e.getMessage(), e);
            }
        } else {
            return EJBLocator.lookupDatasetsBean().performQuery(ticket, query);
        }
    }

    public JSONContainer queryDatasetItem(String datasetId, String itemName) throws ConnexienceException {
        try {
            Object results = EJBLocator.lookupDatasetsBean().getDatasetItemValue(ticket, datasetId, itemName);
            if(results instanceof JSONContainer){
                return (JSONContainer)results;
            } else {
                // Put the data into a json container
                JSONArray data = new JSONArray();
                JSONObject result = new JSONObject();
                
                JSONObject rowObject = new JSONObject();
                rowObject.put(itemName, results);
                data.put(rowObject);
                
                result.put("data", data);
                return new JSONContainer(result);
            }
        } catch (JSONException jse){
            throw new ConnexienceException("Error returning JSON data: " + jse.getMessage(), jse);
        }
    }

    public void refreshLockStatus(long lockId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowLockBean().notifyLockHolderIfComplete(ticket, lockId, false);
    }

    public void removeWorkflowLock(long lockId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowLockBean().removeWorkflowLock(ticket, lockId);
    }

    public void resetDataset(String datasetId) throws ConnexienceException {
        EJBLocator.lookupDatasetsBean().resetDataset(ticket, datasetId);
    }

    public DocumentRecord saveDocument(String parentId, DocumentRecord doc) throws ConnexienceException {
        if(doc.getId()!=null && !doc.getId().isEmpty()){
            // Existing
            DocumentRecord existingDoc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, doc.getId());
            if(existingDoc!=null){
                if(!existingDoc.getContainerId().equals(parentId)){
                    // Change in container ID
                    DocumentRecord docInNewFolder = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, parentId, doc.getName());
                    if(docInNewFolder!=null){
                        // Need to upload to the new document and delete the old one
                        existingDoc = StorageUtils.mergeDocuments(ticket, doc, docInNewFolder);
                        
                    } else {
                        // Nothing here
                        existingDoc.setContainerId(parentId);
                    }
                }
                
                existingDoc.setName(doc.getName());
                existingDoc.setDescription(doc.getDescription());
                
                return EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, existingDoc);
                
            } else {
                doc = StorageUtils.getOrCreateDocumentRecord(ticket, parentId, doc.getName());
                doc.setContainerId(parentId);
                doc.setCreatorId(ticket.getUserId());
                doc.setOrganisationId(ticket.getOrganisationId());
                return EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
            }
            
        } else {
            // New
            doc = StorageUtils.getOrCreateDocumentRecord(ticket, parentId, doc.getName());
            doc.setContainerId(parentId);
            doc.setCreatorId(ticket.getUserId());
            doc.setOrganisationId(ticket.getOrganisationId());            
            return EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
        }
        
    }

    public DynamicWorkflowLibrary saveDynamicWorkflowLibrary(DynamicWorkflowLibrary library) throws ConnexienceException {
        library = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(ticket, library);
        return library;
    }

    public DynamicWorkflowService saveDynamicWorkflowService(DynamicWorkflowService service) throws ConnexienceException {
        service = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(ticket, service);
        return service;
    }

    public Folder saveFolder(Folder f) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().updateFolder(ticket, f);
    }

    public WorkflowInvocationFolder saveWorkflowInvocation(WorkflowInvocationFolder invocation) throws ConnexienceException {
        invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().saveInvocationFolder(ticket, invocation);
        return invocation;
    }

    public void setCurrentBlock(String invocationId, String contextId, int percentComplete) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().setCurrentBlockForInvocation(ticket, invocationId, contextId, percentComplete);
    }

    public void setCurrentBlockStreamingProcess(String invocationId, String contextId, long totalBytesToStream, long bytesStreamed) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().setInvocationStatus(ticket, invocationId, contextId, totalBytesToStream, bytesStreamed);
    }

    public void setInvocationEngineId(String invocationId, String engineId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().setDynamicWorkflowEngineForInvocation(ticket, invocationId, engineId);
    }

    public void setWorkflowLockStatus(long lockId, String status) throws ConnexienceException {
        WorkflowLock lock = WorkflowEJBLocator.lookupWorkflowLockBean().getLock(ticket, lockId);
        if(lock!=null){
            lock.setStatus(status);
            WorkflowEJBLocator.lookupWorkflowLockBean().saveWorkflowLock(ticket, lock);
        } else {
            throw new ConnexienceException("No such lock: " + lockId);
        }
    }

    public void setWorkflowStatus(String invocationId, int status, String message) throws ConnexienceException {
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
        if(invocation!=null){
            invocation.setInvocationStatus(status);
            invocation.setMessage(message);
            
            // Set end time if the status is failed
            if(status==WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS){
                invocation.setExecutionEndTime(new Date());
            }
            
            WorkflowEJBLocator.lookupWorkflowManagementBean().saveInvocationFolder(ticket, invocation);
        } else {
            throw new ConnexienceException("No such invocation");
        }   
    }

    public void updateDatasetItemWithJson(String datasetId, String itemName, long rowId, JSONContainer data) throws ConnexienceException {
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
        if(item!=null){
            EJBLocator.lookupDatasetsBean().updateExistingMultipleValueItemRow(ticket, datasetId, itemName, rowId, data);
        } else {
            throw new ConnexienceException("Item: " + itemName + " does not exist");
        }
    }
    
    public void updateDatasetItemWithJson(String datasetId, String itemName, JSONContainer data) throws ConnexienceException {
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
        if(item!=null){
            EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(ticket, item.getId(), data);
        } else {
            // Auto create the item
            JsonMultipleValueItem newItem = new JsonMultipleValueItem();
            newItem.setDatasetId(datasetId);
            newItem.setName(itemName);
            newItem = (JsonMultipleValueItem)EJBLocator.lookupDatasetsBean().saveDatasetItem(ticket, newItem);
            EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(ticket, newItem.getId(), data);
        }
    }
    
    public void updateDatasetItemWithData(String datasetId, String itemName, Data data) throws ConnexienceException {
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
        if(item!=null){
            EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(ticket, item.getId(), data);
        } else {
            throw new ConnexienceException("No such item: " + itemName + " in dataset");
        }         
    }
    
    public void updateDatasetItemWithNumber(String datasetId, String itemName, Number data) throws ConnexienceException {
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
        if(item!=null){
            EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(ticket, item.getId(), data);
        } else {
            throw new ConnexienceException("No such item: " + itemName + " in dataset");
        }        
    }

    public DocumentVersion updateDocumentVersion(DocumentVersion version) throws ConnexienceException {
        return EJBLocator.lookupStorageBean().updateVersion(ticket, version);
    }

    public void updateServiceLog(String invocationId, String contextId, String outputData, String statusText, String statusMessage) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceLog(ticket, invocationId, contextId, outputData, statusText, statusMessage);
    }

    public void updateServiceLogMessage(String invocationId, String contextId, String statusText, String statusMessage) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceLogMessage(ticket, invocationId, contextId, statusText, statusMessage);
    }

    public void uploadMetadata(String objectId, MetadataCollection metaData) throws ConnexienceException {
        MetadataItem item;
            
        for(int i=0;i<metaData.size();i++){
            item = metaData.get(i);
            item.setObjectId(objectId);
            item.setUserId(ticket.getUserId());
            EJBLocator.lookupMetaDataBean().addMetadata(ticket, objectId, item);
        }
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Link addDocumentLink(String sourceDocumentId, String targetDocumentId) throws ConnexienceException {
        Ticket t = getTicket();
        ServerObject source = EJBLocator.lookupObjectDirectoryBean().getServerObject(t, sourceDocumentId, ServerObject.class);
        ServerObject target = EJBLocator.lookupObjectDirectoryBean().getServerObject(t, targetDocumentId, ServerObject.class);

        return EJBLocator.lookupLinkBean().createLink(getTicket(), source, target);
    }


    public DocumentRecord[] getDocumentLinks(String id) throws ConnexienceException {
        Ticket t = getTicket();
        ServerObject source = EJBLocator.lookupObjectDirectoryBean().getServerObject(t, id, DocumentRecord.class);
        Collection<ServerObject> objs = EJBLocator.lookupLinkBean().getLinkedSourceObjects(t, source);
        Collection<DocumentRecord> docs = new ArrayList<>();
        for (ServerObject obj : objs) {
            if (obj instanceof DocumentRecord) {
                docs.add((DocumentRecord) obj);
            }
        }

        DocumentRecord[] results = new DocumentRecord[docs.size()];
        results = docs.toArray(results);
        return results;
    }
    
    public void workflowTerminatedByEngine(String invocationId) throws ConnexienceException {
        WorkflowEJBLocator.lookupWorkflowManagementBean().workflowTerminatedByEngine(invocationId);
    }    
    
    public WorkflowLock getWorkflowLock(long lockId) throws ConnexienceException {
        return WorkflowEJBLocator.lookupWorkflowLockBean().getLock(getTicket(), lockId);
    }
    
    public Integer getDeploymentId(String studyCode, String phase, String loggerSerialNumber) throws ConnexienceException {
        LoggerDeployment deployment = EJBLocator.lookupLoggersBean().getLoggerDeployment(ticket, studyCode, phase, loggerSerialNumber);
        if(deployment!=null){
            return deployment.getId();
        } else {
            throw new ConnexienceException("No deployment");
        }
    }
    
    public Folder getDeploymentFolder(Integer deploymentId) throws ConnexienceException {
        LoggerDeployment deployment = EJBLocator.lookupLoggersBean().getLoggerDeployment(ticket, deploymentId);
        if(deployment!=null){
            return EJBLocator.lookupStorageBean().getFolder(ticket, deployment.getDataFolderId());
        } else {
            throw new ConnexienceException("No deployment");
        }
    }
    
    public void addDataToDeployment(Integer deploymentId, String documentRecordId) throws ConnexienceException {
        EJBLocator.lookupStudyBean().addDataToDeployment(ticket, deploymentId, documentRecordId);
    }
    
    public Dataset saveDataset(Dataset ds) throws ConnexienceException {
        return EJBLocator.lookupDatasetsBean().saveDataset(ticket, ds);
    }
    
    public DatasetItem saveDatasetItem(DatasetItem item) throws ConnexienceException {
        return EJBLocator.lookupDatasetsBean().saveDatasetItem(ticket, item);
    }
    
    public User createAccount(String firstName, String surname, String logon, String password) throws ConnexienceException {
        if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
            User u;
            try {
                u = EJBLocator.lookupUserDirectoryBean().getUserFromLogonName(logon);
                u.setFirstName(firstName);
                u.setSurname(surname);
                u = EJBLocator.lookupUserDirectoryBean().saveUser(ticket, u);
                
                if(password!=null && !password.isEmpty()){
                    EJBLocator.lookupUserDirectoryBean().setPassword(ticket, u.getId(), logon, password);
                }
                
            } catch (ConnexienceException ce){
                // No such user
                u = new User();
                u.setFirstName(firstName);
                u.setSurname(surname);
                u.setName(firstName + " " + surname);
                u = EJBLocator.lookupUserDirectoryBean().createAccount(u, logon, password);                
            }

            return u;
        } else {
            // Only Admins can do this
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    } 
    
    public Ticket createTicketForUser(String userId) throws ConnexienceException {
        if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
            return EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
        } else {
            // Only Admins can do this
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    public DatasetItem lookupUserDatasetItemByName(String datasetName, String itemName) throws ConnexienceException {
        Dataset ds = EJBLocator.lookupDatasetsBean().getUserDatasetByName(ticket, datasetName);
        if(ds!=null){
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, ds.getId(), itemName);
            if(item!=null){
                return item;
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException("User does not own a Dataset called: " + datasetName);
        }
    }
    
    public boolean userHasNamedDataset(String datasetName) throws ConnexienceException {
        Dataset ds = EJBLocator.lookupDatasetsBean().getUserDatasetByName(ticket, datasetName);
        if(ds!=null){
            return true;
        } else {
            return false;
        }
    }
    
    public Dataset lookupUserDatasetByName(String datasetName) throws ConnexienceException {
        Dataset ds = EJBLocator.lookupDatasetsBean().getUserDatasetByName(ticket, datasetName);
        if(ds!=null){
            return ds;
        } else {
            throw new ConnexienceException("User does not own a Dataset called: " + datasetName);
        }        
    }
    
    public DatasetItem[] listDatasetItems(String datasetId) throws ConnexienceException {
        Dataset ds = EJBLocator.lookupDatasetsBean().getDataset(ticket, datasetId);
        if(ds!=null){
            List results = EJBLocator.lookupDatasetsBean().getDatasetItems(ticket, datasetId);
            DatasetItem[] items = new DatasetItem[results.size()];
            for(int i=0;i<results.size();i++){
                items[i] = (DatasetItem)results.get(i);
            }
            return items;
        } else {
            throw new ConnexienceException("No such dataset: " + datasetId);
        }         
    }
    
    public Group getGroupByName(String name) throws ConnexienceException {
        return EJBLocator.lookupGroupDirectoryBean().getGroupByName(ticket, name);
    }
    
    public void addUserToGroup(String groupId, String userId) throws ConnexienceException {
        EJBLocator.lookupGroupDirectoryBean().addUserToGroup(ticket, userId, groupId);
    }
    
    public void removeUserFromGroup(String groupId, String userId) throws ConnexienceException {
        EJBLocator.lookupGroupDirectoryBean().removeUserFromGroup(ticket, userId, groupId);
    }
    
    public Group createGroup(String groupName, boolean ownerApprovalRequired, boolean nonMembersView) throws ConnexienceException {
        Group g = new Group();
        g.setName(groupName);
        g.setAdminApproveJoin(ownerApprovalRequired);
        g.setNonMembersList(nonMembersView);
        g.setProtectedGroup(false);
        g = EJBLocator.lookupGroupDirectoryBean().saveGroup(ticket, g);
        return g;
    }
    
    public ServerObject getServerObject(String objectId) throws ConnexienceException {
        return EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, objectId, ServerObject.class);
    }
    
    public WorkflowProject getProject(int projectId) throws ConnexienceException {
        Project p = EJBLocator.lookupProjectsBean().getProject(ticket, projectId);
        
        WorkflowProject wfp = new WorkflowProject();
        wfp.setAdminGroupId(p.getAdminGroupId());
        wfp.setDataFolderId(p.getDataFolderId());
        wfp.setDescription(p.getDescription());
        wfp.setExternalId(p.getExternalId());
        wfp.setId(p.getId());
        wfp.setMembersGroupId(p.getMembersGroupId());
        wfp.setName(p.getName());
        wfp.setOwnerId(p.getOwnerId());
        wfp.setPrivateProject(p.isPrivateProject());
        wfp.setRemoteScannerId(p.getRemoteScannerId());
        wfp.setWorkflowFolderId(p.getWorkflowFolderId());
        
        // Add the additional properties
        wfp.getAdditionalProperties().clear();
        for(String key : p.getAdditionalProperties().keySet()){
            wfp.getAdditionalProperties().put(key, p.getAdditionalProperty(key));
        }      
        return wfp;
    }
    
    public WorkflowFilesystemScanner getScanner(long scannerId) throws ConnexienceException {
        RemoteFilesystemScanner s = EJBLocator.lookupScannerBean().getScanner(ticket, scannerId);
        WorkflowFilesystemScanner wfs = new WorkflowFilesystemScanner();
        if(s instanceof AzureScanner){
            wfs.setContainerOrBucketName(((AzureScanner)s).getContainerName());
            wfs.setCredentialsId(((AzureScanner)s).getCredentialsId());
            wfs.setImportDataSuffix(((AzureScanner)s).getImportedDataContainerSuffix());
            wfs.setExportDataSuffix((((AzureScanner)s).getExportedDataContainerSuffix()));
            wfs.setImportExportSeparationEnabled(((AzureScanner)s).isImportExportContainerSeparationEnabled());
            
        } else if(s instanceof S3Scanner){
            wfs.setContainerOrBucketName(((S3Scanner)s).getBucketName());
            wfs.setCredentialsId(((S3Scanner)s).getCredentialsId());
            wfs.setImportDataSuffix(((S3Scanner)s).getImportedDataBucketSuffix());
            wfs.setExportDataSuffix(((S3Scanner)s).getExportedDataBucketSuffix());
            wfs.setImportExportSeparationEnabled(((S3Scanner)s).isImportExportBucketSeparationEnabled());
            
        } else if(s instanceof DiskScanner){
            wfs.setContainerOrBucketName(((DiskScanner)s).getFolderPath());
            wfs.setImportDataSuffix(((DiskScanner)s).getImportSubdirectory());
            wfs.setExportDataSuffix(((DiskScanner)s).getExportSubdirectory());
            wfs.setImportExportSeparationEnabled(((DiskScanner)s).isImportExportSeparationEnabled());
            
        }
        
        wfs.setId(s.getId());
        wfs.setTypeName(s.getTypeName());
        return wfs;
    }
    
    public XmlDataStore getAllServerProperties() throws ConnexienceException {
        return EJBLocator.lookupPreferencesBean().getAllProperties();
    }
    
    public long lookupDatasetItemId(String datasetId, String itemName) throws ConnexienceException {
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
        if(item!=null){
            return item.getId();
        } else {
            throw new ConnexienceException("No such item: " + itemName + " in dataset: " + datasetId);
        }
    }
    
    public HashMap getStudyStats(int studyId) throws ConnexienceException {
        return EJBLocator.lookupStudyBean().getStudyStats(ticket, studyId);
    }
    
    public WorkflowStudy getStudy(int studyId) throws ConnexienceException {
        return EJBLocator.lookupWorkflowStudyBean().getStudy(ticket, studyId);
    }
    
    public WorkflowStudyPhase[] listStudyPhases(int studyId) throws ConnexienceException {
        List<WorkflowStudyPhase> phases = EJBLocator.lookupWorkflowStudyBean().listStudyPhases(ticket, studyId);
        return phases.toArray(new WorkflowStudyPhase[phases.size()]);
    }
    
    public WorkflowStudySubjectGroup[] listPhaseGroups(int phaseId) throws ConnexienceException {
        List<WorkflowStudySubjectGroup> groups = EJBLocator.lookupWorkflowStudyBean().listPhaseGroups(ticket, phaseId);
        return groups.toArray(new WorkflowStudySubjectGroup[groups.size()]);
    }
    
    public WorkflowStudySubjectGroup[] listChildGroups(int parentGroupId) throws ConnexienceException {
        List<WorkflowStudySubjectGroup> groups = EJBLocator.lookupWorkflowStudyBean().listChildGroups(ticket, parentGroupId);
        return groups.toArray(new WorkflowStudySubjectGroup[groups.size()]);
    }
    
    public WorkflowStudySubject[] listGroupSubjects(int parentGroupId) throws ConnexienceException {
        List<WorkflowStudySubject> subjects = EJBLocator.lookupWorkflowStudyBean().listGroupSubjects(ticket, parentGroupId);
        return subjects.toArray(new WorkflowStudySubject[subjects.size()]);
    }
    
    public WorkflowStudyDeployment[] listGroupDeployments(int parentGroupId) throws ConnexienceException {
        List<WorkflowStudyDeployment> deployments = EJBLocator.lookupWorkflowStudyBean().listGroupDeployments(ticket, parentGroupId);
        return deployments.toArray(new WorkflowStudyDeployment[deployments.size()]);
    }
    
    public WorkflowStudyLogger getLogger(int loggerId) throws ConnexienceException {
        return EJBLocator.lookupWorkflowStudyBean().getLogger(ticket, loggerId);
    }
    
    public WorkflowStudyObject getStudyObjectAssociatedWithFolder(String folderId) throws ConnexienceException {
        return EJBLocator.lookupWorkflowStudyBean().getStudyObjectAssociatedWithFolder(ticket, folderId);
    }
    
    public void updateStudyObjectProperties(WorkflowStudyObject object) throws ConnexienceException {
        EJBLocator.lookupWorkflowStudyBean().updateStudyObjectProperties(ticket, object);
    }
}
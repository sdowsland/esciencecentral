/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.rest;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.security.*;
import com.connexience.server.model.social.Link;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.workflow.*;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.types.WorkflowFilesystemScanner;
import com.connexience.server.workflow.types.WorkflowProject;
import com.connexience.server.workflow.types.WorkflowStudy;
import com.connexience.server.workflow.types.WorkflowStudyDeployment;
import com.connexience.server.workflow.types.WorkflowStudyLogger;
import com.connexience.server.workflow.types.WorkflowStudyObject;
import com.connexience.server.workflow.types.WorkflowStudyPhase;
import com.connexience.server.workflow.types.WorkflowStudySubject;
import com.connexience.server.workflow.types.WorkflowStudySubjectGroup;
import com.connexience.server.workflow.util.WorkflowManagerHelper;
import java.io.ByteArrayOutputStream;

import javax.ws.rs.*;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.pipeline.core.data.io.JsonDataImporter;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamWriter;


/**
 * Thist class provides the server that is used by workflow engines to communicate
 * @author hugo
 */
@Path("/rest/wf")
public class JSONService {
    /** Object mapper for this service */
    private ObjectMapper mapper;

    /** Context object */
    @Context
    private HttpServletRequest request;
    
    public JSONService() {
        mapper = new ObjectMapper();
        mapper.enableDefaultTyping();
    }
    
    /** Create a ticket */
    private Ticket createTicket() throws ConnexienceException {
        // Try and create a ticket from the header parameters
        try {
            String uid = request.getHeader("cnx-userid");
            String orgid = request.getHeader("cnx-organisationid");
            
            String groups = request.getHeader("cnx-groups");

            if(groups!=null){
                int groupCount = Integer.parseInt(groups);
                String[] groupList = new String[groupCount];
                String group;
                
                for(int i=0;i<groupCount;i++){
                    group = request.getHeader("cnx-group" + i);
                    if(group!=null){
                        groupList[i] = group;
                    }
                }
                WebTicket wt = new WebTicket();
                wt.setUserId(uid);
                wt.setOrganisationId(orgid);
                wt.setRemoteHost(request.getRemoteHost());
                wt.setGroupIds(groupList);
                return wt;
                
            } else {
                WebTicket wt = new WebTicket();
                wt.setUserId(uid);
                wt.setOrganisationId(orgid);
                wt.setRemoteHost(request.getRemoteHost());
                return wt;
            }
            
        } catch (Exception e){
            throw new ConnexienceException("Error creating ticket from request data: " + e.getMessage(), e);
        }
    }
    
    @GET
    @Path("/ds")
    @Produces("application/json")
    public DataStore getDataStore() throws ConnexienceException {
        Ticket pt = EJBLocator.lookupTicketBean().createPublicWebTicket();
        DataStore ds = EJBLocator.lookupStorageBean().getOrganisationDataStore(pt, pt.getOrganisationId());
        return ds;
    }
    
    @POST
    @Path("/login")
    @Produces("application/json")
    public Ticket authenticate(@FormParam("username")String username, @FormParam("password")String password) throws ConnexienceException {
        return EJBLocator.lookupTicketBean().createWebTicket(username, password);
    }
    
    @GET
    @Path("/users/{id}")
    @Produces("application/json")
    public User getUser(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getUser(id);
    }
    
    @GET
    @Path("/documents/{id}")
    @Produces("application/json")
    public DocumentRecord getDocumentRecord(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDocument(id);
    }
    
    @GET
    @Path("/folders/{id}/nameddocuments/{name}")
    @Produces("application/json")
    public DocumentRecord getOrCreateDocumentRecord(@PathParam("id")String parentFolderId, @PathParam("name")String name) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getOrCreateDocumentRecord(parentFolderId, name);
    }
    
    @GET
    @Path("/workflows/{id}")
    public WorkflowDocument getWorkflow(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getWorkflow(id);
    }
    
    @POST
    @Path("/workflows/invocations/{id}/status")
    @Produces("application/json")
    public void logWorkflowComplete(@PathParam("id")String invocationId, @FormParam("status")String status) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).logWorkflowComplete(invocationId, status);      
    }
    
    @GET
    @Path("/workflows/invocations/{id}")
    @Produces("application/json")
    public WorkflowInvocationFolder getWorkflowInvocation(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getWorkflowInvocation(id);
    }
   
    @POST
    @Path("/workflows/invocations/{id}/engineid")
    @Produces("application/json")
    public void setInvocationEngineId(@PathParam("id")String invocationId, @FormParam("engineid")String engineId) throws ConnexienceException {
         new WorkflowManagerHelper(createTicket()).setInvocationEngineId(invocationId, engineId);
    }
    
    @GET
    @Path("/workflows/{id}/invocations")
    @Produces("application/json")
    public List<WorkflowInvocationFolder> listWorkflowInvocations(@PathParam("id")String workflowId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listWorkflowInvocations(workflowId);
    }
    
    @POST
    @Path("/workflows/invocations/")
    @Produces("application/json")
    public WorkflowInvocationFolder saveWorkflowInvocation(@FormParam("invocation")String invocationData) throws ConnexienceException {
        try {
            WorkflowInvocationFolder invocation = mapper.readValue(invocationData, WorkflowInvocationFolder.class);
            return new WorkflowManagerHelper(createTicket()).saveWorkflowInvocation(invocation);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in saveWorkflowInvocation: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in saveWorkflowInvocation: " + e.getMessage(), e);
        }        
    }
    
    @POST
    @Path("/workflows/invocations/{id}/currentblock")
    @Produces("application/json")
    public void setCurrentBlock(@PathParam("id")String invocationId, @FormParam("contextId")String contextId, @FormParam("percentComplete")int percentComplete) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).setCurrentBlock(invocationId, contextId, percentComplete);
    }

    @POST
    @Path("/workflows/invocations/{id}/blocks/{context}/progress")
    @Produces("application/json")
    public void setCurrentBlockStreamingProgress(@PathParam("id")String invocationId, @PathParam("context")String contextId, @FormParam("total")long totalBytesToStream, @FormParam("bytes")long bytesStreamed) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).setCurrentBlockStreamingProcess(invocationId, contextId, totalBytesToStream, bytesStreamed);    
    }
    
    @POST
    @Path("/workflows/invocations/{id}/blocks/{context}/log")
    @Produces("application/json")
    public void updateServiceLog(@PathParam("id")String invocationId, @PathParam("context")String contextId, @FormParam("output")String outputData, @FormParam("status")String statusText, @FormParam("message")String statusMessage) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).updateServiceLog(invocationId, contextId, outputData, statusText, statusMessage);     
    }
    
    @POST
    @Path("/workflows/invocations/{id}/blocks/{context}/message")
    @Produces("application/json")
    public void updateServiceLogMessage(@PathParam("id")String invocationId, @PathParam("context")String contextId, @FormParam("status")String statusText, @FormParam("message")String statusMessage) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).updateServiceLogMessage(invocationId, contextId, statusText, statusMessage);     
    }    
    
    @DELETE
    @Path("/folders/{id}/delete")
    @Produces("application/json")
    public void deleteFolder(@PathParam("id")String id) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).deleteFolder(id);      
    }
    
    @DELETE
    @Path("/documents/{id}/delete")
    @Produces("application/json")
    public void deleteDocument(@PathParam("id")String id) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).deleteDocument(id);
    }
    
    @POST
    @Path("/folders")
    @Produces("application/json")
    public Folder saveFolder(@FormParam("folder")String folderData) throws ConnexienceException {
        try {
            Folder f = mapper.readValue(folderData, Folder.class);
            return new WorkflowManagerHelper(createTicket()).saveFolder(f);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in saveFolder: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in saveFolder: " + e.getMessage(), e);
        }        
    }
    
    @POST
    @Path("/workflows/invocations/{id}/started")
    @Produces("application/json")
    public void logWorkflowExecutionStarted(@PathParam("id")String invocationId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).logWorkflowExecutionStarted(invocationId);      
    }    

    @POST
    @Path("/workflows/invocations/{id}/finished")
    @Produces("application/json")
    public void logWorkflowExecutionFinished(@PathParam("id")String invocationId, @FormParam("status")String status) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).logWorkflowComplete(invocationId, status);     
    }        
    
    @POST
    @Path("/workflows/invocations/{id}/dequeued")
    @Produces("application/json")
    public void logWorkflowDequeud(@PathParam("id")String invocationId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).logWorkflowDequeued(invocationId);     
    }     
    
    @POST
    @Path("/workflows/locks/{id}/refresh")
    @Produces("application/json")
    public void refreshLockStatus(@PathParam("id")long lockId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).refreshLockStatus(lockId);     
    }
            
    @GET
    @Path("/workflows/services/{id}/definitions/latest/get")
    @Produces("application/json")
    public String getServiceXml(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getServiceXml(id); 
    }
    
    @GET
    @Path("/workflows/services/{id}/definitions/{version}/get")
    @Produces("application/json")
    public String getServiceXml(@PathParam("id")String id, @PathParam("version")String versionId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getServiceXml(id, versionId);       
    }    
    
    @GET
    @Path("/objects/{id}/latestid")
    @Produces("text/plain")
    public String getLatestVersionId(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getLatestVersionId(id);
    }
    
    @GET
    @Path("/objects/{id}/latest")
    @Produces("application/json")
    public DocumentVersion getLatestVersion(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getLatestVersion(id);
    }
    
    @POST
    @Path("/workflows/libraries/byname")
    @Produces("application/json")
    public DynamicWorkflowLibrary getDynamicWorkflowLibraryByName(@FormParam("name")String name) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDynamicWorkflowLibraryByName(name);      
    }
    
    @GET
    @Path("/folders/{id}/get")
    @Produces("application/json")
    public Folder getFolder(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getFolder(id);       
    }
    
    @DELETE
    @Path("/workflows/locks/{id}/remove")
    @Produces("application/json")
    public void removeWorkflowLock(@PathParam("id")long lockId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).removeWorkflowLock(lockId);
    }
    
    @POST
    @Path("/workflows/locks/{id}/setstatus")
    @Produces("application/json")
    public void setWorkflowLockStatus(@PathParam("id")long lockId, @FormParam("status")String status) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).setWorkflowLockStatus(lockId, status);
    }
    
    @POST
    @Path("/workflows/locks/create/{invocationid}/{contextid}")
    @Produces("application/json")
    public WorkflowLock createWorkflowLock(@PathParam("invocationid")String invocationId, @PathParam("contextid")String contextId, @FormParam("allowFailedSubworkflows")boolean allowFailedSubworkflows, @FormParam("pauseOnFailures")boolean pauseOnFailures) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).createWorkflowLock(invocationId, contextId, allowFailedSubworkflows,pauseOnFailures);       
    }

    @POST
    @Path("/workflows/locks/{lockId}/invocations/{invocationId}")
    public void attachInvocationToLock(@PathParam("lockId") long lockId, @PathParam("invocationId") String invocationId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).attachInvocationToLock(invocationId, lockId);
    }

    @POST
    @Path("/workflows/{id}/execute")
    @Produces("application/json")
    public WorkflowInvocationFolder executeWorkflow(@PathParam("id")String workflowId, @FormParam("parameters")String parameterData, @FormParam("lockid")long lockId, @FormParam("folder")String folderName, @FormParam("parentinvocationid")String parentInvocationID) throws ConnexienceException {
        try {
            WorkflowParameterList params = mapper.readValue(parameterData, WorkflowParameterList.class);
            return new WorkflowManagerHelper(createTicket()).executeWorkflow(workflowId, params, parentInvocationID, lockId, folderName);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in getWorkflowInvocation: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in getWorkflowInvocation: " + e.getMessage(), e);
        }    
    }
    
    @POST
    @Path("/folders/{containerid}/documents/save")
    @Produces("application/json")
    public DocumentRecord saveDocument(@PathParam("containerid")String containerId, @FormParam("document")String documentData) throws ConnexienceException {
        try {
            DocumentRecord doc = mapper.readValue(documentData, DocumentRecord.class);
            return new WorkflowManagerHelper(createTicket()).saveDocument(containerId, doc);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in saveDocument: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in saveDocument: " + e.getMessage(), e);
        }        
    }
    
    @GET
    @Path("/time")
    @Produces("application/json")
    public Date getServerTime() throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getServerTime();
    }
    
    @POST
    @Path("/workflows/engines/startup")
    @Produces("application/json")
    public void notifyEngineStartup(@FormParam("hostid")String hostId) throws ConnexienceException {
         new WorkflowManagerHelper(createTicket()).notifyEngineStartup(hostId);
    }
    
    @POST
    @Path("/workflows/engines/shutdown")
    @Produces("application/json")
    public void notifyEngineShutdown(@FormParam("hostid")String hostId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).notifyEngineShutdown(hostId);
    }
    
    @GET
    @Path("/documents/{id}/getversions")
    @Produces("application/json")
    public DocumentVersion[] getDocumentVersions(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDocumentVersions(id);
    }
    
    @GET
    @Path("/documents/{id}/versionnumbers/{version}/getversion")
    @Produces("application/json")
    public DocumentVersion getDocumentVersion(@PathParam("id")String documentId, @PathParam("version")int version) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDocumentVersion(documentId, version);
    }
    
    @POST
    @Path("/documents/{id}/nextversion")
    @Produces("application/json")
    public DocumentVersion createNextVersion(@PathParam("id")String documentId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).createNextVersion(documentId);
    }
    
    @POST
    @Path("/documents/versions/updateversion")
    @Produces("application/json")
    public DocumentVersion updateDocumentVersion(@FormParam("version")String documentVersionData) throws ConnexienceException {
        try {
            DocumentVersion version = mapper.readValue(documentVersionData, DocumentVersion.class);
            return new WorkflowManagerHelper(createTicket()).updateDocumentVersion(version);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in updateDocumentVersion: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in updateDocumentVersion: " + e.getMessage(), e);
        }        
    }
    
    @GET
    @Path("/folders/home/{userid}")
    @Produces("application/json")
    public Folder getHomeFolder(@PathParam("userid")String userId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getHomeFolder(userId);
    }
    
    @GET
    @Path("/users/public")
    @Produces("application/json")
    public User getPublicUser() throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getPublicUser();
    }
    
    @POST
    @Path("/permissions/{objectid}/{principalid}/add")
    @Produces("application/json")
    public void grantObjectPermission(@PathParam("objectid")String objectId, @PathParam("principalid")String principalId, @FormParam("permission")String permission) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).grantObjectPermission(objectId, principalId, permission);
    }
    
    @POST
    @Path("/workflows/invocations/{id}/setstatus")
    @Produces("application/json")
    public void setWorkflowStatus(@PathParam("id")String invocationId, @FormParam("status")int status, @FormParam("message")String message) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).setWorkflowStatus(invocationId, status, message);
    }
    
    @POST
    @Path("/objects/{id}/idchange/{newid}")
    @Produces("application/json")
    public int changeObjectId(@PathParam("id")String originalId, @PathParam("newid")String requestedId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).changeObjectId(originalId, requestedId);
    }
    
    @POST
    @Path("/workflows/services/saveservice")
    @Produces("application/json")
    public DynamicWorkflowService saveDynamicWorkflowService(@FormParam("service")String serviceData) throws ConnexienceException {
        try {
            DynamicWorkflowService service = mapper.readValue(serviceData, DynamicWorkflowService.class);
            return new WorkflowManagerHelper(createTicket()).saveDynamicWorkflowService(service);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in saveDynamicWorkflowService: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in saveDynamicWorkflowService: " + e.getMessage(), e);
        }        
    }
    
    @GET
    @Path("/workflows/services/{id}/getservice")
    @Produces("application/json")
    public DynamicWorkflowService getDynamicWorkflowService(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDynamicWorkflowService(id);
    }
    
    @POST
    @Path("/workflows/libraries/savelibrary")
    @Produces("application/json")
    public DynamicWorkflowLibrary saveDynamicWorkflowLibrary(@FormParam("library")String libraryData) throws ConnexienceException {
        try {
            DynamicWorkflowLibrary library = mapper.readValue(libraryData, DynamicWorkflowLibrary.class);
            return new WorkflowManagerHelper(createTicket()).saveDynamicWorkflowLibrary(library);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in saveDynamicWorkflowLibrary: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in saveDynamicWorkflowLibrary: " + e.getMessage(), e);
        }        
    }    
    
    @GET
    @Path("/folders/{id}/children")
    @Produces("application/json")
    public Folder[] getChildFolders(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getChildFolders(id);
    }
    
    @GET
    @Path("/workflows")
    @Produces("application/json")
    public WorkflowDocument[] listWorkflows() throws ConnexienceException{
        return new WorkflowManagerHelper(createTicket()).listWorkflows();
    }
    
    @GET
    @Path("/folders/{id}/documents")
    @Produces("application/json")
    public DocumentRecord[] getChildDocuments(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getChildDocuments(id);
    }
    
    @GET
    @Path("/metadata/{id}")
    @Produces("application/json")
    public MetadataCollection getMetadata(@PathParam("id")String id) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getMetadata(id);
    }
    
    @POST
    @Path("/metadata/{id}")
    @Produces("application/json")
    public void uploadMetadata(@PathParam("id")String id, @FormParam("metadata")String metadataJson) throws ConnexienceException {
        try {
            MetadataCollection mdc = mapper.readValue(metadataJson, MetadataCollection.class);
            new WorkflowManagerHelper(createTicket()).uploadMetadata(id, mdc);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in getWorkflowInvocation: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in getWorkflowInvocation: " + e.getMessage(), e);
        }        
    }
    
    @GET
    @Path("/datasets/{id}/reset")
    @Produces("application/json")
    public void resetDataset(@PathParam("id")String id) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).resetDataset(id);
    }
    
    @POST
    @Path("/datasets/{id}/items/{name}/rows/{rowid}json")
    @Produces("application/json")
    public void updateDatasetItemWithJson(@PathParam("id")String id, @PathParam("name")String name, @PathParam("rowid")long rowId, @FormParam("objectData")String objectData) throws ConnexienceException {
        try {
            JSONContainer value = mapper.readValue(objectData, JSONContainer.class);
            new WorkflowManagerHelper(createTicket()).updateDatasetItemWithJson(id, name, rowId, value);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in updateDatasetItemWithJson: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in updateDatasetItemWithJson: " + e.getMessage(), e);
        }        
    }
    
    @POST
    @Path("/datasets/{id}/items/{name}/json")
    @Produces("application/json")
    public void updateDatasetItemWithJson(@PathParam("id")String id, @PathParam("name")String name, @FormParam("objectData")String objectData) throws ConnexienceException {
        try {
            JSONContainer value = mapper.readValue(objectData, JSONContainer.class);
            new WorkflowManagerHelper(createTicket()).updateDatasetItemWithJson(id, name, value);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in updateDatasetItemWithJson: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in updateDatasetItemWithJson: " + e.getMessage(), e);
        }        
    }
    
    @POST
    @Path("/datasets/{id}/items/{name}/data")
    @Produces("application/json")
    public void updateDatasetItemWithData(@PathParam("id")String id, @PathParam("name")String name, @FormParam("objectData")String objectData) throws ConnexienceException {
        try {
            JsonDataImporter importer = new JsonDataImporter(new JSONObject(objectData));
            new WorkflowManagerHelper(createTicket()).updateDatasetItemWithData(id, name, importer.toData());
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in updateDatasetItemWithJson: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in updateDatasetItemWithJson: " + e.getMessage(), e);
        }        
    }
    
    @POST
    @Path("/datasets/{id}/items/{name}/number")
    @Produces("application/json")
    public void updateDatasetItemWithNumber(@PathParam("id")String id, @PathParam("name")String name, @FormParam("objectData")String objectData) throws ConnexienceException {
        try {
            Number value = mapper.readValue(objectData, Number.class);
            new WorkflowManagerHelper(createTicket()).updateDatasetItemWithNumber(id, name, value);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in updateDatasetItemWithNumber: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in updateDatasetItemWithNumber: " + e.getMessage(), e);
        }        
    }    
    
    @GET
    @Path("/datasets/{id}/items/{name}/contents")
    @Produces("application/json")
    public JSONContainer queryDatasetItem(@PathParam("id")String datasetId, @PathParam("name")String itemName) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).queryDatasetItem(datasetId, itemName);
    }
    
    @POST
    @Path("/dataqueries")
    @Produces("application/json")
    public JSONContainer queryDatasetItem(@FormParam("queryData")String queryData) throws ConnexienceException {
        try {
            DatasetQuery query = mapper.readValue(queryData, DatasetQuery.class);
            return new WorkflowManagerHelper(createTicket()).queryDatasetItem(query);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in queryDatasetItem: " + ce.getMessage(), ce);
        } catch (Exception e){
            throw new ConnexienceException("Exception in queryDatasetItem: " + e.getMessage(), e);
        }
    }
    
    @GET
    @Path("/folders/{id}/namedsubdirectories/{name}")
    @Produces("application/json")
    public Folder getNamedSubdirectory(@PathParam("id")String id, @PathParam("name")String name) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getNamedSubdirectory(id, name);
    }

    @POST
    @Path(value = "/documents/{id}/links")
    @Produces(value="application/json")
    @Consumes(value = "application/json")
    public Link addDocumentLink(@PathParam(value = "id") String sourceDocumentId, String targetDocumentId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).addDocumentLink(sourceDocumentId, targetDocumentId);

    }

    @GET
    @Path("/documents/{id}/links")
    @Produces("application/json")
    public DocumentRecord[] getDocumentLinks(@PathParam("id")String id) throws ConnexienceException {
       return new WorkflowManagerHelper(createTicket()).getDocumentLinks(id);
    }
    
    @GET
    @Path("/workflows/locks/{id}")
    @Produces("application/json")
    public WorkflowLock getWorkflowLock(@PathParam("id")long lockId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getWorkflowLock(lockId);
    }
    
    @GET
    @Path("/studydeployments/{studycode}/{phase}/{loggerserialnumber}")
    @Produces("application/json")
    public Integer getDeploymentId(@PathParam("studycode")String studyCode, @PathParam("phase") String phase, @PathParam("loggerserialnumber")String loggerSerialNumber) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDeploymentId(studyCode, phase, loggerSerialNumber);
    }
   
    @GET
    @Path("/deployments/{deploymentId}/datafolder")
    @Produces("application/json")
    public Folder getDeploymentFolder(@PathParam("deploymentId")Integer deploymentId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getDeploymentFolder(deploymentId);
    }
    
    @POST
    @Path("/deployments/{deploymentId}/data")
    @Produces("application/json")
    @Consumes("text/plain")
    public void addDataToDeployment(@PathParam("deploymentId")Integer deploymentId, @FormParam("documentRecordId")String documentRecordId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).addDataToDeployment(deploymentId, documentRecordId);
    }
    
    @POST
    @Path("/datasets")
    @Produces("application/json")
    public Dataset saveDataset(@FormParam("datasetData")String datasetData) throws ConnexienceException {
        try {
            Dataset ds = mapper.readValue(datasetData, Dataset.class);
            return new WorkflowManagerHelper(createTicket()).saveDataset(ds);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error saving dataset: " + ce.getMessage(), ce);
        } catch (Exception e){
            throw new ConnexienceException("Exception saving dataset: " + e.getMessage(), e);
        }
    }
    
    @POST
    @Path("/datasetitems")
    @Produces("application/json")
    public DatasetItem saveDatasetItem(@FormParam("datasetItemData")String itemData) throws ConnexienceException {
        try {
            DatasetItem item = mapper.readValue(itemData, DatasetItem.class);
            return new WorkflowManagerHelper(createTicket()).saveDatasetItem(item);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error saving dataset item: " + ce.getMessage(), ce);
        } catch (Exception e){
            throw new ConnexienceException("Exception saving dataset item: " + e.getMessage(), e);
        }
    }
    
    @POST
    @Path("/newusers")
    @Produces("application/json")
    public User createAccount(@FormParam("firstName")String firstName, @FormParam("surname")String surname, @FormParam("logon")String logon, @FormParam("password")String password) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).createAccount(firstName, surname, logon, password);
    }
    
    @POST
    @Path("/newgroups")
    @Produces("application/json")
    public Group createGroup(@FormParam("name")String groupName, @FormParam("ownerapprovalrequired")boolean ownerApprovalRequired, @FormParam("nonmembersview")boolean nonMembersView) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).createGroup(groupName, ownerApprovalRequired, nonMembersView);
    }
    
    @GET
    @Path("/usertickets/{userid}")
    @Produces("application/json")
    public Ticket createTicketForUser(@PathParam("userid")String userId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).createTicketForUser(userId);
    }
    
    @GET
    @Path("/userdatasetsbyname/{name}/items/{itemname}")
    @Produces("application/json")
    public DatasetItem lookupUserDatasetItemByName(@PathParam("name")String datasetName, @PathParam("itemname")String itemName) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).lookupUserDatasetItemByName(datasetName, itemName);
    }
    
    @GET
    @Path("/datasets/{id}/items")
    @Produces("application/json")
    public DatasetItem[] listDatasetItems(@PathParam("id")String datasetId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listDatasetItems(datasetId);
    }
    
    @GET
    @Path("/userdatasetownership/{name}")
    @Produces("application/json")
    public boolean userHasNamedDataset(@PathParam("name")String datasetName) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).userHasNamedDataset(datasetName);
    }
    
    @GET
    @Path("/userdatasetsbyname/{name}")
    @Produces("application/json")
    public Dataset lookupUserDatasetByName(@PathParam("name")String datasetName) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).lookupUserDatasetByName(datasetName);
    }
    
    @GET
    @Path("/groupsbyname/{name}")
    @Produces("application/json")
    public Group getGroupByName(@PathParam("name")String name) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getGroupByName(name);
    }
    
    @POST
    @Path("/groups/{id}/membership")
    @Produces("application/json")
    public void addUserToGroup(@PathParam("id")String groupId, @FormParam("userId")String userId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).addUserToGroup(groupId, userId);
    }
    
    @DELETE
    @Path("/groups/{id}/membership/{userId}")
    public void removeUserFromGroup(@PathParam("id")String groupId, @PathParam("userId")String userId) throws ConnexienceException {
        new WorkflowManagerHelper(createTicket()).removeUserFromGroup(groupId, userId);
    }    
    
    @GET
    @Path("/serverobjects/{id}")
    public ServerObject getServerObject(@PathParam("id")String objectId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getServerObject(objectId);
    }
    
    @GET
    @Path("/projects/{id}")
    public WorkflowProject getProject(@PathParam("id")int projectId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getProject(projectId);
    }
    
    @GET
    @Path("/scanners{id}")
    public WorkflowFilesystemScanner getScanner(@PathParam("id")long scannerId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getScanner(scannerId);
    }
    
    @GET
    @Path("/serverproperties")
    @Produces("application/json")
    public String getAllServerProperties() throws ConnexienceException {
        try {
            XmlDataStore store = new WorkflowManagerHelper(createTicket()).getAllServerProperties();
            XmlDataStoreStreamWriter writer = new XmlDataStoreStreamWriter(store);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            writer.write(stream);
            stream.flush();
            stream.close();
            return new String(stream.toByteArray());
        } catch (Exception e){
            throw new ConnexienceException("Error getting all server properties: " + e.getMessage());
        }
    }
    
    @GET
    @Path("/datasetitemids/{datasetId}/{name}")
    @Produces("application/json")
    public long lookupDatasetItemId(@PathParam("datasetId")String datasetId, @PathParam("name")String itemName) throws ConnexienceException {
        try {
            return new WorkflowManagerHelper(createTicket()).lookupDatasetItemId(datasetId, itemName);
        } catch (Exception e){
            throw new ConnexienceException("Error looking up dataset item id: " + e.getMessage(), e);
        }
    }
    
    @GET
    @Path("/studies/{id}/stats")
    @Produces("application/json")
    public HashMap getStudyStats(@PathParam("id")int studyId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getStudyStats(studyId);
    }
    
    @GET
    @Path("/studies/{id}")
    @Produces("application/json")
    public WorkflowStudy getStudy(@PathParam("id")int studyId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getStudy(studyId);
    }
    
    @GET
    @Path("/studies/{id}/phases")
    @Produces("application/json")
    
    public WorkflowStudyPhase[] listStudyPhases(@PathParam("id")int studyId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listStudyPhases(studyId);
    }
    
    @GET
    @Path("/studyphases/{id}/groups")
    @Produces("application/json")
    public WorkflowStudySubjectGroup[] listPhaseGroups(@PathParam("id")int phaseId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listPhaseGroups(phaseId);
    }
    
    @GET
    @Path("/studygroups/{id}/children")
    @Produces("application/json")
    public WorkflowStudySubjectGroup[] listChildGroups(@PathParam("id")int parentGroupId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listChildGroups(parentGroupId);
    }
    
    @GET
    @Path("/studygroups/{id}/subjects")
    @Produces("application/json")    
    public WorkflowStudySubject[] listGroupSubjects(@PathParam("id")int parentGroupId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listGroupSubjects(parentGroupId);
    }
    
    @GET
    @Path("/studygroups/{id}/deployments")
    @Produces("application/json")    
    public WorkflowStudyDeployment[] listGroupDeployments(@PathParam("id")int parentGroupId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).listGroupDeployments(parentGroupId);
    }
    
    @GET
    @Path("/studyloggers/{id}")
    @Produces("application/json")
    public WorkflowStudyLogger getLogger(@PathParam("id")int loggerId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getLogger(loggerId);
    }
    
    @GET
    @Path("/folderstudyobjects/{id}")
    @Produces("application/json")
    public WorkflowStudyObject getStudyObjectAssociatedWithFolder(@PathParam("id")String folderId) throws ConnexienceException {
        return new WorkflowManagerHelper(createTicket()).getStudyObjectAssociatedWithFolder(folderId);
    }
    
    @POST
    @Path("/studyobjectupdates")
    @Produces("application/json")
    public void updateStudyObjectProperties(@FormParam("studyobject")String studyObjectData) throws ConnexienceException {
        try {
            WorkflowStudyObject obj = mapper.readValue(studyObjectData, WorkflowStudyObject.class);
            new WorkflowManagerHelper(createTicket()).updateStudyObjectProperties(obj);
        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error in updateStudyObjectProperties: " + ce.getMessage(), ce);
        } catch (Exception e) {
            throw new ConnexienceException("Exception in updateStudyObjectProperties: " + e.getMessage(), e);
        }           
    }
}
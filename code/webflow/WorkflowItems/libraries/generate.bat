@echo off

mvn archetype:generate ^
	-DarchetypeGroupId=com.connexience ^
	-DarchetypeArtifactId=workflow-library ^
	-DarchetypeVersion=3.1-SNAPSHOT ^
	-DgroupId=com.connexience.libraries ^
	-DartifactId=r-bin ^
	-Dversion=1.0

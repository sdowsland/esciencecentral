/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.workflow.util;

import com.connexience.server.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import com.connexience.server.workflow.xmlstorage.*;

public class WorkflowParameterBuilder {
    private Data propertyData = null;
    private StringPairListWrapper propertyMappings = null;
    private API api;
    private boolean convertDoubleColumns = true;
    
    public WorkflowParameterBuilder(API api){
        this.api = api;
    }
    
    public void setPropertyData(Data propertyData) throws Exception {
        if(convertDoubleColumns){
            this.propertyData = convertToInteger(propertyData);
        } else {
            this.propertyData = propertyData;
        }
    }
    
    public void setPropertyMappings(StringPairListWrapper propertyMappings){
        this.propertyMappings = propertyMappings;   
    }
    
    public void setConvertDoubleColumns(boolean convertDoubleColumns){
        this.convertDoubleColumns = convertDoubleColumns;
    }
    
    public IWorkflowParameterList build(int rowNumber) throws Exception {
        IWorkflowParameterList params = (IWorkflowParameterList)api.createObject(IWorkflowParameterList.XML_NAME);
        
        Data row = propertyData.getRowSubset(rowNumber, rowNumber, true);
        Column col;
        String blockName;
        String propertyName;
        String propertyValue;
        
        if(row.getColumns()==propertyMappings.getSize()){
            for(int i=0;i<row.getColumns();i++){
                blockName = propertyMappings.getValue(i, 0);
                propertyName = propertyMappings.getValue(i, 1);
                if(!row.column(i).isMissing(0)){
                    propertyValue = row.column(i).getCxDFormatValue(0);
                    params.add(blockName, propertyName, propertyValue);   
                    System.out.println("Set: " + blockName + "." + propertyName + "=" + propertyValue);
                }
            }
            
		} else {
			throw new Exception(
				"Number of property columns: " + row.getColumns() + 
				" does not match the number of mapping entries: " + propertyMappings.getSize());
		}

		return params;
	}
    
    public Data convertToInteger(Data source) throws Exception {
        Data output = new Data();
        Column original;
        Column converted;
        
        for(int i=0;i<source.getColumns();i++){
            original = source.column(i);
            
            if(original instanceof DoubleColumn){
                converted = new IntegerColumn(original.getName());
                for(int j=0;j<original.getRows();j++){
                    if(!original.isMissing(j)){
                        ((IntegerColumn)converted).appendIntValue((int)((DoubleColumn)original).getDoubleValue(j));
                    } else {
                        converted.appendObjectValue(MissingValue.get());
                    }   
                }
            } else {
                converted = original.getCopy();
            }    
            output.addColumn(converted);
        }
                
        return output;    
    }
}
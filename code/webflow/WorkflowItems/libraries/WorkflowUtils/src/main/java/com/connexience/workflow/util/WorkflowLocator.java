/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.workflow.util;

import com.connexience.server.api.*;
import java.util.*;

public class WorkflowLocator {
    private API api;
    
    public WorkflowLocator(API api){
        this.api = api;   
    }
        
    public IWorkflow findWorkflowByName(String name) throws Exception {
        List<IWorkflow> workflows = api.listWorkflows();
        for(int i=0;i<workflows.size();i++){
            if(workflows.get(i).getName().equals(name)){
                return workflows.get(i);   
            }   
        }
        return null;
    }
    
    public IWorkflow findWorkflowById(String id) throws Exception {
        List<IWorkflow> workflows = api.listWorkflows();
        for(int i=0;i<workflows.size();i++){
            if(workflows.get(i).getId().equals(id)){
                return workflows.get(i);   
            }   
        }
        return null;
    }
}
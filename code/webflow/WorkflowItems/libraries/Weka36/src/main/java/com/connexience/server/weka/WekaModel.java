
package com.connexience.server.weka;

import java.io.Serializable;
import weka.classifiers.Classifier;

/** This abstract class is used as a container for a Weka 'Classifier' object 
 * (i.e. a classification or regression model). 
 *  
 * Concrete implemenations of this container also store other information
 * about the model (e.g. class labels), that would otherwise not be accessible
 * from the Classifier model itself. 
 * This is necessary because - in esc - the model is often serialised
 * and passed between blocks but the original training data Instances are not,
 * and Weka uses Instances - not the model - to store class label information.
 * 
 * NOTE: This is not a concrete class. The subclasses WekaRegressionModel and 
 * WekaClassificationModel should be used in practice.
 *
 * @author Dominic Searson 14/8/2014
 */
public abstract class WekaModel implements Serializable {
    
    /* The Weka Classifier object (i.e. a classification or regression model) */
    private Classifier classifier;

    public Classifier getClassifier() {
        return classifier;
    }

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }
       
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.weka;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This class converts a set of Weka instances into an e-SC data object
 * @author hugo
 */
public class InstancesToData {
    Instances wekaInstances;

    public InstancesToData(Instances wekaInstances) {
        this.wekaInstances = wekaInstances;
    }
    
    public Data toData() throws Exception {
        
        Data data = new Data();
        
        
        String dataName = wekaInstances.relationName();
        if ( (dataName != null) && (!dataName.isEmpty()) ) {
            data.createEmptyProperties();
            data.setName(dataName);
        }
        
        int attributeCount = wekaInstances.numAttributes();
        Attribute attr;
       
        Column c;
        for(int i=0;i<attributeCount;i++){
            attr = wekaInstances.attribute(i);
            if(attr.isNumeric()){
                c = new DoubleColumn(attr.name());
            } else {
                c = new StringColumn(attr.name());
            }
            data.addColumn(c);
        }
        
        Instance instance;
        
        for(int i=0;i<wekaInstances.numInstances();i++){
            instance = wekaInstances.instance(i);
            for(int j=0;j<instance.numAttributes();j++){
                c = data.column(j);
                if(c instanceof NumericalColumn && instance.attribute(j).isNumeric()){
                    ((NumericalColumn)c).appendDoubleValue(instance.value(j));
                } else {
                    c.appendStringValue(instance.stringValue(j));
                }
            }
        }
        
        return data;
    }
}
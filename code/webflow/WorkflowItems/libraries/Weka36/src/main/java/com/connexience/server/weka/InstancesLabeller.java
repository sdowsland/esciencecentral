/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.weka;

import org.pipeline.core.data.manipulation.ColumnPicker;
import weka.core.Instances;

/**
 * This class labels a Weka Instances object with an output variable definition.
 * This class lets instances be used for modelling in a consistent way.
 * @author hugo
 */
public class InstancesLabeller {
    /** Column selection definition. This is of the standard #xx or column name
     * format.*/
    private String columnDefinition;

    public InstancesLabeller(String columnDefinition) {
        this.columnDefinition = columnDefinition;
    }
    
    /** Label a set of instances with an output variable */
    public void labelInstances(Instances wekaInstances) throws Exception {
        ColumnPicker picker = new ColumnPicker(columnDefinition);
        if(picker.getPickType()==ColumnPicker.PICK_BY_NUMBER){
            wekaInstances.setClassIndex(picker.getColumnIndex());
        } else {
            wekaInstances.setClass(wekaInstances.attribute(picker.getColumnName()));
        }
    }
}
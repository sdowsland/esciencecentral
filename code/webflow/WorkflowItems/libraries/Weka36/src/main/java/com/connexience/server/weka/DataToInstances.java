/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.weka;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.ColumnPicker;
import weka.core.*;

/**
 * This class converts an e-SC Data object (DataWrapper) into a Weka Instances class
 * @author hugo
 */
public class DataToInstances {
    /** Data being converted */
    private Data data;

    public DataToInstances(Data data) {
        this.data = data;
    }
    
    /** Convert all of the data in the data set to a collection of Weka instances. */
    public Instances toWekaInstances() throws Exception {
        FastVector attInfo = new FastVector();
        
        // Create the attributes for the data
        Column c;
        int rows = data.getLargestRows();
        int cols = data.getColumns();

        for(int i=0;i<cols;i++){
            c = data.column(i);
            if(c instanceof NumericalColumn){
                // Number column
                attInfo.addElement(new Attribute(c.getName()));
            } else {
                // Treat as text
                attInfo.addElement(new Attribute(c.getName(), (FastVector)null));
            }
        }
        
        // Add the individual rows
        Instances wekaInstances = new Instances("DataSet", attInfo, rows);
        double[] vals;
        for(int i=0;i<rows;i++){
            vals = new double[cols];
            for(int j=0;j<cols;j++){
                c = data.column(j);
                if(!c.isMissing(i)){
                    if(c instanceof NumericalColumn){
                        vals[j] = ((NumericalColumn)c).getDoubleValue(i);
                    } else {
                        //vals[2] = data.attribute(2).addStringValue("And another one!");
                        vals[j] = wekaInstances.attribute(j).addStringValue(c.getStringValue(i));
                    }
                } else {
                    // Set as the Weka missing value
                    vals[j] = Instance.missingValue();
                }
            }
            wekaInstances.add(new Instance(1.0, vals));
        }

        return wekaInstances;
    }
    
    /** Create a set of instances with a class column index */
    public Instances toWekaInstances(int classColumn) throws Exception {
        Instances wekaInstances = toWekaInstances();
        wekaInstances.setClassIndex(classColumn);
        return wekaInstances;
    }
    
    /** Create a set of instances with a column picker definition */
    public Instances toWekaInstances(String columnDefinition) throws Exception {
        Instances wekaInstances = toWekaInstances();
        ColumnPicker picker = new ColumnPicker(columnDefinition);
        if(picker.getPickType()==ColumnPicker.PICK_BY_NUMBER){
            wekaInstances.setClassIndex(picker.getColumnIndex());
        } else {
            int index = data.getColumnIndex(picker.getColumnName());
            if(index!=-1){
                wekaInstances.setClassIndex(index);
            } else {
                throw new Exception("Column: " + picker.getColumnName() + " not in data");
            }
        }        
        return wekaInstances;
    }
}
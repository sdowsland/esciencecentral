/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.apps.neuralnetwork.blocks.ffann;
import org.pipeline.apps.neuralnetwork.ffann.*;

import org.pipeline.core.drawing.*;

/**
 * This class provides a transferrable wrapper for a FFANN model
 * @author hugo
 */
public class FFANNModelDataTypeWrapper implements TransferData {
    /** Static reference so only one copy exists */
    public static final FFANNModelDataType FFANN_MODEL_DATA_TYPE = new FFANNModelDataType();
    
    /** Object payload */
    private FFANNModel model;
    
    /**
     * Creates a new instance of FFANNModelDataTypeWrapper 
     */
    public FFANNModelDataTypeWrapper(FFANNModel model) {
        this.model = model;
    }
    
    /** Get the payload from this object */
    public Object getPayload(){
        return model.getCopy();
    }
    
    /** Get a copy of this object */
    public TransferData getCopy(){
        return new FFANNModelDataTypeWrapper(model.getCopy());
    }
}

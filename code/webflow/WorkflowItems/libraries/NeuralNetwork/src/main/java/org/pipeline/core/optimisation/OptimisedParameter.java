/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;

/**
 * This class represents a parameter that can be adjusted by the optimiser. It
 * provides methods to specify the suggested maximum and minimum values and 
 * whether to enforce these ranges.
 * @author nhgh
 */
public class OptimisedParameter {
    /** Maximum value */
    private double maximumValue = Double.MAX_VALUE;
    
    /** Minimum value */
    private double minimumValue = Double.MIN_VALUE;
    
    /** Enforce range */
    private boolean enforceRange = false;
    
    /** Current value */
    private double currentValue = 0;
    
    /** Parameter label */
    private String label;
    
    /** Creates a new instance of OptimisedParameter */
    public OptimisedParameter() {
    }

    /** Create with a value */
    public OptimisedParameter(double value) {
        this.currentValue = value;
    }
    
    /** Create from an existing parameter */
    public OptimisedParameter(OptimisedParameter original){
        this.maximumValue = original.getMaximumValue();
        this.minimumValue = original.getMinimumValue();
        this.enforceRange = original.isEnforceRange();
        this.currentValue = original.getCurrentValue();
        this.label = original.getLabel();
    }
    
    /** Get the maximum value */
    public double getMaximumValue() {
        return maximumValue;
    }

    /** Set the maximum permitted value */
    public void setMaximumValue(double maximumValue) {
        this.maximumValue = maximumValue;
    }

    /** Get the minimum permitted value */
    public double getMinimumValue() {
        return minimumValue;
    }

    /** Set the minimum permitted value */
    public void setMinimumValue(double minimumValue) {
        this.minimumValue = minimumValue;
    }

    /** Is range enforcing enabled */
    public boolean isEnforceRange() {
        return enforceRange;
    }

    /** Set whether range enforcing is enabled */
    public void setEnforceRange(boolean enforceRange) {
        this.enforceRange = enforceRange;
    }

    /** Get the current parameter value */
    public double getCurrentValue() {
        return currentValue;
    }

    /** Set the current parameter value */
    public void setCurrentValue(double currentValue) {
        if(enforceRange){
            // Do range enforcement
            if(currentValue < minimumValue){
                this.currentValue = minimumValue;
                
            } else if(currentValue > maximumValue){
                this.currentValue = maximumValue;
                
            } else {
                this.currentValue = currentValue;
            }
            
        } else {
            this.currentValue = currentValue;
        }
    }

    /** Get the parameter label */
    public String getLabel() {
        return label;
    }

    /** Set the parameter label */
    public void setLabel(String label) {
        this.label = label;
    }
    
}
/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation.gui;
import org.pipeline.core.optimisation.*;
import javax.swing.DefaultComboBoxModel;
import org.pipeline.core.xmlstorage.*;
import org.pipeline.gui.xmlstorage.*;
import java.util.*;

/**
 * This class provides a toolbar that can be used to control an optimisation
 * process.
 * @author  nhgh
 */
public class OptimiserToolbar extends javax.swing.JToolBar implements OptimiserListener {
    /** Optimisation controller managing the optimisation */
    private OptimisationController controller;
    
    /** Creates new form BeanForm */
    public OptimiserToolbar() {
        initComponents();
        listOptimisers();
        setButtonStatus();
    }

    /** Set the controller */
    public void setController(OptimisationController newController){
        // Unregister existing listener if there is one
        if(this.controller!=null){
            controller.removeOptimiserListener(this);
        }
        
        this.controller = newController;
        
        // Set up the selected optimiser 
        if(controller!=null){ 
            controller.addOptimiserListener(this);
            if(controller.getOptimiser()==null){
                try {
                    controller.setOptimiser(OptimiserFactory.createDefaultOptimiser());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            
            if(controller.getOptimiser()!=null){
                OptimiserInfo info = OptimiserFactory.getInfoForOptimiser(controller.getOptimiser());
                if(info!=null){
                    optimiserList.setSelectedItem(info);
                }
            }
        }
        setButtonStatus();
    }

    /** Remove listeners during GC */
    protected void finalize() throws Throwable {
        if(controller!=null){
            controller.removeOptimiserListener(this);
        }
    }
    
    /** Set the correct button status */
    private void setButtonStatus(){
        if(controller!=null){
            if(controller.getOptimiser()!=null){
                if(controller.getOptimiser().getStatus()==Optimiser.RUNNING){
                    // Optimiser is running
                    startButton.setEnabled(false);
                    resetButton.setEnabled(false);
                    stopButton.setEnabled(true);
                    optimiserList.setEnabled(false);
                    propertiesButton.setEnabled(false);
                    
                } else {
                    // Optimiser is idle
                    startButton.setEnabled(true);
                    resetButton.setEnabled(true);
                    stopButton.setEnabled(false);
                    optimiserList.setEnabled(true);
                    propertiesButton.setEnabled(true);
                }
                
            } else {
                startButton.setEnabled(false);
                resetButton.setEnabled(false);
                stopButton.setEnabled(false);
                optimiserList.setEnabled(false);
                propertiesButton.setEnabled(false);
            }
        }
    }
    
    /** Build the optimiser list */
    private void listOptimisers(){
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        Enumeration e = OptimiserFactory.getOptimisers();
        while(e.hasMoreElements()){
            model.addElement(e.nextElement());
        }
        optimiserList.setModel(model);
    }
    
    public void optimiserIterationCompleted(int iteration) {
    }

    public void optimiserImprovedError(int iteration, double error) {
    }

    /** Optimiser has been reset */
    public void optimiserReset() {
    }    
    
    /** Optimsier has stopped or been stopped */
    public void optimiserStopped() {
        setButtonStatus();
    }

    /** Optimiser has started */
    public void optimiserStarted() {
        setButtonStatus();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        startButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        propertiesButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        optimiserLabel = new javax.swing.JLabel();
        optimiserList = new javax.swing.JComboBox();

        setRollover(true);
        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/pipeline/core/optimisation/resource/Play16.gif")));
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/pipeline/core/optimisation/resource/Messages"); // NOI18N
        startButton.setText(bundle.getString("START_CAPTION")); // NOI18N
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        add(startButton);

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/pipeline/core/optimisation/resource/Stop16.gif")));
        stopButton.setText(bundle.getString("STOP_CAPTION")); // NOI18N
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });

        add(stopButton);

        resetButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/pipeline/core/optimisation/resource/Refresh16.gif")));
        resetButton.setText(bundle.getString("RESET_CAPTION")); // NOI18N
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        add(resetButton);

        propertiesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/pipeline/core/optimisation/resource/Edit16.gif")));
        propertiesButton.setToolTipText("Set Optimiser Properties");
        propertiesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                propertiesButtonActionPerformed(evt);
            }
        });

        add(propertiesButton);

        jPanel1.setLayout(new java.awt.BorderLayout());

        optimiserLabel.setText(bundle.getString("SELECT_CAPTION")); // NOI18N
        jPanel1.add(optimiserLabel, java.awt.BorderLayout.WEST);

        optimiserList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optimiserListActionPerformed(evt);
            }
        });

        jPanel1.add(optimiserList, java.awt.BorderLayout.CENTER);

        add(jPanel1);

    }// </editor-fold>//GEN-END:initComponents

    private void propertiesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_propertiesButtonActionPerformed
        if(controller!=null && controller.isRunning()==false && controller.getOptimiser()!=null){
            XmlDataStore properties = controller.getOptimiser().getProperties();
            XmlDataStoreEditorDialog dialog = new XmlDataStoreEditorDialog(properties);
            dialog.setTitle("Optimiser Settings");
            dialog.setVisible(true);
        }
    }//GEN-LAST:event_propertiesButtonActionPerformed

    /** Change the optimiser */
    private void optimiserListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optimiserListActionPerformed
        if(controller!=null && controller.isRunning()==false){
            OptimiserInfo info = (OptimiserInfo)optimiserList.getSelectedItem();
            try {
                if(!info.getId().equals(OptimiserFactory.getInfoForOptimiser(controller.getOptimiser()).getId())){
                    Optimiser optimiser = OptimiserFactory.createOptimiser(info);
                    controller.setOptimiser(optimiser);
                }
            } catch (Exception e){
                javax.swing.JOptionPane.showMessageDialog(this, "Cannot create Optimiser", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_optimiserListActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        if(controller!=null && controller.getOptimiser()!=null){
            controller.resetOptimiser();
        }
    }//GEN-LAST:event_resetButtonActionPerformed

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
        if(controller!=null && controller.getOptimiser()!=null){
            controller.stopOptimiser();
        }
    }//GEN-LAST:event_stopButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        if(controller!=null && controller.getOptimiser()!=null && controller.getOptimiser().getStatus()!=Optimiser.RUNNING){
            controller.startOptimiser();
        }
    }//GEN-LAST:event_startButtonActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel optimiserLabel;
    private javax.swing.JComboBox optimiserList;
    private javax.swing.JButton propertiesButton;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton startButton;
    private javax.swing.JButton stopButton;
    // End of variables declaration//GEN-END:variables
    
}

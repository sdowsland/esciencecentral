/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;

import org.pipeline.core.xmlstorage.*;

import java.util.*;

/**
 * This class controls the optimisation process. It holds the OptimisableObject
 * that is going to have its parameters adjusted and an Optimiser class. It
 * also holds the current best set of parameters and can deliver events to
 * OptimiserListener objects.
 * @author hugo
 */
public class OptimisationController implements XmlStorable {
    /** Optimiser engine */
    private Optimiser optimiser = null;
    
    /** Problem being solved */
    private OptimisableObject target = null;
    
    /** Listeners to this optimiser */
    private Vector listeners = new Vector();
    
    /** Creates a new instance of OptimisationController */
    public OptimisationController() {
    }
    
    /** Add a listener */
    public void addOptimiserListener(OptimiserListener l){
        if(!listeners.contains(l)){
            listeners.addElement(l);
        }
    }
    
    /** Remove a listener */
    public void removeOptimiserListener(OptimiserListener l){
        if(listeners.contains(l)){
            listeners.removeElement(l);
        }
    }
    
    /** Notify a reset */
    protected void notifyReset(){
        Enumeration e = listeners.elements();
        while(e.hasMoreElements()){
            ((OptimiserListener)e.nextElement()).optimiserReset();
        }
    }
    
    /** Notify an inproved error */
    protected void notifyImprovedError(int iteration, double error){
        Enumeration e = listeners.elements();
        while(e.hasMoreElements()){
            ((OptimiserListener)e.nextElement()).optimiserImprovedError(iteration, error);
        }
    }
    
    /** Notify iteration finished */
    protected void notifyIteration(int iteration){
        Enumeration e = listeners.elements();
        while(e.hasMoreElements()){
            ((OptimiserListener)e.nextElement()).optimiserIterationCompleted(iteration);
        }
    }
    
    /** Notify started */
    protected void notifyStarted(){
        Enumeration e = listeners.elements();
        while(e.hasMoreElements()){
            ((OptimiserListener)e.nextElement()).optimiserStarted();
        }
        
    }
    
    /** Notify stopped */
    protected void notifyStopped(){
        Enumeration e = listeners.elements();
        while(e.hasMoreElements()){
            ((OptimiserListener)e.nextElement()).optimiserStopped();
        }        
    }
    
    /** Set the target */
    public void setTarget(OptimisableObject target){
        this.target = target;
        if(optimiser!=null){
            optimiser.setTarget(target);
        }
    }
    
    /** Get the optimiser */
    public Optimiser getOptimiser(){
        return optimiser;
    }
    
    /** Set the optimiser */
    public void setOptimiser(Optimiser optimiser){
        this.optimiser = optimiser;
        optimiser.setParent(this);
        optimiser.setTarget(target);
    }
    
    /** Set the optimiser by creating a new one */
    public void setOptimiser(OptimiserInfo info) throws OptimisationException {
        setOptimiser(OptimiserFactory.createOptimiser(info));
    }
    
    /** Set the optimiser by creating a new one */
    public void setOptimiser(String id) throws OptimisationException {
        setOptimiser(OptimiserFactory.createOptimiser(id));
    }
    
    /** Start the optimiser */
    public void startOptimiser(){
        if(optimiser!=null){
            if(optimiser.getStatus()==Optimiser.STOPPED){
                Thread optimisationThread = new Thread(optimiser);
                optimisationThread.setPriority(Thread.MIN_PRIORITY);
                optimisationThread.start();
            }
        }
    }
    
    /** Stop the optimiser */
    public void stopOptimiser(){
        if(optimiser!=null){
            optimiser.setStopFlag(true);
        }
    }
    
    /** Reset everything */
    public void resetOptimiser(){
        if(optimiser!=null){
            stopOptimiser();
            if(optimiser.getTarget()!=null){
                optimiser.getTarget().reset();
            }
            optimiser.initialiseBestParameters();
            optimiser.notifyReset();
        }
    }
    
    /** Is the optimiser running */
    public boolean isRunning(){
        if(optimiser!=null){
            if(optimiser.getStatus()==Optimiser.RUNNING){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /** Save the optimiser and settings */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("OptimisationController");
        if(optimiser!=null){
            store.add("OptimiserType", OptimiserFactory.getInfoForOptimiser(optimiser).getId());
            store.add("OptimiserProperties", optimiser.getProperties());
        } else {
            store.add("OptimiserType", "NONE");
            store.add("OptimiserProperties", new XmlDataStore());
        }
        return store;
    }

    /** Load from storage and setup the optimiser */
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        String optimiserId = xmlDataStore.stringValue("OptimiserType", "");
        XmlDataStore optimiserProperties = xmlDataStore.xmlDataStoreValue("OptimiserProperties");
        try {
            optimiser = OptimiserFactory.createOptimiser(optimiserId);
            if(optimiser!=null){
                optimiser.getProperties().copyProperties(optimiserProperties);
            }
        } catch (Exception e){
            System.out.println("Cannot create optimiser: " + e.getMessage() + " creating default");
            try {
                optimiser = OptimiserFactory.createDefaultOptimiser();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}

/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation.simplex;
import org.pipeline.core.optimisation.*;
import org.pipeline.core.matrix.*;
import org.pipeline.core.xmlstorage.*;

/**
 * This is a very simple optimiser that used the Nelder-Mean simplex algorithm.
 * @author hugo
 */
public class SimplexOptimiser extends Optimiser {
    /** TODO: Move these */
    private double SIMPLEX_FTOL=0.00001;
    private int maxIterations = Integer.MAX_VALUE;
    
    private double SIMPLEX_ALPHA = 1;
    private double SIMPLEX_GAMMA = 2;
    private double SIMPLEX_BETAOPT = 0.5;
    private double SIMPLEX_PERTURBATION = 0.5;
    
    /** Parameter collection used to represent simplex */
    private OptimisedParameterMatrix parameterMatrix;
    
    /** Cost collection used to represent simplex */
    private Matrix costMatrix;
    
    /** Creates a new instance of SimplexOptimiser */
    public SimplexOptimiser() {
        super();
        
    }
    
    /** Get the default properties for this optimiser */
    public XmlDataStore getDefaultProperties(){
        XmlDataStore store = new XmlDataStore("SimplexProperties");
        store.add("Tolerance", 0.00001, "Algorithm Convergence Tolerance");
        store.add("Alpha", 1.0, "Simplex Alpha parameter");
        store.add("Gamma", 2.0, "Simplex Gamma parameter");
        store.add("Beta", 0.5, "Simplex Beta parameter");
        store.add("Perturbation", 0.5, "Simplex initial parameter perturbation factor");
        return store;
    }
    
    /** Load the simplex properties */
    private void loadProperties(){
        XmlDataStore properties = getProperties();
        SIMPLEX_FTOL = properties.doubleValue("Tolerance", 0.00001);
        SIMPLEX_ALPHA = properties.doubleValue("Alpha", 1.0);
        SIMPLEX_BETAOPT = properties.doubleValue("Beta", 0.5);
        SIMPLEX_GAMMA = properties.doubleValue("Gamma", 2.0);
        SIMPLEX_PERTURBATION = properties.doubleValue("Perturbation", 0.5);
        maxIterations = properties.intValue("MaxIterations", Integer.MAX_VALUE);        
    }
    /** Main optimisation method */
    public void performOptimisation() {
        // Get the properties 
        loadProperties();
        
        if(parameterMatrix!=null && costMatrix!=null && getTarget()!=null){
            runSimplex(parameterMatrix, costMatrix);
        }
    }
    
    /** Set up the optimiser for running */
    public void initialiseOptimiser() {       
        if(getTarget()!=null){
            loadProperties();
            int parameterCount = getTarget().getParameterCount();
            parameterMatrix = new OptimisedParameterMatrix(parameterCount + 1, parameterCount);
            costMatrix = new Matrix(parameterCount + 1, 1);
            
            // Get the initial (best) parameters from the superclass
            // And make random perturbations to create the intial
            // simplex.
            OptimisedParameterVector currentBest = null;
            if(containsBestParameters()){
                currentBest = getBestParameters();
            } else {
                initialiseBestParameters();
                currentBest = getBestParameters();
            }

            // Copy parameters
            for(int i=0;i<parameterCount + 1;i++){
                parameterMatrix.setRow(i, currentBest);
            }
            
            // Make small random changes to these parameters - don't perturb the initial estimates
            for(int i=0;i<parameterCount + 1;i++){
                for(int j=0;j<parameterCount;j++){
                    parameterMatrix.set(i, j, parameterMatrix.get(i, j) + (parameterMatrix.get(i, j) * SIMPLEX_PERTURBATION * ((Math.random() * 2) - 1)));
                }
            }
            
            // Calculate the costs associated with each of these parameter sets
            OptimisedParameterVector tempParams;
            for(int i=0;i<parameterCount + 1;i++){
                tempParams = parameterMatrix.extractRow(i);
                costMatrix.set(i, 0, getTarget().getCost(tempParams));
            }
        }
    }
    
    /** This method performs a Simplex fit on the data */  
    private void runSimplex(OptimisedParameterMatrix p, Matrix Y) {
        resetIterations();
        loadProperties();
        
        try {

            int NDIM = p.getColumnDimension();
            int ilo = 0;
            int ihi = 0;
            int inhi = 0;
            int mpts = NDIM + 1;
            int i = 0;
            int j = 0;
            double rtol = 0;
            OptimisedParameterVector tempMatrix = new OptimisedParameterVector(NDIM); // Temporary matrix for evaluating function
            double dTemp = 0;
            double ypr = 0;
            double yprr = 0;
            
            // Temporary arrays
            double[] pr = new double[NDIM + 1];
            double[] prr = new double[NDIM + 1];
            double[] pbar = new double[NDIM + 1];

            // AMOEBA routine from Numerical Recipes
            for (;;) {
                ilo = 0;

                if (Y.get(0, 0) > Y.get(1, 0)) {
                    ihi = 0;
                    inhi = 1;
                } else {
                    ihi = 1;
                    inhi = 0;
                }

                for (i = 0; i < mpts; i++) {
                    if (Y.get(i, 0) < Y.get(ilo, 0)) ilo = i;

                    if (Y.get(i, 0) > Y.get(ihi, 0)) {
                        inhi = ihi;
                        ihi = i;
                    } else if (Y.get(i, 0) > Y.get(inhi, 0)) {
                        if (i != ihi) inhi = i;
                    }
                }

                rtol = 2.0 * Math.abs(Y.get(ihi, 0) - Y.get(ilo, 0)) / (Math.abs(Y.get(ihi, 0)) + Math.abs(Y.get(ilo, 0)));
                
                if (rtol < SIMPLEX_FTOL || Double.isNaN(rtol)) {
                    System.out.println("Converged");
                    
                    // TODO: notifyEvent(OptimisationEvent.OPTIMISATION_CONVERGED);
                    setStatus(Optimiser.STOPPED);
                    break;
                }

                // Maximum number of iterations or stop flag has been set
                if (getIterations() >= maxIterations || getStopFlag() == true) {
                    break;
                }

                incrementIterations();

                for (j = 0; j < NDIM; j++) {
                    pbar[j] = 0;
                }

                for (i = 0; i < mpts; i++) {
                    if (i != ihi) {
                        for (j = 0; j < NDIM; j++) {
                            pbar[j] = pbar[j] + p.get(i, j);
                        }
                    }
                }

                for (j = 0; j < NDIM; j++) {
                    pbar[j] = pbar[j] / NDIM;
                    pr[j] = (1 + SIMPLEX_ALPHA) * pbar[j] - SIMPLEX_ALPHA * p.get(ihi, j);
                }

                // Copy parameters to temporary matrix
                for(i=0;i<NDIM;i++){
                    tempMatrix.set(i, pr[i]);
                }
                
                try {
                    ypr = getTarget().getCost(tempMatrix);
                } catch (Exception e) {
                    ypr = Double.POSITIVE_INFINITY;
                }

                if (ypr <= Y.get(ilo, 0)) {
                    for (j = 0; j < NDIM; j++) {
                        prr[j] = SIMPLEX_GAMMA * pr[j] + (1 - SIMPLEX_GAMMA) * pbar[j];
                    }

                    // Copy parameters to temporary matrix
                    for(i=0;i<NDIM;i++){
                        tempMatrix.set(i, prr[i]);
                    }
                    
                    try {
                        yprr = getTarget().getCost(tempMatrix);
                    } catch (Exception e) {
                        yprr = Double.POSITIVE_INFINITY;
                    }

                    if (yprr < Y.get(ilo, 0)) {
                        for (j = 0; j < NDIM; j++) {
                            p.set(ihi, j, prr[j]);
                        }
                        Y.set(ihi, 0, yprr);
                    } else {
                        for (j = 0; j < NDIM; j++) {
                            p.set(ihi, j, pr[j]);
                        }
                        Y.set(ihi, 0, ypr);
                    }
        
                    // Main
                } else if (ypr >= Y.get(inhi, 0)) {
                    
                    if (ypr < Y.get(ihi, 0)) {
                        for (j = 0; j < NDIM; j++) {
                            p.set(ihi, j, pr[j]);
                        }
                        Y.set(ihi, 0, ypr);
                    }
        
                    for (j = 0; j < NDIM; j++) {
                        prr[j] = SIMPLEX_BETAOPT * p.get(ihi, j) + (1 - SIMPLEX_BETAOPT) * pbar[j];
                    }

                    // Copy parameters to temporary matrix
                    for(i=0;i<NDIM;i++){
                        tempMatrix.set(i, prr[i]);
                    }
                    
                    try {
                        yprr = getTarget().getCost(tempMatrix);
                    } catch (Exception e) {
                        yprr = Double.POSITIVE_INFINITY;
                    }
                    
                    if (yprr < Y.get(ihi, 0)) {
                        for (j = 0; j < NDIM; j++) {
                            p.set(ihi, j, prr[j]);
                        }
                        Y.set(ihi, 0, yprr);
                      
                    } else {
                        for (i = 0; i < mpts; i++) {
                            if (i != ilo) {
                                for (j = 0; j < NDIM; j++) {
                                    pr[j] = 0.5 * (p.get(i, j) + p.get(ilo, j));
                                    p.set(i, j, pr[j]);
                                }

                                // Copy parameters to temporary matrix
                                for(i=0;i<NDIM;i++){
                                    tempMatrix.set(i, pr[i]);
                                }
                                
                                try {
                                    Y.set(i, 0, getTarget().getCost(tempMatrix));
                                } catch (Exception e) {
                                    Y.set(i, 0, Double.POSITIVE_INFINITY);
                                }
                            }
                        }
                    }
        
                    // Main
                } else {
                    for (j = 0; j < NDIM; j++) {
                        p.set(ihi, j, pr[j]);
                    }
                    Y.set(ihi, 0, ypr);
      
                    // Main
                }

                // Notify manager of iteration completion
                // TODO: notifyEvent(OptimisationEvent.OPTIMISATION_ITERATION_DONE);
                
                // Store if best so far and inform manager
                if (Y.get(ilo, 0) < getBestError()) {
                    setBestError(Y.get(ilo, 0));
                    setBestParameters(p.extractRow(ilo));
                    notifyImprovedError();
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    
}

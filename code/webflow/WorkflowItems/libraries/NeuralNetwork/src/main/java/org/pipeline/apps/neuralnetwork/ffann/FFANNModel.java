/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.apps.neuralnetwork.ffann;
import org.pipeline.core.matrix.*;
import org.pipeline.core.matrix.xmlstorage.*;
import org.pipeline.core.xmlstorage.*;

/**
 * This class provides a very basic Feed-forward neural network
 * with a single hidden layer containing sigmoidal transfer functions
 * bias terms and a linear output layer. No data scaling is done or
 * variable selection is done. This class just provides the actual
 * model. To train the network, the FFANNModelTrainer is needed, 
 * which wraps the network up in an OptimisableObject interface
 * and calculates error functions etc.
 * @author hugo
 */
public class FFANNModel implements XmlStorable {
    /** Number of hidden layer neurons */
    private int hiddenNeurons = 5;
    
    /** Number of inputs */
    private int inputs = 0;
    
    /** Number of outputs */
    private int outputs = 0;
    
    /** Input-hidden layer weights */
    private Matrix hiddenLayerWeights;
    
    /** Hidden-output layer weights */
    private Matrix outputLayerWeights;
            
    /** Hidden layer bias terms */
    private Matrix hiddenLayerBias;
    
    /** Total number of parameters */
    private int totalParameterCount = 0;
    
    /** Size of hidden layer parameters vector */
    private int hiddenLayerWeightSize = 0;
    
    /** Size of hidden bias terms parameter vector */
    private int hiddenBiasSize = 0;
    
    /** Size of output layer parameters vector */
    private int outputLayerWeightSize = 0;
     
    /**
     * Creates a new instance of FFANNModel 
     */
    public FFANNModel() {
    }

    /** Get a copy of this model */
    public FFANNModel getCopy(){
        FFANNModel model = new FFANNModel();
        model.setInputs(getInputs());
        model.setHiddenNeurons(getHiddenNeurons());
        model.setOutputs(getOutputs());
        model.calculateParameterCount();
        model.initialiseWeights();
        model.setParameterVector(getParameterVector());
        return model;
    }
    
    /** Get the number of hidden layer neurons */
    public int getHiddenNeurons() {
        return hiddenNeurons;
    }

    /** Set the number of hidden layer neurons */
    public void setHiddenNeurons(int hiddenNeurons) {
        this.hiddenNeurons = hiddenNeurons;
    }

    /** Get the number of input variables */
    public int getInputs() {
        return inputs;
    }

    /** Get the number of input variables */
    public void setInputs(int inputs) {
        this.inputs = inputs;
    }

    /** Get the number of output variables */
    public int getOutputs() {
        return outputs;
    }

    /** Set the number of output variables */
    public void setOutputs(int outputs) {
        this.outputs = outputs;
    }

    /** Get the hidden layer weights */
    public Matrix getHiddenLayerWeights() {
        return hiddenLayerWeights;
    }

    /** Get the output layer weights */
    public Matrix getOutputLayerWeights() {
        return outputLayerWeights;
    }

    /** Get the hidden layer bias weights */
    public Matrix getHiddenLayerBias() {
        return hiddenLayerBias;
    }

    /** Calculate the total number of parameters */
    protected void calculateParameterCount(){
        // Calculate sizes
        hiddenLayerWeightSize = inputs * hiddenNeurons;
        hiddenBiasSize = hiddenNeurons;
        outputLayerWeightSize = hiddenNeurons * outputs;
        totalParameterCount = hiddenLayerWeightSize + hiddenBiasSize + outputLayerWeightSize;
        
        // Construct empty matrices
        hiddenLayerWeights = new Matrix(inputs, hiddenNeurons);
        hiddenLayerBias = new Matrix(hiddenNeurons, 1);
        outputLayerWeights = new Matrix(hiddenNeurons, outputs);
    }
    
    /** Return the total number of parameters */
    public int getTotalParameterCount(){
        return totalParameterCount;
    }
    
    /** Randomise the weights */
    public void initialiseWeights(){
        calculateParameterCount();
        Matrix params = new Matrix(1, totalParameterCount);
        for(int i=0;i<totalParameterCount;i++){
            params.set(0, i, Math.random() - 0.5);
        }
        setParameterVector(params);
    }
    
    /** Set the parameter vector and unfold the data to the various weight matrices */
    public synchronized void setParameterVector(Matrix parameters){
        if(parameters.getRowDimension()==1 && parameters.getColumnDimension()==totalParameterCount){
            int count = 0;
            
            // Input - hidden layer
            for(int i=0;i<inputs;i++){
                for(int j=0;j<hiddenNeurons;j++){
                    hiddenLayerWeights.set(i, j, parameters.get(0, count));
                    count++;
                }
            }
            
            // Hidden layer biases
            for(int i=0;i<hiddenNeurons;i++){
                hiddenLayerBias.set(i, 0, parameters.get(0, count));
                count++;
            }
            
            // Hidden to output layer weights
            for(int i=0;i<hiddenNeurons;i++){
                for(int j=0;j<outputs;j++){
                    outputLayerWeights.set(i, j, parameters.get(0, count));
                    count++;
                }
            }
        }
    }
    
    /** Compress all of the parameters into a single row vector */
    public Matrix getParameterVector(){
        Matrix m = new Matrix(1, totalParameterCount);
        int count = 0;
        
        // Add hidden layer weights
        for(int i=0;i<inputs;i++){
            for(int j=0;j<hiddenNeurons;j++){
                m.set(0, count, hiddenLayerWeights.get(i, j));
                count++;
            }
        }        
        
        // Add hidden layer biasses
        for(int i=0;i<hiddenNeurons;i++){
            m.set(0, count, hiddenLayerBias.get(i, 0));
            count++;
        }
        
        // Add output layer weights
        for(int i=0;i<hiddenNeurons;i++){
            for(int j=0;j<outputs;j++){
                m.set(0, count, outputLayerWeights.get(i, j));
                count++;
            }
        }        
        return m;
    }
    
    /** Sigmoid function */
    public double sigmoid(double x){
        return 1 / (1 + Math.exp(-x));
    }
    
    /** Get a prediction matrix for a set of input data */
    public synchronized Matrix getPredictions(Matrix x){
        /*
        Matrix tempHiddenLayerWeights = hiddenLayerWeights.copy();
        Matrix tempHiddenLayerBias = hiddenLayerBias.copy();
        Matrix tempOutputLayerWeights = outputLayerWeights.copy();
        */
        
        Matrix hiddenOutputs = x.times(hiddenLayerWeights);
        int rows = hiddenOutputs.getRowDimension();
        int cols = hiddenOutputs.getColumnDimension();
        
        // Add bias terms
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                hiddenOutputs.set(i, j, hiddenOutputs.get(i, j) + hiddenLayerBias.get(j, 0));
            }
        }
        
        // Do the sigmoid transform
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                hiddenOutputs.set(i, j, sigmoid(hiddenOutputs.get(i, j)));
            }
        }
        
        // Multiply by output layer weights
        return hiddenOutputs.times(outputLayerWeights);
    }

    /** Recreate from storage */
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        hiddenNeurons = xmlDataStore.intValue("HiddenNeurons", 5);
        inputs = xmlDataStore.intValue("Inputs", 0);
        outputs = xmlDataStore.intValue("Outputs", 0);
        initialiseWeights();
        hiddenLayerWeights = ((MatrixSerializer)xmlDataStore.xmlStorableValue("HiddenLayerWeights")).getMatrix();
        hiddenLayerBias = ((MatrixSerializer)xmlDataStore.xmlStorableValue("HiddenLayerBias")).getMatrix();
        outputLayerWeights = ((MatrixSerializer)xmlDataStore.xmlStorableValue("OutputLayerWeights")).getMatrix();
    }

    /** Save to storage */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("FFANNModel");
        store.add("HiddenNeurons", hiddenNeurons);
        store.add("Inputs", inputs);
        store.add("Outputs", outputs);
        
        // Save the parameters
        store.add("HiddenLayerWeights", new MatrixSerializer(hiddenLayerWeights));
        store.add("HiddenLayerBias", new MatrixSerializer(hiddenLayerBias));
        store.add("OutputLayerWeights", new MatrixSerializer(outputLayerWeights));
        
        return store;
    }
}

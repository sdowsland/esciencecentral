/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;
import java.util.*;

/**
 * This class manages the various optimisers that are present in the system. It
 * allows new optimisers to be registered and provides a uniform way of accessing
 * different optimisation engines.
 * @author hugo
 */
public abstract class OptimiserFactory {
    /** Hashtable of optimiser info objects */
    private static Hashtable optimisers = new Hashtable();
    
    /** Default optimiser ID */
    private static String defaultOptimiser = "simplex";
    
    /** Register an optimiser */
    public static void registerOptimiser(OptimiserInfo info){
        if(!optimisers.containsKey(info.getId())){
            optimisers.put(info.getId(), info);
        }
    }
    
    /** Un-register an optimiser */
    public static void unregisterOptimiser(String id){
        if(optimisers.containsKey(id)){
            optimisers.remove(id);
        }
    }
    
    /** Un-register an optimiser */
    public static void unregisterOptimiser(OptimiserInfo info){
        unregisterOptimiser(info.getId());
    }
    
    /** Get the information for a specific optimiser */
    public static OptimiserInfo getInfo(String id){
        if(optimisers.containsKey(id)){
            return (OptimiserInfo)optimisers.get(id);
        } else {
            return null;
        }
    }
    
    /** Get the optimiser info object for a specific optimiser */
    public static OptimiserInfo getInfoForOptimiser(Optimiser optimiser){
        Enumeration e = optimisers.elements();
        OptimiserInfo info;
        while(e.hasMoreElements()){
            info = (OptimiserInfo)e.nextElement();
            if(info.getOptimiserClass().equals(optimiser.getClass())){
                return info;
            }
        }
        return null;
    }
    
    /** Get an Enumeration of all the optimiser types */
    public static Enumeration getOptimisers(){
        return optimisers.elements();
    }
    
    /** Create an optimiser */
    public static Optimiser createOptimiser(String id) throws OptimisationException {
        try {
            if(optimisers.containsKey(id)){
                return (Optimiser)((OptimiserInfo)optimisers.get(id)).getOptimiserClass().newInstance();
                
            } else {
                throw new Exception("Optimiser type does not exist");
            }   
            
        } catch (Exception e){
            throw new OptimisationException("Cannot create optimiser: " + e.getLocalizedMessage());
        }
    }
    
    /** Create an optimiser */
    public static Optimiser createOptimiser(OptimiserInfo info) throws OptimisationException {
        return createOptimiser(info.getId());
    }
    
    /** Create the default optimiser */
    public static Optimiser createDefaultOptimiser() throws OptimisationException {
        return createOptimiser(defaultOptimiser);
    }
}
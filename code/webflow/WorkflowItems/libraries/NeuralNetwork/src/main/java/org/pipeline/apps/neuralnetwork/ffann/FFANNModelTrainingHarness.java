/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.apps.neuralnetwork.ffann;
import org.pipeline.core.matrix.*;
import org.pipeline.core.optimisation.*;
import org.pipeline.apps.neuralnetwork.ffann.*;
import org.pipeline.core.xmlstorage.*;

/**
 * This class provides a harness for training a FFANN model
 * @author hugo
 */
public class FFANNModelTrainingHarness implements OptimisableObject {
    /** FFANN Model */
    private FFANNModel model = null;
    
    /** X-data */
    private Matrix x = null;

    /** Y-data */
    private Matrix y = null;
    
    /** Creates a new instance of FFANNModelTrainingHarness */
    public FFANNModelTrainingHarness(FFANNModel model) {
        this.model = model;
    }
    
    /** Get the model */
    public FFANNModel getModel(){
        return model;
    }
    
    /** Set the x-data */
    public void setX(Matrix x){
        this.x = x;
    }
    
    /** Get the x-data */
    public Matrix getX(){
        return x;
    }
    
    /** Set the y-data */
    public void setY(Matrix y){
        this.y = y;
    }
    
    /** Get the y-data */
    public Matrix getY(){
        return y;
    }

    /** Return the number of parameters */
    public int getParameterCount() {
        if(model!=null){
            return model.getTotalParameterCount();
        } else {
            return 0;
        }
    }

    /** Return an initial weight set */
    public OptimisedParameterVector getInitialParameters() {
        //return model.getParameterVector();
        Matrix m = model.getParameterVector();
        OptimisedParameterVector params = new OptimisedParameterVector(m.getColumnDimension());
        params.setFromMatrix(m);
        
        // Set the parameter bounds. These are not enforced, but are
        // specified so that optimisers like the binary GA can set their
        // bit encoding strategy correctly.
        for(int i=0;i<params.getSize();i++){
            params.getParameter(i).setMaximumValue(4);
            params.getParameter(i).setMinimumValue(-4);
            params.getParameter(i).setEnforceRange(true);
        }
        return params;
    }

    /** Get the properties */
    public XmlDataStore getProperties(){
        return new XmlDataStore();
    }
    
    /** Calculate prediction error */
    public double getCost(OptimisedParameterVector params) {
        if(x.getRowDimension()==y.getRowDimension() && model!=null){
            Matrix matrix = params.convertToMatrix();
            model.setParameterVector(matrix);
            Matrix yest = model.getPredictions(x);
            int rows = yest.getRowDimension();
            int cols = yest.getColumnDimension();
            double sum = 0;
            double colsum;
            for(int i=0;i<cols;i++){
                colsum = 0;
                for(int j=0;j<rows;j++){
                    colsum = colsum + (Math.pow(y.get(j, i) - yest.get(j, i), 2) / rows);
                }
                sum = sum + Math.sqrt(colsum);
            }
            return sum;
        } else {
            return 0;
        }
    }

    /** Can the model supply a set of initial values */
    public boolean canSupplyInitialParameters() {
        return true;
    }
    
    /** Reset to initial conditions */
    public void reset(){
        model.initialiseWeights();
    }
}

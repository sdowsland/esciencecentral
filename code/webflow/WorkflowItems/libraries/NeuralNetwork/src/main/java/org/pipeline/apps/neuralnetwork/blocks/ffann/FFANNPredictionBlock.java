/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.apps.neuralnetwork.blocks.ffann;
import org.pipeline.core.drawing.*;
import org.pipeline.core.drawing.model.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.*;
import org.pipeline.core.matrix.*;
import org.pipeline.core.util.*;
import org.pipeline.apps.neuralnetwork.ffann.*;

/**
 * This class provides a block that propagates a new set of data
 * through a FFANN model object.
 * @author hugo
 */
public class FFANNPredictionBlock extends DefaultBlockModel {
    
    /** Creates a new instance of FFANNPredictionBlock */
    public FFANNPredictionBlock() throws DrawingException {
        super();
        setLabel("NN Pred");
        
        // X-Data input
        DefaultInputPortModel xPort = new DefaultInputPortModel("x-data", PortModel.LEFT_OF_BLOCK, 50, this);
        xPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addInputPort(xPort);
        
        // Model input
        DefaultInputPortModel modelPort = new DefaultInputPortModel("neural-network-model", PortModel.TOP_OF_BLOCK, 50, this);
        modelPort.addDataType(FFANNModelDataTypeWrapper.FFANN_MODEL_DATA_TYPE);
        addInputPort(modelPort);
        
        // Y-Est output
        DefaultOutputPortModel predictionPort = new DefaultOutputPortModel("predicted-y-data", PortModel.RIGHT_OF_BLOCK,  50, this);
        predictionPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addOutputPort(predictionPort);            
    }
    
    /** Execute this block */
    public BlockExecutionReport execute() throws BlockExecutionException {
        try {
            Data xData = DrawingDataUtilities.getInputData(getInput("x-data"));
            FFANNModel model = (FFANNModel)((FFANNModelDataTypeWrapper)getInput("neural-network-model").getData()).getPayload();
            
            Matrix x = new DataToMatrixConverter(xData).toMatrix();
            
            if(x.getColumnDimension()==model.getInputs()){
                
                Matrix yest = model.getPredictions(x);                
                Data outData = new MatrixToDataConverter(yest).toData();  
                DrawingDataUtilities.setOutputData(getOutput("predicted-y-data"), outData);
                return new BlockExecutionReport(this);
                
            } else {
                return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, "X and Y data sets do not have the same number of rows");
            }
        } catch (Exception e){
            return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, e.getMessage());
        }
        
    }        
}

/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.apps.neuralnetwork.blocks.ffann;
import org.pipeline.apps.neuralnetwork.*;
import org.pipeline.apps.neuralnetwork.ffann.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.*;
import org.pipeline.core.matrix.*;
import org.pipeline.core.drawing.*;
import org.pipeline.core.drawing.model.*;
import org.pipeline.core.xmlstorage.*;
import org.pipeline.core.util.*;
import org.pipeline.core.optimisation.*;

/**
 * This class provides a feed-forward neural network.
 * @author hugo
 */
public class FFANNTrainingBlock extends DefaultBlockModel {   
    /** Hidden layer neuron count */
    private int hiddenNeurons = 5;
    
    /** Neural network object */
    private FFANNModel model = null;
    
    /** Model optimiser */
    private OptimisationController controller = new OptimisationController();
    
    /** Model training harness */
    private FFANNModelTrainingHarness harness = null;
    
    /** Creates a new instance of FFANNTrainingBlock */
    public FFANNTrainingBlock() throws DrawingException {
        super();
        setLabel("NN Train");
        
        // Create the x-data input
        DefaultInputPortModel xPort = new DefaultInputPortModel("x-data", PortModel.LEFT_OF_BLOCK, 30, this);
        xPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addInputPort(xPort);
        
        // Create the y-data input 
        DefaultInputPortModel yPort = new DefaultInputPortModel("y-data", PortModel.LEFT_OF_BLOCK, 70, this);
        yPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addInputPort(yPort);
        
        // Add a model output port
        DefaultOutputPortModel modelPort = new DefaultOutputPortModel("neural-network-model", PortModel.BOTTOM_OF_BLOCK, 50, this);
        modelPort.addDataType(FFANNModelDataTypeWrapper.FFANN_MODEL_DATA_TYPE);
        addOutputPort(modelPort);
        
        // Create the estimated data output
        DefaultOutputPortModel predictionPort = new DefaultOutputPortModel("predicted-y-data", PortModel.RIGHT_OF_BLOCK,  50, this);
        predictionPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addOutputPort(predictionPort);
    }
    
    /** Get the optimisation controller */
    public OptimisationController getController(){
        return controller;
    }
    
    /** Get the training harness */
    public FFANNModelTrainingHarness getHarness(){
        return harness;
    }
    
    /** Get the network model */
    public FFANNModel getModel(){
        return model;
    }
    
    /** Get the number of hidden layer neurons */
    public int getHiddenNeurons(){
        return hiddenNeurons;
    }
    
    /** Set the number of hidden layer neurons */
    public void setHiddenNeurons(int hiddenNeurons){
        if(hiddenNeurons>0){
            this.hiddenNeurons = hiddenNeurons;
            if(model!=null){
                if(this.hiddenNeurons!=model.getHiddenNeurons()){
                    // Reset model 
                    model.setHiddenNeurons(hiddenNeurons);
                    model.initialiseWeights();
                }
            }
        }
    }
        
    /** Execute this block */
    public BlockExecutionReport execute() throws BlockExecutionException {
        try {           
            Data xData = DrawingDataUtilities.getInputData(getInput("x-data"));
            Data yData = DrawingDataUtilities.getInputData(getInput("y-data"));
            
            Matrix x = new DataToMatrixConverter(xData).toMatrix();
            Matrix y = new DataToMatrixConverter(yData).toMatrix();
            
            if(x.getRowDimension()==y.getRowDimension()){
                if(model==null){
                    // Create new model
                    model = new FFANNModel();
                    model.setInputs(x.getColumnDimension());
                    model.setOutputs(y.getColumnDimension());
                    model.setHiddenNeurons(hiddenNeurons);
                    model.initialiseWeights();
                    harness = new FFANNModelTrainingHarness(model);
                    controller.setTarget(harness);
                    
                } else {
                    // Check sizes
                    if(x.getColumnDimension()!=model.getInputs() || y.getColumnDimension()!=model.getOutputs() || hiddenNeurons!=model.getHiddenNeurons()){
                        // Reset model
                        model.setInputs(x.getColumnDimension());
                        model.setOutputs(y.getColumnDimension());
                        model.setHiddenNeurons(hiddenNeurons);
                        model.initialiseWeights();                    
                    }                
                }
                
                Matrix yest = model.getPredictions(x);                
                Data outData = new MatrixToDataConverter(yest).toData(); 
 
                // Copy column names to output data
                if(yData.getColumns()==outData.getColumns()){
                    for(int i=0;i<yData.getColumns();i++){
                        outData.column(i).setName(yData.column(i).getName() + "_est");
                    }
                }
                
                DrawingDataUtilities.setOutputData(getOutput("predicted-y-data"), outData);
                getOutput("neural-network-model").setData(new FFANNModelDataTypeWrapper(model));
                return new BlockExecutionReport(this);
                
            } else {
                return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, "X and Y data sets do not have the same number of rows");
            }
        } catch (Exception e){
            return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, e.getMessage());
        }
        
    }    
    
    /** Recreate from storage */
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        super.recreateObject(xmlDataStore);
        hiddenNeurons = xmlDataStore.intValue("HiddenNeurons", 5);
        if(xmlDataStore.containsName("Network")){
            model = (FFANNModel)xmlDataStore.xmlStorableValue("Network");
        } else {
            model = null;
        }
        
        if(xmlDataStore.containsName("OptimisationController")){
            controller = (OptimisationController)xmlDataStore.xmlStorableValue("OptimisationController");
        }
        
        if(model!=null && controller!=null){
            harness = new FFANNModelTrainingHarness(model);
            controller.setTarget(harness);
        }
    }

    /** Save to storage */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("HiddenNeurons", hiddenNeurons);
        if(model!=null){
            store.add("Network", model);
        }
        store.add("OptimisationController", controller);
        return store;
    }    
}

/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.apps.neuralnetwork.blocks.ffann;
import org.pipeline.core.drawing.*;
import org.pipeline.apps.neuralnetwork.ffann.*;
/**
 * This class defines a data type for a FFANNModel object that can
 * be passed between blocks.
 * @author hugo
 */
public class FFANNModelDataType extends DataType {
   
    /** Creates a new instance of FFANNModelDataType */
    public FFANNModelDataType() {
        super("FFANN Model", "Feed-forward Neural Network object", FFANNModel.class);
    }
}

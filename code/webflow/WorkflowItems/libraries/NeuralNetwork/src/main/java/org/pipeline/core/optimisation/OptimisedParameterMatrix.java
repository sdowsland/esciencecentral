/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;
import org.pipeline.core.matrix.*;

/**
 * This class contains a matrix of optimised parameters.
 * @author nhgh
 */
public class OptimisedParameterMatrix {
    /** Array of parameters */
    OptimisedParameter[][] parameters;
    
    /** Number of rows */
    private int rows;
    
    /** Number of columns */
    private int cols;
    
    /** Creates a new instance of OptimisedParameterMatrix */
    public OptimisedParameterMatrix(int rows, int cols) {
        parameters = new OptimisedParameter[rows][cols];
        this.rows = rows;
        this.cols = cols;
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                parameters[i][j] = new OptimisedParameter(0);
            }
        }
    }
    
    /** Create by copying an existing parameter vector */
    public OptimisedParameterMatrix(OptimisedParameterMatrix original){
        rows = original.getRowDimension();
        cols = original.getColumnDimension();
        
        parameters = new OptimisedParameter[rows][cols];
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                parameters[i][j] = new OptimisedParameter(original.getParameter(i, j));
            }
        }
    }
    
    /** Get the number of rows */
    public int getRowDimension(){
        return rows;
    }
    
    /** Get the number of columns */
    public int getColumnDimension(){
        return cols;
    }
    
    /** Set a parameter */
    public void setParameter(int row, int col, OptimisedParameter value) {
        parameters[row][col] = value;
    }
    
    /** Get a parameter */
    public OptimisedParameter getParameter(int row, int col) {
        return parameters[row][col];
    }
    
    /** Get a value */
    public double get(int row, int col){
        return parameters[row][col].getCurrentValue();
    }
    
    /** Set a value */
    public void set(int row, int col, double value){
        parameters[row][col].setCurrentValue(value);
    }    
    
    /** Extract a row of this matrix as a OptimisedParameterVector */
    public OptimisedParameterVector extractRow(int row){
        OptimisedParameterVector params = new OptimisedParameterVector(cols);
        for(int i=0;i<cols;i++){
            params.setParameter(i, new OptimisedParameter(getParameter(row, i)));
        }
        return params;
    }
    
    /** Set a row in this matrix using an OptimisedParameterVector */
    public void setRow(int row, OptimisedParameterVector params){
        for(int i=0;i<cols;i++){
            setParameter(row, i, new OptimisedParameter(params.getParameter(i)));
        }
    }
    
    /** Convert to a Matrix */
    public Matrix convertToMatrix(){
        Matrix m = new Matrix(rows, cols);
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                m.set(i, j, get(i, j));
            }
        }
        return m;
    }
    
    /** Set from a Matrix */
    public void setFromMatrix(Matrix m){
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                set(i, j, m.get(i, j));
            }
        }
    }
}
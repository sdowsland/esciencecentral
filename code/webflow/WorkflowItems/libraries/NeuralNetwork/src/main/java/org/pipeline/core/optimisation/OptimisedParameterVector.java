/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;
import org.pipeline.core.matrix.*;

/**
 * This class contains a vector of optimised parameters
 * @author nhgh
 */
public class OptimisedParameterVector {
    /** Array of parameters */
    OptimisedParameter[] parameters;
    
    /** Creates a new instance of OptimisedParameterVector */
    public OptimisedParameterVector(int size) {
        parameters = new OptimisedParameter[size];
        for(int i=0;i<size;i++){
            parameters[i] = new OptimisedParameter(0);
        }
    }
    
    /** Create by copying an existing parameter vector */
    public OptimisedParameterVector(OptimisedParameterVector original){
        int size = original.getSize();
        parameters = new OptimisedParameter[size];
        for(int i=0;i<size;i++){
            parameters[i] = new OptimisedParameter(original.getParameter(i));
        }
    }
    
    /** Get the size of this vector */
    public int getSize(){
        return parameters.length;
    }
    
    /** Set a parameter */
    public void setParameter(int index, OptimisedParameter value) {
        parameters[index] = value;
    }
    
    /** Get a parameter */
    public OptimisedParameter getParameter(int index) {
        return parameters[index];
    }
    
    /** Get a value */
    public double get(int index){
        return parameters[index].getCurrentValue();
    }
    
    /** Set a value */
    public void set(int index, double value){
        parameters[index].setCurrentValue(value);
    }
    
    /** Convert this vector to a Matrix object */
    public Matrix convertToMatrix(){
        Matrix m = new Matrix(1, parameters.length);
        for(int i=0;i<parameters.length;i++){
            m.set(0, i, get(i));
        }
        return m;
    }
    
    /** Set parameter values from a Matrix */
    public void setFromMatrix(Matrix m){
        for(int i=0;i<parameters.length;i++){
            set(i, m.get(0, i));
        }
    }
    
    /** Print to a string */
    public String toString(){
        StringBuffer buffer = new StringBuffer();
        for(int i=0;i<parameters.length;i++){
            buffer.append(parameters[i].getCurrentValue());
            if(i<parameters.length-2){
                buffer.append(",");
            }
        }
        return buffer.toString();
    }
}
/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;
import org.pipeline.core.xmlstorage.*;

/**
 * This is the base class for an optimiser. It provides basic properties
 * and a notification infrastructure to support display of optimiser
 * progress over time.
 * @author hugo
 */
public abstract class Optimiser implements Runnable {
    /** Parent optimisation controller */
    private OptimisationController parent = null;
    
    // Status flags
    
    /** Optimiser is running */
    public static final int RUNNING = 1;
    
    /** Optimiser is not running */
    public static final int STOPPED = 0;
    
    /** Current status */
    private int status = STOPPED;
    
    /** Optimisation target */
    private OptimisableObject target = null;
    
    /** Current number of iterations */
    private int iterations = 0;
    
    /** Stop flag */
    private boolean stopFlag = false;
    
    /** Current best error */
    private double bestError = 0;
    
    /** Current best parameters */
    private OptimisedParameterVector bestParameters = null;
    
    /** Current properties */
    private XmlDataStore currentProperties = null;
    
    /** Standard shared optimiser properties */
    private XmlDataStore standardProperties = null;
    
    /** Creates a new instance of Optimiser */
    public Optimiser() {
        standardProperties = new XmlDataStore();
        standardProperties.add("MaxIterations", Integer.MAX_VALUE, "Maximum number of iterations");
    }

    /** Notify an optimiser reset */
    public void notifyReset(){
        if(parent!=null){
            parent.notifyReset();
        }
    }
    
    /** Notify that an iteration has finished */
    public void notifyIteration(){
        if(parent!=null){
            parent.notifyIteration(iterations);
        }
    }
    
    /** Notify the parent controller that the optimiser has started */
    public void notifyStarted(){
        if(parent!=null){
            parent.notifyStarted();
        }
    }
    
    /** Notify the parent that the optimiser has an improved error */
    public void notifyImprovedError(){
        if(parent!=null){
            parent.notifyImprovedError(iterations, bestError);
        }
    }
    
    /** Notify the parent that the optimiser has stopped */
    public void notifyStopped(){
        if(parent!=null){
            parent.notifyStopped();
        }
    }
    
    /** Set the status */
    public synchronized void setStatus(int status){
        this.status = status;
    }
    
    /** Get the status */
    public synchronized int getStatus(){
        return status;
    }
    
    /** Set the target */
    public void setTarget(OptimisableObject target){
        this.target = target;
    }
    
    /** Get the target */
    public OptimisableObject getTarget(){
        return target;
    }
    
    /** Set the parent */
    public void setParent(OptimisationController parent){
        this.parent = parent;
    }
    
    /** Get the parent */
    public OptimisationController getParent(){
        return parent;
    }
    
    /** Get current number of iterations */
    public synchronized int getIterations() {
        return iterations;
    }

    /** Set current number of iterations */
    public synchronized void setIterations(int iterations) {
        this.iterations = iterations;
    }

    /** Increment the iteration counter */
    public synchronized void incrementIterations(){
        iterations++;
        notifyIteration();
    }
    
    /** Reset iteration counter */
    public synchronized void resetIterations(){
        iterations = 0;
    }
    
    /** Get the status of the stop flag */
    public synchronized boolean getStopFlag() {
        return stopFlag;
    }

    /** Set the status of the stop flag */
    public synchronized void setStopFlag(boolean stopFlag) {
        this.stopFlag = stopFlag;
    }
    
    /** Get the best error */
    public synchronized double getBestError(){
        return bestError;
    }
    
    /** Set the best error */
    public synchronized void setBestError(double bestError){
        this.bestError = bestError;
    }
    
    /** Get the best parameters */
    public synchronized OptimisedParameterVector getBestParameters(){
        return bestParameters;
    }
    
    /** Set the best parameters */
    public synchronized void setBestParameters(OptimisedParameterVector bestParameters){
        // Copy parameters
        this.bestParameters = new OptimisedParameterVector(bestParameters);
    }
    
    /** Does this optimiser contain a valid set of best parameters */
    public boolean containsBestParameters(){
        if(bestParameters!=null){
            if(target!=null){
                if(bestParameters.getSize()==target.getParameterCount()){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /** Reset the optimiser and initialise the best parameters matrix with
     * either the initial values from the optimisable object, or a random
     * set of parameters if the target object cannot supply an initial
     * parameter guess */
    public void initialiseBestParameters(){
        if(target!=null){
            
            if(target.canSupplyInitialParameters()){
                bestParameters = target.getInitialParameters();
            } else {
                int parameterCount = target.getParameterCount();
                bestParameters = new OptimisedParameterVector(parameterCount);
                for(int i=0;i<parameterCount;i++){
                    bestParameters.set(i, Math.random() - 0.5);
                }
            }
            bestError = target.getCost(bestParameters);
        }
    }
    
    /** Run this optimiser */
    public void run(){
        if(target!=null){
            // Set everything up
            setStopFlag(false);
            setStatus(Optimiser.RUNNING);
            notifyStarted();
            initialiseOptimiser();
            
            // Run the optimisation process
            performOptimisation();
            setStatus(Optimiser.STOPPED);
            notifyStopped();
        }
    }
    
    /** Get the algorithm properties */
    public XmlDataStore getProperties(){
        if(currentProperties==null){
            currentProperties = getDefaultProperties();
            try {
                currentProperties.copyProperties(standardProperties);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return currentProperties;
    }
    
    /** Set up the optimiser before running. This method must be overridden by
     * the optimiser to provide specific setup behaviour */
    public abstract void initialiseOptimiser();
   
    /** Main optimisation method */
    public abstract void performOptimisation();
    
    /** Get the default properties */
    public abstract XmlDataStore getDefaultProperties();
}
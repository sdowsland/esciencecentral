/* =================================================================
 *                     conneXience Data Pipeline
 * =================================================================
 *
 * Copyright 2006 Hugo Hiden and Adrian Conlin
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.pipeline.core.optimisation;

/**
 * This class holds information about an optimiser including the class to 
 * create and the editor panel that is used to configure it.
 * @author hugo
 */
public class OptimiserInfo {
    /** Optimiser id */
    private String id;
    
    /** Optimiser name */
    private String name;
    
    /** Optimiser description */
    private String description;
    
    /** Optimiser class to create */
    private Class optimiserClass;
    
    /** Creates a new instance of OptimiserInfo */
    public OptimiserInfo(String id, String name, String descriptionm, Class optimiserClass) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.optimiserClass = optimiserClass;
    }

    /** Get the id string that is used to create an instance of this optimiser */
    public String getId() {
        return id;
    }

    /** Get the display name of the optimiser */
    public String getName() {
        return name;
    }

    /** Get the description of the optimiser */
    public String getDescription() {
        return description;
    }

    /** Get the class of the optimiser that will get created */
    public Class getOptimiserClass() {
        return optimiserClass;
    }
    
    /** Override toString method */
    public String toString(){
        return name;
    }

}

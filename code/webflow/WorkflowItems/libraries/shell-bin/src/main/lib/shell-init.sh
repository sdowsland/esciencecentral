sanitizeName() {
    echo $( printf "$1" | tr -c [:alnum:] _ )
}

getProperty() {
    if [ -z "$1" ] ; then
        echo "Missing input argument: expected property name"
        return 1
    fi

    varname=PROP__$(sanitizeName "$1")
    echo "${!varname}"
}

getDependency() {
    if [ -z "$1" ] ; then
        echo "Missing input argument: expected dependency library name"
        return 1
    fi

    if [ -z "$2" ] ; then
        echo "Missing input argument: expected command name"
        return 2
    fi

    varname=DEP__$(sanitizeName "$1")__$(sanitizeName "$2")
    echo "${!varname}"
}

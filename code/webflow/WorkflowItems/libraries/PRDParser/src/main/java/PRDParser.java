import com.connexience.server.ConnexienceException;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.StringColumn;

import java.util.List;

/**
 * User: nsjw7
 * Date: 08/07/2014
 * Time: 14:13
 */
public class PRDParser {

    /**
     * Parse the top level element data into a summary dataset
     *
     * @param serializableProfileData The root element of the PRD file, i.e. the parent
     *                                element of the summary data
     * @param data dataset to add the summary data to
     * @param profileVersionNumber Version number of the profile.prd file
     * @throws Exception Something went wrong
     */
    public static void populateTopLevelData(Element serializableProfileData, Data data, String profileVersionNumber) throws Exception {
        getOrCreateColumnFromChildText(serializableProfileData, data, "ProfileID");
        getOrCreateColumnFromChildText(serializableProfileData, data, "ProfileName");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalScoreEarned");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalBalloonModels");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalBalloonsReleased");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalTeeterTricks");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalPiesThrown");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalClownsHit");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalPerfectHighDives");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalTrapezeTricks");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalJugglingTricks");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalGlobeTricks");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalGlobeFalls");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalGlobeDistance");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalTigerTricks");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalTigerFeeds");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalTigersBored");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalPlateSpinTime");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalPlatesSmashed");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalKnivesThrown");
        getOrCreateColumnFromChildText(serializableProfileData, data, "TotalBalloonsBurst");
        getOrCreateColumnFromChildText(serializableProfileData, data, "LeftHanded");
        getOrCreateColumnAndAppendValue(data, "ProfileVersion", profileVersionNumber);

    }

    /**
     * Parse the data from the Difficulties section of the profile into a dataset.
     *
     * Create a 300x9 dataset
     *
     * @param serializableProfileData The root element of the PRD file, i.e. the parent
     *                                element of the Difficulties element
     * @param data Dataset to add the difficulties data to
     * @param profileVersionNumber Version number of the profile.prd file
     * @throws Exception Something went wrong
     */
    public static  void populateDifficultiesData(Element serializableProfileData, Data data, String profileVersionNumber) throws Exception {
        Element difficulty = serializableProfileData.getChild("Difficulties");
        List<Element> difficultyContainers = difficulty.getChildren("DifficultyContainer");
        for (Element difficultyContainer : difficultyContainers) {

            //Info from the DifficultyContainer element (easy, med, hard)
            String difficultyUnlocked = difficultyContainer.getAttributeValue("Unlocked");
            String difficultyLevel = difficultyContainer.getAttributeValue("Difficulty");

            List<Element> gameTypes = difficultyContainer.getChildren("GameTypes");
            for (Element gameType : gameTypes) {

                //Info from the GameTypes element (each game)
                String gameName = gameType.getAttributeValue("GameType");
                String gamePlayTime = gameType.getAttributeValue("PlayTime");
                String gameUnlocked = gameType.getAttributeValue("Unlocked");

                List<Element> levels = gameType.getChildren("Levels");
                for (Element level : levels) {

                    //Add the 'static' data for each game level
                    getOrCreateColumnAndAppendValue(data, "ProfileVersion", profileVersionNumber);
                    getOrCreateColumnAndAppendValue(data, "DifficultyUnlocked", difficultyUnlocked);
                    getOrCreateColumnAndAppendValue(data, "DifficultyLevel", difficultyLevel);
                    getOrCreateColumnAndAppendValue(data, "GameName", gameName);
                    getOrCreateColumnAndAppendValue(data, "GamePlayTime", gamePlayTime);
                    getOrCreateColumnAndAppendValue(data, "GameUnlocked", gameUnlocked);

                    //Add each of the game levels which are attributes
                    for (Attribute attribute : level.getAttributes()) {
                        getOrCreateColumnAndAppendValue(data, attribute.getName(), attribute.getValue());
                    }
                }
            }
        }
    }

    /**
     * Parse the data from the achievements section of the profile into a dataset
     *
     * Will create a 1x153 dataset
     *
     * @param serializableProfileData The root element of the PRD file, i.e. the parent
     *                                element of the Achievements element
     * @param data Dataset to add the achievements data to
     * @param profileVersionNumber Version number of the profile.prd file
     * @throws Exception Something went wrong
     */
    public static  void populateAchievementsData(Element serializableProfileData, Data data, String profileVersionNumber) throws Exception {
        Element achievement = serializableProfileData.getChild("Achievements");
        getOrCreateColumnAndAppendValue(data, "ProfileVersion", profileVersionNumber);
        List<Element> achievementContainers = achievement.getChildren("AchievementContainer");
        for (Element achievementContainer : achievementContainers) {
            getOrCreateColumnAttributes(achievementContainer, data, "Name", "Unlocked");
        }
    }

    /**
     * Parse the data from the gestures section of the Profile file - the moves that have been
     * completed
     *
     * Will create a 93x10 dataset
     *
     * @param serializableProfileData The root element of the PRD file, i.e. the parent
     *                                element of the TaggedInfo element
     * @param data  Dataset to add the gestures data to
     * @param profileVersionNumber Version number of the profile.prd file
     * @throws Exception Something went wrong
     */
    public static  void populateGesturesData(Element serializableProfileData, Data data, String profileVersionNumber) throws Exception {
        Element taggedInfo = serializableProfileData.getChild("TaggedInfo");
        List<Element> gestureTaggedInfoContainers = taggedInfo.getChildren("GestureTaggedInfoContainer");
        for (Element gestureTaggedInfoContainer : gestureTaggedInfoContainers) {
            //Add the profile version to each row
            getOrCreateColumnAndAppendValue(data, "ProfileVersion", profileVersionNumber);

            //The row will be Name, GlobalTime, SessionTime, ...
            String gestureName = gestureTaggedInfoContainer.getChildText("GestureName");

            //The other values are attributes of the movement element
            Element movement = gestureTaggedInfoContainer.getChild("Movement");
            getOrCreateColumnAndAppendValue(data, "Name", gestureName);
            for (Attribute attribute : movement.getAttributes()) {
                getOrCreateColumnAndAppendValue(data, attribute.getName(), attribute.getValue());
            }
        }
    }

    /**
     * Get the text from an XML child element and create a column with the specified
     * name and append the child text as a cell in the column. Will create the column if it
     * doesn't exist or re-use an existing one with the correct name
     *
     * e.g. <ProfileName>FRI2102</ProfileName>
     *
     * @param element XML element which contains the child text
     * @param data dataset to append the column and value to
     * @param columnName Name of the column to create
     * @return Column which has been created or found with the child text value appended
     * @throws org.pipeline.core.data.DataException Something went wrong
     * @throws com.connexience.server.ConnexienceException Somethind went wrong
     */
    private static Column getOrCreateColumnFromChildText(Element element, Data data, String columnName) throws DataException, ConnexienceException {
        String childText = element.getChildText(columnName);
        return getOrCreateColumnAndAppendValue(data, columnName, childText);
    }

    /**
     * Append a value to a column after creating it if it doesn't already exits.  The name of the
     * column will be the value of the named attribute and the value appended will be the value
     * of the other named attribute.
     *
     * e.g.  <AchievementContainer Unlocked="false" Name="GAMECOMPLETE_EASY_PLATESPIN"/>
     *
     * Will produce | GAMECOMPLETE_EASY_PLATESPIN |
     *              | false                       |
     *
     * @param element element containing the attributes
     * @param data dataset to append the column and value to
     * @param nameAttribute name of the attribute to take the column name from
     * @param valueAttribute name of the attribute to take the cell value from
     * @return Column which has been created or found with the new value appended
     * @throws DataException Something went wrong
     * @throws ConnexienceException Somethind went wrong
     */
    private static Column getOrCreateColumnAttributes(Element element, Data data, String nameAttribute, String valueAttribute) throws DataException, ConnexienceException {
        String columnName = element.getAttributeValue(nameAttribute);
        String value = element.getAttributeValue(valueAttribute);
        return getOrCreateColumnAndAppendValue(data, columnName, value);
    }

    /**
     * Append the value to a column with the specified name.  Will create the column if it doesn't
     * exist
     *
     * @param data dataset to append the column and value to
     * @param columnName name of the column
     * @param value value to append
     * @return Column which has been created or found with the value appended
     * @throws DataException Something went wrong
     * @throws ConnexienceException Something went wrong
     */
    private static Column getOrCreateColumnAndAppendValue(Data data, String columnName, String value) throws DataException, ConnexienceException {
        if (value != null && !value.equals("")) {
            Column column;
            //If the dataset doesn't contain a column for this text then create one
            if (!data.containsColumn(columnName)) {
                column = new StringColumn(columnName);
                data.addColumn(column);
            } else {
                column = data.column(columnName);
            }
            //Add the value from the XML file to the column
            column.appendObjectValue(value);
            return column;
        } else {
            throw new ConnexienceException("Cannot find Game data for " + columnName);
        }
    }
}

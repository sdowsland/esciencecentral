/*
 * Test.java
 */
package com.connexience.apps.ec50;

import org.pipeline.core.data.*;
import org.pipeline.core.data.io.*;
import java.io.*;
/**
 * Tests EC50Fitter
 * @author hugo
 */
public class Test {
    public Test(){
        try {
            DelimitedTextDataImporter importer = new DelimitedTextDataImporter();
            importer.setImportColumnNames(false);
            importer.setDataStartRow(1);
            Data d = importer.importInputStream(getClass().getResourceAsStream("/testdata.csv"));
            EC50Fitter fitter = new EC50Fitter();
            fitter.setRawData(d);
            fitter.initialise();
            fitter.execute();
            
            Data observed = fitter.getObservedDataPlotMatrix();
            Data fitted = fitter.getFittedDataPlotMatrix(100);
            System.out.println();
            
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args){
        new Test();
    }
}
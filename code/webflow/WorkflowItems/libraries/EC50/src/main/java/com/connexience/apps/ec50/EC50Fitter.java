/*
 * EC50Fitter.java
 */
package com.connexience.apps.ec50;

import com.connexience.apps.ec50.EC50Curve.FitType;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.manipulation.*;
import org.pipeline.core.data.maths.*;
import org.pipeline.core.optimisation.*;
import org.pipeline.core.optimisation.simplex.*;
import org.pipeline.core.xmlstorage.*;
/**
 * This class takes an input Data object and does an EC50 fit on it. The data
 * needs to have concentrations in the first column with each remaining column
 * containing the observed values for each experiment.
 * @author hugo
 */
public class EC50Fitter implements OptimiserListener {
    /** EC50 Curve that will be used to fit to */
    private EC50Curve curve;
    
    /** Fitting mode */
    private EC50Curve.FitType fitType = EC50Curve.FitType.TOPBOTTOMEC50;
    
    /** Raw data for fitting */
    private Data rawData;
    
    /** Concentration values */
    private DoubleColumn concentrationColumn;
    
    /** Fitted experimental data */
    private DoubleColumn observedColumn;
    
    /** Maximum observed values */
    private DoubleColumn maximumObservedColumn;
    
    /** Minimum observed values */
    private DoubleColumn minimumObservedColumn;
    
    /** Data containing the matrix of experimental values */
    private Data experimentalMatrix;
    
    /** Column picker to select the concentration data */
    private ColumnPicker concentrationPicker = new ColumnPicker();
    
    /** Should the concentration be logged */
    private boolean logConcentration = true;
    
    /** Controller that will manage the optimisation process */
    private OptimisationController controller;
    
    /** Simplex optimiser used to fit data */
    SimplexOptimiser optimiser;    
    
    /** Number of resets */
    private int resetLimit = 10;
    
    /** Current reset count */
    private int resetCount = 0;
    
    /** Set the raw data for fitting */
    public void setRawData(Data rawData){
        this.rawData = rawData;
    }

    public void setFitType(FitType fitType) {
        this.fitType = fitType;
    }

    public FitType getFitType() {
        return fitType;
    }
    
    /** Get the column picker for the concentration */
    public ColumnPicker getConcentrationPicker(){
        return concentrationPicker;
    }
    
    /** Convert the raw data into fitting data */
    private void setupFittingData() throws DataException {
        ColumnPickerCollection pickers = new ColumnPickerCollection();
        pickers.addColumnPicker(concentrationPicker);
        
        concentrationColumn = (DoubleColumn)concentrationPicker.pickColumn(rawData);
        experimentalMatrix = pickers.extractNonSelectedData(rawData);
        
        // Need to build a single column containing the mean of the experimental matrix
        observedColumn = new DoubleColumn("fittingdata");
        maximumObservedColumn = new DoubleColumn("max");
        minimumObservedColumn = new DoubleColumn("min");
        for(int i=0;i<experimentalMatrix.getLargestRows();i++){
            observedColumn.appendDoubleValue(rowMean(experimentalMatrix, i));
            maximumObservedColumn.appendDoubleValue(rowMax(experimentalMatrix, i));
            minimumObservedColumn.appendDoubleValue(rowMin(experimentalMatrix, i));
        }
    }
    
    /** Calculate the maximum of a row of data */
    private double rowMax(Data d, int row) throws DataException {
        double max = -Double.MAX_VALUE;
        int count = 0;
        for(int i=0;i<d.getColumns();i++){
            if(!d.column(i).isMissing(row)){
                if(((NumericalColumn)d.column(i)).getDoubleValue(row) > max){
                    max = ((NumericalColumn)d.column(i)).getDoubleValue(row);
                }
                count++;
            }
        }
        
        if(count>0){
            return max;
        } else {
            throw new DataException("No valid values in data row");
        }          
    }
    
    /** Calculate the minimum of a row of data */
    private double rowMin(Data d, int row) throws DataException {
        double min = Double.MAX_VALUE;
        int count = 0;
        for(int i=0;i<d.getColumns();i++){
            if(!d.column(i).isMissing(row)){
                if(((NumericalColumn)d.column(i)).getDoubleValue(row) < min){
                    min = ((NumericalColumn)d.column(i)).getDoubleValue(row);
                }
                count++;
            }
        }
        
        if(count>0){
            return min;
        } else {
            throw new DataException("No valid values in data row");
        }        
    }
    
    /** Calculate the mean of a row of data */
    private double rowMean(Data d, int row) throws DataException {
        double sum = 0;
        int count = 0;
        for(int i=0;i<d.getColumns();i++){
            if(!d.column(i).isMissing(row)){
                sum = sum + ((NumericalColumn)d.column(i)).getDoubleValue(row);
                count++;
            }
        }
        
        if(count>0){
            return sum / (double)count;
        } else {
            throw new DataException("No valid values in data row");
        }
    }
    
    // Optimiser listener implementation
    @Override
    public void optimiserImprovedError(int iteration, double error) {
        System.out.println("Improved error: " + error);
    }

    @Override
    public void optimiserIterationCompleted(int iteration) {
        
    }

    @Override
    public void optimiserReset() {
        
    }

    @Override
    public void optimiserStarted() {
        
    }

    @Override
    public void optimiserStopped() {
        
    }
    
    /** Initialise the fitting process */
    public void initialise() throws Exception {
        // Sort out the data that will be used for the fitting
        setupFittingData();
        
        // Setup the optimiser
        controller = new OptimisationController();
        optimiser = new SimplexOptimiser();
        curve = new EC50Curve();
        curve.setFitMode(fitType);
        
        controller.setTarget(curve);
        controller.setOptimiser(optimiser);
        controller.addOptimiserListener(this);        
        
        // Put the data into the EC50Curve
        Data fitData = new Data();
        fitData.addColumn(concentrationColumn.getCopy());
        fitData.addColumn(observedColumn.getCopy());
        curve.setObservedData(fitData);
        
    }
    
    /** Perform the optimisation */
    public void execute(){
        controller.resetOptimiser();
        controller.startOptimiser();
        while(optimiser.getStatus()==Optimiser.RUNNING){
            try {
                Thread.sleep(10);
            } catch (Exception e){

            }

        }
    }
    
    /** Get the data matrix to use when generating the plot of actual values */
    public Data getObservedDataPlotMatrix() throws DataException {
        Data results = new Data();
        
        DoubleColumn conc = (DoubleColumn)concentrationColumn.getCopy();
        conc.setName("Concentration");
        
        results.addColumn(conc);
        results.addColumn(observedColumn.getCopy());
        results.addColumn(minimumObservedColumn.getCopy());
        results.addColumn(maximumObservedColumn.getCopy());
        return results;
    }
    
    /** Get the fitted data matrix that has more interpolated points in it */
    public Data getFittedDataPlotMatrix(int points) throws DataException {
        Data results = new Data();
        
        DoubleColumn x = new DoubleColumn("Concentration");
        DoubleColumn y = new DoubleColumn("Fitted");
        
        double minConcentration = new MinValueCalculator(concentrationColumn).doubleValue();
        double maxConcentration = new MaxValueCalculator(concentrationColumn).doubleValue();
        double delta = (maxConcentration - minConcentration) / (double)points;

        double xVal;
        double yVal;
        for(int i=0;i<points;i++){
            xVal = minConcentration + ((double)i * delta);
            yVal = curve.getPrediction(optimiser.getBestParameters(), xVal);
            x.appendDoubleValue(xVal);
            y.appendDoubleValue(yVal);
        }
        
        results.addColumn(x);
        results.addColumn(y);
        return results;
    }
    
    public XmlDataStore getParameters(){
        return curve.getParametersAsProperties(optimiser.getBestParameters());
    }
}
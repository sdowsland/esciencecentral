
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.matrix.Matrix;
import org.pipeline.core.util.DataToMatrixConverter;

/** Block to generate data for the 'Unwrapped Ball' problem with a user specified 
 * dimension and number of data points. 
 * 
 * This can be used for testing function approximation / non-linear 
 * regression algorithms E.g. Weka blocks.
 * 
 * Note: The 5D version was proposed as a 'standardised' symbolic regression
 * benchmark (Vladislavleva-4) in White et al., 'Better GP benchmarks: 
 * community survey results and proposals', Genet. Program Evolvable Mach. 
 * (2013) (Table 4).
 * 
 * @author Dominic Searson
 */
public class UballDataGenerator extends CloudDataProcessorService {

    
    @Override
    public void executionAboutToStart() throws Exception {
    }

   
    @Override
    public void execute() throws Exception {
       
        
        /* Get user settable block parameters */
        // number of data points (rows) (2nd value is default but doesnt seem to work!, user service.xml instead)
        int numData = getProperties().intValue("NumData", 500); 
        int dimension = getProperties().intValue("Dimension", 5);
        double minVal = getProperties().doubleValue("MinValue", 0.05);
        double maxVal = getProperties().doubleValue("MaxValue", 6.0);
        
        /* error checks */
        if (numData < 1) {
            throw new IllegalArgumentException("The 'NumData' block parameter must be 1 or greater");
        }
        
        if (dimension < 1) {
            throw new IllegalArgumentException("The 'Dimension' block parameter must be set as 1 or greater");
        }
        
        if (minVal >= maxVal) {
            throw new IllegalArgumentException("The 'MinValue' block parameter must be smaller than the 'MaxValue' block parameter");
        }
        
    
        /* create workflow data object to store x1, ..., xn , y */
        Data uballData = new Data();
        
        /* generate x data, column by column */
        DoubleColumn currXCol;
        double xrange = maxVal - minVal;
             
        /* iterate over required number of input columns */
        for (int i = 0; i < dimension; i++) {
            currXCol = new DoubleColumn(numData);
            currXCol.setName("x" +(i+1));
            
            /*iterate over current column and add random x vals by row */
            for (int j = 0; j < numData; j++) {
                currXCol.appendDoubleValue( minVal + ( Math.random() * xrange ) );     
            }
            
            /* add each x column to Data */
            uballData.addColumn(currXCol);
            
        }
       
        /* Compute the y column for the random x vals and add to Data object */
        calcUballVal(uballData);
        
        uballData.setName("Uball-" +dimension +"dimensional");

        // set UballData as output data for block
        this.setOutputDataSet("output-1", uballData);
    }

    /** Method to calculate the Uball y value for a given 
     *  set of x values
     * 
     * @param uballXData  A Data object containing x values as columns
     * 
     */
    private static void calcUballVal(Data uballXData) throws DataException {
        
        /* check for existing y column and remove if there's already one there */
       int yind = uballXData.getColumnIndex("y");
        
        if (yind != -1) {
            uballXData.removeColumn(yind);
        }
        
        /* Use connexience matrix data type to get rows and col. count */
        DataToMatrixConverter c = new DataToMatrixConverter(uballXData);
        Matrix x = c.toMatrix(); //NB Matrix uses zero based indexing
        int numRows = x.getRowDimension();
        int numXCols = x.getColumnDimension();
        
        /* prepare output column */
        DoubleColumn yCol = new DoubleColumn(numRows);
        yCol.setName("y");
        
        /* loop through rows of x data and calc y for each row */
        double ycurr, sum;
        for (int i = 0; i < numRows; i++) {
            sum = 0;
            for (int j = 0; j < numXCols; j++ ) {
                sum = sum + Math.pow( (x.get(i, j) - 3d), 2d); 
            }
            ycurr = 10 / (5 + sum);
            yCol.appendDoubleValue(ycurr);
        }
        
        uballXData.addColumn(yCol);
      
    }
    
    @Override
    public void allDataProcessed() throws Exception {
    }
}

import com.connexience.server.workflow.*;
import org.pipeline.core.data.Data;

public class MyService implements WorkflowBlock {

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {
        
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        Data d = inputs.getInputDataSet("input-1");
        outputs.setOutputDataSet("output-1", d);
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {
        
    }
}
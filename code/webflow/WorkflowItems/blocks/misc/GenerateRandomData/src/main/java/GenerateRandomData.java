/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.util.ZipUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.IntegerColumn;

import java.io.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class GenerateRandomData extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {

    }

    /**
     * This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        int minRows = getProperties().intValue("MinRows", 1);
        int maxRows = getProperties().intValue("MaxRows", 100000);
        int valuesPerRow = getProperties().intValue("ValuesPerRow", 10);
        double minVal = getProperties().doubleValue("MinValue", 1.0);
        double maxVal = getProperties().doubleValue("MaxValue", 1000.0);
        int precision = getProperties().intValue("Precision", 2);
        String separator = getProperties().stringValue("Separator", ",");
        boolean columnNames = getProperties().booleanValue("IncludeColumnNames", true);

        //Set the decimal places
        DecimalFormat df = new DecimalFormat("#." + precision + "#");
        df.setMinimumFractionDigits(precision);
        df.setMaximumFractionDigits(precision);
        df.setRoundingMode(RoundingMode.DOWN);

        int numRows = minRows + (int) (Math.random() * ((maxRows - minRows) + 1));
        File file = createTempFile(ZipUtils.escapeFileName("random.csv"), getWorkingDirectory());
        System.out.println("Creating " + numRows + " rows of data");
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));

        //write the headers
        if (columnNames) {
            String headers = "";
            for (int i = 0; i < valuesPerRow; i++) {
                headers += "Column" + i + separator;
            }
            headers = headers.substring(0, headers.length() - 1); //remove the trailing separator
            writer.write(headers + "\n");
        }

        for (int i = 0; i < numRows; i++) {
            String thisLine = "";
            for (int j = 0; j < valuesPerRow; j++) {
                double val = minVal + (Math.random() * ((maxVal - minVal) + 1));
                String valString = df.format(val);  //strip to n decimal palces
                thisLine += valString + separator;
            }
            thisLine = thisLine.substring(0, thisLine.length() - 1); //remove the trailing separator
            writer.write(thisLine + "\n");
            writer.flush();
        }

        //set output
        writer.close();
        FileWrapper outputFiles = new FileWrapper(getWorkingDirectory());
        outputFiles.addFile(file, false);
        setOutputData("output-file", outputFiles);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {

    }
}
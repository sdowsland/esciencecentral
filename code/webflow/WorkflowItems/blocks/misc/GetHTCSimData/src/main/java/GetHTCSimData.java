
import com.connexience.libraries.jdbc.DriverLoader;
import com.connexience.libraries.jdbc.JDBCConnectionInformation;
import com.connexience.server.workflow.*;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.sql.ResultSetConverter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class GetHTCSimData implements WorkflowBlock {

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {

    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {

        String workflowId = env.getStringProperty("WorkflowId", "");
        DriverLoader.loadDrivers();

        JDBCConnectionInformation jdbcConnexience = new JDBCConnectionInformation();
        jdbcConnexience.setUsername("connexience");
        jdbcConnexience.setPassword("connexience");
        jdbcConnexience.setJdbcString("jdbc:postgresql://localhost:5432/connexience3");

        JDBCConnectionInformation jdbcLogevents = new JDBCConnectionInformation();
        jdbcLogevents.setUsername("connexience");
        jdbcLogevents.setPassword("connexience");
        jdbcLogevents.setJdbcString("jdbc:postgresql://localhost:5432/logeventsdb");

        Connection connexienceConnection = jdbcConnexience.openConnection();
        Connection logeventsConnection = jdbcLogevents.openConnection();

        Data outputData = new Data();

        Column invIdCol = new StringColumn("invocationId");
        outputData.addColumn(invIdCol);

        Column queuedTimeCol = new IntegerColumn("queuedTime");
        outputData.addColumn(queuedTimeCol);

        Column deQueuedTimeCol = new DateColumn("deQueuedTime");
        outputData.addColumn(deQueuedTimeCol);

        Column exStartTimeCol = new DateColumn("exStartTime");
        outputData.addColumn(exStartTimeCol);

        Column exEndTimeCol = new DateColumn("exEndTime");
        outputData.addColumn(exEndTimeCol);

        Column serviceIdCol = new StringColumn("serviceId");
        outputData.addColumn(serviceIdCol);

        Column serviceStartTimeCol = new DateColumn("serviceStartTime");
        outputData.addColumn(serviceStartTimeCol);

        Column serviceEndTimeCol = new DateColumn("serviceEndTime");
        outputData.addColumn(serviceEndTimeCol);

//        invocationdate timestamp without time zone,
//                queuedtime timestamp without time zone,
//        dequeuedtime timestamp without time zone,
//                executionstarttime timestamp without time zone,
//        executionendtime timestamp without time zone,
//

        String objectsFlatSQL = "SELECT " +
                "id AS invId, " +
                "queuedtime AS queuedtime, " +
                "dequeuedtime AS dequeuedtime, " +
                "executionstarttime AS executionstarttime, " +
                "executionendtime AS executionendtime " +
                "FROM objectsflat " +
                "WHERE workflowid = ?";
        PreparedStatement objFlatStmt = connexienceConnection.prepareStatement(objectsFlatSQL);
        objFlatStmt.setString(1, workflowId);
        ResultSet objFlatRS = objFlatStmt.executeQuery();

        while (objFlatRS.next()) {

            //todo: add precision to timestamp
            String invocationId = objFlatRS.getString("invId");
            Long queuedTime = objFlatRS.getLong("queuedtime");
            Date dequeuedTime = objFlatRS.getTimestamp("dequeuedtime");
            Date exStartTime = objFlatRS.getTimestamp("executionstarttime");
            Date exEndTime = objFlatRS.getTimestamp("executionendtime");



            //find out how much the block read and wrote
            String graphOpsQuery = "select * " +
                    "from graphoperations  " +
                    "where invocationid = ? and " +
                    "documentid is not null";

            PreparedStatement graphOpsStmt = logeventsConnection.prepareStatement(graphOpsQuery);
            graphOpsStmt.setString(1, invocationId);
            ResultSet datasizesRS = graphOpsStmt.executeQuery();



            //for each block in the workflow
            String logeventsQuery = "SELECT " +
                    "serviceid as serviceid, " +
                    "starttime as starttime, " +
                    "endtime as endtime " +
                    "FROM executions " +
                    "WHERE invocationid = ?";


            PreparedStatement logeventsStmt = logeventsConnection.prepareStatement(logeventsQuery);
            logeventsStmt.setString(1, invocationId);
            ResultSet executionsRS = logeventsStmt.executeQuery();

            while(executionsRS.next()){
                String serviceId = executionsRS.getString("serviceid");
                Date serviceStartTime = new Date(executionsRS.getLong("starttime"));
                Date serviceEndTime = new Date(executionsRS.getLong("endtime"));

                invIdCol.appendStringValue(invocationId);
                queuedTimeCol.appendObjectValue(queuedTime);
                deQueuedTimeCol.appendObjectValue(dequeuedTime);
                exStartTimeCol.appendObjectValue(exStartTime);
                exEndTimeCol.appendObjectValue(exEndTime);
                serviceIdCol.appendStringValue(serviceId);
                serviceStartTimeCol.appendObjectValue(serviceStartTime);
                serviceEndTimeCol.appendObjectValue(serviceEndTime);
            }
            executionsRS.close();
            logeventsStmt.close();
        }
        objFlatRS.close();
        objFlatStmt.close();


        outputs.setOutputDataSet("sim-data", outputData);
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {

    }
}
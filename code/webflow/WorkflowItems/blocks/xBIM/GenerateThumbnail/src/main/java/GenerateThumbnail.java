
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.util.CommandRunner;
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class GenerateThumbnail implements WorkflowBlock {


    @Override
    public void preExecute(BlockEnvironment env) throws Exception {
    }


    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        List<File> outputFiles = new ArrayList<File>();
        List<File> streamFiles = new ArrayList<File>();
        List<File> inputFiles = inputs.getInputFiles("input-files");

        File xBIMInterpretDir = env.getDependencyDirectory("xBIMToolkit");

        for (File inputFile : inputFiles) {
            String inputFileNameWithoutExt = inputFile.getName().substring(0, inputFile.getName().lastIndexOf('.'));

            //Command string
            //XbimThumbnail.exe IFC_FILE THUMB_FILE bmpwidth bmpHeight

            File bmpOutputFile = new File(inputFileNameWithoutExt + ".bmp");

            List<String> command = new ArrayList<String>();
            command.add(xBIMInterpretDir + File.separator + "XbimThumbnail.exe");
            command.add(inputFile.getPath()); // + " " + bmpOutputFile.getPath() + " 800 600\"");

            System.out.println("\n****COMMAND****");
            for (String s : command) {
                System.out.print(s + " ");
            }
            System.out.println("\n****END COMMAND****\n\n");

            String[] commandArray = command.toArray(new String[command.size()]);
            ProcessBuilder pb = new ProcessBuilder(commandArray);

            File stdOutFile = new File(inputFileNameWithoutExt + "-generatethumbnail-stdout.txt");
            File stdErrFile = new File(inputFileNameWithoutExt + "-generatethumbnail-stderr.txt");

            //Call Parser and redirect the outputs to files
            pb.redirectOutput(ProcessBuilder.Redirect.appendTo(stdOutFile));
            pb.redirectError(ProcessBuilder.Redirect.appendTo(stdErrFile));

            System.out.println("Output from process has been redirected to files");

            //Kick off and wait for the process to finish
            Process process = pb.start();

//            Thread.sleep(10*1000);
//            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
//
//            out.write("\n");
//            out.newLine();
//            out.flush();
//            out.close();

            try {
                int exitValue = process.waitFor();

                System.out.println("\n\nExit Value converting " + inputFile.getName() + ": " + exitValue);
                if (exitValue != 0) {

                    //don't fail the entire list if one file fails
                    System.err.println("Non zero exit value when converting " + inputFile.getAbsolutePath() + " : " + exitValue);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //If the STD::OUT and ERR exist then set them as outputs
            if (stdOutFile.exists() && stdOutFile.length() > 0) {
                streamFiles.add(stdOutFile);
            } else {
                System.out.println("STD::OUT does not exist when converting: " + inputFile.getName());
            }

            if (stdErrFile.exists() && stdErrFile.length() > 0) {
                streamFiles.add(stdErrFile);
            } else {
                System.out.println("STD::ERR does not exist when converting: " + inputFile.getName());
            }

            //Add the XLS output file
            if (bmpOutputFile.exists() && bmpOutputFile.length() > 0) {
                outputFiles.add(bmpOutputFile);
            } else {
                System.out.println("No BMP file generated when converting: " + inputFile.getName());
            }
        }

        outputs.setOutputFiles("output-files", outputFiles);
        outputs.setOutputFiles("stdout-stderr", streamFiles);
    }


    @Override
    public void postExecute(BlockEnvironment env) throws Exception {
    }

}

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.libraries.tsb.CompaniesHouseJDBCReference;
import com.connexience.server.workflow.*;

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.sql.*;
import java.util.Properties;

public class
        TSBJDBCConnection extends CloudDataProcessorService {
    private Connection c = null;
    Statement s = null;
    
    @Override
    public void executionAboutToStart() throws Exception {
        com.connexience.libraries.jdbc.DriverLoader.loadDrivers();
    }

    @Override
    public void execute() throws Exception {
        String url = getEditableProperties().stringValue("JDBCURL", null);
        String username = getEditableProperties().stringValue("Username", null);
        String password = getEditableProperties().stringValue("Password", null);
        String companiesHouseTableName = getEditableProperties().stringValue("CompaniesHouseTable", null);
        String libraryTableName = getEditableProperties().stringValue("LibraryTable", null);
        String neccTableName = getEditableProperties().stringValue("NECCTable", null);
        String processedTableName = getEditableProperties().stringValue("ProcessedDataTableName", null);
        String masterTableName = getEditableProperties().stringValue("MasterTableName", null);

        // Test the connection
        Properties props = new Properties();
        props.put("user", username);
        props.put("password", password);
        c = DriverManager.getConnection(url, props);

        // Check the tables exist
        s = c.createStatement();
        ResultSet r = null;
        
        try {
            r = s.executeQuery("select count(*) from " + companiesHouseTableName);
            if(r.next()){
                System.out.println("Companies House table contains: " + r.getString(1) + " records");
            }
        } catch (Exception e){
            throw new Exception("Cannot query CompaniesHouseTable");
        } finally {
            try {r.close();}catch (Exception e){}
        }

        try {
            r = s.executeQuery("select count(*) from " + libraryTableName);
            if(r.next()){
                System.out.println("Library table contains: " + r.getString(1) + " records");
            }            
        } catch (Exception e){
            throw new Exception("Cannot query library table");
        } finally {
            try {r.close();}catch (Exception e){}
        }
    
        try {
            r = s.executeQuery("select count(*) from " + neccTableName);
            if(r.next()){
                System.out.println("NECC table contains: " + r.getString(1) + " records");
            }            
        } catch (Exception e){
            throw new Exception("Cannot query NECC table");
        } finally {
            try {r.close();}catch (Exception e){}
        }

        try {
            r = s.executeQuery("select count(*) from " + masterTableName);
            if(r.next()){
                System.out.println("Master table contains: " + r.getString(1) + " records");
            }
        } catch (Exception e){
            throw new Exception("Cannot query Master table");
        } finally {
            try {r.close();}catch (Exception e){}
        }


        
        CompaniesHouseJDBCReference results = new CompaniesHouseJDBCReference();
        
        // Use a backup of library data
        if(getEditableProperties().booleanValue("", true)){
            String newLibraryTableName = libraryTableName + "_" + System.currentTimeMillis();
            s.executeUpdate("select * into " + newLibraryTableName + " from " + libraryTableName);
            results.setLibraryTableName(newLibraryTableName);
            results.setLibraryBackupUsed(true);
        } else {
            results.setLibraryTableName(libraryTableName);    
            results.setLibraryBackupUsed(false);
        }
        
        results.setCompaniesHouseTableName(companiesHouseTableName);
        results.setJdbcString(url);
        results.setNeccTableName(neccTableName);
        results.setPassword(password);
        results.setUsername(username);
        results.setProcessedTableName(processedTableName);
        results.setMasterTableName(masterTableName);

        ObjectWrapper output = new ObjectWrapper(results);
        
        setOutputData("jdbc-information", output);   
    }

    @Override
    public void allDataProcessed() throws Exception {
        if(s!=null){
            try {s.close();}catch(Exception e){}
        }
        
        if(c!=null){
            try {c.close();}catch(Exception e){}
        }
    }
}
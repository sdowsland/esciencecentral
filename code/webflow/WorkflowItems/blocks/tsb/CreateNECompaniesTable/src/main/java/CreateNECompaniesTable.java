
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.libraries.jdbc.DriverLoader;
import com.connexience.libraries.tsb.CompaniesHouseJDBCReference;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import org.postgresql.core.Query;

import java.sql.Connection;
import java.sql.Statement;

public class CreateNECompaniesTable extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
        DriverLoader.loadDrivers();
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        // Pass on the jdbc reference
        CompaniesHouseJDBCReference jdbcReference = (CompaniesHouseJDBCReference) ((ObjectWrapper) getInputData("jdbc-information")).getPayload();
        String tableName = getEditableProperties().stringValue("NECompaniesTableName", null);
        jdbcReference.setNeCompaniesTableName(tableName);

        setOutputData("jdbc-information", new ObjectWrapper(jdbcReference));
        Connection c = null;
        Statement s = null;

        try {
            c = jdbcReference.openConnection();
            s = c.createStatement();

            if (getEditableProperties().booleanValue("EmptyTableFirst", true)) {
                System.out.println("Dropping table: " + tableName);
                s.executeUpdate("drop table if exists " + tableName);
            }
            s.executeUpdate("select  * into " + tableName + " from chdata where regaddresspostcode like 'TS%' or regaddresspostcode like 'NE%' or regaddresspostcode like 'SR%' or regaddresspostcode like 'DL%' or regaddresspostcode like 'DH%'");
            System.out.println("Created table: " + jdbcReference.getProcessedTableName());

            //Create the cleaned company name column
            s.executeUpdate("update chne set cleancompanyname = replace(upper(companyname), 'LIMITED','');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), 'LTD', '');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), 'LLP', '');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), 'AND', '');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), 'PLC', '');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '&', '');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), 'THE', '');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '.', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), ',', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '''', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '-', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '(', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), ')', ' ');");

            //Reduce internal spaces
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '  ', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '  ', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '  ', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '  ', ' ');");
            s.executeUpdate("UPDATE chne set cleancompanyname = replace(upper(cleancompanyname), '  ', ' ');");

            //Trim space from both ends
            s.executeUpdate("UPDATE chne set cleancompanyname = trim(both ' ' from cleancompanyname);");

            //Add a primary key column
            s.executeUpdate("ALTER TABLE chne ADD COLUMN id BIGSERIAL PRIMARY KEY;");


        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (s != null) {
                    s.close();
                }
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
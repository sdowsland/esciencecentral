
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.libraries.jdbc.DriverLoader;
import com.connexience.libraries.tsb.CompaniesHouseJDBCReference;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.sql.Connection;
import java.sql.Statement;
import org.pipeline.core.data.*;

public class CreateMasterTable extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
        DriverLoader.loadDrivers();
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        // Pass on the jdbc reference
        String tableName = getEditableProperties().stringValue("MasterTableName", "master");
        CompaniesHouseJDBCReference jdbcReference = (CompaniesHouseJDBCReference)((ObjectWrapper)getInputData("jdbc-information")).getPayload();
        jdbcReference.setMasterTableName(tableName);
        setOutputData("jdbc-information", new ObjectWrapper(jdbcReference));
        
        Connection c = null;
        Statement s = null;
        
        try {
            
            
            c = jdbcReference.openConnection();
            c.setAutoCommit(false);
            s = c.createStatement();
            String sql="create table if not exists " + tableName + "(\n" +
                        "CHCompanyName text,\n" +
                        "CHCompanyNumber text,\n" +
                        "CHRegAddressCareOf text,\n" +
                        "CHRegAddressPOBox text,\n" +
                        "CHRegAddressAddressLine1 text,\n" +
                        "CHRegAddressAddressLine2 text,\n" +
                        "CHRegAddressPostTown text,\n" +
                        "CHRegAddressCounty text,\n" +
                        "CHRegAddressCountry text,\n" +
                        "CHRegAddressPostCode text,\n" +
                        "CHCompanyCategory text,\n" +
                        "CHCompanyStatus text,\n" +
                        "CHCountryOfOrigin text,\n" +
                        "CHDissolutionDate text,\n" +
                        "CHIncorporationDate text,\n" +
                        "CHAccountsAccountRefDay text,\n" +
                        "CHAccountsAccountRefMonth text,\n" +
                        "CHAccountsNextDueDate text,\n" +
                        "CHAccountsLastMadeUpDate text,\n" +
                        "CHAccountsAccountCategory text,\n" +
                        "CHReturnsNextDueDate text,\n" +
                        "CHReturnsLastMadeUpDate text,\n" +
                        "CHMortgagesNumMortCharges text,\n" +
                        "CHMortgagesNumMortOutstanding text,\n" +
                        "CHMortgagesNumMortPartSatisfied text,\n" +
                        "CHMortgagesNumMortSatisfied text,\n" +
                        "CHSICCodeSicText_1 text,\n" +
                        "CHSICCodeSicText_2 text,\n" +
                        "CHSICCodeSicText_3 text,\n" +
                        "CHSICCodeSicText_4 text,\n" +
                        "CHLimitedPartnershipsNumGenPartners text,\n" +
                        "CHLimitedPartnershipsNumLimPartners text,\n" +
                        "CHURI text,\n" +
                        "CHPreviousName_1CONDATE text,\n" +
                        "CHPreviousName_1CompanyName text,\n" +
                        "CHPreviousName_2CONDATE text,\n" +
                        "CHPreviousName_2CompanyName text,\n" +
                        "CHPreviousName_3CONDATE text,\n" +
                        "CHPreviousName_3CompanyName text,\n" +
                        "CHPreviousName_4CONDATE text,\n" +
                        "CHPreviousName_4CompanyName text,\n" +
                        "CHPreviousName_5CONDATE text,\n" +
                        "CHPreviousName_5CompanyName text,\n" +
                        "CHPreviousName_6CONDATE text,\n" +
                        "CHPreviousName_6CompanyName text,\n" +
                        "CHPreviousName_7CONDATE text,\n" +
                        "CHPreviousName_7CompanyName text,\n" +
                        "CHPreviousName_8CONDATE text,\n" +
                        "CHPreviousName_8CompanyName text,\n" +
                        "CHPreviousName_9CONDATE text,\n" +
                        "CHPreviousName_9CompanyName text,\n" +
                        "CHPreviousName_10CONDATE text,\n" +
                        "CHPreviousName_10CompanyName text,\n" +
                        "LIBCompanyName text,\n" +
                        "LIBRegisteredNumber text,\n" +
                        "LIBTypeofAccounts text,\n" +
                        "LIBCorporateStatus text,\n" +
                        "LIBTradingStatus text,\n" +
                        "LIBAccountingReferenceDate text,\n" +
                        "LIBPrincipalActivityDescription text,\n" +
                        "LIBTradingAddress1 text,\n" +
                        "LIBTradingAddress2 text,\n" +
                        "LIBTradingAddress3 text,\n" +
                        "LIBTradingAddress4 text,\n" +
                        "LIBTradingTown text,\n" +
                        "LIBTradingPostcode text,\n" +
                        "LIBTradingCountry text,\n" +
                        "LIBTradingPhoneNumber text,\n" +
                        "LIBUKSICCode text,\n" +
                        "LIBUKSICDesc text,\n" +
                        "LIBTotalSalesSLASHTurnover text,\n" +
                        "LIBGrossProfit  text,\n" +
                        "LIBEBIT text,\n" +
                        "LIBEBITDA  text,\n" +
                        "LIBProfitBeforeTax  text,\n" +
                        "LIBProfitAfterTax  text,\n" +
                        "LIBNumberofEmployees  text,\n" +
                        "LIBCommDelphiScoreCur text);";
            s.executeUpdate(sql);
            
        } catch (Exception e){
            e.printStackTrace();
            if(c!=null){
                try {c.rollback();}catch(Exception ex){}
            }
            throw e;
        } finally {
            try {if(s!=null){s.close();}}catch(Exception e){}
            try {
                c.commit();
                c.close();
            }catch(Exception e){}
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
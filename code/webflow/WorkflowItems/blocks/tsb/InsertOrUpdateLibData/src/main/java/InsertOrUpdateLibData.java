
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.libraries.jdbc.DriverLoader;
import com.connexience.libraries.tsb.CompaniesHouseJDBCReference;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class InsertOrUpdateLibData extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
        DriverLoader.loadDrivers();
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        // Pass on the jdbc reference
        CompaniesHouseJDBCReference jdbcReference = (CompaniesHouseJDBCReference) (getInputData("jdbc-information")).getPayload();
        String processedTableName = jdbcReference.getProcessedTableName();
        String masterTableName = jdbcReference.getMasterTableName();

        setOutputData("jdbc-information", new ObjectWrapper(jdbcReference));

        Statement processedStatement = null;
        PreparedStatement rowExistsInMasterStatement = null;

        Connection c = jdbcReference.openConnection();

        try {

            int limit = getProperties().intValue("Limit", -1);

            //Get all of the Companies House data
            String processedQuery = "SELECT * FROM " + processedTableName;

            processedQuery += " ORDER BY id ASC";

            if (limit > 0) {
                processedQuery += " limit " + limit;
            }

            System.out.println("Performing:" + processedQuery);

            processedStatement = c.createStatement();
            ResultSet processedRS = processedStatement.executeQuery(processedQuery);

            //find the number of CH NE records so that we can update progress
            Statement s = c.createStatement();
            ResultSet countResults = s.executeQuery("SELECT COUNT(*) FROM " + processedTableName);
            Long total = 0L;
            if (countResults.next()) {
                total = countResults.getLong(1);
            }

            int count = 0;
            Set<String> columnNames = new HashSet<>();

            while (processedRS.next()) {

                //Get the list of column names only once
                if (columnNames.isEmpty()) {
                    for (int i = 1; i < processedRS.getMetaData().getColumnCount() +1; i++) {
                        String columnName = processedRS.getMetaData().getColumnName(i);
                        columnNames.add(columnName);
                    }
                    columnNames.remove("id");
                }

                //Get the CH data for this row
                HashMap<String, String> thisRow = new HashMap<>();
                for (String columnName : columnNames) {
                    String value = processedRS.getString(columnName);
                    thisRow.put(columnName, value);
                }

                String masterQuery = "SELECT authcompanynumber FROM " + masterTableName + " WHERE authcompanynumber = ?";
                rowExistsInMasterStatement = c.prepareStatement(masterQuery);
                rowExistsInMasterStatement.setString(1, thisRow.get("registerednumber"));
                ResultSet existingRow = rowExistsInMasterStatement.executeQuery();

                //If the row exists in the Master DB then update it, if not insert it
                if (existingRow.next()) {

                    //Create the base query
                    String updateQuery = "UPDATE master SET ";
                    for (String columnName : columnNames) {
                        String thisValue = thisRow.get(columnName);
                        if(thisValue != null) {
                            thisValue = thisValue.replace("\'", "");
                        }

                        updateQuery += "lib" + columnName + "='" + thisValue + "',";
                    }

                    //Drop the trailing command add add where clause
                    updateQuery = updateQuery.substring(0, updateQuery.length() - 1);
                    updateQuery += " WHERE authcompanynumber = '" + thisRow.get("registerednumber") + "'";

                    PreparedStatement updateStatement = c.prepareStatement(updateQuery);
                    updateStatement.executeUpdate();
                    updateStatement.close();
                } else {

                    //row doesn't exist, insert
                    String insertQueryCols = "INSERT INTO master (authcompanynumber,";
                    String insertQueryVals = " VALUES ('" + thisRow.get("registerednumber") + "',";

                    for (String columnName : columnNames) {
                        insertQueryCols += "lib" + columnName + ",";
                        String thisValue = thisRow.get(columnName);
                        if(thisValue != null) {
                            thisValue = thisValue.replace("\'", "");
                        }
                        insertQueryVals += "'" + thisValue + "',";
                    }

                    //drop trailing commas
                    insertQueryCols = insertQueryCols.substring(0, insertQueryCols.length() - 1) + ") ";
                    insertQueryVals = insertQueryVals.substring(0, insertQueryVals.length() - 1) + ");";
                    String insertQuery = insertQueryCols + insertQueryVals;

                    PreparedStatement insertStatement = c.prepareStatement(insertQuery);
                    insertStatement.executeUpdate();
                    insertStatement.close();
                }

                count++;
                if (total != 0) {
                    reportProgress(total, count);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                try {
                    c.rollback();
                } catch (Exception ignored) {
                }
            }
            throw e;
        } finally {
            try {
                if (processedStatement != null) {
                    processedStatement.close();
                }
                if (rowExistsInMasterStatement != null) {
                    {
                        rowExistsInMasterStatement.close();
                    }
                }
            } catch (Exception ignored) {
            }
            try {
                if (c != null) {
                    c.commit();
                    c.close();
                }
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}

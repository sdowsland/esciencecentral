
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.libraries.tsb.CompaniesHouseJDBCReference;
import com.connexience.server.workflow.*;

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.sql.*;
import java.util.Properties;

public class AddCompanyNumberFromCH extends CloudDataProcessorService {
    private Connection c = null;
    Statement s = null;
    
    @Override
    public void executionAboutToStart() throws Exception {
        com.connexience.libraries.jdbc.DriverLoader.loadDrivers();
    }

    @Override
    public void execute() throws Exception {
        // Pass on the jdbc reference
        CompaniesHouseJDBCReference jdbcReference = (CompaniesHouseJDBCReference)((ObjectWrapper)getInputData("jdbc-information")).getPayload();
        setOutputData("jdbc-information", new ObjectWrapper(jdbcReference));
        Connection c = null;
        Statement s = null;

        try {
            String query = getEditableProperties().stringValue("SQLQuery", "");
            System.out.println("Performing:" + query);

            c = jdbcReference.openConnection();
            c.setAutoCommit(false);
            s = c.createStatement();
            s.executeUpdate(query);
        } catch (Exception e){
            e.printStackTrace();
            if(c!=null){
                try {c.rollback();}catch(Exception ex){}
            }
            throw e;
        } finally {
            try {if(s!=null){s.close();}}catch(Exception e){}
            try {
                c.commit();
                c.close();
            }catch(Exception e){}
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
        if(s!=null){
            try {s.close();}catch(Exception e){}
        }
        
        if(c!=null){
            try {c.close();}catch(Exception e){}
        }
    }
}
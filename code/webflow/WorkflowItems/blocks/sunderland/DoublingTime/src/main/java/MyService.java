
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.manipulation.*;
import org.pipeline.core.xmlstorage.*;

public class MyService extends CloudDataProcessorService {

    /**
     * Raw data for fitting
     */
    private Data rawData;
    /**
     * Concentration values
     */
    private DoubleColumn concentrationColumn;
    /**
     * Fitted experimental data
     */
    private DoubleColumn observedColumn;
    /**
     * Maximum observed values
     */
    private DoubleColumn maximumObservedColumn;
    /**
     * Minimum observed values
     */
    private DoubleColumn minimumObservedColumn;
    /**
     * Data containing the matrix of experimental values
     */
    private Data experimentalMatrix;
    /**
     * Column picker to select the concentration data
     */
    private ColumnPicker concentrationPicker = new ColumnPicker();
    /**
     * Fitted a value
     */
    private double a = 0;
    /**
     * Fitted b value
     */
    private double b = 0;

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        String concentrationColumnName = getEditableProperties().stringValue("ConcentrationColumn", "#0");
        concentrationPicker.configure(concentrationColumnName);
        rawData = getInputDataSet("experimental-data");
        setupFittingData();
        exponentialRegression(concentrationColumn, observedColumn);


        Data observed = new Data();
        observed.addColumn(concentrationColumn);
        observed.addColumn(observedColumn);
        observed.addColumn(minimumObservedColumn);
        observed.addColumn(maximumObservedColumn);
        setOutputDataSet("observed-data", observed);

        XmlDataStore parameters = new XmlDataStore("Parameters");
        parameters.add("A", a);
        parameters.add("B", b);
        PropertiesWrapper paramWrapper = new PropertiesWrapper(parameters);
        setOutputData("model-parameters", paramWrapper);

        XmlDataStore report = new XmlDataStore("Report");
        report.add("A", a);
        report.add("B", b);
        report.add("Equation", a + "exp(" + b + " x Time)");
        report.add("DoublingTime", (Math.log(2) / b));
        report.add("ConcentrationAtT0", a);
        PropertiesWrapper reportWrapper = new PropertiesWrapper(report);

        setOutputData("fitting-report", reportWrapper);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }

    /**
     * Convert the raw data into fitting data
     */
    private void setupFittingData() throws DataException {
        ColumnPickerCollection pickers = new ColumnPickerCollection();
        pickers.addColumnPicker(concentrationPicker);

        concentrationColumn = (DoubleColumn) concentrationPicker.pickColumn(rawData);
        experimentalMatrix = pickers.extractNonSelectedData(rawData);

        // Need to build a single column containing the mean of the experimental matrix
        observedColumn = new DoubleColumn("fittingdata");
        maximumObservedColumn = new DoubleColumn("max");
        minimumObservedColumn = new DoubleColumn("min");
        for (int i = 0; i < experimentalMatrix.getLargestRows(); i++) {
            observedColumn.appendDoubleValue(rowMean(experimentalMatrix, i));
            maximumObservedColumn.appendDoubleValue(rowMax(experimentalMatrix, i));
            minimumObservedColumn.appendDoubleValue(rowMin(experimentalMatrix, i));
        }
    }

    /**
     * Calculate the maximum of a row of data
     */
    private double rowMax(Data d, int row) throws DataException {
        double max = -Double.MAX_VALUE;
        int count = 0;
        for (int i = 0; i < d.getColumns(); i++) {
            if (!d.column(i).isMissing(row)) {
                if (((NumericalColumn) d.column(i)).getDoubleValue(row) > max) {
                    max = ((NumericalColumn) d.column(i)).getDoubleValue(row);
                }
                count++;
            }
        }

        if (count > 0) {
            return max;
        } else {
            throw new DataException("No valid values in data row");
        }
    }

    /**
     * Calculate the minimum of a row of data
     */
    private double rowMin(Data d, int row) throws DataException {
        double min = Double.MAX_VALUE;
        int count = 0;
        for (int i = 0; i < d.getColumns(); i++) {
            if (!d.column(i).isMissing(row)) {
                if (((NumericalColumn) d.column(i)).getDoubleValue(row) < min) {
                    min = ((NumericalColumn) d.column(i)).getDoubleValue(row);
                }
                count++;
            }
        }

        if (count > 0) {
            return min;
        } else {
            throw new DataException("No valid values in data row");
        }
    }

    /**
     * Calculate the mean of a row of data
     */
    private double rowMean(Data d, int row) throws DataException {
        double sum = 0;
        int count = 0;
        for (int i = 0; i < d.getColumns(); i++) {
            if (!d.column(i).isMissing(row)) {
                sum = sum + ((NumericalColumn) d.column(i)).getDoubleValue(row);
                count++;
            }
        }

        if (count > 0) {
            return sum / (double) count;
        } else {
            throw new DataException("No valid values in data row");
        }
    }

    public void exponentialRegression(DoubleColumn xColumn, DoubleColumn yColumn) throws Exception {
        if (xColumn.getRows() == yColumn.getRows()) {
            int rows = xColumn.getRows();
            int i;
            double t1 = 0;
            double t2 = 0;
            double t3 = 0;
            double t4 = 0;
            double t5 = 0;

            for (i = 0; i < rows; i++) {

                // sum(x^2  y)
                t1 = t1 + (Math.pow(xColumn.getDoubleValue(i), 2) * yColumn.getDoubleValue(i));

                // sum(y ln(y))
                t2 = t2 + (yColumn.getDoubleValue(i) * Math.log(yColumn.getDoubleValue(i)));

                // sum(x y)
                t3 = t3 + (xColumn.getDoubleValue(i) * yColumn.getDoubleValue(i));

                // sum(xy ln(y)
                t4 = t4 + (xColumn.getDoubleValue(i) * yColumn.getDoubleValue(i) * Math.log(yColumn.getDoubleValue(i)));

                // sum(y);
                t5 = t5 + yColumn.getDoubleValue(i);

            }


            a = Math.exp(((t1 * t2) - (t3 * t4)) / ((t5 * t1) - Math.pow(t3, 2)));
            b = ((t5 * t4) - (t3 * t2)) / ((t5 * t1) - Math.pow(t3, 2));
            /*
             * return{ a: Math.exp(a), b: b }
             *
             */
        } else {
            throw new Exception("Number of rows in x and y columns do not match");
        }
    }
}
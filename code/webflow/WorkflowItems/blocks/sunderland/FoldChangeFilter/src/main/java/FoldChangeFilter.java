/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import org.pipeline.core.data.Data;
import org.pipeline.core.data.io.DelimitedTextDataImporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: nsjw7
 * Date: 08/08/2012
 * Time: 08:40
 * Parse Jill's CSV file and split into three files based on number of occurences of data in the last 3 columns.
 * Output each protein split on semi-colon into different files.
 */
public class FoldChangeFilter
{

  private Data inputData;
  private Double threshold1 = Double.MAX_VALUE;
  private Double threshold2 = Double.MIN_NORMAL;

  public FoldChangeFilter(Data inputData, Double threshold1, Double threshold2)
  {
    this.inputData = inputData;
    this.threshold1 = threshold1;
    this.threshold2 = threshold2;
  }

  public Data parse() throws Exception
  {
    try
    {
      Data outputData = inputData.getEmptyCopy();

      int rows = inputData.getLargestRows();
      for (int i = 0; i < rows; i++)
      {
        //Get the Mix values
        Data thisRow = inputData.getRowSubset(i, i, true);
        String mix1Col = thisRow.column("Ratio H/L Normalized MIX_1").getStringValue(0);
        String mix2Col = thisRow.column("Ratio H/L Normalized MIX_2").getStringValue(0);
        String mix3Col = thisRow.column("Ratio H/L Normalized MIX_3").getStringValue(0);

        Double mix1 = Double.parseDouble(mix1Col);
        Double mix2 = Double.parseDouble(mix2Col);
        Double mix3 = Double.parseDouble(mix3Col);

        if (mix1 <= threshold1 || mix1 > threshold2)
        {
          outputData.appendRows(thisRow, false);
        }
        else if (mix2 <= threshold1 || mix2 > threshold2)
        {
          outputData.appendRows(thisRow, false);
        }
        else if (mix3 <= threshold1 || mix3 > threshold2)
        {
          outputData.appendRows(thisRow, false);
        }

      }
      return outputData;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }

  }

  //Test Method
  public static void main(String[] args)
  {
    InputStream inStream = null;

    try
    {

      File input = new File("/Users/nsjw7/Desktop/Fibro_SILAC_MaxQuant_02.csv");
      DelimitedTextDataImporter importer = new DelimitedTextDataImporter();

      // Set the properties
      int startRow = 2;
      int labelRow = 1;
      boolean containsLabels = true;
      String columnDelimiter = ",";
      int endRow = 1;
      boolean limitRows = false;
      boolean subsample = false;
      int sampleInterval = 1;

      importer.setDataStartRow(startRow);
      importer.setDataEndRow(endRow);
      importer.setDelimiterString(columnDelimiter);
      importer.setDelimiterType(DelimitedTextDataImporter.CUSTOM_DELIMITED);
      importer.setErrorsAsMissing(true);
      importer.setImportColumnNames(containsLabels);
      importer.setLimitRows(limitRows);
      importer.setNameRow(labelRow);
      importer.setSampleInterval(sampleInterval);
      importer.setSubsample(subsample);
      importer.setUseEnclosingQuotes(true);
      inStream = new FileInputStream(input);
      importer.setForceTextImport(true);


      Data mqData = importer.importInputStream(inStream);
      FoldChangeFilter filter = new FoldChangeFilter(mqData, 0.71, 1.4);
      Data outputData = filter.parse();

      System.out.println("outputs Row size = " + outputData.getLargestRows());

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      if (inStream != null)
      {
        try {inStream.close();} catch (Exception ignored) {}
      }
    }
  }


}

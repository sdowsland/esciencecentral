/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import au.com.bytecode.opencsv.CSVReader;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: nsjw7
 * Date: 08/08/2012
 * Time: 08:40
 * Parse Jill's CSV file and split into three files based on number of occurences of data in the last 3 columns.
 * Output each protein split on semi-colon into different files.
 */
public class ProteinFileParser
{

  private File inputFile;

  public ProteinFileParser()
  {
  }

  public ProteinFileParser(File inputFile)
  {
    this.inputFile = inputFile;
  }

  public Collection<File> parse() throws Exception
  {
    File onesFile = new File("ones.csv");
    File twosFile = new File("twos.csv");
    File threesFile = new File("threes.csv");


    BufferedWriter ones = new BufferedWriter(new FileWriter(onesFile));
    BufferedWriter twos = new BufferedWriter(new FileWriter(twosFile));
    BufferedWriter threes = new BufferedWriter(new FileWriter(threesFile));

    try
    {
      //Protein Names,Protein IDs,PEP,Ratio H/L Normalized MIX_1,Ratio H/L Normalized MIX_2,Ratio H/L Normalized MIX_3

      CSVReader reader = new CSVReader(new FileReader(inputFile));
      reader.readNext(); //header

      int lineNumber = 1;
      String[] data;
      int totalCount = 0;

      while ((data = reader.readNext()) != null)
      {
        lineNumber++;

        if (data.length != 6)
        {
          System.out.println(Arrays.toString(data));
          throw new Exception("More commas than expected.  Line number " + lineNumber + "Length = " + data.length);
        }

//        String proteinNames = data[0];
        String proteinsList = data[1];
//        String pep = data[2];
//        String mix1 = data[3];
//        String mix2 = data[4];
//        String mix3 = data[5];

        int count = 0;

        //Number of occurrences of data in the three columns
        for (int i = 3; i <= 5; i++)
        {
          if (!data[i].equals("NaN"))
          {
            count++;
          }
        }

        BufferedWriter outputFile = null;

        if (count == 0)
        {
          continue; //Not interested in data with no points
        }
        if (count == 1)
        {
          outputFile = ones;
        }
        else if (count == 2)
        {
          outputFile = twos;
        }
        else if (count == 3)
        {
          outputFile = threes;
        }

        //write out the proteins on a newline
        String[] proteins = proteinsList.split(";");
        for (String protein : proteins)
        {
          outputFile.write(protein + "\n");
        }
        totalCount += count;
      }

      System.out.println("Processed " + lineNumber + " lines");
      System.out.println("Found " + totalCount + " data points");

      //cleanup
      ones.close();
      twos.close();
      threes.close();

      //pass the files back
      Vector<File> outputFiles = new Vector<File>();
      outputFiles.add(onesFile);
      outputFiles.add(twosFile);
      outputFiles.add(threesFile);

      return outputFiles;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
    finally
    {
      ones.close();
      twos.close();
      threes.close();
    }
  }

  public static void main(String[] args)
  {
    File input = new File("/Users/nsjw7/Desktop/DataMining_Ratio.csv");
    ProteinFileParser p = new ProteinFileParser(input);
    try
    {
      p.parse();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


}

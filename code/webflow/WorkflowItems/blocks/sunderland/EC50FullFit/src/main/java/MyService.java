
import com.connexience.server.workflow.cloud.services.*;
import org.pipeline.core.data.*;
import com.connexience.server.workflow.engine.datatypes.*;


import com.connexience.apps.ec50.*;

public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {

        // The following method is used to get a set of input data
        // and should be configured using the name of the input
        // data to fetch
        Data experimentalData = getInputDataSet("experimental-data");
        String concentrationColumn = getEditableProperties().stringValue("ConcentrationColumn", "#0");
        int points = getEditableProperties().intValue("PredictedDataPoints", 100);
        
        EC50Fitter fitter = new EC50Fitter();
        fitter.setFitType(EC50Curve.FitType.TOPBOTTOMEC50);
        
        fitter.setRawData(experimentalData);
        fitter.getConcentrationPicker().configure(concentrationColumn);
        fitter.initialise();
        fitter.execute();
        
        // Set outputs
        setOutputData("parameters", new PropertiesWrapper(fitter.getParameters()));
        setOutputDataSet("predicted-data", fitter.getFittedDataPlotMatrix(points));
        setOutputDataSet("observed-data", fitter.getObservedDataPlotMatrix());
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
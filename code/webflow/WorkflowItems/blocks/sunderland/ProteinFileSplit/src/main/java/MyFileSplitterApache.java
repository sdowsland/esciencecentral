/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import au.com.bytecode.opencsv.CSVReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: nsjw7
 * Date: 08/08/2012
 * Time: 08:40
 * Parse Jill's CSV file and split into three files based on number of occurences of data in the last 3 columns.
 * Output each protein split on semi-colon into different files.
 */
public class MyFileSplitterApache
{

  private File inputFile;

  public MyFileSplitterApache()
  {
  }

  public MyFileSplitterApache(File inputFile)
  {
    this.inputFile = inputFile;
  }

  public Collection<File> parse() throws Exception
  {
    File zerosFile = new File("zeros.csv");
    File onesFile = new File("ones.csv");
    File twosFile = new File("twos.csv");
    File threesFile = new File("threes.csv");

    BufferedWriter zeros = new BufferedWriter(new FileWriter(zerosFile));
    BufferedWriter ones = new BufferedWriter(new FileWriter(onesFile));
    BufferedWriter twos = new BufferedWriter(new FileWriter(twosFile));
    BufferedWriter threes = new BufferedWriter(new FileWriter(threesFile));

    try
    {
      CSVReader reader = new CSVReader(new FileReader(inputFile));

      //Read the header and find which columns the data is in
      String[] headers = reader.readNext();
      Vector<Integer> columnIndex = new Vector<Integer>();
      for (int i = 0; i < headers.length; i++)
      {
        String header = headers[i];
        if (header.equals("Ratio H/L Normalized MIX_1") || header.equals("Ratio H/L Normalized MIX_2") || header.equals("Ratio H/L Normalized MIX_3"))
        {
          columnIndex.add(i);
        }
      }
      if (columnIndex.size() != 3)
      {
        throw new Exception("Can't find the columns, are headers set correctly? Looking for 'Ratio H/L Normalized MIX_'");
      }

      //write the header
      zeros.write(arrayToString(headers) + "\n");
      ones.write(arrayToString(headers) + "\n");
      twos.write(arrayToString(headers) + "\n");
      threes.write(arrayToString(headers) + "\n");

      int lineNumber = 1;  //0 is header
      String[] data;

      while ((data = reader.readNext()) != null)
      {
        lineNumber++;

        //Number of occurrences of data in the three columns
        int count = 0;
        for (Integer i : columnIndex)
        {
          if (!data[i].equals("NaN"))
          {
            count++;
          }
        }

        //Get the correct output file
        BufferedWriter outputFile = null;
        if (count == 0)
        {
          outputFile = zeros;
        }
        if (count == 1)
        {
          outputFile = ones;
        }
        else if (count == 2)
        {
          outputFile = twos;
        }
        else if (count == 3)
        {
          outputFile = threes;
        }

        //write the outpiut
        outputFile.write(arrayToString(data) + "\n");
      }


      System.out.println("Processed " + lineNumber + " lines");

      //cleanup
      ones.close();
      twos.close();
      threes.close();

      //pass the files back
      Vector<File> outputFiles = new Vector<File>();
      outputFiles.add(zerosFile);
      outputFiles.add(onesFile);
      outputFiles.add(twosFile);
      outputFiles.add(threesFile);

      return outputFiles;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
    finally
    {
      zeros.close();
      ones.close();
      twos.close();
      threes.close();
    }
  }

  public static void main(String[] args)
  {
    File input = new File("/Users/nsjw7/Desktop/Fibro_SILAC_MaxQuant_02.csv");
    MyFileSplitterApache p = new MyFileSplitterApache(input);
    try
    {
      p.parse();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private static String arrayToString(String[] data)
  {
    String d = Arrays.toString(data);
    return d.substring(1, d.length() - 1);
  }

}

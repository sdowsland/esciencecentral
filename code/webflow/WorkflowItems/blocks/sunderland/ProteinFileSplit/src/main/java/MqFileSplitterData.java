/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import org.pipeline.core.data.Data;
import org.pipeline.core.data.io.DelimitedTextDataImporter;
import org.pipeline.core.data.io.DelimitedTextDataStreamImporter;
import org.pipeline.core.data.manipulation.BestGuessDataTyper;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: nsjw7
 * Date: 08/08/2012
 * Time: 08:40
 * Parse Jill's CSV file and split into three files based on number of occurences of data in the last 3 columns.
 * Output each protein split on semi-colon into different files.
 */
public class MqFileSplitterData
{

  private Data inputData;

  public MqFileSplitterData()
  {
  }

  public MqFileSplitterData(Data inputData)
  {
    this.inputData = inputData;
  }

  public HashMap<String, Data> parse() throws Exception
  {
    //Create empty datasets which have the same cols as the input data
    Data onesFile = inputData.getEmptyCopy();
    Data twosFile = inputData.getEmptyCopy();
    Data threesFile = inputData.getEmptyCopy();

    try
    {
      int rows = inputData.getLargestRows();
      for (int i = 0; i < rows; i++)
      {
        //Get the Mix values
        Data thisRow = inputData.getRowSubset(i, i, true);
        String mix1Col = thisRow.column("Ratio H/L Normalized MIX_1").getStringValue(0);
        String mix2Col = thisRow.column("Ratio H/L Normalized MIX_2").getStringValue(0);
        String mix3Col = thisRow.column("Ratio H/L Normalized MIX_3").getStringValue(0);

        int count = 0;
        if (!mix1Col.equals("NaN")) {count++;}
        if (!mix2Col.equals("NaN")) {count++;}
        if (!mix3Col.equals("NaN")) {count++;}

        //Split into the files based on the number of non-NaN values
        if (count == 0)
        {
          //do nothing
        }
        else if (count == 1)
        {          // Sample this row
          onesFile.appendRows(thisRow, true);
        }
        else if (count == 2)
        {
          twosFile.appendRows(thisRow, true);
        }
        else if (count == 3)
        {
          threesFile.appendRows(thisRow, true);
        }
        else
        {
          throw new Exception("Unexpected number of mixes");
        }
      }

      //output the results
      HashMap<String, Data> results = new HashMap<String, Data>();
      results.put("ones", onesFile);
      results.put("twos", twosFile);
      results.put("threes", threesFile);

      return results;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }

  }

  //Test Method
  public static void main(String[] args)
  {
    InputStream inStream = null;

    try
    {

      File input = new File("/Users/nsjw7/Desktop/Fibro_SILAC_MaxQuant_02.csv");
      DelimitedTextDataImporter importer = new DelimitedTextDataImporter();

      // Set the properties
      int startRow = 2;
      int labelRow = 1;
      boolean containsLabels = true;
      String columnDelimiter = ",";
      int endRow = 1;
      boolean limitRows = false;
      boolean subsample = false;
      int sampleInterval = 1;
      boolean textData = false;
      int importChunkSize = 5000;

      importer.setDataStartRow(startRow);
      importer.setDataEndRow(endRow);
      importer.setDelimiterString(columnDelimiter);
      importer.setDelimiterType(DelimitedTextDataImporter.CUSTOM_DELIMITED);
      importer.setErrorsAsMissing(true);
      importer.setImportColumnNames(containsLabels);
      importer.setLimitRows(limitRows);
      importer.setNameRow(labelRow);
      importer.setSampleInterval(sampleInterval);
      importer.setSubsample(subsample);
      importer.setUseEnclosingQuotes(true);
      inStream = new FileInputStream(input);
      importer.setForceTextImport(true);
      /*
      importer.setChunkSize(importChunkSize);
      importer.resetWithInputStream(inStream);
        */
      int chunkCount = 0;
      int rowCount = 0;

      BestGuessDataTyper guesser;
      Data data;

      data = importer.importInputStream(inStream);

      Data mqData = data;

      System.out.println("Imported: " + rowCount + " rows of data in: " + chunkCount + " chunk(s)");

      MqFileSplitterData mqFileSplitter = new MqFileSplitterData(mqData);
      HashMap<String, Data> outputs = mqFileSplitter.parse();

      System.out.println("outputs Row size = " + outputs.get("ones").getLargestRows());


    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      if (inStream != null)
      {
        try {inStream.close();} catch (Exception ignored) {}
      }
    }
  }


}

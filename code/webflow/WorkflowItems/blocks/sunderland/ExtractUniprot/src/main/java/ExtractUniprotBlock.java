/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.Data;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;

public class ExtractUniprotBlock extends CloudDataProcessorService
{

  public void execute() throws Exception
  {
    //Import the Max Quant File
    Data inputData =  getInputDataSet("mqData");

    //Split into three files depending on the number of values in the Mix cols
    ExtractUniprot extractor = new ExtractUniprot(inputData);
    try
    {
      //hashman contains ones, twos, threes as keys
      Data outputData = extractor.extract();
      setOutputDataSet("UniprotIDs", outputData);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


}
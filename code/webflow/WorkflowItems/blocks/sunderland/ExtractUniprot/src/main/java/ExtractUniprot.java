/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.DelimitedTextDataImporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: nsjw7
 * Date: 08/08/2012
 * Time: 08:40
 * Parse Jill's CSV file and split into three files based on number of occurences of data in the last 3 columns.
 * Output each protein split on semi-colon into different files.
 */
public class ExtractUniprot
{

  private Data inputData;

  public ExtractUniprot(Data inputData)
  {
    this.inputData = inputData;
  }

  public Data extract() throws Exception
  {
    try
    {
      Data outputUniprots = new Data();
      Column uniProtCol = new StringColumn("Uniprot");

      int rows = inputData.getLargestRows();
      for (int i = 0; i < rows; i++)
      {
        //Get the Mix values
        Data thisRow = inputData.getRowSubset(i, i, true);
        String allUniprots = thisRow.column("Uniprot").getStringValue(0);

        String[] uniprots = allUniprots.split(";");
        for(String uniprot : uniprots)
        {
          uniProtCol.appendStringValue(uniprot);
        }
      }

      outputUniprots.addColumn(uniProtCol);
      return outputUniprots;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }

  }

  //Test Method
  public static void main(String[] args)
  {
    InputStream inStream = null;

    try
    {

      File input = new File("/Users/nsjw7/Desktop/Fibro_SILAC_MaxQuant_02.csv");
      DelimitedTextDataImporter importer = new DelimitedTextDataImporter();

      // Set the properties
      int startRow = 2;
      int labelRow = 1;
      boolean containsLabels = true;
      String columnDelimiter = ",";
      int endRow = 1;
      boolean limitRows = false;
      boolean subsample = false;
      int sampleInterval = 1;

      importer.setDataStartRow(startRow);
      importer.setDataEndRow(endRow);
      importer.setDelimiterString(columnDelimiter);
      importer.setDelimiterType(DelimitedTextDataImporter.CUSTOM_DELIMITED);
      importer.setErrorsAsMissing(true);
      importer.setImportColumnNames(containsLabels);
      importer.setLimitRows(limitRows);
      importer.setNameRow(labelRow);
      importer.setSampleInterval(sampleInterval);
      importer.setSubsample(subsample);
      importer.setUseEnclosingQuotes(true);
      inStream = new FileInputStream(input);
      importer.setForceTextImport(true);


      Data mqData = importer.importInputStream(inStream);
      ExtractUniprot exgtractor= new ExtractUniprot(mqData);
      Data outputData = exgtractor.extract();

      System.out.println("outputs Row size = " + outputData.getLargestRows());

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      if (inStream != null)
      {
        try {inStream.close();} catch (Exception ignored) {}
      }
    }
  }


}

/**
 * e-Science Central Copyright (C) 2008-2016 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameter;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.notification.WorkflowLock;

import com.connexience.server.workflow.*;
import com.connexience.server.workflow.api.*;

import org.pipeline.core.data.*;
import java.io.File;

import org.pipeline.core.data.columns.StringColumn;
import java.util.*;


public class WorkflowPerFile implements WorkflowBlock
{
    private static final String Prop_WORKFLOW_FILE = "WorkflowFile";

    private static final String Prop_TARGET_BLOCK_NAME = "TargetBlockName";
    private static final String Prop_TARGET_PROPERTY_NAME = "TargetPropertyName";

    private static final String Prop_FAIL_ON_EMPTY = "FailOnEmpty";

    private static final String Prop_WAIT_FOR_WORKFLOW = "WaitForWorkflow";
    private static final String Prop_PAUSE_FOR_FAILED_WORKFLOWS = "PauseForFailedWorkflows";
    private static final String Prop_ALLOW_FAILED_WORKFLOWS = "AllowFailedWorkflows";

    private static final String Prop_TRANSFER_FOLDER_NAME = "TransferFolderName";

    private static final String Prop_DATA_STRUCTURE = "DataStructure";

    private static final String DS_ONE_COLUMN = "ONE_COLUMN_MODE";
    private static final String DS_TWO_COLUMN = "TWO_COLUMN_MODE";
    private static final String DS_THREE_COLUMN = "THREE_COLUMN_MODE";

    private static final String Input_PROPERTY_DATA = "property-data";
    private static final String Input_INPUT_FILES = "input-files";
    private static final String Input_INPUT_FILE_REFS = "input-file-refs";

    private static final String Output_INVOCATION_IDS = "invocation-ids";


    @Override
    public void preExecute(BlockEnvironment env) throws Exception
    {
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception
    {
        // Sanity check for some properties and inputs
        String targetBlockName = env.getStringProperty(Prop_TARGET_BLOCK_NAME, "").trim();
        if ("".equals(targetBlockName)) {
            throw new Exception("Property " + Prop_TARGET_BLOCK_NAME + " has not been set.");
        }

        String targetPropertyName = env.getStringProperty(Prop_TARGET_PROPERTY_NAME, "").trim();
        if ("".equals(targetPropertyName)) {
            throw new Exception("Property " + Prop_TARGET_PROPERTY_NAME + " has not been set.");
        }

        // Get the input files if the port is connected
        List<File> files = env.getExecutionService().isInputConnected(Input_INPUT_FILES) ?
                files = inputs.getInputFiles(Input_INPUT_FILES) :
                new ArrayList<File>();

        // Get the input file references if the port is connected
        List<ServerObject> fileRefs = env.getExecutionService().isInputConnected(Input_INPUT_FILE_REFS) ?
                inputs.getReferencesInput(Input_INPUT_FILE_REFS) :
                new ArrayList<ServerObject>();

        int dataLen = files.size() + fileRefs.size();

        if (dataLen == 0 && env.getBooleanProperty(Prop_FAIL_ON_EMPTY, false)) {
            throw new Exception("No input files provided while property " + Prop_FAIL_ON_EMPTY + " was set.");
        }

        String dataStructure = null;
        Data propertyData = null;
        boolean onePropertyRowPerFile = true;

        if (env.getExecutionService().isInputConnected(Input_PROPERTY_DATA)) {
            propertyData = inputs.getInputDataSet(Input_PROPERTY_DATA);
            if (propertyData != null) {
                int propRowNo = propertyData.getLargestRows();
                if (propRowNo == 1) {
                    // This is to handle a common case when a single row of property data can be reused across all the input files
                    onePropertyRowPerFile = false;
                } else if (propRowNo != dataLen) {
                    throw new Exception(
                            String.format("Inconsistent number of input files (%d + %d) and rows in the property data (%d).", files.size(), fileRefs.size(), propertyData.getLargestRows()));
                }

                dataStructure = env.getStringProperty(Prop_DATA_STRUCTURE, "").trim();
                if ("".equals(dataStructure)) {
                    throw new Exception("Property " + Prop_DATA_STRUCTURE + " has not been set.");
                }
            }
        }

        APIBroker api = env.getExecutionService().createApiLink();
        ArrayList<DocumentRecord> transferDocuments = null;
        try {
            transferDocuments = _doDataTransfer(env, files, api);
        } finally {
            api.release();
        }

        WorkflowDocument wfDocument = api.getWorkflow(env.getDocumentProperty(Prop_WORKFLOW_FILE).getId());
        if (wfDocument == null) {
            throw new IllegalArgumentException("Invalid property: " + Prop_WORKFLOW_FILE + ": Cannot access workflow document to execute.");
        }

        Data invocationIds = new Data();
        invocationIds.addColumn(new StringColumn("InvocationID"));
        invocationIds.addColumn(new StringColumn("InputFileName"));

        WorkflowInvocationFolder invocation;
        WorkflowParameterList params;

        WorkflowLock lock = null;
        if (env.getBooleanProperty(Prop_WAIT_FOR_WORKFLOW, false)) {
            lock = env
                    .getExecutionService()
                    .createWorkflowLock(
                            env.getBooleanProperty(Prop_ALLOW_FAILED_WORKFLOWS, false),
                            env.getBooleanProperty(Prop_PAUSE_FOR_FAILED_WORKFLOWS, false));
        }

        int propertyRow = 0;
        for (DocumentRecord doc : transferDocuments) {
            params = _getWorkflowParameters(propertyData, propertyRow, dataStructure);
            if (onePropertyRowPerFile) {
                propertyRow++;
            }
            params.add(targetBlockName, targetPropertyName, doc.getId());

            if (lock != null) {
                invocation = env
                        .getExecutionService()
                        .executeWorkflowWithLock(wfDocument, params, lock, wfDocument.getName() + ": " + doc.getName());
            } else {
                invocation = env
                        .getExecutionService()
                        .executeWorkflowWithLock(wfDocument, params, lock, wfDocument.getName() + ": " + doc.getName());
            }

            invocationIds.column(0).appendStringValue(invocation.getInvocationId());
            invocationIds.column(1).appendStringValue(doc.getName());
        }

        for (ServerObject ref : fileRefs) {
            params = _getWorkflowParameters(propertyData, propertyRow, dataStructure);
            if (onePropertyRowPerFile) {
                propertyRow++;
            }
            params.add(targetBlockName, targetPropertyName, ref.getId());

            if (lock != null) {
                invocation = env
                        .getExecutionService()
                        .executeWorkflowWithLock(wfDocument, params, lock, wfDocument.getName() + ": " + ref.getName());
            } else {
                invocation = env
                        .getExecutionService()
                        .executeWorkflowWithLock(wfDocument, params, lock, wfDocument.getName() + ": " + ref.getName());
            }

            invocationIds.column(0).appendStringValue(invocation.getInvocationId());
            invocationIds.column(1).appendStringValue(ref.getName());
        }

        outputs.setOutputDataSet(Output_INVOCATION_IDS, invocationIds);
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {

    }


    private ArrayList<DocumentRecord> _doDataTransfer(BlockEnvironment env, List<File> inputFiles, APIBroker api) throws Exception {
        ArrayList<DocumentRecord> transferDocuments = new ArrayList<>();

        if (inputFiles.size() == 0) {
            return transferDocuments;
        }

        String transferFolderName = env.getStringProperty(Prop_TRANSFER_FOLDER_NAME, "transfer");

        // Create a transfer folder for copying data
        Folder transferFolder = new Folder();
        transferFolder.setName(transferFolderName);
        transferFolder.setContainerId(env.getExecutionService().getInvocationFolder().getId());
        transferFolder = api.saveFolder(transferFolder);

        // Upload all of the source files
        HashMap<String, Folder> subFolderMap = new HashMap<>();
        for (File file : inputFiles) {
            if (!file.exists()) {
                System.err.println("Cannot locate input file: " + file);
                System.err.println("\tcurrent dir: " + System.getProperty("user.dir"));
                continue;
            }

            Folder parentFolder = transferFolder;
            int i = 0;
            int j;
            String filePath = file.getPath();
            String wd = env.getWorkingDirectory().getPath();
            if (!filePath.startsWith(wd)) {
                // Potential fix might be to use .getAbsolutePath in the lines above
                throw new Exception("Internal error: file " + filePath + " is not located in the working directory: " + wd);
            }
            filePath = filePath.substring(wd.length());

            while ((j = filePath.indexOf(File.separator, i)) > -1) {
                Folder subFolder;
                if (j > 0) {
                    subFolder = subFolderMap.get(filePath.substring(0, j));
                    if (subFolder == null) {
                        subFolder = new Folder();
                        subFolder.setName(filePath.substring(i, j));
                        subFolder.setContainerId(parentFolder.getId());
                        subFolder = api.saveFolder(subFolder);
                        subFolderMap.put(filePath.substring(0, j), subFolder);
                    }
                    parentFolder = subFolder;
                }
                i = j + 1;
            }

            DocumentRecord uploadDoc = new DocumentRecord();
            uploadDoc.setName(filePath.substring(i, filePath.length()));
            uploadDoc = api.saveDocument(parentFolder, uploadDoc);
            api.uploadFile(uploadDoc, file);
            transferDocuments.add(uploadDoc);
        }

        return transferDocuments;
    }


    private WorkflowParameterList _getWorkflowParameters(Data propertyData, int rowNo, String dataStructure)
    {
        WorkflowParameterList params = new WorkflowParameterList();

        if (propertyData == null) {
            return params;
        }

        switch (dataStructure) {
            case DS_ONE_COLUMN:
                for (int c = 0; c < propertyData.getColumns(); c++) {
                    if (propertyData.column(c).isMissing(rowNo)) {
                        // Stop reading row rowNo if a Missing value has been detected.
                        break;
                    }
                    params.add(_parse_1ColData(
                            propertyData.column(c).getStringValue(rowNo)));
                }
                break;
            case DS_TWO_COLUMN:
                if (propertyData.getColumns() % 2 != 0) {
                    throw new IllegalArgumentException(
                            String.format("Invalid number of columns: %d. Mode %s requires an even number", propertyData.getColumns(), DS_TWO_COLUMN));
                }

                for (int c = 0; c < propertyData.getColumns(); c += 2) {
                    if (propertyData.column(c).isMissing(rowNo)) {
                        // Stop reading row rowNo if a Missing value has been detected.
                        break;
                    }
                    if (propertyData.column(c + 1).isMissing(rowNo)) {
                        throw new IllegalArgumentException(
                                String.format("Invalid property data: missing property value for %s in row: %d, column: %d",
                                        propertyData.column(c).getStringValue(rowNo), rowNo, c));
                    }
                    params.add(_parse_2ColData(
                            propertyData.column(c).getStringValue(rowNo),
                            propertyData.column(c + 1).getStringValue(rowNo)));
                }
                break;
            case DS_THREE_COLUMN:
                if (propertyData.getColumns() % 3 != 0) {
                    throw new IllegalArgumentException(
                            String.format("Invalid number of columns: %d. Mode %s requires a number divisible by 3", propertyData.getColumns(), DS_THREE_COLUMN));
                }

                for (int c = 0; c < propertyData.getColumns(); c += 3) {
                    if (propertyData.column(c).isMissing(rowNo)) {
                        // Stop reading the row if the block name column has missing value in that row
                        break;
                    }
                    if (propertyData.column(c + 1).isMissing(rowNo) ||
                            propertyData.column(c + 2).isMissing(rowNo)) {
                        throw new IllegalArgumentException("Invalid property data: missing value in row: %d, column: %d");
                    }
                    params.add(new WorkflowParameter(
                            propertyData.column(c).getStringValue(rowNo),
                            propertyData.column(c + 1).getStringValue(rowNo),
                            propertyData.column(c + 2).getStringValue(rowNo)));
                }
                break;
            default:
                throw new IllegalArgumentException("Invalid value for property " + Prop_DATA_STRUCTURE);
        }

        return params;
    }


    private WorkflowParameter _parse_2ColData(String blockProp, String value)
    {
        int dot = blockProp.indexOf('.');
        if (dot <= 0 || dot >= blockProp.length() - 1) {
            throw new IllegalArgumentException("Invalid value of the block-property column: " + blockProp + "; Expected: BlockName.PropertyName");
        }

        return new WorkflowParameter(
                blockProp.substring(0, dot),
                blockProp.substring(dot + 1),
                value);
    }


    private WorkflowParameter _parse_1ColData(String blockPropValue)
    {
        int dot = blockPropValue.indexOf('.');
        int eq = blockPropValue.indexOf('=');
        if (dot <= 0 || eq <= dot + 1) {
            throw new IllegalArgumentException("Invalid value of the block-property-value column: " + blockPropValue + "; Expected: BlockName.PropertyName=Value");
        }

        return new WorkflowParameter(
                blockPropValue.substring(0, dot),
                blockPropValue.substring(dot + 1, eq),
                blockPropValue.substring(eq + 1));
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import com.connexience.server.workflow.util.ZipUtils;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;

import java.io.File;
import java.util.List;

public class CollectWorkflowResults extends CloudDataProcessorService
{
    private static final String Prop_WILDCARD = "Wildcard";
    private static final String Prop_DOWNLOAD_FILES = "Download files";
    private static final String Prop_RESOLVE_CONFLICTS = "Resolve name conflicts";
    private static final String Opt_ADD_PREFIX = "ADD PREFIX";
    private static final String Opt_ADD_SUFFIX = "ADD SUFFIX";
    private static final String Opt_ADD_NAME_SUFFIX = "ADD NAME SUFFIX";
    private static final String Opt_OVERWRITE = "OVERWRITE";
    private static final String Opt_DISCARD = "DISCARD";

    private static final String Input_INVOCATION_IDS = "invocation-ids";

    private static final String Output_FILES = "files";
    private static final String Output_FILE_REFS = "file-references";


    @Override
    public void executionAboutToStart() throws Exception
    {
        // This is to avoid block failing after new output port 'file-references' has been added.
        getEditableProperties().add("ErrorsForNonExistentPorts", false);
    };


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    @Override
    public void execute() throws Exception
    {
        APIBroker api = createApiLink();
        File workingDir = getWorkingDirectory();
        FileWrapper fileWrapper = new FileWrapper(workingDir);
        LinkWrapper linkWrapper = new LinkWrapper();
        String wildcard = getProperties().stringValue(Prop_WILDCARD, "*.*");
        boolean download = getProperties().booleanValue(Prop_DOWNLOAD_FILES, false);

        String conflictResolutionStr = getProperties().stringValue(Prop_RESOLVE_CONFLICTS, "");
        CreateOption cResolution;
        boolean overwrite = false;
        switch (conflictResolutionStr) {
        case Opt_ADD_PREFIX: cResolution = CreateOption.AddPrefix; break;
        case Opt_ADD_NAME_SUFFIX: cResolution = CreateOption.AddNameSuffix; break;
        case Opt_ADD_SUFFIX: cResolution = CreateOption.AddSuffix; break;
        case Opt_OVERWRITE:
            overwrite = true;
            // no break is intentional
        case Opt_DISCARD:
            cResolution = null; 
            break;
        default:
            // Keep it compatible with the old version
            cResolution = CreateOption.AddNameSuffix;
            break;
        }

        Data invocationIds = getInputDataSet(Input_INVOCATION_IDS);
        Column ids = invocationIds.column(0);

        for (int i = 0; i < ids.getRows(); i++) {
            String invocationId = ids.getStringValue(i);
            WorkflowInvocationFolder folder = api.getWorkflowInvocation(invocationId);

            if (folder == null) {
                throw new Exception("Invalid invocation id: " + invocationId);
            }

            List<DocumentRecord> docs = api.getFolderDocuments(folder);
            for (DocumentRecord doc : docs) {
                if (WildcardUtils.wildCardMatch(doc.getName(), wildcard)) {
                    // Add document reference...
                    linkWrapper.addDocument(doc, api.getLatestVersion(doc.getId()));

                    // and download file if needed
                    if (download) {
                        if (cResolution == null) {
                            File file = new File(workingDir, ZipUtils.escapeFileName(doc.getName()));
                            if (file.exists()) {
                                if (overwrite) {
                                    api.downloadToFile(doc, file);
                                    boolean notFound = true; 
                                    for (File f : fileWrapper) {
                                        if (f.equals(file)) {
                                            notFound = false;
                                            break;
                                        }
                                    }
                                    if (notFound) {
                                        fileWrapper.addFile(file, false);
                                    }
                                } else {
                                    continue; // discard file
                                }
                            } else {
                                api.downloadToFile(doc, file);
                                fileWrapper.addFile(file, false);
                            }
                        } else {
                            File file = createTempFile(ZipUtils.escapeFileName(doc.getName()), workingDir, cResolution);
                            api.downloadToFile(doc, file);
                            fileWrapper.addFile(file, false);
                        }
                    }
                }
            }
            
            // Remove the invocation if needed
            if(getEditableProperties().booleanValue("RemoveInvocations", false)){
                api.deleteFolder(folder.getId());
            }
        }

        setOutputData(Output_FILES, fileWrapper);
        setOutputData(Output_FILE_REFS, linkWrapper);
    }
}
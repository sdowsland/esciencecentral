/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.services.workflow;

import java.util.ArrayList;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;

import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;


public class WaitForWorkflows extends CloudDataProcessorService
{
    private final static String Prop_PAUSE_FAILED = "PauseForFailedWorkflows";
    private final static String Prop_ALLOW_FAILED = "AllowFailedWorkflows";
    
    private final static String Input_INVOCATION_IDS = "invocation-ids";
    private final static String Output_INVOCATION_IDS = "invocation-ids";


    @Override
    public void execute() throws Exception
    {
        APIBroker api = createApiLink();

        // First get workflow invocation folders referred by the input column
        // This will also check whether the input refers to correct workflow invocations
        Data invocationIds = getInputDataSet(Input_INVOCATION_IDS);
        Column ids = invocationIds.column(0);
        ArrayList<WorkflowInvocationFolder> invocationFolders = new ArrayList<>();
        for (int i = 0; i < ids.getRows(); i++) {
            String invocationId = ids.getStringValue(i);
            invocationFolders.add(api.getWorkflowInvocation(invocationId));
        }

        // Having invocation folders create one big lock that waits on all of them
        WorkflowLock lock = createWorkflowLock(getProperties().booleanValue(Prop_ALLOW_FAILED, false), getProperties().booleanValue(Prop_PAUSE_FAILED, false));
        for (WorkflowInvocationFolder f : invocationFolders) {
            attachInvocationToLock(f, lock);
        }

        // Copy input to the output, so that the block can be placed in the middle between 
        // an async invocation and collect results
        setOutputDataSet(Output_INVOCATION_IDS, invocationIds);
    }
}
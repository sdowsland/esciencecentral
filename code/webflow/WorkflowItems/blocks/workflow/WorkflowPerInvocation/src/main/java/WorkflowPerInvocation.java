/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameter;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;

import org.pipeline.core.data.*;

import org.pipeline.core.data.columns.StringColumn;


public class WorkflowPerInvocation extends CloudDataProcessorService
{
    private static final String Prop_WORKFLOW_FILE = "WorkflowFile";
    private static final String Prop_TRANSFER_FILES = "TransferFiles";
    private static final String Prop_TARGET_BLOCK_NAME = "TargetBlockName";
    private static final String Prop_TARGET_PROPERTY_NAME = "TargetPropertyName";
    private static final String Prop_DATA_STRUCTURE = "DataStructure";

    private static final String DS_ONE_COLUMN = "ONE_COLUMN_MODE";
    private static final String DS_TWO_COLUMN = "TWO_COLUMN_MODE";
    private static final String DS_THREE_COLUMN = "THREE_COLUMN_MODE";

    private static final String Prop_WAIT_FOR_WORKFLOW = "WaitForWorkflow";
    private static final String Prop_PAUSE_FOR_FAILED_WORKFLOWS = "PauseForFailedWorkflows";
    private static final String Prop_ALLOW_FAILED_WORKFLOWS = "AllowFailedWorkflows";
    private static final String Prop_FAIL_ON_EMPTY = "FailOnEmpty";

    private static final String Input_PROPERTY_DATA = "property-data";
    private static final String Input_INVOCATION_IDS = "invocation-ids";
    
    private static final String Output_INVOCATION_IDS = "invocation-ids";

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        Data inputIds = getInputDataSet(Input_INVOCATION_IDS);
        if (inputIds.getColumns() != 1) {
            throw new IllegalArgumentException("Wrong number of columns in the invocation-ids input. Exactly one column expected.");
        }
        Column inputIdsCol = inputIds.column(0);
        int inputLen = inputIdsCol.getRows();
        if (inputLen == 0) {
            if (getProperties().booleanValue(Prop_FAIL_ON_EMPTY, false)) {
                throw new Exception("No invocations provided while property " + Prop_FAIL_ON_EMPTY + " was set.");
            }
        }

        APIBroker api = createApiLink();
        DocumentRecord wfDoc = (DocumentRecord) getEditableProperties().xmlStorableValue(Prop_WORKFLOW_FILE);
        WorkflowDocument wf = api.getWorkflow(wfDoc.getId());

        String targetBlockName = null;
        String targetPropertyName = null;

        if (getProperties().booleanValue(Prop_TRANSFER_FILES, false)) {
            targetBlockName = getProperties().stringValue(Prop_TARGET_BLOCK_NAME, "").trim();
            if ("".equals(targetBlockName)) {
                throw new Exception("Property " + Prop_TARGET_BLOCK_NAME + " has not been set.");
            }

            targetPropertyName = getProperties().stringValue(Prop_TARGET_PROPERTY_NAME, "").trim();
            if ("".equals(targetPropertyName)) {
                throw new Exception("Property " + Prop_TARGET_PROPERTY_NAME + " has not been set.");
            }
        }

        int dataStructNo = 0;
        Data propertyData = null;
        if (isInputConnected(Input_PROPERTY_DATA)) {
            propertyData = getInputDataSet(Input_PROPERTY_DATA);
            if (propertyData.getLargestRows() > 0) {
                if (propertyData.getLargestRows() != inputLen) {
                    throw new Exception(
                            String.format("Inconsistent number of invocation ids (%d) and rows in the property data (%d).", inputLen, propertyData.getLargestRows()));
                }

                String dataStructure = getProperties().stringValue(Prop_DATA_STRUCTURE, "").trim();
                if ("".equals(dataStructure)) {
                    throw new Exception("Property " + Prop_DATA_STRUCTURE + " has not been set.");
                }

                if (DS_ONE_COLUMN.equals(dataStructure)) {
                    dataStructNo = 1;
                } else if (DS_TWO_COLUMN.equals(dataStructure)) {
                    dataStructNo = 2;
                } else if (DS_THREE_COLUMN.equals(dataStructure)) {
                    dataStructNo = 3;
                } else {
                    throw new Exception("Invalid value for property " + Prop_DATA_STRUCTURE);
                }
            }
        }

        WorkflowInvocationFolder invocation;
        WorkflowLock lock = null;

        if (getProperties().booleanValue(Prop_WAIT_FOR_WORKFLOW, false)) {
            lock = createWorkflowLock(
                    getProperties().booleanValue(Prop_ALLOW_FAILED_WORKFLOWS, false), 
                    getProperties().booleanValue(Prop_PAUSE_FOR_FAILED_WORKFLOWS, false));
        }

        StringColumn outputIdsCol = new StringColumn("InvocationID");

        for (int i = 0; i < inputLen; i++) {
            WorkflowParameterList params;
            // Prepare parameters if needed.
            if (dataStructNo > 0) {
                params = _getWorkflowParameters(propertyData, i, dataStructNo);
            } else {
                params = new WorkflowParameterList();
            }

            // Add transfer information if needed.
            if (targetBlockName != null) {
                params.add(targetBlockName, targetPropertyName, inputIdsCol.getStringValue(i));
            }

            // Run workflow with or without lock.
            if (lock == null) {
                invocation = executeWorkflow(wf, params, wf.getName() + ": "+ i);
            } else {
                invocation = executeWorkflowWithLock(wf, params, lock,wf.getName() + ": " + i);
            }

            outputIdsCol.appendStringValue(invocation.getInvocationId());
        }

        Data output = new Data();
        output.addColumn(outputIdsCol);
        setOutputDataSet(Output_INVOCATION_IDS, output);
    }
    
    private WorkflowParameterList _getWorkflowParameters(Data propertyData, int rowNo, int dataStructure)
    {
        WorkflowParameterList params = new WorkflowParameterList();
        
        if (dataStructure == 1) {
            for (int c = 0; c < propertyData.getColumns(); c++) {
                if (propertyData.column(c).isMissing(rowNo)) {
                    // Stop reading row rowNo if a Missing value has been detected.
                    break;
                }
                params.add(_parse_1ColData(
                        propertyData.column(c).getStringValue(rowNo)));
            }
        } else if (dataStructure == 2) {
            if (propertyData.getColumns() % 2 != 0) {
                throw new IllegalArgumentException(
                        String.format("Invalid number of columns: %d. Mode %s requires an even number", propertyData.getColumns(), DS_TWO_COLUMN));
            }

            for (int c = 0; c < propertyData.getColumns(); c += 2) {
                if (propertyData.column(c).isMissing(rowNo)) {
                    // Stop reading row rowNo if a Missing value has been detected.
                    break;
                }
                if (propertyData.column(c + 1).isMissing(rowNo)) {
                    throw new IllegalArgumentException(
                            String.format("Invalid property data: missing property value for %s in row: %d, column: %d", 
                                    propertyData.column(c).getStringValue(rowNo), rowNo, c));
                }
                params.add(_parse_2ColData(
                        propertyData.column(c).getStringValue(rowNo), 
                        propertyData.column(c + 1).getStringValue(rowNo)));
            }
        } else if (dataStructure == 3) {
            if (propertyData.getColumns() % 3 != 0) {
                throw new IllegalArgumentException(
                        String.format("Invalid number of columns: %d. Mode %s requires a number divisible by 3", propertyData.getColumns(), DS_THREE_COLUMN));
            }

            for (int c = 0; c < propertyData.getColumns(); c += 3) {
                if (propertyData.column(c).isMissing(rowNo)) {
                    // Stop reading the row if the block name column has missing value in that row
                    break;
                }
                if (propertyData.column(c + 1).isMissing(rowNo) ||
                    propertyData.column(c + 2).isMissing(rowNo)) {
                    throw new IllegalArgumentException("Invalid property data: missing value in row: %d, column: %d");
                }
                params.add(new WorkflowParameter(
                        propertyData.column(c).getStringValue(rowNo),
                        propertyData.column(c + 1).getStringValue(rowNo),
                        propertyData.column(c + 2).getStringValue(rowNo)));
            }
        }

        return params;
    }
    
    private WorkflowParameter _parse_2ColData(String blockProp, String value)
    {
        int dot = blockProp.indexOf('.');
        if (dot <= 0 || dot >= blockProp.length() - 1) {
            throw new IllegalArgumentException("Invalid value of the block-property column: " + blockProp + "; Expected: BlockName.PropertyName");
        }

        return new WorkflowParameter(
                blockProp.substring(0, dot),
                blockProp.substring(dot + 1),
                value);
    }

    private WorkflowParameter _parse_1ColData(String blockPropValue)
    {
        int dot = blockPropValue.indexOf('.');
        int eq = blockPropValue.indexOf('=');
        if (dot <= 0 || eq <= dot + 1) {
            throw new IllegalArgumentException("Invalid value of the block-property-value column: " + blockPropValue + "; Expected: BlockName.PropertyName=Value");
        }

        return new WorkflowParameter(
                blockPropValue.substring(0, dot),
                blockPropValue.substring(dot + 1, eq),
                blockPropValue.substring(eq + 1));
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RunWorkflows extends CloudDataProcessorService
{
    List<WorkflowDocument> workflows;
    APIBroker api;

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
        api = createApiLink();
        workflows = api.listWorkflows();
    }


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        boolean transferData = getEditableProperties().booleanValue("TransferData", true);
        boolean waitForWorkflow = getEditableProperties().booleanValue("WaitForWorkflow", false);
        String targetBlockName = getEditableProperties().stringValue("TargetBlockName", "input");
        String targetPropertyName = getEditableProperties().stringValue("TargetPropertyName", "Source");
        boolean pauseForFailedWorkflows = getEditableProperties().booleanValue("PauseForFailedWorkflows", false);
        boolean allowFailedSubworkflows = getEditableProperties().booleanValue("AllowFailedWorkflows", false);
        
        String wfName;
        WorkflowDocument wf;
        ArrayList<WorkflowDocument> workflowsToRun = new ArrayList<WorkflowDocument>();
        Column wfNames = getInputDataSet("workflow-list").column(0);

        // Get a list of all of the workflows to run
        for (int i = 0; i < wfNames.getRows(); i++) {
            if (!wfNames.isMissing(i)) {
                wfName = wfNames.getStringValue(i);
                wf = findWorkflow(wfName);
                if (wf != null) {
                    workflowsToRun.add(wf);
                } else {
                    throw new Exception("Cannot find workflow: " + wfName);
                }
            }
        }

        // Build the common parameter set
        WorkflowParameterList params = new WorkflowParameterList();
        
        // Transfer data back to the server if required
        if (transferData) {
            String transferFolderId = doDataTransfer(api);
            // Set the target parameter value
            params.add(targetBlockName, targetPropertyName, transferFolderId);
        }        
        
        // Execute the individual workflows
        WorkflowInvocationFolder invocation;
        StringColumn idColumn = new StringColumn("InvocationID");

        // Call in correct way depending on lock requirements
        if (waitForWorkflow) {
            WorkflowLock lock = createWorkflowLock(allowFailedSubworkflows, pauseForFailedWorkflows);
            for (WorkflowDocument workflowToRun : workflowsToRun) {
                invocation = executeWorkflowWithLock(workflowToRun, params, lock, workflowToRun.getName() + " run #0");
                idColumn.appendStringValue(invocation.getInvocationId());
            }
        } else {
            for (WorkflowDocument workflowToRun : workflowsToRun) {
                invocation = executeWorkflow(workflowToRun, params, workflowToRun.getName() + " run #0");
                idColumn.appendStringValue(invocation.getInvocationId());
            }
        }

        Data invocationIds = new Data();
        invocationIds.addColumn(idColumn);
        setOutputDataSet("invocation-ids", invocationIds);
    }


    /** Find a workflow by name */
    private WorkflowDocument findWorkflow(String name)
    {
        for (int i = 0; i < workflows.size(); i++) {
            if (workflows.get(i).getName().equals(name)) {
                return workflows.get(i);
            }
        }

        return null;
    }


    private String doDataTransfer(APIBroker api) throws Exception
    {
        String transferFolderName = getEditableProperties().stringValue("TransferFolderName", "transfer");
        FileWrapper inputFiles = (FileWrapper)getInputData("input-files");

        // Create a transfer folder for copying data
        Folder transferFolder = new Folder();
        transferFolder.setName(transferFolderName);
        transferFolder.setContainerId(getInvocationFolder().getId());
        transferFolder = api.saveFolder(transferFolder);

        // Upload all of the source files
        HashMap<String, Folder> subFolderMap = new HashMap<String, Folder>();
        File baseDir = inputFiles.getBaseDir();
        if (!baseDir.isAbsolute()) {
            baseDir = new File(getWorkingDirectory(), baseDir.getPath());
        }

        for (String fileName : inputFiles.relativeFilePaths()) {
            File file = new File(baseDir, fileName);
            if (!file.exists()) {
                System.err.println("Cannot locate input file: " + fileName);
                System.err.println("\tbase dir: " + baseDir);
                System.err.println("\tcurrent dir: " + System.getProperty("user.dir"));
                continue;
            }

            Folder parentFolder = transferFolder;
            int i = 0;
            int j;
            while ((j = fileName.indexOf(File.separator, i)) > -1) {
                Folder subFolder;
                if (j > 0) {
                    subFolder = subFolderMap.get(fileName.substring(0, j));
                    if (subFolder == null) {
                        subFolder = new Folder();
                        subFolder.setName(fileName.substring(i, j));
                        subFolder.setContainerId(parentFolder.getId());
                        subFolder = api.saveFolder(subFolder);
                        subFolderMap.put(fileName.substring(0, j), subFolder);
                    }
                    parentFolder = subFolder;
                }
                i = j + 1;
            }

            DocumentRecord uploadDoc = new DocumentRecord();
            uploadDoc.setName(fileName.substring(i, fileName.length()));
            uploadDoc = api.saveDocument(parentFolder, uploadDoc);
            api.uploadFile(uploadDoc, file);
        }

        return transferFolder.getId();
    }
}
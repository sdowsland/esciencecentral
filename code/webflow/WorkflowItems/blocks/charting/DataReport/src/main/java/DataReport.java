
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.cloud.services.support.GnuPlotEngine;
import com.connexience.server.workflow.util.ZipUtils;
import com.connexience.tools.html2pdf.Html2PdfConverter;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import javax.imageio.ImageIO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.CSVDataExporter;
import org.pipeline.core.data.manipulation.ColumnSorter;
import org.pipeline.core.data.manipulation.NumericalColumnExtractor;
import org.pipeline.core.data.maths.CorrelationCalculator;
import org.pipeline.core.data.maths.MaxValueCalculator;
import org.pipeline.core.data.maths.MinValueCalculator;
import org.pipeline.core.data.maths.StdCalculator;
import org.pipeline.core.data.maths.UnitVarianceColumnScaler;
import weka.classifiers.functions.LinearRegression;
import weka.core.Instances;
import weka.core.SelectedTag;

public class DataReport implements WorkflowBlock {
    private File tmpDir;
    private int plotCount = 0;
    private DecimalFormat df = new DecimalFormat("#.#####");
    private DecimalFormat shortDf = new DecimalFormat("#.##");
    private boolean firstPage = true;
    
    @Override
    public void preExecute(BlockEnvironment env) throws Exception {
        // Create a temporary directory to hold the .html files
        // Create a temporary directory to store the report html file in
        tmpDir = new File(env.getWorkingDirectory(), env.getBlockContextId() + "-html");
        tmpDir.mkdir();

        // Copy the stylesheet
        ZipUtils.copyFile(env.getExecutionService().getLibraryItem().getFile("report.css"), new File(tmpDir, "report.css"));        
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        env.getExecutionService().populateInputDataSetMap();
        Data inputData = inputs.getInputDataSet("input-data");
        
        plotCount = 0;
                
        File report = new File(tmpDir, "report.html");
        PrintWriter writer = new PrintWriter(report);
        
        // Top of page
        writer.println("<html>");
        writer.println("<link rel=\"stylesheet\" href=\"report.css\"/>");
        writer.println("<head></head><body>");
        
        // Metadata collection if there is any
        generateMetadataTable(env, writer);
                
        // Work through columns
        Column c;
        for(int i=0;i<inputData.getColumnCount();i++){
            c = inputData.column(i);
            if(c instanceof NumericalColumn){
                addNumericalColumnPage(env, inputData, i, writer);
                if(env.getBooleanProperty("IncludeRegression", true)){
                    // Add a regression page if selected
                    addRegressionPage(env, inputData, i, writer);
                }
            } else if(c instanceof StringColumn){
                addTextColumnPage(env, inputData, i, writer);
            }
        }
        
        // Finish off
        writer.flush();
        writer.close();
        
        // Convert to pdf
        File reportPdf = new File(env.getWorkingDirectory(), env.getStringProperty("ReportName", "report.pdf"));
        Html2PdfConverter converter = new Html2PdfConverter(report, reportPdf);
        converter.convert();
        outputs.setOutputFileWithFullPath("report", reportPdf);        
    }

    /** Add any metadata to the report */
    private void generateMetadataTable(BlockEnvironment env, PrintWriter writer) throws Exception {
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("input-data");
        if (mdc != null && mdc.size() > 0) {
            writer.println("<h2>Metadata</h2>");
            writer.println("<table width=\"100%;\"><thead><tr><th>Category</th><th>Name</th><th>Value</th></tr></thead><tbody>");
            MetadataItem md;
            for (int i = 0; i < mdc.size(); i++) {
                md = mdc.get(i);
                writer.println("<tr>");
                writer.println("<td>" + md.getCategory() + "</td>");
                writer.println("<td>" + md.getName() + "</td>");
                writer.println("<td>" + md.getStringValue() + "</td>");
                writer.println("</tr>");
                
            }
            writer.println("</tbody></table>");
        }
    }    
    
    // Create a file and HTML fragment with a regression analysis column
    private void addRegressionPage(BlockEnvironment env, Data allData, int index, PrintWriter writer){
        try {
            Column col = allData.column(index);
            NumericalColumn yScaled = new UnitVarianceColumnScaler(((NumericalColumn)col)).scaledColumn();
            
            // Build some regression data
            Data xData = new Data();
            
            
            for(int i=0;i<allData.getColumnCount();i++){
                if(i!=index && allData.column(i) instanceof NumericalColumn){
                    xData.addColumn(new UnitVarianceColumnScaler((NumericalColumn)allData.column(i)).scaledColumn());
                }
            }
            XYDataToInstances converter = new XYDataToInstances(xData, (Column)yScaled);
            Instances wekaInstances = converter.toWekaInstances();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);
            
            // Get data name from 'x' data
            wekaInstances.setRelationName(xData.getName());

            // Create new LR model and set user parameters and defaults
            LinearRegression model = new LinearRegression();
            model.setEliminateColinearAttributes(true); 
            
            SelectedTag attributeSelectionMethods = model.getAttributeSelectionMethod();
            SelectedTag selectedMethod = new SelectedTag(LinearRegression.SELECTION_M5, attributeSelectionMethods.getTags());
            model.setAttributeSelectionMethod(selectedMethod);            
                    
            model.buildClassifier(wekaInstances);
            
            // Get predictions
            DoubleColumn predicted = new DoubleColumn("Predicted");
            for (int i = 0; i < wekaInstances.numInstances(); i++) {
                predicted.appendDoubleValue(model.classifyInstance(wekaInstances.instance(i)));
            }

            Data predictionData = new Data();
            predictionData.addColumn(((Column)yScaled).getCopy());
            predictionData.column(0).setName("Actual");
            predictionData.addColumn(predicted);
                        
            
            File tmpDataFile = new File(tmpDir, "rr_" + index + ".csv");
            writeData(predictionData, tmpDataFile);
            GnuPlotEngine plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/actualvpredicted.txt", tmpDir.getName() + File.separator + "cc_" + plotCount + ".png");
            plotEngine.getAdditionalParameters().add("plot-data", tmpDataFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() +"/" + "rr_" + index + ".png");
            plotEngine.createPlot();  
                         
            
            if(firstPage){
                writer.println("<h2>Regression analysis for column: " + col.getName() + "</h2>");
                firstPage = false;
            } else {
                writer.println("<h2 class=\"break\">Regression analysis for column: " + col.getName() + "</h2>");
            }
            
            // Regression parameter column
            double[] coeffs = model.coefficients();
            double absMax = Double.MIN_VALUE;
            for(int i=0;i<coeffs.length;i++){
                if(coeffs[i]>absMax){
                    absMax = coeffs[i];
                }       
            }
            
            
            writer.println("<table width=\"100%\"><thead style=\"text-align: center\"><tr><th style=\"width:30%;\">Variable Contributions</th><th style=\"width:70%;\">Model Prediction (Scaled)</th></tr></thead><tbody><tr>");
            writer.println("<td>");
            
            double regressionCutoff = env.getDoubleProperty("RegressionCutoff", 1e-20);
            NumberFormat format = NumberFormat.getNumberInstance();
            format.setMinimumFractionDigits(5);
            format.setMaximumFractionDigits(5);
            
            writer.println("<table width=\"100%\"><tbody>");
            String background;
            
            for(int i=0;i<xData.getColumnCount();i++){
                if(Math.abs(coeffs[i])>regressionCutoff){
                    writer.println("<tr>");
                    writer.println("<td style=\"width:20%;\" class=\"regressiontable\">" + xData.column(i).getName() + "</td>");
                    //writer.println("<td class=\"regressiontable\">" + format.format(coeffs[i]) + "</td>");
                    int widthPercent = (int)((Math.abs(coeffs[i]) / absMax) * 100d);
                    if(coeffs[i]<0){
                        background = "#CC0000";
                    } else {
                        background = "#00CC00";
                    }
                    
                    //writer.println("<td style=\"width:80%;\"><div style=\"foreground: white;background-color:black;width:" + widthPercent + "px;height:20px;\">" + format.format(coeffs[i]) + "</div></td>");
                    writer.println("<td style=\"width:80%;\"><div style=\"foreground: white;background-color:" + background + ";width:" + widthPercent + "px;height:10px;\"></div></td>");
                    writer.println("</tr>");
                }
            }
            
            writer.println("</table>");
            writer.println("</td>");
            
            // Regression graph column
            writer.println("<td><img width=\"100%;\" src=\"rr_" + index + ".png\"/></td>");   
            
            writer.println("</tr></tbody></table>");
        } catch (Exception e){
            System.out.println("Error generating regression page: " + e.getMessage());
        }
    }
    
    // Create a file and HTML fragment for a numerical column
    private void addNumericalColumnPage(BlockEnvironment env, Data allData, int index, PrintWriter writer) {
        try {
            Column col = allData.column(index);
            NumericalColumn nCol = (NumericalColumn)col;

            // Write this data in a form suitable for plotting
            File tmpFile = new File(tmpDir, "td.csv");
            Data tmpData = new Data();
            tmpData.addColumn(col.getCopy());

            MinValueCalculator minCalc = new MinValueCalculator(nCol);
            MaxValueCalculator maxCalc = new MaxValueCalculator(nCol);        
            double min = minCalc.doubleValue();
            double max = maxCalc.doubleValue();
            writeData(tmpData, tmpFile);

            GnuPlotEngine plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/histogram.txt", tmpDir.getName() + File.separator + "histogram" + plotCount + ".png");
            int plot1 = plotCount;
            plotEngine.getAdditionalParameters().add("plot-data", tmpFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() + "/" + "histogram" + plotCount + ".png");
            plotEngine.getAdditionalParameters().add("variable-name", col.getName());
            plotEngine.getAdditionalParameters().add("data-min", minCalc.doubleValue());
            plotEngine.getAdditionalParameters().add("data-max", maxCalc.doubleValue());
            plotEngine.createPlot();        
            plotCount++;
           
            plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/trend.txt", tmpDir.getName() + File.separator + "trend" + plotCount + ".png");
            int plot2 = plotCount;
            plotEngine.getAdditionalParameters().add("plot-data", tmpFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() + "/" + "trend" + plotCount + ".png");
            plotEngine.getAdditionalParameters().add("variable-name", col.getName());
            plotEngine.getAdditionalParameters().add("data-min", minCalc.doubleValue());
            plotEngine.getAdditionalParameters().add("data-max", maxCalc.doubleValue());
            plotEngine.createPlot();        
            plotCount++;
            
            if(firstPage){
                writer.println("<h2>Report for column: " + col.getName() + "</h2>");
                firstPage = false;
            } else {
                writer.println("<h2 class=\"break\">Report for column: " + col.getName() + "</h2>");
            }
            
            writer.println("<table width=\"100%\"><thead style=\"text-align: center\"><tr><th style=\"width:50%;\">Distribution</th><th style=\"width:50%;\">Trend Plot</th></tr></thead><tbody><tr>");
            writer.println("<td><img width=\"100%;\" src=\"histogram" + plot1 + ".png\"/></td>");      
            writer.println("<td><img width=\"100%;\" src=\"trend" + plot2 + ".png\"/></td>");      
            writer.println("</tr></tbody></table>");
            
            // Correlation structure
            Data correlations = new Data();
            correlations.addColumn(new IntegerColumn("Index"));
            correlations.addColumn(new StringColumn("Name"));
            correlations.addColumn(new DoubleColumn("Correlation"));
            correlations.addColumn(new DoubleColumn("AbsCorrelation"));
            
            NumericalColumn x;
            CorrelationCalculator calc;
            double correlation;
            
            // Work out all of the correlations
            for(int i=0;i<allData.getColumnCount();i++){
                if(i!=index && allData.column(i) instanceof NumericalColumn){
                    x = (NumericalColumn)allData.column(i);
                    calc = new CorrelationCalculator(x, nCol);
                    correlation = calc.doubleValue();
                    ((IntegerColumn)correlations.column(0)).appendIntValue(i);
                    correlations.column(1).appendObjectValue(x.getName());
                    correlations.column(2).appendObjectValue(correlation);
                    correlations.column(3).appendObjectValue(Math.abs(correlation));
                }
            }
            
            // Sort
            Data sortedCorrelations = sortData(correlations, 3);
            int corrPlots = 4;
            if(sortedCorrelations.getLargestRows()<corrPlots){
                corrPlots = sortedCorrelations.getLargestRows();
            }
            
            writer.println("<h3>Top: " + corrPlots + " correlated columns</h3>");
            writer.println("<table width=\"100%\">");
            
            writer.println("<thead style=\"text-align: center\"><tr>");
            for(int i=0;i<corrPlots;i++){
                String corrValue = "";
                double corrValueDbl = ((NumericalColumn)sortedCorrelations.column(2)).getDoubleValue(i);
                if(Double.isNaN(corrValueDbl)){
                    corrValue = "NaN";
                }
                else{
                    corrValue = shortDf.format(corrValueDbl);
                }

                writer.println("<th style=\"width:" + (int)(100 / corrPlots) + "%;\">" + sortedCorrelations.column(1).getStringValue(i) + "(" + corrValue + ")</th>");
            }
            
            writer.println("</tr></thead><tbody><tr>");
            for(int i=0;i<corrPlots;i++){
                Data xyTemp = new Data();
                xyTemp.addColumn(col.getCopy());
                xyTemp.addColumn(allData.column(((IntegerColumn)sortedCorrelations.column(0)).getIntValue(i)).getCopy());
                
                File tmpDataFile = new File(tmpDir, "cc_" + plotCount + ".csv");
                writeData(xyTemp, tmpDataFile);
                plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/xyplot.txt", tmpDir.getName() + File.separator + "cc_" + plotCount + ".png");
                plotEngine.getAdditionalParameters().add("plot-data", tmpDataFile.getPath());
                plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() +"/" + "cc_" + plotCount + ".png");
                plotEngine.getAdditionalParameters().add("x-variable", xyTemp.column(0).getName());
                plotEngine.getAdditionalParameters().add("y-variable", xyTemp.column(1).getName());
                plotEngine.createPlot();  
                writer.println("<td><img width=\"100%;\" src=\"cc_" + plotCount + ".png\"/></td>");      
                plotCount++;
            }
            writer.println("</tr></tbody></table>");
            
            
            // Summary for the data
            writer.println("<h3>Data summary</h3>");
            writer.println("<table width=\"100%\"><thead><tr><th>Property</th><th>Value</th></tr></thead></tbody>");
            writer.println("<tr><td>Number of rows</td><td>" + nCol.getRows() + "</td></tr>");
            writer.println("<tr><td>Minimum value</td><td>" + df.format(min) + "</td></tr>");
            writer.println("<tr><td>Maximum value</td><td>" + df.format(max) + "</td></tr>");
            writer.println("<tr><td>Standard deviation</td><td>" + df.format(new StdCalculator(nCol).doubleValue()) + "</td></tr>");
            writer.println("</tbody></table>");
            

            
        } catch (Exception e){
            System.out.println("Error creating graph: " + e.getMessage());
        }
    } 
    
    private void addTextColumnPage(BlockEnvironment env, Data allData, int index, PrintWriter writer){
        try {
            Column col = allData.column(index);
            StringColumn strCol = (StringColumn)col;
            HashMap<String, Long> valueMap = new HashMap<>();

            String value;
            long count;
            for(int i=0;i<strCol.getRows();i++){
                if(!strCol.isMissing(i)){
                    value = strCol.getStringValue(i);

                    if(valueMap.containsKey(value)){
                        // Increment
                        count = valueMap.get(value);
                        count++;
                        valueMap.put(value, count);

                    } else {
                        // Create
                        valueMap.put(value, 1L);
                    }
                }
            }

            DefaultPieDataset dataset = new DefaultPieDataset();
            Data frequencyData = new Data();
            frequencyData.addColumn(new StringColumn("Value"));
            frequencyData.addColumn(new IntegerColumn("Frequency"));
            
            for(String key : valueMap.keySet()){
                dataset.setValue(key, valueMap.get(key));
                frequencyData.column(0).appendStringValue(key);
                frequencyData.column(1).appendObjectValue(valueMap.get(key));
            }

            JFreeChart jfc = ChartFactory.createPieChart("Text Value Breakdown", dataset, false, false, false);
            jfc.getTitle().setFont(new Font("Arial", Font.BOLD, 40));
            PiePlot pie = (PiePlot)jfc.getPlot();
            pie.setSimpleLabels(true);
            pie.setLabelFont(new Font("Arial", Font.BOLD, 20));
            pie.setBackgroundPaint(Color.WHITE);
            BufferedImage img = jfc.createBufferedImage(1024, 1024);
            File plotFile = new File(tmpDir, "pie" + plotCount + ".png");
            int plot1 = plotCount;
            
            plotCount++;
            
            ImageIO.write(img, "png", plotFile);
            
            if(firstPage){
                writer.println("<h2>Report for column: " + col.getName() + "</h2>");
                firstPage = false;
            } else {
                writer.println("<h2 class=\"break\">Report for column: " + col.getName() + "</h2>");
            }
            
            writer.println("<img class=\"centred\"  width=\"70%\" src=\"pie" + plot1 + ".png\"/>");      
            
            // Summary for the data
            writer.println("<h2>Data summary</h2>");
            writer.println("<table width=\"100%\"><thead><tr><th>Property</th><th>Value</th></tr></thead></tbody>");
            writer.println("<tr><td>Number of rows</td><td>" + col.getRows() + "</td></tr>");
            writer.println("<tr><td>Number of distict values</td><td>" + valueMap.size() + "</td></tr>");
            writer.println("</tbody></table>");
            
            // Top values
            Data sortedValues = sortData(frequencyData, 1);
            
            if(sortedValues.getLargestRows()>10){
                // Need to truncate
                writer.println("<h2>Top 10 values</h2>");
                writer.println("<table width=\"100%\"><thead><tr><th>Value</th><th>Frequency</th></tr></thead></tbody>");
                for(int i=0;i<10;i++){
                    writer.println("<tr><td>" + sortedValues.column(0).getStringValue(i) + "</td><td>" + sortedValues.column(1).getStringValue(i) + "</td></tr>");
                }
                writer.println("</tbody></table>");         
                
            } else {
                // Can show everything
                writer.println("<h2>Value frequency</h2>");
                writer.println("<table width=\"100%\"><thead><tr><th>Value</th><th>Frequency</th></tr></thead></tbody>");
                for(int i=0;i<sortedValues.getLargestRows();i++){
                    writer.println("<tr><td>" + sortedValues.column(0).getStringValue(i) + "</td><td>" + sortedValues.column(1).getStringValue(i) + "</td></tr>");
                }
                writer.println("</tbody></table>");                
            }
            
        } catch (Exception e){
            System.out.println("Error producing pie chart: " + e.getMessage());
        }
    }
    
    private Data sortData(Data data, int sortIndex) throws Exception {
        Column index = data.column(sortIndex);
        ColumnSorter sorter = new ColumnSorter(index);
        sorter.setDescending(true);
        sorter.sort();

        IntegerColumn rowIndices = sorter.getIndexColumn();
        Data output = data.getEmptyCopy();
        Data row;
        int rows = rowIndices.getRows();
        int indexPosition;

        for(int i=0;i<rows;i++){
            indexPosition = rowIndices.getIntValue(i);
            row = data.getRowSubset(indexPosition, indexPosition, true);
            output.appendRows(row, false);
        }     
        return output;
    }
    
    private void writeData(Data data, File f) throws Exception {
        // Need to create a new data file
        CSVDataExporter exporter = new CSVDataExporter(data);
        exporter.setDelimiter(" ");
        exporter.setIncludeNames(false);
        exporter.setRowIndexIncluded(true);
        exporter.setRowIndexValue(0);
        PrintWriter dataWriter = new PrintWriter(f);
        exporter.writeToPrintWriter(new PrintWriter(f));
        dataWriter.flush();
        dataWriter.close();
    }    
}
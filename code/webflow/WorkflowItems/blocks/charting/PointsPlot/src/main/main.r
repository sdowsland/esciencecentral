
xydata = read.csv(inputFile);
title = getProperty("title")
xlabel = getProperty("xLabel")
ylabel = getProperty("yLabel")
type = getProperty("type")

#Reverse the X Axis?
xorder = getProperty("reverseXAxis");

xlim=range(xydata[1])

if(xorder == "true"){
    xlim=rev(range(xydata[1]));
}

chart = getProperty("chart")

#Plot the chart
png(filename=chart);
plot(xydata,
     main=title,
     type=type,
     xlab=xlabel,
     ylab=ylabel,
     xlim=xlim)
abline(0,0);
dev.off()


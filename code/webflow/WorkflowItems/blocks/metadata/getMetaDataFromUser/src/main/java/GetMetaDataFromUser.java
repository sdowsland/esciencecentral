
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.StringColumn;

import java.util.List;

public class GetMetaDataFromUser extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        // This is the API link that can be used to access data in the
        // Inkspot platform. It is configured using the identity of
        // the user executing the service

        String category = getProperties().stringValue("ItemCategory", "");
        String name = getProperties().stringValue("ItemName", "");
        APIBroker api = createApiLink();

        Ticket ticket = getTicket();
        ticket.getUserId();

        User user = new User();
        user.setId(ticket.getUserId());

        Data outputData = new Data();
        Column mdValues = new StringColumn();
        outputData.addColumn(mdValues);
        MetadataCollection mdc= api.getMetadata(user);
        List<MetadataItem> mdItems = mdc.getItems();
        for(MetadataItem md : mdItems)
        {
            if(md.getCategory() != null && md.getCategory().equals(category))
            {
                if(md.getName() != null && md.getName().equals(name))
                {
                   String value = md.getStringValue();
                    mdValues.appendObjectValue(value);
                }
            }
        }



        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputDataSet("metadata", outputData);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
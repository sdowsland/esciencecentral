@echo off

mvn archetype:generate ^
    -DinteractiveMode=false ^
    -DarchetypeGroupId=com.connexience ^
    -DarchetypeArtifactId=workflow-block-java ^
    -DarchetypeVersion=3.1-SNAPSHOT ^
    -DgroupId=com.connexience.services.workflow ^
    -DartifactId=ControlBarrier ^
    -Dversion=1.0

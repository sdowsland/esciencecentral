/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;

import org.pipeline.core.data.*;
import org.pipeline.apps.neuralnetwork.ffann.*;
import org.pipeline.core.matrix.*;
import org.pipeline.core.optimisation.*;
import org.pipeline.core.optimisation.simplex.*;
import org.pipeline.core.xmlstorage.*;
import org.pipeline.core.xmlstorage.io.*;
import org.pipeline.core.util.*;
import com.connexience.server.workflow.engine.datatypes.*;
import java.io.*;


public class MyService extends CloudDataProcessorService implements OptimiserListener
{
    private OptimisationController controller = new OptimisationController();
    private SimplexOptimiser optimiser;
    private int maxIterations = 4000;
    private boolean running = false;
    
    /** This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here. */
    public void executionAboutToStart() throws Exception {
        controller.addOptimiserListener(this);
        optimiser = new SimplexOptimiser();
        
    }

    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
        
        int hiddenNeurons = getEditableProperties().intValue("Neurons", 5);
        maxIterations = getEditableProperties().intValue("MaxIterations", 4000);
        optimiser.getProperties().add("MaxIterations", maxIterations);
        
        // The following method is used to get a set of input data
        // and should be configured using the name of the input
        // data to fetch
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");
        
        Matrix x = new DataToMatrixConverter(xData).toMatrix();
        Matrix y = new DataToMatrixConverter(yData).toMatrix();
        
        // Create new model
        FFANNModel model = new FFANNModel();
        model.setInputs(x.getColumnDimension());
        model.setOutputs(y.getColumnDimension());
        model.setHiddenNeurons(hiddenNeurons);
        model.initialiseWeights();
        FFANNModelTrainingHarness harness = new FFANNModelTrainingHarness(model);
        harness.setY(y);
        harness.setX(x);
        controller.setTarget(harness);
        controller.setOptimiser(optimiser);
                    
        controller.startOptimiser();
        running = true;
        while(running){
            Thread.sleep(1000);    
        }
        
        OptimisedParameterVector parameters;
        if(controller.getOptimiser().getBestParameters()!=null){
            parameters = controller.getOptimiser().getBestParameters();
        } else {
            parameters = new OptimisedParameterVector(harness.getParameterCount());
            parameters.setFromMatrix(harness.getModel().getParameterVector());
        }        
        model.setParameterVector(parameters.convertToMatrix());
        Matrix yest = model.getPredictions(x);                
        Data outData = new MatrixToDataConverter(yest).toData(); 
                                    
        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputDataSet("y-pred", outData);
        File modelFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("ModelFile", "model.xml"));
        XmlDataStore holder = new XmlDataStore("Model");
        holder.add("NeuralNetwork", model);
        
        XmlDataStoreStreamWriter writer = new XmlDataStoreStreamWriter(holder);
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(modelFile);
            writer.write(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

        try {
            FileWrapper wrapper = new FileWrapper(getWorkingDirectory());
            wrapper.addFile(modelFile);
            setOutputData("model-file", wrapper);
        } catch (Exception e){
            System.out.println("Warning: Could not set model file output");
        }
    }

    /** All of the data has been passed through the service. Any clean up code
    * should be placed here */
    public void allDataProcessed() throws Exception {

    }
    
    /** Optimiser has started */
    public void optimiserStarted(){
        running = true;
    }
    
    /** Optimiser has finished */
    public void optimiserStopped(){
        running = false;
    }
    
    /** Optimiser has found an improved error */
    public void optimiserImprovedError(int iteration, double error){
        System.out.println("Better error: " + iteration + ", " + error);
    }
    
    /** Optimisation iteration has completed */
    public void optimiserIterationCompleted(int iteration){
        if(iteration>maxIterations){
            controller.stopOptimiser();
        }
    }
    
    /** Optimiser has been reset */
    public void optimiserReset(){
    }
}
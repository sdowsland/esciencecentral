
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.manipulation.*;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IMolecule;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.qsar.*;
import org.openscience.cdk.qsar.result.*;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.*;
import org.openscience.cdk.aromaticity.*;

import java.util.*;

public class MyService extends CloudDataProcessorService {
    private List<IDescriptor> descriptors = null;
    
    /** This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here. */
    public void executionAboutToStart() throws Exception {
        List<String>    descriptorNames;
        descriptorNames = new ArrayList<String>();
        descriptorNames.add("BCUTDescriptor");
        descriptorNames.add("ZagrebIndexDescriptor");
        descriptorNames.add("XLogPDescriptor");
        descriptorNames.add("WienerNumbersDescriptor");
        descriptorNames.add("WeightedPathDescriptor");
        descriptorNames.add("WeightDescriptor");
        descriptorNames.add("VAdjMaDescriptor");
        descriptorNames.add("TPSADescriptor");
        descriptorNames.add("RuleOfFiveDescriptor");
        descriptorNames.add("RotatableBondsCountDescriptor");
        descriptorNames.add("PetitjeanNumberDescriptor");
        descriptorNames.add("MDEDescriptor");
        descriptorNames.add("LongestAliphaticChainDescriptor");
        descriptorNames.add("LargestPiSystemDescriptor");
        descriptorNames.add("LargestChainDescriptor");
        descriptorNames.add("KierHallSmartsDescriptor");
        descriptorNames.add("KappaShapeIndicesDescriptor");
//        descriptorNames.add("IPMolecularLearningDescriptor");
        descriptorNames.add("HBondDonorCountDescriptor");
        descriptorNames.add("HBondAcceptorCountDescriptor");
        descriptorNames.add("FragmentComplexityDescriptor");
        descriptorNames.add("EccentricConnectivityIndexDescriptor");
        descriptorNames.add("ChiChainDescriptor");
        descriptorNames.add("ChiClusterDescriptor");
        descriptorNames.add("ChiPathDescriptor");
        descriptorNames.add("ChiPathClusterDescriptor");
        descriptorNames.add("CarbonTypesDescriptor");
        descriptorNames.add("BondCountDescriptor");
        descriptorNames.add("AutocorrelationDescriptorPolarizability");
        descriptorNames.add("AutocorrelationDescriptorMass");
        descriptorNames.add("AutocorrelationDescriptorCharge");
        descriptorNames.add("AtomCountDescriptor");
        descriptorNames.add("AromaticBondsCountDescriptor");
        descriptorNames.add("AromaticAtomsCountDescriptor");
        descriptorNames.add("APolDescriptor");
        descriptorNames.add("ALOGPDescriptor");

        descriptors = instantiateDescriptors(descriptorNames);
    }

    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
        IMolecule im;
        SmilesParser sp = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        String descriptDelim = ",";

        Data data1 = getInputDataSet("input-data");
        String Smi1columnName = getEditableProperties().stringValue("SMILESColumn", "#0");
        ColumnPicker picker1 = new ColumnPicker(Smi1columnName);
        picker1.setCopyData(false);
        Column smiles1Column = picker1.pickColumn(data1);

        Data outData = new Data();

        // The first column will contain ALL descriptor names:
        StringColumn allDescriptorNames = new StringColumn("descriptorNames");
        for (IDescriptor descriptor : descriptors) {
            String[] names = descriptor.getDescriptorNames();
            for (int i = 0; i < names.length; i++) {
                String desName = new String();
                desName = /*"K" +*/ names[i];
                // get rid of the characters that will cause problems in the BUS
                String clean1 = new String();
                String clean2 = new String();
                String[] toks = desName.split("[.]");
                for (int j = 0; j < toks.length; j++) {
                    clean1 += toks[j];
                }
                toks = clean1.split("[-]");
                for (int j = 0; j < toks.length; j++) {
                    clean2 += toks[j];
                }
                allDescriptorNames.appendStringValue(clean2);
            }
        }
        outData.addColumn(allDescriptorNames);

        for (int i = 0; i < smiles1Column.getRows(); i++) {
            if (!smiles1Column.isMissing(i)) {
                try {
                    String Smiles = smiles1Column.getStringValue(i);
                    System.out.println("Calculating: " + Smiles);
                    im = sp.parseSmiles(Smiles);
                    // Add implicit hydrogens
                    CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(im.getBuilder());
                    adder.addImplicitHydrogens(im);
                    //configure atom types
                    AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(im);
                    //detect aromaticity
                    CDKHueckelAromaticityDetector.detectAromaticity(im);
                    boolean thrown = false;
                    DoubleColumn col = new DoubleColumn(Smiles);
                    for (int j = 0; j < descriptors.size(); j++) {
                        try {
                            IDescriptor descriptor = (IDescriptor) descriptors.get(j);
                            DescriptorValue value = ((IMolecularDescriptor) descriptor).calculate(im);
                            IDescriptorResult descValues = value.getValue();
                            String descValStr = descValues.toString();
                            // Now tokenize the string
                            StringTokenizer stringTokens = new StringTokenizer(descValStr, descriptDelim);
                            while (stringTokens.hasMoreTokens()) {
                                String val = stringTokens.nextToken();
                                double doubleResult = Double.parseDouble(val);
                                col.appendDoubleValue(doubleResult);
                            }
                        } catch (Exception exception) {
                            thrown = true;
                            break;
                        }
                    }
                    // Guarantee that each row contains ALL descriptors
                    if (!thrown) {
                        // Put the descriptors value on the Data array
                        outData.addColumn(col);
                    }
                } catch (Exception exception) {
                    System.err.println("Cannot recognize SMILES number " + i);
                }
            }
        }

        // Transpose if needed
        if(getEditableProperties().booleanValue("TransposeResults", true)){
            DataTransposer transposer = new DataTransposer(outData);
            transposer.setExtractNamesColumn(true);
            transposer.setIncludeOriginalNames(false);
            transposer.setNamesPicker(new ColumnPicker("#0"));
            transposer.setColumnNamesColumnName("Descriptor");
            setOutputDataSet("descriptors", transposer.transpose());
        } else {
            setOutputDataSet("descriptors", outData);
        }
    }

    /** All of the data has been passed through the service. Any clean up code
    * should be placed here */
    public void allDataProcessed() throws Exception {

    }
    
    public List<IDescriptor> instantiateDescriptors(List<String> descriptorNames) {
        List<IDescriptor> newDescriptors;
        newDescriptors = new ArrayList<IDescriptor>();
        for (String DescName : descriptorNames) {
            String fullDescName = "org.openscience.cdk.qsar.descriptors.molecular." + DescName;
            try {
                IDescriptor descriptor = (IDescriptor) Class.forName(fullDescName).newInstance();
                newDescriptors.add(descriptor);
            }
            catch(ClassNotFoundException exception) {
                System.err.println("ClassNotFoundException. Descriptor: " + DescName + " not found");
            }
            catch(InstantiationException exception) {
                System.err.println("InstantiationException. Descriptor: " + DescName + " not found");
            }
            catch(IllegalAccessException exception) {
                System.err.println("IllegalAccessException. Descriptor: " + DescName + " not found");
            }
        }
        return newDescriptors;
    }    
}
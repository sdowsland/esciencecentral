/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.dashboard.Dashboard;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.*;
import org.pipeline.core.data.*;
import org.json.*;

public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        // This is the API link that can be used to access data in the
        // Inkspot platform. It is configured using the identity of
        // the user executing the service
        APIBroker api = createApiLink();

        // The following method is used to get a set of input data
        // and should be configured using the name of the input
        // data to fetch
        Data inputData = getInputDataSet("input-data");
        
        Dashboard dashboard = (Dashboard)getProperties().xmlStorableValue("Dashboard");
        String itemName = getProperties().stringValue("DashboardItem", null);
        if(dashboard!=null && dashboard.getId()!=null && itemName!=null && !itemName.isEmpty()){
            JSONObject dataRow;
            
            // Add each row in the input data
            for(int i=0;i<inputData.getLargestRows();i++){
                dataRow = dataRowToJson(inputData, i);
                api.updateDatasetItem(dashboard.getId(), itemName, new JSONContainer(dataRow));
            }
        } else {
            throw new Exception("Dashboard properties incorrectly configured.");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
    
    /** Save a data row to a JSON Object */
    public JSONObject dataRowToJson(Data data, int row) throws Exception {
        JSONObject dataRow = new JSONObject();
        Column c;
        String name;
        for(int i=0;i<data.getColumns();i++){
            c = data.column(i);
            name = c.getName().replace(" ", "_");
            if(!c.isMissing(row)){
                dataRow.put(name, c.getObjectValue(row));
            } else {
                dataRow.put(name, (Object)null);
            }
        }
        
        return dataRow;
    }
}
<!--
  e-Science Central
  Copyright (C) 2008-2013 School of Computing Science, Newcastle University

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  version 2 as published by the Free Software Foundation at:
  http://www.gnu.org/licenses/gpl-2.0.html

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<WorkflowService>
    <!-- Name of the service, and also the caption that will appear     -->
    <!-- in the top line of the block on the workflow editor            -->
    <Name>ActrExecute</Name>

    <!-- Service description that appears at the bottom of the editor   -->
    <!-- window when the block is selected                              -->
    <Description>Executes the CogTool customised ACT-R against a specified model and memory image.</Description>

    <!-- Category to place the service in on the editor palette         -->
    <Category>Projects.CogTool</Category>

    <!-- Homepage for block documentation                               -->
    <Homepage>/</Homepage>

    <!-- Class name of the service. This needs to extend either the     -->
    <!-- DataProcessorService or the CloudDataProcessorService object   -->
    <ServiceRoutine>cogtool.actr.ActrExecute</ServiceRoutine>

    <!-- Auto deployed service. Do NOT change for dynamically deployed  -->
    <!-- services that are uploaded via this editor                     -->
    <ServiceType>AUTO</ServiceType>

    <!-- Data streaming mode for this service. This can be one of:      -->
    <!--                                                                -->
    <!-- nostream   - Data is passed in one block through service       -->
    <!-- sequential - Data is streamed one connection at a time         -->
    <!-- parallel   - Data is streamed from all connections in parallel -->
    <StreamMode>nostream</StreamMode>

    <!-- Editable service parameters. These properties define what is   -->
    <!-- displayed in the properties panel when a block is selected in  -->
    <!-- the workflow editor. The format of properties is:              -->
    <!--                                                                -->
    <!-- <Property name="" type="" description="" default=""/>          -->
    <!--                                                                -->
    <!-- Where:     name = property name without spaces                 -->
    <!--            type = Document - file reference                    -->
    <!--                   Folder - folder reference                    -->
    <!--                   Integer - integer paramater                  -->
    <!--                   Boolean - true / false value                 -->
    <!--                   String - text parameter                      -->
    <!--                   Double - floating point value                -->
    <!--                   Date - java date parameter                   -->
    <!--                   StringList - vector of text values           -->
    <!--                   TwoColumnList - two columns of text values   -->
    <!--                   ExternalObject - Application defined object  -->
    <Properties>
        <Property name="DebugMode" type="Boolean" default="false" description="" category="Debugging"/>
        <Property name="DebugSuspended" type="Boolean" default="true" description="" category="Debugging"/>
        <Property name="DebugPort" type="Integer" default="5005" description="" category="Debugging"/>

        <Property name="ClispBinary" type="String" default="clisp" category="CLISP"
                  description="Command to execute CLISP on the system"/>
        <Property name="ClispEncoding" type="String" default="ISO-8859-1" category="CLISP"
                  description="The text encoding for CLISP output"/>
        <Property name="ClispCommand" type="String" default="*cogtool-result*" category="CLISP"
                  description="The lisp command argument (-x) to pass to CLISP"/>
        <Property name="ClispAdditionalParameters" type="StringList" category="CLISP"
                  description="Additional command line parameters to pass to CLISP."/>

        <Property name="ActrVisualAttention" type="Integer" default="85" category="ACTR"
                  description="ACT-R Visual Attention time, in milliseconds."/>
        <Property name="ActrMotorInitiation" type="Integer" default="50" category="ACTR"
                  description="ACT-R Motor Initiation time, in milliseconds."/>
        <Property name="ActrPeckFittsCoefficient" type="Integer" default="75" category="ACTR"
                  description="ACT-R peck Fitts co-efficient, in milliseconds."/>
        <Property name="ActrDat" type="Integer" default="50" category="ACTR"
                  description="ACT-R dat, in milliseconds."/>
    </Properties>

    <!-- Definition of all of the inputs to a service. The format is:   -->
    <!--                                                                -->
    <!-- <Input name="" type="" streaming=""/>                          -->
    <!--                                                                -->
    <!-- Where:     name = name of input also displayed on connections  -->
    <!--            type = data-wrapper - mixed matrix of data          -->
    <!--                   file-wrapper - list of file names            -->
    <!--                   object-wrapper - Serialized Java object      -->
    <!--            streaming = true / false - is this a streaming link -->
    <Inputs>
        <Input name="input-model" type="file-wrapper" streaming="false"/>
    </Inputs>

    <!-- Definition of all of the outputs from service. The format is:  -->
    <!--                                                                -->
    <!-- <Output name="" type="" streaming=""/>                         -->
    <!--                                                                -->
    <!-- Where:     name = name of input also displayed on connections  -->
    <!--            type = data-wrapper - mixed matrix of data          -->
    <!--                   file-wrapper - list of file names            -->
    <!--                   object-wrapper - Serialized Java object      -->
    <!--            streaming = true / false - is this a streaming link -->
    <Outputs>
        <Output name="output-file" type="file-wrapper" streaming="false"/>
        <Output name="output-parameters" type="data-wrapper" streaming="false"/>
    </Outputs>

</WorkflowService>
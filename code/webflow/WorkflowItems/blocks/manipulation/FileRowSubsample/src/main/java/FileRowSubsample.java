
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import org.pipeline.core.data.*;

public class FileRowSubsample extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper inputWrapper = (FileWrapper)getInputData("input-file");
        if(inputWrapper.getFileCount()==1){
            long rowCount = 0;
            long sampleInterval = getEditableProperties().longValue("SampleInterval", 10);
            String prefix = getEditableProperties().stringValue("FilePrefix", "sampled-");
            File inputFile = inputWrapper.getFile(0);
            File outputFile = new File(getWorkingDirectory(), prefix + inputFile.getName());
            
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(outputFile)));
            String line;
            while((line=reader.readLine())!=null){
                rowCount++;
                if((rowCount % sampleInterval)==0){
                    writer.println(line);
                }
            }
            writer.flush();
            writer.close();
            reader.close();
            
            FileWrapper outputWrapper = new FileWrapper(getWorkingDirectory());
            outputWrapper.addFile(outputFile, false);
            setOutputData("subsampled-file", outputWrapper);
        } else {
            throw new Exception("Only supports a single input file");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
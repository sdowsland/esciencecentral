/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import org.pipeline.core.data.*;

import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyService extends CloudDataProcessorService
{
    private static final String PROP_NEW_NAMES = "New Names";
    private static final String PROP_USE_REGEX = "Use Regex";
    private static final String PROP_REGEX = "Regex";
    private static final String PROP_REPLACEMENT = "Replacement";

    private static final String INPUT_DATA = "input-data";

    private static final String OUTPUT_RENAMED_COLUMNS = "renamed-columns";
    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        Data inputData = getInputDataSet(INPUT_DATA);
        Data outputData = inputData.getCopy();

        if (getProperties().booleanValue(PROP_USE_REGEX, false)) {
            String regex = getProperties().stringValue(PROP_REGEX, "").trim();
            System.out.println("Using regular expression: " + regex);
            if ("".equals(regex)) {
                throw new IllegalArgumentException("Property " + PROP_REGEX + " not set.");
            }
            String replacement = getProperties().stringValue(PROP_REPLACEMENT, "");

            Pattern p = Pattern.compile(regex);
            Enumeration e = outputData.columns();
            while (e.hasMoreElements()) {
                Column c = (Column)e.nextElement();
                Matcher m = p.matcher(c.getName());
                if (m.find()) {
                    c.setName(m.replaceAll(replacement));
                }
            }
        } else {
            StringListWrapper names = (StringListWrapper) getProperties().xmlStorableValue(PROP_NEW_NAMES);

            int minCol = Math.min(names.getSize(), outputData.getColumnCount());
            for (int i = 0; i < minCol; i++) {
                outputData.column(i).setName(names.getValue(i));
            }
        }

        setOutputDataSet(OUTPUT_RENAMED_COLUMNS, outputData);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
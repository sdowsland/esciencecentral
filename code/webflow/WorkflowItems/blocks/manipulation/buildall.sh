#!/bin/sh

for arg in `ls -d */`
do
	cd ${arg}
	echo `pwd`
	mvn -o clean install
	cd ..
done

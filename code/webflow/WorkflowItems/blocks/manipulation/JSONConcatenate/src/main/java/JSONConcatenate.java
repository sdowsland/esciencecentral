
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p/>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;

public class JSONConcatenate extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper jsonFiles1 = (FileWrapper) getInputData("json-file-1");
        FileWrapper jsonFiles2 = (FileWrapper) getInputData("json-file-2");

        if(jsonFiles1.getFileCount() != 1 || jsonFiles2.getFileCount() != 1){
            throw new ConnexienceException("Only one file supported on each connection");
        }
        JSONParser parser = new JSONParser();

        JSONObject j1 = (JSONObject) parser.parse(new FileReader(jsonFiles1.getFile(0)));
        JSONObject j2 = (JSONObject) parser.parse(new FileReader(jsonFiles2.getFile(0)));

        for(Object key : j2.keySet())
        {
            Object value = j2.get(key);

            j1.put(key, value);
        }

        File jsonFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("OutputFilename", "md.json"));
        PrintWriter jsonWriter = new PrintWriter(jsonFile);
        j1.writeJSONString(jsonWriter);
        jsonWriter.flush();
        jsonWriter.close();

        FileWrapper jsonWrapper = new FileWrapper(getWorkingDirectory());
        jsonWrapper.addFile(jsonFile, false);
        setOutputData("md-file", jsonWrapper);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
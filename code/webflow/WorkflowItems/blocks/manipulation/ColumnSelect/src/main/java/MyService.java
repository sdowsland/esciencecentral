
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.manipulation.ColumnPickerCollection;
import org.pipeline.core.xmlstorage.XmlDataStore;

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

/** E-sc block to select columns from an E-sc datawrapper
 * 
 * @author hugo, modified by Dominic
 */
public class MyService extends CloudDataProcessorService {

   
    @Override
    public void executionAboutToStart() throws Exception {
        
    }

  
    @Override
    public void execute() throws Exception {
        Data input = getInputDataSet("input-data");
        StringListWrapper list = (StringListWrapper)getEditableProperties().xmlStorableValue("ColumnList");
        
        ColumnPickerCollection pickers = new ColumnPickerCollection();
        pickers.setPickersCopyData(false);
        pickers.populateFromStringArray(list.toStringArray());
        
        Data selected = pickers.extractData(input);
        Data remaining = pickers.extractNonSelectedData(input);
        
        //ensure properties from input data are propagated to both output data sets (dps 27/3/14)
        if (input.hasProperties()) {
            
            XmlDataStore inputProps = input.getProperties();
            selected.getProperties().copyProperties(inputProps);
            remaining.getProperties().copyProperties(inputProps);
         
        }
        
        setOutputDataSet("selected-columns", selected);
        setOutputDataSet("remaining-columns", remaining);
    }

   
    @Override
    public void allDataProcessed() throws Exception {
        System.out.println("Column selection finished");
    }
}
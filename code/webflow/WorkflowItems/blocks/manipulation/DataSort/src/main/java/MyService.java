/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.manipulation.*;

public class MyService extends CloudDataProcessorService {

    /** This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here. */
    public void executionAboutToStart() throws Exception {

    }

    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
        boolean descending = getEditableProperties().booleanValue("SortDescending", false);
        String indexSpecifier = getEditableProperties().stringValue("IndexColumn", "");
        Data input = getInputDataSet("input-data");
        
        ColumnPicker indexPicker = new ColumnPicker(indexSpecifier);
        
        Column index = indexPicker.getColumnReference(input);
        ColumnSorter sorter = new ColumnSorter(index);
        sorter.setDescending(descending);
        sorter.sort();

        IntegerColumn rowIndices = sorter.getIndexColumn();
        Data output = input.getEmptyCopy();
        Data row;
        int rows = rowIndices.getRows();
        int indexPosition;

        for(int i=0;i<rows;i++){
            indexPosition = rowIndices.getIntValue(i);
            row = input.getRowSubset(indexPosition, indexPosition, true);
            output.appendRows(row, false);
        }        
        
        setOutputDataSet("sorted-data", output);
    }

    /** All of the data has been passed through the service. Any clean up code
    * should be placed here */
    public void allDataProcessed() throws Exception {

    }
}
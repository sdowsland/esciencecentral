package com.connexience.blocks;

/**
 * e-Science Central Copyright (C) 2008-2016 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.manipulation.ColumnPicker;


public class DataJoin implements WorkflowBlock
{
    private static class Selector {
        private static final String A = "A.";
        private static final String B = "B.";
        private static final String Star = "*";
    }

    private static final String Prop_OUTPUT_COLUMNS = "Output Columns";
    private static final String Prop_JOINING_COLUMNS = "Join Columns";

    private static final String Prop_JOIN_TYPE = "Join Type";
    private static final String _JOIN_INNER = "INNER";
    private static final String _JOIN_LEFT = "LEFT";
    private static final String _JOIN_RIGHT = "RIGHT";
    private static final String _JOIN_OUTER = "OUTER";

    private static final String Input_A = "input-A";
    private static final String Input_B = "input-B";

    private static final String Output_AB = "joined-output";

    private static class ColumnEntry
    {
        Column A;
        Column B;
        Column out;
    }

    private boolean _inputValidated;
    private String[][] _joiningColumnNames;

    private Data _inputRight;
    private Data _selectedRight;
    private Data _nonSelectedRight;

    /**
     * Needed to implement the hashed version of the JOIN: O(N + M)
     */
    HashMap<String, ArrayList<Integer>> _keyToRightRow;

    // Variables needed to handle RIGHT and OUTER joins
    ArrayList<ColumnEntry> _lastColumnMap;
    ArrayList<Integer> _rightRowsFound = new ArrayList<>();


    @Override
    public void preExecute(BlockEnvironment env) throws Exception
    {
        _joiningColumnNames  = env.getStringMatrixProperty(Prop_JOINING_COLUMNS);
        if (_joiningColumnNames.length < 1) {
            throw new IllegalArgumentException("List " + Prop_JOINING_COLUMNS + " is empty.");
        }

        // Input_B is non-streaming link, so we can read as a whole...
        _inputRight = env.getExecutionService().getInputDataSet(Input_B);

        // Note that the order of the columns must match the JOINING_COLUMNS property
        _selectedRight = new Data();
        for (String[] stringPair : _joiningColumnNames) {
            _selectedRight.addColumn(_selectColumn(_inputRight, stringPair[1]));
        }
        _nonSelectedRight = _extractNonSelectedColumns(_inputRight, _selectedRight);

        // and build the key to row map.
        int rowNo = _inputRight.getLargestRows();
        _keyToRightRow = new HashMap<>(2 * rowNo);

        for (int r = 0; r < rowNo; r++) {
            String rowKey = _computeKey(_selectedRight, r);
            ArrayList<Integer> rows = _keyToRightRow.get(rowKey);
            if (rows == null) {
                rows = new ArrayList<>();
                _keyToRightRow.put(rowKey, rows);
            }
            rows.add(r);
        }
    }


    @Override
    public void postExecute(BlockEnvironment env)
            throws Exception
    {
        // Note that _rightRowsFound is only used by RIGHT and OUTER joins, so below we can safely refer to
        // the _inputRight
        if (_rightRowsFound.size() > 0) {
            Data output = new Data();
            // Recreate the structure of the output data from the last column map
            for (ColumnEntry entry : _lastColumnMap) {
                entry.out = entry.out.getEmptyCopy();
                output.addColumn(entry.out);
            }

            // Sort the rows and add RIGHT.size() to make the following loop working for the last range (lastFound--size)
            Collections.sort(_rightRowsFound);
            _rightRowsFound.add(_inputRight.getLargestRows());

            // Add missing LEFTs and RIGHTs according to the last column map
            int start = 0;
            for (Integer foundRight : _rightRowsFound) {
                for (int rightRow = start; rightRow < foundRight; rightRow++) {
                    for (ColumnEntry entry : _lastColumnMap) {
                        if (entry.B != null) {
                            entry.out.appendObjectValue(entry.B.getObjectValue(rightRow));
                        } else if (entry.A != null) {
                            entry.out.appendObjectValue(MissingValue.get());
                        } else {
                            throw new Exception("Internal error: input column not set.");
                        }
                    }
                }
                start = foundRight + 1;
            }

            env.getExecutionService().setOutputDataSet(Output_AB, output);
        }
    }


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception
    {
        Data inputLeft = inputs.getInputDataSet(Input_A);

        Data selectedLeft = new Data();
        // Note that the order of the columns must match the JOINING_COLUMNS property
        for (String[] stringPair : _joiningColumnNames) {
            selectedLeft.addColumn(_selectColumn(inputLeft, stringPair[0]));
        }

        if (!_inputValidated) {
            _validateInput(selectedLeft, _selectedRight);
            _inputValidated = true;
        }

        //if (selectedLeft.getColumnCount() < 1) {
        //    throw new IllegalArgumentException("Invalid property: " + Prop_JOINING_COLUMNS + ". No columns found in input '" + Input_A + "'");
        //}
        //if (_selectedRight.getColumnCount() < 1) {
        //    throw new IllegalArgumentException("Invalid property: " + Prop_JOINING_COLUMNS + ". No columns found in input '" + Input_B + "'");
        //}

        Data output = new Data();
        String[] outputColumnRefs = env.getStringListProperty(Prop_OUTPUT_COLUMNS);
        if (outputColumnRefs.length > 0) {
            _lastColumnMap = _constructColumnMap(outputColumnRefs, inputLeft, _inputRight, selectedLeft, _selectedRight, output, null);
        } else {
            _lastColumnMap = _constructColumnMap(inputLeft, _nonSelectedRight, selectedLeft, _selectedRight, output, null);
        }

        switch (env.getStringProperty(Prop_JOIN_TYPE, "")) {
            case _JOIN_INNER:
                _innerJoin(selectedLeft, _lastColumnMap);
                break;
            case _JOIN_OUTER:
                _outerJoin(selectedLeft, _lastColumnMap);
                break;
            case _JOIN_LEFT:
                _leftJoin(selectedLeft, _lastColumnMap);
                break;
            case _JOIN_RIGHT:
                _rightJoin(selectedLeft, _lastColumnMap);
                break;
        }

        outputs.setOutputDataSet(Output_AB, output);
    }


    private void _outerJoin(Data leftChunk, ArrayList<ColumnEntry> outputColumnMap)
            throws Exception
    {
        int rowsNo = leftChunk.getLargestRows();
        for (int leftRow = 0; leftRow < rowsNo; leftRow++) {
            String key = _computeKey(leftChunk, leftRow);
            ArrayList<Integer> rightRows = _keyToRightRow.get(key);
            if (rightRows != null) {
                // Match found: copy all Left x Right rows combinations into the output
                for (int rightRow : rightRows) {
                    for (ColumnEntry entry : outputColumnMap) {
                        if (entry.A != null) {
                            entry.out.appendObjectValue(entry.A.getObjectValue(leftRow));
                        } else if (entry.B != null) {
                            entry.out.appendObjectValue(entry.B.getObjectValue(rightRow));
                        } else {
                            throw new Exception("Internal error: input column not set.");
                        }
                    }
                }

                // Mark rightRows as found -- non-found right rows will be added at the end in postExecute.
                _rightRowsFound.addAll(rightRows);

            } else {
                // Match not found: copy all Left entries and add Missing for the right
                for (ColumnEntry entry : outputColumnMap) {
                    if (entry.A != null) {
                        entry.out.appendObjectValue(entry.A.getObjectValue(leftRow));
                    } else if (entry.B != null) {
                        entry.out.appendObjectValue(MissingValue.get());
                    } else {
                        throw new Exception("Internal error: input column not set.");
                    }
                }
            }
        }
    }


    private void _leftJoin(Data leftChunk, ArrayList<ColumnEntry> outputColumnMap)
            throws Exception
    {
        int rowsNo = leftChunk.getLargestRows();
        for (int leftRow = 0; leftRow < rowsNo; leftRow++) {
            String key = _computeKey(leftChunk, leftRow);
            ArrayList<Integer> rightRows = _keyToRightRow.get(key);
            if (rightRows != null) {
                // Match found: copy all Left x Right rows combinations into the output
                for (int rightRow : rightRows) {
                    for (ColumnEntry entry : outputColumnMap) {
                        if (entry.A != null) {
                            entry.out.appendObjectValue(entry.A.getObjectValue(leftRow));
                        } else if (entry.B != null) {
                            entry.out.appendObjectValue(entry.B.getObjectValue(rightRow));
                        } else {
                            throw new Exception("Internal error: input column not set.");
                        }
                    }
                }
            } else {
                // Match not found: copy all Left entries and add Missing for the right
                for (ColumnEntry entry : outputColumnMap) {
                    if (entry.A != null) {
                        entry.out.appendObjectValue(entry.A.getObjectValue(leftRow));
                    } else if (entry.B != null) {
                        entry.out.appendObjectValue(MissingValue.get());
                    } else {
                        throw new Exception("Internal error: input column not set.");
                    }
                }
            }
        }
    }


    private void _rightJoin(Data leftChunk, ArrayList<ColumnEntry> outputColumnMap)
            throws Exception
    {
        int rowsNo = leftChunk.getLargestRows();
        for (int leftRow = 0; leftRow < rowsNo; leftRow++) {
            String key = _computeKey(leftChunk, leftRow);
            ArrayList<Integer> rightRows = _keyToRightRow.get(key);
            if (rightRows != null) {
                // Match found: copy all Left x Right rows combinations into the output
                for (int rightRow : rightRows) {
                    for (ColumnEntry entry : outputColumnMap) {
                        if (entry.A != null) {
                            entry.out.appendObjectValue(entry.A.getObjectValue(leftRow));
                        } else if (entry.B != null) {
                            entry.out.appendObjectValue(entry.B.getObjectValue(rightRow));
                        } else {
                            throw new Exception("Internal error: input column not set.");
                        }
                    }
                }

                // Mark rightRows as found -- non-found rows will be added at the end in postExecute.
                _rightRowsFound.addAll(rightRows);
            }
        }
    }


    private void _innerJoin(Data leftChunk, ArrayList<ColumnEntry> outputColumnMap)
            throws Exception
    {
        int rowsNo = leftChunk.getLargestRows();
        for (int leftRow = 0; leftRow < rowsNo; leftRow++) {
            String key = _computeKey(leftChunk, leftRow);
            ArrayList<Integer> rightRows = _keyToRightRow.get(key);
            if (rightRows != null) {
                // Match found: copy all Left x Right rows combinations into the output
                for (int rightRow : rightRows) {
                    for (ColumnEntry entry : outputColumnMap) {
                        if (entry.A != null) {
                            entry.out.appendObjectValue(entry.A.getObjectValue(leftRow));
                        } else if (entry.B != null) {
                            entry.out.appendObjectValue(entry.B.getObjectValue(rightRow));
                        } else {
                            throw new Exception("Internal error: input column not set.");
                        }
                    }
                }
            } // It's INNER join -> skip missing hits
        }
    }


    private int _validateInput(Data selectedLeft, Data selectedRight)
            throws IllegalArgumentException
    {
        assert (selectedLeft.getColumnCount() == selectedRight.getColumnCount());
        assert (selectedLeft.getColumnCount() > 0);

        int colNo = selectedLeft.getColumnCount();
        // Check that types of the selected LEFT and RIGHT columns match
        for (int c = 0; c < colNo; c++) {
            if (!selectedLeft.column(c).getDataType().equals(selectedRight.column(c).getDataType())) {
                throw new IllegalArgumentException(String.format(
                        "Type mismatch between column %s = %s and %s = %s",
                        selectedLeft.column(c).getName(), selectedLeft.column(c).getDataType().getSimpleName(),
                        selectedRight.column(c).getName(), selectedRight.column(c).getDataType().getSimpleName()));
            }
        }

        return colNo;
    }


    private static String _computeKey(Data columnSet, int row)
    {
        StringBuilder key = new StringBuilder();
        for (Column c : columnSet) {
            Object value = c.getObjectValue(row);
            if (value == null) {
                key.append(MissingValue.MISSING_VALUE_REPRESENTATION);
            } else {
                key.append(value.toString());
            }
        }
        return key.toString();
    }


    private static Column _selectColumn(Data columnSet, String columnName)
            throws DataException
    {
        if (columnName.startsWith("#")) {
            // First, always try to match columns by name; even if they start with #
            try {
                return columnSet.column(columnName);
            } catch (DataException x) {
                // If the column name cannot be found, let's check whether it's in the #NUMBER format
                try {
                    return columnSet.column(Integer.parseInt(columnName.substring(1)));
                } catch (NumberFormatException xx) {
                    // It's not the #NUMBER format, so re-throw the original data exception.
                    throw x;
                }
            }
        } else {
            return columnSet.column(columnName);
        }
    }


    private Data _extractNonSelectedColumns(Data allColumns, Data selectedColumns)
            throws DataException
    {
        Data output = new Data();

        for (Column c : allColumns) {
            if (_containsColumn(selectedColumns, c) == -1) {
                output.addColumn(c);
            }
        }

        return output;
    }


    private static ArrayList<ColumnEntry> _constructColumnMap(Data set_A, Data set_B, Data selected_A, Data selected_B, Data outputData, ArrayList<ColumnEntry> outputMap)
            throws DataException
    {
        if (outputMap == null) {
            outputMap = new ArrayList<>();
        }

        // First add all from A
        if (set_A != null) {
            int cols = set_A.getColumnCount();
            for (int c = 0; c < cols; c++) {
                ColumnEntry entry = new ColumnEntry();
                entry.A = set_A.column(c);
                int B_col = _containsColumn(selected_A, entry.A);
                if (B_col >= 0 && selected_B != null) {
                    entry.B = selected_B.column(B_col);
                }
                entry.out = entry.A.getEmptyCopy();
                outputData.addColumn(entry.out);
                outputMap.add(entry);
            }
        }

        // Then add all from B
        if (set_B != null) {
            int cols = set_B.getColumnCount();
            for (int c = 0; c < cols; c++) {
                ColumnEntry entry = new ColumnEntry();
                entry.B = set_B.column(c);
                int A_col = _containsColumn(selected_B, entry.B);
                if (A_col >= 0 && selected_A != null) {
                    entry.A = selected_A.column(A_col);
                }
                entry.out = entry.B.getEmptyCopy();
                outputData.addColumn(entry.out);
                outputMap.add(entry);
            }
        }

        return outputMap;
    }


    private static ArrayList<ColumnEntry> _constructColumnMap(String[] columnRefs, Data set_A, Data set_B, Data selected_A, Data selected_B, Data outputData, ArrayList<ColumnEntry> outputMap)
            throws DataException
    {
        if (outputMap == null) {
            outputMap = new ArrayList<>();
        }

        ColumnPicker picker = new ColumnPicker();
        picker.setCopyData(false);

        for (String columnRef : columnRefs) {
            if (columnRef.startsWith(Selector.A)) {
                String selector = columnRef.substring(Selector.A.length());
                if (Selector.Star.equals(selector)) {
                    _constructColumnMap(set_A, null, selected_A, selected_B, outputData, outputMap);
                } else {
                    picker.configure(selector);
                    ColumnEntry entry = new ColumnEntry();
                    entry.A = picker.pickColumn(set_A);
                    int B_col = _containsColumn(selected_A, entry.A);
                    if (B_col >= 0) {
                        entry.B = selected_B.column(B_col);
                    }
                    entry.out = entry.A.getEmptyCopy();
                    outputData.addColumn(entry.out);
                    outputMap.add(entry);
                }
            } else if (columnRef.startsWith(Selector.B)) {
                String selector = columnRef.substring(Selector.B.length());
                if (Selector.Star.equals(selector)) {
                    _constructColumnMap(null, set_B, selected_A, selected_B, outputData, outputMap);
                } else {
                    picker.configure(selector);
                    ColumnEntry entry = new ColumnEntry();
                    entry.B = picker.pickColumn(set_B);
                    int A_col = _containsColumn(selected_B, entry.B);
                    if (A_col >= 0) {
                        entry.A = selected_A.column(A_col);
                    }
                    entry.out = entry.B.getEmptyCopy();
                    outputData.addColumn(entry.out);
                    outputMap.add(entry);
                }
            } else {
                throw new IllegalArgumentException("Invalid column reference in property " + Prop_OUTPUT_COLUMNS + ": " + columnRef);
            }
        }

        return outputMap;
    }


    private static int _containsColumn(Data dataSet, Column column)
    {
        int colsNo = dataSet.getColumnCount();
        for (int c = 0; c < colsNo; c++) {
            if (dataSet.column(c) == column) {
                return c;
            }
        }

        return -1;
    }
}

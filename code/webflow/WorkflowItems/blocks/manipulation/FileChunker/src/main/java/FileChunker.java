/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;


public class FileChunker extends CloudDataProcessorService
{
    private long lineCount = 0;


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {

        int startRow = getEditableProperties().intValue("StartRow", 1);
        int chunkSize = getEditableProperties().intValue("ChunkSize", 1000);
        boolean allowShortChunks = getEditableProperties().booleanValue("AllowShortChunks", true);
        String baseChunkName = getEditableProperties().stringValue("BaseChunkName", "chunk");
        String chunkFileExtension = getEditableProperties().stringValue("ChunkFileExtension", ".csv");


        FileWrapper inputData = (FileWrapper)getInputData("input-data");
        if (inputData.getFileCount() != 1) {
            throw new Exception("FileChunker only operates on a single input file.");
        }

        File inputFile = inputData.getFile(0);
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));

        String line;
        File currentChunk;
        int chunkCount = 0;
        int currentChunkSize;
        
        File tmpDir = createTempDir("FileChunker-", "", new File("."));
        FileWrapper chunks = new FileWrapper(tmpDir);
        FileWrapper excessData = new FileWrapper(tmpDir);

        while ((line = reader.readLine()) != null) {
            lineCount++;
            if (lineCount >= startRow) {
                // In main data section
                currentChunk = new File(tmpDir, baseChunkName + chunkCount + chunkFileExtension);
                chunkCount++;
                currentChunkSize = processChunk(reader, line, chunkSize, currentChunk);
                if (allowShortChunks) {
                    chunks.addFile(currentChunk, false);
                } else {
                    if(currentChunkSize >= chunkSize){
                        chunks.addFile(currentChunk, false);
                    }
                    else
                    {
                        excessData.addFile(currentChunk, false);
                    }
                }
            }
        }


        setOutputData("file-chunks", chunks);
        setOutputData("excess-data", excessData);
    }


    private int processChunk(BufferedReader reader, String topLine, int chunkSize, File chunkFile)
    throws Exception
    {
        PrintWriter writer = null;
        try {
            int chunkRows = 1;
            writer = new PrintWriter(chunkFile);
            writer.println(topLine);
            String line;
            while (chunkRows < chunkSize && (line = reader.readLine()) != null) {
                writer.println(line);
                chunkRows++;
            }
            return chunkRows;
        } catch (Exception e) {
            throw e;
        } finally {
            if (writer!=null) {
                writer.close();
            }
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.api.*;
import com.connexience.server.workflow.api.rpc.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.xmlstorage.DocumentRecordWrapper;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import com.connexience.server.workflow.xmlstorage.WorkflowLockWrapper;
import org.pipeline.core.data.*;

import java.io.*;

import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.util.DataWrapper;
import org.pipeline.core.xmlstorage.XmlDataStore;

import java.util.*;

public class CuffMerge extends CloudDataProcessorService
{

  /**
   * This method is call when a service is about to be started. It is called
   * once regardless of whether or not the service is streaming data. Code
   * that is needed to set up information that needs to be preserved over
   * multiple chunks should be executed here.
   */
  public void executionAboutToStart() throws Exception
  {
  }

  /**
   * This is the main service execution routine. It is called once if the
   * service has not been configured to accept streaming data or once for each
   * chunk of data if the service has been configured to accept data streams
   */
  public void execute() throws Exception
  {
    API api = getApiLink();
    FileWrapper refGeneWrapper = (FileWrapper) getInputData("referenceGene");
    FileWrapper faWrapper = (FileWrapper) getInputData("faFile");
    String pValue = getEditableProperties().stringValue("p", "8");

    Data cufflinksInvocationsIds = getInputDataSet("invocationIds");
    Column col = cufflinksInvocationsIds.column(0);

    File workingDir = getWorkingDirectory();

    String assemblies = "";
    for (int i = 0; i < col.getRows(); i++)
    {
      String documentId = col.getStringValue(i);
      IFolder thinvocation = api.getFolder(documentId);
      for (IObject obj : api.getFolderContents(thinvocation))
      {
        if (obj instanceof IDocument)
        {
          IDocument doc = (IDocument) obj;
          if (doc.getName().equalsIgnoreCase("transcripts.gtf"))
          {

            String nonClashingName = findNonClashingFileName(doc.getName());
            System.out.println("nonClashingName = " + nonClashingName);

            File tmpFile = new File(workingDir, nonClashingName);
            FileOutputStream stream = new FileOutputStream(tmpFile);
            api.download(doc, stream);
            stream.flush();
            stream.close();
            assemblies += "./" + nonClashingName + "\n";
          }
        }
      }
    }

    File assembliesFile = new File("assemblies.txt");
    BufferedWriter bw = new BufferedWriter(new FileWriter(assembliesFile));
    bw.write(assemblies);
    bw.close();

    String cuffMergeBin = "/Users/nsjw7/Misc/cufflinks-2.0.2.OSX_x86_64/cuffmerge";


    List<String> command = new ArrayList<String>();
    command.add(cuffMergeBin);
    command.add("-g");
    command.add(refGeneWrapper.getFile(0));
    command.add("-s");
    command.add(faWrapper.getFile(0));
    command.add("-p");
    command.add(pValue);
    command.add("-o");
    command.add(".");
    command.add("assemblies.txt");


    //Call TopHat
    String[] commandArray = command.toArray(new String[command.size()]);
    ProcessBuilder pb = new ProcessBuilder(commandArray);
    Process process = pb.start();


    //Redirect the STD OUT and ERR so that they are output to the workflow block OUT and ERR
    BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
    BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
    String errline, outline;
    while ((outline = br.readLine()) != null)
    {
      System.out.println(outline);
    }

    while ((errline = errReader.readLine()) != null)
    {
      System.err.println(errline);
    }

    //Wait for the process to finish
    try
    {
      int exitValue = process.waitFor();
      System.out.println("\n\nExit Value is " + exitValue);
      if (exitValue != 0)
      {
        throw new Exception("Non zero exit value: " + exitValue);
      }
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }


    //Get the results
    FileWrapper outputWrapper = new FileWrapper();
    outputWrapper.addFile("merged.gtf");
    setOutputData("merged", outputWrapper);

  }

  /**
   * All of the data has been passed through the service. Any clean up code
   * should be placed here
   */
  public void allDataProcessed() throws Exception
  {
  }
}
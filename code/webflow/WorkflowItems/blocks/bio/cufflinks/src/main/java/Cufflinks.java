/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Cufflinks extends CloudDataProcessorService
{

  /**
   * This method is call when a service is about to be started. It is called once
   * regardless of whether or not the service is streaming data. Code that is
   * needed to set up information that needs to be preserved over multiple
   * chunks should be executed here.
   */
  public void executionAboutToStart() throws Exception
  {

  }

  /**
   * This is the main service execution routine. It is called once if the service
   * has not been configured to accept streaming data or once for each chunk of
   * data if the service has been configured to accept data streams
   */
  public void execute() throws Exception
  {
    FileWrapper accepted_hits = (FileWrapper) getInputData("acceptedhits");

    String pValue = getEditableProperties().stringValue("p", "8");


//    String tophatBin = "/home/ubuntu/biotools/tophat-2.0.4.Linux_x86_64/tophat2";
    String cufflinksBin = "/Users/nsjw7/Misc/cufflinks-2.0.2.OSX_x86_64/cufflinks";

    List<String> command = new ArrayList<String>();
    command.add(cufflinksBin);
    command.add("-p");
    command.add(pValue);
    command.add("-o");
    command.add(".");

    command.add(accepted_hits.getFile(0));

    //Call Cufflinks
    String[] commandArray = command.toArray(new String[command.size()]);
    ProcessBuilder pb = new ProcessBuilder(commandArray);
    Process process = pb.start();


    //Redirect the STD OUT and ERR so that they are output to the workflow block OUT and ERR
    BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
    BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
    String errline, outline;
    while ((outline = br.readLine()) != null)
    {
      System.out.println(outline);
    }

    while ((errline = errReader.readLine()) != null)
    {
      System.err.println(errline);
    }

    //Wait for the process to finish
    try
    {
      int exitValue = process.waitFor();
      System.out.println("\n\nExit Value is " + exitValue);
      if (exitValue != 0)
      {
        throw new ConnexienceException("Non zero exit value: " + exitValue);
      }
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }


    //Get the results
    FileWrapper outputWrapper = new FileWrapper();
    outputWrapper.addFile("transcripts.gtf");
    setOutputData("transcripts", outputWrapper);
  }

//  /*
//  * Add a directory to a datawrapper.  Will recursively add subdirectories
//  * */
//  FileWrapper addDirToFileWrapper(FileWrapper wrapper, File dir, String base)
//  {
//    FilenameFilter filter = new FilenameFilter()
//    {
//      public boolean accept(File dir, String name)
//      {
//        return !name.startsWith(".");
//      }
//    };
//
//
//    File[] children = dir.listFiles(filter);
//    if (children == null)
//    {
//      // Either dir does not exist or is not a directory
//    }
//    else
//    {
//      for (File file : children)
//      {
//        if (!file.isDirectory())
//        {
//          String thisBase;
//          if (base.equals(""))
//          {
//            thisBase = dir.getName();
//          }
//          else
//          {
//            thisBase = base + File.separator + dir.getName();
//          }
//
//          String fileToAdd = thisBase + File.separator + file.getName();
//          System.out.println("Adding: " + fileToAdd);
//
//          wrapper.addFile(fileToAdd);
//        }
//        else
//        {
//          String thisBase = dir.getName();
//          addDirToFileWrapper(wrapper, file, thisBase);
//        }
//      }
//    }
//    return wrapper;
//  }
//

  /**
   * All of the data has been passed through the service. Any clean up code
   * should be placed here
   */
  public void allDataProcessed() throws Exception
  {

  }
}
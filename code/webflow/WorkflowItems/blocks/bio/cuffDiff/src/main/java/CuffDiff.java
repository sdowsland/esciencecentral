/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.api.API;
import com.connexience.server.api.IDocument;
import com.connexience.server.api.IFolder;
import com.connexience.server.api.IObject;
import com.connexience.server.util.CommandRunner;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class CuffDiff extends CloudDataProcessorService
{

  /**
   * This method is call when a service is about to be started. It is called
   * once regardless of whether or not the service is streaming data. Code
   * that is needed to set up information that needs to be preserved over
   * multiple chunks should be executed here.
   */
  public void executionAboutToStart() throws Exception
  {
  }

  /**
   * This is the main service execution routine. It is called once if the
   * service has not been configured to accept streaming data or once for each
   * chunk of data if the service has been configured to accept data streams
   */
  public void execute() throws Exception
  {
    API api = getApiLink();

    Data tophatInvocationIds = getInputDataSet("invocationIds");
    FileWrapper mergedWrapper = (FileWrapper) getInputData("mergedGtf");
    FileWrapper faWrapper = (FileWrapper) getInputData("faFile");

    String pValue = getEditableProperties().stringValue("p", "8");
    String lValue = getEditableProperties().stringValue("L", "C1,C2");

    Column col = tophatInvocationIds.column(0);

    File workingDir = getWorkingDirectory();
    Vector<String> acceptedHits = new Vector<String>();
    for (int i = 0; i < col.getRows(); i++)
    {
      String documentId = col.getStringValue(i);
      IFolder thinvocation = api.getFolder(documentId);
      for (IObject obj : api.getFolderContents(thinvocation))
      {
        if (obj instanceof IDocument)
        {
          IDocument doc = (IDocument) obj;
          if (doc.getName().equalsIgnoreCase("accepted_hits.bam"))
          {

            String nonClashingName = findNonClashingFileName(doc.getName());
            System.out.println("nonClashingName = " + nonClashingName);

            File tmpFile = new File(workingDir, nonClashingName);
            FileOutputStream stream = new FileOutputStream(tmpFile);
            api.download(doc, stream);
            stream.flush();
            stream.close();
            acceptedHits.add(nonClashingName);
          }
        }
      }
    }

//    String cuffDiffBin = "/Users/nsjw7/Misc/cufflinks-2.0.2.OSX_x86_64/cuffdiff";
//
//
//    List<String> command = new ArrayList<String>();
//    command.add(cuffDiffBin);
//    command.add("-v");
//    command.add("-b");
//    command.add(faWrapper.getFile(0));
//    command.add("-p");
//    command.add(pValue);
//    command.add("-o");
//    command.add("diff_out");
//    command.add("-L");
//    command.add(lValue);
//    command.add("-u");
//    command.add(mergedWrapper.getFile(0));
//
//    String c = "";
//    int three = 1;
//    for (String acceptedHit : acceptedHits)
//    {
//      c += "./" + acceptedHit + ",";
//      if (three % 3 == 0) //todo: fix
//      {
//        c = c.substring(0, c.length() - 1);
//        command.add(c);
//        c = "";
//      }
//      three++;
//    }
//
//
//    //Call TopHat
//    String[] commandArray = command.toArray(new String[command.size()]);
//
//    for (String aCommandArray : commandArray)
//    {
//      System.out.print(aCommandArray + " ");
//    }
//    ProcessBuilder pb = new ProcessBuilder(commandArray);
//    Process process = pb.start();

    String command2 = "/Users/nsjw7/Misc/cufflinks-2.0.2.OSX_x86_64/cuffdiff -v -b genome.fa -p 8 -o diff_out -L C1,C2 -u merged.gtf ./accepted_hits.bam,./accepted_hits1.bam,./accepted_hits2.bam ./accepted_hits3.bam,./accepted_hits4.bam,./accepted_hits5.bam";
    CommandRunner commandRunner = new CommandRunner();
    commandRunner.run(command2);




//    File log = new File(getWorkingDirectory() + File.separator + "cuffdifflog.txt");
//    BufferedWriter logWriter = new BufferedWriter(new FileWriter(log));
//
//    //Redirect the STD OUT and ERR so that they are output to the workflow block OUT and ERR
//    BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
//    BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//    String errline, outline;
//    while ((outline = br.readLine()) != null)
//    {
//      System.out.println(outline);
//      logWriter.write("out " + outline);
//    }
//
//    while ((errline = errReader.readLine()) != null)
//    {
//      System.err.println(errline);
//      logWriter.write("err " + errline);
//    }
//
//    //Wait for the process to finish
//    try
//    {
//      int exitValue = process.waitFor();
//      System.out.println("\n\nExit Value is " + exitValue);
//      if (exitValue != 0)
//      {
//        throw new Exception("Non zero exit value: " + exitValue);
//      }
//    }
//    catch (InterruptedException e)
//    {
//      e.printStackTrace();
//    }
//    finally {
//      br.close();
//      errReader.close();
//      logWriter.close();
//    }


    //Get the results
//    File dir = new File("diff_out");
//    //This is necessary as the e-SC server doesn't yet have the FileWrapper#addDir method
//    FileWrapper outputWrapper = new FileWrapper();
//    outputWrapper.addDir(dir);
//    setOutputData("outputFiles", outputWrapper);
//
  //Get the results
    File dir = new File("diff_out");
    //This is necessary as the e-SC server doesn't yet have the FileWrapper#addDir method
    //Can be refactored to     outputWrapper.addDir(dir);
    FileWrapper outputWrapper = addDirToFileWrapper(new FileWrapper(), dir, "");

    setOutputData("diff_merge", outputWrapper);
  }

  /*
  * Add a directory to a datawrapper.  Will recursively add subdirectories
  * */
  FileWrapper addDirToFileWrapper(FileWrapper wrapper, File dir, String base)
  {
    FilenameFilter filter = new FilenameFilter()
    {
      public boolean accept(File dir, String name)
      {
        return !name.startsWith(".");
      }
    };


    File[] children = dir.listFiles(filter);
    if (children == null)
    {
      // Either dir does not exist or is not a directory
    }
    else
    {
      for (File file : children)
      {
        if (!file.isDirectory())
        {
          String thisBase;
          if (base.equals(""))
          {
            thisBase = dir.getName();
          }
          else
          {
            thisBase = base + File.separator + dir.getName();
          }

          String fileToAdd = thisBase + File.separator + file.getName();
          System.out.println("Adding: " + fileToAdd);

          wrapper.addFile(fileToAdd);
        }
        else
        {
          String thisBase = dir.getName();
          addDirToFileWrapper(wrapper, file, thisBase);
        }
      }
    }
    return wrapper;
  }


  /**
   * All of the data has been passed through the service. Any clean up code
   * should be placed here
   */
  public void allDataProcessed() throws Exception
  {
  }
}
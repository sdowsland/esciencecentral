/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.api.*;
import com.connexience.server.workflow.api.rpc.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.xmlstorage.DocumentRecordWrapper;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import com.connexience.server.workflow.xmlstorage.WorkflowLockWrapper;
import org.pipeline.core.data.*;

import java.io.*;

import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.util.DataWrapper;

import java.util.*;

public class ParallelCufflinks extends CloudDataProcessorService
{

  /**
   * This method is call when a service is about to be started. It is called
   * once regardless of whether or not the service is streaming data. Code
   * that is needed to set up information that needs to be preserved over
   * multiple chunks should be executed here.
   */
  public void executionAboutToStart() throws Exception
  {
  }

  /**
   * This is the main service execution routine. It is called once if the
   * service has not been configured to accept streaming data or once for each
   * chunk of data if the service has been configured to accept data streams
   */
  public void execute() throws Exception
  {
    boolean waitForWorkflow = getEditableProperties().booleanValue("WaitForWorkflow", true);
    String targetBlockName = getEditableProperties().stringValue("TargetBlockName", "input");
    String targetPropertyName = getEditableProperties().stringValue("TargetPropertyName", "Source");

    API api = getApiLink();

    Data tophatInvocationIds = getInputDataSet("invocationIds");
    Column col = tophatInvocationIds.column(0);

    DocumentRecordWrapper wfDoc = (DocumentRecordWrapper) getEditableProperties().xmlStorableValue("WorkflowFile");

    IWorkflow wf = api.getWorkflow(wfDoc.getId());
    IWorkflowParameterList params;

    Data invocationIds = new Data();
    invocationIds.addColumn(new StringColumn("InvocationID"));
    IWorkflowInvocation invocation;

    if (waitForWorkflow)
    {
      WorkflowLockWrapper lock = createWorkflowLock();
      for (int i = 0; i < col.getRows(); i++)
      {
        String documentId = col.getStringValue(i);
        IFolder thinvocation = api.getFolder(documentId);
        for(IObject obj : api.getFolderContents(thinvocation))
        {
          if(obj instanceof IDocument)
          {
            IDocument doc = (IDocument) obj;
            if(doc.getName().equalsIgnoreCase("accepted_hits.bam"))
            {
              params = (IWorkflowParameterList) api.createObject(IWorkflowParameterList.XML_NAME);
              params.add(targetBlockName, targetPropertyName, doc.getId());
              invocation = executeWorkflowWithLock(wf, params, lock, wf.getName() + ": " + doc.getName());
              invocationIds.column(0).appendStringValue(invocation.getInvocationId());
            }
          }
        }
      }
    }
    else
    {
      for (int i = 0; i < col.getRows(); i++)
      {
        String documentId = col.getStringValue(i);
        IDocument doc = api.getDocument(documentId);

        params = (IWorkflowParameterList) api.createObject(IWorkflowParameterList.XML_NAME);
        params.add(targetBlockName, targetPropertyName, doc.getId());
        invocation = executeWorkflow(wf, params, wf.getName() + ": " + doc.getName());
        invocationIds.column(0).appendStringValue(invocation.getInvocationId());
      }
    }

    setOutputDataSet("invocation-ids", invocationIds);

//    // Create a transfer folder for copying data
//    IFolder transferFolder = (IFolder) api.createObject(IFolder.XML_NAME);
//    transferFolder.setName(transferFolderName);
//    transferFolder.setContainerId(workingFolder.getId());
//    transferFolder = api.saveFolder(transferFolder);
//
//    int cMin = 1;
//    int cMax = 2;
//    int rMin = 1;
//    int rMax = 3;
//
//    IDocument uploadDoc;
//    ArrayList<IFolder> transferFolders = new ArrayList<IFolder>();
//
//    for (int c = cMin; c <= cMax; c++)
//    {
//      for (int r = rMin; r <= rMax; r++)
//      {
//        String readsFolderName = "C" + c + "_R" + r;
//        IFolder readsFolder = (IFolder) api.createObject(IFolder.XML_NAME);
//        readsFolder.setName(readsFolderName);
//        readsFolder.setContainerId(transferFolder.getId());
//        readsFolder = api.saveFolder(readsFolder);
//
//        transferFolders.add(readsFolder);
//
//        for (int i = 0; i < inputFiles.getFileCount(); i++)
//        {
//          File sourceFile = new File(getWorkingDirectory(), inputFiles.getFile(i));
//          String readName = inputFiles.getFile(i);
//          if (readName.startsWith(readsFolderName))
//          {
//            //upload file to this folder
//            uploadDoc = (IDocument) api.createObject(IDocument.XML_NAME);
//            uploadDoc.setName(inputFiles.getFile(i));
//            uploadDoc = api.saveDocument(readsFolder, uploadDoc);
//            api.upload(uploadDoc, new FileInputStream(sourceFile));
//          }
//        }
//      }
//    }


//
//    // Upload all of the source files
//    for (int i = 0; i < inputFiles.getFileCount(); i++)
//    {
//      sourceFile
//      if (sourceFile.exists())
//      {
//        uploadDoc = (IDocument) api.createObject(IDocument.XML_NAME);
//        uploadDoc.setName(inputFiles.getFile(i));
//        uploadDoc = api.saveDocument(transferFolder, uploadDoc);
//        api.upload(uploadDoc, new FileInputStream(sourceFile));
//        transferDocuments.add(uploadDoc);
//      }
//    }


  }

  /**
   * All of the data has been passed through the service. Any clean up code
   * should be placed here
   */
  public void allDataProcessed() throws Exception
  {
  }
}
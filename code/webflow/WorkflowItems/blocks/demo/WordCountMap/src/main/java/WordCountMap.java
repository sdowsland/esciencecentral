
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WordCountMap extends CloudDataProcessorService {


    public void execute() throws Exception {

        FileWrapper inputFiles = (FileWrapper) getInputData("input-files");

        Map<String, Integer> counts = new HashMap<String, Integer>();

        for(int i = 0; i < inputFiles.getFileCount(); i++)
        {
            File f = inputFiles.getFile(i);
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while((line = br.readLine()) != null)
            {
                String[] words = line.split(" ");
                for(String word : words){
                    word = word.replaceAll("[^A-Za-z]", "").trim();

                    if(!word.equals(""))
                    {
                        addToMap(counts, word);
                    }
                }
            }

            br.close();
        }

        File outputFile = new File("countedWords.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
        for(String word : counts.keySet()){
            bw.write(word + ", " + counts.get(word) + "\n");
        }

        bw.flush();
        bw.close();

        FileWrapper outputFiles = new FileWrapper();
        outputFiles.addFile(outputFile);
        setOutputData("output-file", outputFiles);
    }

    private void addToMap(Map<String, Integer> counts, String word){
        if(counts.containsKey(word))
        {
            counts.put(word, counts.get(word) +1);
        }
        else{
            counts.put(word, 1);
        }
    }
}
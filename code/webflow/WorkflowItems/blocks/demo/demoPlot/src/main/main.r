
# This command simply copies input x to output y
title = getProperty("title")
xlabel = getProperty("xLabel")
ylabel = getProperty("yLabel")
chart = getProperty("chart")

data = lapply(inputFiles, read.csv2, header=FALSE)

png(filename=chart);
plot(data.matrix(t(data.frame(data))),
     main=title,
     xlab=xlabel,
     ylab=ylabel)
dev.off()




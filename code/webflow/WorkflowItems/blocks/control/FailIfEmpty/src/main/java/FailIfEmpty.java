/**
 * e-Science Central Copyright (C) 2008-2015 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.*;
import org.pipeline.core.data.*;

import java.io.File;
import java.util.List;


public class FailIfEmpty implements WorkflowBlock
{
    private final static String Prop_ERROR_MESSAGE = "Error Message";
    private final static String Prop_ERROR_DETAILS = "Error Details";
    private final static String Prop_FILE_INPUT_EMPTY = "Fail if input-files is empty";

    private final static String Input_DATA = "input-data";
    private final static String Input_FILES = "input-files";

    private final static String Output_DATA = "output-data";
    private final static String Output_FILES = "output-files";


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    { }

    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception
    {
        String errMsg = env.getStringProperty(Prop_ERROR_MESSAGE, "");
        boolean noInput = true;

        if (env.getExecutionService().isInputConnected(Input_DATA)) {
            Data inputData = inputs.getInputDataSet(Input_DATA);
            if (inputData.getLargestRows() == 0) {
                throw new Exception(errMsg);
            }
            outputs.setOutputDataSet(Output_DATA, inputData);
            noInput = false;
        }

        if (env.getExecutionService().isInputConnected(Input_FILES)) {
            List<File> inputFiles = inputs.getInputFiles(Input_FILES);
            System.out.println("input files size: " + inputFiles.size());
            if (env.getBooleanProperty(Prop_FILE_INPUT_EMPTY, false) && inputFiles.size() == 0) {
                if (env.getBooleanProperty(Prop_ERROR_DETAILS, false)) {
                    errMsg += "; Input " + Input_FILES + " contains no files.";
                }
                throw new Exception(errMsg);
            }

            for (File f : inputFiles) {
                if (f.length() == 0) {
                    if (env.getBooleanProperty(Prop_ERROR_DETAILS, false)) {
                        errMsg += "; File: " + f.getName() + " is empty";
                    }
                    throw new Exception(errMsg);
                }
            }

            outputs.setOutputFiles(Output_FILES, inputFiles);
            noInput = false;
        }

        if (noInput) {
            if (env.getBooleanProperty(Prop_ERROR_DETAILS,false)) {
                errMsg += "; No inputs connected.";
            }
            throw new Exception(errMsg);
        }
    }

    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    { }
}

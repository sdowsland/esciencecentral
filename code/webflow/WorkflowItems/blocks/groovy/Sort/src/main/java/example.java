import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.manipulation.RowExtractor;


public class example {
	public static List<String> SelectedColumnName=new ArrayList<>();
	public static List<String> SelectedSort=new ArrayList<>();
	public Data generate() throws IndexOutOfBoundsException, DataException{
		Data input = new Data();
		IntegerColumn Age = new IntegerColumn("Age");
		StringColumn Sex = new StringColumn("Sex");
		IntegerColumn ID = new IntegerColumn("ID");
		DoubleColumn Score= new DoubleColumn("Score");
		ID.nullifyToSize(10);
		Age.nullifyToSize(10);
		Score.nullifyToSize(10);
		Score.setDoubleValue(0, 60.7);
		Score.setDoubleValue(1, 66.8);
		Score.setDoubleValue(2, 73.8);
		Score.setDoubleValue(3, 74.6);
		Score.setDoubleValue(4, 80.88);
		Score.setDoubleValue(5, 81.82);
		Score.setDoubleValue(6, 84.74);
		Score.setDoubleValue(7, 90.6);
		Score.setDoubleValue(8, 97.77);
		Score.setDoubleValue(9, 100);
		ID.setIntValue(0, 1);
		ID.setIntValue(1,2);
		ID.setIntValue(2,3);
		ID.setIntValue(3, 4);
		ID.setIntValue(4, 5);
		ID.setIntValue(5, 6);
		ID.setIntValue(6, 7);
		ID.setIntValue(7, 8);
		ID.setIntValue(8, 9);
		ID.setIntValue(9, 10);
		Age.setIntValue(0, 34);
		Age.setIntValue(1, 3);
		Age.setIntValue(2, 12);
		Age.setIntValue(3, 54);
		Age.setIntValue(4, 2);
		Age.setIntValue(5, 31);
		Age.setIntValue(6, 20);
		Age.setIntValue(7, 27);
		Age.setIntValue(8, 30);
		Age.setIntValue(9, 26);
		Sex.insertObjectValue(0, "Male", true);
		Sex.insertObjectValue(1, "Female", true);
		Sex.insertObjectValue(2, "Female", true);
		Sex.insertObjectValue(3, "Male", true);
		Sex.insertObjectValue(4, "Male", true);
		Sex.insertObjectValue(5, "Female", true);
		Sex.insertObjectValue(6, "Male", true);
		Sex.insertObjectValue(7, "Female", true);
		Sex.insertObjectValue(8, "Male", true);
		Sex.insertObjectValue(9, "Female", true);
		input.addColumn(ID);
		input.addColumn(Sex);
		input.addColumn(Age);
		input.addColumn(Score);
		return input;
	}
	public List<Data> generateList(Data inputData) throws IndexOutOfBoundsException, DataException{
		List<Data> compareData=new ArrayList<>();
		RowExtractor rows=new RowExtractor(inputData);
		
		
		for(int i=0;i<inputData.getLargestRows();i++){
		Data compareRow=rows.extract(i);
		compareData.add(compareRow);
		}
		return compareData;
	}
	public Data Sort(String[][] conditionList,List<Data> compareData) throws IndexOutOfBoundsException, DataException{
		Data outputData=new Data();

		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		
		for(int i=0;i<conditionList.length;i++){
			String condition=conditionList[i][0];
			Matcher ColumnMatcher = ColumnPattern.matcher(condition);
			 final List<String> SelectedColumnName=new ArrayList<>();
				while (ColumnMatcher.find()) {
					String ColumnName = ColumnMatcher.group().replace("{", "")
							.replace("}", "");
					System.out.println(ColumnName);
					SelectedColumnName.add(ColumnName);
				} 
			
			String SortDirection="";
		
			if(!conditionList[i][1].isEmpty()){		
				SortDirection = conditionList[i][1];
			}
			else{
				SortDirection="ASC";
			}
			final String Groovyinput = condition.replace("{", "")
					.replace("}", "");
			System.out.println("Groovyinput: "+Groovyinput);
			System.out.println("SortDirection: "+SortDirection);
			if (SortDirection.equals("ASC")) {
				
				System.out.println("This is ASC sort Direction ");
				Collections.sort(compareData, new Comparator<Data>() {
					@Override
					public int compare(Data arg0, Data arg1) {
						Binding context_0=new Binding();
						for(String columName:SelectedColumnName){
							try {
								context_0.setVariable(columName, arg0.column(columName).getObjectValue(0));
							} catch (IndexOutOfBoundsException | DataException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						GroovyShell sh_0 = new GroovyShell(context_0);
						Object x = sh_0.evaluate(Groovyinput);
					
						System.out.println("X: "+x);
						Binding context_1=new Binding();
						for(String columName:SelectedColumnName){
							try {
								context_1.setVariable(columName, arg1.column(columName).getObjectValue(0));
							} catch (IndexOutOfBoundsException | DataException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						GroovyShell sh_1 = new GroovyShell(context_1);
						Object y = sh_1.evaluate(Groovyinput);
						System.out.println("Y: "+y);
				if(x instanceof BigDecimal&&y instanceof BigDecimal){
					BigDecimal value_1=(BigDecimal)x;
					BigDecimal value_2=(BigDecimal)y;
					System.out.println("BigDecimal: "+value_1.compareTo(value_2));
					return value_1.compareTo(value_2);
				}else if(x instanceof Double&&y instanceof Double){
					System.out.println("Double: "+ Double.compare((Double)x, (Double)y));
					return Double.compare((Double)x, (Double)y);
				}else if(x instanceof Long&&y instanceof Long){
					System.out.println("Long: "+ Long.compare((Long)x, (Long)y));
					return Long.compare((Long)x, (Long)y);
				}else {
					System.out.println("String: "+String.valueOf(x).compareTo( String.valueOf(y)));
					return String.valueOf(x).compareTo( String.valueOf(y));
				}
					}
				});
			} else if (SortDirection.equals("DESC")) {
				System.out.println("This is DESC sort Direction ");
				Collections.sort(compareData,
						Collections.reverseOrder(new Comparator<Data>() {
							@Override
							public int compare(Data o1, Data o2) {
								Binding context_0=new Binding();
								for(String columName:SelectedColumnName){
									try {
										context_0.setVariable(columName, o1.column(columName).getObjectValue(0));
									} catch (IndexOutOfBoundsException | DataException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								GroovyShell sh_0 = new GroovyShell(context_0);
								Object x = sh_0.evaluate(Groovyinput);
								System.out.println("X: "+x);
								Binding context_1=new Binding();
								for(String columName:SelectedColumnName){
									try {
										context_1.setVariable(columName, o2.column(columName).getObjectValue(0));
									} catch (IndexOutOfBoundsException | DataException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								GroovyShell sh_1 = new GroovyShell(context_1);
							
                                Object y = sh_1.evaluate(Groovyinput);
								System.out.println("Y: "+y);
								
								if(x instanceof BigDecimal&&y instanceof BigDecimal){
									BigDecimal value_1=(BigDecimal)x;
									BigDecimal value_2=(BigDecimal)y;
									System.out.println("BigDecimal: "+value_1.compareTo(value_2));
									return value_1.compareTo(value_2);
								}else if(x instanceof Double&&y instanceof Double){
									System.out.println("Double: "+ Double.compare((Double)x, (Double)y));
									return Double.compare((Double)x, (Double)y);
								}else if(x instanceof Long&&y instanceof Long){
									System.out.println("Long: "+ Long.compare((Long)x, (Long)y));
									return Long.compare((Long)x, (Long)y);
								}else {
									System.out.println("String: "+String.valueOf(x).compareTo( String.valueOf(y)));
									return String.valueOf(x).compareTo( String.valueOf(y));
								}
							}
						}));

			}
		}
		
	
		for (Data finalData : compareData) {
			if(outputData.getColumnCount()==0){
				outputData.addColumns(finalData.getColumnAray());
			}
			else{
			outputData.appendRows(finalData, true);
			}
		}
		return outputData;
	}
	public static void main(String[] args) throws DataException {
		example example=new example();
		final Data inputData=example.generate();
		List<Data> compareData=example.generateList(inputData);
		//String condition="def id={ID};def age={Age};Math.sqrt(age/id) and {Sex}[DESC]";
				String[][] conditionList={{"'Age='+{Age}","DESC"},{"{Age}/{ID}",""}};

			
		Data outputData=example.Sort( conditionList, compareData);
		System.out.println("Final result:");
		for (int i = 0; i < outputData.getColumnCount(); i++) {
			for (int j = 0; j < outputData.column(i).getRows(); j++) {
				System.out.println(outputData.column(i).getName() + ": "
						+ outputData.column(i).getObjectValue(j));
			}
		}
	
	}
	
}

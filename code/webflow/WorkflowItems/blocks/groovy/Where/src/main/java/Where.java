/**
 * e-Science Central Copyright (C) 2008-2015 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import groovy.lang.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlStorableDataObject;


public class Where implements WorkflowBlock
{
    private final static String Prop_CONDITION = "Condition";
    private final static String Prop_STATIC_COMPILATION = "Use Static Compilation";
    private final static String Prop_IS_DETERMINISTIC = "Is Condition Deterministic";
    private final static String Prop_OUTPUT_LIMIT = "Output Limit";
    private final static String Prop_REMAINDER_LIMIT = "Remainder Limit";
    private final static String Prop_USERPROP_1 = "UserProp1";
    private final static String Prop_USERPROP_2 = "UserProp2";
    private final static String Prop_USERPROPS = "UserProps";

    private final static String Input_A = "A";

    private final static String Output_DATA = "filtered-data";
    private final static String Output_REMAINDER = "remainder";

    private GroovyClassLoader _classLoader;
    private GroovyObject _conditionObj;

    private HashMap<String, String> _col2var;
    private ArrayList<String> _selectedColumnNames;
    private String _sanitizedCondition;
    private StringBuilder _scriptText;

    // Cache to avoid calling Groovy --> requires a deterministic condition.
    private HashMap<ArgumentArray, Boolean> _predicateValueMap;

    private long _totalRows;
    private long _totalChunks;

    private long _outSize;
    private long _outLimit;

    private long _remSize;
    private long _remLimit;

    private static String _sanitizeProps(BlockEnvironment env, String condition, StringBuilder scriptText)
    throws XmlStorageException
    {
        HashMap<String, String> prop2var = new HashMap<>();

        // Sanitize variable names for the block properties
        Pattern propPattern = Pattern.compile("\\$\\{\\{.*?\\}\\}");
        Matcher propMatcher = propPattern.matcher(condition);
        StringBuilder sanitized = new StringBuilder();
        int varCnt = 0;
        int lastMatchEnd = 0;
        while (propMatcher.find()) {
            String match = propMatcher.group();
            String propName = match.substring(3, match.length() - 2);
            String variableName = prop2var.get(propName);
            if (variableName ==  null) {
                variableName = "PVarP" + varCnt++;
                prop2var.put(propName, variableName);

                scriptText.append("    private static ");
                XmlDataObject prop = env.getExecutionService().getProperties().get(propName);
                switch (prop.getTypeLabel()) {
                    case "String":
                        scriptText.append("String ");
                        scriptText.append(variableName);
                        scriptText.append(" = '");
                        scriptText.append(prop.getValue().toString());
                        scriptText.append("'");
                        break;
                    case "Integer":
                    case "Long":
                    case "Double":
                    case "Boolean":
                        scriptText.append(prop.getTypeLabel());
                        scriptText.append(' ');
                        scriptText.append(variableName);
                        scriptText.append(" = ");
                        scriptText.append(prop.getValue().toString());
                        break;
                    case "XmlStorable":
                        XmlStorable storableValue = ((XmlStorableDataObject)prop).xmlStorableValue();
                        if (storableValue instanceof StringPairListWrapper) {
                            StringPairListWrapper pairList = (StringPairListWrapper)storableValue;
                            scriptText.append("Map ");
                            scriptText.append(variableName);
                            scriptText.append(" = [");
                            for (String[] entry : pairList) {
                                scriptText.append(entry[0]);
                                scriptText.append(":'");
                                scriptText.append(entry[1]);
                                scriptText.append("', ");
                            }
                            if (pairList.getSize() > 0) {
                                scriptText.setLength(scriptText.length() - 2);
                                scriptText.append("]");
                            } else {
                                scriptText.append(":]");
                            }
                        } else {
                            throw new UnsupportedOperationException("Sanitization of the condition property failed: unsupported property type: " + storableValue.getClass());
                        }
                        break;
                    default:
                        throw new UnsupportedOperationException("Sanitization of the condition property failed: unsupported property type: " + prop.getTypeLabel());
                }
                scriptText.append(";\n");
            }
            sanitized.append(condition.substring(lastMatchEnd, propMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = propMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        return sanitized.toString();
    }

    public void preExecute(BlockEnvironment env) throws Exception
    {
        if (env.getExecutionService().isOutputConnected(Output_DATA)) {
            _outLimit = env.getLongProperty(Prop_OUTPUT_LIMIT, -1L);
            if (_outLimit < 0) {
                _outLimit = Long.MAX_VALUE; // Produce as much output data as it is possible
            }
        } else {
            _outLimit = -1; // Do not produce any output data
        }

        if (env.getExecutionService().isOutputConnected(Output_REMAINDER)) {
            _remLimit = env.getLongProperty(Prop_REMAINDER_LIMIT, -1L);
            if (_remLimit < 0) {
                _remLimit = Long.MAX_VALUE; // Produce as much remaining data as it is possible
            }
        } else {
            _remLimit = -1; // Do not produce any remaining data
        }


        String condition = env.getStringProperty(Prop_CONDITION, "");
        System.out.println("User condition:\n" + condition);

        _selectedColumnNames = new ArrayList<>();

        // Sanitize variable names for block properties
        _scriptText = new StringBuilder();
        if (env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            _scriptText.append("@groovy.transform.CompileStatic\n");
        }
        _scriptText.append("class Condition {\n");
        condition = _sanitizeProps(env, condition, _scriptText);

        // Sanitize variable names for the columns in Input_A
        Pattern columnPattern = Pattern.compile("\\$" + Input_A + "\\.\\{.*?\\}");
        Matcher columnMatcher = columnPattern.matcher(condition);

        int varCnt = 0;
        int lastMatchEnd = 0;
        _col2var = new HashMap<>();
        StringBuilder sanitized = new StringBuilder();

        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(3 + Input_A.length(), match.length() - 1);
            String variableName = _col2var.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var.put(columnName, variableName);
                _selectedColumnNames.add(columnName);
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        // For non static compilation we can build a Groovy script that embeds the user condition without data types.
        _classLoader = new GroovyClassLoader();

        if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            _scriptText.append("    static boolean predicate(");
            if (_selectedColumnNames.size() > 0) {
                for (String columnName : _selectedColumnNames) {
                    _scriptText.append(_col2var.get(columnName));
                    _scriptText.append(", ");
                }
                _scriptText.setLength(_scriptText.length() - 2);
            }
            _scriptText.append(") {\n        ");
            _scriptText.append(sanitized);
            _scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + _scriptText.toString());

            Class conditionClass = _classLoader.parseClass(_scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        } else {
            _sanitizedCondition = sanitized.toString();
        }
    }

    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        Data inputData = inputs.getInputDataSet(Input_A);

        if (_outSize >= _outLimit && _remSize >= _remLimit) {
            // Skip execution if the limits have been reached
            outputs.setOutputDataSet(Output_DATA, inputData.getEmptyCopy());
            outputs.setOutputDataSet(Output_REMAINDER, inputData.getEmptyCopy());
            return;
        }

        ArrayList<Column> selectedColumns = new ArrayList<>();
        for (String columnName : _selectedColumnNames) {
            selectedColumns.add(inputData.column(columnName));
        }

        if (_predicateValueMap == null && env.getBooleanProperty(Prop_IS_DETERMINISTIC, false)) {
            _predicateValueMap = new HashMap<>(inputData.getLargestRows() * 2, 0.5f);
        }

        if (_conditionObj == null) {
            if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
                throw new IllegalStateException("Property '" + Prop_STATIC_COMPILATION + "' not set but the condition object is null");
            }

            // Build a Groovy script that embeds the user condition.
            _scriptText.append("    static boolean predicate(");
            if (selectedColumns.size() > 0) {
                for (Column column : selectedColumns) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("Boolean ");
                    }
                    _scriptText.append(_col2var.get(column.getName()));
                    _scriptText.append(", ");
                }
                _scriptText.setLength(_scriptText.length() - 2);
            }

            _scriptText.append(") {\n        ");
            _scriptText.append(_sanitizedCondition);
            _scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + _scriptText.toString());

            Class conditionClass = _classLoader.parseClass(_scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        }

        Data outputData = inputData.getEmptyCopy();
        outputData.setIndexColumn(null);
        Data remainderData = inputData.getEmptyCopy();
        remainderData.setIndexColumn(null);

        if (_outLimit > 0) {
            if (_remLimit > 0) {
                _calculate_WhereOutAndRem(_conditionObj, inputData, selectedColumns, outputData, remainderData);
            } else {
                _calculate_WhereOut(_conditionObj, inputData, selectedColumns, outputData);
            }
        } else if (_remLimit > 0) {
            _calculate_WhereRem(_conditionObj, inputData, selectedColumns, remainderData);
        }

        // Compute some basic statistics
        _totalRows += inputData.getLargestRows();
        _totalChunks++;

        // Pass the outputs
        outputs.setOutputDataSet(Output_DATA, outputData);
        outputs.setOutputDataSet(Output_REMAINDER, remainderData);
    }

    public void postExecute(BlockEnvironment env) throws Exception
    {
        System.out.println("Processed total of " + _totalRows + " rows in " + _totalChunks + " chunks.");
        if (_predicateValueMap != null) {
            System.out.println("Cache size: " + _predicateValueMap.size() + ".");
        } else {
            System.out.println("No cache used.");
        }
    }


    private void _calculate_WhereOutAndRem(GroovyObject conditionObj, Data inputData, ArrayList<Column> selectedColumns, Data outputData, Data remainderData)
    throws DataException
    {
        Object[] args = new Object[selectedColumns.size()];
        int rowNo = inputData.getLargestRows();
        int colNo = inputData.getColumns();
        for (int row = 0; row < rowNo; row++) {
            // For each row set the value of all column variables
            int argNo = 0;
            for (Column column : selectedColumns) {
                Object value = column.getObjectValue(row);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            Boolean result;
            // Compute with caching if allowed
            if (_predicateValueMap != null) {
                ArgumentArray argsArr = new ArgumentArray(args);
                result = _predicateValueMap.get(argsArr);
                if (result == null) {
                    result = (Boolean) conditionObj.invokeMethod("predicate", args);
                    _predicateValueMap.put(argsArr, result);
                }
            } else {
                result = (Boolean) conditionObj.invokeMethod("predicate", args);
            }

            if (result) {
                if (_outSize < _outLimit) {
                    for (int col = 0; col < colNo; col++) {
                        outputData.column(col).appendObjectValue(inputData.column(col).getObjectValue(row));
                    }
                    _outSize++;

                    // Shortcut if the limits have been reached.
                    if (_outSize >= _outLimit && _remSize >= _remLimit) {
                        System.out.println("The output and remainder limits have been reached. Processing terminated.");
                        break;
                    }
                }
            } else {
                if (_remSize < _remLimit) {
                    for (int col = 0; col < colNo; col++) {
                        remainderData.column(col).appendObjectValue(inputData.column(col).getObjectValue(row));
                    }
                    _remSize++;

                    // Shortcut if the limits have been reached.
                    if (_outSize >= _outLimit && _remSize >= _remLimit) {
                        System.out.println("The output and remainder limits have been reached. Processing terminated.");
                        break;
                    }
                }
            }
        }
    }


    private void _calculate_WhereOut(GroovyObject conditionObj, Data inputData, ArrayList<Column> selectedColumns, Data outputData)
    throws DataException
    {
        Object[] args = new Object[selectedColumns.size()];
        int rowNo = inputData.getLargestRows();
        int colNo = inputData.getColumns();
        for (int row = 0; row < rowNo; row++) {
            // For each row set the value of all column variables
            int argNo = 0;
            for (Column column : selectedColumns) {
                Object value = column.getObjectValue(row);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            Boolean result;
            // Compute with caching if allowed
            if (_predicateValueMap != null) {
                ArgumentArray argsArr = new ArgumentArray(args);
                result = _predicateValueMap.get(argsArr);
                if (result == null) {
                    result = (Boolean) conditionObj.invokeMethod("predicate", args);
                    _predicateValueMap.put(argsArr, result);
                }
            } else {
                result = (Boolean) conditionObj.invokeMethod("predicate", args);
            }

            if (result) {
                if (_outSize < _outLimit) {
                    for (int col = 0; col < colNo; col++) {
                        outputData.column(col).appendObjectValue(inputData.column(col).getObjectValue(row));
                    }
                    _outSize++;

                    // Shortcut if the limits have been reached.
                    if (_outSize >= _outLimit) {
                        System.out.println("The output and remainder limits have been reached. Processing terminated.");
                        break;
                    }
                }
            }
        }
    }


    private void _calculate_WhereRem(GroovyObject conditionObj, Data inputData, ArrayList<Column> selectedColumns, Data remainderData)
            throws DataException
    {
        Object[] args = new Object[selectedColumns.size()];
        int rowNo = inputData.getLargestRows();
        int colNo = inputData.getColumns();
        for (int row = 0; row < rowNo; row++) {
            // For each row set the value of all column variables
            int argNo = 0;
            for (Column column : selectedColumns) {
                Object value = column.getObjectValue(row);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            Boolean result;
            // Compute with caching if allowed
            if (_predicateValueMap != null) {
                ArgumentArray argsArr = new ArgumentArray(args);
                result = _predicateValueMap.get(argsArr);
                if (result == null) {
                    result = (Boolean) conditionObj.invokeMethod("predicate", args);
                    _predicateValueMap.put(argsArr, result);
                }
            } else {
                result = (Boolean) conditionObj.invokeMethod("predicate", args);
            }

            if (!result) {
                if (_remSize < _remLimit) {
                    for (int col = 0; col < colNo; col++) {
                        remainderData.column(col).appendObjectValue(inputData.column(col).getObjectValue(row));
                    }
                    _remSize++;

                    // Shortcut if the limits have been reached.
                    if (_remSize >= _remLimit) {
                        System.out.println("The output and remainder limits have been reached. Processing terminated.");
                        break;
                    }
                }
            }
        }
    }


    /**
     * A simple container class to allow arguments array as the key in a hash map.
     *
     */
    private static class ArgumentArray
    {
        final Object[] arguments;
        final int hashCode;

        public ArgumentArray(Object... args)
        {

            this.arguments = args;
            // This is going to be used as the Map key, so calculate hash once and eagerly.
            this.hashCode = Arrays.hashCode(args);
        }


        @Override
        public boolean equals(Object other)
        {
            return other instanceof ArgumentArray && Arrays.equals(arguments, ((ArgumentArray)other).arguments);
        }

        @Override
        public int hashCode()
        {
            return this.hashCode;
        }
    }
}

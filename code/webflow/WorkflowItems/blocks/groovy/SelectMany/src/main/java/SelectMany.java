/**
 * e-Science Central Copyright (C) 2008-2015 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import groovy.lang.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.RowExtractor;


public class SelectMany implements WorkflowBlock
{
    private final static String Prop_CONDITION = "Condition";
    private final static String Prop_STATIC_COMPILATION = "Use Static Compilation";

    private final static String Input_INPUT_1 = "input-1";

    private final static String Output_OUTPUT_1 = "output-1";

    
    public void preExecute(BlockEnvironment arg0) throws Exception
    { }

    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        HashMap<String, String> col2var = new HashMap<>(); // maps column names to groovy variable names

        String condition = env.getStringProperty(Prop_CONDITION, "");
        boolean compileStatic = env.getBooleanProperty(Prop_STATIC_COMPILATION, false);

        System.out.println("Original condition passed: " + condition);

        ArrayList<Column> selectedColumns = new ArrayList<>();
        Data inputData = inputs.getInputDataSet(Input_INPUT_1);
        Data outputData = inputData.getEmptyCopy();

        Pattern columnPattern = Pattern.compile("\\{.*?\\}");
        Matcher columnMatcher = columnPattern.matcher(condition);
        int varCnt = 0;
        StringBuilder sanitized = new StringBuilder();
        int lastMatchEnd = 0;
        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(1, match.length() - 1);
            String variableName = col2var.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                col2var.put(columnName, variableName);
                selectedColumns.add(inputData.column(columnName));
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        StringBuilder scriptText = new StringBuilder();
        if (compileStatic) {
            scriptText.append("@groovy.transform.CompileStatic\n");
        }
        scriptText.append("class Condition { static boolean predicate(");
        if (selectedColumns.size() > 0) {
            if (compileStatic) {
                for (Column column : selectedColumns) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Boolean ");
                    }
                    scriptText.append(col2var.get(column.getName()) + ", ");
                }
            } else {
                for (Column column : selectedColumns) {
                    scriptText.append(col2var.get(column.getName()) + ", ");
                }
            }
            scriptText.setLength(scriptText.length() - 2);
        }
        scriptText.append(") { ");
        scriptText.append(sanitized);
        scriptText.append("} }");
        System.out.println(scriptText.toString());

        GroovyClassLoader classLoader = new GroovyClassLoader();
        Class conditionClass = classLoader.parseClass(scriptText.toString());
        GroovyObject conditionObj = (GroovyObject)conditionClass.newInstance();

        Object[] args = new Object[selectedColumns.size()];
        int rowNo = inputData.getLargestRows();
        int colNo = inputData.getColumns();
        for (int row = 0; row < rowNo; row++) {
            // For each row set the value of all column variables
            int argNo = 0;
            for (Column column : selectedColumns) {
                args[argNo++] = column.getObjectValue(row);
            }

            if ((Boolean)conditionObj.invokeMethod("predicate", args)) {
                for (int col = 0; col < colNo; col++) {
                    outputData.column(col).appendObjectValue(inputData.column(col).getObjectValue(row));
                }
                if (inputData.getIndexColumn() != null) {
                    outputData.getIndexColumn().appendObjectValue(inputData.getIndexColumn().getObjectValue(row));
                }
            }
        }

        // Pass this to the output called "output-1"
        outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
    }

    public void postExecute(BlockEnvironment env) throws Exception
    { }
}

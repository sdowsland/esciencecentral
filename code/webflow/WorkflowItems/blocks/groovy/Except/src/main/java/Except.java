/**
 * e-Science Central Copyright (C) 2008-2016 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;


public class Except implements WorkflowBlock
{
    private final static String Prop_CONDITION = "Equality Condition";
    private final static String Prop_OUTPUT_LIMIT = "Output Limit";
    private final static String Prop_REMAINDER_LIMIT = "Remainder Limit";
    private final static String Prop_STATIC_COMPILATION = "Use Static Compilation";
    private final static String Prop_IS_DETERMINISTIC = "Is Condition Deterministic";

    /**
     * This field refers to input port 'input-1' defined in service.xml
     */
    private final static String Input_A = "A";
    private final static String Input_B = "B";
    /**
     * This field refers to output port 'output-1' defined in service.xml
     */
    private final static String Output_A_EXCEPT_B = "A-except-B";
    private final static String Output_A_IN_B = "remainder";

    private GroovyClassLoader _classLoader;
    private GroovyObject _conditionObj;

    private HashMap<String, String> _col2var_I1;
    private HashMap<String, String> _col2var_I2;
    private ArrayList<String> _selectedColumnNames_I1;
    private ArrayList<String> _selectedColumnNames_I2;
    private String _sanitizedCondition;

    Data _inputData_I2;
    ArrayList<Column> _selectedColumns_I2;

    // Cache to avoid calling Groovy --> requires a deterministic condition.
    private HashMap<ArgumentArray, Boolean> _predicateValueMap;

    private long _totalRows1;
    private long _totalChunks1;

    private long _outLimit;
    private long _outSize;
    private boolean _isOutConnected;

    private long _remLimit;
    private long _remSize;
    private boolean _isRemConnected;

    private DateFormat _sdf = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG);

    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        System.out.println(_sdf.format(new Date()) + ": pre-execute started.");

        _outLimit = env.getLongProperty(Prop_OUTPUT_LIMIT, -1L);
        if (_outLimit < 0) {
            _outLimit = Long.MAX_VALUE;
        }

        _remLimit = env.getLongProperty(Prop_REMAINDER_LIMIT, -1L);
        if (_remLimit < 0) {
            _remLimit = Long.MAX_VALUE;
        }

        _isOutConnected = env.getExecutionService().isOutputConnected(Output_A_EXCEPT_B);
        _isRemConnected = env.getExecutionService().isOutputConnected(Output_A_IN_B);

        if ((_outLimit == 0 || !_isOutConnected) && (_remLimit == 0 || !_isRemConnected)) {
            System.out.println("The output and remainder limits are set to zero or ports not connected. Processing terminated.");
            return;
        }

        String condition = env.getStringProperty(Prop_CONDITION, "");
        System.out.println("User condition:\n" + condition);

        _col2var_I1 = new HashMap<>(); // maps columns to groovy variable names
        _col2var_I2 = new HashMap<>(); // maps columns to groovy variable names

        _selectedColumnNames_I1 = new ArrayList<>();
        _selectedColumnNames_I2 = new ArrayList<>();

        // Sanitize variable names for the columns in INPUT_1
        Pattern columnPattern = Pattern.compile("\\$" + Input_A + "\\.\\{.*?\\}");
        Matcher columnMatcher = columnPattern.matcher(condition);

        int varCnt = 0;
        StringBuilder sanitized = new StringBuilder();
        int lastMatchEnd = 0;
        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(Input_A.length() + 3, match.length() - 1); // +3 for the dollar sign, dot and curly brace '$input-1.{'
            String variableName = _col2var_I1.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var_I1.put(columnName, variableName);
                _selectedColumnNames_I1.add(columnName);
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        // Sanitize variable names for the columns in INPUT_2
        condition = sanitized.toString();
        sanitized.setLength(0);
        columnPattern = Pattern.compile("\\$" + Input_B + "\\.\\{.*?\\}");
        columnMatcher = columnPattern.matcher(condition);
        lastMatchEnd = 0;
        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(Input_B.length() + 3, match.length() - 1); // +3 for the dollar sign, dot and curly brace '$input-2.{'
            String variableName = _col2var_I2.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var_I2.put(columnName, variableName);
                _selectedColumnNames_I2.add(columnName);
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        // Build a Groovy script that embeds the user condition.
        _classLoader = new GroovyClassLoader();

        if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            StringBuilder scriptText = new StringBuilder();
            scriptText.append("class Condition {\n    static boolean predicate(");
            if (_selectedColumnNames_I1.size() > 0) {
                for (String columnName : _selectedColumnNames_I1) {
                    scriptText.append(_col2var_I1.get(columnName));
                    scriptText.append(", ");
                }
            }

            if (_selectedColumnNames_I2.size() > 0) {
                for (String columnName : _selectedColumnNames_I2) {
                    scriptText.append(_col2var_I2.get(columnName));
                    scriptText.append(", ");
                }
            }

            if (_selectedColumnNames_I1.size() + _selectedColumnNames_I2.size() > 0) {
                scriptText.setLength(scriptText.length() - 2);
            }

            scriptText.append(") {\n        ");
            scriptText.append(sanitized);
            scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + scriptText.toString());

            Class conditionClass = _classLoader.parseClass(scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        } else {
            _sanitizedCondition = sanitized.toString();
        }

        // Input-B is non-streamed so we can read it here in full.
        if (_inputData_I2 == null) {
            System.out.println(_sdf.format(new Date()) + ": reading in data through port " + Input_B + ".");
            _inputData_I2 = env.getExecutionService().getInputDataSet(Input_B);
            _selectedColumns_I2 = new ArrayList<>();
            for (String columnName : _selectedColumnNames_I2) {
                _selectedColumns_I2.add(_inputData_I2.column(columnName));
            }
            System.out.println(_sdf.format(new Date()) + ": data ready.");
        }
    }


    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception
    {
        Data inputData_I1 = inputs.getInputDataSet(Input_A);

        if ((_outSize >= _outLimit || !_isOutConnected) && (_remSize >= _remLimit || !_isRemConnected)) {
            // Skip execution if the limits have been reached
            outputs.setOutputDataSet(Output_A_EXCEPT_B, inputData_I1.getEmptyCopy());
            outputs.setOutputDataSet(Output_A_IN_B, inputData_I1.getEmptyCopy());
            return;
        }

        ArrayList<Column> selectedColumns_I1 = new ArrayList<>();
        for (String columnName : _selectedColumnNames_I1) {
            selectedColumns_I1.add(inputData_I1.column(columnName));
        }

        if (_predicateValueMap == null && env.getBooleanProperty(Prop_IS_DETERMINISTIC, false)) {
            _predicateValueMap = new HashMap<>(Math.max(inputData_I1.getLargestRows(), _inputData_I2.getLargestRows()) * 2, 0.5f);
        }

        if (_conditionObj == null) {
            if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
                throw new IllegalStateException("Property '" + Prop_STATIC_COMPILATION + "' not set but the condition object is null");
            }

            // Build a Groovy script that embeds the user condition.
            StringBuilder scriptText = new StringBuilder();
            scriptText.append("@groovy.transform.CompileStatic\n");
            scriptText.append("class Condition {\n    static boolean predicate(");
            if (selectedColumns_I1.size() > 0) {
                for (Column column : selectedColumns_I1) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Boolean ");
                    }
                    scriptText.append(_col2var_I1.get(column.getName()));
                    scriptText.append(", ");
                }
            }

            if (_selectedColumns_I2.size() > 0) {
                for (Column column : _selectedColumns_I2) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Boolean ");
                    }
                    scriptText.append(_col2var_I2.get(column.getName()));
                    scriptText.append(", ");
                }
            }

            if (selectedColumns_I1.size() + _selectedColumns_I2.size() > 0) {
                scriptText.setLength(scriptText.length() - 2);
            }

            scriptText.append(") {\n        ");
            scriptText.append(_sanitizedCondition);
            scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + scriptText.toString());

            Class conditionClass = _classLoader.parseClass(scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        }

        Data outputData = inputData_I1.getEmptyCopy();
        outputData.setIndexColumn(null);
        Data remainderData = inputData_I1.getEmptyCopy();
        remainderData.setIndexColumn(null);

        if (_isOutConnected) {
            if (_isRemConnected) {
                _calculateExcept_OutAndRem(_conditionObj, inputData_I1, selectedColumns_I1, _inputData_I2, _selectedColumns_I2, outputData, remainderData);
            } else {
                _calculateExcept_Out(_conditionObj, inputData_I1, selectedColumns_I1, _inputData_I2, _selectedColumns_I2, outputData);
            }
        } else if (_isRemConnected) {
            _calculateExcept_Rem(_conditionObj, inputData_I1, selectedColumns_I1, _inputData_I2, _selectedColumns_I2, remainderData);
        }

        // Compute some basic statistics
        _totalRows1 += inputData_I1.getLargestRows();
        _totalChunks1++;

        // Pass the data to the output.
        outputs.setOutputDataSet(Output_A_EXCEPT_B, outputData);
        outputs.setOutputDataSet(Output_A_IN_B, remainderData);
    }


    private void _calculateExcept_Rem(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data remainderData)
    throws DataException
    {
        int rowNo_I1 = inputData_I1.getLargestRows();
        int colNo_I1 = inputData_I1.getColumnCount();
        int rowNo_I2 = inputData_I2.getLargestRows();

        Object[] args = new Object[selectedColumns_I1.size() + selectedColumns_I2.size()];

        for (int r1 = 0; r1 < rowNo_I1; r1++) {
            // For each row in Input-A set the value of all its column variables
            int argNo = 0;
            for (Column column : selectedColumns_I1) {
                Object value = column.getObjectValue(r1);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            int r2;
            for (r2 = 0; r2 < rowNo_I2; r2++) {
                // For each row in Input_B set the value of all its column variables
                argNo = selectedColumns_I1.size();
                for (Column column : selectedColumns_I2) {
                    Object value = column.getObjectValue(r2);
                    args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                Boolean result;

                if (_predicateValueMap != null) {
                    ArgumentArray argsArr = new ArgumentArray(args);
                    result = _predicateValueMap.get(argsArr);
                    if (result == null) {
                        result = (Boolean)conditionObj.invokeMethod("predicate", args);
                        _predicateValueMap.put(argsArr, result);
                    }
                } else {
                    result = (Boolean)conditionObj.invokeMethod("predicate", args);
                }

                if (result) {
                    break;
                }
            }

            if (r2 < rowNo_I2) {
                inputData_I2.removeRow(r2);
                rowNo_I2--;

                if (_remSize < _remLimit) {
                    for (int c = 0; c < colNo_I1; c++) {
                        remainderData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    _remSize++;

                    if (_remSize >= _remLimit) {
                        System.out.println("The remainder limit has been reached. Processing terminated.");
                        break;
                    }
                }
            }
        }
    }


    /**
     * A naive O(n*logm) implementation of the SUBTRACT SET operation when only the A-except-B output is connected.
     * TODO: Find time and make it more efficient.
     */
    private void _calculateExcept_Out(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data outputData)
    throws DataException
    {
        int rowNo_I1 = inputData_I1.getLargestRows();
        int colNo_I1 = inputData_I1.getColumnCount();
        int rowNo_I2 = inputData_I2.getLargestRows();

        Object[] args = new Object[selectedColumns_I1.size() + selectedColumns_I2.size()];

        // The outer loop goes over a chunk of I2
        // The inner loop goes over the whole I1
        for (int r1 = 0; r1 < rowNo_I1; r1++) {
            // For each row in Input-A set the value of all its column variables
            int argNo = 0;
            for (Column column : selectedColumns_I1) {
                Object value = column.getObjectValue(r1);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            int r2;
            for (r2 = 0; r2 < rowNo_I2; r2++) {
            // For each row in Input_B set the value of all its column variables
                argNo = selectedColumns_I1.size();
                for (Column column : selectedColumns_I2) {
                    Object value = column.getObjectValue(r2);
                    args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                Boolean result;

                if (_predicateValueMap != null) {
                    ArgumentArray argsArr = new ArgumentArray(args);
                    result = _predicateValueMap.get(argsArr);
                    if (result == null) {
                        result = (Boolean)conditionObj.invokeMethod("predicate", args);
                        _predicateValueMap.put(argsArr, result);
                    }
                } else {
                    result = (Boolean)conditionObj.invokeMethod("predicate", args);
                }

                if (result) {
                    break;
                }
            }

            if (r2 < rowNo_I2) {
                inputData_I2.removeRow(r2);
                rowNo_I2--;
            } else {
                if (_outSize < _outLimit) {
                    for (int c = 0; c < colNo_I1; c++) {
                        outputData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    _outSize++;

                    if (_outSize >= _outLimit) {
                        System.out.println("The output limit has been reached. Processing terminated.");
                        break;
                    }
                }
            }
        }
    }


    private void _calculateExcept_OutAndRem(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data outputData, Data remainderData)
    throws DataException
    {
        int rowNo_I1 = inputData_I1.getLargestRows();
        int colNo_I1 = inputData_I1.getColumnCount();
        int rowNo_I2 = inputData_I2.getLargestRows();

        Object[] args = new Object[selectedColumns_I1.size() + selectedColumns_I2.size()];

        for (int r1 = 0; r1 < rowNo_I1; r1++) {
            // For each row in Input-A set the value of all its column variables
            int argNo = 0;
            for (Column column : selectedColumns_I1) {
                Object value = column.getObjectValue(r1);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            int r2;
            for (r2 = 0; r2 < rowNo_I2; r2++) {
                // For each row in Input_B set the value of all its column variables
                argNo = selectedColumns_I1.size();
                for (Column column : selectedColumns_I2) {
                    Object value = column.getObjectValue(r2);
                    args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                Boolean result;

                if (_predicateValueMap != null) {
                    ArgumentArray argsArr = new ArgumentArray(args);
                    result = _predicateValueMap.get(argsArr);
                    if (result == null) {
                        result = (Boolean)conditionObj.invokeMethod("predicate", args);
                        _predicateValueMap.put(argsArr, result);
                    }
                } else {
                    result = (Boolean)conditionObj.invokeMethod("predicate", args);
                }

                if (result) {
                    break;
                }
            }

            if (r2 < rowNo_I2) {
                inputData_I2.removeRow(r2);
                rowNo_I2--;

                if (_remSize < _remLimit) {
                    for (int c = 0; c < colNo_I1; c++) {
                        remainderData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    _remSize++;

                    if (_remSize >= _remLimit && _outSize >= _outLimit) {
                        System.out.println("The remainder and output limits have been reached. Processing terminated.");
                        break;
                    }
                }
            } else {
                if (_outSize < _outLimit) {
                    for (int c = 0; c < colNo_I1; c++) {
                        outputData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    _outSize++;

                    if (_outSize >= _outLimit && _remSize >= _remLimit) {
                        System.out.println("The output and remainder limits have been reached. Processing terminated.");
                        break;
                    }
                }
            }
        }
    }


    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        System.out.println("Processed " + _totalRows1 + " row(s) of input " + Input_A + " in " + _totalChunks1 + " chunk(s).");
        if (_inputData_I2 != null) {
            System.out.println("Processed " + _inputData_I2.getLargestRows() + " row(s) of input " + Input_B);
        } else {
            System.out.println("Processed 0 rows of input " + Input_B);
        }
        if (_predicateValueMap != null) {
            System.out.println("Cache size: " + _predicateValueMap.size() + ".");
        } else {
            System.out.println("No cache used.");
        }
    }


    /**
     * A simple container class to allow arguments array as the key in a hash map.
     *
     */
    private static class ArgumentArray
    {
        final Object[] arguments;
        final int hashCode;

        public ArgumentArray(Object... args)
        {

            this.arguments = args;
            // This is going to be used as the Map key, so calculate hash once and eagerly.
            this.hashCode = Arrays.hashCode(args);
        }


        @Override
        public boolean equals(Object other)
        {
            return other instanceof ArgumentArray && Arrays.equals(arguments, ((ArgumentArray)other).arguments);
        }

        @Override
        public int hashCode()
        {
            return this.hashCode;
        }
    }
}



import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import java.util.List;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.manipulation.NumericalColumnExtractor;
import org.pipeline.core.data.maths.CorrelationCalculator;

/**
 * e-sc block to calculate the Pearson product-moment correlation ('r') between 
 * 2 data tables
 * 
 */
public class MyService extends CloudDataProcessorService {

    
    @Override
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    @Override
    public void execute() throws Exception {
        // This is the API link that can be used to access data in the
        // Inkspot platform. It is configured using the identity of
        // the user executing the service
        // APIBroker api = createApiLink();

        // The following method is used to get a set of input data
        // and should be configured using the name of the input
        // data to fetch
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");
        
        List xColumns = new NumericalColumnExtractor(xData).extractColumns();
        List yColumns = new NumericalColumnExtractor(yData).extractColumns();
        
        if(xColumns.size()==yColumns.size()){
            Data results = new Data();
            NumericalColumn x;
            NumericalColumn y;
            CorrelationCalculator c;
            DoubleColumn r;
            
            for(int i=0;i<xColumns.size();i++){
                x = (NumericalColumn)xColumns.get(i);
                y = (NumericalColumn)yColumns.get(i);
                r = new DoubleColumn(x.getName() + "-v-" + y.getName());
                c = new CorrelationCalculator(x, y);
                try {
                    r.appendDoubleValue(c.doubleValue());
                } catch (Exception e){
                    System.out.println("Cannot calculate correlation between: " + x.getName() + " and " + y.getName() + ": " + e.getMessage());
                    r.appendObjectValue(MissingValue.get());
                }
                results.addColumn(r);
            }
            setOutputDataSet("r", results);
            
        } else if(yData.getColumnCount() ==1){
            // Only a single y column
            Data results = new Data();
            NumericalColumn x;
            NumericalColumn y = (NumericalColumn)yColumns.get(0);
            CorrelationCalculator c;
            DoubleColumn r;
            
            for(int i=0;i<xColumns.size();i++){
                x = (NumericalColumn)xColumns.get(i);
                r = new DoubleColumn(x.getName() + "-v-" + y.getName());
                c = new CorrelationCalculator(x, y);
                try {
                    r.appendDoubleValue(c.doubleValue());
                } catch (Exception e){
                    System.out.println("Cannot calculate correlation between: " + x.getName() + " and " + y.getName() + ": " + e.getMessage());
                    r.appendObjectValue(MissingValue.get());
                }
                results.addColumn(r);
            }
            setOutputDataSet("r", results);
            
        } else {
            throw new Exception("Numerical column counts do not match");
        }
        
        

    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    @Override
    public void allDataProcessed() throws Exception {
    }
}
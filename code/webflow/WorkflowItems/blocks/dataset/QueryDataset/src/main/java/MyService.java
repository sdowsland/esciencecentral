/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;

public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();
        DatasetQuery query = (DatasetQuery)getEditableProperties().xmlStorableValue("Query");
        
        if(query!=null){
            JSONContainer data = api.queryDatasetItem(query);
            JSONObject json = data.getJSONObject();
            JSONArray dataArray = json.getJSONArray("data");
            
            int rows = dataArray.length();
            Iterator keys;
            String stringValue;
            double doubleValue;
            Column col;
            String key;
            JSONObject rowJson;
            

            
            // Get the first row
            Data results = new Data();
            if(rows>0){
                // Get a sorted set of keys
                rowJson = dataArray.getJSONObject(0);
                keys = rowJson.keys();
                ArrayList<String> sortedKeys = new ArrayList<String>();
                while(keys.hasNext()){
                    sortedKeys.add(keys.next().toString());
                }
                Collections.sort(sortedKeys);
                keys = sortedKeys.iterator();
                
                // Create the columns with the correct names                
                while(keys.hasNext()){
                    key = keys.next().toString();
                    if(!key.startsWith("_")){
                        stringValue = rowJson.getString(key);
                        try {
                            doubleValue = Double.parseDouble(stringValue);
                            col = new DoubleColumn(key);
                            ((DoubleColumn)col).appendDoubleValue(doubleValue);

                        } catch (Exception e){
                            col = new StringColumn(key);
                            ((StringColumn)col).appendStringValue(stringValue);
                        }
                        results.addColumn(col);
                    }
                }
                
                // Now work through the rest of the data
                if(rows>1){
                    for(int i=0;i<rows;i++){
                        rowJson = dataArray.getJSONObject(i);
                        keys = rowJson.keys();
                        while(keys.hasNext()){
                            key = keys.next().toString();
                            if(!key.startsWith("_")){
                                if(results.containsColumn(key)){
                                    col = results.column(key);
                                    stringValue = rowJson.getString(key);
                                    if(col instanceof NumericalColumn){
                                        try {
                                            ((DoubleColumn)col).appendDoubleValue(Double.parseDouble(stringValue));
                                        } catch (Exception e){
                                            col.appendObjectValue(MissingValue.get());
                                        }
                                    } else {
                                        col.appendStringValue(stringValue);
                                    }
                                }
                            }
                        }

                    }
                }

                setOutputDataSet("imported-data", results);
            } else {
                System.out.println("No data imported");
                setOutputDataSet("imported-data", results);
            }

        } else {
            throw new Exception("Dataset properties incorrectly configured.");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
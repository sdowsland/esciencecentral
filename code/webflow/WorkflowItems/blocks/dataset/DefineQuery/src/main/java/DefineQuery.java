/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.workflow.*;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import org.pipeline.core.data.*;

public class DefineQuery extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
        super.executionAboutToStart(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execute() throws Exception {
        DatasetQuery query = (DatasetQuery)getEditableProperties().xmlStorableValue("Query");
        if(query!=null){
            setOutputData("query", new ObjectWrapper(query));
        } else {
            throw new Exception("No query defined");
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
        super.allDataProcessed(); //To change body of generated methods, choose Tools | Templates.
    }
}
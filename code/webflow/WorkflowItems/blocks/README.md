# Workflow blocks

The list below includes references to all *core* blocks available in the system.
Click any of the links to see documentation for a selected block.

## Group: charting

* [ActualVPredictedPlot](charting/ActualVPredictedPlot)
* [DataReport](charting/DataReport)
* [PointsPlot](charting/PointsPlot)
* [TrendAndPoints](charting/TrendAndPoints)
* [TrendPlot](charting/TrendPlot)

## Group: dashboard

* [AddDashboardRows](dashboard/AddDashboardRows)
* [ResetDashboard](dashboard/ResetDashboard)
* [UpdateDashboard](dashboard/UpdateDashboard)

## Group: dataset

* [AddDatasetRows](dataset/AddDatasetRows)
* [DefineQuery](dataset/DefineQuery)
* [GetDatasetItemByName](dataset/GetDatasetItemByName)
* [GetDatasetItemUsingProperties](dataset/GetDatasetItemUsingProperties)
* [PickDatasetItem](dataset/PickDatasetItem)
* [QueryDataset](dataset/QueryDataset)
* [ResetDataset](dataset/ResetDataset)
* [UpdateDataset](dataset/UpdateDataset)
* [UpdateItems](dataset/UpdateItems)

## Group: demo

* [Partition](demo/Partition)
* [WordCountMap](demo/WordCountMap)
* [WordCountReduce](demo/WordCountReduce)

## Group: io

* [CSVExport](io/CSVExport)
* [CSVImport](io/CSVImport)
* [ExportFiles](io/ExportFiles)
* [ExportProperties](io/ExportProperties)
* [FileJoin](io/FileJoin)
* [FileListJoin](io/FileListJoin)
* [ImportDirectory](io/ImportDirectory)
* [ImportFile](io/ImportFile)
* [MatFileWriter](io/MatFileWriter)
* [MoveFilesToSubdirectory](io/MoveFilesToSubdirectory)
* [OctaveCSVtoMat](io/OctaveCSVtoMat)
* [ParseCSV](io/ParseCSV)
* [PickFiles](io/PickFiles)
* [PrepareLibrary](io/PrepareLibrary)
* [ReadObject](io/ReadObject)
* [RenameFiles](io/RenameFiles)
* [SerializeCSV](io/SerializeCSV)
* [SerializeObject](io/SerializeObject)
* [Unzip](io/Unzip)

## Group: manipulation

* [3RowJoin](manipulation/3RowJoin)
* [AddSampleCount](manipulation/AddSampleCount)
* [ColumnJoin](manipulation/ColumnJoin)
* [ColumnSelect](manipulation/ColumnSelect)
* [CreateProperties](manipulation/CreateProperties)
* [DataJoin](manipulation/DataJoin)
* [DataShuffle](manipulation/DataShuffle)
* [DataSort](manipulation/DataSort)
* [DataToProperties](manipulation/DataToProperties)
* [ExtractNumerical](manipulation/ExtractNumerical)
* [FileChunker](manipulation/FileChunker)
* [FileRowSubsample](manipulation/FileRowSubsample)
* [IDMatching](manipulation/IDMatching)
* [PickColumnByName](manipulation/PickColumnByName)
* [RenameColumns](manipulation/RenameColumns)
* [RowJoin](manipulation/RowJoin)
* [RowSplit](manipulation/RowSplit)
* [SubSample](manipulation/SubSample)
* [Transpose](manipulation/Transpose)

## Group: maths

* [Correlation](maths/Correlation)
* [TTest](maths/TTest)

## Group: metadata

* [AddMetadata](metadata/AddMetadata)
* [AttachMetadataToFiles](metadata/AttachMetadataToFiles)
* [MetadataCrossReference](metadata/MetadataCrossReference)
* [getMetaDataFromUser](metadata/getMetaDataFromUser)

## Group: misc

* [BubbleSort](misc/BubbleSort)
* [GenerateRandomData](misc/GenerateRandomData)
* [HTTPGet](misc/HTTPGet)
* [HTTPPost](misc/HTTPPost)
* [SimpleBlockExample](misc/SimpleBlockExample)
* [SimplifiedBlockExample](misc/SimplifiedBlockExample)
* [StringList](misc/StringList)
* [UballDataGenerator](misc/UballDataGenerator)

## Group: modelling

* [JavaFFANN](modelling/JavaFFANN)
* [JavaFFANNPredict](modelling/JavaFFANNPredict)
* [PLS](modelling/PLS)

## Group: mongodb

* [ExecuteMongoQuery](mongodb/ExecuteMongoQuery)
* [MongoUpdate](mongodb/MongoUpdate)

## Group: numerical

* [ABS](numerical/ABS)
* [KalmanFilter](numerical/KalmanFilter)
* [OutlierFilter](numerical/OutlierFilter)
* [ScaleData](numerical/ScaleData)
* [UnscaleData](numerical/UnscaleData)

## Group: server

* [CreateUser](server/CreateUser)
* [DeleteFileReferences](server/DeleteFileReferences)
* [DownloadReferences](server/DownloadReferences)
* [GetFileReference](server/GetFileReference)
* [GetFileReferences](server/GetFileReferences)
* [GetReferenceInfo](server/GetReferenceInfo)
* [GetRelatedFileReferences](server/GetRelatedFileReferences)
* [LinkFiles](server/LinkFiles)
* [LinkReferencesToInvocation](server/LinkReferencesToInvocation)
* [MoveReferencesToFolder](server/MoveReferencesToFolder)
* [PickReferences](server/PickReferences)
* [ReferenceListJoin](server/ReferenceListJoin)
* [RenameFromReference](server/RenameFromReference)

## Group: testing

* [Sleep](testing/Sleep)
* [SleepIO](testing/SleepIO)

## Group: tsb

* [CleanupWorkingData](tsb/CleanupWorkingData)
* [CreateEmptyLibraryTable](tsb/CreateEmptyLibraryTable)
* [CreateMasterTable](tsb/CreateMasterTable)
* [CreateNECompaniesTable](tsb/CreateNECompaniesTable)
* [PerformTSBQuery](tsb/PerformTSBQuery)
* [TSBJDBCConnection](tsb/TSBJDBCConnection)
* [ValidateCompanyNumber](tsb/ValidateCompanyNumber)

## Group: weka

* [ARFF-ApplyFilter](weka/ARFF-ApplyFilter)
* [ARFF-CFSFilterFeatures](weka/ARFF-CFSFilterFeatures)
* [ArffToData](weka/ArffToData)
* [C4.5Classifier](weka/C4.5Classifier)
* [CSV-To-ARFF](weka/CSV-To-ARFF)
* [ClassificationPrediction](weka/ClassificationPrediction)
* [ClassifierReport](weka/ClassifierReport)
* [DataToArff](weka/DataToArff)
* [KNN](weka/KNN)
* [LinearRegression](weka/LinearRegression)
* [LoadARFF](weka/LoadARFF)
* [ModelReport](weka/ModelReport)
* [NeuralNetwork](weka/NeuralNetwork)
* [PLSModel](weka/PLSModel)
* [PaceRegression](weka/PaceRegression)
* [RBFNetwork](weka/RBFNetwork)
* [RandomForest](weka/RandomForest)
* [ResampleARFF](weka/ResampleARFF)
* [StandardiseARFF](weka/StandardiseARFF)
* [String2Nominal](weka/String2Nominal)
* [SupportVectorRegression](weka/SupportVectorRegression)
* [WekaPrediction](weka/WekaPrediction)
* [ZeroR](weka/ZeroR)

## Group: workflow

* [CollectWorkflowResults](workflow/CollectWorkflowResults)
* [CombinationSweep](workflow/CombinationSweep)
* [ConcatenateDataSets](workflow/ConcatenateDataSets)
* [FailWorkflow](workflow/FailWorkflow)
* [ParameterSweep](workflow/ParameterSweep)
* [RunWorkflow](workflow/RunWorkflow)
* [RunWorkflowMutipleTimes](workflow/RunWorkflowMutipleTimes)
* [RunWorkflows](workflow/RunWorkflows)
* [WaitForWorkflows](workflow/WaitForWorkflows)
* [WorkflowPerFile](workflow/WorkflowPerFile)
* [WorkflowPerFileRefAndProp](workflow/WorkflowPerFileRefAndProp)
* [WorkflowPerFileWithRef](workflow/WorkflowPerFileWithRef)

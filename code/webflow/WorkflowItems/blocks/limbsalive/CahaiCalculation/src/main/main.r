
#install.packages(cahaiLibrary[1], repos = NULL, type="source")

library(lapkg)

# path to directory with the game files
 path0  <- paste(getwd(), getProperty("DataFolder"), sep="/");

# file location with patient static data
std0  <- staticDataFile[1];

# cahai prediction
cahai <- predict_la(gameFiles_dir = path0, staticData_file = std0, whatmodel = 'cahai')  # outputs dataframe

cahai$yhat            # cahai prediction

calibrationdates = cahai$calDate     # calibration dates
predictions = cahai$yhat   # cahai prediction for each date




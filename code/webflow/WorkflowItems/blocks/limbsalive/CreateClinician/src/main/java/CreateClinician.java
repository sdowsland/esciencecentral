
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;

public class CreateClinician extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        String email = getEditableProperties().stringValue("EMail", null);
        String firstName = getEditableProperties().stringValue("FirstName", null);
        String surname = getEditableProperties().stringValue("Surname", null);
        String password = getEditableProperties().stringValue("Password", null);
        if(email!=null && firstName!=null && surname!=null && password!=null && !email.isEmpty() && !firstName.isEmpty() && !surname.isEmpty() && !password.isEmpty()){
            User clinician = api.createAccount(firstName, surname, email, password);
            
            // Make the clinician an admin
            Group adminGroup = api.getGroupByName("Admins");
            api.addUserToGroup(adminGroup.getId(), clinician.getId());
            
            // Do the rest as the clinician
            APIBroker clinicianApi = createAdditionalApiLink(clinician);
            
            // Create the clinican group
            Group patients = clinicianApi.getGroupByName("Patients_" + clinician.getId());
            if(patients==null){
                System.out.println("Creating Patients group: Patients_" + clinician.getId());
                patients = clinicianApi.createGroup("Patients_" + clinician.getId(), true, false);
            }
            
            // Create the summary data set
            Dataset summaryDataset = new Dataset();
            summaryDataset.setName("PlayerSummary");
            summaryDataset.setDescription("Player summary data");
            summaryDataset = clinicianApi.saveDataset(summaryDataset);
            
            // Add patients to this dataset
            clinicianApi.grantObjectPermission(summaryDataset, patients, Permission.READ_PERMISSION);
            clinicianApi.grantObjectPermission(summaryDataset, patients, Permission.WRITE_PERMISSION);
            
            // Set user type as metadata
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(new TextMetadata("LimbsAlive", "UserType", "Clinician"));
            clinicianApi.uploadMetadata(clinician, mdc);
            
            Data outputData = new Data();
            outputData.addSingleValue("UserID", clinician.getId());
            setOutputDataSet("user-id", outputData);
            
        } else {
            throw new Exception("Missing information in CreateClinician");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;

import java.io.File;

public class ParseHistoricPRDFiles extends CloudDataProcessorService {


    public void execute() throws Exception {

        FileWrapper prdFileWrapper = ((FileWrapper) getInputData("prd-file"));
        Data versionsInfo = getInputDataSet("versions-info");

        Data summaryData = new Data();
        Data difficultiesData = new Data();
        Data achievementsData = new Data();
        Data gesturesData = new Data();

        Column versionNumberCol = versionsInfo.column("versionNumber");
        Column fileNamesCol = versionsInfo.column("filename");


        //for each row in the versions info
        for (int i = 0; i < versionNumberCol.getRows(); i++) {
            //get the version number out of the data passed in
            String profileVersionNumber = versionNumberCol.getStringValue(i);

            //Parse the file
            SAXBuilder sax = new SAXBuilder();

            //The file has been converted from a .prd into a .prd.xml
            String filename = fileNamesCol.getStringValue(i) + ".xml";
            Document doc = sax.build(new File(filename));
            Element serializableProfileData = doc.getRootElement();

            //Top level summary data - will create a 1x23 object
            PRDParser.populateTopLevelData(serializableProfileData, summaryData, profileVersionNumber);

            //Difficulty data - creates a 300x9 data object
            PRDParser.populateDifficultiesData(serializableProfileData, difficultiesData, profileVersionNumber);

            //Achievements - will create 1x153 data object
            PRDParser.populateAchievementsData(serializableProfileData, achievementsData, profileVersionNumber);

            //Gestures - will creaet a 93x10 data object
            PRDParser.populateGesturesData(serializableProfileData, gesturesData, profileVersionNumber);

        }

        setOutputDataSet("summary-data", summaryData);
        setOutputDataSet("difficulties-data", difficultiesData);
        setOutputDataSet("achievements-data", achievementsData);
        setOutputDataSet("gestures-data", gesturesData);
    }

}
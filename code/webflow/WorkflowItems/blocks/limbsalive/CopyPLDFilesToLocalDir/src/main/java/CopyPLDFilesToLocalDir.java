
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.WorkflowInvocation;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;

import java.io.File;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.pipeline.core.data.*;

public class CopyPLDFilesToLocalDir extends CloudDataProcessorService {

    public void execute() throws Exception {
        APIBroker api = createApiLink();


        FileWrapper inputFiles = (FileWrapper) getInputData("input-files");

        String targetTopLevelDirectory = getProperties().stringValue("LocalDir", "/tmp/LAData");
        String username = api.getCurrentUser().getFirstName();


//        Path newFile = FileSystems.getDefault().getPath(localDir, "pld", username, relativeFile);
////
//        Files.copy(new File(localDir), newFile, StandardCopyOption.REPLACE_EXISTING);

        for (int i = 0; i < inputFiles.getFileCount(); i++) {
            File file = inputFiles.getFile(i);

            Path oldPath = Paths.get(file.getPath());

            String assessmentFolder = file.getParentFile().getName();

            Path targetFolder = FileSystems.getDefault().getPath(targetTopLevelDirectory, "pld", username, assessmentFolder);
            Files.createDirectories(targetFolder);

            Path newFile = FileSystems.getDefault().getPath(targetTopLevelDirectory, "pld",  username, assessmentFolder, file.getName());

            Files.copy(oldPath, newFile, StandardCopyOption.REPLACE_EXISTING);
//
//            Path oldPath = Paths.get(inputFiles.getFile(i).getPath());
//
//            String cwd = getWorkingDirectory().getAbsolutePath();
//            String relativeFile = file.getAbsolutePath().substring(cwd.length() + 1);
//            System.out.println("relativeFile = " + relativeFile);
//
//
////            Files.createDirectories(FileSystems.getDefault().getPath(localDir, parentOnDisk, username, containerOnServer.getName()));
//            Path newFile = FileSystems.getDefault().getPath(localDir, "pld", username, relativeFile);
////
//            Files.copy(new File(localDir), newFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }


}
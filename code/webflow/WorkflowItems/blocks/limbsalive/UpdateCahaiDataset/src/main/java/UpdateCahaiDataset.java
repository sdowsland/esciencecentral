
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import org.json.*;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.pipeline.core.data.*;

public class UpdateCahaiDataset extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        Data inputData = getInputDataSet("cahai-score");

        DatasetItem item = (DatasetItem)(getInputData("dataset-item")).getPayload();
        String cahaiSource = getEditableProperties().stringValue("CahaiSource", "calculated");
        String datasetId = item.getDatasetId();
        String itemName = item.getName();

        SimpleDateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        double score;
        String dateText;
        JSONObject jsonRow;
        
        for(int i=0;i<inputData.getLargestRows();i++){
            score = ((NumericalColumn)inputData.column(1)).getDoubleValue(i);
            dateText = inputData.column(0).getStringValue(i);
            
            jsonRow = new JSONObject();
            jsonRow.put("score", score);
            
            Date dt = readFormat.parse(dateText);
            jsonRow.put("time", writeFormat.format(dt));
            jsonRow.put("milliseconds", dt.getTime());
            jsonRow.put("source", cahaiSource);
            api.updateDatasetItem(datasetId, itemName, new JSONContainer(jsonRow.toString()));
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
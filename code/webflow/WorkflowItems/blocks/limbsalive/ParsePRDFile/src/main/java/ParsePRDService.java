
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;

public class ParsePRDService extends CloudDataProcessorService {


    public void execute() throws Exception {

        FileWrapper prdFileWrapper = ((FileWrapper) getInputData("prd-file"));

        if (prdFileWrapper.getFileCount() != 1) {
            throw new ConnexienceException("ParsePRDFile can only be called with one file at present as Metadata is required");
        }

        Data summaryData = new Data();
        Data difficultiesData = new Data();
        Data achievementsData = new Data();
        Data gesturesData = new Data();

        // Get the metadata - this is needed so that we have the version number of the
        // .prd file in order to collate rows in the dataset
        Data inputMetadata = getInputDataSet("prd-file-metadata");
        StringColumn mdNames = (StringColumn) inputMetadata.column("Name");
        StringColumn mdValues = (StringColumn) inputMetadata.column("Value");
        String profileVersionNumber = "UKNOWN_VERSION_NUMBER_" + Math.random();
        for (int i = 0; i < mdNames.getRows(); i++) {
            String name = mdNames.getStringValue(i);
            if (name.equalsIgnoreCase("source-version-number")) {
                profileVersionNumber = mdValues.getStringValue(i);
            }
        }

        //Parse the file
        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(prdFileWrapper.getFile(0));
        Element serializableProfileData = doc.getRootElement();

        //Top level summary data - will create a 1x23 object
        PRDParser.populateTopLevelData(serializableProfileData, summaryData, profileVersionNumber);

        //Difficulty data - creates a 300x9 data object
        PRDParser.populateDifficultiesData(serializableProfileData, difficultiesData, profileVersionNumber);

        //Achievements - will create 1x153 data object
        PRDParser.populateAchievementsData(serializableProfileData, achievementsData, profileVersionNumber);

        //Gestures - will creaet a 93x10 data object
        PRDParser.populateGesturesData(serializableProfileData, gesturesData, profileVersionNumber);

        setOutputDataSet("summary-data", summaryData);
        setOutputDataSet("difficulties-data", difficultiesData);
        setOutputDataSet("achievements-data", achievementsData);
        setOutputDataSet("gestures-data", gesturesData);
    }

}

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.security.User;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.CSVDataExporter;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class CreateStaticDataForCahai extends CloudDataProcessorService {

    public void execute() throws Exception {
        try {
            APIBroker api = createApiLink();
            Data staticDataCSV = new Data();

            // 15/05/2010
            SimpleDateFormat javiersFormat = new SimpleDateFormat("dd/MM/yyyy");

            //2014-03-18T00:00:00.000
            DateFormat cahaiDSIDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


            User currentUser = api.getCurrentUser();
            MetadataCollection mdc = api.getMetadata(currentUser);

            Column pat = new StringColumn("pat");
            pat.appendStringValue(currentUser.getFirstName());
            staticDataCSV.addColumn(pat);

            IntegerColumn ageCol = new IntegerColumn("age");
            MetadataItem age = mdc.find("LimbsAlive", "Age");
            ageCol.appendLongValue(((NumericalMetadata) age).getLongValue());
            staticDataCSV.addColumn(ageCol);

            Column genderCol = new StringColumn("gender");
            MetadataItem gender = mdc.find("LimbsAlive", "patientGender");
            genderCol.appendStringValue(gender.getStringValue().substring(0,1));  //needs M/F
            staticDataCSV.addColumn(genderCol);

            IntegerColumn armlengthCol = new IntegerColumn("armlength");
            MetadataItem leftarmlength = mdc.find("LimbsAlive", "leftArmLength");
            MetadataItem rightarmlength = mdc.find("LimbsAlive", "rightArmLength");

            Long longestarmlength;
            if(((NumericalMetadata) leftarmlength).getLongValue() > ((NumericalMetadata) rightarmlength).getLongValue()){
                longestarmlength = ((NumericalMetadata)leftarmlength).getLongValue();
            }
            else{
                longestarmlength = ((NumericalMetadata)rightarmlength).getLongValue();
            }

            armlengthCol.appendLongValue(longestarmlength);
            staticDataCSV.addColumn(armlengthCol);

            //Get the Baseline cahai from the Dataset, not the Metadata
            DatasetItem cahaiDSI = api.lookupUserDatasetItemByName("CAHAI", "CAHAI");
            JSONContainer cahaiScoreContainer = api.queryDatasetItem(cahaiDSI.getDatasetId(), "CAHAI");

            // format: {data:[{time:XXX;score:YYY},{...}]}
            JSONArray cahaiScores = cahaiScoreContainer.getJSONObject().getJSONArray("data");

            // Loop through all of the scores looking for the earliest one
            Date baselineDate = new Date();
            Integer baselineCahai = 0;

            for(int i = 0; i < cahaiScores.length(); i++){
                JSONObject cahai = (JSONObject) cahaiScores.get(i);

                Date thisAssessment = cahaiDSIDateFormat.parse((String)cahai.get("time"));
                if(thisAssessment.before(baselineDate)){
                    baselineDate = thisAssessment;
                    baselineCahai = (Integer)cahai.get("score");
                }
            }


            IntegerColumn baselineCahaiCol = new IntegerColumn("base9");
            baselineCahaiCol.appendLongValue( baselineCahai);
            staticDataCSV.addColumn(baselineCahaiCol);


            Column dobas = new StringColumn("dobas"); //date of baseline
            dobas.appendStringValue(javiersFormat.format(baselineDate));
            staticDataCSV.addColumn(dobas);

            Column pshdomCol = new StringColumn("pshdom");
            MetadataItem pshdom = mdc.find("LimbsAlive", "PreStrokeDominantHand");
            pshdomCol.appendStringValue(pshdom.getStringValue().substring(0,1));
            staticDataCSV.addColumn(pshdomCol);


            Column dosCol = new StringColumn("dos");
            MetadataItem dos = mdc.find("LimbsAlive", "strokeDate");
            DateMetadata dateMetadata = (DateMetadata) dos;
            dosCol.appendStringValue(javiersFormat.format(dateMetadata.getDateValue()));
            staticDataCSV.addColumn(dosCol);


            Column psideCol = new StringColumn("pside");
            MetadataItem sideOfHemiplegia = mdc.find("LimbsAlive", "sideOfHemiplegia");
            psideCol.appendStringValue(sideOfHemiplegia.getStringValue().substring(0,1));
            staticDataCSV.addColumn(psideCol);

            String fileName = "staticData.csv";
            File outputFile = new File(fileName);
            CSVDataExporter exporter = new CSVDataExporter(staticDataCSV);
            exporter.writeFile(outputFile);

            FileWrapper fileWrapper = new FileWrapper(getWorkingDirectory());
            fileWrapper.addFile(outputFile);
            setOutputData("static-data-file", fileWrapper);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
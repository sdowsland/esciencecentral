
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import org.json.*;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.pipeline.core.data.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class UpdateGoalDataset extends CloudDataProcessorService {


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        DatasetItem goalsDSItem = (DatasetItem) (getInputData("dataset-item")).getPayload();
        Data playData = getInputDataSet("play-data");

        JSONContainer currentGoals = api.queryDatasetItem(goalsDSItem.getDatasetId(), goalsDSItem.getName());
        JSONArray currentGoalsJson = currentGoals.getJSONObject().getJSONArray("data");

        /**
         * GOAL datasetItem / JSON
         *
         #	game	goalAmount	goalType	progressAmount	progressPercent	start	stop	timeAchieved
         0	ANY	30	TIME	0	0	1393632000	1393718400	0
         1	ANY	30	TIME	0	0	1393718400	1393804800	0
         2	ANY	30	TIME	0	0	1393804800	1393891200	0
         3	ANY	30	TIME	0	0	1393891200	1393977600	0
         4	ANY	30	TIME	0	0	1393977600	1394064000	0
         5	ANY	30	TIME	0	0	1394064000	1394150400	0
         6	ANY	30	TIME	0	0	1394150400	1394236800	0
         */


        /**
         * PLAY DATA org.pipeline.data
         *
         * GameAttemptDateTime,GameAttemptDuration,Completed,Game,Difficulty,Level,TotalScore,Rating
         * 2014-03-04T10:26:56,47,true,GAME_BALLOONMODELLING,EASY,1,4000,RATING_3BRONZE
         * 2014-03-04T10:27:56,66,true,GAME_BALLOONMODELLING,EASY,2,7000,RATING_3BRONZE
         */


        SimpleDateFormat dateFormatFromPlayData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        //Go through the goals, trying to find any which apply to the date that the game was played
        for (int i = 0; i < currentGoalsJson.length(); i++) {
            JSONObject goal = currentGoalsJson.getJSONObject(i);

            //For all of the levels that come in the play data check the row against the goal
            for (int j = 0; j < playData.getLargestRows(); j++) {

                //Dates of the goal and the game
                String gameAttemptDateTimeString = playData.column("GameAttemptDateTime").getStringValue(j);
                Date gameAttemptDateTime = dateFormatFromPlayData.parse(gameAttemptDateTimeString);
                if (dateInRangeOfGoal(goal, gameAttemptDateTime)) {

                    //Does the game match or is the goal set to "ANY"
                    String goalGame = goal.getString("game");
                    String playDataGame = playData.column("Game").getStringValue(j);
                    if (goalGame.equalsIgnoreCase("ANY") || goalGame.equalsIgnoreCase(playDataGame)) {

                        String goalType = goal.getString("goalType");
                        Long updatedProgressAmount = 0L;
                        Long initialProgressAmount = goal.getLong("progressAmount");

                        //Calculate the new time played or total score
                        if (goalType.equalsIgnoreCase("TIME")) {
                            Long duration = Long.parseLong(playData.column("GameAttemptDuration").getStringValue(j));
                            updatedProgressAmount = initialProgressAmount + duration;
                        }
                        else if(goalType.equalsIgnoreCase("SCORE")){
                            Long score = Long.parseLong(playData.column("TotalScore").getStringValue(j));
                            updatedProgressAmount = initialProgressAmount + score;
                        }
                        else{
                            throw new NotImplementedException();
                        }
                        //update the JSON
                        goal.put("progressAmount", updatedProgressAmount);

                        //Calculate % complete
                        Long goalAmount = goal.getLong("goalAmount");
                        int percentage = (int) (updatedProgressAmount * 100.0 / goalAmount + 0.5);
                        goal.put("progressPercent", percentage);

                        if (percentage >= 100) {
                            goal.put("timeAchieved", gameAttemptDateTime.getTime());
                        }

                        //Update the JSON on the server
                        api.updateDatasetItem(goalsDSItem.getDatasetId(), goalsDSItem.getName(), goal.getLong("_id"), new JSONContainer(goal));

                        //Output what we've done
                        System.out.println("*************************************");
                        System.out.println("Updated Goal for patient: " + api.getCurrentUser().getDisplayName());
                        System.out.println(goal);
                        System.out.println("MATCHED");
                        for (int k = 0; k < playData.getColumnCount(); k++) {
                            System.out.print(playData.column(k).getStringValue(j) + ", ");
                        }
                        System.out.println("\n");

                    }
                }
            }
        }
    }

    public boolean dateInRangeOfGoal(JSONObject goal, Date date) {
        try {
            Long goalStartDate = goal.getLong("start");
            Long goalStopDate = goal.getLong("stop");
            Long gameAttemptDate = date.getTime() / 1000; //Millis to seconds

            if (gameAttemptDate > goalStartDate && gameAttemptDate < goalStopDate) {
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }
}
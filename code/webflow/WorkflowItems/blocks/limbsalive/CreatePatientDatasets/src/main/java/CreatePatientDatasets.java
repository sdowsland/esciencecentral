/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.model.datasets.items.SingleValueItem;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.datasets.items.single.DoubleValueItem;
import com.connexience.server.model.datasets.items.single.SingleJsonRowItem;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import org.pipeline.core.data.Data;

import java.util.ArrayList;
import java.util.List;

public class CreatePatientDatasets extends CloudDataProcessorService {
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the service has not been configured to accept
     * streaming data or once for each chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {

        Data userIdDataset = getInputDataSet("user-id");
        String userId = userIdDataset.column(0).getStringValue(0);
        String clinicianId = userIdDataset.column(1).getStringValue(0);
        User clinician = new User();
        clinician.setId(clinicianId);

        // Create an API running as the new user
        User targetUser = new User();
        targetUser.setId(userId);
        APIBroker userApi = createAdditionalApiLink(targetUser);

        // Get the dataset
        if (!userApi.userHasNamedDataset("LimbsAliveGameData")) {
            // Need to create data
            Dataset gameData = new Dataset();
            gameData.setName("LimbsAliveGameData");
            gameData.setDescription("Datset to capture LimbsAlive game data");
            gameData = userApi.saveDataset(gameData);

            List<String> games = new ArrayList<>();

            games.add("GAME_BALLOONMODELLING");
            games.add("GAME_CLOWNINGAROUND");
            games.add("GAME_HIGHDIVE");
            games.add("GAME_JUGGLING");
            games.add("GAME_KNIFETHROW");
            games.add("GAME_PLATESPIN");
            games.add("GAME_ROLLINGGLOBE");
            games.add("GAME_TEETERBOARD");
            games.add("GAME_TIGERTRAINING");
            games.add("GAME_TRAPEZE");

            for (String game : games) {
                MultipleValueItem item = new JsonMultipleValueItem();
                item.setName(game);
                item.setDatasetId(gameData.getId());

                userApi.saveDatasetItem(item);
            }


            userApi.grantObjectPermission(gameData, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("CAHAI")) {
            // Cahai dataset
            Dataset cahai = new Dataset();
            cahai.setName("CAHAI");
            cahai.setDescription("This dataset contains a CAHAI history for the patient");
            cahai = userApi.saveDataset(cahai);

            DatasetItem cahaiDatasetItem = new JsonMultipleValueItem();
            cahaiDatasetItem.setName("CAHAI");
            cahaiDatasetItem.setDatasetId(cahai.getId());
            cahaiDatasetItem = userApi.saveDatasetItem(cahaiDatasetItem);

            userApi.grantObjectPermission(cahai, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("PlayerSummary")) {
            // Summary dataset.  This is used to build the clinician summery table
            Dataset summary = new Dataset();
            summary.setName("PlayerSummary");
            summary.setDescription("This dataset contains summary data for the patient");
            summary = userApi.saveDataset(summary);

            //Items for all the summary data
            DoubleValueItem averagePlayTime = new DoubleValueItem();
            averagePlayTime.setName("AveragePlayTime_" + userId);
            averagePlayTime.setUpdateStrategy(SingleValueItem.UPDATE_CALCULATES_AVERAGE);
            averagePlayTime.setDatasetId(summary.getId());
            userApi.saveDatasetItem(averagePlayTime);

            DoubleValueItem totalPlayTime = new DoubleValueItem();
            totalPlayTime.setName("TotalPlayTime_" + userId);
            totalPlayTime.setUpdateStrategy(SingleValueItem.UPDATE_CALCULATES_SUM);
            totalPlayTime.setDatasetId(summary.getId());
            userApi.saveDatasetItem(totalPlayTime);

            DoubleValueItem totalAssesements = new DoubleValueItem();
            totalAssesements.setName("TotalAssesements_" + userId);
            totalAssesements.setUpdateStrategy(SingleValueItem.UPDATE_CALCULATES_SUM);
            totalAssesements.setDatasetId(summary.getId());
            userApi.saveDatasetItem(totalAssesements);

            SingleJsonRowItem lastPlay = new SingleJsonRowItem();
            lastPlay.setName("LastPlayTime_" + userId);
            lastPlay.setUpdateStrategy(SingleValueItem.UPDATE_REPLACES_VALUES);
            lastPlay.setDatasetId(summary.getId());
            userApi.saveDatasetItem(lastPlay);

            DoubleValueItem lastCahai = new DoubleValueItem();
            lastCahai.setName("LastCahai_" + userId);
            lastCahai.setDatasetId(summary.getId());
            lastCahai.setUpdateStrategy(SingleValueItem.UPDATE_REPLACES_VALUES);
            userApi.saveDatasetItem(lastCahai);

            userApi.grantObjectPermission(summary, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("FUGL-MEYER")) {
            // Fugl-Meyer dataset
            Dataset fuglmeyer = new Dataset();
            fuglmeyer.setName("FUGL-MEYER");
            fuglmeyer.setDescription("This dataset contains a Fugl-Meyer history for the patient");
            fuglmeyer = userApi.saveDataset(fuglmeyer);

            DatasetItem fuglmeyerDatasetItem = new JsonMultipleValueItem();
            fuglmeyerDatasetItem.setName("FUGL-MEYER");
            fuglmeyerDatasetItem.setDatasetId(fuglmeyer.getId());
            fuglmeyerDatasetItem = userApi.saveDatasetItem(fuglmeyerDatasetItem);

            userApi.grantObjectPermission(fuglmeyer, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("MOCA")) {
            // MOCA dataset
            Dataset moca = new Dataset();
            moca.setName("MOCA");
            moca.setDescription("This dataset contains a MOCA history for the patient");
            moca = userApi.saveDataset(moca);

            DatasetItem mocaItem = new JsonMultipleValueItem();
            mocaItem.setName("MOCA");
            mocaItem.setDatasetId(moca.getId());
            mocaItem = userApi.saveDatasetItem(mocaItem);

            userApi.grantObjectPermission(moca, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("AHUR")) {
            // AHUR dataset
            Dataset ahur = new Dataset();
            ahur.setName("AHUR");
            ahur.setDescription("This dataset contains a AHUR history for the patient");
            ahur = userApi.saveDataset(ahur);

            DatasetItem ahurItem = new JsonMultipleValueItem();
            ahurItem.setName("AHUR");
            ahurItem.setDatasetId(ahur.getId());
            ahurItem = userApi.saveDatasetItem(ahurItem);

            userApi.grantObjectPermission(ahur, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("MAS")) {
            // AHUR dataset
            Dataset mas = new Dataset();
            mas.setName("MAS");
            mas.setDescription("This dataset contains a MAS history for the patient");
            mas = userApi.saveDataset(mas);

            DatasetItem masItem = new JsonMultipleValueItem();
            masItem.setName("MAS");
            masItem.setDatasetId(mas.getId());
            masItem = userApi.saveDatasetItem(masItem);

            userApi.grantObjectPermission(mas, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("PAIN")) {
            // AHUR dataset
            Dataset pain = new Dataset();
            pain.setName("PAIN");
            pain.setDescription("This dataset contains a PAIN history for the patient");
            pain = userApi.saveDataset(pain);

            DatasetItem painItem = new JsonMultipleValueItem();
            painItem.setName("PAIN");
            painItem.setDatasetId(pain.getId());
            painItem = userApi.saveDatasetItem(painItem);

            userApi.grantObjectPermission(pain, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("Progress")) {
            // Progress / Goal dataset
            Dataset progress = new Dataset();
            progress.setName("Progress");
            progress.setDescription("This dataset contains progress and goal tracking for the patient");
            progress = userApi.saveDataset(progress);

            DatasetItem dailyPlayItem = new JsonMultipleValueItem();
            dailyPlayItem.setName("dailyPlay");
            dailyPlayItem.setDatasetId(progress.getId());
            dailyPlayItem = userApi.saveDatasetItem(dailyPlayItem);

            DatasetItem goalItem = new JsonMultipleValueItem();
            goalItem.setName("goals");
            goalItem.setDatasetId(progress.getId());
            goalItem = userApi.saveDatasetItem(goalItem);

            userApi.grantObjectPermission(progress, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("EHI")) {
            // EHI Score
            Dataset EHI = new Dataset();
            EHI.setName("EHI");
            EHI.setDescription("Patient EHI tracking dataset");
            EHI = userApi.saveDataset(EHI);

            DatasetItem ehiItem = new JsonMultipleValueItem();
            ehiItem.setName("EHI");
            ehiItem.setDatasetId(EHI.getId());
            ehiItem = userApi.saveDatasetItem(ehiItem);

            userApi.grantObjectPermission(EHI, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("NIH")) {
            // EHI Score
            Dataset NIH = new Dataset();
            NIH.setName("NIH");
            NIH.setDescription("Patient NIH tracking dataset");
            NIH = userApi.saveDataset(NIH);

            DatasetItem nihItem = new JsonMultipleValueItem();
            nihItem.setName("NIH");
            nihItem.setDatasetId(NIH.getId());
            nihItem = userApi.saveDatasetItem(nihItem);

            userApi.grantObjectPermission(NIH, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("VISUAL-NEGLECT")) {
            // EHI Score
            Dataset visualNeglect = new Dataset();
            visualNeglect.setName("VISUAL-NEGLECT");
            visualNeglect.setDescription("Patient VISUAL-NEGLECT tracking dataset");
            visualNeglect = userApi.saveDataset(visualNeglect);

            DatasetItem visualNeglectItem = new JsonMultipleValueItem();
            visualNeglectItem.setName("VISUAL-NEGLECT");
            visualNeglectItem.setDatasetId(visualNeglect.getId());
            visualNeglectItem = userApi.saveDatasetItem(visualNeglectItem);

            userApi.grantObjectPermission(visualNeglect, clinician, Permission.READ_PERMISSION);
        }

        if (!userApi.userHasNamedDataset("SOCIAL-SITUATION")) {
            // EHI Score
            Dataset socialSupport = new Dataset();
            socialSupport.setName("SOCIAL-SITUATION");
            socialSupport.setDescription("Patient SOCIAL-SITUATION tracking dataset");
            socialSupport = userApi.saveDataset(socialSupport);

            DatasetItem socialSupportItem = new JsonMultipleValueItem();
            socialSupportItem.setName("SOCIAL-SITUATION");
            socialSupportItem.setDatasetId(socialSupport.getId());
            socialSupportItem = userApi.saveDatasetItem(socialSupportItem);

            userApi.grantObjectPermission(socialSupport, clinician, Permission.READ_PERMISSION);
        }
        setOutputDataSet("user-id", userIdDataset);
    }

    public void allDataProcessed() throws Exception {
    }
}
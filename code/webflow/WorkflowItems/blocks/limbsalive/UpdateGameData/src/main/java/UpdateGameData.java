
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.json.JSONObject;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.ColumnPicker;

public class UpdateGameData extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();
        Data playData = getInputDataSet("play-data");
        String dsName = getEditableProperties().stringValue("DatasetName", "LimbsAliveGameData");
        String itemName;
        JSONObject row;
        JSONContainer rowContainer;
        String gameNameColumn = getEditableProperties().stringValue("GameNameColumn", "#2");
        ColumnPicker gamePicker = new ColumnPicker(gameNameColumn);
        Column nameColumn = gamePicker.pickColumn(playData);
        
        Dataset ds = api.lookupUserDatasetByName(dsName);
        if(ds!=null){
            for(int i=0;i<nameColumn.getRows();i++){
                if(!nameColumn.isMissing(i)){
                    itemName = nameColumn.getStringValue(i);
                    System.out.println("Updating: " + itemName);
                    api.updateDatasetItem(ds.getId(), itemName, new JSONContainer(dataRowToJson(playData, i)));
                }
            }
            
        } else {
            throw new Exception("No such dataset: " + dsName);
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
    
    /** Save a data row to a JSON Object */
    public JSONObject dataRowToJson(Data data, int row) throws Exception {
        JSONObject dataRow = new JSONObject();
        Column c;
        String name;
        for(int i=0;i<data.getColumns();i++){
            c = data.column(i);
            name = c.getName().replace(" ", "_");
            if(!c.isMissing(row)){
                dataRow.put(name, c.getObjectValue(row));
            } else {
                dataRow.put(name, (Object)null);
            }
        }
        
        return dataRow;
    }    
}
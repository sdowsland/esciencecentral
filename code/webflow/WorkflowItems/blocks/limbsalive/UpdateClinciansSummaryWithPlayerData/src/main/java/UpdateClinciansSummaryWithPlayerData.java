
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.security.User;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONObject;
import org.pipeline.core.data.*;

public class UpdateClinciansSummaryWithPlayerData extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {

        APIBroker api = createApiLink();
        User patient = api.getCurrentUser();
        Dataset ds = api.lookupUserDatasetByName(getEditableProperties().stringValue("DatasetName", "PlayerSummary"));
        if (ds != null) {
            Data playDataCSV = getInputDataSet("input-data");

            StringPairListWrapper mapping = (StringPairListWrapper) getProperties().xmlStorableValue("ColumnToDSNameMapping");

            //for every row in the input dataset
            for (int i = 0; i < playDataCSV.getLargestRows(); i++) {

                //for every mapping that we're looking for
                for (int j = 0; j < mapping.getSize(); j++) {

                    //each mapping goes into a different dataset item
                    String dsItemName = mapping.getValue(j, 1) + "_" + patient.getId();

                    //Get the input data column from the parameters map, first column
                    String columnName = mapping.getValue(j, 0);
                    Column dataColumn = playDataCSV.column(columnName);

                    //get the value for this row
                    String dataToPutInDS = dataColumn.getStringValue(i);

                    if (NumberUtils.isNumber(dataToPutInDS)) {
                        api.updateDatasetItem(ds.getId(), dsItemName, Double.valueOf(dataToPutInDS));

                    } else {
                        JSONObject json = new JSONObject();
                        json.put("value", dataToPutInDS);
                        JSONContainer jsonContainer = new JSONContainer(json);
                        api.updateDatasetItem(ds.getId(), dsItemName, jsonContainer);
                    }
                }
            }
        } else {
            throw new Exception("User does not have a data set called: " + getEditableProperties().stringValue("DatasetName", "PlayerSummary"));
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}


/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.pipeline.core.data.*;

public class SRDToText implements WorkflowBlock {

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        List<File> outputFiles = new ArrayList<File>();
        List<File> inputFiles = inputs.getInputFiles("input-files");
        File subDir = new File(env.getWorkingDirectory(), env.getStringProperty("OutputSubdirectory", "raw"));
        boolean useSubDir = env.getBooleanProperty("UseSubdirectory", true);
        subDir.mkdir();
        
        for (File inputFile : inputFiles) {
            String outputFileName;
            if(useSubDir){
                outputFileName = env.getStringProperty("OutputSubdirectory", "raw") + File.separator + inputFile.getName() + ".txt";
            } else {
                outputFileName = inputFile.getName() + ".txt";
            }
            
            File outputFile = new File(outputFileName);

            File ccParserLibDir = env.getDependencyDirectory("CCParserLib");

            List<String> command = new ArrayList<String>();
            if(!isWindows()){
                command.add("mono");
            }
            command.add(ccParserLibDir + File.separator + "WorkflowSRDRaw.exe");
            command.add(inputFile.getAbsolutePath());
            command.add(outputFile.getAbsolutePath());

            for (String s : command) {
                System.out.println(s + " ");
            }

            //Call Parser
            String[] commandArray = command.toArray(new String[command.size()]);
            ProcessBuilder pb = new ProcessBuilder(commandArray);
            Process process = pb.start();


            //Redirect the STD OUT and ERR so that they are output to the workflow block OUT and ERR
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String errline, outline;
            while ((outline = br.readLine()) != null) {
                System.out.println(outline);
            }

            while ((errline = errReader.readLine()) != null) {
                System.err.println(errline);
            }

            //Wait for the process to finish
            try {
                int exitValue = process.waitFor();
                System.out.println("\n\nExit Value is " + exitValue);
                if (exitValue != 0) {

                    //don't fail the entire list if one file fails
                    System.err.println("Non zero exit value when converting" + inputFile.getAbsolutePath() + " : " + exitValue);
                    continue;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
            if(outputFile.exists()){
                outputFiles.add(outputFile);
            }
        }

        outputs.setOutputFiles("text-files", outputFiles);
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {
    }

    public static boolean isWindows()
    {
        return System.getProperty("os.name").startsWith("Windows");
    }
}
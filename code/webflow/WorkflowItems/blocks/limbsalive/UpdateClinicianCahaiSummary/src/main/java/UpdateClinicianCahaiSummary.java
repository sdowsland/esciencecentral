
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;

public class UpdateClinicianCahaiSummary extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        Data cahaiData = getInputDataSet("cahai-score");
        Column cahaiColumn = cahaiData.column(1);
        double score = ((NumericalColumn)cahaiColumn).getDoubleValue(cahaiColumn.getRows() - 1);

        // Now find the dataset
        User patient = api.getCurrentUser();
        Dataset ds = api.lookupUserDatasetByName(getEditableProperties().stringValue("DatasetName", "PlayerSummary"));
        if(ds!=null){
            
            String lastCahaiItemName = "LastCahai_" + patient.getId();
            api.updateDatasetItem(ds.getId(), lastCahaiItemName, score);
            
            String assesementCountName = "TotalAssesements_" + patient.getId();
            api.updateDatasetItem(ds.getId(), assesementCountName, cahaiColumn.getRows());
            
        } else {
            throw new Exception("No player summary dataset exists");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
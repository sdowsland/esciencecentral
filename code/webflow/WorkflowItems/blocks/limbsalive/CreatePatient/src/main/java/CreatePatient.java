
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.items.SingleValueItem;
import com.connexience.server.model.datasets.items.single.DoubleValueItem;
import com.connexience.server.model.datasets.items.single.SingleJsonRowItem;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.User;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.pipeline.core.data.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

public class CreatePatient extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        try {
            APIBroker api = createApiLink();

            //Get the basic properties
            String clinicianId = getEditableProperties().stringValue("clinicianId", null);
            String patientCode = getEditableProperties().stringValue("patientCode", null);
            String password = getEditableProperties().stringValue("password", null);

            if (patientCode != null && password != null && clinicianId != null) {

                //Create the account and an API for this account
                User u = api.createAccount(patientCode, patientCode, patientCode, password);
                APIBroker newAPI = createAdditionalApiLink(u);

                //Start adding the metadata
                MetadataCollection mdc = new MetadataCollection();
                mdc.add(new TextMetadata("LimbsAlive", "ClinicianID", clinicianId));
                mdc.add(new TextMetadata("LimbsAlive", "UserType", "Patient"));
                mdc.add(new BooleanMetadata("LimbsAlive", "GAME_ENABLED", false));

                //Add a review date for one weeks time.
                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);
                now.set(Calendar.MILLISECOND, 0);

                //add a week
                now.add(Calendar.WEEK_OF_YEAR, 1);
                mdc.add(new DateMetadata("LimbsAlive", "nextReviewDate", now.getTime()));

                // Make the user a member of the clinicians patients group
                Group g = api.getGroupByName("Patients_" + clinicianId);
                if (g != null) {
                    api.addUserToGroup(g.getId(), u.getId());
                }

                Group wfGroup = api.getGroupByName("WorkflowAPI");
                if (wfGroup != null) {
                    api.addUserToGroup(wfGroup.getId(), u.getId());
                }

                Group storageGroup = api.getGroupByName("StorageAPI");
                if (storageGroup != null) {
                    api.addUserToGroup(storageGroup.getId(), u.getId());
                }

                Group allPatientsGroup = api.getGroupByName("AllPatients");
                if (allPatientsGroup != null) {
                    api.addUserToGroup(allPatientsGroup.getId(), u.getId());
                }

                //Add all of the properties of a patient as Metadata and upload it
                mdc = createPatientMetadata(mdc);
                newAPI.uploadMetadata(u, mdc);

                //Set the new UserId (patient) as the block output
                Data result = new Data();
                result.addSingleValue("UserID", u.getId());
                result.addSingleValue("ClinicianID", clinicianId);
                setOutputDataSet("user-id", result);
            } else {
                throw new Exception("No username and/or password set");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Create a MetaDataCollection from the Limbs Alive properties that we're interested in
     * @param mdc MetaDataCollection to have the items added to
     * @return mdc with the items added
     * @throws ParseException The strokeDate couldn't be parsed
     */
    private MetadataCollection createPatientMetadata(MetadataCollection mdc) throws ParseException {

        Integer age = getEditableProperties().intValue("patientAge", 0);
        if(age!=-1){
            mdc.add(new NumericalMetadata("LimbsAlive", "Age", age));
        }

        String patientGender = getEditableProperties().stringValue("patientGender", null);
        if(patientGender!=null && !patientGender.isEmpty()){
            mdc.add(new TextMetadata("LimbsAlive", "patientGender", patientGender));
        }

        String sideOfHemiplegia = getEditableProperties().stringValue("sideOfHemiplegia", null);
        if(sideOfHemiplegia!=null && !sideOfHemiplegia.isEmpty()){
            mdc.add(new TextMetadata("LimbsAlive", "sideOfHemiplegia", sideOfHemiplegia));
        }

//        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

//        String strokeDate = getEditableProperties().stringValue("strokeDate", null);
//        if(strokeDate!=null && !strokeDate.isEmpty()){
//            mdc.add(new DateMetadata("LimbsAlive", "strokeDate", sdf.parse(strokeDate)));
//        }

//        String strokeLocation = getEditableProperties().stringValue("strokeLocation", null);
//        if(strokeLocation!=null && !strokeLocation.isEmpty()){
//            mdc.add(new TextMetadata("LimbsAlive", "strokeLocation", strokeLocation));
//        }
//
//        String strokeLocationOther = getEditableProperties().stringValue("strokeLocationOther", null);
//        if(strokeLocationOther!=null && !strokeLocationOther.isEmpty()){
//            mdc.add(new TextMetadata("LimbsAlive", "strokeLocationOther", strokeLocationOther));
//        }



//        String preStrokeDominantHand = getEditableProperties().stringValue("preStrokeDominantHand", null);
//        if(preStrokeDominantHand!=null && !preStrokeDominantHand.isEmpty()){
//            mdc.add(new TextMetadata("LimbsAlive", "PreStrokeDominantHand", preStrokeDominantHand));
//        }
//
//        Integer leftArmLength = getProperties().intValue("leftArmLength", -1);
//        if(leftArmLength>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "leftArmLength", leftArmLength));
//        }
//
//        Integer rightArmLength = getProperties().intValue("rightArmLength", -1);
//        if(rightArmLength>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "rightArmLength", rightArmLength));
//        }
//
//        Double  ashworthScale = getProperties().doubleValue("ashworthScale", -1.0);
//        if(ashworthScale>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "ashworthScale", ashworthScale));
//        }


//        Integer nihStrokeScale = getProperties().intValue("nihStrokeScale", -1);
//        if(nihStrokeScale>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "nihStrokeScale", nihStrokeScale));
//        }
//
//        Integer barthelIndex = getProperties().intValue("barthelIndex", -1);
//        if(barthelIndex>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "barthelIndex", barthelIndex));
//        }
//
//        Integer painScore = getProperties().intValue("painScore", -1);
//        if(painScore>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "painScore", painScore));
//        }
//
//        Integer visualNeglectScore = getProperties().intValue("visualNeglectScore", -1);
//        if(visualNeglectScore>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "visualNeglectScore", visualNeglectScore));
//        }
//
//        String socialSupportScore = getProperties().stringValue("socialSupportScore", null);
//        if(socialSupportScore!=null && !socialSupportScore.isEmpty()){
//            mdc.add(new TextMetadata("LimbsAlive", "socialSupportScore", socialSupportScore));
//        }
//
//        String socialSupportScoreNotes = getProperties().stringValue("socialSupportScoreNotes", null);
//        if(socialSupportScoreNotes!=null && !socialSupportScoreNotes.isEmpty()){
//            mdc.add(new TextMetadata("LimbsAlive", "socialSupportScoreNotes", socialSupportScoreNotes));
//        }
//
//        Integer baselineCahai = getProperties().intValue("baselineCahai", -1);
//        if(baselineCahai>=0){
//            mdc.add(new NumericalMetadata("LimbsAlive", "baselineCahai", baselineCahai));
//        }

        return mdc;
    }
}
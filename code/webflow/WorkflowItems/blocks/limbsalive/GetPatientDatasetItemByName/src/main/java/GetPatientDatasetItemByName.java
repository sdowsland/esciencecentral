
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
*/
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.datasets.items.single.DoubleValueItem;
import com.connexience.server.model.datasets.items.single.SingleJsonRowItem;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import org.pipeline.core.data.*;

public class GetPatientDatasetItemByName extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        String datasetName = getEditableProperties().stringValue("DatasetName", null);
        String itemName = getEditableProperties().stringValue("ItemName", null);
        boolean autoCreate = getEditableProperties().booleanValue("CreateIfNotPresent", true);
        String itemType = getEditableProperties().stringValue("ItemToCreate", "MULTI_ROW");
        String userId = getEditableProperties().stringValue("UserID", null);
        
        if(userId!=null && datasetName!=null && itemName!=null && !userId.isEmpty() && !datasetName.isEmpty() && ! itemName.isEmpty()){
            User u = new User();
            u.setId(userId);
            APIBroker userApi = createAdditionalApiLink(u);
            if(autoCreate){
                if(userApi.userHasNamedDataset(datasetName)){
                    Dataset ds = userApi.lookupUserDatasetByName(datasetName);
                    DatasetItem item = userApi.lookupUserDatasetItemByName(datasetName, itemName);
                    if(item!=null){
                        // Already there
                        setOutputData("dataset-item", new ObjectWrapper(item));                
                        
                    } else {
                        // Create
                        System.out.println("Creating dataset item: " + itemName + " of type: " + itemType);
                        item = createItem(itemType, itemName, ds);
                        item = userApi.saveDatasetItem(item);
                        setOutputData("dataset-item", new ObjectWrapper(item));                        
                    }                    
                    
                } else {
                    // Need to create everything
                    System.out.println("Creating dataset: " + datasetName);
                    Dataset ds = new Dataset();
                    ds.setName(datasetName);
                    ds.setDescription("Automatically created by GetDatasetItemByName workflow block");
                    ds = userApi.saveDataset(ds);
                    
                    // Now the item
                    System.out.println("Creating dataset item: " + itemName + " of type: " + itemType);
                    DatasetItem item = createItem(itemType, itemName, ds);
                    item = userApi.saveDatasetItem(item);
                    setOutputData("dataset-item", new ObjectWrapper(item));
                }
                
            } else {
                // Data set has to exist
                Dataset ds = userApi.lookupUserDatasetByName(datasetName);
                if(ds!=null){
                    DatasetItem item = userApi.lookupUserDatasetItemByName(datasetName, itemName);
                    if(item!=null){
                        setOutputData("dataset-item", new ObjectWrapper(item));                
                    } else {
                        throw new Exception("Item: " + itemName + " does not exist in dataset: " + datasetName);
                    }
                } else {
                    throw new Exception("Dataset: " + datasetName + " does not exist");
                }
            }
        } else {
            throw new Exception("Dataset not specified correctly");
        }
    }

    private DatasetItem createItem(String itemType, String name, Dataset parent){
         DatasetItem item;
         if(itemType.equals("SINGLE_ROW")){
             item = new SingleJsonRowItem();

         } else if(itemType.equals("SINGLE_NUMBER")){
             item = new DoubleValueItem();

         } else {
             item = new JsonMultipleValueItem();

         }        
         item.setDatasetId(parent.getId());
         item.setName(name);
         return item;
    }
    
    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
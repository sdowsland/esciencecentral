
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.DataWrapper;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.StringColumn;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ParsePLDService extends CloudDataProcessorService {

    public void execute() throws Exception {

        FileWrapper pldFileWrapper = ((FileWrapper) getInputData("pld-file"));

        Data gameData = new Data();

        if (pldFileWrapper.getFileCount() > 1) {
            throw new ConnexienceException("ParsePLDFile can only be called with one file at present");
        }

        int i = 0;
        File pldFile = pldFileWrapper.getFile(i);

        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(pldFile);

        Element serializablePlayData = doc.getRootElement();
        Element playDataList = serializablePlayData.getChild("playDataList");

        for (Element playData : playDataList.getChildren("PlayData")) {

            Element gameAttemptResult = playData.getChild("GameAttemptResult");
            getOrCreateColumn(playData, gameData, "GameAttemptDateTime");
            getOrCreateColumn(playData, gameData, "GameAttemptDuration");
            getOrCreateColumn(playData, gameData, "Completed");

            getOrCreateColumn(gameAttemptResult, gameData, "Game");
            getOrCreateColumn(gameAttemptResult, gameData, "Difficulty");
            getOrCreateColumn(gameAttemptResult, gameData, "Level");
            getOrCreateColumn(gameAttemptResult, gameData, "TotalScore");
            getOrCreateColumn(gameAttemptResult, gameData, "Rating");

        }

        setOutputDataSet("output-1", gameData);
    }

    private Column getOrCreateColumn(Element element, Data data, String columnName) throws DataException, ConnexienceException {
        //Get the text from the XML
        String childText = element.getChildText(columnName);
        if (childText != null && !childText.equals("")) {
            Column column;
            //If the dataset doesn't contain a column for this text then create one
            if (!data.containsColumn(columnName)) {
                //todo: guess column type
                column = new StringColumn(columnName);
                data.addColumn(column);
            } else {
                column = data.column(columnName);
            }
            //Add the value from the XML file to the column
            column.appendObjectValue(childText);
            return column;
        } else {
            throw new ConnexienceException("Cannot find Game data for " + columnName);
        }
    }
}
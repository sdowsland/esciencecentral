
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.security.User;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.pipeline.core.data.*;

public class ManualUpdateCahaiDataset extends CloudDataProcessorService {

    public void execute() throws Exception {
        String userId = getEditableProperties().stringValue("userId", "0");
        
        User patient = new User();
        patient.setId(userId);
        APIBroker userApi = createAdditionalApiLink(patient);
        
        DatasetItem item = (DatasetItem)((ObjectWrapper)getInputData("dataset-item")).getPayload();

        JSONObject jsonRow = addManualCahai();
        userApi.updateDatasetItem(item.getDatasetId(), item.getName(), new JSONContainer(jsonRow.toString()));

        //If the user doesn't have a baseline cahai set then add this score as the baseline
        MetadataCollection currentMetadata  = userApi.getMetadata(patient);
        MetadataItem baselineCahai = currentMetadata.find("LimbsAlive", "baselineCahai");
        if(baselineCahai == null  || baselineCahai.getStringValue() == null || baselineCahai.getStringValue().equals("") || baselineCahai.getStringValue().equals("0"))
        {
            baselineCahai = new NumericalMetadata("LimbsAlive", "baselineCahai", getProperties().intValue("chTotal", 0));
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(baselineCahai);
            userApi.uploadMetadata(patient, mdc);
        }

        Data value = new Data();
        Integer chTotal = getProperties().intValue("chTotal", 0);
        value.addSingleValue("UserID", userId);
        value.addSingleValue("Value", chTotal);
        setOutputDataSet("cahai-details", value);
    }


    private JSONObject addManualCahai() throws JSONException {
        String assessmentDate = getProperties().stringValue("assessmentDate", "");
        Integer openJar = getProperties().intValue("openJar", 0);
        Integer call911 = getProperties().intValue("call911", 0);
        Integer drawLine = getProperties().intValue("drawLine", 0);
        Integer pourWater = getProperties().intValue("pourWater", 0);
        Integer wringCloth = getProperties().intValue("wringCloth", 0);
        Integer doUpButtons = getProperties().intValue("doUpButtons", 0);
        Integer dryBack = getProperties().intValue("dryBack", 0);
        Integer useToothpaste = getProperties().intValue("useToothpaste", 0);
        Integer cutPutty = getProperties().intValue("cutPutty", 0);
        Integer chTotal = getProperties().intValue("chTotal", 0);


        JSONObject jsonRow = new JSONObject();
        jsonRow.put("openJar", openJar);
        jsonRow.put("call911", call911);
        jsonRow.put("drawLine", drawLine);
        jsonRow.put("pourWater", pourWater);
        jsonRow.put("wringCloth", wringCloth);
        jsonRow.put("doUpButtons", doUpButtons);
        jsonRow.put("dryBack", dryBack);
        jsonRow.put("useToothpaste", useToothpaste);
        jsonRow.put("cutPutty", cutPutty);
        jsonRow.put("score", chTotal);

//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date dt = new Date();
        jsonRow.put("time", assessmentDate);
        jsonRow.put("milliseconds", dt.getTime());
        jsonRow.put("source", "manual");

        return jsonRow;
    }    
}
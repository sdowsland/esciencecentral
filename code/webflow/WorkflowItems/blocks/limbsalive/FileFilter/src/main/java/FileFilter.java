
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.pipeline.core.data.*;

public class FileFilter extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();
        LinkWrapper inFiles = (LinkWrapper)getInputData("input-references");
        String name;
        String movementName;
        Date originalMovementDate;
        Date currentMovementDate;
        
        
        // List of unique movement names
        HashMap<String, String> latestMovements = new HashMap<String, String>();
        
        for(int i=0;i<inFiles.size();i++){
            name = inFiles.getItem(i).getServerObject().getName();
            movementName = getMovementName(name);
            
            if(!latestMovements.containsKey(movementName)){
                // New movement
                latestMovements.put(movementName, name);
                
            } else {
                // Get the date and compare with this one
                originalMovementDate = parseDate(latestMovements.get(movementName));
                currentMovementDate = parseDate(name);
                
                if(currentMovementDate.after(originalMovementDate)){
                    // Newer move
                    latestMovements.put(movementName, name);
                }
            }
        }
        
        LinkWrapper outFiles = new LinkWrapper();
        Iterator<String> i = latestMovements.values().iterator();
        ServerObject locatedObject;
        
        while(i.hasNext()){
            name = i.next();
            locatedObject = null;
            for(int j=0;j<inFiles.size();j++){
                if(inFiles.getItem(j).getServerObject().getName().equals(name)){
                    locatedObject = inFiles.getItem(j).getServerObject();
                }
            }
            if(locatedObject instanceof DocumentRecord){
                outFiles.addDocument((DocumentRecord)locatedObject, api.getLatestVersion(locatedObject.getId()));
            }
        }
        setOutputData("filtered-references", outFiles);
    }

    private Date parseDate(String name) throws Exception {
        // Find first "-"
        // Find first "."
        try {
            int pos1 = name.indexOf("-");
            int pos2 = name.indexOf(".srd");
            String dateText = name.substring(pos1 + 1, pos2 - 1);
            SimpleDateFormat fmt = new SimpleDateFormat("HH.mm.ss");
            return fmt.parse(dateText);
            
        } catch (Exception e){
            System.out.println("Date parse error on: " + name);
            return null;
        }
        
    }
    
    private String getMovementName(String name){
        int pos = name.indexOf("-");
        String movementName = name.substring(0, pos);
        return movementName;
    }
    
    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.security.User;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.pipeline.core.data.*;

public class ManualUpdateFMataset extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        String userId = getEditableProperties().stringValue("userId", "0");        
        User patient = new User();
        patient.setId(userId);
        APIBroker userApi = createAdditionalApiLink(patient);
        
        DatasetItem item = (DatasetItem)((ObjectWrapper)getInputData("dataset-item")).getPayload();


        JSONObject jsonRow = addManualFM();
        userApi.updateDatasetItem(item.getDatasetId(), item.getName(), new JSONContainer(jsonRow.toString()));
        
        Data value = new Data();
        Integer chTotal = getProperties().intValue("fmTotal", 0);
        value.addSingleValue("UserID", userId);
        value.addSingleValue("Value", chTotal);
        setOutputDataSet("fm-details", value);
    }


    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
    
    private JSONObject addManualFM() throws JSONException {
       String assessmentDate = getProperties().stringValue("assessmentDate", "");
        Integer upperExtremity = getProperties().intValue("upperExtremity", 0);
        Integer wrist = getProperties().intValue("wrist", 0);
        Integer hand = getProperties().intValue("hand", 0);
        Integer coordination = getProperties().intValue("coordination", 0);
        Integer fmTotal = getProperties().intValue("fmTotal", 0);

        JSONObject jsonRow = new JSONObject();
        jsonRow.put("upperExtremity", upperExtremity);
        jsonRow.put("wrist", wrist);
        jsonRow.put("hand", hand);
        jsonRow.put("coordination", coordination);
        jsonRow.put("score", fmTotal);

        Date dt = new Date();
        jsonRow.put("time", assessmentDate);
        jsonRow.put("milliseconds", dt.getTime());
        jsonRow.put("source", "manual");

        return jsonRow;
    }    
}
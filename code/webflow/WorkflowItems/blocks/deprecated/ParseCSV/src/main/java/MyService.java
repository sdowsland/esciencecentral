
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.io.DelimitedTextDataImporter;
import org.pipeline.core.data.io.DelimitedTextDataStreamImporter;
import org.pipeline.core.data.manipulation.BestGuessDataTyper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/** E-sc block to parse CSV data from a workflow datawrapper
 *
 * @author modified by Dom 7/2/2014
 */
public class MyService extends CloudDataProcessorService {

    @Override
    public void execute() throws Exception {
        InputStream inStream = null;
        try {
            // Load the data from the organisation data store
            DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();

            // Set the properties
            int startRow = getEditableProperties().intValue("StartRow", 2);
            int labelRow = getEditableProperties().intValue("LabelRow", 1);
            boolean containsLabels = getEditableProperties().booleanValue("ContainsLabels", true);
            String columnDelimiter = getEditableProperties().stringValue("Delimiter", ",");
            int endRow = getEditableProperties().intValue("EndRow", 1);
            boolean limitRows = getEditableProperties().booleanValue("LimitRows", false);
            boolean subsample = getEditableProperties().booleanValue("Subsample", false);
            int sampleInterval = getEditableProperties().intValue("SampleInterval", 1);
            boolean textData = getEditableProperties().booleanValue("TextOnly", false);
            int importChunkSize = getEditableProperties().intValue("ImportChunkSize", 5000);

            importer.setDataStartRow(startRow);
            importer.setDataEndRow(endRow);
            importer.setDelimiterString(columnDelimiter);
            importer.setDelimiterType(DelimitedTextDataImporter.CUSTOM_DELIMITED);
            importer.setErrorsAsMissing(true);
            importer.setImportColumnNames(containsLabels);
            importer.setLimitRows(limitRows);
            importer.setNameRow(labelRow);
            importer.setSampleInterval(sampleInterval);
            importer.setSubsample(subsample);
            importer.setUseEnclosingQuotes(true);
            importer.setForceTextImport(textData);

            FileWrapper inputData = (FileWrapper) getInputData("input-file");
            if (inputData.getFileCount() != 1) {
                throw new Exception("ParseCSV only operates on a single input file.");
            }

            inStream = new FileInputStream(inputData.getFile(0));

            importer.setChunkSize(importChunkSize);
            importer.resetWithInputStream(inStream);
            int chunkCount = 0;
            int rowCount = 0;
            BestGuessDataTyper guesser;
            Data data = null;
            Data guessedData = null;
            String dataName = inputData.getFile(0).getName();

            if (textData) {
                while (!importer.isFinished()) {
                    data = importer.importNextChunk();
                    data.createEmptyProperties();
                    data.setName(dataName);
                    setOutputDataSet("imported-data", data);
                    chunkCount++;
                    rowCount = rowCount + data.getLargestRows();
                }
            } else {
                while (!importer.isFinished()) {
                    data = importer.importNextChunk();
                    guesser = new BestGuessDataTyper(data);
                    guessedData = guesser.guess();
                    guessedData.createEmptyProperties();
                    guessedData.setName(dataName);
                    setOutputDataSet("imported-data", guessedData);
                    chunkCount++;
                    rowCount = rowCount + data.getLargestRows();
                }
            }
   
            System.out.println("Imported: " + rowCount + " rows of data in: " + chunkCount + " chunk(s)");
        } finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException ignored) {
                }
            }
        }
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.IntegerColumn;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class EscHTTPGet extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {

    }

    /**
     * This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {



        String url = getProperties().stringValue("URL", "");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);


        HttpResponse response = httpClient.execute(get);
        System.out.println("status = " + response.getStatusLine().getStatusCode());

        //Add the status to the output
        Data statusData = new Data();
        IntegerColumn statusColumn = new IntegerColumn("HTTP_STATUS");
        statusColumn.appendIntValue(response.getStatusLine().getStatusCode());
        statusData.addColumn(statusColumn);

        FileWrapper outputFiles = new FileWrapper(getWorkingDirectory());
        String outputfilename = "response.txt";

        //try to find the filename from the content-disposition header
        Header[] headers = response.getAllHeaders();
        for(Header header : headers)
        {
            if(header.getName().equals("Content-Disposition"))
            {
                HeaderElement[] headerValue = header.getElements();
                for(HeaderElement thisHeaderElem : headerValue)
                {
                    NameValuePair[] valuePairs = thisHeaderElem.getParameters();
                    for(NameValuePair pair : valuePairs)
                    {
                        if(pair.getName().equals("filename"))
                        {
                            outputfilename = pair.getValue();
                        }
                    }
                }
            }
        }

        //if the status was ok, read the response
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {

                //output file
                File outputFile = new File(outputfilename);
                outputFiles.addFile(outputFile, true);
                OutputStream out = new FileOutputStream(outputFile);

                //write the response to the output file via a byte[]
                InputStream in = entity.getContent();
                byte[] buffer = new byte[1024];
                int len;
                while ((len = in.read(buffer)) != -1) {
                    out.write(buffer, 0, len);
                }
            }
        }
        else {
            throw new Exception("Unable to complete HTTP request - server returned " + response.getStatusLine().getStatusCode());
        }

        setOutputDataSet("http-status",statusData);
        setOutputData("output-file", outputFiles);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {

    }
}
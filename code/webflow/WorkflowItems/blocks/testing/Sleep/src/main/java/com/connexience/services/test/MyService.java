package com.connexience.services.test;

import com.connexience.server.workflow.cloud.services.*;


public class MyService extends CloudDataProcessorService
{
	/**
	 * This is the main service execution routine. It is called once if the service
	 * has not been configured to accept streaming data or once for each chunk of
	 * data if the service has been configured to accept data streams
	 */
	public void execute() throws Exception
	{
        int sleeptime = getProperties().intValue("Sleep time", 0);
        for(int i = 0; i < 10; i++){
            Thread.sleep(sleeptime / 10);
            reportProgress(9, i);
        }

	}
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.util.ZipUtils;

import java.io.File;


public class MyService extends CloudDataProcessorService
{
    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        String prefix = getEditableProperties().stringValue("FilePrefix", "mod_");
        FileWrapper inputFiles = (FileWrapper) getInputData("input-files");

        File tmpDir = createTempDir("RenameFiles-", "", new File("."));
        FileWrapper renamedFiles = new FileWrapper(tmpDir);

        for (File sourceFile : inputFiles.relativeFiles()) {
            String fileParent = sourceFile.getParent();
            File targetFile;
            if (fileParent == null) {
                targetFile = new File(tmpDir, prefix + sourceFile.getName());
            } else {
                targetFile = new File(tmpDir, fileParent + File.separator + prefix + sourceFile.getName());
                targetFile.getParentFile().mkdirs();
            }

            ZipUtils.copyFile(new File(inputFiles.getBaseDir(), sourceFile.getPath()), targetFile);
            renamedFiles.addFile(targetFile, false);
        }

        setOutputData("renamed-files", renamedFiles);
    }
}
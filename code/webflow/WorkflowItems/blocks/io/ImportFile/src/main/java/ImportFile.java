/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.util.ZipUtils;

import com.connexience.server.model.document.*;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.workflow.engine.datatypes.*;
import org.pipeline.core.xmlstorage.*;

import java.io.*;


public class ImportFile extends CloudDataProcessorService
{
    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
        // This is the API link that can be used to access data in the
        // Inkspot platform. It is configured using the identity of
        // the user executing the service
        XmlDataStore properties = getProperties();

        DocumentRecord record = (DocumentRecord)properties.xmlStorableValue("Source");
        File workingDir = getWorkingDirectory();

        APIBroker api = createApiLink();

        if(record.getCurrentArchiveStatus() != DocumentRecord.UNARCHIVED_ARCHIVESTATUS)
        {
            throw new ConnexienceException("Input document (" + record.getName() + ") is archived.  Retrieve before running workflow.");
        }

        // Pass the metadata to the output connection
        MetadataCollection mdc = api.getMetadata(record);
        setOutputMetadata("imported-file", mdc);
        
        File tmpFile = createTempFile(ZipUtils.escapeFileName(record.getName()), workingDir);
        System.out.println("tmpFile name = " + tmpFile.getName());
        api.downloadToFile(record, tmpFile);
        System.out.println("Imported: " + record.getName() + " [" + record.getId() + "]");

        FileWrapper wrapper = new FileWrapper(workingDir);
        wrapper.addFile(tmpFile, false);
        setOutputData("imported-file", wrapper);
        
        // Save the last directory into the global properties
        Folder lastDir = new Folder();
        lastDir.setId(record.getContainerId());
        getGlobalProperties().add("LastImportDir", lastDir);
    }
}
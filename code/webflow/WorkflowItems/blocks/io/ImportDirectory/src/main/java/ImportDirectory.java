/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.cloud.services.*;

import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.util.ZipUtils;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.document.*;

import java.io.*;
import java.util.*;


public class ImportDirectory extends CloudDataProcessorService {

    private APIBroker api;

    public void execute() throws Exception {


        api = createApiLink();
        File workingDir = getWorkingDirectory();
        FileWrapper wrapper = new FileWrapper(workingDir);
        Folder folder = (Folder) getProperties().xmlStorableValue("SourceFolder");
        boolean recursive = getProperties().booleanValue("Recursive", false);
        String wildcard = getProperties().stringValue("Wildcard", "*.*");
        boolean importToSubdir = getProperties().booleanValue("ImportToSubDirectory", false);
        String subDirName = getProperties().stringValue("SubDirectoryName", "");
        boolean keepDirStructure = getProperties().booleanValue("KeepDirStructure", false);
        boolean sameFolderAsServer = getProperties().booleanValue("KeepSameFolderAsServer", false);

        if(importToSubdir) {
            File subdir = new File(workingDir, subDirName);
            subdir.mkdir();
            workingDir = subdir.getAbsoluteFile();
        }
        if(sameFolderAsServer){
            File subdir = new File(workingDir, folder.getName());
            subdir.mkdir();
            workingDir = subdir.getAbsoluteFile();
        }


        importDirectory(folder, wildcard, recursive, workingDir, wrapper, keepDirStructure);
        setOutputData("imported-files", wrapper);
        
        // Save the last directory into the global properties
        Folder lastDir = new Folder();
        lastDir.setId(folder.getId());
        getGlobalProperties().add("LastImportDir", lastDir);
    }

    private void importDirectory(Folder folder, String pattern, boolean recursive, File workingDir, FileWrapper wrapper, boolean keepDirStructure) throws Exception {
        boolean orderByName = getProperties().booleanValue("OrderByName", true);
        boolean orderByTimestamp = getProperties().booleanValue("OrderByTimestamp", false);

        List<DocumentRecord> contents = api.getFolderDocuments(folder);

        if(keepDirStructure) {
            File dir = new File(workingDir, folder.getName());
            dir.mkdir();
            workingDir = dir.getAbsoluteFile();
        }

        if (recursive) {
            List<Folder> subfolders = api.getChildFolders(folder.getId());
            for (Folder subfolder : subfolders) {
                importDirectory(subfolder, pattern, recursive, workingDir, wrapper, keepDirStructure);
            }
        }

        if (orderByName) {
            Collections.sort(contents, new Comparator<DocumentRecord>() {
                public int compare(DocumentRecord documentRecord, DocumentRecord documentRecord1) {
                    return documentRecord.getName().compareTo(documentRecord1.getName());
                }
            });
        } else if (orderByTimestamp) {
            Collections.sort(contents, new Comparator<DocumentRecord>() {
                public int compare(DocumentRecord documentRecord, DocumentRecord documentRecord1) {
                    return new Long(documentRecord.getTimeInMillis()).compareTo(documentRecord1.getTimeInMillis());
                }
            });
        }

        for (DocumentRecord doc : contents) {
            if (WildcardUtils.wildCardMatch(doc.getName(), pattern)) {

                //re-get the document so that the archive status is updated
                doc = api.getDocument(doc.getId());

                if (doc.getCurrentArchiveStatus() != DocumentRecord.UNARCHIVED_ARCHIVESTATUS) {
                    throw new ConnexienceException("Input document (" + doc.getName() + ") is archived.  Retrieve before running workflow.");
                }

                File tmpFile = createTempFile(ZipUtils.escapeFileName(doc.getName()), workingDir);
                api.downloadToFile(doc, tmpFile);
                wrapper.addFile(tmpFile, false);
            }
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.io.CSVDataExporter;
import java.io.File;
import com.connexience.server.workflow.cloud.services.*;
import org.pipeline.core.data.*;

public class MyService extends CloudDataProcessorService {

    File tmp;
    CSVDataExporter exporter;
    int count = 0;
    int chunkCount = 0;
    String filename;

    public void executionAboutToStart() throws Exception {
        filename = getEditableProperties().stringValue("FileName", "out.csv");
        tmp = new File(getWorkingDirectory(), filename);
        exporter = new CSVDataExporter();
        exporter.openFile(tmp);
    }

    public void execute() throws Exception {
        Data data = getInputDataSet("input-data");
        exporter.appendData(data);
        count = count + data.getLargestRows();
        chunkCount++;

    }

    public void allDataProcessed() throws Exception {
        exporter.closeFile();
        FileWrapper fileWrapper = new FileWrapper(getWorkingDirectory());
        fileWrapper.addFile(tmp, false);
        setOutputData("output-file", fileWrapper);
    }
}
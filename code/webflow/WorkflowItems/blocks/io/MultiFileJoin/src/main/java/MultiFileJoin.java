
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import org.pipeline.core.data.*;

public class MultiFileJoin extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper inputFiles = (FileWrapper)getInputData("input-files");
        
        File joinedFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("JoinedFileName", "joined.txt"));
        boolean useHeader = getEditableProperties().booleanValue("UseHeaderRow", true);
        int startRow = getEditableProperties().intValue("DataStartRow", 2);
        int headerRow = getEditableProperties().intValue("HeaderRow", 1);
        
        PrintWriter writer = new PrintWriter(joinedFile);
        String line;
        int counter = 0;
        
        for(int i=0;i<inputFiles.getFileCount();i++){
            File f = inputFiles.getFile(i);
            if(f.exists()){
                System.out.println("Joining file: " + f.getName());
                counter = 0;
                
                BufferedReader reader = new BufferedReader(new FileReader(f));
                while((line = reader.readLine())!=null){
                    counter++;
                    
                    if(i==0){
                        // First file
                        if(useHeader && counter==headerRow) {
                            // Print the header row
                            writer.println(line);
                        } else if(counter>=startRow){
                            // Standard row
                            writer.println(line);
                        }
                        
                    } else {
                        // Subsequent file
                        if(counter>=startRow){
                            writer.println(line);
                        }
                    }
                }
                reader.close();
                writer.flush();
                
            } else {
                System.out.println("File: " + f.getName() + " does not exist");
            }
        }
        
        writer.flush();
        writer.close();
        
        FileWrapper outputWrapper = new FileWrapper(getWorkingDirectory());
        outputWrapper.addFile(joinedFile.getName(), true);
        setOutputData("joined-file", outputWrapper);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
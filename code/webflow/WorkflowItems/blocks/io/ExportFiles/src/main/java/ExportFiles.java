/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;

import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.security.*;
import com.connexience.server.model.document.*;
import com.connexience.server.model.metadata.MetadataCollection;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;

import java.io.*;
import java.util.HashMap;

public class ExportFiles extends CloudDataProcessorService
{
    /** 
     * This is the main service execution routine. It is called once if 
     * the service has not been configured to accept streaming data or once for 
     * each chunk of data if the service has been configured to accept data 
     * streams.
     */
    public void execute() throws Exception
    {
        // Get the basic properties
        APIBroker api = createApiLink();
        Folder folderWrapper = (Folder)getProperties().xmlStorableValue("TargetFolder");
        String comments = getProperties().stringValue("Comments", "Data file");
        boolean automaticFolder = getProperties().booleanValue("AutomaticFolder", true);
        boolean useLastFolder = getProperties().booleanValue("SaveToLastImportFolder", false);
        boolean makePublic = getProperties().booleanValue("MakePublic", false);
        boolean useSubdirectory = getProperties().booleanValue("SaveToSubdirectory", false);
        boolean uploadMetadata = getProperties().booleanValue("UploadMetadata", false);
        String subdirectoryName = getProperties().stringValue("Subdirectory", "processed");

        FileWrapper files = (FileWrapper)this.getInputData("file-list");

        for(int i = 0; i < files.getFileCount(); i++){
            System.out.println("Received File: " + files.getFile(i));
        }

        MetadataCollection mdc = getInputMetadata("file-list");
        if (mdc != null) {
            mdc.debugPrint();
        } else {
            System.out.println("No metadata");
        }

        // Try and get the DocumentRecord to save to
        Folder folder = null;
        if (automaticFolder) {
            if (useLastFolder) {
                // Does the global properties object contain a valid folder
                if(getGlobalProperties().containsName("LastImportDir")){
                    folder = api.getFolder(((Folder)getGlobalProperties().xmlStorableValue("LastImportDir")).getId());
                } else {
                    folder = getInvocationFolder();
                }
            } else {
                folder = getInvocationFolder();
            }
        } else {
            folder = api.getFolder(folderWrapper.getId());
        }
        
        // Do we need a subdirectory
        if (useSubdirectory) {
            folder = api.getSubdirectory(folder, subdirectoryName);
        }

        if (folder == null) {
            throw new Exception("Cannot find target folder");
        }

        User publicUser = null;
        if (makePublic) {
            publicUser = api.getPublicUser();
            System.out.println("Got public user: " + publicUser.getName());    
        }

        // Upload the files
        //
        HashMap<String, Folder> subFolderMap = new HashMap<String, Folder>();
        File baseDir = files.getBaseDir();
        if (!baseDir.isAbsolute()) {
            baseDir = new File(getWorkingDirectory(), baseDir.getPath());
        }
        StringColumn idColumn = new StringColumn("IDs");

        LinkWrapper fileReferences = new LinkWrapper();
        for (String fileName : files.relativeFilePaths()) {
            // Check if the file exists in the local file system.
            File file = new File(baseDir, fileName);
            if (!file.exists()) {
                throw new Exception("Cannot upload file: " + fileName + " file not found");
            }

            // Create appropriate subfolder structure following the path
            // of the input file
            Folder parentFolder = folder;
            int i = 0;
            int j;
            while ((j = fileName.indexOf(File.separator, i)) > -1) {
                Folder subFolder;
                if (j > 0) {
                    subFolder = subFolderMap.get(fileName.substring(0, j));
                    if (subFolder == null) {
                        subFolder = new Folder();
                        subFolder.setName(fileName.substring(i, j));
                        subFolder.setContainerId(parentFolder.getId());
                        subFolder = api.saveFolder(subFolder);
                        subFolderMap.put(fileName.substring(0, j), subFolder);
                    }
                    parentFolder = subFolder;
                }
                i = j + 1;
            }

            // Upload the file to the subfolder
            if(file.isFile()){
                DocumentRecord uploadDoc = new DocumentRecord();
                uploadDoc.setName(fileName.substring(i, fileName.length()));
                uploadDoc.setDescription(comments);
                uploadDoc = api.saveDocument(parentFolder, uploadDoc);
                DocumentVersion version = api.uploadFile(uploadDoc, file);
                fileReferences.addDocument(uploadDoc, version);
                System.out.println("Uploaded file: " + uploadDoc.getName() + "[" + uploadDoc.getId() + "]");

                idColumn.appendStringValue(uploadDoc.getId());
                if (makePublic) {
                    api.grantObjectPermission(uploadDoc, publicUser, Permission.READ_PERMISSION);
                }
                if(uploadMetadata){
                    if (mdc != null) {
                        api.uploadMetadata(uploadDoc, mdc);
                    }
                }
            }
        }

        try {
            Data fileIds = new Data();
            fileIds.addColumn(idColumn);
            setOutputDataSet("file-ids", fileIds);
            setOutputData("file-references", fileReferences);

        } catch (Exception e){
            System.out.println("Warning: Could not set file IDs");
        }
    }
}
[r c] = size(csvfile.files);
segmentedFiles.files = [];
useSubDir = properties.UseSubDir;
subDir = properties.SubDirectoryName;
if(useSubDir == true)
	if ~exist(subDir, 'dir')
	  mkdir(subDir);
	end
end

for i=1:r
	[pathstr,name,ext] = fileparts(csvfile.files(i,:));
	data = csvread(deblank(csvfile.files(i,:)));
	if(useSubDir == true)
		eval(['save -' properties.MatFileVersion ' ' subDir filesep name '.mat data']);
		eval(['segmentedFiles.files = [segmentedFiles.files ; ''' subDir filesep name '.mat''];']);
	else
		eval(['save -' properties.MatFileVersion ' ' name '.mat data']);
		eval(['segmentedFiles.files = [segmentedFiles.files ; ''' name '.mat''];']);
	end
endfor

pause(10)

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.util.ZipUtils;
import org.pipeline.core.xmlstorage.XmlDataStore;

import java.io.File;
import java.util.List;


public class GetNamedFile extends CloudDataProcessorService
{
    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        XmlDataStore properties = getProperties();

        String fileName = getProperties().stringValue("FileName", "");
        if(fileName == null || fileName.equalsIgnoreCase("")){
            throw new ConnexienceException("No filename specified");
        }

        Boolean failIfNotExist = getProperties().booleanValue("FailIfNotExist", true);

        File workingDir = getWorkingDirectory();
        FileWrapper outputFiles = new FileWrapper(workingDir);

        APIBroker api = createApiLink();
        List<DocumentRecord> docs = api.getFolderDocuments(api.getFolder(api.getCurrentUser().getHomeFolderId()));
        for(DocumentRecord doc : docs){
            if(doc.getName().equalsIgnoreCase(fileName)){
                if(doc.getCurrentArchiveStatus() != DocumentRecord.UNARCHIVED_ARCHIVESTATUS)
                {
                    throw new ConnexienceException("Input document (" + doc.getName() + ") is archived.  Retrieve before running workflow.");
                }

                // Pass the metadata to the output connection
                MetadataCollection mdc = api.getMetadata(doc);
                setOutputMetadata("imported-files", mdc);

                File tmpFile = createTempFile(ZipUtils.escapeFileName(doc.getName()), workingDir);
                System.out.println("tmpFile name = " + tmpFile.getName());
                api.downloadToFile(doc, tmpFile);
                System.out.println("Imported: " + doc.getName() + " [" + doc.getId() + "]");
                outputFiles.addFile(tmpFile, false);

                // Save the last directory into the global properties
                Folder lastDir = new Folder();
                lastDir.setId(doc.getContainerId());
                getGlobalProperties().add("LastImportDir", lastDir);

            }
        }

        if(failIfNotExist && (outputFiles.getFileCount() == 0)){
            throw new ConnexienceException("File: " + fileName +" does not exist in user's home directory");
        }

        setOutputData("imported-files", outputFiles);
    }
}
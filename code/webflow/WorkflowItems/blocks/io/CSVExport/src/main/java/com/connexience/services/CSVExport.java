/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.services;

import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.model.document.*;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.workflow.api.APIBroker;

import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import org.pipeline.core.data.*;
import org.pipeline.core.data.io.*;
import org.pipeline.core.data.columns.*;

import java.io.*;

public class CSVExport extends CloudDataProcessorService {

    File tmp;
    CSVDataExporter exporter;
    int count = 0;
    int chunkCount = 0;

    public void executionAboutToStart() throws Exception {
        tmp = createTempFile("tmp.csv");
        exporter = new CSVDataExporter();
        boolean includeNames = getEditableProperties().booleanValue("IncludeNames", true);
        boolean quoteStringOutput = getEditableProperties().booleanValue("QuoteStringOutput", false);
        if (!includeNames) {
            exporter.setIncludeNames(false);
        }
        if (quoteStringOutput) {
            exporter.setQuoteAllOutput(true);
        }
        exporter.openFile(tmp);
    }

    public void execute() throws Exception {
        Data data = getInputDataSet("input-data");
        exporter.appendData(data);
        count = count + data.getLargestRows();
        chunkCount++;
    }

    public void allDataProcessed() throws Exception {
        exporter.closeFile();
        System.out.println(count + " rows exported in " + chunkCount + " chunk(s)");
        boolean automaticFolder = getProperties().booleanValue("AutomaticFolder", true);
        Folder folder = (Folder) getProperties().xmlStorableValue("TargetFolder");
        String fileName = getProperties().stringValue("FileName", "out.csv");
        boolean uploadMetadata = getProperties().booleanValue("UploadMetadata", false);

        // Use the workflow folder if needed
        if (automaticFolder) {
            folder = getInvocationFolder();
        }
        
        MetadataCollection mdc = getInputMetadata("input-data");
        if (mdc != null) {
            mdc.debugPrint();
        } else {
            System.out.println("No metadata");
        }        

        DocumentRecord doc = new DocumentRecord();
        doc.setName(fileName);
        doc.setDescription("Worklow generated CSV file");
        doc = createApiLink().saveDocument(folder, doc);
        DocumentVersion version = createApiLink().uploadFile(doc, tmp);
        tmp.delete();

        try {
            if(uploadMetadata){
                if (mdc != null) {
                    createApiLink().uploadMetadata(doc, mdc);
                }
            }        
        } catch (Exception e){
            System.out.println("Unable to upload metadata: " + e.getMessage());
        }
        
        try {
            Data docId = new Data();
            StringColumn idCol = new StringColumn("DocumentID");
            idCol.appendStringValue(doc.getId());
            docId.addColumn(idCol);
            setOutputDataSet("document-id", docId);

            LinkWrapper fileReferences = new LinkWrapper();
            fileReferences.addDocument(doc, version);
            setOutputData("file-reference", fileReferences);

        } catch (Exception ex) {
            System.out.println("Warning: Could not set document ID");
        }
    }
}
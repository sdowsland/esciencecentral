
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.cloud.library.CloudWorkflowServiceLibraryItem;
import com.connexience.server.workflow.engine.datatypes.LibraryItemWrapper;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;


public class MyService extends CloudDataProcessorService
{
    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        APIBroker api = createApiLink();
        LibraryItemWrapper output = new LibraryItemWrapper();

        String libName = getProperties().stringValue("Library Name", null);
        addLibrary(api, libName, output, true);

        for (String libraryName : (StringListWrapper)getProperties().xmlStorableValue("Additional Libraries")) {
            addLibrary(api, libraryName, output, false);
        }

        if (output.size() == 0) {
            throw new Exception("No libraries to prepare.");
        }

        setOutputData("library", output);
    }
    
    private void addLibrary(APIBroker api, String libraryName, LibraryItemWrapper wrapper, boolean failOnEmpty)
    throws Exception
    {
        if (libraryName == null || "".equals(libraryName.trim())) {
            if (failOnEmpty) {
                throw new Exception("Missing library name");
            } else {
                return;
            }
        }
        libraryName = libraryName.trim();

        CloudWorkflowServiceLibraryItem library = api.prepareLibrary(libraryName);
        System.out.flush();
        if (library == null) {
            throw new Exception("Library preparation failed for library: " + libraryName);
        }

        wrapper.add(library);
    }
}
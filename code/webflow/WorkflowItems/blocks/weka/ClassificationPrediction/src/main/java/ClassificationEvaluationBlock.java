
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.WekaClassificationModel;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.StringColumn;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToNominal;

/**
 * E-sc block to generate class label predictions using an existing Weka
 * CLASSIFICATION model and to output predictions alongside the 'actual' class
 * labels. Requires known 'x' and 'y' inputs.
 *
 * For regression models use the RegressionEvaluation block.
 *
 * @author Dominic Searson
 */
public class ClassificationEvaluationBlock extends CloudDataProcessorService {

    /**
     * WekaClassifictaionModel object which will be used for prediction
     */
    private WekaClassificationModel wekaModel;

    @Override
    public void executionAboutToStart() throws Exception {

        // Load the classifier
        ObjectWrapper modelWrapper = (ObjectWrapper) getInputData("model");
        wekaModel = (WekaClassificationModel) modelWrapper.getPayload();
    }

    @Override
    public void execute() throws Exception {

        //get input data set to block
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");

        // Convert to Weka Instances            
        Column yCol = yData.column(0);

        //extract external classifier object from WekaModel container
        Classifier classifier = wekaModel.getClassifier();

        //convert data to Weka compatible
        XYDataToInstances converter = new XYDataToInstances(xData, yCol);
        Instances wekaInstances = converter.toWekaInstances();
        wekaInstances.setClassIndex(wekaInstances.numAttributes() - 1);

        //get data name from 'x' data
        wekaInstances.setRelationName(xData.getName());

        //convert 'string' labelled vars to nominal class labels 
        StringToNominal filter = new StringToNominal();
        filter.setAttributeRange("first-last");
        filter.setInputFormat(wekaInstances);
        Instances filteredInstances = Filter.useFilter(wekaInstances, filter);

        //generate an evaluation using the new data and get raw numerical predictions
        Evaluation eval = new Evaluation(filteredInstances);
        double[] rawModelPredictions = eval.evaluateModel(classifier, filteredInstances);

        //find the attribute type of the modelled variable (see http://weka.wikispaces.com/Use+WEKA+in+your+Java+code 
        Attribute classAttribute = filteredInstances.classAttribute();

        //error checks
        if (!classAttribute.isNominal()) {
            throw new DataException("The output/class labelled (y) variable could not be converted to a Weka 'Nominal' variable");
        }
        int numClassLabels = classAttribute.numValues();

        if (numClassLabels != wekaModel.getClassAttribute().numValues()) {
            throw new DataException("Number of class labels detected in data different to that in supplied model.");
        }


        // Get class label predictions on training data
        Column predicted = new StringColumn("Predicted");
        double currPredVal;

        String classVal;  //the nominal output class, e.g. 'A','B','C' etc
        for (int i = 0; i < filteredInstances.numInstances(); i++) {
            currPredVal = rawModelPredictions[i];
            classVal = classAttribute.value((int) currPredVal);  // converts e.g. doubles into the class label, e.g. "A" or "B"
            predicted.appendStringValue(classVal);
        }
        Data predictionData = new Data();
        predictionData.addColumn(yCol.getCopy());
        predictionData.column(0).setName("Actual");
        predictionData.addColumn(predicted);
        setOutputDataSet("y-actual-predicted", predictionData);

        // Add some metadata to the outputs
        MetadataCollection mdc = new MetadataCollection();


        mdc.add(new NumericalMetadata("ClassificationPerformanceTesting", "Testing instances correct", eval.correct()));
        mdc.add(new NumericalMetadata("ClassificationPerformanceTesting", "Testing instances incorrect", eval.incorrect()));

        for (int i = 0; i < numClassLabels; i++) {

            mdc.add(new NumericalMetadata("ClassificationPerformanceTesting", "Recall (class '" + classAttribute.value(i) + "')", eval.recall(i)));
            mdc.add(new NumericalMetadata("ClassificationPerformanceTesting", "Precision (class '" + classAttribute.value(i) + "')", eval.precision(i)));
            mdc.add(new NumericalMetadata("ClassificationPerformanceTesting", "F-measure (class '" + classAttribute.value(i) + "')", eval.fMeasure(i)));

        }

        mdc.add(new TextMetadata("ClassificationPerformanceTesting", "Confusion Matrix", "<pre>" + eval.toMatrixString() + "</pre>"));


        mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", false));
        mdc.add(new TextMetadata("DataAttributes", "Weka data summary (filtered)", filteredInstances.toSummaryString()));
        addMetadataToAllOutputs(mdc);

        //Save input data ARFF report
        FileWrapper wrapper = new FileWrapper(getWorkingDirectory());
        File arffFile = new File(getWorkingDirectory(), "WekaTestDataSummary.txt");
        ZipUtils.writeSingleLineFile(arffFile, filteredInstances.toSummaryString());
        wrapper.addFile(arffFile, false);

        setOutputData("report", wrapper);

    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}
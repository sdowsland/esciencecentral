/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */


import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Standardize;

/** E-Sc block that accepts a Weka ARFF file and applies 'standardisation' (i.e.
 * zero mean centering and scaling to unit variance) of each attribute in it.
 * The scaled data is also written out as an ARFF file, as is the filter object in
 * an object wrapper.
 * 
 * The filter object may be applied to new data using the existing scaling parameters
 * using the ARFF-ApplyFilter block.
 * 
 * @author Dominic Searson (3/14)
 */
public class Standardise_ARFF_attributes extends CloudDataProcessorService {

    
    @Override
    public void executionAboutToStart() throws Exception {
    }

    
    @Override
    public void execute() throws Exception {
        
        //load input arff file and convert to Instances
        FileWrapper arffWrapper = (FileWrapper) getInputData("input-arff-file");

        if (arffWrapper.getFileCount() == 1) {

            
            //get user input (whether or not to standardise the last arff attribute (the class variable))
            boolean ignoreY = getEditableProperties().booleanValue("Ignore last column", true);
            
            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);

            //apply standardisation filter
            Instances filteredInstances;
            Standardize stdFilter = new Standardize();
            stdFilter.setIgnoreClass(!ignoreY);
            stdFilter.setInputFormat(wekaInstances);
            filteredInstances = Filter.useFilter(wekaInstances, stdFilter);
            filteredInstances.setRelationName(wekaInstances.relationName());

            //now write arff out in a filewrapper
            String fileName = "StandardisedAttributes.arff";
            FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
            ArffSaver saver = new ArffSaver();
            saver.setInstances(filteredInstances);
            File outFile = new File(getWorkingDirectory(), fileName);
            saver.setFile(outFile);
            saver.writeBatch();
            outputFile.addFile(outFile, false);
            setOutputData("output-arff-file", outputFile);
            
            
            //write the standardisation filter out as an object wrapper
            // Save the model as an esc workflow object
            ObjectWrapper modelWrapper = new ObjectWrapper(stdFilter);
            setOutputData("standardisation-filter", modelWrapper);

        } else {
            throw new WekaException("StandardiseARFF block can only parse a single ARFF file");
        }

    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}
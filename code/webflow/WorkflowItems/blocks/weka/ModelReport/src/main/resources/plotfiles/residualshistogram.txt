set terminal png size 800,600
set output "${plot-file}"

n=50 #number of intervals
max=${data-max} #max value
min=${data-min} #min value
width=(max-min)/n #interval width
#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0

set xrange [min:max]
set yrange [0:]

#to put an empty boundary around the
#data inside an autoscaled graph.
#set offset graph 0.05,0.05,0.05,0.0
set xtics autofreq out mirror
set boxwidth width*0.9
set style fill solid 0.5 #fillstyle
set xlabel "Model residuals"
set ylabel "Frequency"
#set arrow from 0.0,graph(0,0) to 0.0,graph(1,1) nohead lc rgb"black" lw 2

#set datafile separator ','

set key spacing 3
set key width 2

set grid ytics lc rgb "#bbbbbb" lw 1 lt 0
set grid xtics lc rgb "#bbbbbb" lw 1 lt 0
#count and plot
plot '${plot-data}' u (hist($4,width)):(1.0) smooth freq w boxes lc rgb"black" title "Model residuals"



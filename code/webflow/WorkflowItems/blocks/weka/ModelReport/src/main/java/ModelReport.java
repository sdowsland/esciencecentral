
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.support.GnuPlotEngine;
import com.connexience.server.workflow.util.ZipUtils;
import com.connexience.tools.html2pdf.Html2PdfConverter;
import java.io.File;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.CSVDataExporter;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.core.data.manipulation.ColumnSorter;
import org.pipeline.core.data.maths.MaxValueCalculator;
import org.pipeline.core.data.maths.MeanValueCalculator;
import org.pipeline.core.data.maths.MinValueCalculator;

/**
 * Block to generate a PDF report on the properties of a predictive REGRESSION
 * type model on training data with all numeric data types. 
 * 
 * For CLASSIFICATION type models (class labels instead of numeric) use the 
 * ClassifierReport block.
 *
 * This block takes as inputs a column of 'actual' numerical values and a column
 * of 'predicted' numerical values and computes a number of model performance
 * metrics (such as the coefficient of determination R^2). These performance
 * stats are independent of the type of model (or block) that was used to create
 * the predicted values.
 *
 * The generated report depends on whether the data is 'training' data or 'test'
 * data.
 *
 * There is lot of duplication of effort within this class, e.g. calculating
 * errors etc.
 *
 * @author Hugo Hiden, Dominic Searson
 */
public class ModelReport implements WorkflowBlock {

    private File tmpDir;
    private double meanActual;
    private double meanPredicted;
    private boolean haveTrainMeanActual = false;
    private boolean haveTrainMeanPredicted = false;
    private double trainMeanActual = 0;
    private double trainMeanPredicted = 0;
    private boolean isTrainingData = true;
    private boolean isYscrambledTraining = false;
    
    /*
     * String for controlling formatting of model performance metric values
     * in report
     */
    private static String DECIMALFORMATTINGSTR = "#.#####";

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {

   
        //check model type is classification and exit if so
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        MetadataItem modelType = mdc.find("ModelDetails", "Model type");
        
        if ((modelType != null) && (modelType.getStringValue().equalsIgnoreCase("Classification")) ) {
            throw new DataException("The Model Report block can not accept model predictions from 'Classification' blocks");
        }
        
        // Create a temporary directory to store the report html file in
        tmpDir = new File(env.getWorkingDirectory(), env.getBlockContextId() + "-html");
        tmpDir.mkdir();

        // Copy the stylesheet
        ZipUtils.copyFile(env.getExecutionService().getLibraryItem().getFile("report.css"), new File(tmpDir, "report.css"));
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {


        // Get the input data 
        env.getExecutionService().populateInputDataSetMap();
        Data inputData = inputs.getInputDataSet("model-results");

        //get model metadata
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");

        //get optional user data description
        String userDatasetName = env.getStringProperty("Dataset", "");

        //did yscrambling occur during training?
        BooleanMetadata yScrambleMeta = (BooleanMetadata) mdc.find("Scrambled modelled variable");
        if (yScrambleMeta != null) {
            if (yScrambleMeta.isBooleanValue()) {
                this.isYscrambledTraining = true;
            }
        }

        // Find out which columns contain the actual and predicted values and extract them from the input data
        ColumnPicker actualPicker = new ColumnPicker(env.getStringProperty("ActualColumn", "#0"));
        ColumnPicker predictedPicker = new ColumnPicker(env.getStringProperty("PredictionColumn", "#1"));

        DoubleColumn actual = (DoubleColumn) actualPicker.pickColumn(inputData);
        DoubleColumn predicted = (DoubleColumn) predictedPicker.pickColumn(inputData);

        String variableName = getModelledVariableName(env, actual);  

        // Check the number of predictions and actual values match, and only proceed if they do
        if (actual.getRows() == predicted.getRows()) {

            initialiseDataAttributes(env, actual, predicted);

            // Write out data in a gnuplot suitable format
            Data tmpData = new Data();
            tmpData.addColumn(actual.getCopy());
            tmpData.addColumn(predicted.getCopy());

            // Calculate actual-predicted for residuals plot
            int rows = tmpData.getLargestRows();
            DoubleColumn residuals = new DoubleColumn("Residuals");

            for (int i = 0; i < rows; i++) {

                if (actual.isMissing(i) == false && predicted.isMissing(i) == false) {
                    residuals.appendDoubleValue(actual.getDoubleValue(i) - predicted.getDoubleValue(i));
                }
            }

            MinValueCalculator minCalc = new MinValueCalculator(residuals);
            MaxValueCalculator maxCalc = new MaxValueCalculator(residuals);

            tmpData.addColumn(residuals);
            File tmpDataFile = new File(tmpDir, "avp.csv");
            writeData(tmpData, tmpDataFile);
            GnuPlotEngine plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/actualvpredicted.txt", tmpDir.getName() + File.separator + "avp.png");
            plotEngine.getAdditionalParameters().add("plot-data", tmpDataFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() +"/" + "avp.png");
            plotEngine.getAdditionalParameters().add("variable-name", variableName);
            plotEngine.createPlot();

            // Do a trend plot
            plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/predictiontrend.txt", tmpDir.getName() + File.separator + "trend.png");
            plotEngine.getAdditionalParameters().add("plot-data", tmpDataFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() + "/" + "trend.png");
            plotEngine.getAdditionalParameters().add("variable-name", variableName);
            plotEngine.createPlot();

            // Do a residuals plot
            plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/residuals.txt", tmpDir.getName() + File.separator + "residuals.png");
            plotEngine.getAdditionalParameters().add("plot-data", tmpDataFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() + "/" + "residuals.png");
            plotEngine.getAdditionalParameters().add("variable-name", variableName);
            plotEngine.createPlot();

            // Do a histogram of residuals (provided all the residuals are not zero, e.g. for a perfect model on training case) - as this breaks gnuplot
            boolean plotResHist = true;
            try {
                if ( (maxCalc.doubleValue() != 0) && (minCalc.doubleValue() != 0) ) {
                    plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/residualshistogram.txt", tmpDir.getName() + File.separator + "histogram.png");
                    plotEngine.getAdditionalParameters().add("plot-data", tmpDataFile.getPath());
                    plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() + "/" + "histogram.png");
                    plotEngine.getAdditionalParameters().add("variable-name", variableName);
                    plotEngine.getAdditionalParameters().add("data-min", minCalc.doubleValue());
                    plotEngine.getAdditionalParameters().add("data-max", maxCalc.doubleValue());
                    plotEngine.createPlot();
                } else {
                    plotResHist = false;
                }
            } catch (Exception e){
                System.out.println("Error plotting residuals: " + e.getMessage());
                plotResHist = false;
            }
            
            // Do a REC plot
            File recPlot = new File(tmpDir, "rec.png");
            File recDataFile = new File(tmpDir, "rec.csv");
            writeData(generateRECCurve(actual, predicted), recDataFile);
            plotEngine = new GnuPlotEngine(env.getExecutionService(), "plotfiles/rec.txt", tmpDir.getName() + File.separator + "rec.png");
            plotEngine.getAdditionalParameters().add("plot-data", recDataFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", tmpDir.getName() + "/" + "rec.png");
            plotEngine.createPlot();

            // Write the report html
            File report = new File(tmpDir, "report.html");
            PrintWriter writer = new PrintWriter(report);
            writer.println("<html>");
            writer.println("<link rel=\"stylesheet\" href=\"report.css\"/>");
            writer.println("<head></head><body>");

            if (isTrainingData) {
                writer.println("<h1>Model report: " + userDatasetName + " training data</h1>");
            } else {
                writer.println("<h1>Model report: " + userDatasetName + " testing data</h1>");;
            }


            //generate report tables
            generateWorkflowInfoTable(env, writer);
            // generateDataSummaryTable(env, writer); //doesn't work properly (yet)
            generateSummaryTable(env, writer);
            
            Data statistics = generateStatisticsTable(writer, actual, predicted);
            try {
                outputs.setOutputDataSet("summary-data", statistics);
            } catch (Exception e){
                System.out.println("Cannot set output summary-data: " + e.getMessage());
            }
            
            ArrayList<File> charts = new ArrayList<>();
            
            // Actual -v- predicted
            writer.println("<h2 class=\"break\">Actual values vs predicted values</h2>");
            writer.println("<img width=\"100%;\" src=\"avp.png\"/>");
            charts.add(new File(tmpDir, "avp.png"));
            
            // Trend
            writer.println("<h2 class=\"break\">Model prediction</h2>");
            writer.println("<img width=\"100%;\" src=\"trend.png\"/>");
            charts.add(new File(tmpDir, "trend.png"));
            
            // REC
            writer.println("<h2 class=\"break\">Regression error characteristic (REC)</h2>");
            writer.println("<img width=\"100%;\" src=\"rec.png\"/>");
            charts.add(new File(tmpDir, "rec.png"));
            
            // Residuals
            writer.println("<h2 class=\"break\">Model residuals</h2>");
            writer.println("<img width=\"100%;\" src=\"residuals.png\"/>");
            charts.add(new File(tmpDir, "residuals.png"));
            
            // Residuals histogram
            if (plotResHist) {
                writer.println("<h2 class=\"break\">Distribution of model residuals</h2>");
                writer.println("<img width=\"100%;\" src=\"histogram.png\"/>");
                charts.add(new File(tmpDir, "histogram.png"));
            }
            
            writer.println("</body></html>");
            writer.flush();
            writer.close();
            
            // Convert to pdf
            File reportPdf = new File(env.getWorkingDirectory(), env.getStringProperty("ReportName", "report.pdf"));
            Html2PdfConverter converter = new Html2PdfConverter(report, reportPdf);
            converter.convert();
            outputs.setOutputFileWithFullPath("report", reportPdf);
            
            try {
                outputs.setOutputFilesWithFullPath("charts", charts);
            } catch (Exception e){
                System.out.println("Cannot set charts output: " + e.getMessage());
            }

        } else {
            throw new Exception("Row counts in actual and predicted columns do not match");
        }
    }

    /**
     * Generates some HTML workflow info for the report, e.g. the workflow id,
     * invocation date etc.
     *
     * @param env The block environment
     * @param writer The Writer for the report document
     */
    public void generateWorkflowInfoTable(BlockEnvironment env, PrintWriter writer) throws Exception {

        WorkflowInvocationFolder invocationFolder = env.getExecutionService().getInvocationFolder();

        String workflowId = invocationFolder.getWorkflowId();
        APIBroker api = env.getExecutionService().createApiLink();
        ServerObject serverObject = api.getServerObject(workflowId); //gets server object via rmi 
        String displayName = serverObject.getName();

        String workflowDescription = serverObject.getDescription();
        Date creationDate = serverObject.getCreationDate();
        //String creatorId = serverObject.getCreatorId();

        String invocationId = invocationFolder.getInvocationId();
        String versionId = invocationFolder.getVersionId();
        Date invocationDate = invocationFolder.getInvocationDate();


        writer.println("<h2>Workflow info</h2>");
        writer.println("<table width=\"100%;\"><tbody>");
        writer.println("<tr><td>Workflow name</td><td>" + displayName + "</td></tr>");
        writer.println("<tr><td>Workflow description</td><td>" + workflowDescription + "</td></tr>");
        writer.println("<tr><td>Workflow creation date</td><td>" + creationDate.toString() + "</td></tr>");
        // writer.println("<tr><td>Workflow creator</td><td>" +creatorId + "</td></tr>");

        writer.println("<tr><td>Workflow ID</td><td>" + workflowId + "</td></tr>");
        writer.println("<tr><td>Version ID</td><td>" + versionId + "</td></tr>");
        // writer.println("<tr><td>Invocation ID</td><td>" + invocationId + "</td></tr>");
        // writer.println("<tr><td>Description</td><td>" +description +"</td>"); 
        writer.println("<tr><td>Invocation date</td><td>" + invocationDate.toString() + "</td></tr>");
        writer.println("</tbody></table>");
    }

    /**
     * Generates some HTML data set info for the report, e.g. 'name' of data set
     * etc.
     *
     * TODO: needs some work because DataDescription metadata could arrive from
     * multiple paths.
     *
     * @param env The block environment
     * @param writer The Writer for the report document
     * @throws Exception If it doesn't work
     */
    public void generateDataSummaryTable(BlockEnvironment env, PrintWriter writer) throws Exception {
        writer.println("<h2>Data summary</h2>");


        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        if (mdc != null && mdc.size() > 0) {

            writer.println("<table width=\"100%;\"><tbody>");
            MetadataItem md;
            for (int i = 0; i < mdc.size(); i++) {
                md = mdc.get(i);
                if (md.getCategory() != null && md.getCategory().equals("DataDescription")) {
                    writer.println("<tr>");
                    writer.println("<td>" + md.getName() + "</td>");
                    writer.println("<td>" + md.getStringValue() + "</td>");
                    writer.println("</tr>");
                }
            }
            writer.println("</tbody></table>");
        }
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {
    }

    /**
     * Get the modelled variable name if it exists in the workflow metadata.
     *
     * @return A String containing the modelled variable name.
     *
     */
    private String getModelledVariableName(BlockEnvironment env, Column actual) throws Exception {
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        for (MetadataItem i : mdc.getItems()) {
            if (i.getCategory() != null && i.getCategory().equals("ModelDetails") && i.getName() != null && i.getName().equals("ModelledVariableName")) {
                return i.getStringValue();
            }
        }

        // Use name of column
        return actual.getName();
    }

    /*
     * Generates regression model performance statistics HTML table.
     */
    private Data generateStatisticsTable(PrintWriter writer, DoubleColumn actual, DoubleColumn predicted) throws Exception {

        Data statistics = new Data();
        StringColumn titleColumn = new StringColumn("statistic");
        statistics.addColumn(titleColumn);
        DoubleColumn valueColumn = new DoubleColumn("value");
        statistics.addColumn(valueColumn);
        
        /* decimal formatter */
        DecimalFormat df = new DecimalFormat(DECIMALFORMATTINGSTR);

        writer.println("<h2>Model performance metrics</h2>");

        //warning message is y-scrambling was used during training of model
        if (this.isYscrambledTraining) {
            writer.println("<p class='warning'>Output data was scrambled during training/model fitting. Poor performance metrics below are to be expected.</p>");
        }

        writer.println("<table width=\"100%;\"><tbody>");

        try {
            double rSquaredValue = rSquared(actual, predicted);
            writer.println("<tr><td>R<sup>2</sup></td><td>" + df.format(rSquaredValue) + "</td>");
            titleColumn.appendStringValue("RSquared");
            valueColumn.appendDoubleValue(rSquaredValue);

        } catch (Exception e) {
            writer.println("<tr><td>R<sup>2</sup></td><td>Could not calculate: " + e.getMessage() + "</td>");
        }

//remove q2 for now - its use is unhelpful in the majority of cases: there are multiple definitions
//of q2 in circulation and it is of questionable value (see: Tropsha, Beware of q2! paper etc.        

//        if (!isTrainingData){
//            try {
//                writer.println("<tr><td>Q<sup>2</sup></td><td>" + df.format(qSquared(actual, predicted)) + "</td>");
//            } catch (Exception e){
//                writer.println("<tr><td>Q<sup>2</sup></td><td>Could not calculate: " + e.getMessage() + "</td>");
//            }
//        }

        try {
            double corrValue = corrCoef(actual, predicted);
            titleColumn.appendStringValue("Correlation");
            valueColumn.appendDoubleValue(corrValue);
            writer.println("<tr><td>Correlation coefficient <i>r</i></td><td>" + df.format(corrValue) + "</td>");
        } catch (Exception e) {
            writer.println("<tr><td>Correlation coefficient <i>r</i></td><td>Could not calculate: " + e.getMessage() + "</td>");
        }

        try {
            double rmsValue = rmsError(actual, predicted);
            titleColumn.appendStringValue("RMS");
            valueColumn.appendDoubleValue(rmsValue);
            writer.println("<tr><td>RMS error</td><td>" + df.format(rmsValue) + "</td>");
        } catch (Exception e) {
            writer.println("<tr><td>RMS error</td><td>Could not calculate: " + e.getMessage() + "</td>");
        }

        double maeValue = mae(actual, predicted);
        titleColumn.appendStringValue("MAE");
        valueColumn.appendDoubleValue(maeValue);
        writer.println("<tr><td>Mean Absolute Error</td><td>" + df.format(maeValue) + "</td>");
        
        double maxErrValue = maxAbsError(actual, predicted);
        titleColumn.appendStringValue("MaxError");
        valueColumn.appendDoubleValue(maxErrValue);
        writer.println("<tr><td>Max Absolute Error</td><td>" + df.format(maxErrValue) + "</td>");

        writer.println("</tbody></table>");
        return statistics;
    }

    private void generateSummaryTable(BlockEnvironment env, PrintWriter writer) throws Exception {
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        if (mdc != null && mdc.size() > 0) {
            writer.println("<h2>Model summary</h2>");
            writer.println("<table width=\"100%;\"><tbody>");
            MetadataItem md;
            for (int i = 0; i < mdc.size(); i++) {
                md = mdc.get(i);
                if (md.getCategory().equals("ModelDetails")) {
                    writer.println("<tr>");
                    writer.println("<td>" + md.getName() + "</td>");
                    writer.println("<td>" + md.getStringValue() + "</td>");
                    writer.println("</tr>");
                }
            }
            writer.println("</tbody></table>");
        }
    }

    private void writeData(Data data, File f) throws Exception {
        // Need to create a new data file
        CSVDataExporter exporter = new CSVDataExporter(data);
        exporter.setDelimiter(" ");
        exporter.setIncludeNames(false);
        exporter.setRowIndexIncluded(true);
        exporter.setRowIndexValue(0);
        PrintWriter dataWriter = new PrintWriter(f);
        exporter.writeToPrintWriter(new PrintWriter(f));
        dataWriter.flush();
        dataWriter.close();
    }

    /**
     * Calculate the means of the actual data and the predicted data.
     *
     */
    private void initialiseDataAttributes(BlockEnvironment env, DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {

        MeanValueCalculator c1 = new MeanValueCalculator(actualColumn);
        this.meanActual = c1.doubleValue();
        MeanValueCalculator c2 = new MeanValueCalculator(predictedColumn);
        this.meanPredicted = c2.doubleValue();

        // Sort out any extra data attributes that have been passed in
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        for (MetadataItem i : mdc.getItems()) {
            if (i.getCategory().equals("DataAttributes")) {
                if (i.getName().equals("TrainingData") && i instanceof BooleanMetadata) {
                    isTrainingData = ((BooleanMetadata) i).isBooleanValue();
                }

                // Train mean actual value
                if (i.getName().equals("TrainMeanActual") && i instanceof NumericalMetadata) {
                    trainMeanActual = ((NumericalMetadata) i).getDoubleValue();
                    haveTrainMeanActual = true;
                }

                // Train mean predicted value
                if (i.getName().equals("TrainMeanPredicted") && i instanceof NumericalMetadata) {
                    trainMeanPredicted = ((NumericalMetadata) i).getDoubleValue();
                    haveTrainMeanPredicted = true;
                }
            }
        }
    }

    /**
     * Calculate the coefficient of determination R^2 value.
     *
     * @param actualColumn A DoubleColumn containing the actual values.
     * @param predictedColumn A DoubleColumn containing the predicted values.
     * @return The R squared value.
     *
     */
    private double rSquared(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {

        double runsumActuals = 0, ssTotal = 0, ssErr = 0;
        double actual, predicted;
        int count = 0;
        for (int i = 0; i < actualColumn.getRows(); i++) {
            if (!actualColumn.isMissing(i) && !predictedColumn.isMissing(i)) {
                predicted = predictedColumn.getDoubleValue(i);
                actual = actualColumn.getDoubleValue(i);
                runsumActuals = runsumActuals + actual;
                ssTotal = ssTotal + Math.pow((actual - meanActual), 2);
                ssErr = ssErr + Math.pow((actual - predicted), 2);
                count++;
            }
        }

        if (count > 0) {
            return 1 - (ssErr / ssTotal);
        } else {
            throw new Exception("No suitable data");
        }
    }

    /**
     * Calculates the maximum absolute error attained.
     *
     * @param actual A DoubleColumn of actual y values
     * @param predicted A DoubleColumn of predicted y values
     * @return The maximum absolute error
     */
    private double maxAbsError(DoubleColumn actual, DoubleColumn predicted)
            throws Exception {
        double maxsAbserror = -1, pred, act, err;


        for (int i = 0; i < actual.getRows(); i++) {

            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                pred = predicted.getDoubleValue(i);
                act = actual.getDoubleValue(i);
                err = Math.abs(pred - act);

                if (err > maxsAbserror) {
                    maxsAbserror = err;
                }

            }
        }
        return maxsAbserror;
    }

    /**
     * Calculate the RMS (root mean squared) prediction error
     */
    private double rmsError(DoubleColumn actual, DoubleColumn predicted) throws Exception {
        double runSumPredictedSqErr = 0;
        int count = 0;
        for (int i = 0; i < actual.getRows(); i++) {
            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                count++;
                runSumPredictedSqErr = runSumPredictedSqErr + Math.pow((predicted.getDoubleValue(i) - actual.getDoubleValue(i)), 2);
            }
        }
        if (count > 0) {
            double meanSqErr = runSumPredictedSqErr / count;
            return Math.sqrt(meanSqErr);
        } else {
            throw new Exception("No suitable data");
        }
    }

    /**
     * Calculate the q-squared value
     */
    private double qSquared(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {
        double runsumActuals = 0, ssTotal = 0, ssErr = 0;
        double actual, predicted;
        int count = 0;

        double mv;
        if (isTrainingData) {
            mv = meanActual;
        } else {
            if (haveTrainMeanActual) {
                mv = trainMeanActual;
            } else {
                mv = meanActual;
            }
        }

        for (int i = 0; i < actualColumn.getRows(); i++) {
            if (!actualColumn.isMissing(i) && !predictedColumn.isMissing(i)) {
                predicted = predictedColumn.getDoubleValue(i);
                actual = actualColumn.getDoubleValue(i);
                runsumActuals = runsumActuals + actual;

                //note the mean of the training data is used here. See Tropsha's papers.
                ssTotal = ssTotal + Math.pow((actual - mv), 2);
                ssErr = ssErr + Math.pow((actual - predicted), 2);
                count++;
            }

        }
        if (count > 0) {
            return 1 - (ssErr / ssTotal);
        } else {
            throw new Exception("No suitable data");
        }

    }

    /**
     * Calculate correlation coefficient r between the actual values and the
     * predicted values.
     *
     */
    private double corrCoef(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {
        double actual, predicted, r;

        /**
         * (actual value - actuals means) * (predicted value - predicted mean)
         */
        double numeratorTerm;

        /**
         * The running sum of numeratorTerm
         */
        double runsumNumeratorTerm = 0;

        /**
         * (actual - actuals mean)^2
         *
         */
        double denominatorTermActuals;

        /**
         * (predicted -predicted mean)^2
         */
        double denominatorTermPredicted;
        int count = 0;

        double runsum_denominatorTermActuals = 0;
        double runsum_denominatorTermPredicted = 0;
        double actualMinusactualsMean, predictedMinusPredictedMean;

        /**
         * Loop through training vals *
         */
        for (int i = 0; i < actualColumn.getRows(); i++) {
            if (!actualColumn.isMissing(i) && !predictedColumn.isMissing(i)) {
                predicted = predictedColumn.getDoubleValue(i);
                actual = actualColumn.getDoubleValue(i);

                actualMinusactualsMean = actual - trainMeanActual;
                predictedMinusPredictedMean = predicted - trainMeanPredicted;

                numeratorTerm = actualMinusactualsMean * predictedMinusPredictedMean;
                runsumNumeratorTerm = runsumNumeratorTerm + numeratorTerm;

                denominatorTermActuals = Math.pow(actualMinusactualsMean, 2);
                denominatorTermPredicted = Math.pow(predictedMinusPredictedMean, 2);

                runsum_denominatorTermActuals = runsum_denominatorTermActuals + denominatorTermActuals;
                runsum_denominatorTermPredicted = runsum_denominatorTermPredicted + denominatorTermPredicted;
                count++;
            }
        }

        if (count > 0) {
            double corrCoeffTrain = runsumNumeratorTerm / Math.sqrt(runsum_denominatorTermActuals * runsum_denominatorTermPredicted);
            if (Double.isNaN(corrCoeffTrain) || Double.isInfinite(corrCoeffTrain)) {
                corrCoeffTrain = 0;
            }
            return corrCoeffTrain;
        } else {
            throw new Exception("No suitable data");
        }

    }

    /**
     * Calculate mean absolute error (MAE) of the actual values and the
     * predicted values.
     */
    private double mae(DoubleColumn actual, DoubleColumn predicted) throws Exception {
        DoubleColumn errCol = new DoubleColumn();
        for (int i = 0; i < actual.getRows(); i++) {
            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                errCol.appendDoubleValue(Math.abs(predicted.getDoubleValue(i) - actual.getDoubleValue(i)));
            }
        }
        return new MeanValueCalculator(errCol).doubleValue();
    }

    /**
     * Generate the REC (regression error characteristic) data for this model
     */
    private Data generateRECCurve(DoubleColumn actual, DoubleColumn predicted) throws Exception {
        DoubleColumn errCol = new DoubleColumn();
        DoubleColumn refCol = new DoubleColumn();
        double mean = new MeanValueCalculator(actual).doubleValue();

        for (int i = 0; i < actual.getRows(); i++) {
            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                errCol.appendDoubleValue(Math.abs(predicted.getDoubleValue(i) - actual.getDoubleValue(i)));
                refCol.appendDoubleValue(Math.abs(mean - actual.getDoubleValue(i)));
            }
        }

        ColumnSorter sorter = new ColumnSorter(errCol);
        DoubleColumn sortedErrors = (DoubleColumn) sorter.sort();

        ColumnSorter refSorter = new ColumnSorter(refCol);
        DoubleColumn sortedRef = (DoubleColumn) refSorter.sort();

        double errPreviousModel = 0, errCurrentModel = 0;
        double errPreviousRef = 0, errCurrentRef = 0;
        int correct = 0; // the number of correct predicted datapoints at current tolerance
        int numdata = sortedErrors.getRows();
        double acc = 0; //prediction accuracy at current error tolerance

        /**
         * initialise lists for holding x and y plot data *
         */
        DoubleColumn xPlotDataModel = new DoubleColumn("x");
        DoubleColumn yPlotDataModel = new DoubleColumn("y");
        DoubleColumn xPlotDataRef = new DoubleColumn("xRef");
        DoubleColumn yPlotDataRef = new DoubleColumn("yRef");

        /**
         * run through the REC plot algorithm to generate x and y data for the
         * model's REC curve
         */
        for (int i = 0; i < numdata; i++) {

            errCurrentModel = sortedErrors.getDoubleValue(i);
            if (errCurrentModel > errPreviousModel) {
                acc = (double) correct / (double) numdata;
                yPlotDataModel.appendDoubleValue(acc);
                xPlotDataModel.appendDoubleValue(errPreviousModel);
                errPreviousModel = errCurrentModel;
            }

            errCurrentRef = sortedRef.getDoubleValue(i);
            if (errCurrentRef > errPreviousRef) {
                acc = (double) correct / (double) numdata;
                yPlotDataRef.appendDoubleValue(acc);
                xPlotDataRef.appendDoubleValue(errPreviousRef);
                errPreviousRef = errCurrentRef;
            }
            correct++;

        }
        acc = (double) correct / (double) numdata;
        yPlotDataModel.appendDoubleValue(acc);
        xPlotDataModel.appendDoubleValue(errCurrentModel);

        yPlotDataRef.appendDoubleValue(acc);
        xPlotDataRef.appendDoubleValue(errCurrentRef);

        Data results = new Data();
        results.addColumn(xPlotDataModel);
        results.addColumn(yPlotDataModel);
        results.addColumn(xPlotDataRef);
        results.addColumn(yPlotDataRef);
        return results;
    }
}
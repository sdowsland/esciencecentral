
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.weka.DataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import org.pipeline.core.data.Data;
import weka.core.Instances;
import weka.core.converters.ArffSaver;


/** E-sc block to convert a workflow data wrapper to an Weka ARFF file
 * 
 * @author hugo, Dominic
 */
public class DataToArff extends CloudDataProcessorService {

   
    @Override
    public void executionAboutToStart() throws Exception {
    }

  
    @Override
    public void execute() throws Exception {
        
        String fileName = getEditableProperties().stringValue("FileName", "data2arff.arff");
        Data inputData = getInputDataSet("input-data");
        Instances wekaInstances = new DataToInstances(inputData).toWekaInstances();     
        
        //set weka data name to that retrieved from datawrapper (if one exists)
        String dataName  = inputData.getName();
        if (dataName != null) {
        wekaInstances.setRelationName(dataName);
        }
        
        FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
        ArffSaver saver = new ArffSaver();
        saver.setInstances(wekaInstances);
        File outFile = new File(getWorkingDirectory(), fileName);
        saver.setFile(outFile);
        saver.writeBatch();
        outputFile.addFile(outFile, false);
        setOutputData("arff-file", outputFile);
    }

   
    @Override
    public void allDataProcessed() throws Exception {
    }
}

/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import static com.connexience.server.workflow.service.DataProcessorService.createTempFile;
import com.connexience.server.workflow.util.ZipUtils;
import java.io.File;
import org.pipeline.core.xmlstorage.XmlDataStore;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

/**
 * E-Sc block to load a CSV data file directly into a Weka ARFF file.
 *
 *
 * @author Dominic Searson 24/4/2014
 */
public class CSV2ARFFBlock extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {


        //get input csv file
        XmlDataStore properties = getProperties();
        DocumentRecord record = (DocumentRecord) properties.xmlStorableValue("Source CSV");
        File workingDir = getWorkingDirectory();
        APIBroker api = createApiLink();

        if (record.getCurrentArchiveStatus() != DocumentRecord.UNARCHIVED_ARCHIVESTATUS) {
            throw new ConnexienceException("Input file (" + record.getName() + ") is archived.  Retrieve before running workflow.");
        }

        File csvInputFile = createTempFile(ZipUtils.escapeFileName(record.getName()), workingDir);
        System.out.println("tmpFile name = " + csvInputFile.getName());
        api.downloadToFile(record, csvInputFile);

        Instances wekaInstances;

        //attempt to parse csv
        try {
            CSVLoader loader = new CSVLoader();
            loader.setFile(csvInputFile);
            wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() - 1);
        } catch (Exception ex) {
            throw new WekaException("Could not convert the data in " + record.getName() + " to ARFF format. Reason: " + ex.getMessage());
        }


        //write arff out
        String fileName = "LoadedCSV.arff";
        FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
        ArffSaver saver = new ArffSaver();
        saver.setInstances(wekaInstances);
        File outFile = new File(getWorkingDirectory(), fileName);
        saver.setFile(outFile);
        saver.writeBatch();
        outputFile.addFile(outFile, false);
        setOutputData("output-arff-file", outputFile);

    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}
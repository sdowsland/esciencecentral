
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import java.util.Collections;
import java.util.List;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.maths.MeanValueCalculator;
import weka.classifiers.functions.SMOreg;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.functions.supportVector.RegOptimizer;
import weka.classifiers.functions.supportVector.RegSMOImproved;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.WekaException;

/**
 * E-Sc block to carry out regression modelling of a variable using a Weka 
 * implementation of support vector machine regression (SVMr). Both linear and
 * non-linear regression can be performed. 
 *
 * @author Dominic Searson
 */
public class SupportVectorRegression extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    
    @Override
    public void execute() throws Exception {
       
        SMOreg svmModel = new SMOreg();
        PolyKernel pk = null;
        RBFKernel rk = null;
        boolean usePolyKernel = false;
      
        //  LibSVM svmModel = new LibSVM(); NB: this uses the LibSVM implementation, however it has different methods to the Weka SMOreg implementatiob

        //retrieve inputs (x) and modelled variable (y)
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");

        /* process user parameters */
        boolean yscramble = getEditableProperties().booleanValue("Scramble modelled variable", false); //y-scrambling
        String kernelStr = getEditableProperties().stringValue("Kernel", "Polynomial"); //svm kernel type
        boolean polyLowerOrder = getEditableProperties().booleanValue("Use lower order terms", false);
        String dataFilterStr = getEditableProperties().stringValue("Data filter", "Normalise"); //input data scaling
        double polyExp = getEditableProperties().doubleValue("Polynomial exponent", 1.0); //the value of p in K(x,y) = (x.y)^p in the polynomial case 
        double epsilon = getEditableProperties().doubleValue("Epsilon", 1e-3); //loss parameter
        double gamma = getEditableProperties().doubleValue("Gamma", 0.01); //gamma in RBF kernel K(x, y) = e^-(gamma * ((x-y). (x-y)^2)
        double C = getEditableProperties().doubleValue("C constant", 1.0); //C const. in QP optimisation

        //set epsilon
        if (epsilon < 0) {
            throw new IllegalArgumentException("Illegal value (< 0) found for user block parameter 'Epsilon' : " + epsilon);
        }

        //C constant - this determines the influence of the 'error' on the objective function
        if (C < 0) {
            throw new IllegalArgumentException("Illegal value (< 0) found for user block parameter 'C' : " + C);
        }

        //set optimiser type
        RegOptimizer opt = new RegSMOImproved(); //better than the default, apparently
        svmModel.setRegOptimizer(opt);
        svmModel.getRegOptimizer().setEpsilonParameter(epsilon);
        svmModel.setC(C);


        //set kernel
        switch (kernelStr) {
            case "Polynomial":
                usePolyKernel = true;
                if (polyExp <= 0) {
                    throw new IllegalArgumentException("Illegal value (<= 0) found for user block parameter 'Polynominal exponent' : " + polyExp);
                }
                pk = (PolyKernel) svmModel.getKernel();
                pk.setExponent(polyExp);
                pk.setUseLowerOrder(polyLowerOrder);
                break;
            case "Radial basis function (RBF)":

                if (gamma < 0) {
                    throw new IllegalArgumentException("Illegal value (< 0) found for user block parameter 'Gamma' : " + gamma);
                }

                rk = new RBFKernel();
                svmModel.setKernel(rk);
                rk.setGamma(gamma);
                break;
            default:
                throw new IllegalArgumentException("Unknown value found for user block parameter 'Kernel' : " + kernelStr);
        }


        //configure in-block data filtering
        int dataFilterSelectionIndex;

        if (dataFilterStr.equalsIgnoreCase("Normalise")) {
            dataFilterSelectionIndex = SMOreg.FILTER_NORMALIZE; //in the range [0,1] - inputs only, not modelled variable, see http://weka.sourceforge.net/doc.dev/weka/filters/unsupervised/attribute/Normalize.html
        } else if (dataFilterStr.equalsIgnoreCase("None")) {
            dataFilterSelectionIndex = SMOreg.FILTER_NONE;
        } else if (dataFilterStr.equalsIgnoreCase("Standardise")) {
            dataFilterSelectionIndex = SMOreg.FILTER_STANDARDIZE; //zero mean, unit variance - inputs only, not modelled variable
        } else {
            throw new IllegalArgumentException("Unknown value found for user block parameter 'Data filter' : " + dataFilterStr);
        }

        SelectedTag dataFilterMethods = svmModel.getFilterType();
        SelectedTag selectedMethod = new SelectedTag(dataFilterSelectionIndex, dataFilterMethods.getTags());
        svmModel.setFilterType(selectedMethod);

        //y-scrambling if reqd.
        if (yData.getColumnCount() == 1) {
            Column yCol = yData.column(0);
            if (yscramble) {
                List yCol_list = yCol.getList();
                Collections.shuffle(yCol_list); //shuffle the list (because it's a reference to the list witin yCol, this will be shuffled.
            }

            //convert data to Weka format
            XYDataToInstances converter = new XYDataToInstances(xData, yCol);
            Instances wekaInstances = converter.toWekaInstances();
            wekaInstances.setClassIndex(wekaInstances.numAttributes()-1);
            
             //get data name from 'x' data
            wekaInstances.setRelationName(xData.getName());

         
            //train model
            svmModel.buildClassifier(wekaInstances);

            // Get predictions
            DoubleColumn predicted = new DoubleColumn("Predicted");
            double currVal;
            for (int i = 0; i < wekaInstances.numInstances(); i++) {
                currVal = svmModel.classifyInstance(wekaInstances.instance(i));

                if (Double.isInfinite(currVal) || (Double.isNaN(currVal))) {
                    throw new WekaException("Modelled variable predicted as Inf or NaN during training.");
                }

                predicted.appendDoubleValue(currVal);
            }

            //write predictions to block output
            Data predictionData = new Data();
            predictionData.addColumn(yCol.getCopy());
            predictionData.column(0).setName("Actual");
            predictionData.addColumn(predicted);
            setOutputDataSet("y-actual-predicted", predictionData);

            // Save the model as a block output
            ObjectWrapper modelWrapper = new ObjectWrapper(svmModel);
            setOutputData("model", modelWrapper);

            // Save the model report
            File report = new File(getWorkingDirectory(), getEditableProperties().stringValue("ReportName", "SVMregression_report.txt"));
            ZipUtils.writeSingleLineFile(report, svmModel.toString());
            FileWrapper reportWrapper = new FileWrapper(getWorkingDirectory());
            reportWrapper.addFile(report, false);
            
             //Save ARFF report
            File arffFile = new File(getWorkingDirectory(), "WekaTrainingDataSummary.txt");
            ZipUtils.writeSingleLineFile(arffFile,wekaInstances.toSummaryString());
            reportWrapper.addFile(arffFile, false);
            
            setOutputData("report", reportWrapper);

            //add some model meta-data to block outputs
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(new TextMetadata("ModelDetails", "Algorithm", "SVM regression"));
            mdc.add(new TextMetadata("ModelDetails", "Model type", "Regression"));
            mdc.add(new TextMetadata("ModelDetails", "Weka model class", svmModel.getClass().getSimpleName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of x variables", xData.getColumns()));
            mdc.add(new TextMetadata("ModelDetails", "Modelled variable name", yCol.getName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of training instances", wekaInstances.numInstances()));
            mdc.add(new TextMetadata("ModelDetails", "Input data filtering", svmModel.getFilterType().getSelectedTag().getReadable()));

            if (usePolyKernel) {

                if (pk.getUseLowerOrder()) {
                    mdc.add(new TextMetadata("ModelDetails", "Kernel", "Polynomial K(x,y) = ((x.y)+1)^p "));
                } else {
                    mdc.add(new TextMetadata("ModelDetails", "Kernel", "Polynomial K(x,y) = (x.y)^p "));
                }

                mdc.add(new NumericalMetadata("ModelDetails", "Polynomial exponent p", +pk.getExponent()));
             
            } else { //RBF
                mdc.add(new TextMetadata("ModelDetails", "Kernel", "Radial basis function (RBF)"));
                mdc.add(new NumericalMetadata("ModelDetails", "Gamma", +rk.getGamma()));
            }

            mdc.add(new TextMetadata("ModelDetails", "Optimiser", opt.getClass().getSimpleName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Epsilon loss parameter", svmModel.getRegOptimizer().getEpsilonParameter()));
            mdc.add(new NumericalMetadata("ModelDetails", "C parameter", svmModel.getC()));
            mdc.add(new BooleanMetadata("ModelDetails", "Scrambled modelled variable", yscramble));

            //data attributes 
            mdc.add(new TextMetadata("DataAttributes", "Weka data summary", wekaInstances.toSummaryString() ));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanActual", new MeanValueCalculator((NumericalColumn) yCol).doubleValue()));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanPredicted", new MeanValueCalculator(predicted).doubleValue()));
            mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", true));

            addMetadataToAllOutputs(mdc);

        } else {
            throw new DataException("Only a single output y can be modelled");
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}
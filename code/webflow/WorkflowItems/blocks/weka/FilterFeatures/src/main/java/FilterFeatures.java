
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.weka.DataToInstances;
import com.connexience.server.weka.InstancesToData;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.StringColumn;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.GeneticSearch;
import weka.attributeSelection.GreedyStepwise;
import weka.attributeSelection.Ranker;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;

public class FilterFeatures extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {

        //get user options from block
        String evalType = getEditableProperties().stringValue("Evaluation method", "Hall's correlation feature subset selection (CFS)");
        String searchType = getEditableProperties().stringValue("Search method", "Greedy stepwise");
        int rankedFeatures = getEditableProperties().intValue("Number of ranked features to retain (IG)", -1);// this only works with class labelled output variable

        // Convert the input data to weka instances
        Data inputData = getInputDataSet("input-data");
        DataToInstances converter = new DataToInstances(inputData);
        Instances wekaInstances = converter.toWekaInstances(inputData.getColumnCount() - 1);

        //setup filter feature options
        AttributeSelection filter = new AttributeSelection();
        switch (evalType) {
            case "Hall's correlation feature subset selection (CFS)":
                CfsSubsetEval cfs = new CfsSubsetEval();
                filter.setEvaluator(cfs);
                break;

            case "Information gain with respect to the output (IG)":
                GainRatioAttributeEval gr = new GainRatioAttributeEval();
                filter.setEvaluator(gr);
                Ranker ranker = new Ranker(); //NB MUST use ranker with GainRatio eval method
                ranker.setNumToSelect(rankedFeatures);
                filter.setSearch(ranker); 
                break;
            default:
                throw new IllegalArgumentException("Unknown option : " +evalType);
        }


        if (!evalType.equals("Information gain with respect to the output (IG)")) {

            switch (searchType) {
                case "Greedy stepwise":
                    GreedyStepwise greedySearch = new GreedyStepwise();
                    greedySearch.setSearchBackwards(true);
                    filter.setSearch(greedySearch);
                    break;

                case "Genetic":
                    GeneticSearch genSearch = new GeneticSearch();
                    filter.setSearch(genSearch);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown option: " +searchType);
            }

        }

        //apply filter
        filter.setInputFormat(wekaInstances);
        Instances filteredInstances = Filter.useFilter(wekaInstances, filter);
        filteredInstances.setRelationName(wekaInstances.relationName());

        // write filtered arff out in a filewrapper
        InstancesToData instancesConverter = new InstancesToData(filteredInstances);
        Data filteredData = instancesConverter.toData();
        setOutputDataSet("filtered-data", filteredData);
        
        // write the filter out as an object wrapper
        ObjectWrapper modelWrapper = new ObjectWrapper(filter);
        setOutputData("filter", modelWrapper);

        // List the filtered columns
        Data featureList = new Data();
        StringColumn featureColumn = new StringColumn("Features");
        featureList.addColumn(featureColumn);
        if(filteredData.getColumnCount()>1){
            for(int i=0;i<filteredData.getColumnCount() - 1;i++){
                featureColumn.appendStringValue(filteredData.column(i).getName());
            }
        }
        setOutputDataSet("feature-list", featureList);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
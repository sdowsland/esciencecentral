
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import weka.classifiers.functions.PaceRegression;
import weka.core.Instances;

public class WekaPaceRegression extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");
        if(yData.getColumns()==1){
            Column yCol = yData.column(0);
            XYDataToInstances converter = new XYDataToInstances(xData, yCol);
            Instances wekaInstances = converter.toWekaInstances();
            PaceRegression regression = new PaceRegression();
            regression.buildClassifier(wekaInstances);
            
            // Get predictions
            DoubleColumn predicted = new DoubleColumn("Predicted");
            for(int i=0;i<wekaInstances.numInstances();i++){
                predicted.appendDoubleValue(regression.classifyInstance(wekaInstances.instance(i)));
            }
            Data predictionData = new Data();
            predictionData.addColumn(yCol.getCopy());
            predictionData.column(0).setName("Actual");            
            predictionData.addColumn(predicted);
            setOutputDataSet("y-predicted", predictionData);
            
            // Save the model
            ObjectWrapper modelWrapper = new ObjectWrapper(regression);
            setOutputData("model", modelWrapper);
            
            // Save the report
            File report = new File(getWorkingDirectory(), getEditableProperties().stringValue("ReportName", "report.txt"));
            ZipUtils.writeSingleLineFile(report, regression.toString());
            FileWrapper reportWrapper = new FileWrapper(getWorkingDirectory());
            reportWrapper.addFile(report, false);
            setOutputData("report", reportWrapper);
        } else {
            throw new Exception("Only a single output can be modelled");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
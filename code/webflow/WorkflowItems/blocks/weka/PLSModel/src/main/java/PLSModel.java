/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import java.util.Collections;
import java.util.List;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.maths.MeanValueCalculator;
import weka.classifiers.functions.PLSClassifier;
import weka.core.Instances;
import weka.filters.supervised.attribute.PLSFilter;

/** E-Sc  block that implements a linear PLS regression model using Weka.
 * 
 * @author Hugo, Dominic
 */
public class PLSModel extends CloudDataProcessorService {

    
    @Override
    public void executionAboutToStart() throws Exception {
    }

    
    @Override
    public void execute() throws Exception {
        
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");
        
        if (yData.getColumnCount() ==1 ){
            Column yCol = yData.column(0);
            
            //load data
            XYDataToInstances converter = new XYDataToInstances(xData, yCol);
            Instances wekaInstances = converter.toWekaInstances();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);
            
            //get data name from 'x' data
            wekaInstances.setRelationName(xData.getName());
           
            //create new PLS model and set user parameters and defaults
            PLSClassifier plsrModel = new PLSClassifier();
            PLSFilter plsFilt ;
            
            int components = getEditableProperties().intValue("NumberOfComponents", 3);
          
            if (components < 1) {
                throw new IllegalArgumentException("Illegal value (< 1) found for user block parameter 'NumberOfComponents' : " +components);
            }
          
            if (components > wekaInstances.numAttributes() ) {
                throw new IllegalArgumentException(("Illegal value (> number of x variables) found for user block parameter 'NumberOfComponents' : " +components));
            } 
            
            plsFilt = (PLSFilter) plsrModel.getFilter();
            plsFilt.setNumComponents(components);
             
            //shuffle ycol if required
             boolean yscramble = getEditableProperties().booleanValue("Scramble modelled variable",false); 
            if (yscramble) {             
                List yCol_list = yCol.getList();
                Collections.shuffle(yCol_list); //shuffle the list 
            }
            
            //train model
            plsrModel.buildClassifier(wekaInstances);
            
            // Get predictions
            DoubleColumn predicted = new DoubleColumn("Predicted");
            for(int i=0;i<wekaInstances.numInstances();i++){
                predicted.appendDoubleValue(plsrModel.classifyInstance(wekaInstances.instance(i)));
            }
            
            Data predictionData = new Data();
            predictionData.addColumn(yCol.getCopy());
            predictionData.column(0).setName("Actual");
            predictionData.addColumn(predicted);
            setOutputDataSet("y-actual-predicted", predictionData);
            
            // Save the model
            ObjectWrapper modelWrapper = new ObjectWrapper(plsrModel);
            setOutputData("model", modelWrapper);
            
            // Save the model report
            File report = new File(getWorkingDirectory(), getEditableProperties().stringValue("ReportName", "regression_report.txt"));
            ZipUtils.writeSingleLineFile(report, plsrModel.toString());
            FileWrapper reportWrapper = new FileWrapper(getWorkingDirectory());
            reportWrapper.addFile(report, false);
            
             //Save ARFF report
            File arffFile = new File(getWorkingDirectory(), "WekaTrainingDataSummary.txt");
            ZipUtils.writeSingleLineFile(arffFile,wekaInstances.toSummaryString());
            reportWrapper.addFile(arffFile, false);
            
            setOutputData("report", reportWrapper);
            
            // Create some metadata to attach to the outputs (this is used by ModelReport block to generate a
            //PDF of model performance stats and info)
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(new TextMetadata("ModelDetails", "Algorithm", "Weka PLS regression"));
            mdc.add(new TextMetadata("ModelDetails", "Model type", "Regression"));
            mdc.add(new TextMetadata("ModelDetails", "Weka model class", plsrModel.getClass().getSimpleName()));
            mdc.add(new TextMetadata("ModelDetails", "Algorithm type", plsFilt.getAlgorithm().getSelectedTag().getReadable()  ));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of x variables", xData.getColumns()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of training instances", wekaInstances.numInstances()  ));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of latent vars used", plsFilt.getNumComponents() ));
            mdc.add(new TextMetadata("ModelDetails", "Modelled variable name", yCol.getName()));
            mdc.add(new BooleanMetadata("ModelDetails", "Scrambled modelled variable", yscramble ));
           
            mdc.add(new TextMetadata("DataAttributes", "Weka data summary", wekaInstances.toSummaryString() ));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanActual", new MeanValueCalculator((NumericalColumn)yCol).doubleValue()));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanPredicted", new MeanValueCalculator(predicted).doubleValue()));
            mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", true));
            
            addMetadataToAllOutputs(mdc);
            
        } else {
            throw new DataException("Only a single output y can be modelled");
        }
    }

   
    @Override
    public void allDataProcessed() throws Exception {
    }
}
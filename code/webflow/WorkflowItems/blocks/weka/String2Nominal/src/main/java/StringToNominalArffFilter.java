/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToNominal;

/**
 * E-Sc block that accepts an input Weka ARFF data file and converts any 'String'
 * attributes (variables) to Weka 'Nominal' variables to allow them to be used by
 * Weka classification blocks such as RandomForest. A modified ARFF file is
 * outputted by this block in a FileWrapper.
 * 
 *
 * @author Dominic Searson
 */
public class StringToNominalArffFilter extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {

        FileWrapper arffWrapper = (FileWrapper) getInputData("input-arff-file");

        
        //get user block parameters
        String fileOutName = getEditableProperties().stringValue("FileName", "string2nominal_data.arff");
        
        
        //get input file and convert to ARFF format 
        if (arffWrapper.getFileCount() == 1) {

            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);

            //load str2nominal filter and process
            StringToNominal str2nominal = new StringToNominal();
            str2nominal.setAttributeRange("first-last");
            str2nominal.setInputFormat(wekaInstances);
           
            Instances filterInstances = Filter.useFilter(wekaInstances, str2nominal);
            filterInstances.setRelationName(wekaInstances.relationName());
            
            ArffSaver saver = new ArffSaver();
            saver.setInstances(filterInstances);
            
            //write output ARFF file to filewrapper and export from block    
            FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
            File outFile = new File(getWorkingDirectory(), fileOutName);
            saver.setFile(outFile);
            saver.writeBatch();
            outputFile.addFile(outFile, false);
            setOutputData("output-arff-file", outputFile);

        } else {
            throw new WekaException("Only one ARFF data file can be accepted by this block");
        }

    }
    
    @Override
    public void allDataProcessed() throws Exception {
    }
}
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import java.util.Collections;
import java.util.List;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.maths.MeanValueCalculator;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instances;

/** e-Sc block to implement a Weka SINGLE hidden layer 'multilayer perceptron' 
 *  feedforward neural net and train it on some X,y numeric data.
 * 
 *  Hidden layer units are logistic functions 1/(1+e^-x).
 * 
 *  Training is performed using standard batch/epoch approach with a backpropagation 
 *  gradient descent algorithm with momentum and learning rate terms. 
 *  The objective is to minimise the squared error when predicting the output y over all rows.
 *  
 *  Note: Although the Weka class used allows more than one hidden layer, here 
 *  it is restricted to a single hidden layer for the sake of simplicity of 
 *  management.
 * 
 *  Also when interpreting the output 'Linear Node 0' is the OUTPUT summation node
 *  and 'threshold' is the weight attached to a bias node.
 * 
 * @author Hugo Hiden (modified by Dominic Searson)
 * 
 */
public class WekaNeuralNetwork extends CloudDataProcessorService {

    
    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {
        
        //access workflow data objects
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");
        
        if (yData.getColumnCount() == 1){
            
            int epochs = getEditableProperties().intValue("Epochs", 500);
            if ( epochs < 1 ) {
                throw new IllegalArgumentException("Block parameter 'Epochs' must be >= 1 ");
            }
            
            double learningRate = getEditableProperties().doubleValue("LearningRate", 0.1);
            if ( learningRate < 0 || learningRate > 1) {
                throw new IllegalArgumentException("Block parameter 'LearningRate' must be between 0 and 1");
            }
            
            double momentum = getEditableProperties().doubleValue("Momentum", 0.2);
            if ( momentum < 0 || momentum > 1) {
                throw new IllegalArgumentException("Block parameter 'Momentum' must be between 0 and 1");
            }
            
            
            int validationPercent = getEditableProperties().intValue("ValidationPercent", 25);
            if (validationPercent < 0 || validationPercent > 100) {
               throw new IllegalArgumentException("Block parameter 'ValidationPercent' must be between 0 and 100");
            }
            
            int validationTheshold = getEditableProperties().intValue("ValidationThreshold", 20);
            if (validationTheshold < 0 ) {
                throw new IllegalArgumentException("Block parameter 'ValidationThreshold' must be > 0");
            }
            
            /* shuffles modelled variable before modelling */
            boolean yscramble = getEditableProperties().booleanValue("Scramble modelled variable",false);
            
            Column yCol = yData.column(0);
            
            
            //y-scramble if required
            if (yscramble) {
                List yCol_list = yCol.getList();
                Collections.shuffle(yCol_list); //shuffle the list (because it's a reference to the list witin yCol, this will be shuffled.
            }
            
             //convert data to Weka compatible
            XYDataToInstances converter = new XYDataToInstances(xData, yCol);
            Instances wekaInstances = converter.toWekaInstances();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);
              //get data name from 'x' data
            wekaInstances.setRelationName(xData.getName());
            
             // Set network structural and training properties
            MultilayerPerceptron model = new MultilayerPerceptron();
            model.setTrainingTime(epochs);
            model.setLearningRate(learningRate);
            model.setMomentum(momentum);
            model.setValidationSetSize(validationPercent);
            model.setSeed(getEditableProperties().intValue("Seed", 0));
            model.setValidationThreshold(validationTheshold);
            model.setHiddenLayers(getEditableProperties().stringValue("HiddenNeurons", "10"));
            
            //normalises inputs and outputs between 0 and 1 - note this is different to the standard 'normalisatiion' filter in Weka
            // and probably due to the fact that the nodes are logistic functions which range between 0 and 1 in the output
            boolean normalise = getEditableProperties().booleanValue("NormalizeNumerical", true);
            model.setNormalizeNumericClass(normalise);
            model.setNormalizeAttributes(normalise);
            
            //advanced net options
            model.setReset(getEditableProperties().booleanValue("NoResets", false)); 
            model.setDecay(getEditableProperties().booleanValue("LearningRateDecay", false));
            
            // Train
            model.buildClassifier(wekaInstances);
            
            // Get predictions on training data
            DoubleColumn predicted = new DoubleColumn("Predicted");
          
            for(int i=0; i < wekaInstances.numInstances(); i++) {
                predicted.appendDoubleValue(model.classifyInstance( wekaInstances.instance(i) ));
            }
            
            Data predictionData = new Data();
            predictionData.addColumn(yCol.getCopy());
            predictionData.column(0).setName("Actual");            
            predictionData.addColumn(predicted);
            setOutputDataSet("y-actual-predicted", predictionData);
            
            // Save the model as an esc workflow object
            ObjectWrapper modelWrapper = new ObjectWrapper(model);
            setOutputData("model", modelWrapper);
            
            //Save text report
            File report = new File(getWorkingDirectory(), getEditableProperties().stringValue("ReportName", "networkWeights.txt"));
            ZipUtils.writeSingleLineFile(report, model.toString());
            FileWrapper reportWrapper = new FileWrapper(getWorkingDirectory());
            reportWrapper.addFile(report, false);
            
             //Save ARFF report
            File arffFile = new File(getWorkingDirectory(), "WekaTrainingDataSummary.txt");
            ZipUtils.writeSingleLineFile(arffFile,wekaInstances.toSummaryString());
            reportWrapper.addFile(arffFile, false);
            
            setOutputData("report", reportWrapper);
            
            
            /*  Create model metadata to attach to the outputs
             * This can be used, e.g. by a ModelReport block, to display model type
             * and parameters.
             */
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(new TextMetadata("ModelDetails", "Algorithm", "Weka MLP neural net regression"));
              mdc.add(new TextMetadata("ModelDetails", "Model type", "Regression"));
            mdc.add(new TextMetadata("ModelDetails", "Weka model class", model.getClass().getSimpleName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of x variables", xData.getColumns()));
            mdc.add(new TextMetadata("ModelDetails", "Modelled variable name", yCol.getName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of training instances", wekaInstances.numInstances()  ));
          
            mdc.add(new NumericalMetadata("ModelDetails", "Epochs", model.getTrainingTime() ));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of hidden layers", 1));
            mdc.add(new NumericalMetadata("ModelDetails", "Hidden layer neurons", Integer.parseInt(model.getHiddenLayers())));
            mdc.add(new NumericalMetadata("ModelDetails", "Learning rate", model.getLearningRate() ));
            mdc.add(new TextMetadata("ModelDetails", "Learning rate decay", ""+model.getDecay()));
            mdc.add(new NumericalMetadata("ModelDetails", "Momentum", model.getMomentum()));
            mdc.add(new NumericalMetadata("ModelDetails", "Validation data (% of training data)", model.getValidationSetSize()));
            mdc.add(new NumericalMetadata("ModelDetails", "Validation errors allowed", model.getValidationThreshold()));
            mdc.add(new TextMetadata("ModelDetails", "Data scaling (0-1) ", ""+normalise));
            mdc.add(new BooleanMetadata("ModelDetails", "Scrambled modelled variable", yscramble ));
            
            // adds metadata containing means of actual and training data. This is used by the Model Report block.
            mdc.add(new TextMetadata("DataAttributes", "Weka data summary", wekaInstances.toSummaryString() ));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanActual", new MeanValueCalculator((NumericalColumn)yCol).doubleValue()));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanPredicted", new MeanValueCalculator(predicted).doubleValue()));
            mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", true));
            
            addMetadataToAllOutputs(mdc);
            
        } else {
            throw new DataException("Only a single column 'y' output can be modelled using this block.");
        }
        
    }

    
    @Override
    public void allDataProcessed() throws Exception {
    }
}
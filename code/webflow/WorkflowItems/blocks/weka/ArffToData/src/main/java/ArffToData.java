/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.weka.InstancesToData;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/** E-sc block to convert data in an ARFF file to an workflow data wrapper.
 * 
 * @author Hugo, Dominic
 */
public class ArffToData extends CloudDataProcessorService {

    
    @Override
    public void executionAboutToStart() throws Exception {
    }

    
    @Override
    public void execute() throws Exception {
        FileWrapper arffWrapper = (FileWrapper)getInputData("input-arff-file");
        
        if(arffWrapper.getFileCount()==1){
            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            InstancesToData converter = new InstancesToData(wekaInstances);
            Data loadedData = converter.toData();
            setOutputDataSet("output-data", loadedData);
        } else {
            throw new DataException("ArffToData block can only parse a single ARFF file");
        }
    }

    
    @Override
    public void allDataProcessed() throws Exception {
    }
}
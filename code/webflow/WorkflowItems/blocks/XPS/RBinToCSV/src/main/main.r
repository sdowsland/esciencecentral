
#Load the data
load(rBinaryFile);

#Extract the subset of the data
dataName = getProperty("data");
data = eval(parse(text=dataName))

#Set the Spectra as the output so that it can be further processed
assign(dataName, data)

######## Begin Processing ########
#Do something to the Spectra
spectrum = data$Spectrum;

#Get the yVals (processed data in this case)
counts = spectrum;



start = data$abscissa_start;
incr = data$abscissa_increment;

#If we are converting to binding energe and have kinetic energy then do the conversion
convertToBinding = getProperty("convertToBindingEnergy");
if(convertToBinding){
        #For binding energy count backwards
      start = data$analysis_source_characteristic_energy - data$abscissa_start
      end = start - (length(data$Spectrum) * incr) + incr;
      ev = seq(from=start,to=end,by=-incr);
}else{
       start = data$abscissa_start;
       end = start + (length(data$Spectrum) * incr) - incr;
       ev = seq(from=start,to=end,by=incr);
}

xydata = data.frame(ev, counts);


filename = sub("[.][^.]*$", "", rBinaryFile, perl=TRUE);
csvFile = paste(filename, ".csv", sep="");
csvFile = basename(csvFile);
#Use write.table as write.csv includes headers
write.table(xydata, file=csvFile, row.names = FALSE, col.names = FALSE, sep=",", dec=".")

metadata = data;
metadata$Spectrum = NULL;





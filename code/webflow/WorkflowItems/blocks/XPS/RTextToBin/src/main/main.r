#load the file into the workspace
source(rTextFile);

#generate a filename to save to - needs to match output
#property in service.xml
filename = sub("[.][^.]*$", "", basename(rTextFile), perl=TRUE)
rBinaryFile = paste(filename, ".RData", sep="")

#Export the whole workspace  
print("Exporting whole workspace");
rm(properties);
save.image(file=rBinaryFile);

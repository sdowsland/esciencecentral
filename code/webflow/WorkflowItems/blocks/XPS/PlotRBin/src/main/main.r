
#Load the data
load(rBinaryFile);

#Filename to save the plot as
chart = getProperty("Filename");

#Extract the subset of the data
dataName = getProperty("data");
data = eval(parse(text=dataName))

start = data$abscissa_start;
incr = data$abscissa_increment;
end = start + (length(data$Spectrum) * incr) - (incr * 1);

xvals = seq(from=start,to=end,by=incr);
yvals = data$Spectrum;

png(filename=chart);
plot(xvals, 
     data$Spectrum, 
     type="l",
     xlab=data$abscissa_units,
     ylab=data$corresponding_variable_label)
dev.off()



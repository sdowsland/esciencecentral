%Richard Ashby (110049496)
%XPS Research Scholarship

function [CoutCu,CoutAg,CoutAu]=messedEspectrum(ECu,EAg,EAu,r,CCu,CAg,CAu,c)
EoutCu=(ECu*(1+r))+c;
EoutAg=(EAg*(1+r))+c;
EoutAu=(EAu*(1+r))+c;
CoutCu=spline(EoutCu,CCu,ECu);
CoutAg=spline(EoutAg,CAg,EAg);
CoutAu=spline(EoutAu,CAu,EAu);
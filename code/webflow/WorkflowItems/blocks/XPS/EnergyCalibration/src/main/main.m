# Entry point for users octave service.
# This file gets called once each time
# a chunk of data is passed through the
# service. It should therefore not modify
# the environment status. It is executed
# as a script and therefore can access and
# set any global variables that are needed.

# This displays the various variables in the
# workspace. It is just here to show how
# data is passed in to Octave and can
# be removed.
whos;

%Richard Ashby
%110049496
%XPS Research Scholarship

%Energy Callibration program

%inputting a value for r
r=properties.r; %input('input a value for r (-0.2 to 0.20)');
c=properties.c; %%input('input a value for c (-0.2 to 0.20)');

XPSReferenceSpectra
ECu=CuRef(:,1);
CCu=CuRef(:,2);
EAg=AgRef(:,1);
CAg=AgRef(:,2);
EAu=AuRef(:,1);
CAu=AuRef(:,2);

[CoutCu,CoutAg,CoutAu]=messedEspectrum(ECu,EAg,EAu,r,CCu,CAg,CAu,c);

%Plotting copper reference spectra

%Needed to make graphs save to file in e-SC
figure(1, "visible", "off")

subplot(2,2,1)
plot(ECu,CCu,ECu,CoutCu)
title('Copper Spectrum');
xlabel('Binding Energy (eV)');
ylabel('Count Rates (C)');
legend('Reference','Test');

%Plotting sliver reference spectra
subplot(2,2,2)
plot(EAg,CAg,EAg,CoutAg)
title('Sliver Spectrum');
xlabel('Binding Energy (eV)');
ylabel('Count Rates (C)');
legend('Reference','Test');

%Plotting gold reference spectra
subplot(2,2,3)
plot(EAu,CAu,EAu,CoutAu)
title('Gold Spectrum')
xlabel('Binding Energy (eV)');
ylabel('Count Rates (C)');
legend('Reference','Test');

g=0.5;
x=[-2;-1;0;1;2;3];

[Cuq,Cuqout,Agq,Agqout,Auq,Auqout]=FindPeaks(ECu,CCu,CoutCu,EAg,CAg,CoutAg,EAu,CAu,CoutAu);

[EinCu,EinAg,EinAu,EtestCu,EtestAg,EtestAu]=SumPeaks(g,x,Cuq,Cuqout,Agq,Agqout,Auq,Auqout,CCu,CoutCu,CAg,CoutAg,CAu,CoutAu);

Ein=[EinCu,EinAg,EinAu];
Etest=[EtestCu,EtestAg,EtestAu];

subplot(2,2,4)
plot(Etest,Ein,'-',Etest,Ein,'o')

%Print the graphs to files and set the output filename
print -dpng graph.png;
energyplot.files=['graph.png'];

P=polyfit(Etest,Ein,1)
Y2=polyval(P,Etest)

% Set the output values
pvalues.numerical=P;



# To set file outputs, use:
# y.files = ['name1';'name2';...];

# To plot, make sure there is a gnuplot-bin
# dependency and use:
# plot(data);
# print -dpng graph.png;
# y.files=['graph.png'];
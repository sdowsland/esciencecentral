%Richard Ashby
%110049496

function [EinCu,EinAg,EinAu,EtestCu,EtestAg,EtestAu]=SumPeaks(g,x,Cuq,Cuqout,Agq,Agqout,Auq,Auqout,CCu,CoutCu,CAg,CoutAg,CAu,CoutAu)
%SumPeak Copper reference
CuE=[(Cuq-2):(Cuq+3)]
CuCx=CCu([(Cuq-2):(Cuq+3)])
xCuCx=CuCx.*x
x2CuCx=(x.^2).*CuCx
CuP=sum(CuCx(:,1))
CuQ=sum(xCuCx(:,1))
CuR=sum(x2CuCx(:,1))
CuE0=Cuq
EinCu=CuE0+(g/2)*((CuR-(47/15)*CuQ-(8/5)*CuP)/(CuR-CuQ-(8/3)*CuP))

%SumPeak Copper Test
CuEout=[(Cuqout-2):(Cuqout+3)];
CuCxout=CoutCu([(Cuqout-2):(Cuqout+3)]);
xCuCxout=CuCxout.*x;
x2CuCxout=(x.^2).*CuCxout;
CuPout=sum(CuCxout(:,1));
CuQout=sum(xCuCxout(:,1));
CuRout=sum(x2CuCxout(:,1));
CuE0out=Cuqout;  
EtestCu=CuE0out+(g/2)*((CuRout-(47/15)*CuQout-(8/5)*CuPout)/(CuRout-CuQout-(8/3)*CuPout));

%SumPeak Sliver reference
AgE=[Agq-2:1:Agq+3];
[AgCx]=CAg([(Agq-2):(Agq+3)]);
AgxCx=AgCx.*x;
Agx2Cx=(x.^2).*AgCx;
AgP=sum(AgCx(:,1));
AgQ=sum(AgxCx(:,1));
AgR=sum(Agx2Cx(:,1));
AgE0=Agq;
EinAg=AgE0+(g/2)*((AgR-(47/15)*AgQ-(8/5)*AgP)/(AgR-AgQ-(8/3)*AgP));

%SumPeak Sliver Test
AgoutE=[Agqout-2:1:Agqout+3];
[AgCxout]=CoutAg([(Agqout-2):(Agqout+3)]);
AgxCxout=AgCxout.*x;
Agx2Cxout=(x.^2).*AgCxout;
AgPout=sum(AgCxout(:,1));
AgQout=sum(AgxCxout(:,1));
AgRout=sum(Agx2Cxout(:,1));
AgE0out=Agqout;
EtestAg=AgE0out+(g/2)*((AgRout-(47/15)*AgQout-(8/5)*AgPout)/(AgRout-AgQout-(8/3)*AgPout));

%SumPeak Gold Reference
AuE=[Auq-2:Auq+3];
[AuCx]=CAg([(Agq-2):(Agq+3)]);
AuCx=CAu([(Auq-2):(Auq+3)]);
AuxCx=AuCx.*x;
Aux2Cx=(x.^2).*AuCx;
AuP=sum(AuCx(:,1));
AuQ=sum(AuxCx(:,1));
AuR=sum(Aux2Cx(:,1));
AuE0=Auq;
EinAu=AuE0+(g/2)*((AuR-(47/15)*AuQ-(8/5)*AuP)/(AuR-AuQ-(8/3)*AuP));

%SumPeak Gold Test
AuEout=[Auqout-2:Auqout+3];
[AuCxout]=CoutAg([(Agqout-2):(Agqout+3)]);
AuCxout=CoutAu([(Auqout-2):(Auqout+3)]);
AuxCxout=AuCxout.*x;
Aux2Cxout=(x.^2).*AuCxout;
AuPout=sum(AuCxout(:,1));
AuQout=sum(AuxCxout(:,1));
AuRout=sum(Aux2Cxout(:,1));
AuE0out=Auqout;
EtestAu=AuE0out+(g/2)*((AuRout-(47/15)*AuQout-(8/5)*AuPout)/(AuRout-AuQout-(8/3)*AuPout));





%Richard Ashby
%110049496

function [Cuq,Cuqout,Agq,Agqout,Auq,Auqout]=FindPeaks(ECu,CCu,CoutCu,EAg,CAg,CoutAg,EAu,CAu,CoutAu)
%Copper Referencee
ECu(find((ECu>535)&(ECu<565)));
[Cup X]=max(CCu(find((ECu>535)&(ECu<565))));
[Cuq]=(ECu(find(CCu==Cup)));
%Copper Test
ECu(find((ECu>535)&(ECu<565)));
[Cupout Xout]=max(CoutCu(find((ECu>535)&(ECu<565))));
[Cuqout]=(ECu(find(CoutCu==Cupout)));
%Sliver Reference
EAg(find((EAg>1113)&(EAg<1133)));
[Agp z]=max(CAg(find((EAg>1113)&(EAg<1133))));
[Agq]=(EAg(find(CAg==Agp)));
%Sliver Test
EAg(find((EAg>1113)&(EAg<1133)));
[Agpout zout]=max(CoutAg(find((EAg>1113)&(EAg<1133))));
[Agqout]=(EAg(find(CoutAg==Agpout)));
%Gold Reference 
EAu(find((EAu>1387)&(EAu<1417)));
[Aup y]=max(CAu(find((EAu>1387)&(EAu<1417))));
[Auq]=(EAu(find(CAu==Aup)));
%Gold Test
EAu(find((EAu>1387)&(EAu<1417)));
[Aupout yout]=max(CoutAu(find((EAu>1387)&(EAu<1417))));
[Auqout]=(EAu(find(CoutAu==Aupout)));
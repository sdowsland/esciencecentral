
#Load the data
load(rBinaryFile);

#Filename to save the plot as
chart = getProperty("Filename");


#Extract the subset of the data
dataName = getProperty("data");
data = eval(parse(text=dataName))

#Set the Spectra as the output so that it can be further processed
assign(dataName, data)
rBinaryFileOut = getFileName();
save(list=dataName, file=rBinaryFileOut);

######## Begin Processing ########
#Do something to the Spectra
spectrum = data$Spectrum;

#Get the yVals (processed data in this case)
yvals = spectrum;
######## End Processing ########

#Get the values for the x axis
start = data$abscissa_start;
incr = data$abscissa_increment;
end = start + (length(data$Spectrum) * incr) - (incr * 1);
xvals = seq(from=start,to=end,by=incr);

#Chart title and legend
date = paste(data$day_of_month, data$month, data$year_in_full, sep="-");
time = paste(data$hours, data$minutes, data$seconds, sep=":")
passEnergy = paste(data$analyser_pass_energy_or_retard_ratio_or_mass_resoltuion, sep="")
legend = paste(c("Date", "Time", "Pass Energy"), c(date, time, passEnergy), sep="=")

#Plot the chart
png(filename=chart);
plot(xvals, 
     yvals, 
     main=paste(dataName, " - ", data$sample_identifier, sep=""),
     type="l",
     xlab=paste(data$abscissa_label, " (",data$abscissa_units, ")", sep=""),
     ylab=data$corresponding_variable_label)
legend("topleft", leg=legend)
dev.off()






#Load the R Binary File passed as input
load(rBinaryFileIn);

#Extract the subset of the data that we need
subsetName = getProperty("dataSubset")
subset = eval(parse(text=subsetName));

#Get the last element of the name which we'll save it to
nameElements = strsplit(subsetName, "$", fixed=TRUE)
endOfName = tail(nameElements[[1]], 1);
assign(endOfName, subset)

#Generate a filename and save the file
#The output needs to be the filename, not the file
rBinaryFileOut = getFileName();
save(list=endOfName, file = rBinaryFileOut);



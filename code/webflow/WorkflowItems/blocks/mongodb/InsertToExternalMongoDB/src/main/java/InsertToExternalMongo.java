import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.json.JSONObject;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.xmlstorage.XmlDataStore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

public class InsertToExternalMongo extends CloudDataProcessorService {

    private String hostname;
    private int port;
    private String username;
    private String password;
    private String dbName;
    private String collectioname;

    @Override
    public void executionAboutToStart() throws Exception{
        XmlDataStore props = getProperties();
        hostname = props.stringValue("Hostname", "localhost");
        port = props.intValue("Port", 27017);
        username = props.stringValue("Username", "");
        password = props.stringValue("Password", "");
        dbName = props.stringValue("DatabaseName", "");
        collectioname = props.stringValue("CollectionName", "");
    }

    @Override
    public void execute() throws Exception {

        if(dbName.isEmpty() || collectioname.isEmpty()){
            throw new Exception("Please specify both the database and colleciton names");
        }

        MongoClient mongoClient;
        if(username.isEmpty() || password.isEmpty()){
            mongoClient = new MongoClient(hostname, port);
        }
        else {
            throw new Exception("Authentication not yet supported");
        }

        Data data = getInputDataSet("data");
        MongoDatabase db = mongoClient.getDatabase(dbName);
        MongoCollection collection = db.getCollection(collectioname);
        //insert each row
        for(int i =0; i < data.getLargestRows(); i++) {
            collection.insertOne(dataRowToJson(data, i));
        }

        mongoClient.close();
    }

    public Document dataRowToJson(Data data, int row) throws Exception {

        //28-Apr-2015 12:26:10 (BESiDE date format)
        DateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSSS");

        Document bsonDoc = new Document();
        Column c;
        String name;

        //iterate over each column
        for(int i=0;i<data.getColumns();i++){
            c = data.column(i);

            //remove spaces
            name = c.getName().replace(" ", "_");
            if(!c.isMissing(row)){
                try{

                    //if this is a String try to parse it into a date
                    if(c.getDataType().equals(String.class))
                    {
                        //add the date rather than String
                        Date date = format.parse(c.getStringValue(row));

                        bsonDoc.append(name, date);
                    }
                    else{
                        //If it's not a String, add the object
                        bsonDoc.append(name, c.getObjectValue(row));
                    }
                }
                catch (Exception ignored){
                    //If we can't parse the date then add the object
                    bsonDoc.append(name, c.getObjectValue(row));
                }

            }
        }

        return bsonDoc;
    }


}

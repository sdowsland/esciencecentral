/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.extras.mongodatasets.MongoDatasetQuery;
import com.connexience.extras.mongodatasets.MongoDatasetQueryEnactor;
import com.connexience.extras.mongodatasets.MongoDatasetStorage;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.providers.ExternalClientProvider;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.workflow.*;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import com.connexience.server.workflow.types.WorkflowDatasetQuery;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.Date;
import java.util.Set;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.xmlstorage.XmlDataStore;

public class ExecuteMongoQuery extends CloudDataProcessorService {
    @Override
    public void executionAboutToStart() throws Exception {
        
    }

    @Override
    public void execute() throws Exception {
        WorkflowDatasetQuery q = (WorkflowDatasetQuery)((ObjectWrapper)getInputData("query")).getPayload();
        try {
            DatasetQuery query = q.instantiateQuery();
            if(query instanceof MongoDatasetQuery){
                // Load the server properties to try and identify the Mongo instance
                XmlDataStore store = loadServerPropertiesFile();

                if(store.containsName("SystemProperties")){
                    XmlDataStore sysProps = store.xmlDataStoreValue("SystemProperties");
                    int fetchBlockSize = getEditableProperties().intValue("FetchBlockSize", 5000);
                    
                    if(sysProps.containsName("MongoDB")){
                        ExternalClientProvider provider = new ExternalClientProvider(sysProps.xmlDataStoreValue("MongoDB"));
                        MongoDatasetStorage.clientProvider = provider;
                        
                        Class enactorClass = query.getEnactorClass();
                        MongoDatasetQueryEnactor enactor = (MongoDatasetQueryEnactor)enactorClass.newInstance();
                        MongoDatasetItem item = new MongoDatasetItem();
                        item.setDatasetId(query.getDatasetId());
                        item.setName(query.getItemName());
                        long itemId = createApiLink().lookupDatasetItemId(item.getDatasetId(), item.getName());
                        item.setId(itemId);
                        
                        enactor.setItem(item);
                        enactor.setQuery(query);
                        if(enactor.isCursorSupported()){
                            DBCursor c = enactor.getCursor();     
                            boolean fetched = false;
                            while(c.hasNext()){
                                Data result = buildDataFromCursor(c, fetchBlockSize);
                                setOutputDataSet("results", result);
                                fetched = true;
                            }
                            
                            if(!fetched){
                                System.out.println("No data returned from query");
                                setOutputDataSet("results", new Data());
                            }
                        } else {
                            throw new Exception("Cursor not supported");
                        }
                        
                    } else {
                        throw new Exception("No mongo db properties available");
                    }
                } else {
                    throw new Exception("No system properties available");
                }
                

            } else {
                throw new Exception("Query is not a MongoDB query");
            }
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                MongoDatasetStorage.clientProvider.getClient().close();
            } catch (Exception e){}
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
        
    }
    
    public Data buildDataFromCursor(DBCursor cursor, int maxRows) throws ConnexienceException {
        Data data = new Data();
        
        int count = 0;
        DBObject row;
        Set<String> keys;
        Column column;
        
        try {
            while(cursor.hasNext() && count<maxRows){
                row = cursor.next();
                Data dataRow = new Data();
                
                keys = row.keySet();
                for(String key : keys){
                    if(!key.startsWith("_")){
                        if (dataRow.containsColumn(key)){
                            // Column exists
                            dataRow.column(key).appendObjectValue(row.get(key));
                        } else {
                            // New column
                            Object value = row.get(key);
                            if(value instanceof Number){
                                column = new DoubleColumn(key);
                                ((DoubleColumn)column).appendDoubleValue(((Number)value).doubleValue());
                                dataRow.addColumn(column);
                                
                            } else if(value instanceof Date){
                                column = new DateColumn(key);
                                ((DateColumn)column).appendDateValue((Date)value);
                                dataRow.addColumn(column);
                                
                            } else {
                                column = new StringColumn(key);
                                ((StringColumn)column).appendStringValue(value.toString());
                                dataRow.addColumn(column);
                            }

                        }                                        
                    }
                }
                
                if(data.getLargestRows()>0){
                    data.appendRows(dataRow, true);
                } else {
                    data = dataRow;
                }
                count++;
            }
            return data;
        } catch (Exception e){
            throw new ConnexienceException("Error building data from Mongo Cursor: " + e.getMessage(), e);
        }
    }    
}

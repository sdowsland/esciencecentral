

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.extras.mongodatasets.MongoDatasetStorage;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.providers.ExternalClientProvider;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.workflow.*;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import org.pipeline.core.data.*;
import org.pipeline.core.xmlstorage.XmlDataStore;

public class MongoUpdate extends CloudDataProcessorService {
    DatasetItem item;
    MongoDatasetItem mongoItem;
    
    MongoDatasetStorage storage;
    
    @Override
    public void executionAboutToStart() throws Exception {
        super.executionAboutToStart();
        item = (DatasetItem)((ObjectWrapper)getInputData("mongo-item")).getPayload();

        mongoItem = new MongoDatasetItem();
        mongoItem.setDatasetId(item.getDatasetId());
        mongoItem.setName(item.getName());
        long itemId = createApiLink().lookupDatasetItemId(item.getDatasetId(), item.getName());
        mongoItem.setId(itemId);
        
        // Load the server properties to try and identify the Mongo instance
        XmlDataStore store = loadServerPropertiesFile();

        if(store.containsName("SystemProperties")){
            XmlDataStore sysProps = store.xmlDataStoreValue("SystemProperties");

            if(sysProps.containsName("MongoDB")){
                ExternalClientProvider provider = new ExternalClientProvider(sysProps.xmlDataStoreValue("MongoDB"));
                MongoDatasetStorage.clientProvider = provider;
                storage = new MongoDatasetStorage();
            } else {
                throw new Exception("No mongo db properties available");
            }
        } else {
            throw new Exception("No system properties available");
        }
    }

    @Override
    public void execute() throws Exception {
        Data block = getInputDataSet("data");
        storage.appendMultipleValueData(null, mongoItem, block);
    }

    @Override
    public void allDataProcessed() throws Exception {
        super.allDataProcessed(); //To change body of generated methods, choose Tools | Templates.
        MongoDatasetStorage.clientProvider.getClient().close();
    }

}

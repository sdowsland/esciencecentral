/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import org.pipeline.core.data.Data;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.DocumentRecordLinkItem;
import com.connexience.server.workflow.engine.datatypes.FolderLinkItem;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;


public class GetReferenceInfo extends CloudDataProcessorService
{
    private static final String Prop_GET_FILE_NAME = "Get Name";
    private static final String Prop_GET_FILE_SIZE = "Get Size";
    private static final String Prop_GET_DOCUMENT_ID = "Get Document Id";
    private static final String Prop_GET_COMMENTS = "Get Comments";
    private static final String Prop_GET_CTIME = "Get Creation Time";
    private static final String Prop_GET_MTIME = "Get Last Modification Time";
    private static final String Prop_GET_VERSION_NUMBER = "Get Last Version Number";

    private static final String _ColName_NAME = "Name";
    private static final String _ColName_SIZE = "Size";
    private static final String _ColName_DOC_ID = "Document Id";
    private static final String _ColName_VER_NO = "Version Number";
    private static final String _ColName_COMMENTS = "Comments";
    private static final String _ColName_CTIME = "Creation Time";
    private static final String _ColName_MTIME = "Modification Time";

    //private static final String Prop_FAIL_NON_FILE = "Fail If Not File Reference";
    //private static final String Prop_FAIL_NON_FOLDER = "Fail If Not Folder Reference";
    
    private static final String Prop_INCLUDE_FILES = "Include Files";
    private static final String Prop_INCLUDE_FOLDERS = "Include Folders";

    private static final String Input_FILE_REFERENCES = "file-references";

    private static final String Output_FILE_INFO = "file-info";

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute()
    throws Exception
    {
        LinkWrapper inputData = (LinkWrapper)getInputData(Input_FILE_REFERENCES);
        //boolean failOnNonFiles = getProperties().booleanValue(Prop_FAIL_NON_FILE, false);
        //boolean failOnNonFolders = getProperties().booleanValue(Prop_FAIL_NON_FOLDER, false);
        boolean includeFiles = getProperties().booleanValue(Prop_INCLUDE_FILES, false);
        boolean includeFolders = getProperties().booleanValue(Prop_INCLUDE_FOLDERS, false);

        Data outputData = new Data();
        StringColumn fileNames = null;
        IntegerColumn fileSizes = null;
        IntegerColumn versions = null;
        StringColumn docIds = null;
        StringColumn comments = null;
        DateColumn cTimes = null;
        DateColumn mTimes = null;

        if (getProperties().booleanValue(Prop_GET_FILE_NAME, false)) {
            fileNames = new StringColumn(_ColName_NAME);
            outputData.addColumn(fileNames);
        }
        if (getProperties().booleanValue(Prop_GET_FILE_SIZE, false)) {
            fileSizes = new IntegerColumn(_ColName_SIZE);
            outputData.addColumn(fileSizes);
        }
        if (getProperties().booleanValue(Prop_GET_DOCUMENT_ID, false)) {
            docIds = new StringColumn(_ColName_DOC_ID);
            outputData.addColumn(docIds);
        }
        if (getProperties().booleanValue(Prop_GET_VERSION_NUMBER, false)) {
            versions = new IntegerColumn(_ColName_VER_NO);
            outputData.addColumn(versions);
        }
        if (getProperties().booleanValue(Prop_GET_COMMENTS, false)) {
            comments = new StringColumn(_ColName_COMMENTS);
            outputData.addColumn(comments);
        }
        if (getProperties().booleanValue(Prop_GET_CTIME, false)) {
            cTimes = new DateColumn(_ColName_CTIME);
            outputData.addColumn(cTimes);
        }
        if (getProperties().booleanValue(Prop_GET_MTIME, false)) {
            mTimes = new DateColumn(_ColName_MTIME);
            outputData.addColumn(mTimes);
        }

        int size = inputData.size();
        for (int i = 0; i < size; i++) {
            LinkPayloadItem item = inputData.getItem(i);

            if (item instanceof DocumentRecordLinkItem && includeFiles) {
                //if (failOnNonFolders) {
                //    throw new IllegalArgumentException("A non-folder reference has been encountered while flag " + Prop_FAIL_NON_FOLDER + " was set.");
                //}

                DocumentRecordLinkItem docLink = (DocumentRecordLinkItem)item;
                
                if (fileNames != null) {
                    fileNames.appendStringValue(docLink.getDocument().getName());
                }
                if (fileSizes != null) { 
                    fileSizes.appendLongValue(docLink.getVersion().getSize());
                }
                if (docIds != null) {
                    docIds.appendStringValue(docLink.getDocument().getId());
                }
                if (versions != null) {
                    versions.appendIntValue(docLink.getVersion().getVersionNumber());
                }
                if (comments != null) {
                    comments.appendStringValue(docLink.getVersion().getComments());
                }
                if (mTimes != null) {
                    mTimes.appendDateValue(docLink.getVersion().getTimestampDate());
                }
                if (cTimes != null) {
                    cTimes.appendDateValue(docLink.getDocument().getCreationDate());
                }
            } else if (item instanceof FolderLinkItem && includeFolders) {
                //if (failOnNonFiles) {
                //    throw new IllegalArgumentException("A non-file reference has been encountered while flag " + Prop_FAIL_NON_FILE + " was set.");
                //}

                FolderLinkItem folderLink = (FolderLinkItem)item;
                
                if (fileNames != null) {
                    fileNames.appendStringValue(folderLink.getFolder().getName());
                }
                if (fileSizes != null) {
                    fileSizes.appendObjectValue(MissingValue.get());
                }
                if (docIds != null) {
                    docIds.appendStringValue(folderLink.getFolder().getId());
                }
                if (versions != null) {
                    versions.appendObjectValue(MissingValue.get());
                }
                if (comments != null) {
                    comments.appendObjectValue(MissingValue.get());
                }
                if (mTimes != null) {
                    mTimes.appendObjectValue(MissingValue.get());
                }
                if (cTimes != null) {
                    cTimes.appendDateValue(folderLink.getFolder().getCreationDate());
                }
            }
        }

        setOutputDataSet(Output_FILE_INFO, outputData);
    }
}
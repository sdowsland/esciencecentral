
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;


public class MyService extends CloudDataProcessorService
{
    private static final String Input_TOP_LIST = "top-list";
    private static final String Input_BOTTOM_LIST = "bottom-list";
    
    private static final String Output_COMBINED_LIST = "combined-list";

    /** 
     * This is the main service execution routine. It is called once if 
     * the service has not been configured to accept streaming data or once for
     * each chunk of data if the service has been configured to accept data 
     * streams 
     */
    public void execute() throws Exception
    {
        LinkWrapper joinedList = new LinkWrapper();

        for (LinkPayloadItem item : (LinkWrapper)this.getInputData(Input_TOP_LIST)) {
            joinedList.addItem(item);
        }

        for (LinkPayloadItem item : (LinkWrapper)this.getInputData(Input_BOTTOM_LIST)) {
            joinedList.addItem(item);
        }

        setOutputData(Output_COMBINED_LIST, joinedList);
    }
}

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import org.pipeline.core.data.*;

public class MoveReferencesToFolder extends CloudDataProcessorService
{
    private final static String Prop_DESTINATION = "Destination";
    private final static String Prop_TARGET_FOLDER = "Target Folder";
    private final static String Prop_CREATE_SUBFOLDER = "Create Subfolder";
    private final static String Prop_SUBFOLDER_NAME = "Subfolder Name";

    private final static String Opt_INVOCATION = "INVOCATION_FOLDER";
    private final static String Opt_HOME = "HOME_FOLDER";
    private final static String Opt_TARGET = "TARGET_FOLDER";

    private final static String Input_INPUT_REFS = "input-references";

    private final static String Output_MOVED_REFS = "moved-references";


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();
        Folder targetFolder;

        String destination = getProperties().stringValue(Prop_DESTINATION, "");
        if (Opt_HOME.equals(destination)) {
            // Subfolder of home
            User u = api.getCurrentUser();
            targetFolder = api.getFolder(u.getHomeFolderId());
        } else if (Opt_INVOCATION.equals(destination)) {
            targetFolder = getInvocationFolder();
        } else if (Opt_TARGET.equals(destination)) {
            targetFolder = (Folder)getProperties().xmlStorableValue(Prop_TARGET_FOLDER);
            // The api.getFolder call is needed because if Prop_TARGET_FOLDER has not been set, getProperties still
            // returns a non-null reference.
            targetFolder = api.getFolder(targetFolder.getId());
            if (targetFolder == null) {
                throw new Exception("Chosen destination is '" + Opt_TARGET + "' but property '" + Prop_TARGET_FOLDER + "' has not been set correctly.");
            }
        } else {
            throw new Exception("Unrecognized option for the " + Prop_DESTINATION + " property: '" + destination + "'");
        }

        if (getProperties().booleanValue(Prop_CREATE_SUBFOLDER, false)) {
            // Create a subdir
            String subDirName = getProperties().stringValue(Prop_SUBFOLDER_NAME, "").trim();
            if ("".equals(subDirName)) {
                throw new Exception("Invalid subfolder name: '" + subDirName + "'");
            }

            targetFolder = api.getSubdirectory(targetFolder, subDirName);
        }

        LinkWrapper inputLinks = (LinkWrapper)getInputData(Input_INPUT_REFS);
        LinkPayloadItem item;
        ServerObject obj;
        DocumentRecord retrievedDoc;
        Folder retrievedFolder;
        LinkWrapper outputLinks = new LinkWrapper();
        
        for (int i = 0; i < inputLinks.size(); i++) {
            item = inputLinks.getItem(i);
            obj = item.getServerObject();
            if (obj instanceof Folder) {
                retrievedFolder = api.getFolder(obj.getId());
                retrievedFolder.setContainerId(targetFolder.getId());
                retrievedFolder = api.saveFolder(retrievedFolder);
                outputLinks.addFolder(retrievedFolder);
            } else if(obj instanceof DocumentRecord) {
                retrievedDoc = api.getDocument(obj.getId());
                retrievedDoc = api.saveDocument(targetFolder, retrievedDoc);
                outputLinks.addDocument(retrievedDoc, api.getLatestVersion(retrievedDoc.getId()));
            }
        }

        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputData(Output_MOVED_REFS, outputLinks);
    }
}
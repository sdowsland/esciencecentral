
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import static com.connexience.server.workflow.service.DataProcessorService.createTempDir;
import com.connexience.server.workflow.util.ZipUtils;
import java.io.File;
import org.pipeline.core.data.*;

public class RenameFromReference extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        LinkWrapper ref = (LinkWrapper)getInputData("reference-file");
        if(ref.size()==1){
            FileWrapper inputFiles = (FileWrapper)getInputData("files-to-rename");
            String prefix;
            String refName = ref.getItem(0).getServerObject().getName();
            if(getEditableProperties().booleanValue("StripFileExtenstion", true)){
                 String extension = getExtension(refName);
                 if(extension!=null){
                     prefix = refName.substring(0, refName.length() - extension.length() - 1) + "_";
                 } else {
                     prefix = refName + "_";
                 }
            } else {
                prefix = refName + "_";
            }
            
                    
            File tmpDir = createTempDir("RenameFiles-", "", new File("."));
            FileWrapper renamedFiles = new FileWrapper(tmpDir);

            for (File sourceFile : inputFiles.relativeFiles()) {
                String fileParent = sourceFile.getParent();
                File targetFile;
                if (fileParent == null) {
                    targetFile = new File(tmpDir, prefix + sourceFile.getName());
                } else {
                    targetFile = new File(tmpDir, fileParent + File.separator + prefix + sourceFile.getName());
                    targetFile.getParentFile().mkdirs();
                }

                ZipUtils.copyFile(new File(inputFiles.getBaseDir(), sourceFile.getPath()), targetFile);
                renamedFiles.addFile(targetFile, false);
            }

        setOutputData("renamed-files", renamedFiles);
        
        } else {
            throw new Exception("Block only works with a single reference file");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
    
    /**
     * Get the file name extension
     */
    public static String getExtension(String fileName) {
        int lastDotIdx = fileName.lastIndexOf(".");
        if (lastDotIdx > 0 && lastDotIdx < fileName.length() - 1) {
            return fileName.substring(lastDotIdx + 1).trim();
        }
        return null;
    }    
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import java.util.regex.Pattern;

import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.DocumentRecordLinkItem;
import com.connexience.server.workflow.engine.datatypes.FolderLinkItem;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;


public class MyService extends CloudDataProcessorService
{
    private static final String Prop_FILE_NAME_OR_INDEX = "FileNameOrIndex";
    private static final String Prop_PICK_BY_INDEX = "PickByIndex";
    private static final String Prop_FAIL_IF_EMPTY = "FailIfEmpty";
    
    private static final String Input_INPUT_REFS = "input-references";
    
    private static final String Output_SELECTED_REFS = "selected-references";
    private static final String Output_REMAINING_REFS = "remaining-references";

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        String pattern = getEditableProperties().stringValue(Prop_FILE_NAME_OR_INDEX, "");
        if ("".equals(pattern)) {
            throw new Exception("Property " + Prop_FILE_NAME_OR_INDEX + " has not been set.");
        }

        LinkWrapper files = (LinkWrapper)getInputData(Input_INPUT_REFS);
        LinkWrapper selectedFile = new LinkWrapper();
        LinkWrapper remainingFiles = new LinkWrapper();

        if(getProperties().booleanValue(Prop_PICK_BY_INDEX, false)) {
            int index = Integer.parseInt(pattern);
            System.out.println("Selecting by index: " + index);
            if(index >= 0 && index < files.size()) {
                // Add the file selected by index
                selectedFile.addItem(files.getItem(index));

                // Add all the others
                for (int i = 0; i < index; i++) {
                    remainingFiles.addItem(files.getItem(i));
                }
                for (int i = index + 1; i < files.size(); i++) {
                    remainingFiles.addItem(files.getItem(i));
                }
            }
        } else {
            System.out.println("Selecting by name pattern: " + pattern);
            Pattern regEx = WildcardUtils.createRegexFromGlob(pattern);
            for (LinkPayloadItem item : files) {
                if (item instanceof DocumentRecordLinkItem) {
                    DocumentRecordLinkItem docItem = (DocumentRecordLinkItem)item;
                    if (regEx.matcher(docItem.getDocument().getName()).matches()) {
                        System.out.println("Selecting: " + docItem.getDocument().getName());
                        selectedFile.addItem(item);
                    } else {
                        System.out.println("Not selecting: " + docItem.getDocument().getName());
                        remainingFiles.addItem(item);
                    }
                } else if (item instanceof FolderLinkItem) {
                    FolderLinkItem dirItem = (FolderLinkItem)item;
                    if (regEx.matcher(dirItem.getFolder().getName()).matches()) {
                        System.out.println("Selecting: " + dirItem.getFolder().getName());
                        selectedFile.addItem(item);
                    } else {
                        System.out.println("Selecting: " + dirItem.getFolder().getName());
                        selectedFile.addItem(item);
                    }
                }
            }
        }

        if (!getProperties().booleanValue(Prop_FAIL_IF_EMPTY, false) || selectedFile.size() > 0) {
            setOutputData(Output_SELECTED_REFS, selectedFile);
            setOutputData(Output_REMAINING_REFS, remainingFiles);
        } else {
            throw new Exception("No files '" + pattern + "' present in the input files.");
        }
    }
}
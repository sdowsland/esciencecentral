
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.StringColumn;

public class GetFileReference extends CloudDataProcessorService
{
    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        DocumentRecord doc = (DocumentRecord)getEditableProperties().xmlStorableValue("Source");

        APIBroker api = createApiLink();

        DocumentRecord downloadedRecord = doc;
        DocumentVersion version = api.getLatestVersion(doc.getId());

        System.out.println("Importing: " + doc.getName());

        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        LinkWrapper results = new LinkWrapper();
        results.addDocument(downloadedRecord, version);
        setOutputData("file-reference", results);

        // Get the metadata for the file
        Data metaData = new Data();
        StringColumn category = new StringColumn("Category");
        StringColumn name = new StringColumn("Name");
        StringColumn value = new StringColumn("Value");
        metaData.addColumn(category);
        metaData.addColumn(name);
        metaData.addColumn(value);

        MetadataCollection mdc = api.getMetadata(downloadedRecord);
        for(MetadataItem i : mdc.getItems()){
            category.appendStringValue(i.getCategory());
            name.appendStringValue(i.getName());
            value.appendStringValue(i.getStringValue());
        }

        // Should we generate basic metadata
        if(getEditableProperties().booleanValue("CreateBasicMetadata", true)){
            String categoryName = getEditableProperties().stringValue("GeneratedMetadataCategory", "generated");
            category.appendStringValue(categoryName);
            name.appendStringValue("source-filename");
            value.appendStringValue(downloadedRecord.getName());

            category.appendStringValue(categoryName);
            name.appendStringValue("source-document-id");
            value.appendStringValue(downloadedRecord.getId());

            category.appendStringValue(categoryName);
            name.appendStringValue("source-version-number");
            value.appendStringValue(Integer.toString(version.getVersionNumber()));

            category.appendStringValue(categoryName);
            name.appendStringValue("source-size");
            value.appendStringValue(Long.toString(version.getSize()));
        }

        getEditableProperties().add("ErrorsForNonExistentPorts", false);    // Ignore the missing output
        setOutputDataSet("metadata", metaData);

        // Save the last directory into the global properties
        Folder lastDir = new Folder();
        lastDir.setId(downloadedRecord.getContainerId());
        getGlobalProperties().add("LastImportDir", lastDir);
    }
}
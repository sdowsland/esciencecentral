/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class QueryDB extends CloudDataProcessorService {
    private Connection connection = null;

    /**
     * This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {

    }

    /**
     * This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        connection = getConnection();

        List<String> serviceNames = getListOfServices();
        FileWrapper fw = new FileWrapper(getWorkingDirectory());
        boolean onlyLatestData = getProperties().booleanValue("OnlyLatestData", false);
        String minInvocationId = getProperties().stringValue("MinimumInvocationId", "");
        String maxInvocationId = getProperties().stringValue("MaximumInvocationId", "");
        try {
            for (String serviceName : serviceNames) {


                String query = "select extract (hour from(endtimestamp - timestamp)) as hours, " +
                        "extract(minutes from (endtimestamp - timestamp)) as minutes, " +
                        "extract(milliseconds from (endtimestamp - timestamp)) as millis, consumeddata as consumed, produceddata as produced, " +
                        "workflowengineip, concurrentserviceinvocations, cpuvendor, cpumodel, cpucount, averagecpuspeed, averagecpucachesize, physicalram " +
                        "from graphoperations where " +
                        "name = '" + serviceName + "' ";
                if(onlyLatestData)
                {
                    query+=" AND invocationid::int >= " + getCallMessage().getInvocationId();
                }

                if(minInvocationId != null && !minInvocationId.equals(""))
                {
                    query+=" AND invocationid::int >= " + minInvocationId;
                }

                if(maxInvocationId != null && !maxInvocationId.equals(""))
                {
                    query+=" AND invocationid::int <= " + maxInvocationId;
                }

                System.out.println("query = " + query);

                Statement statement = null;
                ResultSet resultSet = null;
                PrintWriter writer = null;


                File f = new File(getWorkingDirectory() + File.separator + serviceName + ".csv");
                fw.addFile(f, false);

                try {
                    writer = new PrintWriter(new OutputStreamWriter(
                            new BufferedOutputStream(
                                    new FileOutputStream(f)), "UTF-8"));
                    writer.append("duration,consumed,produced,workflowengineip, concurrentserviceinvocations, cpucount, averagecpuspeed, averagecpucachesize, physicalram").println();

                    statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

                    resultSet = statement.executeQuery(query);

                    while (resultSet.next()) {
                        if (resultSet.getString(1) != null) {
                            Double hours = resultSet.getDouble("hours");
                            Double minutes = resultSet.getDouble("minutes");
                            Double millis = resultSet.getDouble("millis");

                            writer.append(Double.toString(convertToMillis(hours, minutes, millis))).append(",")
                                    .append(resultSet.getString("consumed")).append(",")
                                    .append(resultSet.getString("produced")).append(",")
                                    .append(resultSet.getString("workflowengineip")).append(",")
                                    .append(resultSet.getString("concurrentserviceinvocations")).append(",")
//                                    .append(resultSet.getString("cpuvendor")).append(",")
//                                    .append(resultSet.getString("cpumodel")).append(",")
                                    .append(resultSet.getString("cpucount")).append(",")
                                    .append(resultSet.getString("averagecpuspeed")).append(",")
                                    .append(resultSet.getString("averagecpucachesize")).append(",")
                                    .append(resultSet.getString("physicalram"))
                                    .println();
                        }
                    }
                } finally {
                    closeConnections(statement, resultSet, writer);

                }
            }


        } finally {
            if (connection != null) try {
                connection.close();
            } catch (SQLException ignored) {
            }
        }
        setOutputData("output-file", fw);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {

    }

    private double convertToMillis(Double hours, Double minutes, Double millis)
    {
        double total = 0;
        total = total + (hours * 60 * 60 * 1000);
        total = total + (minutes * 60 * 1000);
        total = total + millis;
        return total;
    }

    private List<String> getListOfServices() throws Exception {
        if (connection == null) {
            connection = getConnection();
        }
        boolean onlyLatestData = getProperties().booleanValue("OnlyLatestData", false);
        String minInvocationId = getProperties().stringValue("MinimumInvocationId", "");
        String maxInvocationId = getProperties().stringValue("MaximumInvocationId", "");

        String query = "select distinct name AS name from graphoperations where eventtype = 'SERVICEEXECUTION'";

        if(onlyLatestData)
        {
            query+=" AND invocationid::int >= " + getCallMessage().getInvocationId();
        }
        if(minInvocationId != null && !minInvocationId.equals(""))
        {
            query+=" AND invocationid::int >= " + minInvocationId;
        }
        if(maxInvocationId != null && !maxInvocationId.equals(""))
        {
            query+=" AND invocationid::int <= " + maxInvocationId;
        }

        Statement statement = null;
        ResultSet resultSet = null;
        PrintWriter writer = null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            List<String> serviceNames = new ArrayList<String>();
            while (resultSet.next()) {
                serviceNames.add(resultSet.getString("name"));
            }
            return serviceNames;
        } finally {
            closeConnections(statement, resultSet, writer);
        }
    }

    public void closeConnections(Statement statement, ResultSet resultSet, PrintWriter writer) {
        if (resultSet != null) try {
            resultSet.close();
        } catch (SQLException ignored) {
        }

        if (statement != null) try {
            statement.close();
        } catch (SQLException ignored) {
        }
        if (writer != null) {
            writer.close();
        }
    }


    public Connection getConnection() throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection conn;
        Properties connectionProps = new Properties();
        connectionProps.put("user", getProperties().stringValue("Username", ""));
        connectionProps.put("password", getProperties().stringValue("Password", ""));

        String hostname = getProperties().stringValue("Hostname", "localhost");
        String port = getProperties().stringValue("Port", "5432");
        String databaseName = getProperties().stringValue("Database", "logeventsdb");


        conn = DriverManager.getConnection("jdbc:postgresql://" + hostname +":" + port + "/" + databaseName, connectionProps);
        System.out.println("Connected to database");
        return conn;
    }

}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;


import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nu.xom.canonical.Canonicalizer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class XMLDiff implements Diff
{

  public boolean diff(File file1, File file2)
  {
    try {
      Builder parser = new Builder();
      Document doc1 = parser.build(file1);
      Document doc2 = parser.build(file2);

      ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
      Canonicalizer outputter = new Canonicalizer(baos1);
      outputter.write(doc1);

      ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
      Canonicalizer outputter2 = new Canonicalizer(baos2);
      outputter2.write(doc2);

      byte[] doc1bytes = baos1.toByteArray();
      byte[] doc2bytes = baos2.toByteArray();

      return Arrays.equals(doc1bytes, doc2bytes);
    }
    catch (ValidityException ex) {
      System.err.println("Cafe con Leche is invalid today. (Somewhat embarrassing.)");
    }
    catch (ParsingException ex) {
      System.err.println("Cafe con Leche is malformed today. (How embarrassing!)");
    }
    catch (IOException ex) {
      System.err.println("Could not connect to Cafe con Leche. The site may be down.");
    }
    return false;
  }
}

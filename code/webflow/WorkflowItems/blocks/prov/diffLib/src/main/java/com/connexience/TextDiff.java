/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import name.fraser.neil.plaintext.diff_match_patch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;


public class TextDiff implements Diff
{

  /**
   * Line by line diff for two files using Meyers algorithm
   *
   * @param file1 first file
   * @param file2 second file
   * @return true if there are no differences between the files.  False otherwise
   */
  public boolean diff(File file1, File file2)
  {
    try
    {
      //Meyers algorithm for diff from Google
      diff_match_patch diff = new diff_match_patch();

      BufferedReader br1 = new BufferedReader(new FileReader(file1));
      BufferedReader br2 = new BufferedReader(new FileReader(file2));

      String line1 = br1.readLine();
      String line2 = br2.readLine();

      while (line1 != null & line2 != null)
      {
        //Do the diff
        LinkedList<diff_match_patch.Diff> diffs = diff.diff_main(line1, line2);

        //retutn is list of diffs in the line.  There should only be one and it should be op.equal
        if ((diffs.size() != 1) && (diffs.get(0).operation != diff_match_patch.Operation.EQUAL))
        {
          return false;
        }

        line1 = br1.readLine();
        line2 = br2.readLine();

      }
      return line1 == null && line2 == null;

    }
    catch (Exception e)
    {
      e.printStackTrace();
      return false;
    }
  }

}

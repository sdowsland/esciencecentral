/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.File;

/**
 * Unit test for simple TextDiff.
 */
public class TextDiffTest
    extends TestCase
{
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public TextDiffTest(String testName)
  {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite()
  {
    return new TestSuite(TextDiffTest.class);
  }


  public void testTextDiff()
  {

    Diff textDiff = new TextDiff();
    File file1 = new File("src/test/resources/testdata1.csv");
    File file2 = new File("src/test/resources/testdata1.csv");
    boolean sameFile = textDiff.diff(file1, file2);
    assertTrue(sameFile);

    file2 = new File("src/test/resources/testdata2.csv");
    boolean diffFiles = textDiff.diff(file1, file2);
    assertFalse(diffFiles);

    File file3 = new File("src/test/resources/testdata3.csv");
    diffFiles = textDiff.diff(file1, file3);
    assertFalse(diffFiles);

    File file4 = new File("src/test/resources/testdata4.txt");
    File file5 = new File("src/test/resources/testdata5.txt");
    diffFiles = textDiff.diff(file4, file5);
    assertFalse(diffFiles);

  }

  public void testXmlDiff()
  {
    File xmlFile1 = new File("src/test/resources/testdata1.xml");
    Diff xmlDiff = new XMLDiff();
    boolean sameFile = xmlDiff.diff(xmlFile1, xmlFile1);
    assertTrue(sameFile);

    File xmlFile2 = new File("src/test/resources/testdata2.xml");
    boolean attrOrder = xmlDiff.diff(xmlFile1, xmlFile2);
    assertTrue(attrOrder);

    File xmlFile3 = new File("src/test/resources/testdata3.xml");
    boolean different = xmlDiff.diff(xmlFile1, xmlFile3);
    assertFalse(different);


  }

}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;


import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.Thread;

public class DiffBlock extends CloudDataProcessorService
{

  /**
   * This method is call when a service is about to be started. It is called once
   * regardless of whether or not the service is streaming data. Code that is
   * needed to set up information that needs to be preserved over multiple
   * chunks should be executed here.
   */
  public void executionAboutToStart() throws Exception
  {

  }

  /**
   * This is the main service execution routine. It is called once if the service
   * has not been configured to accept streaming data or once for each chunk of
   * data if the service has been configured to accept data streams
   */
  public void execute() throws Exception
  {
    FileWrapper inputFileWrapper1 = (FileWrapper) getInputData("inputFile1");
    FileWrapper inputFileWrapper2 = (FileWrapper) getInputData("inputFile2");

    File file1 = inputFileWrapper1.getFile(0);
    File file2 = inputFileWrapper2.getFile(0);

    String diffType = getProperties().stringValue("diffType", "txt");
    Diff diff;
    if(diffType.equals("txt"))
    {
     diff = new TextDiff();
    }
    else if(diffType.equals("xml"))
    {
      diff = new XMLDiff();
    }
    else{
      throw new Exception("Unknown diff type " + diffType);
    }

    System.out.println("file1.getName() = " + file1.getName());
    System.out.println("file2.getName() = " + file2.getName());



    boolean same = diff.diff(file1, file2);
    System.out.println("same = " + same);

    File outputFile = new File(System.currentTimeMillis() + ".txt");
    BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
    bw.write(String.valueOf(same));
    bw.close();

    FileWrapper outputWrapper = new FileWrapper();
    outputWrapper.addFile(outputFile);

    setOutputData("outputFile", outputWrapper);
  }

  /**
   * All of the data has been passed through the service. Any clean up code
   * should be placed here
   */
  public void allDataProcessed() throws Exception
  {

  }
}
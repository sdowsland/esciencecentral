/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.StoredCredentials;
import com.connexience.server.model.security.credentials.AmazonCredentials;
import com.connexience.server.model.security.credentials.AzureCredentials;
import com.connexience.server.util.DigestBuilder;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import com.connexience.server.workflow.types.WorkflowFilesystemScanner;
import com.connexience.server.workflow.util.ZipUtils;
import com.microsoft.windowsazure.services.blob.client.CloudBlobClient;
import com.microsoft.windowsazure.services.blob.client.CloudBlobContainer;
import com.microsoft.windowsazure.services.blob.client.CloudBlockBlob;
import com.microsoft.windowsazure.services.core.storage.CloudStorageAccount;
import java.io.File;
import java.io.FileInputStream;
import org.jets3t.service.S3Service;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.pipeline.core.data.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class UploadToScanner extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {

        throw new NotImplementedException();
//        APIBroker api = createApiLink();
//
//        // The following method is used to get a set of input data
//        // and should be configured using the name of the input
//        // data to fetch
//        ObjectWrapper wrapper = (ObjectWrapper)getInputData("scanner");
//        if(wrapper.getPayload() instanceof WorkflowFilesystemScanner){
//            WorkflowFilesystemScanner scanner = (WorkflowFilesystemScanner)wrapper.getPayload();
//            FileWrapper files = (FileWrapper)getInputData("upload-files");
//            ServerObject credentialsObj;
//            if(scanner.getCredentialsId()!=null && !scanner.getCredentialsId().isEmpty()){
//                credentialsObj = api.getServerObject(scanner.getCredentialsId());
//            } else {
//                credentialsObj = null;
//            }
//
//            StoredCredentials credentials = (StoredCredentials)credentialsObj;
//            if(credentials instanceof AzureCredentials && scanner.getTypeName().equals("Azure")){
//                uploadToBlobstore(files, scanner, (AzureCredentials)credentials);
//
//            } else if(credentials instanceof AmazonCredentials && scanner.getTypeName().equals("AmazonS3")){
//                uploadToAmazonS3(files, scanner, (AmazonCredentials)credentials);
//
//            } else if(credentials==null && scanner.getTypeName().equals("Directory")){
//                uploadToDirectory(files, scanner);
//
//            } else {
//                throw new Exception("Invalid scanner / credentials combination: " + scanner.getTypeName() + ":" + credentials.getCredentialType());
//            }
//
//        } else {
//            throw new Exception("scanner input data is not a valid scanner object");
//        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
    
    /** Upload files to a local filesystem */
    private void uploadToDirectory(FileWrapper files, WorkflowFilesystemScanner scanner) throws Exception {
        // Do the uploads
        File tmpFile;
        File targetDir;
        if(scanner.isImportExportSeparationEnabled()){
           targetDir = new File(scanner.getContainerOrBucketName() + File.separator + scanner.getExportDataSuffix());
        } else {
           targetDir = new File(scanner.getContainerOrBucketName());
        }
        
        // Create the upload directory if it doesn't exist
        if(!targetDir.exists()){
            targetDir.mkdirs();
        }
        
        for(int i=0;i<files.getFileCount();i++){
            tmpFile = files.getFile(i);
            ZipUtils.copyFile(tmpFile, new File(targetDir, tmpFile.getName()));
        }        
    }
    
    /** Upload files to AmazonS3 */
    private void uploadToAmazonS3(FileWrapper files, WorkflowFilesystemScanner scanner, AmazonCredentials credentials) throws Exception {
        AWSCredentials myCredentials =  new AWSCredentials(credentials.getAccessKeyId(), credentials.getAccessKey());
        S3Service service = new RestS3Service(myCredentials);                    
        
        String compositeBucketName;
        if(scanner.isImportExportSeparationEnabled()){
            compositeBucketName = scanner.getContainerOrBucketName() + "-" + scanner.getExportDataSuffix();
        } else {
            compositeBucketName = scanner.getContainerOrBucketName();
        }

        // Do the uploads
        File tmpFile;
        S3Object fileObject;

        for(int i=0;i<files.getFileCount();i++){
            tmpFile = files.getFile(i);
            fileObject = new S3Object(tmpFile.getName());
            fileObject.setContentLength(tmpFile.length());
            fileObject.setDataInputFile(tmpFile);
            service.putObject(compositeBucketName, fileObject);
        }
    }
    
    /** Upload files to Azure Blobstore */
    private void uploadToBlobstore(FileWrapper files, WorkflowFilesystemScanner scanner, AzureCredentials ac) throws Exception {
        // Setup the account
        String storageConnectionString = "DefaultEndpointsProtocol=http;AccountName=" + ac.getAccountName() + ";AccountKey=" + ac.getAccountKey();        
        CloudBlobContainer container = null;
        CloudStorageAccount account = CloudStorageAccount.parse(storageConnectionString);

        // Setup the storage container
        if(account!=null){
            CloudBlobClient client = account.createCloudBlobClient();
            if(scanner.isImportExportSeparationEnabled()){
                container = client.getContainerReference(scanner.getContainerOrBucketName() + ";" + scanner.getExportDataSuffix());
            } else {
                container = client.getContainerReference(scanner.getContainerOrBucketName());
            }
            
            container.createIfNotExist();
            
            // Do the uploads
            File tmpFile;
            
            for(int i=0;i<files.getFileCount();i++){
                tmpFile = files.getFile(i);
                CloudBlockBlob blob = container.getBlockBlobReference(tmpFile.getName());
                FileInputStream inStream = new FileInputStream(tmpFile);
                blob.upload(inStream, tmpFile.length());
                inStream.close();
            }
            
        } else {
            throw new ConnexienceException("No cloud storage account created");
        }        
    }
}
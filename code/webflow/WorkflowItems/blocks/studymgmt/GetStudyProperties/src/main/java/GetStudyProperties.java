
/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.PropertiesWrapper;
import com.connexience.server.workflow.types.WorkflowProject;

import java.util.Set;

public class GetStudyProperties extends CloudDataProcessorService {

    public void executionAboutToStart() throws Exception {
    }

    public void execute() throws Exception {
        APIBroker api = createApiLink();
        WorkflowProject p = null;
        boolean useWildcard = getEditableProperties().booleanValue("FilterProperties", false);
        String wildcard = getEditableProperties().stringValue("PropertyNameWildcard", "*");

        if (getEditableProperties().booleanValue("InferProject", true)) {
            // Try and get the project from the workflow invocation folder
            WorkflowInvocationFolder folder = getInvocationFolder();
            if (folder.getProjectId() != null && !folder.getProjectId().isEmpty()) {
                p = api.getProject(Integer.parseInt(folder.getProjectId()));
            } else {
                System.out.println("Workflow not being run as part of a study, outputting empty properties.");
                PropertiesWrapper wrapper = new PropertiesWrapper();
                setOutputData("properties", wrapper);
            }
        } else {
            // Use explcitly specified project
            p = (WorkflowProject) getEditableProperties().xmlStorableValue("Project");
            p = api.getProject(p.getId()); // Re-fetch to get latest data
        }

        if (p != null) {
            PropertiesWrapper wrapper = new PropertiesWrapper();
            Set<String> names = p.getAdditionalProperties().keySet();

            for (String name : names) {
                if (useWildcard) {
                    if (WildcardUtils.wildCardMatch(name, wildcard)) {
                        wrapper.properties().add(name, p.getAdditionalProperties().get(name));
                    }
                } else {
                    wrapper.properties().add(name, p.getAdditionalProperties().get(name));
                }
            }

            setOutputData("properties", wrapper);

        } else {
            System.out.println("Workflow not being run as part of a study, outputting empty properties.");
            PropertiesWrapper wrapper = new PropertiesWrapper();
            setOutputData("properties", wrapper);
        }
    }

    public void allDataProcessed() throws Exception {
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.cloud.services.*;

import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.util.ZipUtils;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.document.*;

import java.io.*;
import java.rmi.RemoteException;
import java.util.*;


public class LoadStudyData extends CloudDataProcessorService {

    private APIBroker api;


    public void execute() throws Exception {
        // This is the API link that can be used to access data in the
        // Inkspot platform. It is configured using the identity of
        // the user executing the service
        api = createApiLink();
        File workingDir = getWorkingDirectory();
        FileWrapper wrapper = new FileWrapper(workingDir);
        Folder folder = (Folder) getProperties().xmlStorableValue("Folder");
        boolean recursive = getProperties().booleanValue("Recursive", false);

        String pattern = getProperties().stringValue("Pattern", "*.*");

        importDirectory(folder, pattern, recursive, workingDir, wrapper);

        setOutputData("imported-files", wrapper);

        // Save the last directory into the global properties
        Folder lastDir = new Folder();
        lastDir.setId(folder.getId());
        getGlobalProperties().add("LastImportDir", lastDir);
    }

    private void importDirectory(Folder folder, String pattern, boolean recursive, File workingDir, FileWrapper wrapper) throws Exception {
        List<DocumentRecord> contents = api.getFolderDocuments(folder);

        if (recursive) {
            List<Folder> subfolders = api.getChildFolders(folder.getId());
            for (Folder subfolder : subfolders) {
                importDirectory(subfolder, pattern, recursive, workingDir, wrapper);
            }
        }

        //sort by name
        Collections.sort(contents, new Comparator<DocumentRecord>() {
            public int compare(DocumentRecord documentRecord, DocumentRecord documentRecord1) {
                return documentRecord.getName().compareTo(documentRecord1.getName());
            }
        });

        for (DocumentRecord doc : contents) {
            if (WildcardUtils.wildCardMatch(doc.getName(), pattern)) {

                //re-get the document so that the archive status is updated
                doc = api.getDocument(doc.getId());

                if (doc.getCurrentArchiveStatus() != DocumentRecord.UNARCHIVED_ARCHIVESTATUS) {
                    throw new ConnexienceException("Input document (" + doc.getName() + ") is archived.  Retrieve before running workflow.");
                }

                File tmpFile = createTempFile(ZipUtils.escapeFileName(doc.getName()), workingDir);
                api.downloadToFile(doc, tmpFile);
                wrapper.addFile(tmpFile, false);
            }
        }
    }


}
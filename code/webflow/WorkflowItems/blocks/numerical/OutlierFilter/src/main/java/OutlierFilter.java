
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.apps.filters.OutlierScreeningFilter;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;

public class OutlierFilter extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        int windowLength = getEditableProperties().intValue("WindowLength", 10);
        double detectionLimit = getEditableProperties().doubleValue("StandardDeviationLimit", 2.0);
        int offset = getEditableProperties().intValue("ValueOffset", 2);
        Data inputData = getInputDataSet("input-data");
        double filterConstant = getEditableProperties().doubleValue("FilterConstant", 0);
        Data outputData = new Data();
        
        for(int i=0;i<inputData.getColumnCount();i++){
            if(inputData.column(i) instanceof NumericalColumn){
                NumericalColumn rawColumn = (NumericalColumn)inputData.column(i);
                DoubleColumn filteredColumn = new DoubleColumn(rawColumn.getName());
                OutlierScreeningFilter filter = new OutlierScreeningFilter(windowLength);
                filter.setDetectionLimit(detectionLimit);
                filter.setReturnedValueOffset(offset);
                filter.setFilterConstant(filterConstant);
                
                for(int j=0;j<rawColumn.getRows();j++){
                    if(!rawColumn.isMissing(j)){
                        filteredColumn.appendDoubleValue(filter.filterValue(rawColumn.getDoubleValue(j)));
                    }
                }
                outputData.addColumn(filteredColumn);
            }
        }

        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputDataSet("filtered-data", outputData);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
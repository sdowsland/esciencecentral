/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.services;
import org.pipeline.core.drawing.*;
import java.io.*;

/**
 * This class holds a set of scaling information for a set of data
 * @author hugo
 */
public class ScalingInformation implements TransferData, Cloneable, Serializable {
    /** Mean values */
    private double[] mean;
    
    /** Standard deviations */
    private double[] std;
    
    /** Static data type reference */
    public final static ScalingInformationDataType SCALING_INFORMATION_TYPE = new ScalingInformationDataType();
    
    /** Creates a new instance of ScalingInformation */
    public ScalingInformation(int size) {
        mean = new double[size];
        std = new double[size];
    }

    /** Get the parameter count */
    public int getParameterCount(){
        return mean.length;
    }
    
    /** Set a mean value */
    public void setMean(int index, double value){
        mean[index] = value;
    }
    
    /** Get a mean value */
    public double getMean(int index){
        return mean[index];
    }
    
    /** Set a standard deviation */
    public void setStd(int index, double value){
        std[index]=value;
    }
    
    /** Get a standard deviation */
    public double getStd(int index){
        return std[index];
    }
    
    /** Get a copy of this object */
    public TransferData getCopy() throws DrawingException {
        try {
            return (TransferData)clone();
        } catch (Exception e){
            throw new DrawingException(e.getLocalizedMessage());
        }
    }

    /** Get the payload */
    public Object getPayload() {
        return this;
    }
}

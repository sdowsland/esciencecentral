
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.cloud.services.support.GnuPlotEngine;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;


import org.pipeline.core.data.*;

public class PhysicalActivityPlot extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper inputWrapper = (FileWrapper)getInputData("activity-data");
        
        String title;
        MetadataItem item;
        MetadataCollection mdc = null;
        if(inputWrapper.getFileCount()==1){
            File dataFile = inputWrapper.getFile(0);
            
            if(getEditableProperties().booleanValue("UseMetadata", true)){
                String category = getEditableProperties().stringValue("MetadataCategory", "Info");
                
                mdc = getInputMetadata("activity-data");
                item = mdc.find(category, getEditableProperties().stringValue("PatientIDMetadata", "PatientID"));
                if(item!=null){ 
                    title = "Activity plot for patient: " + item.getStringValue();
                } else {
                    title = "Activity plot for file: " + dataFile.getName();
                }
            } else {
                title = "Activity plot for file: " + dataFile.getName();
            }
            
            File plotFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("PlotFile", "plot.png"));
            GnuPlotEngine plotEngine = new GnuPlotEngine(this, "plotfiles/activity.txt", plotFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-data", dataFile.getPath());
            plotEngine.getAdditionalParameters().add("plot-file", getEditableProperties().stringValue("PlotFile", "plot.png"));   
            plotEngine.getAdditionalParameters().add("Title", title);
            plotEngine.createPlot();
            
            FileWrapper plotWrapper = new FileWrapper(getWorkingDirectory());
            plotWrapper.addFile(plotFile, false);
            setOutputData("activity-plot", plotWrapper);
            
            if(mdc!=null){
                setOutputMetadata("activity-plot", mdc);
            }
        } else {
            throw new Exception("Only a single file supported");
        }

    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
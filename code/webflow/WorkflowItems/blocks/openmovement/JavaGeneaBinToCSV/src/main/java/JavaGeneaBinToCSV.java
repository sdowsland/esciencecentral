
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.projects.openmovement.GENEAParser;
import com.connexience.projects.openmovement.PageReadListener;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.service.DataProcessorException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hugo - modified by dominic
 */
public class JavaGeneaBinToCSV extends CloudDataProcessorService implements PageReadListener {

    @Override
    public void executionAboutToStart() throws Exception {
        super.executionAboutToStart(); 
    }

    @Override
    public void execute() throws Exception {


        //get user props
        File binFile = ((FileWrapper) getInputData("bin-file")).getFile(0);
        File csvFile = null;
        boolean useDefaultOutputFileName = this.getEditableProperties().booleanValue("DefaultCSVfileName", true);
        
        
        //generate empty output csv file
         if (useDefaultOutputFileName) {
             //replace any whitespace in output csv file name with '_' because Octave has difficulty with it
            String binFileName = binFile.getName();
            String binFileNameNoSpaces = binFileName.replaceAll("\\s","_");
            csvFile = new File(getWorkingDirectory(), binFileNameNoSpaces +".csv");
        }  else {
            String csvFileName = getEditableProperties().stringValue("CSVFileName", "GENEactivData.csv");
            String csvFileNameNoSpaces = csvFileName.replaceAll("\\s","_"); 
            csvFile =  new File(getWorkingDirectory(), csvFileNameNoSpaces);
        }
        
        //start parsing
        GENEAParser parser = new GENEAParser(binFile, csvFile);
        parser.setListener(this);
        parser.setPagesLimited(getEditableProperties().booleanValue("LimitPages", false));
        parser.setPageLimit(getEditableProperties().intValue("MaximumPages", 1000));
        
        System.out.println("Parsing starting ... ");
        try {
            parser.process();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        System.out.println("Parsing complete.");
        
        //set output data file to wrapper
        FileWrapper outWrapper = new FileWrapper(getWorkingDirectory());
        outWrapper.addFile(csvFile, false);
        setOutputData("csv-data", outWrapper);

        // Add metadata if needed
        if (getEditableProperties().booleanValue("CreateMetadata", true)) {
            System.out.println("Creating metadata ....");
            MetadataCollection mdc = new MetadataCollection();
            addHashmapToMetadata("Capabilities", parser.getCapabilitiesProperties(), mdc);
            addHashmapToMetadata("Configuration", parser.getConfigurationProperties(), mdc);
            addHashmapToMetadata("Identity", parser.getIdentityProperties(), mdc);
            addHashmapToMetadata("Memory", parser.getMemoryProperties(), mdc);
            addHashmapToMetadata("Calibration", parser.getCalibrationProperties(), mdc);
            addHashmapToMetadata("Subject", parser.getSubjectProperties(), mdc);
            addHashmapToMetadata("Trial", parser.getTrialProperties(), mdc);
            mdc.add(new TextMetadata("eSc", "SourceBinaryGENEActivFile", binFile.getName()));

            setOutputMetadata("csv-data", mdc);
            System.out.println("Set metadata");

            //write meta to header file and attach to outputs
            writeMetaDataToTextFile(mdc);
        }

    }

    /**
     * Writes the Gene Active MetaData to a separate 'header' text file. This is
     * wip and so header file format may change (Dom).
     *
     * @param mdc A MetaDataCollection containing Gene Active watch metadata
     * @param env The Block environment
     * @param outputs The Block outputs
     */
    private void writeMetaDataToTextFile(MetadataCollection mdc) throws ConnexienceException, IOException, DataProcessorException {

        File headerFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("HeaderInfoFileName", "GENEactivHEADER.txt"));
        PrintWriter writer = new PrintWriter(new FileWriter(headerFile));


        //if meta data is null or empty just write a warning message to the file
        if ((mdc == null) || mdc.size() == 0) {

            writer.write(">Could not find any Gene Active header information\n");

        } else {

            ArrayList<MetadataItem> items = mdc.getItems();

            String category, name, value;
            //loop through items and write each to a file pre-pended by a > 
            for (MetadataItem item : items) {
                category = item.getCategory();
                name = item.getName();
                value = item.getStringValue();
                if (value.isEmpty()) {
                    value = "-";
                }
                writer.write(">Category: " + category + ". Name: " + name + ". Value: " + value);
                writer.write("\n");
            }

        }
        writer.flush();
        writer.close();
        FileWrapper headerWrapper = new FileWrapper(getWorkingDirectory());
        headerWrapper.addFile(headerFile, false);
        setOutputData("header-file", headerWrapper);
    }

    private void addHashmapToMetadata(String categoryName, HashMap<String, String> map, MetadataCollection mdc) {
        String value;
        for (String name : map.keySet()) {
            value = map.get(name).trim();
            mdc.add(new TextMetadata(categoryName, name, value));
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
        super.allDataProcessed(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pageRead(long index, long total) {
        if (getEditableProperties().booleanValue("ReportProgress", false)) {
            try {
                System.out.println("Progress: " + index + "/" + total);
            } catch (Exception e) {
                System.out.println("Error notifying progress: " + e.getMessage());
            }
        }
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.projects.openmovement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 *
 * @author hugo
 */
public class GENEAParser {

    private File inputFile;
    private File outputFile;

    // Device calibration data
    private boolean calibrationOk = false;
    private double xGain;
    private double xOffset;
    private double yGain;
    private double yOffset;
    private double zGain;
    private double zOffset;
    private double volts;
    private double lux;

    // Attributes
    private HashMap<String, String> allProperties = new HashMap<>();
    private HashMap<String, String> identityProperties = new HashMap<>();
    private HashMap<String, String> capabilitiesProperties = new HashMap<>();
    private HashMap<String, String> memoryProperties = new HashMap<>();
    private HashMap<String, String> configurationProperties = new HashMap<>();
    private HashMap<String, String> trialProperties = new HashMap<>();
    private HashMap<String, String> subjectProperties = new HashMap<>();
    private HashMap<String, String> calibrationProperties = new HashMap<>();

    // Total number of pages
    long totalPages = 0;
    long pagesRead = 0;

    private boolean pagesLimited = false;
    private long pageLimit = 100;

    // Page read listener to report progress
    private PageReadListener listener;

    public enum PAGE_TYPE {

        IDENTITY, CAPABILITIES, MEMORY, CONFIGURATION, TRIAL, CALIBRATION, RECORDING, SUBJECT, NOTHING
    }

    public GENEAParser(File inputFile, File outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    public void setListener(PageReadListener listener) {
        this.listener = listener;
    }

    public void setPagesLimited(boolean pagesLimited) {
        this.pagesLimited = pagesLimited;
    }

    public void setPageLimit(long pageLimit) {
        this.pageLimit = pageLimit;
    }

    public void process() throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        PrintWriter writer = new PrintWriter(new FileWriter(outputFile));
        PAGE_TYPE pageType = PAGE_TYPE.NOTHING;
        RecordingPage currentPage = null;
        boolean stopFlag = false;

        String line;
        while ((line = reader.readLine()) != null && stopFlag == false) {
            if (line != null) {
                if ("Device Identity".equals(line)) {
                    pageType = PAGE_TYPE.IDENTITY;

                } else if ("Device Capabilities".equals(line)) {
                    pageType = PAGE_TYPE.CAPABILITIES;

                } else if ("Configuration Info".equals(line)) {
                    pageType = PAGE_TYPE.CONFIGURATION;

                } else if ("Trial Info".equals(line)) {
                    pageType = PAGE_TYPE.TRIAL;

                } else if ("Subject Info".equals(line)) {
                    pageType = PAGE_TYPE.SUBJECT;

                } else if ("Calibration Data".equals(line)) {
                    pageType = PAGE_TYPE.CALIBRATION;

                } else if ("Memory Status".equals(line)) {
                    pageType = PAGE_TYPE.MEMORY;

                } else if ("Recorded Data".equals(line)) {

                    // Write existing page if there is one
                    if (currentPage != null) {
                        currentPage.writePage(writer);
                        pagesRead++;
                        if (listener != null) {
                            listener.pageRead(pagesRead, totalPages);
                        }
                        if (pagesLimited && pagesRead >= pageLimit) {
                            stopFlag = true;
                        }
                        currentPage = null;
                    }
                    currentPage = new RecordingPage();
                    pageType = PAGE_TYPE.RECORDING;

                } else if (line.trim().isEmpty()) {
                    pageType = PAGE_TYPE.NOTHING;
                } else {
                    // Potentially in an interesting section
                    switch (pageType) {
                        case CALIBRATION:
                            processCalibrationLine(line);
                            calibrationOk = true; // Assumes that as soon as any calibration is read all is Ok
                            break;

                        case CAPABILITIES:
                            processCapabilitiesLine(line);
                            break;

                        case CONFIGURATION:
                            processConfigLine(line);
                            break;

                        case IDENTITY:
                            processIdentityLine(line);
                            break;

                        case MEMORY:
                            processMemoryLine(line);
                            break;

                        case SUBJECT:
                            processSubjectLine(line);
                            break;

                        case TRIAL:
                            processTrialLine(line);
                            break;

                        case RECORDING:
                            if (currentPage != null) {
                                currentPage.processLine(line);
                            } else {
                                System.out.println("Not in page");
                            }
                            break;

                        default:
                            System.out.println("Unrecognised data: " + line);
                            break;

                    }

                }

            }
        }
        writer.flush();
        writer.close();
    }

    private void processIdentityLine(String line) {
        String name = getPropertyLabel(line);
        String value = getPropertyValue(line);
        allProperties.put(name, value);
        identityProperties.put(name, value);
    }

    private void processCapabilitiesLine(String line) {
        String name = getPropertyLabel(line);
        String value = getPropertyValue(line);
        allProperties.put(name, value);
        capabilitiesProperties.put(name, value);
    }

    private void processConfigLine(String line) {
        String name = getPropertyLabel(line);
        String value = getPropertyValue(line);
        allProperties.put(name, value);
        configurationProperties.put(name, value);
    }

    private void processTrialLine(String line) {
        String name = getPropertyLabel(line);
        String value = getPropertyValue(line);
        allProperties.put(name, value);
        trialProperties.put(name, value);
    }

    private void processSubjectLine(String line) {
        String name = getPropertyLabel(line);
        String value = getPropertyValue(line);
        allProperties.put(name, value);
        subjectProperties.put(name, value);
    }

    private void processMemoryLine(String line) {
        String name = getPropertyLabel(line);
        String value = getPropertyValue(line);
        allProperties.put(name, value);
        memoryProperties.put(name, value);
        if (name.equals("Number of Pages")) {
            totalPages = Long.parseLong(value);
        }
    }

    private void processCalibrationLine(String line) {
        try {
            String name = getPropertyLabel(line);
            String value = getPropertyValue(line);
            allProperties.put(name, value);
            capabilitiesProperties.put(name, value);

            if (name.equals("x gain")) {
                xGain = Double.parseDouble(value);

            } else if (name.equals("x offset")) {
                xOffset = Double.parseDouble(value);

            } else if (name.equals("y gain")) {
                yGain = Double.parseDouble(value);

            } else if (name.equals("y offset")) {
                yOffset = Double.parseDouble(value);

            } else if (name.equals("z gain")) {
                zGain = Double.parseDouble(value);

            } else if (name.equals("offset")) {
                zOffset = Double.parseDouble(value);

            } else if (name.equals("Volts")) {
                volts = Double.parseDouble(value);

            } else if (name.equals("Lux")) {
                lux = Double.parseDouble(value);

            }

        } catch (Exception e) {
            System.out.println("Cannot process CALIBRATION: " + line);
        }
    }

    private String getPropertyLabel(String line) {
        StringTokenizer t = new StringTokenizer(line, ":");
        return t.nextToken();
    }

    private String getPropertyValue(String line) {
        StringTokenizer t = new StringTokenizer(line, ":");

        // Find the location of the first :
        int location = line.indexOf(":");
        return line.substring(location + 1);
    }

    public HashMap<String, String> getAllProperties() {
        return allProperties;
    }

    public HashMap<String, String> getCapabilitiesProperties() {
        return capabilitiesProperties;
    }

    public HashMap<String, String> getConfigurationProperties() {
        return configurationProperties;
    }

    public HashMap<String, String> getIdentityProperties() {
        return identityProperties;
    }

    public HashMap<String, String> getMemoryProperties() {
        return memoryProperties;
    }

    public HashMap<String, String> getSubjectProperties() {
        return subjectProperties;
    }

    public HashMap<String, String> getTrialProperties() {
        return trialProperties;
    }

    public HashMap<String, String> getCalibrationProperties() {
        return calibrationProperties;
    }

    /**
     * One single page of activity data
     */
    public class RecordingPage {

        private int lineIndex = 0;
        private String dataBits;
        private Date startDate;
        private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        private boolean headerOk = false;
        private double measurementFrequency = 100.0;
        private long millisecondsPerSample = 10;
        private double temperature;

        private ArrayList<String> csvData = new ArrayList<>();

        public RecordingPage() {
        }

        public void processLine(String line) {
            lineIndex++;
            if (lineIndex < 9) {
                String name = getPropertyLabel(line);
                String value = getPropertyValue(line);

                if (name.equals("Page Time")) {
                    try {
                        startDate = dateFormat.parse(value);
                        headerOk = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (name.equals("Measurement Frequency")) {
                    measurementFrequency = Double.parseDouble(value);
                    millisecondsPerSample = (long) (1000 / measurementFrequency);

                } else if (name.equals("Temperature")) {
                    temperature = Double.parseDouble(value);

                }

            } else {
                dataBits = GENEAParser.hexStringToBitString(line);
                processBitSet();
            }
        }

        public void processBitSet() {
            int location = 0;

            DecimalFormat format = new DecimalFormat();
            format.setMinimumFractionDigits(4);
            format.setMaximumFractionDigits(4);

            DecimalFormat temperatureFormat = new DecimalFormat();
            temperatureFormat.setMinimumFractionDigits(1);
            temperatureFormat.setMaximumFractionDigits(1);

            boolean finished = false;
            String xBits, yBits, zBits, luxBits, buttonBits, reserved;
            String xSign, ySign, zSign;
            double xValue, yValue, zValue;
            long luxValue;
            String reading;

            for (int i = 0; i < 300; i++) {
                location = 48 * i;
                reading = dataBits.substring(location, location + 48);

                // Get the lux bits
                xSign = reading.substring(0, 1);
                xBits = reading.substring(1, 12);

                ySign = reading.substring(12, 13);
                yBits = reading.substring(13, 24);

                zSign = reading.substring(24, 25);
                zBits = reading.substring(25, 36);

                luxBits = reading.substring(36, 46);
                buttonBits = reading.substring(46, 47);
                reserved = reading.substring(47, 48);

                // Light measurement
                luxValue = (long) (GENEAParser.valueOfBitString(luxBits) * lux / volts);

                // X-Accel
                if (xSign.equals("1")) {
                    xValue = (((double) GENEAParser.valueOfBitString(xBits) - 2048) * 100.0 - xOffset) / xGain;
                } else {
                    xValue = (((double) GENEAParser.valueOfBitString(xBits)) * 100.0 - xOffset) / xGain;
                }

                // Y-Accel
                if (ySign.equals("1")) {
                    yValue = (((double) GENEAParser.valueOfBitString(yBits) - 2048) * 100.0 - yOffset) / yGain;
                } else {
                    yValue = (((double) GENEAParser.valueOfBitString(yBits)) * 100.0 - yOffset) / yGain;
                }

                // Z-Accel
                if (zSign.equals("1")) {
                    zValue = (((double) GENEAParser.valueOfBitString(zBits) - 2048) * 100.0 - zOffset) / zGain;
                } else {
                    zValue = (((double) GENEAParser.valueOfBitString(zBits)) * 100.0 - zOffset) / zGain;
                }

                // Work out the timestamp
                Date timestamp = new Date(startDate.getTime() + (i * millisecondsPerSample));

                //System.out.println(reading + ": " + xBits + yBits + zBits + luxBits + buttonBits + reserved + " lux=" + lux);
                csvData.add(dateFormat.format(timestamp) + "," + format.format(xValue) + "," + format.format(yValue) + "," + format.format(zValue) + "," + luxValue + "," + buttonBits + "," + temperatureFormat.format(temperature));
            }
        }

        public void writePage(PrintWriter writer) {
            for (String s : csvData) {
                writer.println(s);
            }
        }

    }

    public static long valueOfBitString(String s) {
        int valCounter = s.length() - 1;
        long value = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                value += (long) Math.pow(2, valCounter);
            }
            valCounter--;
        }
        return value;
    }

    public static String hexStringToBitString(String s) {
        int len = s.length();
        StringBuilder bits = new StringBuilder();
        String bt;
        for (int i = 0; i < len; i += 2) {
            bt = s.substring(i, i + 2);
            bits.append(hexToByte(bt));

        }
        return bits.toString();
    }

    public static String hexToByte(String hex) {
        BitSet bs = new BitSet();
        HexUtil.hexToBits(hex, bs, 8);

        StringBuilder bits = new StringBuilder();
        for (int i = 7; i >= 0; i--) {
            if (bs.get(i)) {
                bits.append("1");
            } else {
                bits.append("0");
            }
        }

        return bits.toString();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            GENEAParser parser = new GENEAParser(new File("/Users/hugo/Downloads/SJW.bin"), new File("/Users/hugo/sjw2.csv"));

            parser.process();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

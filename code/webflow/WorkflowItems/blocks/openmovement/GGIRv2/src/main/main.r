


#main script
# rm(list=ls())
graphics.off()
#======================================================
# INPUT NEEDED:
#===========================================

# Where is the data?
# Note: datadir can be an array with specific names or a folder name.
# If it is a folder name then all csv and bin files within the folder need to be
# accelerometer data


datadir = inputFiles; # "/Users/nsjw7/Desktop/GGIR/SJW__014890_2013-05-28_13-46-45.bin"
studyname = "ggir"
outputdir = getwd(); #"/Users/nsjw7/Desktop/GGIR"    # Where do you want to store the output?6

f0 = c()
f1 = c()

do.cal= TRUE #< turn this to TRUE

mode = c(1:5) # analysis mode
#---------------------------------
# general and physical activity related parameters
includedaycrit = 10 # number of minimum valid hours in a day to attempt physical activity analysis
strategy = 1 # strategy
maxdur = 14 # How many DAYS of measurement do you maximumally expect?
hrs.del.start = 0 # Only relevant when strategy = 2. How many HOURS need to be ignored at the START of the measurement?
hrs.del.end = 0 # Only relevant when strategy = 2. How many HOURS need to be ignored at the END of the measurement?
idloc = 1 # specify id-location
mvpathreshold = c(100) #needs to include 100 if you want the report to work (currently hard coded, may become mroe flexible later on)
qlevels = c(c(1380/1440),c(1410/1440))
#---------------------------------
# sleep related parameters:
includenightcrit = 10 # number of minimum valid hours in a day to attempt sleep analysis
excludefirstlast = FALSE # Exclude first and last night for sleep analysis?
sleeplogidnum = TRUE # Is the participant in the sleep log stored as a number (TRUE) or as a character (FALSE)
MIN_NAP = c(10)
MIN_SLEEP_INTER = c(0)
ignorenonwear = FALSE # see tutorial
nightsperpage = 7
def.noc.sleep = c(21,9)
#---------------------------------
# if sleep log is available:
loglocation= c() # full directory and name of the sleep log (if available, otherwise leave value as c() )
colid=1 #colomn in which the participant id or filename is stored
coln1=9 #column number for first day
nnights = maxdur #number of nights in the sleep log
relyonsleeplog = FALSE
# logcorrdectionkey= c() #"Q:/scripts/GGIR/test_version4/correction key_v5.csv" # c()
criterror = 3
outliers.only = TRUE
#-------------------------------------------
# report related
viewingwindow=2
dofirstpage = TRUE



#======================================================
# NO INPUT NEEDED FROM HERE ONWARDS:
#===========================================
# load functions
ffnames = dir(paste(outputdir,"/functions",sep="")) # creating list of filenames of scriptfiles to load
#for (i in 1:length(ffnames)) {
#  source(paste(outputdir,"/functions/",ffnames[i],sep="")) #loading scripts for reading geneactiv data
#}

g.shell.GGIR(mode=mode,datadir=datadir,studyname=studyname,outputdir=outputdir,f0=f0,f1=f1,strategy=strategy,
             hrs.del.start=hrs.del.start,hrs.del.end=hrs.del.end,idloc=idloc,mvpathreshold=mvpathreshold,
             qlevels=qlevels,
           do.hfenplus = FALSE,do.teLindert2013=FALSE,do.anglez=TRUE,do.enmoa=TRUE,do.cal=do.cal,
             dofirstpage = dofirstpage,viewingwindow=viewingwindow,maxdur=maxdur, nightsperpage =nightsperpage,MIN_NAP = MIN_NAP,MIN_SLEEP_INTER = MIN_SLEEP_INTER,ignorenonwear=FALSE,
           loglocation = loglocation,logcorrectionkey = logcorrectionkey,includedaycrit=includedaycrit,
           colid = colid,coln1 = coln1,nnights = nnights,outliers.only = outliers.only,
           excludefirstlast=excludefirstlast,criterror = criterror,includenightcrit=includenightcrit,
           relyonsleeplog=relyonsleeplog,sleeplogidnum=sleeplogidnum,def.noc.sleep=def.noc.sleep);

           outputFiles = list.files(path="output_ggir", recursive=TRUE, full.names=TRUE);

print("Current Dir");
print(getwd());
print("outputFiles");
print(outputFiles);
print("---");



dir.create(paste(getwd(), "/test", "/meta", sep=""), recursive=TRUE)
dir.create(paste(getwd(), "/test", "/results", sep=""), recursive=TRUE)
#file.rename(basename(inputFiles), paste(getwd(), "test/meta", basename(inputFiles), sep="/"))

maxdur = getProperty("maxdur");
hrs.del.start = getProperty("hrs.del.start");
hrs.del.end = getProperty("hrs.del.end");
idloc = getProperty("idloc"); #2 for Whitehall!
strategy = getProperty("strategy"); #2 for Whitehall!

part2optimistic(filename=inputFiles,outputfolder="/test",idloc = idloc, strategy = strategy,
      hrs.del.start = hrs.del.start, hrs.del.end = hrs.del.end,maxdur = maxdur)

#Add the files to the output if they exist
outputFiles = list.files(path=".", pattern="*.xlsx", full.names=TRUE)


/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.apache.commons.lang.StringUtils;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ExtractHeadersJava extends CloudDataProcessorService {

    private HashMap<String, String> headers = new HashMap<String, String>();

    /**
     * This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {

    }

    /**
     * This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper fw = (FileWrapper) getInputData("input-file");

        if (fw.getFileCount() != 1) {
            throw new ConnexienceException("Extract Headers only works with single files");
        }


        DataInputStream in = new DataInputStream(new FileInputStream(fw.getFile(0)));
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line;

        boolean genea = false;


        while ((line = br.readLine()) != null) {
            if (StringUtils.isEmpty(line)) {

                if (genea) {
                    break;  //GENEA has blank line at the end of the headers.  GENEActiv has blank lines in the middle
                } else {
                    //do nothing
                }

            } else if (line.equalsIgnoreCase("Recorded Data")) {
                break;  //Stop processing GENEActiv when we get to the first page header
            } else if (line.startsWith("GENEA/")) {
                genea = true;  //we've got GENEA data
            }
            else{
                //parse the line
                if(line.contains(":"))
                {
                    //: is the separator
                    int colonLoc = line.indexOf(":");
                    String key = line.substring(0, colonLoc);
                    String val = line.substring(colonLoc + 1, line.length());

                    //replace spaces with underscores
                    if(key.contains(" "))
                    {
                        key = key.replace(" ", "_");
                    }

                    //don't include empty or \0x00 strings
                    if(!StringUtils.isEmpty(val.trim()))
                    {
                        headers.put(key, val);
                    }
                }
            }
        }


        //Create the output data and add the hashmap to it
        Data outputData = new Data();
        Column keys = new StringColumn("key");
        Column values = new StringColumn("values");

        for(String key : headers.keySet())
        {
            keys.appendStringValue(key);
            values.appendStringValue(headers.get(key));
        }

        outputData.addColumn(keys);
        outputData.addColumn(values);

        setOutputDataSet("headers", outputData);

        br.close();
        in.close();

    }

    private void parseLine(String line) {

    }


    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {

    }
}

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p/>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.util.CommandRunner;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.cloud.library.CloudWorkflowServiceLibraryItem;
import com.connexience.server.workflow.cloud.library.types.BinaryLibrary;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.json.JSONObject;
import org.pipeline.core.data.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CWASplit extends CloudDataProcessorService {
    SimpleDateFormat df;

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
        df = new SimpleDateFormat(getEditableProperties().stringValue("DateFormat", "yyyy-MM-dd HH:mm:ss.SSS"));
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper wrapper = (FileWrapper) getInputData("cwa-file");

        if (wrapper.getFileCount() == 1) {
            File inputFile = wrapper.getFile(0);
            String line;
            long lineCount = 0;
            int subsampleRate = getEditableProperties().intValue("SubsampleRate", 1000);

            // Convert the CWA file to .CSV first
            File tempFile = createTempFile("cwatemp");
            CloudWorkflowServiceLibraryItem cwaItem = getDependencyItem("CWAConvert");
            BinaryLibrary cwaLibrary = (BinaryLibrary) cwaItem.getWrapper();
            BinaryLibrary.Executable exec = cwaLibrary.getExecutable("cwa-convert");

            String cmd = cwaLibrary.getFile(exec.getRelativeCmd()) + " " + inputFile.getName() + " -f:csv -out " + tempFile.getName();
            System.out.println(cmd);
            CommandRunner runner = new CommandRunner();
            runner.run(cmd);
            System.out.println("Conversion exit code: " + runner.getExitCode());

            String date, fullDate, xyz;
            String startDate = "", endDate = "";
            long elapsed;
            ArrayList<File> fileList = new ArrayList<>();
            HashMap<String, PrintWriter> writerMap = new HashMap<>();
            HashMap<String, Long> offsetMap = new HashMap<>();

            FileInputStream inStream = new FileInputStream(tempFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

            File summaryFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("SummaryFilename", "summary.csv"));
            PrintWriter summaryWriter = new PrintWriter(summaryFile);

            int fileCount = 1;

            //Setup the date formats for input and output
            SimpleDateFormat inputDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat outputDF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

            while ((line = reader.readLine()) != null) {
                date = line.substring(0, 10).trim();
                fullDate = line.substring(0, 23).trim();
                xyz = line.substring(23, line.length());

                Date parsedDate = inputDF.parse(fullDate);

                //Setup the start and end dates to save the metadata
                if (startDate.equals("")) {
                    startDate = fullDate;
                }
                endDate = fullDate;

                if (!writerMap.containsKey(date)) {
                    // Need to open a new file
                    createWriterForDate(date, fullDate, writerMap, offsetMap, fileList, fileCount);
                    fileCount++;
                }
                elapsed = df.parse(fullDate).getTime() - offsetMap.get(date);
                writerMap.get(date).println(outputDF.format(parsedDate) + xyz);

                lineCount++;
                if ((lineCount % subsampleRate) == 0) {
                    //summaryWriter.println(line);
                    summaryWriter.print(outputDF.format(parsedDate) + xyz);
                }
            }

            // Close all of the writers
            for (PrintWriter p : writerMap.values()) {
                p.flush();
                p.close();
            }
            summaryWriter.flush();
            summaryWriter.close();
            reader.close();
            inStream.close();

            FileWrapper outputFiles = new FileWrapper(getWorkingDirectory());
            for (File f : fileList) {
                outputFiles.addFile(f, false);
            }
            setOutputData("split-files", outputFiles);


            FileWrapper summaryWrapper = new FileWrapper(getWorkingDirectory());
            summaryWrapper.addFile(summaryFile, false);
            setOutputData("summary-file", summaryWrapper);

            // Remove the temporary file
            tempFile.delete();

            //Export the metadata as JSON
            JSONObject md = new JSONObject();
            md.put("start", startDate);
            md.put("end", endDate);

            Date startDateD = df.parse(startDate);
            Date endDateD = df.parse(endDate);
            long diff = endDateD.getTime() - startDateD.getTime();
            long days =  TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            //This is incremented as Lynn Rochester's group need the number of complete days
            //which is calculated and also the addition of two half days, one at either end.
            //These half days are added together to get an extra full day.
            md.put("days", days + 1);

            File jsonFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("MetaDataFilename", "ax_times_md.json"));
            PrintWriter jsonWriter = new PrintWriter(jsonFile);
            jsonWriter.write(md.toString());
            jsonWriter.flush();
            jsonWriter.close();

            FileWrapper jsonWrapper = new FileWrapper(getWorkingDirectory());
            jsonWrapper.addFile(jsonFile, false);
            setOutputData("md-file", jsonWrapper);

        } else {
            throw new Exception("Only supports a single cwa-file");
        }
    }

    private void createWriterForDate(String dateText, String fullDate, HashMap<String, PrintWriter> writerMap, HashMap<String, Long> offsetMap, ArrayList<File> fileMap, int fileCount) throws Exception {
        // CSV output file
        File file = new File(getWorkingDirectory(), "day" + fileCount + "_" + dateText + ".csv");
        PrintWriter writer = new PrintWriter(file);
        writerMap.put(dateText, writer);

        // Time calculator
        offsetMap.put(dateText, df.parse(fullDate).getTime());

        fileMap.add(file);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
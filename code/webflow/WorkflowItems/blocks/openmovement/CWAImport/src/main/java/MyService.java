/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.xmlstorage.*;
import com.connexience.server.model.document.*;
import com.connexience.server.workflow.api.APIBroker;
import org.pipeline.core.data.*;
import org.pipeline.core.data.io.*;
import org.pipeline.core.xmlstorage.*;
import org.pipeline.core.data.manipulation.*;

import newcastle.cwa.*;
import java.io.*;

public class MyService extends CloudDataProcessorService {

    /** This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here. */
    public void executionAboutToStart() throws Exception {

    }

    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
	    InputStream inStream = null;
        InputStream cwaStream = null;
	    try {
			XmlDataStore properties = getProperties();

	        // Load the data from the organisation data store
	        DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();

	        // Set the properties
	        int startRow = getEditableProperties().intValue("StartRow", 2);
	        String columnDelimiter = getEditableProperties().stringValue("Delimiter", ",");
	        int endRow = getEditableProperties().intValue("EndRow", 1);
	        boolean limitRows = getEditableProperties().booleanValue("LimitRows", false);
	        boolean subsample = getEditableProperties().booleanValue("Subsample", false);
	        int sampleInterval = getEditableProperties().intValue("SampleInterval", 1);

	        int importChunkSize = getEditableProperties().intValue("ImportChunkSize", 5000);

	        importer.setDataStartRow(startRow);
	        importer.setDataEndRow(endRow);
	        importer.setDelimiterString(",");
	        importer.setDelimiterType(DelimitedTextDataImporter.COMMA_DELIMITED);
	        importer.setErrorsAsMissing(true);
	        importer.setImportColumnNames(false);
	        importer.setLimitRows(limitRows);
	        importer.setSampleInterval(sampleInterval);
	        importer.setSubsample(subsample);
	        importer.setUseEnclosingQuotes(false);
		DocumentRecord record = (DocumentRecord) properties.xmlStorableValue("Source");
                APIBroker apiLink = createApiLink();
                File tmpFile = createTempFile("cwadownload");
                apiLink.downloadToFile(record, tmpFile);
                inStream = new FileInputStream(tmpFile);
                cwaStream = new CwaCsvInputStream(inStream);
                importer.setForceTextImport(false);
	        importer.setChunkSize(importChunkSize);

                // Create a CWA input stream

	        importer.resetWithInputStream(cwaStream);
	        int chunkCount = 0;
	        int rowCount = 0;
	        BestGuessDataTyper guesser;
			Data data;
	        while(!importer.isFinished()){

	        data = importer.importNextChunk();
                data.column(0).setName("Time");
                data.column(1).setName("X");
                data.column(2).setName("Y");
                data.column(3).setName("Z");
	            guesser = new BestGuessDataTyper(data);
	            setOutputDataSet("imported-data", guesser.guess());
	            chunkCount++;
	            rowCount = rowCount + data.getLargestRows();
	        }

	        System.out.println("Imported: " + rowCount + " rows of data in: " + chunkCount + " chunk(s)");

	        } catch (Exception e){
	            e.printStackTrace();
	            throw e;
	        } finally {
                if(cwaStream!=null){
                    try {cwaStream.close();} catch (Exception ex){}
                }

	            if(inStream!=null){
	                try {inStream.close();} catch (Exception ex){}
	            }
	        }
    }

    /** All of the data has been passed through the service. Any clean up code
    * should be placed here */
    public void allDataProcessed() throws Exception {

    }
}


outputfolder = "/output"

dir.create(paste(getwd(), "/output", "/meta", sep=""), recursive=TRUE)
dir.create(paste(getwd(), "/output", "/results", sep=""), recursive=TRUE)

#todo: add daylimit and windowsizes
desiredtz = getProperty("DesiredTimezone");
usetemp = getProperty("UseTemperature");
doangle = getProperty("doangle");
epochsize = getProperty("epochsize");


part1(filename=basename(inputFiles),outputfolder=outputfolder,desiredtz = desiredtz,
      use.temp = usetemp, daylimit = FALSE, windowsizes = c(epochsize,900,3600), doangle = doangle)

print("DesiredTimezone is: ");
print(desiredtz);

#Add the files to the output if they exist
outputFiles = c()
meta_long_filename =paste("/output/meta/meta_", basename(inputFiles), ".RData", sep="");
if(file.exists(paste(getwd(), meta_long_filename, sep=""))){
  outputFiles = c(outputFiles, meta_long_filename)
}

#
#meta_short_filename = paste("/output/meta_short/meta_short_", basename(inputFiles), ".csv", sep="");
#if(file.exists(paste(getwd(), meta_short_filename, sep=""))){
#  outputFiles = c(outputFiles, meta_short_filename)
#}
#
#autocalibFilename = paste("/output/autocalibration/c_", basename(inputFiles), ".csv", sep="");
#if(file.exists(paste(getwd(), autocalibFilename, sep=""))){
#  outputFiles = c(outputFiles, autocalibFilename)
#}

#corruptNameFileName = "/output/fnames_corrupt_gr1.csv";
#if(file.exists(paste(getwd(), corruptNameFileName, sep=""))){
#  outputFiles = c(outputFiles, corruptNameFileName )
#}
#
#tooShortFileName ="/output/fnames_tooshort_gr1.csv";
#if(file.exists(paste(getwd(), tooShortFileName, sep=""))){
#  outputFiles = c(outputFiles, tooShortFileName)
#}





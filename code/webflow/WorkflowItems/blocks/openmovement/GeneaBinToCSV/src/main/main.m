# Entry point for users octave service.
# This file gets called once each time
# a chunk of data is passed through the
# service. It should therefore not modify
# the environment status. It is executed
# as a script and therefore can access and
# set any global variables that are needed.


inputfilename = geneaFile.files(1,:);

outputfilename = 'results.csv';
csvFile.files = [outputfilename];

[xyz,header,timest,vbat] = bin2csv(inputfilename, outputfilename, inf, inf);

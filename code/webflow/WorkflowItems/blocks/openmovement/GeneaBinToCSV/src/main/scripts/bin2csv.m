%% Read accelerometer data from a binary file and write to csv file.  Usage:
%%
%%     [xyz,header,timest,vbat] = bin2csv(fname)
%%
%% By default, bin2csv reads the entire file (see below for loading part of
%% a file)
%%
%% Data is returned in xyz, an array with x-,y- and z-axis data in columns
%% 1 to 3 respectively.  The data is in units of milli-gs.  
%%
%% header is a struct containing header information as found in the file.
%% including such things as study codes, volunteer numbers etc. 
%%
%% timest is the timestamp in Matlab-format, so 
%%             datestr(timest(1))
%% gives the first timestamp in the file in a readable form and
%%             datestr(timest(end))
%% gives the last timestamp in the file.  
%%
%% timest is in the form needed by binplot to plot xyz against date
%% and time:
%%
%%     binplot(timest,xyz)
%%
%% vbat is a structure giving recorded battery voltage during the
%% data logging.  It can also be plotted with binplot:
%%
%%     binplot(vbat.t,vbat.Vbat)
%%
%% To load only part of the datafile, two additional arguments are
%% starttime and endtime.  Usage:
%% 
%%     [xyz,header,timest,vbat] = binread(fname,starttime,endtime)
%%
%% starttime and endtime are Matlab-format dates and times.  They are
%% either:
%%   (a) strings in a format accepted by the Matlab function datestr
%%        (e.g. '12-Mar-2008' or '01-Apr-2008 1:30:00 PM')
%%   (b) Matlab-standard date numbers as returned by built-in functions
%%        datenum, datestr etc.
%%   (c) Zero (for starttime) for the start of file
%%       inf (for endtime) for the end of the file
%%
%% Time and memory space can be saved by omitting some of the output
%% arguments if they are not needed, e.g.: 
%% 
%%     [xyz,header] = binread(fname)
%%
%% SJW added outFile parameter to control output file location



function [xyz,header,timest,vbat] = bin2csv(fname,outFile,starttime,endtime)

    disp("bin2csv")
    %% Parse input arguments
    if nargin < 1,
        fname = 'out.bin';
    end
    %% Start from start of file
    if nargin < 2,
        starttime = 0;
        pagestart = 0;
    end
    %% Continue to end of file
    if nargin < 3,
        endtime = inf;
        pageend = inf;
    end

    %% If return value timest is not needed, skip timestamps,
    %% if needed, put in a placeholder for now
    if nargout < 3,
        timest = [];
    else
        timest = inf;
    end

    pagesize = 2048;

    %% Get time arguments into a standard form: UNIX-style seconds
    %% from 1-Jan-1970, which is the time format used on GENEA
    starttime = parse_time_arg(starttime);
    endtime   = parse_time_arg(endtime);

    %% Open .bin file
    [fid,message] = fopen(fname,'rb');
    if fid < 0,
        error(message)
    end

    filelen = getfilelen(fid);

    %% Read in header
    [header,header_end] = getheader(fid);

    %% Search for start and end timestamps
    [pagestart,pageend] = find_pages(fid,header_end,filelen,starttime,endtime);
    pagestart = pagestart*pagesize;
    pageend   = pageend*pagesize;

    %% Move file pointer to start of data of interest
    fseek(fid,pagestart+header_end,'bof');

    %% Extract accel calibration
    accel = getaccelcalib(header);

    %% Find endpoint
    pageend = min(pageend,filelen);

    %% Read file (with or without timestamps as required)
    if isempty(timest),
        [accidx,xyz,vbat] = doread(fid,pagestart,pageend,accel);
    else
        [accidx,xyz,vbat,timest] = doread(fid,pagestart,pageend,accel);
    end
    fclose(fid);

    %% Convert vbat to volts
    vbat.Vbat = vbat.Vbat *3.3*11/1024;

    %% Get timestamps into useful format (interpolating as necessary)
    if ~isempty(timest),
        timest = reformat_timestamps(timest,accidx);
    end

    if ~isempty(vbat),
        vbat = reformat_vbat_timestamps(vbat);
    end

    fxyz = cast(xyz,'double')*6.0/4096.0;
    clear xyz
    writecsv(fname, outFile, header,timest, fxyz);


function ntime   = parse_time_arg(arg),
    disp("parse_time_arg")

    if ischar(arg),
        %% Assume it's in a standard date/time format
        ntime = datenum(arg);
    else
        %% For now pass argument
        ntime = arg;
    end
    %% Convert to UNIX-style seconds from 1-Jan-1970
    ntime = (24*60^2)*(ntime - datenum(1970,1,1));



function filelen = getfilelen(fid),
    disp("getfilelen")
    %% Get file length
    fseek(fid,0,'eof');
    filelen = ftell(fid);
    fseek(fid,0,'bof');

function [header,header_end,verstr] = getheader(fid),
      disp("getheader")
  %% Read file header and return as a struct.  Fail if version string
    %% is not present.

    header = [];

    %% Get ID string
    idstr = fread(fid,5,'*char').';
    if all(idstr=='GENEA'),
        %% File with header
        verstr = fread(fid,7,'*char').';
        %% TODO: check file format version
        header_items_raw = fread(fid,10*1024,'*char').';
        %% Find field ends (search for newline chars)
        ends = [0,find(header_items_raw==10)];
        header_end = ends(min(find(diff(ends)==2)))+2;
        header_items_raw = header_items_raw(1:header_end);
        ends = ends(ends<header_end);
        for i=1:length(ends)-1,
            %% Extract CR-LF-terminated strings.
            %% ends gives the position of LF's, so start after one LF
            %% up to before the next CR
            hitem = header_items_raw(1+ends(i):ends(i+1)-2);
            %% Split field label from value by the first ':' in the string
            separ = min(find(hitem==':'));
            %% Trap blank line (the end-of-header marker)
            if ~isempty(hitem),
                key = hitem(1:separ-1);
                %% Replace '-' with '_' in key
                key(key=='-') = '_';
                if ~isempty(key),
                    header = setfield(header,key,hitem(separ+1:end));
                end
            end
        end
    else,
        error('File format not recognised')
    end

    %% Seek to position past the end of the header (add 12 to allow for
    %% the version string before the header fields)
    header_end = header_end+12;
    fseek(fid,header_end,'bof');


function accel = getaccelcalib(header),
         disp("getaccelcalib")
 %% Parse accelerometer calibration information from header or
    %% substitute defaults.

    accel = [];
    if isfield(header,'Accel_Gains'),
        accel.gains = int32(sscanf(header.Accel_Gains,'%i'));
    else
        accel.gains = int32([8192;8192;8192]);
    end
    if isfield(header,'Accel_Offsets'),
        accel.offsets = int16(sscanf(header.Accel_Offsets,'%i'));
    else
        accel.offsets = int16([0;0;0]);
    end


function [pagestart,pageend] = find_pages(fid,header_end,filelen,starttime,endtime),
                    disp("find_pages")

    npages = floor(filelen-header_end)/2048;

    %% Find timestamps corresponding to starttime and endtime,
    %somewhere between header_end and filelen.

    pagestart = find_timest(fid,header_end,0,npages,starttime);
    [dummy,pageend] = find_timest(fid,header_end,pagestart,npages,endtime);


function [npage,npagebound] = find_timest(fid,noff,l,u,t);
                                disp("find_timest")

    npages = u;
    %% Check for default parameters
    if t==0,
        npage=0;
        npagebound = 1;
        return
    end
    if isinf(t)
        npage = inf;
        npagebound = inf;
        return
    end

    %% Initialise everything
    pagesize  = 2048;
    nchunks   = floor(pagesize/6);

    while 1,
        %% Choose a page in middle of the range
        n = l + floor((u-l)/2);

        %% Go to n'th page and read it in
        fseek(fid,n*pagesize + noff,'bof');

        page = fread(fid,pagesize);
        page = reshape(page(1:(6*nchunks),:),[6,nchunks]);

        %% Unpack timestamps
        chunktypes = getchunktypes(page);
        current_timest = unpack_timestamps(page,chunktypes);
        %% Debug
        %[l,n,u]
        %datestr(current_timest/(24*60^2)+datenum(1970,1,1))

        if isempty(current_timest),
            %% If no timestamps in this page, go to nearby page and retry
            if n < u-1,
                n=n+1;
            elseif n > l,
                n=n-1;
            else
                %% If n=l=u-1 then this is the only possible page
                pagestart = n;
                pageend   = n+1;
                return;
            end
            continue
        end

        if current_timest(end) > t,
            u = n;
        elseif current_timest(1) >= t,
            u = n+1;
        end
        if current_timest(1) <= t,
            l = n;
        end

        if l >= u-1,
            npage = l;
            npagebound = min(l+1,npages);
            return
        end

    end


function [accidx,xyz,vbat,timest] = doread(fid,pagestart,pageend,accel)
                                    disp("doread")
%% Load bulk of data from the file

    pagesize = 2048;

    %% Preallocate array, big enough for every 6-byte chunk in the file
    %% to carry data samples
    xyz = zeros(341*ceil(((pageend-pagestart)/pagesize)),3,'int16');
    if nargout > 3,
        %% One timestamp per accelerometer triple
        timest = zeros(size(xyz,1),1,'double');
    else
        timest = [];
    end

    %% One battery voltage sample per page
    vbat = [];
    vbat.Vbat = zeros(ceil(((pageend-pagestart)/pagesize)),1,'double');
    vbat.t = zeros(ceil(((pageend-pagestart)/pagesize)),1,'double');
    accidx = 1;
    batidx = 1;

    %% Use NaN as value of unknown timestamp
    preceding_timest = NaN;

    %% Try loading 10 pages at a time
    npages = 10;
    nchunks = floor(pagesize/6);

    %% Progress display
    counter = 0;
    nextmeg = 1024^2;

    done = 0;
    while (~done),
        %% Read next npages pages
        pages = fread(fid,min(npages*pagesize,pageend-pagestart-counter));

        %% Progress meter
        counter = counter + length(pages);

        if (counter >= pageend-pagestart) || (length(pages)<npages*pagesize),
            % Last page
            done = 1;                       % Exit main loop next time
            npages = floor(length(pages)/pagesize);
            pages = pages(1:(npages*pagesize));
        end
        pages = reshape(pages,pagesize,npages);

        %% Sort into 6-byte chunks, dropping trailing padding bytes
        pages = reshape(pages(1:(6*nchunks),:),[6,nchunks*npages]);

        %% Find chunk types (which identify data vs. timestamp etc.)
        chunktypes = getchunktypes(pages);

        %% Unpack 12-bit data
        n_acc_chunks = sum(chunktypes(:)==0);
        xyz(accidx:(accidx+n_acc_chunks-1),1) = ...
            (pages(3,chunktypes==0) + bitshift(bitand(pages(2,chunktypes==0),15),8)).';
        xyz(accidx:(accidx+n_acc_chunks-1),2) = ...
            (bitshift(pages(4,chunktypes==0),4) + bitshift(pages(5,chunktypes==0),-4)).';
        xyz(accidx:(accidx+n_acc_chunks-1),3) = ...
            (bitshift(bitand(pages(5,chunktypes==0),15),8) + pages(6,chunktypes==0)).';

        %% Unpack timestamp data
        current_timest = unpack_timestamps(pages,chunktypes);

        %% Lookup table to map back to preceding timestamp
        timestamp_lookup = cumsum(chunktypes(:)==2);

        %% Prepend timestamp from before the current block
        current_timest = [preceding_timest,current_timest];

        %% Unpack bat voltage data
        [vbat,batidx] = unpack_vbat(vbat,batidx,pages,chunktypes,current_timest,timestamp_lookup);

        if ~isempty(timest),
            %% Put copies of the most recent timestamp in an array.
            %% Each entry in timest corresponds to an accelerometer sample
            %(chunktype 0).  At first it takes the value of
            %preceding_timest from a previous iteration.  Later it takes
            %the value of the timestamps unpacked above.

            %% Now fill in values
            timest(accidx:(accidx+n_acc_chunks-1)) = ...
                current_timest(1+timestamp_lookup(chunktypes(:)==0));

        end

        %% Store most recent timestamp for the next loop iteration
        preceding_timest = current_timest(end);

        %% Cast and calibrate accelerometer data
        xyz = convert_xyz(xyz,accel,accidx,n_acc_chunks);

        %% Move index into main array on
        accidx = accidx + n_acc_chunks;

        %% Display progress
        if counter > nextmeg,
            %disp(sprintf('%iMB',nextmeg/1024^2))
            nextmeg = nextmeg + 1024^2;
        end
    end

function chunktypes = getchunktypes(pages),
                                      disp("getchunktypes")
  chunktypes = bitshift(pages(2,:),-4);

function [vbat,batidx] = unpack_vbat(vbat,batidx,pages,chunktypes,current_timest,timestamp_lookup);
                                          disp("unpack_vbat")
%% Get battery voltages
    vbat.Vbat(batidx:batidx+sum(chunktypes(:)==4)-1) = pages(3,chunktypes==4);
    %% Get timestamps
    vbat.t(batidx:batidx+sum(chunktypes(:)==4)-1) = current_timest(1+timestamp_lookup(chunktypes(:)==4));
    %% Increment index
    batidx = batidx + sum(chunktypes(:)==4);

function timest = unpack_timestamps(pages,chunktypes),
                                             disp("unpack_timestamps")

    timestamp_places = find(chunktypes(:)==2);
    timest = double( ...
        bitshift(uint32(pages(3,timestamp_places)),24) + ...
        bitshift(uint32(pages(4,timestamp_places)),16) + ...
        bitshift(uint32(pages(5,timestamp_places)),8) + ...
        uint32(pages(6,chunktypes(:)==2)));


function [xyz] = convert_xyz(xyz,accel,accidx,n_acc_chunks),
                                                disp("convert_xyz")

    %% Convert 12-bit two's complement data to 16-bit signed int
    %% (no built-in cast to do this, so have to do it by hand)
    xyz(accidx:(accidx+n_acc_chunks-1),:) = ...
        xyz(accidx:(accidx+n_acc_chunks-1),:)...
        - int16(4096 * (xyz(accidx:(accidx+n_acc_chunks-1),:)>2047));
    %% Invert x and z axes
    xyz(accidx:(accidx+n_acc_chunks-1),[1,3]) = -xyz(accidx:(accidx+n_acc_chunks-1),[1,3]);
    %% Apply calibration settings
    xyz(accidx:(accidx+n_acc_chunks-1),:) = xyz(accidx:(accidx+n_acc_chunks-1),:)+...
        repmat(accel.offsets.',n_acc_chunks,1);
    xyz(accidx:(accidx+n_acc_chunks-1),:) = int16(...
        (int32((xyz(accidx:(accidx+n_acc_chunks-1),:))).*...
         repmat(accel.gains.',n_acc_chunks,1))/8192);

    %% Convert to milliG
    xyz(accidx:(accidx+n_acc_chunks-1),:) = int16((int32(xyz(accidx:(accidx+n_acc_chunks-1),:)) ...
                                             *1000)/340);




function timest = reformat_timestamps(timest,endofdata),
                                                  disp("reformat_timestamps")
  %% Interpolate timestamp values and return in Matlab-friendly format

    %% Find positions where timestamp changes
    %% (which includes the position of the earliest timestamp)
    ts_steps  = find(diff(timest)>0)+1;

    rate = diff(timest(ts_steps-1))./diff(ts_steps);
    for stepidx = 1:length(ts_steps)-1,
        rg = ts_steps(stepidx):(ts_steps(stepidx+1)-1);

        timest(rg) = timest(rg) + rate(stepidx)*(rg(:)-rg(1));
    end

    %% Fill in before the first timestamp
    timest(1:ts_steps(1)-1) = timest(ts_steps(1)) - diff(timest(ts_steps(1:2)))./diff(ts_steps(1:2))...
        *((ts_steps(1)-1):-1:1);

    %% Fill in after the last timestamp
    timest(ts_steps(end):endofdata) = timest(ts_steps(end)) ...
        + diff(timest(ts_steps(end-1:end)))./diff(ts_steps(end-1:end)) ...
        .*(0:(endofdata-ts_steps(end)));

    %% Pad end of timestamp array with last value of timestamp
    timest(endofdata:end) = timest(endofdata-1);

    %% Convert time to Matlab-style datenums
    timest = timest/(24*60^2)+datenum(1970,1,1);
    %% Now datestr(timest(i)) converts timest(i) to date string


function vbat = reformat_vbat_timestamps(vbat),
                                                     disp("reformat_vbat_timestamps")

    %% Fill in timestamps before the first
    idx = 1;
    while isnan(vbat.t(idx)),
        idx = idx+1;
        if idx>length(vbat.t),
            %% All NaN, so nothing to do
            return
        end
    end
    first_ts = vbat.t(idx);
    %% Previous timestamp must be first_ts-1, so fill in this
    %% missing value
    vbat.t(1:idx-1) = first_ts-1;


    %% Convert time to Matlab-style datenums
    vbat.t = vbat.t/(24*60^2)+datenum(1970,1,1);
    %% Now datestr(vbat.t(i)) converts vbat.t(i) to date string



%% SJW added outFile parameter
function writecsv(fname, outFile, header,timest, fxyz),

    disp("writecsv")


    %% convert xyz data to floating point g values




    %% create csv output file name
    %outFile = regexprep(fname, '.bin', '.csv') ;
    %fout = outFile;


    % Write out the data

    % Add an index column as we don't have the dates
    disp(length(fxyz(:,1)));
    index = [1:length(fxyz(:,1))]';
    ifxyz = [index fxyz];

    csvwrite(outFile, ifxyz);

    % and open for writing
    %fidout =fopen(outFile, 'w');

    %% Write header according to stanard genea csv file format:
    % t,x,y,z,Study Centre,
    % ,,,,Study Code,
    % ,,,,Investigator ID,
    % ,,,,Exercise Code,
    % ,,,,Volunteer Number,1071
    % ,,,,Body Location,
    % ,,,,Sample Rate,80Hz
    % ,,,,Identifier,
    % ,,,,Config Operator ID,
    % ,,,,Start Time,2010 02 02 16.23.15
    % ,,,,Serial Number,01071
    % ,,,,Battery Voltage,4.0
    % ,,,,Upload Time,2010 02 02 16.25.19
    % ,,,,Upload Operator ID,
    % ,,,,Upload,Finished
    % ,,,,Firmware version,c7931 /trunk 606

    % first 16 lines of file
    % SJW Removed as NE85+ data has different headers
    %fprintf(fidout, 't,x,y,z,Study Centre, %s\r\n', header.Study_Centre);
    %fprintf(fidout, ',,,,Study Code, %s\r\n',header.Study_Code);
    %fprintf(fidout, ',,,,Investigator ID, %s\r\n',header.Investigator_Id);
    %fprintf(fidout, ',,,,Exercise Code, %s\r\n',header.Exercise_Code);
    %fprintf(fidout, ',,,,Volunteer Number, %s\r\n',header.Volunteer_Number);
    %fprintf(fidout, ',,,,Body Location, %s\r\n',header.Body_Location);
    %fprintf(fidout, ',,,,Sample Rate,  %s\r\n',header.Sample_Rate);
    %fprintf(fidout, ',,,,Identifier, %s\r\n',header.Identifier);
    %fprintf(fidout, ',,,,Config Operator ID, %s\r\n',header.Operator_ID_Config);
    %fprintf(fidout, ',,,,Start Time, %s\r\n',header.Trial_Start);
    %fprintf(fidout, ',,,,Serial Number, %s\r\n',header.Serial_Number)
    %fprintf(fidout, ',,,,Battery Voltage, %s\r\n',header.Battery_Voltage);
    %fprintf(fidout, ',,,,Upload Time, %s\r\n',header.Upload_Time);
    %fprintf(fidout, ',,,,Upload Operator ID, %s\r\n',header.Operator_ID_Upload);
    %fprintf(fidout, ',,,,Upload,  %s\r\n',header.Upload);
    %fprintf(fidout, ',,,,Firmware version, %s\r\n',header.Firmware_Version);

    %followed by 100- 16 lines before data
    %for (linecount=1:((100-16)-1))
    %    fprintf(fidout,'\r\n');
    %end

    % and then write triaxial data fxyz in floating point format

    %for observation = 1:length(fxyz(:,1))

        %SJW Removed as formatting the dates doesn't work in Octave 3.6.3
    %    dates = '2013:12:12 12:12:12:121';    % datestr(timest(observation),'yyyy:mm:dd HH:MM:SS.FFF');
    %    x=cast(round(fxyz(observation,1)*1000),'double')/1000.0;
    %    y=cast(round(fxyz(observation,2)*1000),'double')/1000.0;
    %    z=cast(round(fxyz(observation,3)*1000),'double')/1000.0;
    %    fprintf(fidout,'%s0,%6.3f,%6.3f,%6.3f\r\n',dates,x,y,z);
    %end



    %fclose (fidout);

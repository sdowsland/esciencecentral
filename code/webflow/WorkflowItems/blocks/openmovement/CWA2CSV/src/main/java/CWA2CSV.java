
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.*;
import com.connexience.server.workflow.util.ZipUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import org.pipeline.core.data.*;
import newcastle.cwa.*;

public class CWA2CSV implements WorkflowBlock {

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {

    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {

    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        File cwaFile = inputs.getInputFiles("cwa-file").get(0);
        File outFile = new File(env.getWorkingDirectory(), env.getStringProperty("FileName", "cwafile.csv"));
        Boolean light = env.getBooleanProperty("IncludeLight", false);  //0x01
        Boolean temp = env.getBooleanProperty("IncludeTemperature", false);    //0x02

        int options = 0;
        String headerString = "Time,X,Y,Z";

        if(light){
            options += 1;
            headerString += ",L";
        }

        if(temp){
            options += 2;
            headerString += ",T";
        }

        
        CwaCsvInputStream inStream = new CwaCsvInputStream(new FileInputStream(cwaFile), 0, 1, -1, options);
        OutputStream outStream = new FileOutputStream(outFile);

        PrintWriter header = new PrintWriter(outStream);
        header.println(headerString);
        header.flush();
        
        
        ZipUtils.copyInputStream(inStream, outStream);
        outStream.flush();
        header.close();
        outStream.close();
        inStream.close();
        outputs.setOutputFileWithFullPath("csv-file", outFile);
    }
}
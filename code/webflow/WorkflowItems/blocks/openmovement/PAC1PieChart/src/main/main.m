# Entry point for users octave service.
# This file gets called once each time
# a chunk of data is passed through the
# service. It should therefore not modify
# the environment status. It is executed
# as a script and therefore can access and
# set any global variables that are needed.

# This displays the various variables in the
# workspace. It is just here to show how
# data is passed in to Octave and can 
# be removed.

x = summary.numerical(:,1);
sed1 = sum(x==1);
light1 = sum(x==2);
walk1 = sum(x==3);
run1 = sum(x==4);


figure(2, "visible", "off");
piedata = [sed1 ; light1 ; walk1 ; run1];
pie(piedata, {"Sedentary", "Light Activity", "Walking", "Running"});
print "-S640,480" -djpeg piechart.jpg;

chart.files= [ "piechart.jpg"];


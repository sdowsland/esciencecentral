
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.model.document.*;
import com.connexience.server.model.folder.*;
        
import org.pipeline.core.data.*;
import java.io.*;
public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        System.out.println("Do not run this block unless you really know what you're doing!!!!");
        System.exit(1);

        APIBroker api = createApiLink();

        LinkWrapper inputData = (LinkWrapper)getInputData("file-references");

        File dir = getWorkingDirectory();

        LinkPayloadItem item;
        
        for(int i=0;i<inputData.size();i++){

            item = inputData.getItem(i);
            if(item instanceof DocumentRecordLinkItem){
                DocumentRecord ggirFile  = ((DocumentRecordLinkItem)item).getDocument();
                ggirFile = api.getDocument(ggirFile.getId());
                System.out.println("ggirFile.getName() = " + ggirFile.getName());
                Folder metaFolder = api.getFolder(ggirFile.getContainerId());
                System.out.println("metaFolder = " + metaFolder);
                Folder outputFolder =api.getFolder(metaFolder.getContainerId());
                System.out.println("outputFolder = " + outputFolder);
                Folder invocationFolder = api.getFolder(outputFolder.getContainerId());
                System.out.println("invocationFolder = " + invocationFolder);
                Folder monthFolder = api.getFolder(invocationFolder.getContainerId());

                System.out.println("monthFolder.getName() = " + monthFolder.getName());

                ggirFile.setContainerId(monthFolder.getId());
                api.saveDocument(monthFolder, ggirFile);
            }
        }
        
        

    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}
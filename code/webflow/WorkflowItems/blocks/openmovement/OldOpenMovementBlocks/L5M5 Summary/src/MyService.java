/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;

import com.connexience.server.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.manipulation.*;
import org.pipeline.core.data.maths.*;

import java.text.*;
import java.util.*;

public class MyService extends CloudDataProcessorService {
    DateFormat format;
    Calendar refCalendar = null;
    Data[] days = null;
    boolean firstSample = true;

    public void executionAboutToStart() throws Exception {
        System.out.println("Execution about to start");
        String dateFormatText = getEditableProperties().stringValue("DateFormat", "yyyy-MM-dd HH:mm:ss");
        format = new SimpleDateFormat(dateFormatText);
    }

    public void execute() throws Exception {
        System.out.println("Execute");
        int daysToBuffer = getEditableProperties().intValue("SummaryDays", 7);
        Data inputData = getInputDataSet("l5m5-data");
        String colRef = getEditableProperties().stringValue("TimeColumn", "#0");
        ColumnPicker indexPicker = new ColumnPicker(colRef);        
        Column timeCol = indexPicker.getColumnReference(inputData);
        int rows = inputData.getLargestRows();
        String timestamp;
        int day;
        System.out.println("Scanning " + rows);
        for(int i=0;i<rows;i++){
            if(firstSample){
                timestamp = timeCol.getStringValue(i);
                refCalendar = Calendar.getInstance();
                refCalendar.setTime(format.parse(timestamp));
                createBuffers(inputData, daysToBuffer);
                firstSample = false;   
            }   
            
            timestamp = timeCol.getStringValue(i);
            day = calculateDay(timestamp);
            System.out.println("Day: " + day);
            if(day>=0 && day<daysToBuffer){
                days[day].appendRows(inputData.getRowSubset(i, i, true), true);
            }
        }
        
        // Calculate the summaries
        System.out.println("Calculating summary");
        Data summaryData = null;
        for(int i=0;i<daysToBuffer;i++){
            if(i==0){
                summaryData = calculateSummary(i);
            } else {
                summaryData.appendRows(calculateSummary(i), true);
            }   
        }

        setOutputDataSet("l5m5-summary", summaryData);
    }
    
    /** Calculate the day offset for a calendar */
    private int calculateDay(String timeValue) throws Exception {
        Calendar day = Calendar.getInstance();
        day.setTime(format.parse(timeValue));
        int refDayOfYear = refCalendar.get(Calendar.DAY_OF_YEAR);
        int dayOfYear = day.get(Calendar.DAY_OF_YEAR);
        return dayOfYear - refDayOfYear;   
    }
    
    /** Create the days buffers */
    private void createBuffers(Data template, int daysToBuffer) throws Exception {
        days = new Data[daysToBuffer];
        for(int i=0;i<daysToBuffer;i++){
            days[i] = template.getEmptyCopy();
        }   
    }
    
    /** Calculate the summary for a day */
    private Data calculateSummary(int day) throws Exception {
        Data dayData = days[day];
        double minzL5 = Double.MAX_VALUE;
        double maxzL5 = Double.MIN_VALUE;
        double minxyzL5 = Double.MAX_VALUE;
        double maxxyzL5 = Double.MIN_VALUE;
        
        String minzL5Time = "";
        String maxzL5Time = "";
        
        String minxyzL5Time = "";
        String maxxyzL5Time = "";
        
        double zL5Value;
        double xyzL5Value;
        
        int rows = dayData.getLargestRows();
        for(int i=0;i<rows;i++){
            zL5Value = ((NumericalColumn)dayData.column(5)).getDoubleValue(i);
            if(zL5Value<minzL5){
                minzL5 = zL5Value;
                minzL5Time = dayData.column(0).getStringValue(i);   
            }
            
            if(zL5Value>maxzL5){
                maxzL5 = zL5Value;
                maxzL5Time = dayData.column(0).getStringValue(i);   
            }
            
            xyzL5Value = ((NumericalColumn)dayData.column(6)).getDoubleValue(i);
            if(xyzL5Value<minxyzL5){
                minxyzL5 = xyzL5Value;
                minxyzL5Time = dayData.column(0).getStringValue(i);   
            }
            
            if(xyzL5Value>maxxyzL5){
                maxxyzL5 = xyzL5Value;
                maxxyzL5Time = dayData.column(0).getStringValue(i);   
            }
        }
        
        Data summary = new Data();
        summary.addSingleValue("Day", day + 1);
        summary.addSingleValue("MinZL5", minzL5);
        summary.addSingleValue("TimeOfMinZL5", minzL5Time);
        summary.addSingleValue("MinXYZL5", minxyzL5);
        summary.addSingleValue("TimeOfMinXYZL5", minxyzL5Time);
        summary.addSingleValue("MaxZL5", maxzL5);
        summary.addSingleValue("TimeOfMaxZL5", maxzL5Time);
        summary.addSingleValue("MaxXYZL5", maxxyzL5);
        summary.addSingleValue("TimeOfMaxXYZL5", maxxyzL5Time);
        return summary;   
    }
    

    public void allDataProcessed() throws Exception {

    }
}
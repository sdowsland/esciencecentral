function  [startTime,endTime] = writeTimeStampedClassifications(inputfilename,outputfilename,classifications,watchdata,writeUnclassified)
%WRITETIMESTAMPEDCLASSIFICATIONS Writes the GeneActiv watch accel. csv data to a new file with a predicted activity code
%
%   Dominic Searson, Newcastle University. Last updated:  6/8/2014

rows = size(watchdata,1);
delete outputfilename;
fidOut = fopen(outputfilename,'w');
fidIn = fopen(inputfilename,'r');
rowsWritten = 0;

if  ~(exist('OCTAVE_VERSION', 'builtin') ~= 0) %matlab version
    
    for i=1:rows
        
        if mod(i,100000) == 0
            disp([ int2str(i) ' rows processed.']);
        end
        
        predictedClass = double(classifications(i));
        %skip writing unclassified data if flag is set
        if ~writeUnclassified && ~predictedClass
            continue;
        end
        
        tline = fgetl(fidIn);
        timestamp = tline(1:23); %the first 23 chars of each line is the timestamp
        
        if i==1
            startTime = timestamp;
        end
        
        fprintf(fidOut,[timestamp ',']);
        fprintf(fidOut,'%6.4f,%6.4f,%6.4f,%u,%u,%3.1f,%u\n',[watchdata(i,:) predictedClass]);
        rowsWritten = rowsWritten + 1;
        
    end
    
    
    
else %octave version
    disp('Octave writing timestamped data ...');
    l= length(watchdata(1,:));

    for i=1:rows
        
        if mod(i,100000) == 0
            disp([ int2str(i) ' rows processed.']);
        end
        
        predictedClass = classifications(i);
        
        %skip writing unclassified data if flag is set
        if ~writeUnclassified && ~predictedClass
            continue;
        end
        
        tline = fgetl(fidIn);
        timestamp = tline(1:23);
        %fskipl(fidIn,1);
        
        if i==1
            startTime = timestamp;
        end
        
        fprintf(fidOut,[timestamp ',']);
        if l==6 % for full watch data including button presses etc
            fprintf(fidOut,'%6.4f,%6.4f,%6.4f,%u,%u,%3.1f,%u\n',[watchdata(i,:) predictedClass]);
        else %for timestamp and x,y,z accel data only
           fprintf(fidOut,'%6.4f,%6.4f,%6.4f,%u\n',[watchdata(i,:) predictedClass]);
        end

        rowsWritten = rowsWritten + 1;
        
    end
    
end

endTime = timestamp;

fclose(fidOut);
fclose(fidIn);
disp([ int2str(i) ' total rows read.']);
disp([ int2str(rowsWritten) ' total rows written.']);

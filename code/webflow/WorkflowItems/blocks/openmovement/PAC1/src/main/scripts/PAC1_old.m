%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Walking Detection Using FFT & Wavelet
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [sumsed,sumlight,sumwalk,sumrun,segments,data] = PAC1(filename, resultname)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Load and read data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%Read in the rest of the data (should be numeric)
%data=csvread(filename,100,1);
data=csvread(filename,1,1);
[L,n] = size(data);

%plot(data)
% if(L>600000),plot(data(1:600000,:));
% else plot(data);
% end
% if(L>600000), data=data(1:600000,1:3); [L, n] = size(data);end
% disp(sprintf('%i', L,'n=', '%i',n));

fd=sqrt(sum(data.^2,2));
Fs = 80;                      % Sampling frequency
% T = 1/Fs;                     % Sample time
% t = (0:L-1)*T;                % Time vector
% plot(Fs*t(1:L),fd(1:L))
% % plot(Fs*t(1:L),data)
% title('Signal Corrupted with Zero-Mean Random Noise')
% xlabel('time (milliseconds)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start the signal processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
totalep=floor(L/1024);
% totalep=min(totalep,500);
output=zeros(totalep,2);
for it=1:totalep,
    fdit=fd((it-1)*1024+1:it*1024);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Fourier Transform 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     NFFT = pow2(nextpow2(LL(1,1))); % Next power of 2 from length of y
    NFFT=1024;
    y = fft(fdit,NFFT);            % DFT
    f = (0:NFFT-1)*(Fs/NFFT);      % Frequency range
    power = y.*conj(y)/NFFT;       % Power of the DFT
    power(1)=0.0;
    power(2)=0.0;
%     f1 = Fs/2*linspace(0,1,NFFT/2);
    z=power(1:NFFT/2);
    [a,point]=max(z);
    fff=(point-1)*Fs/NFFT;
    totalpower=0.0;
    maxpower=0.0;
    single625=0.0;
    for i=1:NFFT/4,
        if (f(i)>=0.8) && (f(i)<=2.5),
            totalpower=totalpower+ power(i,1);
            if (power(i,1)>maxpower), 
                maxpower=power(i,1);
                single625=f(i);
            end
        end
    end
    if (it==1),
        varia=1.0;
    else
        varia=single625/single625old;
    end
    ratio=a/totalpower;
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%     Wavelet Transform 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    alpha=5;
    beta=6;
    J=8;
    [c,cl]=wavedec(fdit,J,'db10');

    k=J+1;

    index=beta;
    first=sum(cl(1:k-index))+1;
    last=sum(cl(1:k-index+1));
    indd1=first:last;
    ns1=c(indd1);

    index=alpha;
    first=sum(cl(1:k-index))+1; 
    last=sum(cl(1:k-index+1));
    indd2=first:last;
    ns2=c(indd2);

    ns3=c(cl(1)+1:end);

%     SVM=(sum(ns1.^2)+sum(ns2.^2))/sum(fdit.^2);
    SVM1=(sum(ns1.^2)+sum(ns2.^2))/sum(ns3.^2);
    
    [class]=rules4class(a,fff,ratio,varia,maxpower,totalpower,SVM1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%     output the results 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     output(it,1)=it;
%     output(it,2)=fff;   % main frequency with the highese power
%     output(it,3)=a;          % highest single power
%     output(it,4)=totalpower; % sum of power with f between 0.6 and 2.5
%     output(it,5)=single625;
%     output(it,6)=maxpower;
%     output(it,7)=varia;        % output of wavelet
%     output(it,8)=ratio;
%     output(it,9)=SVM1;       % output of wavelet
    output(it,1)=class;
    single625old=single625;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%     Filter the results 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for it=1:totalep,
%     if(output(it,1)==2||output(it,1)==3), wit=it;break;end
%     output(it,2)=0;
% end

seg64=floor(totalep/5);
mod5=mod(totalep,5);
output64=zeros(seg64,1);
sumrun=0.0;
sumwalk=0.0;
sumlight=0.0;
sumsed=0.0;
for seg=1:seg64,
    num1=0;num2=0;num3=0;num4=0;
    for it=1+(seg-1)*5:seg*5,
        if (output(it,1)==1),
            num1=num1+1;
        elseif(output(it,1)==2),
            num2=num2+1;
        elseif(output(it,1)==3),
            num3=num3+1;
        elseif(output(it,1)==4),
            num4=num4+1;
        end
    end
    if (num2>=3),
        output(1+(seg-1)*5:seg*5,2)=1;
        output64(seg,1)=2;
    elseif (num3>=3),
        output(1+(seg-1)*5:seg*5,2)=2;
        output64(seg,1)=3;
    elseif (num4>=3),
        output(1+(seg-1)*5:seg*5,2)=3;
        output64(seg,1)=4;
    else
        output64(seg,1)=1;
    end
end
if(mod5>0),
    output(1+seg64*5:totalep,2)=output(1+seg64*5:totalep,1)-1;
end


segmin5=floor(seg64/5);

for segmin=1:segmin5,
    num=0;
    for it=1+(segmin-1)*5:segmin*5,
        if (output64(it,1)==3),
            num=num+1;
        end
    end
    if  (num==4),
        output64(1+(segmin-1)*5:segmin*5,1)=3;
    end
end

for seg=1:seg64,
    if (output64(seg,1)==1), sumsed=sumsed+1;
    elseif (output64(seg,1)==2), sumlight=sumlight+1;
    elseif (output64(seg,1)==3), sumwalk=sumwalk+1;
    else sumrun=sumrun+1;
    end
end
sumwalk=sumwalk*64/60;
sumrun=sumrun*64/60;
sumlight=sumlight*64/60;
sumsed=sumsed*64/60;
csvwrite(resultname,output64);
segments = output64;



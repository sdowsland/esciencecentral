
function [class] = rules4class(highestpower,frehighestpower,ratio,varia,power625,powersum,svm1)

if (power625<=0.17768),
    if(power625<=0.067328), class=1;
    else
        if(frehighestpower<=0.55),class=2;
        else
            if(power625<=0.13), class=1;
            else
                if (frehighestpower<=1.78), class=1;
                else
                    class=2;
                end
            end
        end
    end
else
    if(highestpower<97.118),
        if(svm1<0.53186),
            if(svm1<=0.44747),
                if(ratio<=0.3134), class=2;
                else
                    if(frehighestpower<=1.7188), class=2;
                    else
                        if(svm1<=0.27554), class=2;
                        else
                            class=3;
                        end
                    end
                end
            else
                if(ratio<=0.29413),
                    if(frehighestpower<=1.1719), class=2;
                    else
                        if(highestpower<=10.244),
                            if (varia<=0.88235), class=2;
                            else
                                if (ratio<=0.26087),
                                    if(powersum<=26.584),
                                        if(varia<=1.0),
                                            if(power625<=4.3076), 
                                                if(frehighestpower<=1.3218),class=3;
                                                else
                                                    if (highestpower<=1.2554),class=2;
                                                    else
                                                        if(frehighestpower<=1.4844),
                                                            if(varia<=0.96875), class=2;
                                                            else
                                                                class=3;
                                                            end
                                                        else
                                                            class=3;
                                                        end
                                                    end
                                                end
                                            else
                                                class=2;
                                            end
                                        else
                                            class=2;
                                        end
                                    else
                                        if (highestpower<=5.8903), class=3;
                                        else
                                            if(frehighestpower<=1.95), class=2;
                                            else
                                                class=3;
                                            end
                                        end
                                    end
                                else
                                    class=3;
                                end
                            end
                        else
                            class=2;
                        end
                    end
                else
                    if(frehighestpower<=0.78125), class=2;
                    else
                        if(frehighestpower<=1.7188),
                            if (varia<=1.0952),
                                if(highestpower<=13.202), class=3;
                                else
                                    class=2;
                                end
                            else
                                class=2;
                            end
                        else
                           class=3;
                        end
                    end
                end
            end
        else
            if(frehighestpower<=1.25),
                if(ratio<=0.32803),
                    if(frehighestpower<=1.1719),
                        if(varia<=0.52174),
                            if(ratio<=0.2591), class=2;
                            else
                                class=3;
                            end
                        else
                            class=2;
                        end
                    else
                        if(powersum<=29.941),
                            if(varia<=1.0714)
                                if(highestpower<=1.8961), class=2;
                                else
                                    class=3;
                                end
                            else
                                class=2;
                            end
                        else
                            class=2;
                        end
                    end
                else
                   if(frehighestpower<=0.78125), class=2;
                   else
                       if(varia<=0.93333),
                           if(varia<=0.53333),
                               if(highestpower<=6.9108), class=2;
                               else
                                   class=3;
                               end
                           else
                               class=2;
                           end
                       else
                           if(varia<=1.03333), class=3;
                           else
                               class=2;
                           end
                       end
                   end
                end
            else
                if(varia<=1.125),
                    if(varia<=0.88462),
                        if(powersum<=24.926),
                            if(highestpower<=2.0305), class=1;
                            else
                                class=3;
                            end
                        else
                            class=2;
                        end
                    else
                        if(powersum<=83.942),
                            if(ratio<=0.30915),
                                if(powersum<=49.166), class=3;
                                else
                                    if(frehighestpower<=1.7969), class=2;
                                    else
                                        class=3;
                                    end
                                end
                            else
                                class=3;
                            end
                        else
                            if(frehighestpower<=1.6406), class=2;
                            else
                                if(highestpower<=27.882), class=2;
                                else
                                    class=3;
                                end
                            end
                        end
                    end
                else
                    if(ratio<=0.30052),
                        if(svm1<=0.73257), class=2;
                        else
                            class=3;
                        end
                    else
                        class=3;
                    end
                end
            end
        end
    else
        if(frehighestpower<=2.2), class=3;
        else
            class=4;
        end
    end
end
                                
                        
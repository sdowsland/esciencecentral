function generateActivityReportHTML(report)
%generateActivityReportHTML Generates a PAC activity HTML file containing JS pie chart.
%
%   Uses the Google Ajax API and Google Visualisation API.
%   
%  last modified: 23/7/2014
%
%  (c) Dominic Searson 2014


if report.stats.pie.pie3D
    chart3D = 'true';
else
    chart3D = 'false';
end

%create file
htmlFileName = [report.filename '.htm'];
fid = fopen(htmlFileName,'w+');

%generate html header info
fprintf(fid,'<!DOCTYPE html>');
fprintf(fid,'\n');
fprintf(fid,'<html lang="en">\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<meta http-equiv="content-type" content="text/html; charset=utf-8" name="description" content="PAC1 activity report" name="author" content="Dominic Searson"/>\n');
fprintf(fid,'<title>PAC1 Activity Report</title>\n');


%Google ajax api
fprintf(fid,'<script type="text/javascript" src="http://www.google.com/jsapi"></script>\n');

%load table & charts vis from Google
fprintf(fid,'<script type="text/javascript">google.load(''visualization'', ''1'', {packages: [''table'']});\n');
fprintf(fid,'google.load(''visualization'', ''1'', {packages: [''corechart'']});');
fprintf(fid,'</script>\n');

%pie viz js
fprintf(fid,'<script type="text/javascript">\n');
%fprintf(fid,'google.load(''visualization'', ''1'', {packages: [''corechart'']});');
fprintf(fid,'google.setOnLoadCallback(drawPie);\n');
fprintf(fid,'function drawPie() {\n');
fprintf(fid,'var data = new google.visualization.arrayToDataTable([\n');
fprintf(fid,'['''', ''''],\n');

pielabels = report.stats.pie.pielabels;
piedata = report.stats.pie.piedata;
piehole = report.stats.pie.piehole;
title = report.stats.pie.pietitle;

for i=1:length(report.stats.pie.pielabels)
    fprintf(fid,['[''' pielabels{i} ''''  ',' num2str(piedata(i)) ']' ]);
    if i<length(pielabels)
        fprintf(fid,',');
    end
    fprintf(fid,'\n');
end

fprintf(fid,']);\n'); %end of pie chart data table

%options
fprintf(fid,['var options = {title: ''' title  ''', pieHole:' num2str(piehole) ',is3D:' chart3D '};\n']);

fprintf(fid,'var chart = new google.visualization.PieChart(document.getElementById(''piechart''));\n');
fprintf(fid,'chart.draw(data, options);\n');

fprintf(fid,'};\n'); %end of drawpie function

fprintf(fid,'</script>\n');


%insert scripts for styles and tables
reportStyles(fid);
summaryTableJS(fid,report);
statsTableJS(fid,report);

%html content
fprintf(fid,'</head>\n');
fprintf(fid,'<body style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; border: 0;">\n');
fprintf(fid,'<div style="text-align: left;margin-bottom: 50px;margin-top: 50px;margin-left: 15px;">');

fprintf(fid,'<h2>PAC1 activity report</h2>\n');
fprintf(fid,['<p class="date">' datestr(now) '</p>\n']);


fprintf(fid,'<h3>Summary data</h3>');
fprintf(fid, '<div id="summarytable"></div>\n');

fprintf(fid,'<h3>Descriptive stats</h3>');
fprintf(fid, '<div id="statstable"></div>\n');

fprintf(fid,'<h3>Activity frame breakdown</h3>');
fprintf(fid, '<div id="piechart" style="width: 900px; height: 500px;"></div>\n');
fprintf(fid,'</div>');
fprintf(fid,'<p style="color:gray;text-align:center;">&#169 BESiDE - The Built Environment for Social Inclusion in the Digital Economy 2014</p>');
fprintf(fid,'</body>\n');
fprintf(fid,'</html>');
fclose(fid);
disp(['Activity report written to ' htmlFileName]);


function summaryTableJS(fid,report)
%javascript for summary config table

fprintf(fid,'<script type="text/javascript">google.load(''visualization'', ''1'', {packages: [''table'']});\n');
fprintf(fid,'google.load(''visualization'', ''1'', {packages: [''corechart'']});\n');
fprintf(fid,'function drawSummaryTable() {\n');
fprintf(fid,'var configdata = new google.visualization.DataTable();\n');
fprintf(fid,'configdata.addColumn(''string'','''');\n');
fprintf(fid,'configdata.addColumn(''string'','''');\n');
fprintf(fid,'configdata.addRows(8);\n');

fprintf(fid,'configdata.setCell(0, 0, ''<p class="table">Data file</p>'');\n');
fprintf(fid,['configdata.setCell(0, 1, ''<p class="table">' report.summary.csvfile  '</p>'');\n']);

fprintf(fid,'configdata.setCell(1, 0, ''<p class="table">Sample frequency (Hz)</p>'');\n');
fprintf(fid,['configdata.setCell(1, 1, ''<p class="table">' num2str(report.summary.sampleFreq) '</p>'');\n']);

fprintf(fid,'configdata.setCell(2, 0, ''<p class="table">Hours of data</p>'');\n');
fprintf(fid,['configdata.setCell(2, 1, ''<p class="table">' num2str(report.summary.hoursOfData) '</p>'');\n']);

fprintf(fid,'configdata.setCell(3, 0, ''<p class="table">Start time</p>'');\n');
fprintf(fid,['configdata.setCell(3, 1, ''<p class="table">' report.summary.startTime '</p>'');\n']);


fprintf(fid,'configdata.setCell(4, 0, ''<p class="table">End time</p>'');\n');
fprintf(fid,['configdata.setCell(4, 1, ''<p class="table">' report.summary.endTime '</p>'');\n']);

fprintf(fid,'configdata.setCell(5, 0, ''<p class="table">Rows of data</p>'');\n');
fprintf(fid,['configdata.setCell(5, 1, ''<p class="table">' num2str(report.summary.classifiedRows) '</p>'');\n']);

fprintf(fid,'configdata.setCell(6, 0, ''<p class="table">Activity frame length (sec)</p>'');\n');
fprintf(fid,['configdata.setCell(6, 1, ''<p class="table">' num2str(report.summary.activityFrameLength) '</p>'');\n']);

fprintf(fid,'configdata.setCell(7, 0, ''<p class="table">Number of activity frames</p>'');\n');
fprintf(fid,['configdata.setCell(7, 1, ''<p class="table">' num2str(report.summary.numActivityFrames) '</p>'');\n']);


fprintf(fid,'summaryviz = new google.visualization.Table(document.getElementById(''summarytable''));\n');
fprintf(fid,'summaryviz.draw(configdata,  {width: 800, allowHtml: true});}\n');
fprintf(fid,'google.setOnLoadCallback(drawSummaryTable);\n');
fprintf(fid,' </script>\n');

function reportStyles(fid)
% Apply CSS formatting to page elements
fprintf(fid,'\n<style>\n');
fprintf(fid,'h1, h2, h3 {\n');
fprintf(fid,'color: #003366; ');
fprintf(fid,'margin-top: 30px; ');
fprintf(fid,'\n}\n');
fprintf(fid,'p.warn {\n');
fprintf(fid,'color: #993300; ');
fprintf(fid,'\n}\n');
fprintf(fid,'p.table {\n');
fprintf(fid,'font-size: 120%%; ');
fprintf(fid,'\n}\n');
fprintf(fid,'p.muted {\n');
fprintf(fid,'font-size: 120%%; color: gray;');
fprintf(fid,'\n}\n');
fprintf(fid,'.google-visualization-table-table {\n');
fprintf(fid,'font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;');
fprintf(fid,'\n}\n');
fprintf(fid,'</style>');

function statsTableJS(fid,report)
%javascript for activity stats table

fprintf(fid,'<script type="text/javascript">google.load(''visualization'', ''1'', {packages: [''table'']});\n');
fprintf(fid,'google.load(''visualization'', ''1'', {packages: [''corechart'']});\n');
fprintf(fid,'function drawStatsTable() {\n');
fprintf(fid,'var configdata = new google.visualization.DataTable();\n');
fprintf(fid,'configdata.addColumn(''string'','''');\n');
fprintf(fid,'configdata.addColumn(''string'','''');\n');
fprintf(fid,'configdata.addRows(4);\n');

fprintf(fid,'configdata.setCell(0, 0, ''<p class="table">Mean activity class</p>'');\n');
fprintf(fid,['configdata.setCell(0, 1, ''<p class="table">' num2str(report.stats.meanActivityClass)  '</p>'');\n']);

fprintf(fid,'configdata.setCell(1, 0, ''<p class="table">Activity class SD</p>'');\n');
fprintf(fid,['configdata.setCell(1, 1, ''<p class="table">' num2str(report.stats.stdevActivityClass) '</p>'');\n']);

fprintf(fid,'configdata.setCell(2, 0, ''<p class="table">Median activity class</p>'');\n');
fprintf(fid,['configdata.setCell(2, 1, ''<p class="table">' num2str(report.stats.medianActivityClass) '</p>'');\n']);

fprintf(fid,'configdata.setCell(3, 0, ''<p class="table">Max activity class</p>'');\n');
fprintf(fid,['configdata.setCell(3, 1, ''<p class="table">' num2str(report.stats.maxActivityClass) '</p>'');\n']);


fprintf(fid,'statsviz = new google.visualization.Table(document.getElementById(''statstable''));\n');
fprintf(fid,'statsviz.draw(configdata,  {width: 800, allowHtml: true});}\n');
fprintf(fid,'google.setOnLoadCallback(drawStatsTable);\n');
fprintf(fid,' </script>\n');
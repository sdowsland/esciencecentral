%MAIN Script that runs the PAC1 activity classifier on GENEActiv input data.
%
%   Rewritten by Dominic Searson to fix loading data bugs, to use 100Hz
%   sampling frequency in the spectral decomposition, to use improved JS pie
%   charts and to export time stamped frame aligned predicted activity classes
%   in a way that works in OCTAVE and MATLAB and E-Sc. The actual ruleset
%   governing the predicted classifications has not been changed and seems
%   suspect to me.
%
%   Each time this is run a file called octaveReport.txt is generated which
%   summarises the processing steps.
%
%   Last modified: 6-8-2014, Dominic Searson, Newcastle University.

warning off; more off; tic;

delete octaveReport.txt;
diary('octaveReport.txt');
disp('PAC1 classifier report (ver: 6-8-2014)');
disp(datestr(now));

%determines if unclassified data is to be written to the output csv
writeUnclassified = true;

%attempt esc environment but fall back to local CSV
try
    inputfilename = x.files(1,:);
    
if isfield(properties,'writeUnclassifiedData')
    writeUnclassified = properties.writeUnclassifiedData;
end

catch
    inputfilename = 'converted.csv';
    %inputfilename = '01bc01_left_wrist_016547_2014-05-21_12-05-03.bin.csv';
    %inputfilename = '01bc01_left_wrist_2014_05_21_12_05_03_noheader.csv';
    %inputfilename = '01jw02_left wrist_018402_2014-05-26_12-07-50.csv';
end

%set output filename (for time stamped data with corresponding predicted
%activity class)
outputfilename = 'results.csv';

%set watch sampling frequency
sampleFreq = 100; %Hz

disp([inputfilename ' contains ' int2str(filelineCount(inputfilename)) ' rows.']);
disp(['Loading accelerometry CSV data from: ' inputfilename ' ... ' ]);

%load csv accel. data
loadedData = single(csvread(inputfilename,0,1));
rows = size(loadedData,1);

if isnan(rows) || rows <= 0
    diary off;
    error(['Cannot read CSV data from: ' inputfilename '. Check that this file does not contain a header.']);
end

%remove the bizarrely added column of zeros (by csvread in MATLAB but not Octave)
if ~(exist('OCTAVE_VERSION', 'builtin') ~= 0)
    loadedData(:,7) = [];
end

%stats
numEpochs = floor(rows /1024);
numFrames = floor(rows/(5*1024));
unclassifiedRows = mod(rows,(5*1024));
classifiedRows = rows - unclassifiedRows;
disp(['Number of rows of GENEActiv data loaded: ' int2str(rows)]);
disp(['Number of complete epochs (1024 samples) in loaded data: ' int2str(numEpochs) ]);
disp('Activity frame length: 5 epochs (5120 samples)');
disp(['Sampling frequency of ' int2str(sampleFreq) ' Hz']);
disp(['Number of activity frames in data: ' int2str(numFrames) ]);
disp(['Number of out of frame rows (these will be classified as 0) : ' int2str(unclassifiedRows) ]);
if ~writeUnclassified
    disp('Out of frame rows will not be written to output file');
else
    disp('Out of frame rows will be written to output file');
end

%perform classifications
disp('Performing spectral analysis and classification ...');
[sed1, light1, walk1, run1, classifications] = PAC1(loadedData,sampleFreq);
disp(['Classifications obtained : ' int2str(length(classifications))]);
disp('Classifications complete.');





%expand classifications vector to match rows of loaded data.
%Each activity class (from PAC1) appears to be assigned to 5 epochs (5*1024
%= 5120 rows).
%Call this an 'Activity Frame'.
%So, these frames won't line up with either (a) the actual number of rows in
%the loaded data or (b) the trimmed value rtrim.
%So, for any unclassified rows, just leave the classification code as zero.
disp('Assigning classifications to rows ...');
classificationsRows = zeros(rows,1);
offset = 1;
activityFrame = 5 *1024; %approx a minute
for i=1:length(classifications)
    classificationsRows(offset:(offset + activityFrame -1 ) ) = classifications(i);
    offset = offset + activityFrame;
end


%only plot summary data if less than 15,000,000 rows (otherwise it just pretty
%much kills Octave)
noPlot= false;
if rows <= 15000000
    
    if (exist('OCTAVE_VERSION', 'builtin') ~= 0)
        figure('visible','off');
    end
    
    plot(loadedData(1:classifiedRows,1:3));hold on;xlabel('Row number');grid;
    title(['PAC1. Freq. (Hz): ' num2str(sampleFreq) ...
        '. Rows: ' int2str(classifiedRows) '. Epochs: ' int2str(numEpochs) '. Frames: ' int2str(numFrames)]);
    ylabel('XYZ acceleration (g) & activity class');
    stairs(classificationsRows(1:classifiedRows), 'k-','LineWidth',2);
    
    if (exist('OCTAVE_VERSION', 'builtin') ~= 0)
        %print -djpeg summary.jpg  -S640,480;
        print -djpeg summary.jpg  -landscape;
        close all;
    end
    
else
    disp('Skipping summary plot because data size > 15000000 rows.');
    noPlot = true;
end


%write output file
disp('Writing predicted activity class tagged accelerometry data to output csv file ...');
[startTime,endTime] = writeTimeStampedClassifications(inputfilename, outputfilename, classificationsRows, loadedData, writeUnclassified);
disp(['Modified accelerometry CSV file written to: ' outputfilename]);
disp(['File start row: ' startTime]);
disp(['File end row: ' endTime]);



%compute some reporting & descriptive 'stats' based on activity frame classifications
disp('Calculating stats ...');
%  if ~writeUnclassified 
%    classificationsRows(classificationsRows == 0) = [];
%  end

report.summary.numActivityFrames = length(classifications);
report.stats.numClass1Frames = sum(classifications == 1);
report.stats.numClass2Frames = sum(classifications == 2);
report.stats.numClass3Frames = sum(classifications == 3);
report.stats.numClass4Frames = sum(classifications == 4);
report.stats.numUnclassifiedFrames = numel(find(classifications == 0));
report.summary.hoursOfData = (classifiedRows/(sampleFreq *60 *60));
report.summary.startTime = startTime;
report.summary.endTime = endTime;
report.filename = 'activity';
report.summary.csvfile = inputfilename;
report.summary.sampleFreq = sampleFreq;
report.summary.classifiedRows = classifiedRows;
report.summary.activityFrameLength = (5 *1024) * (1/sampleFreq);

report.stats.meanActivityClass = mean(classifications);
report.stats.maxActivityClass = max(classifications);
report.stats.medianActivityClass = median(classifications);
report.stats.modeActivityClass = mode(classifications);
report.stats.stdevActivityClass = std(classifications);

%report.stats.pie.piedata = [sed1 ; light1 ; walk1 ; run1];
report.stats.pie.piedata = [report.stats.numClass1Frames; report.stats.numClass2Frames; report.stats.numClass3Frames; report.stats.numClass4Frames ];
report.stats.pie.pielabels = {'Sedentary','Light activity','Walking','Running'};
report.stats.pie.pietitle = 'Activity class';
report.stats.pie.piecategory = 'activity';
report.stats.pie.pie3D = false;
report.stats.pie.piehole = 0.5;

%generate HTML activity report
generateActivityReportHTML(report);

disp(['Hours of data processed: ' num2str(report.summary.hoursOfData)]); 
disp('Processing report written to: octaveReport.txt');
disp(['Time taken: ' num2str(toc) ' seconds']);
diary off;

%esc environment
if (exist('OCTAVE_VERSION', 'builtin') ~= 0)
    if noPlot
        y.files = ['results.csv' ; 'activity.htm';'octaveReport.txt']; % ok for octave but not valid matlab syntax -dps
    else
        y.files= ['results.csv' ; 'summary.jpg' ; 'activity.htm';'octaveReport.txt'];
    end
end


function generatePieChartHTML(pielabels, piedata, title, filename,chart3D,piehole)
%GENERATEPIECHARTHTML Generates an HTML file containing pie chart.
%
%   GENERATEPIECHARTHTML(PIELABELS,PIEDATA,TITLE,FILENAME,3DCHART, PIEHOLE)
%   generates an HTML file containing a pie chart.
%   PIELABELS is a linear cell array containing the names of each pie slice.
%   PIEDATA is a vector (the same length as PIELABELS) containing the
%   quantities to be represented in the pie chart and FILENAME is the name
%   of the HTML file to be created. CHART3D is a boolean to use a '3d' pie
%   chart and PIEHOLE is a number between 0 and 1 defining the size of the
%   hole in the middle of the chart (default = 0).
%
%   Example:
%   generatePieChartHTML({'Dog','Cat','Budgie','Kodiak bear'},[300 400 200 30],'Favourite pet','pets')
%   creates a piechart in the file pets.htm.
%
%   Uses the Google Ajax API and Google Visualisation API.
%
% (c) Dominic Searson 2014

if nargin <3 || isempty(title)
    title = 'Pie chart';
end

if nargin < 4 || isempty(filename)
    filename = 'piechart';
end

if nargin < 5 || isempty(chart3D)
    chart3D = false;
end

if nargin< 6 || isempty(piehole)
    piehole = 0;
end

if piehole < 0 || piehole > 1
    error('PIEHOLE must be a scalar between 0 and 1');
end

if length(piedata) ~= length(pielabels)
    error('The cell array containing pie chart labels must be the same length as the vector containing the chart data.');
end

if chart3D
    chart3D = 'true';
else
    chart3D = 'false';
end

%create file
htmlFileName = [filename '.htm'];
fid = fopen(htmlFileName,'w+');

%generate html header info
fprintf(fid,'<!DOCTYPE html>');
fprintf(fid,'\n');
fprintf(fid,'<html lang="en">\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<meta http-equiv="content-type" content="text/html; charset=utf-8" name="description" content="pie chart vis" name="author" content="Dominic Searson"/>\n');
fprintf(fid,'<title>Pie chart</title>\n');

%Google ajax api
fprintf(fid,'<script type="text/javascript" src="http://www.google.com/jsapi"></script>\n');

%viz js
fprintf(fid,'<script type="text/javascript">\n');
fprintf(fid,'google.load(''visualization'', ''1'', {packages: [''corechart'']});');
fprintf(fid,'google.setOnLoadCallback(drawPie);\n');
fprintf(fid,'function drawPie() {\n');
fprintf(fid,'var data = new google.visualization.arrayToDataTable([\n');
fprintf(fid,'['''', ''''],\n');

for i=1:length(pielabels)
    fprintf(fid,['[''' pielabels{i} ''''  ',' num2str(piedata(i)) ']' ]);
    if i<length(pielabels)
        fprintf(fid,',');
    end
    fprintf(fid,'\n');
end

fprintf(fid,']);\n'); %end of data table

%options
fprintf(fid,['var options = {title: ''' title  ''', pieHole:' num2str(piehole) ',is3D:' chart3D '};\n']);

fprintf(fid,'var chart = new google.visualization.PieChart(document.getElementById(''piechart''));\n');
fprintf(fid,'chart.draw(data, options);\n');

fprintf(fid,'};\n'); %end of drawpie function

fprintf(fid,'</script>\n');

%html content
fprintf(fid,'</head>\n');
fprintf(fid,'<body>\n');
fprintf(fid, '<div id="piechart" style="width: 900px; height: 500px;"></div>\n');
fprintf(fid,'</body>\n');
fprintf(fid,'</html>');
fclose(fid);
disp(['Pie chart written to ' htmlFileName]);

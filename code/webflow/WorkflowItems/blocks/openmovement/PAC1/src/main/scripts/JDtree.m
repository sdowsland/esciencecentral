
function [class] = JDtree(dominant,dpower,sumpower,f125,p125,f612,p612,variation,intense,svm,svma)

if (p125 <= 0.14371),
   if (p612 <= 0.024048), 
       class=1;
   else
      if (dpower <= 0.17124),
        if (p612 <= 0.024512), 
            class=2;
        else
            class=1;
        end
      else
          class=2;
      end
    end
else 
   if (dpower <= 97.118),
      if (svma <= 0.50551),
         if (intense <= 0.29172),
            if (svma <= 0.4613),
                class=2;
            else 
               if (dominant <= 1.25),
                   class=2;
               else
                  if (f125 <= 1.4063),
                     if (variation <= 0.91667),
                         class=2;
                     else
                         class=3;
                     end
                  else
                     if (f612 <= 0.9375),
                         class=2;
                     else
                        if (variation <= 1.04),
                           if (dominant <= 2.0313),
                              if (dominant <= 1.6406),
                                  class=3;
                              else
                                  class=2;
                              end
                           else
                               class=3;
                           end
                        else
                            class=2;
                        end
                     end
                  end
               end
            end
         else
            if (dominant <= 0.85938),
                class=2;
            else
               if (variation <= 1.1538),
                  if (variation <= 0.86364),
                      class=2;
                  else
                     if (svma <= 0.27301),
                        if (variation <= 1.0833),
                            class=2;
                        else
                            class=3;
                        end
                     else
                        if (f125 <= 1.7188),
                           if (dominant <= 1.5625),
                              if (p612 <= 1.7332),
                                  class=3;
                              else
                                 if (dominant <= 1.25),
                                     class=2;
                                 else
                                     class=3;
                                 end
                              end
                           else
                               class=2;
                           end
                        else
                            class=3;
                        end
                     end
                  end
               else
                   class=2;
               end
            end
         end
      else
         if (variation <= 0.88),
            if (p612 <= 2.1213),
               if intense <= 0.20287
                  if (f125 <= 1.6406),
                      class=2;
                  else
                      class=1;
                  end 
               else
                  if (dpower <= 13.152),
                      class=3;
                  else
                      class=2;
                  end
               end
            else
                class=2;
            end
         else
            if (variation <= 1.0833),
               if (dominant <= 1.1719),
                  if (f125 <= 1.6406),
                     if (f612 <= 1.0938),
                         class=2;
                     else
                        if (variation <= 1),
                            class=2;
                        else
                            class=3;
                        end
                     end
                  else
                     if (intense <= 0.25897),
                        if (variation <= 0.9375),
                            class=3;
                        else
                            class=2;
                        end
                     else
                        if (variation <= 0.95652),
                           if (dpower <= 12.883),
                               class=2;
                           else
                               class=3;
                           end
                        else
                            class=3;
                        end
                     end
                  end
               else
                  if (intense <= 0.24403),
                     if (sumpower <= 50.308),
                        if (f612 <= 1.0156),
                           if (intense <= 0.19465),
                               class=2;
                           else
                              if (sumpower <= 25.21),
                                 if (variation <= 1.0435)
                                     class=3;
                                 else
                                    if (dominant <= 1.4844)
                                        class=3;
                                    else
                                        class=2;
                                    end 
                                 end
                              else
                                  class=2;
                              end
                           end
                        else
                            class=3;
                        end 
                     else
                         class=2;
                     end
                  else
                     if (svm <= 0.1151),
                         class=3;
                     else
                        if (dominant <= 1.6406),
                           if (p612 <= 3.1416),
                               class=3;
                           else
                               class=2;
                           end
                        else
                            class=3;
                        end 
                     end
                  end
               end
            else
               if (p612 <= 0.92792),
                  if (f125 <= 1.7969),
                      class=3;
                  else
                      class=2;
                  end
               else
                  if (dominant <= 2.3438),
                     if (f125 <= 1.4063),
                        if (p612 <= 2.5611),
                            class=3;
                        else
                            class=2;
                        end
                     else
                         class=2;
                     end
                  else
                      class=3;
                  end 
               end
            end
         end
      end
   else
      if (dominant <= 2.2656),
         if (dominant <= 2.1875),
             class=3;
         else
             class=4;
         end
      else
          class=4;
      end
   end
end

                                
                        
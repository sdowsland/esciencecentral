function lineCount = filelineCount (fileName)
%FILELINECOUNT  Counts lines in file or returns -1 if it could not be determined.
%
% Dominic Searson 27/6/2014.
%
% Last updated: 15/7/2014 dps

lineCount = -1;
try
    if (isunix)
        [a, result] = system( ['wc -l ', fileName] );
        resultTrim = strtrim(result);
        resultTrim = resultTrim(1:strfind(resultTrim,' ')-1);
        lineCount = str2double(resultTrim);
        
    elseif (ispc) %TODO : needs testing on win platforms
        
       % lineCount  = str2double( perl('countlines.pl', fileName) );
        error('Unsupported OS');
    end
catch
    lineCount = -1;
    disp(['Oops. Could not determine line count because: [' lasterr]);
end

# Entry point for users octave service.
# This file gets called once each time
# a chunk of data is passed through the
# service. It should therefore not modify
# the environment status. It is executed
# as a script and therefore can access and
# set any global variables that are needed.

inputfilename = x.files(1,:);
outputfilename = 'results.csv';


[sed1, light1, walk1, run1, summary, rawdata] = PAC1(inputfilename, outputfilename);


# To plot, make sure there is a gnuplot-bin
# dependency and use:
# plot(data);
# print -dpng graph.png;

# Plot the results data
[r c] = size(rawdata);
rtrim = floor(r / 1024) * 1024;

[sr sc] = size(summary);
xdelta = rtrim / sr;
sxplot = 1:xdelta:rtrim;


figure(1, "visible", "off");
hold on;

xplot = 1:rtrim;
plot(xplot', rawdata(1:rtrim, 3));
stairs(sxplot', summary, 'r','LineWidth',2);
print  -djpeg summary.jpg -S640,480;

figure(2, "visible", "off");
piedata = [sed1 ; light1 ; walk1 ; run1];
pie(piedata, {"Sedentary", "Light Activity", "Walking", "Running"});
print "-S640,480" -djpeg piechart.jpg;

y.files= ["results.csv" ; "summary.jpg" ; "piechart.jpg"];

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.cloud;
import org.apache.log4j.*;
/**
 * This thread attaches a workflow engine to the JMS server and reconnects if there
 * are any errors.
 * @author hugo
 */
public class JMSAttachThread implements Runnable
{
    private static Logger logger = Logger.getLogger(CloudWorkflowEngine.class);

    /** Default interval between attempts to reattach to JMS = 1h */
    private static final int DefaultInterval = 3_600_000;

    /** Default interval between attempts to reattach to JMS when error has occured = 10 sec. */
    private static final int DefaultErrorInterval = 10_000;

    private final String hostname;
    private final int port;
    private final String user;
    private final String password;
    private final String queueName;
    private final Integer bufferSize;
    private final WorkflowJMSListener target;

    /** Worker thread */
    private Thread worker = null;
    private volatile int interval;
    private volatile boolean runFlag = false;
    private volatile boolean autoAttach = true;


    public JMSAttachThread(WorkflowJMSListener target, String hostname, int port, String user, String password, String queueName, Integer bufferSize)
    {
        this.hostname = hostname;
        this.user = user;
        this.password = password;
        this.port = port;
        this.queueName = queueName;
        this.bufferSize = bufferSize;
        this.interval = DefaultInterval;
        this.target = target;
    }


    /** Set the retry interval */
    public void setInterval(int interval)
    {
        synchronized (this) {
            this.interval = interval;
            if (worker != null) {
                worker.interrupt();
            }
        }
    }


    /**
     * Initiates a JMS attach thread that makes sure the JMS connection is in desired state:
     * attached or detached.
     */
    public void start()
    {
        synchronized (this) {
            if (worker == null) {
                runFlag = true;
                interval = DefaultInterval;
                worker = new Thread(this, "JMSAttacherWorker");
                worker.start();
                logger.debug("JMSAttacherThread worker started: " + queueName);
            } else {
                // Possibly, interrupts the sleep call in the run method.
                worker.interrupt();
            }
        }
    }


    /**
     * Terminates a JMS attach thread.
     */
    public void stop()
    {
        synchronized (this) {
            if (worker != null) {
                logger.debug("JMSAttacherThread worker stopping: " + queueName);
                runFlag = false;
                worker.interrupt();
                worker = null;
            }
        }
    }


    /**
     * Detaches from the JMS queue without terminating the attach thread. It is supposed
     * to be used to temporarily detach from JMS.
     */
    public void detach()
    {
        // First, make sure that the worker is not trying to attachJms
        autoAttach = false;
        worker.interrupt();

        // Then detach
        if (target.isJmsAttached()) {
            target.detachJms();
        }
    }


    /**
     * Makes sure that the JMS queue is connected (initiating the thread if needed).
     */
    public void attach()
    {
        autoAttach = true;
        start();
    }


    public void run()
    {
        logger.debug("JMSAttachThread worker started");
        while (runFlag) {
            logger.debug("JMSAttachThread worker loop");
            if (!target.isJmsAttached()) {
                logger.debug("JMSAttachThread JMS is not attached.");
                try {
                    if (autoAttach) {
                        logger.debug("JMSAttachThread attempt to attach queue: " + queueName);
                        target.attachJms(hostname, port, user, password, queueName, bufferSize);
                    }
                    interval = DefaultInterval; // Check every hour
                } catch (Exception e) {
                    logger.error("Error attaching JMS: " + e.getMessage());
                    interval = DefaultErrorInterval;   // check if disconnected every 10 sec.
                }
            }

            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                // Ignore exception as it may be to escape from sleep to quickly reattempt attachJms
            }
        }

        if (target.isJmsAttached()) {
            target.detachJms();
        }

        logger.debug("JMSAttachThread worker finished: " + queueName);
    }
}

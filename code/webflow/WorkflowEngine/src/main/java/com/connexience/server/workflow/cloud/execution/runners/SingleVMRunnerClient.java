/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.cloud.execution.runners;

import com.connexience.server.util.RegistryUtil;
import com.connexience.server.workflow.cloud.execution.runners.server.SingleVMServer;
import com.connexience.server.workflow.cloud.execution.runners.server.SingleVMServiceInstance;
import com.connexience.server.workflow.service.DataProcessorCallMessage;
import com.connexience.server.workflow.service.DataProcessorException;


/**
 * This class can connect to a remote service runner that executes all of the 
 * blocks from a workflow in a single VM.
 * @author hugo
 */
public class SingleVMRunnerClient extends AbstractRunner {
    /** Link to the remote VM */
    private SingleVMServiceInstance instance;
    
    public SingleVMRunnerClient(DataProcessorCallMessage message) throws DataProcessorException {
        super(message);
    }

    private SingleVMServer lookupServer() throws Exception {
        try {
            SingleVMServer server = (SingleVMServer)RegistryUtil.lookup("localhost", "INVOCATION_SERVER_" + message.getInvocationId());
            return server;
        } catch (Exception e){
            throw new Exception("Cannot connect to invocation server: " + e.getMessage());
        }
    }
    
    @Override
    public void start() throws Exception {
        SingleVMServer server = lookupServer();
        if(server!=null){
            instance = server.createServiceInstance(message);
            instance.setup(baseDir, invocationDirectory, classpathList);
        } else {
            throw new Exception("No connection to server process");
        }
    }

    @Override
    public int waitFor() throws Exception {

        if(instance!=null){
            try {
                return instance.runService();
            } catch (Throwable t){
                return 0;
            }
        } else {
            throw new Exception("No server available");
        }
    }

    @Override
    public void startDumpers() throws Exception {
        try {
            if(instance!=null){
                instance.startDumpers();
                stdErrFile = instance.getStdErrFile();
                stdOutFile = instance.getStdOutFile();
            }
        } catch (Throwable t){
            throw new Exception("Error starting dumpers: " + t.getMessage());
        }
    }

    @Override
    public void stopDumpers() throws Exception {
        try {
            if(instance!=null){
                instance.stopDumpers();
            }
        } catch (Throwable t){
            
        }
    }

    @Override
    public void kill() {
        try {
            if(instance!=null){
                try {
                    instance.kill();
                } catch (Exception e){

                }
            }
        } catch (Throwable t){
            
        }
    }

    @Override
    public void addExternalPID(long pid) {
        try {
            instance.addExternalPID(pid);
        } catch (Throwable t){
        }
    }

    @Override
    public long getMaximumMemorySize() throws Exception{
        try {
            SingleVMServer server = lookupServer();
            if(server!=null){
                return server.getMaximumMemorySize() + instance.getMaximumMemorySize();
            } else {
                throw new Exception("Cannot locate server VM");
            }
        } catch (Throwable t){
            return 0;
        }
    }

    @Override
    public long getMaximumResidentMemory() throws Exception {
        try {
            SingleVMServer server = lookupServer();
            if(server!=null){
                return server.getMaximumResidentMemory()+ instance.getMaximumResidentMemory();
            } else {
                throw new Exception("Cannot locate server VM");
            }
        } catch (Throwable t){
            return 0;
        }
    }

    @Override
    public int getProcessCount() throws Exception {
        try {
            return instance.getProcessCount();
        } catch (Throwable t){
            return 0;
        }
    }

    @Override
    public void stopMonitoring() throws Exception {
        try {
            instance.stopMonitoring();
        } catch (Throwable t){
        }
    }
}

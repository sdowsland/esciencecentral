/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.engine.datatypes;

import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.StorableTransferData;
import org.pipeline.core.drawing.TransferData;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class ControlDependency implements TransferData, StorableTransferData {

    public ControlDependency()
    { }

    @Override
    public Object getPayload() {
        return null;
    }

    @Override
    public TransferData getCopy() {
        return this;
    }

    @Override
    public void saveToOutputStream(OutputStream stream) throws DrawingException
    {
        try {
            stream.close();
        } catch (IOException x) {
            throw new DrawingException("Internal error when storing control dependency data: " + x);
        }
    }

    @Override
    public void loadFromInputStream(InputStream stream) throws DrawingException
    {
        try {
            stream.close();
        } catch (IOException x) {
            throw new DrawingException("Internal error when loading control dependency data: " + x);
        }
    }
}

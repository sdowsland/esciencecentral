package com.connexience.server.workflow.cloud.services;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Jacek on 17/06/2015.
 */
public class ShellDataProcessorServiceTest {

    @Test
    public void test_wrapInQuotes() throws Exception {
        assertEquals("a", "'ala ma kota'", ShellDataProcessorService._wrapInQuotes("ala ma kota"));
        assertEquals("b", "'ala '\\''ma kota'", ShellDataProcessorService._wrapInQuotes("ala 'ma kota"));
        assertEquals("c", "'ala '\\'\\''ma kota'", ShellDataProcessorService._wrapInQuotes("ala ''ma kota"));
        assertEquals("d", "'ala '\\''ma ko'\\''ta'", ShellDataProcessorService._wrapInQuotes("ala 'ma ko'ta"));
        assertEquals("e", "'ala ma kota'\\'", ShellDataProcessorService._wrapInQuotes("ala ma kota'"));
        assertEquals("f", "\\'", ShellDataProcessorService._wrapInQuotes("'"));
        assertEquals("g", "\\''ala ma kota'", ShellDataProcessorService._wrapInQuotes("'ala ma kota"));
        assertEquals("h", "\\''ala ma kota'\\'", ShellDataProcessorService._wrapInQuotes("'ala ma kota'"));
    }
}

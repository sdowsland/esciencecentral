#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp "$DIR/../lib/*" com.connexience.server.workflow.cloud.cmd.StopLocalServer $1

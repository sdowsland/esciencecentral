#!/bin/bash
echo "Running engine"
if [ -z "$1" ]; then
        echo "No config host supplied"
        exit 2
fi

if [ -z "$JAVA_HOME" ]; then
	echo "JAVA_HOME is not defined"
	exit 2
fi
DIR=$2
ENGINE_DIR=$3

rm -rf $ENGINE_DIR
if [ ! -d "$DIRECTORY" ]; then
	mkdir $ENGINE_DIR
fi

set -e -x
URL="$1/workflow/download/engine-package.zip"
wget --directory-prefix $ENGINE_DIR $URL
unzip $ENGINE_DIR/engine-package.zip -d $ENGINE_DIR
chmod a+x $ENGINE_DIR/bin/*.sh

for i in $ENGINE_DIR/lib/*.jar; do
	CP="$CP:$i";
done

java -Dlog4j.configuration=enginedebug.properties -Djava.library.path=$ENGINE_DIR/bin/lib  -cp $CP  com.connexience.server.workflow.cloud.CloudWorkflowEngine -configPath "$1"

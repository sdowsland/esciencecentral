/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.project.study.parser.ParsedStudy;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.StorageUtils;
import junit.framework.Assert;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * User: nsjw7
 * Date: 01/09/15
 * Time: 09:34
 */
public class GhanaTest extends javax.servlet.http.HttpServlet {

    private Ticket ticket;

    @Context
    SecurityContext secContext;


    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        try {
            response.setContentType("text/plain");

            ticket = getTicket();

            response.getWriter().write("GHANA TEST\n\n");

            long startTime = System.currentTimeMillis();
            Study study = testCreateStudy();
            response.getWriter().write("Passed testCreateSudy()\n");

            DocumentRecord doc = testBasicUpload(study);
            response.getWriter().write("Passed testBasicUpload()\n");

            ParsedStudy parsedStudy = testBasicParse(study, doc);
            response.getWriter().write("Passed testBasicParse()\n");

            testBasicPersist(study, parsedStudy);
            response.getWriter().write("Passed testBasicPersist()\n");


            testDeleteStudy(study);
            response.getWriter().write("Passed testDeleteSudy()\n");
            long endTime = System.currentTimeMillis();
            System.out.println("Time for tests: " + (endTime - startTime));
            response.getWriter().write("Time for tests: " + (endTime - startTime));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Ticket getTicket() throws ConnexienceException {

        return EJBLocator.lookupTicketBean().createWebTicket("simon.woodman@ncl.ac.uk");
    }


    private Study testCreateStudy() throws ConnexienceException {
        String studyName = UUID.randomUUID().toString();
        Study study = new Study(studyName, ticket.getUserId());
        study.setExternalId(studyName);
        study.setStartDate(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 2)));
        study.setEndDate(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 2)));

        study = EJBLocator.lookupStudyBean().saveStudy(ticket, study);
        Assert.assertNotNull(study.getId());

        study = EJBLocator.lookupStudyBean().getStudy(ticket, study.getId());
        Assert.assertNotNull(study.getId());

        return study;
    }

    private void testDeleteStudy(Study study) throws ConnexienceException {

        study = EJBLocator.lookupStudyBean().getStudy(ticket, study.getId());
        Assert.assertNotNull(study.getId());

        EJBLocator.lookupStudyBean().deleteStudy(ticket, study.getId(), true);

        try {
            study = EJBLocator.lookupStudyBean().getStudy(ticket, study.getId());
            Assert.assertNull(study);
        } catch (ConnexienceException ignored) {
        }
    }

    private DocumentRecord testBasicUpload(Study study) throws Exception {

        File ss = new File("/Users/nsjw7/Desktop/Ghana Baseline Study Structure.xlsx");

        String dataFolderId = study.getDataFolderId();

        DocumentRecord doc = new DocumentRecord();
        doc.setName(ss.getName());
        doc.setContainerId(dataFolderId);
        doc.setCreatorId(ticket.getUserId());
        doc.setProjectId(String.valueOf(study.getId()));
        doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);

        DocumentVersion dv = StorageUtils.upload(ticket, new FileInputStream(ss), ss.length(), doc, "Saved From Study " + study.getName());
        doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, dv.getDocumentRecordId());
        Assert.assertNotNull(doc.getId());
        return doc;
    }

    private ParsedStudy testBasicParse(Study study, DocumentRecord doc) throws Exception {

        ParsedStudy parsedStudy = EJBLocator.lookupStudyParserBean().parseStudyStructure(ticket, doc.getId(), study.getId(), "");
//        Assert.assertEquals(1, parsedStudy.groups.size());
//        Assert.assertEquals(2, parsedStudy.groups.get("Australia").children.size());
//        Assert.assertEquals(3, parsedStudy.groups.get("Australia").children.get("Melbourne").children.size());

        //todo: check other properties
        return parsedStudy;
    }

    private void testBasicPersist(Study study, ParsedStudy parsedStudy) throws Exception {
        Ticket ticket = getTicket();
        StudyRemote studyBean = EJBLocator.lookupStudyBean();
        SubjectsRemote subjectBean = EJBLocator.lookupSubjectsBean();
        LoggersRemote loggersBean = EJBLocator.lookupLoggersBean();

        final List<String> feedback = EJBLocator.lookupStudyParserBean().persistParsedStudy(ticket, parsedStudy);
        Assert.assertEquals(0, feedback.size());

//        study = studyBean.getStudy(ticket, study.getId());
//        Collection<SubjectGroup> groups = subjectBean.getSubjectGroups(ticket, study.getId());
//        //check there are the right number of groups
//        Assert.assertEquals(9, groups.size());
//
//        List<Phase> phases = studyBean.getPhases(ticket, study.getId());
//        Assert.assertEquals(2, phases.size());
//
//        Phase phaseTwo = studyBean.getPhaseByName(ticket, study.getId(), "two");
//        Assert.assertNotNull(phaseTwo);
//
//        Collection<SubjectGroup> phaseTwoGroups = subjectBean.getPhaseSubjectGroups(ticket, phaseTwo.getId());
//        Assert.assertEquals(9, phaseTwoGroups.size());
//
//        Collection<SubjectGroup> topLevelGroups = subjectBean.getChildSubjectGrous(ticket, phaseTwo.getId(), null);
//        Assert.assertEquals(1, topLevelGroups.size());
//
//        Collection<SubjectGroup> ozGroups = subjectBean.getChildSubjectGrous(ticket, phaseTwo.getId(), topLevelGroups.iterator().next().getId());
//        Assert.assertEquals(2, ozGroups.size());
//
//        Collection<SubjectGroup> melGroups = subjectBean.getChildSubjectGrous(ticket, phaseTwo.getId(), ozGroups.iterator().next().getId());
//
//        SubjectGroup house1 = melGroups.iterator().next();
//        Assert.assertNotNull(house1);
//
//        Collection<Subject> subjects = subjectBean.getSubjectsInGroup(ticket, house1.getId());
//        Assert.assertEquals(1, subjects.size()); //ordering means it gets house 4 for some reason
//
//        Collection<LoggerDeployment> deployments = loggersBean.getLoggerDeployments(ticket, house1.getId());
//        Assert.assertEquals(1, deployments.size());
//
//        LoggerConfiguration config = loggersBean.getLoggerDeploymentConfiguration(ticket, deployments.iterator().next().getId());
//        Assert.assertNotNull(config);
//        Assert.assertEquals("Default", config.getName());

    }

}

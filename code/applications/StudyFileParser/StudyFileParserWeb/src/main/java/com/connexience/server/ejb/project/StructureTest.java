/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.security.Ticket;
import junit.framework.Assert;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.util.*;

/**
 * User: nsjw7
 * Date: 01/09/15
 * Time: 09:34
 */
public class StructureTest extends javax.servlet.http.HttpServlet {

    private Ticket ticket;

    @Context
    SecurityContext secContext;


    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        try {
            response.setContentType("text/plain");

            response.getWriter().write("STRUCTURE TEST\n\n");

            ticket = getTicket();
            long startTime = System.currentTimeMillis();
            Study study = testCreateStudy();
            response.getWriter().write("Passed testCreateSudy()\n");

            String numGroupsParam = request.getParameter("numGroups");
            if (numGroupsParam == null || numGroupsParam.equals("")) {
                numGroupsParam = "100";
            }

            List<Logger> loggers = testCreateLogger(study,Integer.parseInt(numGroupsParam));

            testCreateGroups(study, Integer.parseInt(numGroupsParam));
            response.getWriter().write("Passed testCreateGroups()\n");

            testCreateDeployment(study, loggers);

            testDeleteStudy(study);
            response.getWriter().write("Passed testDeleteSudy()\n");
            long endTime = System.currentTimeMillis();
            System.out.println("Time for tests: " + (endTime - startTime));
            response.getWriter().write("Time for tests: " + (endTime - startTime));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Ticket getTicket() throws ConnexienceException {

        return EJBLocator.lookupTicketBean().createWebTicket("simon.woodman@ncl.ac.uk");
    }


    private Study testCreateStudy() throws ConnexienceException {
        String studyName = UUID.randomUUID().toString();
        Study study = new Study(studyName, ticket.getUserId());
        study.setExternalId(studyName);
        study.setStartDate(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 2)));
        study.setEndDate(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 2)));

        study = EJBLocator.lookupStudyBean().saveStudy(ticket, study);
        Assert.assertNotNull(study.getId());

        study = EJBLocator.lookupStudyBean().getStudy(ticket, study.getId());
        Assert.assertNotNull(study.getId());

        return study;
    }

    private void testDeleteStudy(Study study) throws ConnexienceException {

        study = EJBLocator.lookupStudyBean().getStudy(ticket, study.getId());
        Assert.assertNotNull(study.getId());

        EJBLocator.lookupStudyBean().deleteStudy(ticket, study.getId(), true);

        try {
            study = EJBLocator.lookupStudyBean().getStudy(ticket, study.getId());
            Assert.assertNull(study);
        } catch (ConnexienceException ignored) {
        }
    }

    private void testCreateGroups(Study study, int numGroups) throws Exception {
        System.out.println("StructureTest.testCreateGroups");
        SubjectsRemote subjectBean = EJBLocator.lookupSubjectsBean();

        Phase phase = EJBLocator.lookupStudyBean().getPhaseByName(ticket, study.getId(), "Default");
        for (int i = 0; i < numGroups; i++) {

            long startTime = System.currentTimeMillis();
            subjectBean.createSubjectGroup(ticket, study.getId(), "TestGroup" + i, phase.getId());

            long endTime = System.currentTimeMillis();
            System.out.println("Group " + i + ": " + (endTime - startTime) + "ms");
        }
    }

    private List<Logger> testCreateLogger(Study study, int numLoggers) throws Exception {
        System.out.println("StructureTest.testCreateLogger");
        LoggersRemote loggersBean = EJBLocator.lookupLoggersBean();

        List<Logger> loggers = new ArrayList<Logger>();

        LoggerType loggerType = loggersBean.createLoggerType(ticket, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        for (int i = 0; i < numLoggers; i++) {
            long startTime = System.currentTimeMillis();

            String serialNumber = UUID.randomUUID().toString();
            Logger logger = loggersBean.createLogger(ticket, loggerType.getId(), serialNumber);
            loggers.add(logger);

            long endTime = System.currentTimeMillis();
            System.out.println("Logger " + i + ": " + (endTime - startTime) + "ms");

        }
        return loggers;
    }


    private void testCreateDeployment(Study study, List<Logger> loggers) throws Exception {
        System.out.println("StructureTest.testCreateDeployment");
        LoggersRemote loggersBean = EJBLocator.lookupLoggersBean();

        Object[] groups = EJBLocator.lookupSubjectsBean().getSubjectGroups(ticket, study.getId()).toArray();

        Collection<LoggerConfiguration> configs = loggersBean.getLoggerConfigurationsByType(ticket, loggers.get(0).getLoggerType().getId());
        LoggerConfiguration config = configs.iterator().next();

        for (int i = 0; i < groups.length; i++) {
            long startTime = System.currentTimeMillis();

            loggersBean.createLoggerDeployment(ticket, study.getId(), loggers.get(i).getId(), config.getId(), ((SubjectGroup) groups[i]).getId());

            long endTime = System.currentTimeMillis();
            System.out.println("Deployment " + i + ": " + (endTime - startTime) + "ms");
        }
    }

}

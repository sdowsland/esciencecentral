/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.scanner.ScannerRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.project.study.parser.ParsedGroup;
import com.connexience.server.model.project.study.parser.ParsedLoggerDeployment;
import com.connexience.server.model.project.study.parser.ParsedStudy;
import com.connexience.server.model.project.study.parser.ParsedSubject;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.util.SignatureUtils;
import com.connexience.server.util.StorageUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/StudyParserBean", beanInterface = StudyParserRemote.class)
public class StudyParserBean implements StudyParserRemote {

    @Inject
    private EntityManager em;

    @EJB(lookup = "java:global/esc/server-beans/ProjectsBean")
    private ProjectsRemote projectsBean;

    @EJB(lookup = "java:global/esc/server-beans/StudyBean")
    private StudyRemote studyBean;

    @EJB(lookup = "java:global/esc/server-beans/LoggersBean")
    private LoggersRemote loggersBean;

    @EJB(lookup = "java:global/esc/server-beans/SubjectsBean")
    private SubjectsRemote subjectsBean;

    @EJB(lookup = "java:global/esc/server-beans/UploaderBean")
    private UploaderRemote uploaderBean;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    private StorageRemote storageBean;

    @EJB(lookup = "java:global/esc/server-beans/ScannerBean")
    private ScannerRemote scannerBean;


    @Override
    public ParsedStudy parseStudyStructure(final Ticket ticket, final String escFileId, final Integer studyId, final String loggerColumnName) throws ConnexienceException {
        final DocumentRecord documentRecord = storageBean.getDocumentRecord(ticket, escFileId);
        final DocumentVersion documentVersion = storageBean.getLatestVersion(ticket, escFileId);
        final InputStream inputStream = StorageUtils.getInputStream(ticket, documentRecord, documentVersion);

        final ParsedStudy study = new ParsedStudy();

        study.escID = studyId;
        study.properties.put("xslx.structure", "true");
        study.properties.put("xslx.structure.time", new SimpleDateFormat("dd-MM-yyyy HH:MM:ss").format(new Date()));
        study.properties.put("xslx.structure.file", documentRecord.getName());

        final Study escStudy = studyBean.getStudy(ticket, studyId);

        try {
            final Workbook workbook = WorkbookFactory.create(inputStream);
            workbook.setMissingCellPolicy(Row.CREATE_NULL_AS_BLANK);
            final Sheet sheet = workbook.getSheetAt(0);

            final Map<Integer, String> columnNames = new HashMap<>();

            // Find the column with the LoggerIds in
            final Integer loggerColumnIndex = getNamedColumn(workbook, Arrays.asList("loggerid", "serialnumber"), study.problems, true);
            if (loggerColumnIndex < 0) {//cannot find the loggerId column
                return study;
            }

            int subjectOffset = 2;  //this is always 2 to shift the column over the config and onto the first subject
            int startDateCol = getNamedColumn(workbook, Arrays.asList("startdate", "deploymentdate"), study.problems, false);
            int endDateCol = getNamedColumn(workbook, Arrays.asList("enddate", "retrieveddate"), study.problems, false);
            int phaseCol = getNamedColumn(workbook, Collections.singletonList("phase"), study.problems, false);
            int passwordCol = getNamedColumn(workbook, Collections.singletonList("password"), study.problems, false);

            if (startDateCol > 0) {
                subjectOffset++;
            }
            if (endDateCol > 0) {
                subjectOffset++;
            }
            if (phaseCol > 0) {
                subjectOffset++;
            }
            if (passwordCol > 0) {
                subjectOffset++;
            }

            //todo: add start time and end time - have already incremented the subject offset

            for (final Row row : sheet) {
                for (final Cell cell : row) {
                    // set all cell types to STRING to prevent POI ruining identifiers (e.g., int -> decimal)
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                }

                // Check there are enough cells (groups, logger, 1 subject)
                final int expectedCellCount = loggerColumnIndex + 2;

                boolean warn = false;

                if (row.getPhysicalNumberOfCells() < expectedCellCount) {
                    for (final Cell cell : row) {
                        // Enable warning if entire row is not empty/null
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (StringUtils.isNotBlank(cell.getStringCellValue())) {
                            warn = true;
                            break;
                        }
                    }

                    if (warn) {
                        study.problems.add("Row " + row.getRowNum() + " skipped, expected: >= " + expectedCellCount + " cells, got: " + row.getPhysicalNumberOfCells() + " cells.");
                    }

                    if (row.getRowNum() == 0) {
                        study.problems.add("Failure on Row 0, aborting parse.");
                        return study;
                    }

                    continue;
                }

                // Check at least one group exists
                boolean nullFound = false;

                if (row.getCell(0) == null) {
                    study.problems.add("Cell 0 on Row " + row.getRowNum() + " is null, skipping row.");
                    nullFound = true;
                }

                // can't put this continue within the above for-loop
                if (nullFound) {
                    if (row.getRowNum() == 0) {
                        study.problems.add("Failure on Row 0, aborting parse.");
                        return study;
                    }

                    continue;
                }

                // if row 0, extract column names for groups
                if (row.getRowNum() == 0) {
                    for (int i = 0; i < loggerColumnIndex; i++) {
                        final Cell cell = row.getCell(i);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (StringUtils.isNotBlank(cell.getStringCellValue())) {
                            columnNames.put(i, cell.getStringCellValue());
                        }
                    }
                } else {
                    ParsedGroup owner = null;

                    //get the phase name
                    String phaseName = "Default";
                    if (phaseCol > 0) {
                        final Cell cell = row.getCell(phaseCol);

                        if (cell != null && StringUtils.isNotBlank(cell.getStringCellValue())) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            phaseName = cell.getStringCellValue();
                        }
                    }

                    // Subject Groups
                    for (int i = 0; i < loggerColumnIndex; i++) {
                        final Cell cell = row.getCell(i);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        final String groupID = cell.getStringCellValue();
                        final String categoryName = columnNames.get(i);
                        //final String groupName = groupID;

                        String password = null;
                        String username = null;

                        //only set the password on the 'leaf' group
                        if (i == loggerColumnIndex - 1) {
                            if (passwordCol > 0) {
                                password = row.getCell(passwordCol).getStringCellValue();

                                //Hash the password rather than storing plaintext - will be hashed with SHA1 by default
                                password = SignatureUtils.getHashedPassword(password);

                            }
                        }

                        if (!groupID.isEmpty()) {
                            // Resolve down the tree, starting with the study (i == 0)
                            if (i == 0) {
                                owner = study.checkGroup(groupID, categoryName, phaseName, password);
                            } else {
                                owner = owner.checkGroup(groupID, categoryName, phaseName, password);
                            }
                        }
                    }

                    // Subjects
                    for (int i = loggerColumnIndex + subjectOffset; i < row.getLastCellNum(); i++) {
                        final Cell cell = row.getCell(i);

                        if (owner != null && cell != null && StringUtils.isNotBlank(cell.getStringCellValue())) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            final String subjectName = cell.getStringCellValue();

                            owner.checkSubject(subjectName);
                        }
                    }

                    // Logger deployment info
                    final Cell idCell = row.getCell(loggerColumnIndex);
                    final Cell configCell = row.getCell((loggerColumnIndex + 1));

                    Date startDate = escStudy.getStartDate();
                    if (startDateCol > 0) {

                        //Get the cell which contains the date
                        final Cell startDateCell = row.getCell(startDateCol);

                        //for some reason parsing it as an excel numerical date doesn't work properly
                        // so get the String representation which is days since 1900
                        startDateCell.setCellType(Cell.CELL_TYPE_STRING);
                        String startDateStr = startDateCell.getStringCellValue();

                        //Use the POI date utils to parse the date
                        if (startDateStr != null && !startDateStr.equals("")) {
                            startDate = DateUtil.getJavaDate(Double.parseDouble(startDateStr));
                        }
                    }

                    Date endDate = escStudy.getEndDate();
                    if (endDateCol > 0) {

                        //Get the cell which contains the date
                        final Cell endDateCell = row.getCell(endDateCol);

                        //for some reason parsing it as an excel numerical date doesn't work properly
                        // so get the String representation which is days since 1900
                        endDateCell.setCellType(Cell.CELL_TYPE_STRING);
                        String endDateStr = endDateCell.getStringCellValue();

                        //Use the POI date utils to parse the date
                        if (endDateStr != null && !endDateStr.equals("")) {
                            endDate = DateUtil.getJavaDate(Double.parseDouble(endDateStr));
                        }
                    }

                    idCell.setCellType(Cell.CELL_TYPE_STRING);
                    configCell.setCellType(Cell.CELL_TYPE_STRING);
                    if (owner != null && StringUtils.isNotBlank(idCell.getStringCellValue()) && StringUtils.isNotBlank(configCell.getStringCellValue())) {
                        final String loggerID = idCell.getStringCellValue();
                        final String configName = configCell.getStringCellValue();

                        owner.checkDeployment(loggerID, configName, startDate, endDate);
                    }
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException(e);
        }

        // Walk groups and generate warnings
        for (final ParsedGroup group : study.groups.values()) {
            walkGroup(ticket, null, study, group, study.problems, false);
        }

        return study;
    }

    @Override
    public List<String> persistParsedStudy(final Ticket ticket, final ParsedStudy parsedStudy) throws ConnexienceException {
        final List<String> problems = new ArrayList<>();

        final Study study = studyBean.getStudy(ticket, parsedStudy.escID);

        for (Map.Entry<String, String> entry : parsedStudy.properties.entrySet()) {
            study.setAdditionalProperty(entry.getKey(), entry.getValue());
        }

        studyBean.saveStudy(ticket, study);

        for (final ParsedGroup group : parsedStudy.groups.values()) {
            walkGroup(ticket, study, parsedStudy, group, problems, true);
        }

        //If there is an unpacking workflow *and* some data in the uploads directory then attempt to run the workflow
        if (study.getRemoteScannerId() != null) {
            RemoteFilesystemScanner scanner = scannerBean.getScanner(ticket, study.getRemoteScannerId());

            if (scanner != null) {
                if (scanner.getWorkflowId() != null && scanner.getTargetFolderId() != null) {

                    ticket.setDefaultProjectId(Integer.toString(study.getId()));
                    ticket.setDefaultStorageFolderId(study.getDataFolderId());

                    List<DocumentRecord> documents = storageBean.getFolderDocumentRecords(ticket, scanner.getTargetFolderId());

                    for (DocumentRecord doc : documents) {

                        System.out.println("Starting workflow: " + scanner.getWorkflowId() + " on document: " + doc.getName() + "[" + doc.getId() + "]");

                        WorkflowInvocationMessage invocationMsg = new WorkflowInvocationMessage(ticket, scanner.getWorkflowId());
                        invocationMsg.setTargetFileId(doc.getId());

                        WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, invocationMsg);
                    }
                }
            }
        }
        return problems;
    }

    @Override
    public List<String> parseLoggerDefinitions(final Ticket ticket, final String escFileId, final Integer loggerTypeId) throws ConnexienceException {
        final List<String> problems = new ArrayList<>();

        final DocumentRecord documentRecord = storageBean.getDocumentRecord(ticket, escFileId);
        final DocumentVersion documentVersion = storageBean.getLatestVersion(ticket, escFileId);
        final InputStream inputStream = StorageUtils.getInputStream(ticket, documentRecord, documentVersion);

        final LoggerType loggerType = loggersBean.getLoggerType(ticket, loggerTypeId);

        try {
            final Workbook workbook = WorkbookFactory.create(inputStream);
            final Sheet sheet = workbook.getSheetAt(0);

            //Get the loggerId column

            final Integer loggerColumnIndex = getNamedColumn(workbook, Arrays.asList("loggerid", "serialnumber"), problems, true);
            if (loggerColumnIndex < 0) { //Cannot find the column with LoggerIds in
                return problems;
            }

            for (final Row row : sheet) {
                if (row.getRowNum() > 0) {
                    final Cell cell = row.getCell(loggerColumnIndex);

                    if (cell != null) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);

                        if (StringUtils.isNotBlank(cell.getStringCellValue())) {
                            try {
                                //Try to get the location from the next column along
                                final Cell locationCell = row.getCell(loggerColumnIndex + 1);
                                String location = null;
                                if (locationCell != null) {
                                    locationCell.setCellType(Cell.CELL_TYPE_STRING);
                                    location = locationCell.getStringCellValue();
                                }

                                if (loggersBean.getLoggersBySerial(ticket, getCellValueAsString(cell)).size() == 0) {
                                    loggersBean.createLogger(ticket, loggerTypeId, getCellValueAsString(cell), location);
                                } else {
                                    problems.add("Could not create Logger: " + getCellValueAsString(cell) + " as it already exists");
                                }
                            } catch (ConnexienceException e) {
                                problems.add("Could not create Logger: " + e.getMessage());
                            }
                        }
                    } else {
                        problems.add("Cell " + loggerColumnIndex + " in Row " + row.getRowNum() + " is null.");
                    }
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException(e);
        }

        return problems;
    }

    @Override
    public List<String> parseLoggerProperties(final Ticket ticket, final String escFileId, final Integer studyId) throws ConnexienceException {
        final List<String> problems = new ArrayList<>();

        final DocumentRecord documentRecord = storageBean.getDocumentRecord(ticket, escFileId);
        final DocumentVersion documentVersion = storageBean.getLatestVersion(ticket, escFileId);
        final InputStream inputStream = StorageUtils.getInputStream(ticket, documentRecord, documentVersion);

        Phase phase;
        Study study = em.merge(studyBean.getStudy(ticket, studyId));

        Collection<LoggerDeployment> loggerDeployments = null;
        final Map<String, LoggerDeployment> loggerDeploymentMap = new HashMap<>();


        try {
            final Workbook workbook = WorkbookFactory.create(inputStream);
            final Sheet sheet = workbook.getSheetAt(0);

            final Map<Integer, String> columnNames = new HashMap<>();
            final int loggerIdColIndex = getNamedColumn(workbook, Arrays.asList("loggerid", "serialnumber"), problems, false);
            int phaseOffset = 0;

            for (final Row row : sheet) {
                // Collect Column Names
                if (row.getRowNum() == 0) {
                    for (final Cell cell : row) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);

                        if (StringUtils.isBlank(cell.getStringCellValue())) {
                            problems.add("Cell " + cell.getColumnIndex() + " on Row 0 was empty, every column should have a title.");
                            return problems;
                        }

                        columnNames.put(cell.getColumnIndex(), cell.getStringCellValue());
                    }
                } else {
                    boolean skip = true;

                    for (final Cell cell : row) {
                        // Enable warning if entire row is not empty/null
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (StringUtils.isNotBlank(cell.getStringCellValue())) {
                            skip = false;
                            break;
                        }
                    }

                    if (skip) {
                        continue;
                    }

                    if (loggerDeployments == null) {

                        int phaseCellNumber = getNamedColumn(workbook, Arrays.asList("phase", "phasename", "phaseid"), problems, false);
                        if (phaseCellNumber >= 0) {
                            Cell phaseCell = row.getCell(phaseCellNumber);

                            phaseCell.setCellType(Cell.CELL_TYPE_STRING);
                            String phaseName = phaseCell.getStringCellValue();

                            try {
                                phase = em.createNamedQuery("Phase.namedPhase", Phase.class)
                                        .setParameter("studyId", studyId)
                                        .setParameter("name", phaseName)
                                        .getSingleResult();
                            } catch (Exception e) {
                                problems.add("Cannot find phase: " + phaseName + " from Row " + row.getRowNum());
                                continue;
                            }

                            if (phase == null) {
                                problems.add("Cannot find phase for Row " + row.getRowNum() + ". Skipping");
                                continue;
                            }
                            phaseOffset++; //increment the phase offset before we get to the props
                        } else {
                            //no phase in spreadsheet so get the first one from the study
                            phase = study.getPhases().get(0);
                        }


                        loggerDeployments = loggersBean.getLoggerDeploymentsInPhase(ticket, studyId, phase.getId());
                        if (loggerDeployments.isEmpty()) {
                            problems.add("There are 0 logger deployments for Phase" + phase.getName() + " in Study id='" + studyId + "', cannot set properties.");
                            return problems;
                        }

                        for (final LoggerDeployment loggerDeployment : loggerDeployments) {
                            loggerDeploymentMap.put(loggerDeployment.getLogger().getSerialNumber(), loggerDeployment);
                        }
                    }

                    LoggerDeployment loggerDeployment;
                    Cell loggerIdCell = row.getCell(loggerIdColIndex);
                    if (loggerIdCell == null) {
                        problems.add("Cannot find loggerId on Row: " + row.getRowNum());
                        continue;
                    }
                    loggerIdCell.setCellType(Cell.CELL_TYPE_STRING);
                    String loggerDeploymentName = loggerIdCell.getStringCellValue();
                    loggerDeployment = em.merge(loggerDeploymentMap.get(loggerDeploymentName));
                    if (loggerDeployment == null) {
                        problems.add("No logger deployment could be found for Logger: " + loggerDeploymentName + " on Row " + row.getRowNum() + ".");
                        continue;
                    }


                    // find logger deployment, set additional props, save
                    for (int i = loggerIdColIndex; i < row.getLastCellNum(); i++) {

                        final Cell cell = row.getCell(i);
                        cell.setCellType(Cell.CELL_TYPE_STRING);

                        if (StringUtils.isBlank(cell.getStringCellValue())) {
                            final String propertyName = columnNames.get(cell.getColumnIndex());

                            problems.add("Cell " + cell.getColumnIndex() + " on Row " + row.getRowNum() + " is empty, skipping property (name = \"" + propertyName + "\".");
                            continue;
                        }

                        final String propertyName = columnNames.get(cell.getColumnIndex());
                        final String propertyValue = cell.getStringCellValue();

                        loggerDeployment.putAdditionalProperty(propertyName, propertyValue);
                    }

                    if (loggerDeployment != null) {
                        loggersBean.saveLoggerDeployment(ticket, loggerDeployment);
                    }
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException(e);
        }

        study.setAdditionalProperty("xslx.loggerprops", "true");
        study.setAdditionalProperty("xslx.loggerprops.time", new SimpleDateFormat("dd-MM-yyyy HH:MM:ss").format(new Date()));
        study.setAdditionalProperty("xslx.loggerprops.file", documentRecord.getName());

        studyBean.saveStudy(ticket, study);

        return problems;
    }

    @Override
    public List<String> parseSubjectProperties(final Ticket ticket, final String escFileId, final Integer studyId) throws ConnexienceException {
        final List<String> problems = new ArrayList<>();

        try {

            final DocumentRecord documentRecord = storageBean.getDocumentRecord(ticket, escFileId);
            final DocumentVersion documentVersion = storageBean.getLatestVersion(ticket, escFileId);
            final InputStream inputStream = StorageUtils.getInputStream(ticket, documentRecord, documentVersion);

            final Map<String, Subject> subjectMap = new HashMap<>();

            final Workbook workbook = WorkbookFactory.create(inputStream);
            final Sheet sheet = workbook.getSheetAt(0);

            final Map<Integer, String> columnNames = new HashMap<>();

            Study study = em.merge(studyBean.getStudy(ticket, studyId));
            Phase phase = null;
            Collection<Subject> subjects = null;
            int phaseOffset = 0;
            int subjectIdColIndex = getNamedColumn(workbook, Collections.singletonList("subjectid"), problems, true);


            for (final Row row : sheet) {
                Subject subject = null;
                SubjectGroup subjectGroup = null;

                // Collect Column Names
                if (row.getRowNum() == 0) {
                    for (final Cell cell : row) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);

                        if (StringUtils.isBlank(cell.getStringCellValue())) {
                            problems.add("Cell " + cell.getColumnIndex() + " on Row 0 was empty, every column should have a title.");
                            return problems;
                        }

                        columnNames.put(cell.getColumnIndex(), cell.getStringCellValue());
                    }
                } else {

                    //initialise the subjectMap
                    if (subjects == null) {

                        //Get the phase name for this study

                        int phaseCellNumber = getNamedColumn(workbook, Arrays.asList("phase", "phasename", "phaseid"), problems, false);
                        if (phaseCellNumber > 0) {
                            Cell phaseCell = row.getCell(phaseCellNumber);

                            phaseCell.setCellType(Cell.CELL_TYPE_STRING);
                            String phaseName = phaseCell.getStringCellValue();

                            try {
                                phase = em.createNamedQuery("Phase.namedPhase", Phase.class)
                                        .setParameter("studyId", studyId)
                                        .setParameter("name", phaseName)
                                        .getSingleResult();
                            } catch (Exception e) {
                                problems.add("Cannot find phase: " + phaseName + " from Row " + row.getRowNum());
                                continue;
                            }

                            if (phase == null) {
                                problems.add("Cannot find phase for Row " + row.getRowNum() + ". Skipping");
                                continue;
                            }
                            phaseOffset++; //increment the phase offset before we get to the props
                        } else {
                            //no phase in spreadsheet so get the first one from the study
                            phase = study.getPhases().get(0);
                        }

                        subjects = subjectsBean.getSubjectsInPhase(ticket, studyId, phase.getId());

                        if (subjects.isEmpty()) {
                            problems.add("There are 0 Subjects in Study id='" + studyId + "', cannot set properties.");
                            return problems;
                        }

                        for (Subject s : subjects) {
                            subjectMap.put(s.getExternalId(), s);
                        }
                    }

                    boolean skip = true;
                    for (final Cell cell : row) {
                        // Enable warning if entire row is not empty/null
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (StringUtils.isNotBlank(cell.getStringCellValue())) {
                            skip = false;
                            break;
                        }
                    }

                    if (skip) {
                        continue;
                    }

                    // Get the Id of the subject if it exists
                    row.getCell(subjectIdColIndex).setCellType(Cell.CELL_TYPE_STRING);
                    String subjectExternalId = row.getCell(subjectIdColIndex).getStringCellValue();

                    //If the subject doesn't exist, try to find a group
                    if (subjectExternalId != null && !subjectExternalId.equals("")) {

                        //subject properties rather than subject gorup
                        Subject s = subjectMap.get(subjectExternalId);
                        if (s == null) {
                            problems.add("Cannot find Subject: " + subjectExternalId);
                            continue;
                        }

                        subject = em.merge(subjectMap.get(subjectExternalId));

                    } else {

                        //find the top level groups for this phase
                        List<SubjectGroup> topLevelGroups = em.createNamedQuery("SubjectGroup.findTopLevelGroups", SubjectGroup.class)
                                .setParameter("phaseId", phase.getId())
                                .getResultList();

                        //Iterate over the groups to find the group to attach properties to
                        for (int i = 0; i < subjectIdColIndex; i++) {

                            final Cell cell = row.getCell(i);
                            if (cell != null) {

                                cell.setCellType(Cell.CELL_TYPE_STRING);

                                if (i == 0) {  //top level groups
                                    for (SubjectGroup group : topLevelGroups) {
                                        if (group.getDisplayName().equalsIgnoreCase(cell.getStringCellValue())) {
                                            subjectGroup = group;
                                        }
                                    }
                                } else {
                                    //subgroups which may or may not exist in the spreadsheet
                                    for (SubjectGroup childGroup : subjectGroup.getChildren()) {
                                        if (childGroup.getDisplayName().equalsIgnoreCase(cell.getStringCellValue())) {
                                            subjectGroup = childGroup;
                                        }
                                    }
                                }
                            } else {
                                //cell is null
                                if (i == 0) {
                                    problems.add("Couldn't find Subject or Subject Group on Row " + (i + 1));
                                }
                                //ignore if not first column as the group may have alerady been found
                            }
                        }
                    }
                }

                //loop and add the properties from the rows after subjectId column
                for (int i = subjectIdColIndex + phaseOffset + 1; i <= row.getLastCellNum(); i++) {

                    final Cell cell = row.getCell(i);
                    if (cell != null) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);

                        if (StringUtils.isBlank(cell.getStringCellValue())) {
                            final String propertyName = columnNames.get(cell.getColumnIndex());

                            problems.add("Cell " + cell.getColumnIndex() + " on Row " + row.getRowNum() + " is empty, skipping property (name = \"" + propertyName + "\").");
                            continue;
                        }

                        String propertyName = columnNames.get(cell.getColumnIndex());
                        String propertyValue = cell.getStringCellValue();

                        System.out.println("propertyValue = " + propertyValue);

                        if (subject != null) {
                            subject.putAdditionalProperty(propertyName, propertyValue);
                        }
                        if (subjectGroup != null) {
                            subjectGroup.putAdditionalProperty(propertyName, propertyValue);
                        }
                    } else {
                        problems.add("Cell " + (i + 1) + " on Row " + row.getRowNum() + " is empty, skipping.");
                    }
                }

                if (subject != null) {
                    subjectsBean.saveSubject(ticket, subject);
                }
                if (subjectGroup != null) {
                    subjectsBean.saveSubjectGroup(ticket, subjectGroup);
                }

            }

            study.setAdditionalProperty("xslx.subjectprops", "true");
            study.setAdditionalProperty("xslx.subjectprops.time", new SimpleDateFormat("dd-MM-yyyy HH:MM:ss").format(new Date()));
            study.setAdditionalProperty("xslx.subjectprops.file", documentRecord.getName());

            studyBean.saveStudy(ticket, study);

        } catch (Exception e) {
            problems.add("Exception thrown: " + e.getMessage());
            e.printStackTrace();
        }
        return problems;

    }

    private void walkGroup(final Ticket ticket, final Study eSCStudy, final ParsedStudy study, ParsedGroup group, List<String> problems, boolean persist) throws ConnexienceException {
        if (group.parent == null) {
            if (persist) {
                HashMap<String, String> additionalProperties = new HashMap<>();
                if (group.password != null) {
                    additionalProperties.put("herd_hashed_password", group.password);

                }
                group.escID = subjectsBean.createSubjectGroup(ticket, study.escID, group.name, group.category, group.phaseName, additionalProperties).getId();
            }
        } else {
            if (persist) {
                HashMap<String, String> additionalProperties = new HashMap<>();
                if (group.password != null) {
                    additionalProperties.put("herd_hashed_password", group.password);
                }
                group.escID = subjectsBean.createChildGroup(ticket, study.escID, group.parent.escID, group.name, group.category, group.phaseName, additionalProperties).getId();
            }
        }

        for (final ParsedGroup child : group.children.values()) {
            walkGroup(ticket, eSCStudy, study, child, problems, persist);
        }

        for (final ParsedSubject subject : group.subjects.values()) {
            if (persist) {
                subject.escID = subjectsBean.createSubject(ticket, group.escID, subject.externalID).getId();
            }
        }

        for (final ParsedLoggerDeployment parsedLoggerDeployment : group.deployments.values()) {
            final List<Logger> loggers = new ArrayList<>(loggersBean.getLoggersBySerial(ticket, parsedLoggerDeployment.loggerID));

            if (loggers.size() != 1) {
                problems.add(loggers.size() + " Loggers (Serial: '" + parsedLoggerDeployment.loggerID + "'), expected 1, cannot deploy Logger.");
                continue;
            }

            // Get Logger and all Configurations
            final Logger logger = em.merge(loggers.get(0));

            final List<LoggerConfiguration> loggerConfigurations = new ArrayList<>(loggersBean.getLoggerConfigurationsByType(ticket, logger.getLoggerType().getId()));

            if (loggerConfigurations.isEmpty()) {
                problems.add("No Logger Configurations found for Logger (Serial: " + parsedLoggerDeployment.loggerID + ", Manufacturer: '" + logger.getLoggerType().getManufacturer() + "' Type: '" + logger.getLoggerType().getName() + "'), cannot deploy Logger.");
                continue;
            }

            boolean foundConfig = false;

            for (final LoggerConfiguration loggerConfiguration : loggerConfigurations) {
                if (StringUtils.equals(loggerConfiguration.getName(), parsedLoggerDeployment.config)) {
                    foundConfig = true;

                    if (persist) {
                        parsedLoggerDeployment.escID = loggersBean.createLoggerDeployment(ticket, eSCStudy, logger.getId(), loggerConfiguration.getId(), group.escID, parsedLoggerDeployment.startDate, parsedLoggerDeployment.endDate).getId();
                    }

                    break;
                }
            }

            if (!foundConfig) {
                problems.add("Could not find Logger Configuration (Name: '" + parsedLoggerDeployment.config + "') for Logger (Serial: " + parsedLoggerDeployment.loggerID + ", Manufacturer: '" + logger.getLoggerType().getManufacturer() + "' Type: '" + logger.getLoggerType().getName() + "'), cannot deploy Logger.");
            }
        }
    }


    private int getNamedColumn(Workbook workbook, List<String> columnNames, List<String> problems, boolean addToProblems) {

        //The 0th row contains the column names
        Row spreadsheetColumnNames = workbook.getSheetAt(0).getRow(0);

        int foundColIndex = -1;
        for (Iterator<Cell> it = spreadsheetColumnNames.cellIterator(); it.hasNext(); ) {
            Cell thisColumnName = it.next();

            String cleanColumnName = thisColumnName.toString()
                    .toLowerCase()
                    .trim()
                    .replace(" ", "")
                    .replace("_", "")
                    .replace("-", "")
                    .replace(".", "");

            if (columnNames.contains(cleanColumnName)) {
                foundColIndex = thisColumnName.getColumnIndex();
                break;
            }
        }

        if (addToProblems && foundColIndex < 0) {
            problems.add("Could not find a Column with name '" + columnNames + "'.");
        }

        return foundColIndex;
    }


    private String getCellValueAsString(Cell cell) {
        try {
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_BLANK:
                    return "";

                case Cell.CELL_TYPE_BOOLEAN:
                    return Boolean.toString(cell.getBooleanCellValue());

                case Cell.CELL_TYPE_ERROR:
                    return "";

                case Cell.CELL_TYPE_FORMULA:
                    return cell.getCellFormula();

                case Cell.CELL_TYPE_NUMERIC:
                    return Double.toString(cell.getNumericCellValue());

                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue();

                default:
                    return cell.toString();
            }
        } catch (Exception e) {
            // Fall back to toString()
            return cell.toString();
        }
    }
}

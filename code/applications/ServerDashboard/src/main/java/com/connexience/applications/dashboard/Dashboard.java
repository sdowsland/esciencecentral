/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.applications.dashboard;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JFrame;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorage;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;
import org.apache.log4j.Logger;

/**
 *
 * @author hugo
 */
public class Dashboard extends javax.swing.JFrame {
    private static final Logger logger = Logger.getLogger(Dashboard.class);
    
    ArrayList<ServerPanel> servers = new ArrayList<ServerPanel>();
    ArrayList<String> emails = new ArrayList<String>();
    private File dataDir;
    
    Session mailSession;
    String mailUser = "";
    String mailPassword = "";
    boolean enableMessages = false;
    
    Timer timer = new Timer(true);
    
    /**
     * Creates new form Dashboard
     */
    public Dashboard() {
        XmlStorage.registerClass(com.connexience.applications.dashboard.ServerPanel.class);
        dataDir = new File(System.getProperty("user.home") + File.separator + ".inkspot/dashboard");
        if(!dataDir.exists()){
            dataDir.mkdirs();
        }
        
        initComponents();
        setVisible(true);
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);    
        loadSettings();
        
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 17);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        
        // Daily task at 5pm
        timer.scheduleAtFixedRate(new SendStatusTask(), c.getTime(), 86400000);
    }

    public void addServer(String label, String host){
        ServerPanel p = new ServerPanel(this, label, host);
        servers.add(p);
        serversPanel.add(p);
        revalidate();
    }

    public ArrayList<String> getEmails() {
        return emails;
    }
    
    public void removeServer(ServerPanel panel){
        panel.cancelTasks();
        serversPanel.remove(panel);
        servers.remove(panel);
        revalidate();
        repaint();
        saveSettings();;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public String getMailUser() {
        return mailUser;
    }

    public void setMailUser(String mailUser) {
        this.mailUser = mailUser;
    }

    public boolean isEnableMessages() {
        return enableMessages;
    }

    public void setEnableMessages(boolean enableMessages) {
        this.enableMessages = enableMessages;
    }
    
    public void setupEMailSession(){
        if(enableMessages && !mailUser.isEmpty() && !mailPassword.isEmpty()){
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");

            props.put("mail.user", mailUser);

            props.put("mail.smtp.host", "smtp.googlemail.com");
            props.put("mail.smtp.port", "465");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.enable", "true");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.from", mailUser);
            props.put("mail.debug", "false");

            mailSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailUser, mailPassword);
                }
            });
        } else {
            mailSession = null;
        }
    }
    
    public void logMessage(ServerPanel server, String message){
        logger.info(server.getHostLabel() + ": " + message);
    }
    
    public void sendMessage(ServerPanel server, final String message){
        if(mailSession!=null){
            ArrayList<InternetAddress> recipientsList = new ArrayList<InternetAddress>();
            for(String email : emails){
                try {
                    recipientsList.add(new InternetAddress(email));
                } catch (Exception e){
                    logger.error("Error sending message; " + e.getMessage());
                }
            }

            final InternetAddress[] addresses = recipientsList.toArray(new InternetAddress[recipientsList.size()]);
            new Thread(new Runnable(){
                public void run(){
                    try {
                        Message msg = new MimeMessage(mailSession);
                        msg.setRecipients(javax.mail.Message.RecipientType.TO, addresses);
                        msg.setSubject("Monitoring Message");
                        msg.setContent(message, "text/html");
                        msg.saveChanges();

                        Transport.send(msg);            
                    } catch (Exception e){
                        logger.error("EMail error: " +e. getMessage());
                    }                    
                }
            }).start();

        } else {
            if(server!=null){
                logger.info(server.getHostLabel() + ": " + message);
            } else {
                logger.info("Message: " + message);
            }
        }        
    }
    

    public void loadSettings() {
        if(PreferenceManager.loadPropertiesFromHomeDir(".inkspot", "dashboard.xml")){
            XmlDataStore store = PreferenceManager.getSystemPropertyGroup("Servers");
            int serverCount = store.intValue("ServerCount", 0);
            ServerPanel p;
                    
            for(int i=0;i<serverCount;i++){
                if(store.containsName("Server" + i)) {
                    try {
                        p = (ServerPanel)store.xmlStorableValue("Server" + i);
                        p.refreshUI();
                        p.setParentDashboard(this);
                        servers.add(p);
                        serversPanel.add(p);
                        p.rescheduleTasks();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            
            XmlDataStore settings = PreferenceManager.getSystemPropertyGroup("Settings");
            mailUser = settings.stringValue("MailUser", "");
            mailPassword = settings.stringValue("MailPassword", "");
            enableMessages = settings.booleanValue("EnableMessages", false);
            setupEMailSession();
            
            int emailCount = settings.intValue("EMailCount", 0);
            emails.clear();
            for(int i=0;i<emailCount;i++){
                emails.add(settings.stringValue("EMail" + i, ""));
            }
        }
        
    }
    
    public void saveSettings(){
        try {
            if(PreferenceManager.checkConfigDir(new File(System.getProperty("user.home") + File.separator + ".inkspot"))) {
                PreferenceManager.getSystemPropertyGroup("Servers").clear();
                PreferenceManager.getSystemPropertyGroup("Servers").add("ServerCount", servers.size());
                for(int i=0;i<servers.size();i++){
                    PreferenceManager.getSystemPropertyGroup("Servers").add("Server" + i, servers.get(i));
                }
                
                PreferenceManager.getSystemPropertyGroup("Settings").add("MailUser", mailUser);
                PreferenceManager.getSystemPropertyGroup("Settings").add("MailPassword", mailPassword);
                PreferenceManager.getSystemPropertyGroup("Settings").add("EnableMessages", enableMessages);
                PreferenceManager.getSystemPropertyGroup("Settings").add("EMailCount", emails.size());
                for(int i=0;i<emails.size();i++){
                    PreferenceManager.getSystemPropertyGroup("Settings").add("EMail" + i, emails.get(i));
                }
            }
            PreferenceManager.saveProperties();
            setupEMailSession();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        serversPanel = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        serversPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        getContentPane().add(serversPanel, java.awt.BorderLayout.CENTER);

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/application_osx_add.png"))); // NOI18N
        jButton1.setText("Add Server...");
        jButton1.setToolTipText("Add a new monitored server");
        jButton1.setBorderPainted(false);
        jButton1.setFocusable(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pencil.png"))); // NOI18N
        jButton2.setText("Settings...");
        jButton2.setToolTipText("General dashboard settings");
        jButton2.setBorderPainted(false);
        jButton2.setFocusable(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/upload.png"))); // NOI18N
        jButton3.setText("Deploy to all...");
        jButton3.setToolTipText("Deploy a file to all servers");
        jButton3.setBorderPainted(false);
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(jButton3);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        saveSettings();
    }//GEN-LAST:event_formWindowClosing

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ServerPanel p = new ServerPanel(this);

        ConfigServerDialog dialog = new ConfigServerDialog(this, p);
        dialog.setVisible(true);
        if(dialog.isAccepted()){
            servers.add(p);
            serversPanel.add(p);
            saveSettings();
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Preferences dialog = new Preferences(this, this);
        dialog.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Dashboard().setVisible(true);
            }
        });
    }
    
    private class SendStatusTask extends TimerTask {
        public void run(){
            for(ServerPanel p : servers){
                if(p.isSendStatusReport()){
                    p.sendSummaryReport();
                }
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel serversPanel;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.applications.dashboard;

import com.connexience.server.util.HexUtils;
import com.connexience.server.util.SignatureUtils;
import java.awt.Dimension;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.naming.Context;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 *
 * @author hugo
 */
public class ServerPanel extends javax.swing.JPanel implements XmlStorable {
    String hostLabel = "Host";
    String serverUrl = "http://localhost:8080/beta";
    long pingCount = 0;
    long totalPings = 0;
    double uptimePercent = 0;
    int engineCount = 0;
    boolean previouslyUp = false;
    boolean previousWorkflowEnginesOk = false;
    boolean previousStorageOk = false;
    private String rootPassword = "";
    Dashboard parentDashboard;
    
    Timer timer = new Timer(true);
    
    PingTask pingerTask;
    CountEnginesTask counterTask;
    BasicValidateTask validateTask;
    ReidentifyEnginesTask reidentifyTask;
    
    int pingInterval = 60;
    int countEnginesInterval = 600;
    int validateInterval = 3600;
    int reidentiftInterval = 3600;
            
    int uptimeWarning = 97;
    int uptimeError = 95;
    int enginesWarning = 0;
    int enginesError = 1;
    
    boolean sendStatusReport = false;
    boolean sendServerFailed = false;
    boolean sendOtherErrors = false;
    
    /**
     * Creates new form ServerPanel
     */
    public ServerPanel(Dashboard parentDashboard, String hostLabel, String serverUrl) {
        initComponents();
        this.hostLabel = hostLabel;
        this.serverUrl = serverUrl;    
        this.parentDashboard = parentDashboard;
        setupSize();
        refreshUI();
    }

    public ServerPanel(Dashboard parentDashboard) {
        this.parentDashboard = parentDashboard;        
        initComponents();
        setupSize();
        refreshUI();
    }
    
    public ServerPanel(){
        initComponents();
        setupSize();
    }
    
    private void setupSize(){
        setPreferredSize(new Dimension(300, 300));
        setMaximumSize(new Dimension(300, 300));
        setMinimumSize(new Dimension(300, 300));
        setSize(new Dimension(300, 300));        
    }
    
    public void refreshUI(){
        hostnameLabel.setText(hostLabel);
        urlLabel.setText(serverUrl);
    }

    public void setParentDashboard(Dashboard parentDashboard) {
        this.parentDashboard = parentDashboard;
    }
    
    public void cancelTasks(){
        if(pingerTask!=null){
            pingerTask.cancel();
            pingerTask = null;
        }
        
        if(counterTask!=null){
            counterTask.cancel();
            counterTask = null;
        }
        
        if(validateTask!=null){
            validateTask.cancel();
            validateTask = null;
        }
        
        if(reidentifyTask!=null){
            reidentifyTask.cancel();
            reidentifyTask = null;
        }
        timer.purge();        
    }
    
    public void rescheduleTasks(){
        cancelTasks();
        pingerTask = new PingTask(this);
        timer.scheduleAtFixedRate(pingerTask, 250, pingInterval * 1000);
        
        counterTask = new CountEnginesTask(this);
        timer.scheduleAtFixedRate(counterTask, 2500, countEnginesInterval * 1000);
        
        validateTask = new BasicValidateTask(this);
        timer.scheduleAtFixedRate(validateTask, 1500, validateInterval * 1000);
        
        reidentifyTask = new ReidentifyEnginesTask(this);
        timer.scheduleAtFixedRate(reidentifyTask, 0, reidentiftInterval * 1000);        
    }
    
    /** Get a connection */
    public HttpURLConnection getConnection(String methodName) throws Exception {
        URL u = new URL(serverUrl + "/servlets/monitoring?method=" + methodName);
        return (HttpURLConnection)u.openConnection();
    }

    public String getHostLabel() {
        return hostLabel;
    }

    public void setHostLabel(String hostLabel) {
        this.hostLabel = hostLabel;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public int getPingInterval() {
        return pingInterval;
    }

    public void setPingInterval(int pingInterval) {
        this.pingInterval = pingInterval;
    }

    public String getRootPassword() {
        return rootPassword;
    }

    public void setRootPassword(String rootPassword) {
        this.rootPassword = rootPassword;
    }

    public void setSendOtherErrors(boolean sendOtherErrors) {
        this.sendOtherErrors = sendOtherErrors;
    }

    public boolean isSendOtherErrors() {
        return sendOtherErrors;
    }

    public void setSendServerFailed(boolean sendServerFailed) {
        this.sendServerFailed = sendServerFailed;
    }

    public boolean isSendServerFailed() {
        return sendServerFailed;
    }

    public void setSendStatusReport(boolean sendStatusReport) {
        this.sendStatusReport = sendStatusReport;
    }

    public boolean isSendStatusReport() {
        return sendStatusReport;
    }

    public int getCountEnginesInterval() {
        return countEnginesInterval;
    }

    public void setCountEnginesInterval(int countEnginesInterval) {
        this.countEnginesInterval = countEnginesInterval;
    }

    public int getValidateInterval() {
        return validateInterval;
    }

    public void setValidateInterval(int validateInterval) {
        this.validateInterval = validateInterval;
    }

    public int getReidentiftInterval() {
        return reidentiftInterval;
    }

    public void setReidentiftInterval(int reidentiftInterval) {
        this.reidentiftInterval = reidentiftInterval;
    }

    public int getUptimeWarning() {
        return uptimeWarning;
    }

    public void setUptimeWarning(int uptimeWarning) {
        this.uptimeWarning = uptimeWarning;
    }

    public int getUptimeError() {
        return uptimeError;
    }

    public void setUptimeError(int uptimeError) {
        this.uptimeError = uptimeError;
    }

    public int getEnginesWarning() {
        return enginesWarning;
    }

    public void setEnginesWarning(int enginesWarning) {
        this.enginesWarning = enginesWarning;
    }

    public int getEnginesError() {
        return enginesError;
    }

    public void setEnginesError(int enginesError) {
        this.enginesError = enginesError;
    }
    
    
    
    public void resetStats(){
        pingCount = 0;
        totalPings = 0;
        uptimePercent = 0.0;
        displayUptime();
    }

    private void displayUptime(){
        if(totalPings>0){
            uptimePercent = (((double)pingCount / (double)totalPings) * 100);
        } else {
            uptimePercent = 0;
        }
                        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                uptimeLabel.setText((int)uptimePercent + "%");
                
                if(uptimePercent<=uptimeError){
                    uptimeLabel.setForeground(DashboardColors.uptimeErrorColor);
                } else if(uptimePercent>uptimeError && uptimePercent<=uptimeWarning){
                    uptimeLabel.setForeground(DashboardColors.uptimeWarningColor);
                } else {
                    uptimeLabel.setForeground(DashboardColors.uptimeOkColor);
                }
            }
        });
    }

    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("ServerPanel");
        store.add("ServerURL", serverUrl);
        store.add("HostLabel", hostLabel);
        store.add("Uptime", uptimePercent);
        store.add("PingInterval", pingInterval);
        store.add("ValidateInterval", validateInterval);
        store.add("ReidentifyInterval", reidentiftInterval);
        store.add("CountEnginesInterval", countEnginesInterval);
        store.add("PreviouslyUp", previouslyUp);
        store.add("PreviousWorkflowEnginesOk", previousWorkflowEnginesOk);
        store.add("PreviousStorageOk", previousStorageOk);
        store.add("UptimeWarning", uptimeWarning);
        store.add("UptimeError", uptimeError);
        store.add("EnginesWarning", enginesWarning);
        store.add("EnginesError", enginesError);
        store.add("SendStatusReport", sendStatusReport);
        store.add("SendServerFailed", sendServerFailed);
        store.add("SendOtherErros", sendOtherErrors);
        store.add("RootPassword", rootPassword);
        return store;
    }

    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        serverUrl = store.stringValue("ServerURL", "");
        hostLabel = store.stringValue("HostLabel", "");
        uptimePercent = store.doubleValue("Uptime", 0);
        pingInterval = store.intValue("PingInterval", 10);
        validateInterval = store.intValue("ValidateInterval", 20);
        reidentiftInterval = store.intValue("ReidentifyInterval", 60);
        countEnginesInterval = store.intValue("CountEnginesInterval", 10);
        previouslyUp = store.booleanValue("PreviouslyUP", false);
        uptimeWarning = store.intValue("UptimeWarning", 97);
        uptimeError = store.intValue("UptimeError", 95);
        enginesWarning = store.intValue("EnginesWarning", 0);
        enginesError = store.intValue("EnginesError", 1);
        sendStatusReport = store.booleanValue("SendStatusReport", false);
        sendServerFailed = store.booleanValue("SendServerFailed", false);
        sendOtherErrors = store.booleanValue("SendOtherErrors", false);
        previousWorkflowEnginesOk = store.booleanValue("PreviousWorkflowEnginesOk", false);
        previousStorageOk = store.booleanValue("PreviousStorageOk", false);
        rootPassword = store.stringValue("RootPassword", "");
    }

    public Dashboard getParentDashboard() {
        return parentDashboard;
    }
    
    public void sendSummaryReport() {
        if(sendStatusReport){
            try {
                HttpURLConnection c = getConnection("report");
                c.connect();
                InputStream inStream = c.getInputStream();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ServerPanel.copyInputStream(inStream, outStream);
                outStream.flush();
                outStream.close();
                inStream.close();
                parentDashboard.sendMessage(this, new String(outStream.toByteArray()));
            } catch (Exception e){
                parentDashboard.sendMessage(this, "Error generating report: " + e.getMessage());
            }         
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        hostnameLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        enginesLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        storageLabel = new javax.swing.JLabel();
        uptimeLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        urlLabel = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        resetStatsButton = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setSize(new java.awt.Dimension(200, 200));
        setLayout(new java.awt.BorderLayout());

        hostnameLabel.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        hostnameLabel.setText("jLabel1");
        hostnameLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        hostnameLabel.setOpaque(true);
        add(hostnameLabel, java.awt.BorderLayout.NORTH);

        jLabel1.setText("Uptime:");

        jLabel2.setText("Engines:");

        enginesLabel.setText("Waiting...");
        enginesLabel.setOpaque(true);

        jLabel3.setText("Storage:");

        storageLabel.setText("Waiting...");
        storageLabel.setOpaque(true);

        uptimeLabel.setFont(new java.awt.Font("Lucida Grande", 0, 48)); // NOI18N
        uptimeLabel.setText("50%");

        jLabel4.setText("URL:");

        urlLabel.setText("URL");
        urlLabel.setOpaque(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(uptimeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                .addGap(80, 80, 80))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(urlLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(storageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(enginesLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(12, 12, 12))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(uptimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jLabel1)))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(enginesLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(storageLabel)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(urlLabel))
                .addContainerGap(190, Short.MAX_VALUE))
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pencil.png"))); // NOI18N
        jButton3.setToolTipText("Edit server settings");
        jButton3.setBorderPainted(false);
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/document_letter_blank.png"))); // NOI18N
        jButton4.setToolTipText("Generate status report");
        jButton4.setBorderPainted(false);
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        resetStatsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/document_letter_new.png"))); // NOI18N
        resetStatsButton.setBorderPainted(false);
        resetStatsButton.setFocusable(false);
        resetStatsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        resetStatsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        resetStatsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetStatsButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(resetStatsButton);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/refresh.png"))); // NOI18N
        jButton5.setToolTipText("Refresh data");
        jButton5.setBorderPainted(false);
        jButton5.setFocusable(false);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/remove.png"))); // NOI18N
        jButton1.setToolTipText("Remove this server");
        jButton1.setBorderPainted(false);
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/upload.png"))); // NOI18N
        jButton2.setToolTipText("Push a file for deployment");
        jButton2.setBorderPainted(false);
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        add(jToolBar1, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        ConfigServerDialog dialog = new ConfigServerDialog(parentDashboard, this);
        dialog.setVisible(true);
        if(dialog.isAccepted()){
            parentDashboard.saveSettings();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        ReportDialog dialog = new ReportDialog(parentDashboard, this);
        dialog.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        rescheduleTasks();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        parentDashboard.removeServer(this);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void resetStatsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetStatsButtonActionPerformed
        resetStats();
        displayUptime();
    }//GEN-LAST:event_resetStatsButtonActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            JFileChooser chooser = new JFileChooser();
            chooser.showOpenDialog(this);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setMultiSelectionEnabled(false);
            
            if(chooser.getSelectedFile()!=null){
                SwingUtilities.invokeLater(new Runnable(){
                    public void run(){jButton2.setEnabled(false);}
                });
                
                File f = chooser.getSelectedFile();
                
                // Work out the hash
                String salt = getHashedPassword(rootPassword);
                String hash = SignatureUtils.getFileHash(f, salt);
                
                HttpURLConnection c = getConnection("deploy");
                c.setDoOutput(true);
                c.setRequestMethod("POST");
                c.setFixedLengthStreamingMode(f.length());
                c.addRequestProperty("filename", f.getName());
                c.addRequestProperty("hash", hash);
                c.connect();

                FileInputStream inStream = new FileInputStream(f);
                OutputStream outStream = c.getOutputStream();
                copyInputStream(inStream, outStream);
                inStream.close();
                outStream.flush();
                outStream.close();
            }
            
        } catch (Exception e){
            e.printStackTrace();
        }
        
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){jButton2.setEnabled(true);}
        });        
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel enginesLabel;
    private javax.swing.JLabel hostnameLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton resetStatsButton;
    private javax.swing.JLabel storageLabel;
    private javax.swing.JLabel uptimeLabel;
    private javax.swing.JLabel urlLabel;
    // End of variables declaration//GEN-END:variables
    
    private abstract class DashboardTask extends TimerTask {
        private ServerPanel server;
        public DashboardTask(ServerPanel server){
            this.server = server;
        }

        public ServerPanel getServer() {
            return server;
        }
    }
    
    private class PingTask extends DashboardTask {

        public PingTask(ServerPanel server) {
            super(server);
        }
        
        public void run(){
            totalPings++;
            try {
                HttpURLConnection c = getConnection("alive");
                c.connect();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ServerPanel.copyInputStream(c.getInputStream(), outStream);
                c.disconnect();
                pingCount++;
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        hostnameLabel.setBackground(DashboardColors.okBackgroundColor);
                        hostnameLabel.setForeground(DashboardColors.okTextColor);
                        displayUptime();
                        previouslyUp = true;
                    }
                });
                
            } catch (Exception e){
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        hostnameLabel.setBackground(DashboardColors.errorBackgroundColor);
                        hostnameLabel.setForeground(DashboardColors.errorTextColor);
                        displayUptime();
                        if(previouslyUp){
                            previouslyUp = false;
                            if(sendServerFailed){
                                parentDashboard.sendMessage(getServer(), "Server: " + hostLabel + " has stopped responding");
                            }
                        }
                    }
                });
            }
        }
    }
    
    private class CountEnginesTask extends DashboardTask {

        public CountEnginesTask(ServerPanel server) {
            super(server);
        }
        
        public void run(){
            try {
                HttpURLConnection c = getConnection("countengines");
                c.connect();
                InputStream inStream = c.getInputStream();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ServerPanel.copyInputStream(inStream, outStream);
                outStream.flush();
                outStream.close();
                inStream.close();
                final String size = new String(outStream.toByteArray());
                engineCount = Integer.parseInt(size);
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        enginesLabel.setText(size);
                        
                        
                        try {
                            int parsedSize = Integer.parseInt(size);
                            if(parsedSize<enginesError){
                                if(sendOtherErrors){
                                    // Send the message
                                    if(!previousWorkflowEnginesOk){
                                        parentDashboard.sendMessage(getServer(), "Workflow engines less than minimum required");
                                    }
                                }
                                previousWorkflowEnginesOk = false;
                                enginesLabel.setBackground(DashboardColors.errorBackgroundColor);
                                enginesLabel.setForeground(DashboardColors.errorTextColor);
                                
                            } else if(parsedSize<enginesWarning){
                                previousWorkflowEnginesOk = true;
                                enginesLabel.setBackground(DashboardColors.warningBackgroundColor);
                                enginesLabel.setForeground(DashboardColors.warningTextColor);
                                
                            } else {
                                previousWorkflowEnginesOk = true;
                                enginesLabel.setBackground(DashboardColors.okBackgroundColor);
                                enginesLabel.setForeground(DashboardColors.okTextColor);
                            }
                            
                        } catch (Exception e){
                            
                        }
                    }
                });
                
                
            } catch (Exception e){
                enginesLabel.setText("ERROR");
            }
        }
    }
    
    private class BasicValidateTask extends DashboardTask {

        public BasicValidateTask(ServerPanel server) {
            super(server);
        }
        
        public void run(){
            try {
                HttpURLConnection c = getConnection("status");
                c.connect();
                InputStream inStream = c.getInputStream();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ServerPanel.copyInputStream(inStream, outStream);
                outStream.flush();
                outStream.close();
                inStream.close();
                final String status = new String(outStream.toByteArray());
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        if(status!=null && status.equalsIgnoreCase("ok")){
                            storageLabel.setBackground(DashboardColors.okBackgroundColor);
                            storageLabel.setForeground(DashboardColors.okTextColor);
                            previousStorageOk = true;
                            
                        } else {
                            storageLabel.setBackground(DashboardColors.errorBackgroundColor);
                            storageLabel.setForeground(DashboardColors.errorTextColor);
                            
                            if(sendOtherErrors){
                                if(!previousStorageOk){
                                    parentDashboard.sendMessage(getServer(), "Encountered a storage problem");
                                }
                            }
                            previousStorageOk = false;
                            
                        }
                        storageLabel.setText(status);
                    }
                });
                
            } catch (Exception e){
                storageLabel.setBackground(DashboardColors.errorBackgroundColor);
                storageLabel.setForeground(DashboardColors.errorTextColor);                
                storageLabel.setText("ERROR");
            }            
        }
    }
    
    private class ReidentifyEnginesTask extends DashboardTask {

        public ReidentifyEnginesTask(ServerPanel server) {
            super(server);
        }
        public void run(){
            try {
                HttpURLConnection c = getConnection("reidentify");
                c.connect();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ServerPanel.copyInputStream(c.getInputStream(), outStream);
                c.disconnect();
            } catch (Exception e){
                parentDashboard.logMessage(getServer(), "Error reidentifying engines");
            }
        }
    }
    
    /** Copy the data from one stream to another */
    public static void copyInputStream(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[4096];
        int len;

        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }

        in.close();
        out.close();
    }    
    
    public static String getHashedPassword(String plainPassword) {
        try {
            //compute the SHA-1 hash of the password
            MessageDigest msgDigest = MessageDigest.getInstance("SHA-1");
            msgDigest.update(plainPassword.getBytes());
            byte rawByte[] = msgDigest.digest();
            return HexUtils.getHexString(rawByte);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }        
}

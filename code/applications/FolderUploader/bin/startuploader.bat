@echo off
setlocal EnableDelayedExpansion

set DIR=%~dp0..\lib
FOR /F %%P IN ('dir /B %DIR%\*.jar') DO (
  set CP=!CP!;%%P
)

pushd %DIR%
java -cp %CP% com.connexience.applications.uploader.SimpleUploader
popd %DIR%

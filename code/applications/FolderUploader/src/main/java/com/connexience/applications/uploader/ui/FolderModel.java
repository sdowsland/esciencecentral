/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.ui;

import com.connexience.api.StorageClient;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.document.*;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.*;
import java.util.*;
import javax.swing.event.TreeModelEvent;

/**
 * This class provides a model that can supply a JTree with a folder structure
 * @author hugo
 */
public class FolderModel implements TreeModel {
    private ArrayList<TreeModelListener> listeners = new ArrayList<TreeModelListener>();
    private StorageClient api;
    private FolderHolder root;
    private boolean includeFiles = false;
    
    public FolderModel(StorageClient api){
        this.api = api;
    }

    public void setIncludeFiles(boolean includeFiles) {
        this.includeFiles = includeFiles;
    }
    
    public StorageClient getApi(){
        return api;
    }
    
    public FolderHolder getRootHolder(){
        return root;
    }
    
    @Override
    public void addTreeModelListener(TreeModelListener tl) {
        listeners.add(tl);
    }

    @Override
    public Object getChild(Object o, int i) {
        if(o instanceof FolderHolder){
            return ((FolderHolder)o).getChildAt(i);
        } else if (o instanceof EscDocument){
            if(includeFiles){
                return o;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public int getChildCount(Object o) {
        if(o instanceof FolderHolder){
            return ((FolderHolder)o).getChildCount();
        } else {
            return 0;
        }
    }

    @Override
    public int getIndexOfChild(Object o, Object o1) {
        if(o instanceof FolderHolder){
            return ((FolderHolder)o).indexOfChild((FolderHolder)o1);
        } else {
            return -1;
        }
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public boolean isLeaf(Object o) {
        if(o instanceof FolderHolder){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void removeTreeModelListener(TreeModelListener tl) {
        listeners.remove(tl);
    }

    @Override
    public void valueForPathChanged(TreePath tp, Object o) {
        
    }
    
    public void refresh(){
        
    }
    

    public EscFolder fetchHomeFolder(){
        try {
            EscFolder home = api.homeFolder();
            if(home!=null){
                root = new FolderHolder(null, home);
                notifyDataFetched(root);
                return root.getFolder();
            } else {
                return null;
            }
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public EscFolder fetchFolder(String id){
        try {
            EscFolder f = api.getFolder(id);
            if(f!=null){
                root = new FolderHolder(null, f);
                notifyDataFetched(root);
                return root.getFolder();
            } else {
                return null;
            }
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }        
    }
    
    public void notifyDataFetched(FolderHolder holder){
        Object[] path = holder.getPathToHere();
        TreeModelEvent evt = new TreeModelEvent(this, path);   
        for(int i=0;i<listeners.size();i++){
            listeners.get(i).treeStructureChanged(evt);
        }
    }
    
    public class FolderHolder {
        private EscFolder folder;
        private ArrayList children = new ArrayList();
        
        private boolean populated = false;
        private FolderHolder parent;
        
        public FolderHolder(FolderHolder parent, EscFolder folder) {
            this.folder = folder;
            this.parent = parent;
        }
        
        private void populate(){
            if(folder!=null){
                try {
                    
                    EscFolder[] contents = api.listChildFolders(folder.getId());
                   
                    EscFolder f;
                    children.clear();
                    for(int i=0;i<contents.length;i++){
                        if(contents[i] instanceof EscFolder){
                            f = (EscFolder)contents[i];
                            if(!folder.getId().equals(f.getId())){
                                children.add(new FolderHolder(this, f));
                            }
                            
                        }
                    }
                    
                    if(includeFiles){
                        EscDocument[] files = api.folderDocuments(folder.getId());
                        for(EscDocument d : files){
                            children.add(d);
                        }
                    }
                    populated = true;
                } catch (Exception e){
                    e.printStackTrace();
                }
                
            }
        }
        
        public EscFolder getFolder(){
            return folder;
        }
        
        public int getChildCount(){
            if(!populated){
                populate();
            }
            return children.size();
        }
        
        public Object getChildAt(int index){
            if(!populated){
                populate();
            }
            return children.get(index);
        }

        @Override
        public String toString() {
            return folder.getName();
        }
        
        public int indexOfChild(Object child){
            if(!populated){
                populate();
            }
            return children.indexOf(child);
        }
        
        /** Get the path to this point */
        public Object[] getPathToHere(){
            ArrayList path = new ArrayList();
            appendToPath(path);
            return path.toArray();
        }

        /** Append this object to a path and mode up the heirarchy */
        public void appendToPath(ArrayList path){
            
            if(parent!=null){
                parent.appendToPath(path);
            }
            path.add(this);
        }        
        
    }
}

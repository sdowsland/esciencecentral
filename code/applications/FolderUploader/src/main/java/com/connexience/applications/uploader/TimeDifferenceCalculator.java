/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;

import com.connexience.api.StorageClient;

/**
 * This class periodically checks the time difference between the local and
 * server times
 * @author hugo
 */
public class TimeDifferenceCalculator extends Thread {
    boolean stopFlag = false;
    long timeInterval = 10000;
    long callCount = 6;
    long callsToSkip = 2;
    long intervalBetweenCalls = 1000;
    long nextCheckTime;
    StorageClient api;
    SimpleUploader parent;
    
    public TimeDifferenceCalculator(SimpleUploader parent, long interval) {
        this.timeInterval = interval;
        this.parent = parent;
        nextCheckTime = System.currentTimeMillis();
        setDaemon(true);
    }
    
    private void createApi(){
        try {
            api = new StorageClient(parent.getDefaultServer(), parent.getDefaultPort(), false, parent.getDefaultUserName(), parent.getDefaultPassword());
        } catch (Exception e){
            parent.logError(null, "Cannot create API: " + e.getMessage());
        }        
    }    
    
    public StorageClient getApi(){
        if(api!=null){
            return api;
        } else {
            createApi();
            return api;
        }
    }    
    
    @Override
    public void run() {
        super.run();
        while(!stopFlag){
            if(System.currentTimeMillis()>nextCheckTime){
                try {
                    long sum = 0;
                    long value;
                    long count = 0;
                    for(int i=0;i<callCount;i++){
                        if(i>callsToSkip){
                            // Ignore first one
                            value = calculateTimeDifference();
                            sum = sum + value;
                            count++;
                            Thread.sleep(intervalBetweenCalls);
                        }
                    }
                    long difference = (long)((double)sum / (double)(count));
                    parent.setDefaultTimeDifference(difference);
                } catch (Exception e){
                    parent.logError(null, "Error calculating time difference: " + e.getMessage());
                }
                nextCheckTime = System.currentTimeMillis() + timeInterval;
            }
            try {
                Thread.sleep(1000);
            } catch(InterruptedException ie){
                stopFlag = true;
            }
        }
    }

    private long calculateTimeDifference() throws Exception {
        long localTime = System.currentTimeMillis();
        long timestamp = getApi().getTimestamp();
        long responseTime = System.currentTimeMillis();
        long roundTripTime = responseTime - localTime;
        long timeDelta = timestamp - (localTime + (roundTripTime / 2));
        return timeDelta;
    }
    
    public void setStopFlag(boolean stopFlag) {
        this.stopFlag = stopFlag;
    }
    
    
}

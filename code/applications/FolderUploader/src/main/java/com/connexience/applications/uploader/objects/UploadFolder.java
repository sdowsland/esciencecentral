/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.objects;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.applications.uploader.RootUploader;
import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;
import com.connexience.applications.uploader.UploadTaskCallback;
import com.connexience.applications.uploader.objects.UploadFile;
import com.connexience.applications.uploader.tasks.*;


import java.util.*;
import java.io.*;

/**
 * Represents a link between a local and remote folder
 * @author hugo
 */
public class UploadFolder extends UploadObject {
    public ArrayList<UploadObject> children = new ArrayList<UploadObject>();

    public UploadFolder(File localFile, RootUploader root) {
        super(localFile, root);
    }
    
    /** Build a list of children */
    public void buildChildList(){
        children.clear();
        File[] contents = localFile.listFiles();
        // Folders first
        for(int i=0;i<contents.length;i++){
            if(!root.getParent().isBannedName(contents[i].getName())){
                if(contents[i].isDirectory()){
                    UploadFolder folder = new UploadFolder(contents[i], root);
                    folder.setContainerId(id);
                    children.add(folder);
                    folder.buildChildList();
                }
            }
        }
        
        // Then documents
        for(int i=0;i<contents.length;i++){
            if(!root.getParent().isBannedName(contents[i].getName())){
                if(!contents[i].isDirectory()){
                    UploadFile file = new UploadFile(contents[i], root, this);
                    file.setContainerId(id);
                    children.add(file);
                }
            }
        }        
    }

    @Override
    public void process() {
        super.process();
        if(!root.getParent().isBannedName(localFile.getName())){
            final UploadFolder f = this;

            // List the contents of the folder on the server
            UploadTaskCallback cb = new UploadTaskCallback() {

                @Override
                public void taskSucceeded(UploadTask task) {
                    // For each document that is not on the server, make the document
                    // here do its upload
                    ListFolderContents results = (ListFolderContents)task;
                    Object remoteObject;

                    // Files that are here
                    if(root.isUploadEnabled()){
                        for(UploadObject obj : children){
                            if(!results.containsNamedChild(obj.getLocalFile().getName())){
                                // Not on server
                                if(obj instanceof UploadFile){
                                    // Local object is a file
                                    obj.setContainerId(id);
                                    obj.process();
                                } else {
                                    // Local object is a folder
                                    ((UploadFolder)obj).createRemoteFolderThenProcess(id);
                                }
                            } else {
                                remoteObject = results.getChildByName(obj.getLocalFile().getName());
                                if(remoteObject instanceof EscDocument){
                                    if(obj instanceof UploadFile){
                                        // Get the ID of the remote file 
                                        ((UploadFile)obj).setId(((EscDocument)remoteObject).getId());
                                        ((UploadFile)obj).setContainerId(f.getId());                                    
                                        ((UploadFile)obj).setUploadProcessed(true);
                                    } else {
                                        ((UploadFolder)obj).setId(((EscDocument)remoteObject).getId());
                                        ((UploadFolder)obj).setContainerId(f.getId());                                
                                        ((UploadFolder)obj).process();
                                    }

                                } else if(remoteObject instanceof EscFolder){
                                    if(obj instanceof UploadFile){
                                        // Get the ID of the remote file 
                                        ((UploadFile)obj).setId(((EscFolder)remoteObject).getId());
                                        ((UploadFile)obj).setContainerId(f.getId());                                    
                                        ((UploadFile)obj).setUploadProcessed(true);
                                    } else {
                                        ((UploadFolder)obj).setId(((EscFolder)remoteObject).getId());
                                        ((UploadFolder)obj).setContainerId(f.getId());                                
                                        ((UploadFolder)obj).process();
                                    }

                                }
                            }
                        }
                    }
                    
                    // Files that are not here
                    if(root.isDownloadEnabled()){
                        downloadServerDocuments(results);   
                        dowloadServerFolders(results);
                    }
                }

                @Override
                public void taskFailed(UploadTask task, String message) {
                    root.logError(message);
                }
            };

            ListFolderContents task = new ListFolderContents(this, id);
            root.queueTask(task, cb);
        }
    }
    
    /** Download data from the server */
    public void downloadServerDocuments(ListFolderContents results){
        // Check remote documents
        for(int i=0;i<results.getDocumentCount();i++){
            EscDocument serverDoc = results.getDocument(i);
            if(getChildByName(serverDoc.getName())==null){
                // Not a file that we have here
                UploadFile fileToDownload = new UploadFile(new File(localFile, serverDoc.getName()), root, this);
                children.add(fileToDownload);
                fileToDownload.downloadFromServer(serverDoc);

            } else if(getChildByName(serverDoc.getName()) instanceof UploadFile){
                // A file that we do have - download if there is a newer remote version
                if(root.isOverwriteWithNewer()){
                    UploadFile fileToDownload = (UploadFile)getChildByName(serverDoc.getName());
                    fileToDownload.downloadFromServer(serverDoc);
                }
            }
        }        
    }
    
    /** Download the server folders */
    public void dowloadServerFolders(ListFolderContents results){
        // Go through the folders
        for(int i=0;i<results.getFolderCount();i++){
            EscFolder serverFolder = results.getFolder(i);
            if(getChildByName(serverFolder.getName())==null){
                // Not a local folder that we have here
                UploadFolder folderToDownload = new UploadFolder(new File(localFile, serverFolder.getName()), root);
                children.add(folderToDownload);
                folderToDownload.createLocalFolderThenProcess(serverFolder);
            }
        }
    }
            
    /** Get a local document by name */
    public UploadObject getChildByName(String name){
        for(UploadObject child : children){
            if(child.getLocalFile().getName().equals(name)){
                return child;
            }
        }
        return null;
    }
    
    /** Remove the record of a child document */
    public void removeChildDocument(File childFile){
        UploadObject child = getChildByName(childFile.getName());
        if(child instanceof UploadFile){
            children.remove(child);
        }
    }
    
    /** Get the number of children */
    public int getChildCount(){
        return children.size();
    }
    
    /** Get a child */
    public UploadObject getChild(int index){
        return children.get(index);
    }
    
    /** Is a file name present in the local list */
    public boolean containsChildFile(String name){
        UploadObject child = getChildByName(name);
        if(child!=null){
            if(child instanceof UploadFile){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /** Get the index of a child */
    public int getIndexOfChild(UploadObject child){
        return children.indexOf(child);
    }
    
    /** Is a file name present in the local list */
    public boolean containsChildDirectory(String name){
        UploadObject child = getChildByName(name);
        if(child!=null){
            if(child instanceof UploadFolder){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }
    
    /** Create the local folder then process the server contents */
    public void createLocalFolderThenProcess(EscFolder serverFolder){
        if(!localFile.exists()){
            localFile.mkdir();
            root.logMessage("Created folder: " + localFile.getName());
        }
        
        // List the contents of the remote folder then do the downloads
        UploadTaskCallback cb = new UploadTaskCallback() {

            @Override
            public void taskSucceeded(UploadTask task) {
                root.logMessage("Listed remote files: ");
                ListFolderContents results = (ListFolderContents)task;
                downloadServerDocuments(results);
                dowloadServerFolders(results);
            }

            @Override
            public void taskFailed(UploadTask task, String message) {
                root.logError("Error listing remote files: " + message);
            }
        };
        
        id = serverFolder.getId();
        containerId = serverFolder.getContainerId();
        
        ListFolderContents task = new ListFolderContents(this, serverFolder.getId());
        root.queueTask(task, cb);
    }
    
    /** Create the remote folder for this local folder and perform the next steps */
    public void createRemoteFolderThenProcess(String parentFolderId){
        final UploadFolder f = this;
        UploadTaskCallback cb = new UploadTaskCallback() {

            @Override
            public void taskSucceeded(UploadTask task) {
                EscFolder createdFolder = ((CreateFolder)task).getCreatedFolder();
                
                // Set the container ID of everything
                f.setId(createdFolder.getId());
                f.setContainerId(createdFolder.getContainerId());
                f.setName(createdFolder.getName());
                
                for(UploadObject o : f.children){
                    o.setContainerId(f.getId());
                }
                f.process();
            }

            @Override
            public void taskFailed(UploadTask task, String message) {
                root.logError("Failed to create folder: " + message);
            }
        };
        
        CreateFolder task = new CreateFolder(this, parentFolderId, localFile.getName());
        root.queueTask(task, cb);
    }

    @Override
    public void purgeTasksFromQueue() {
        super.purgeTasksFromQueue();
        
        for(UploadObject child : children){
            child.purgeTasksFromQueue();
        }
    }

    @Override
    public void clear() {
        for(UploadObject child : children){
            child.clear();
        }
        children.clear();
        super.clear();
    }
    
    /** Check to see if there are any new files */
    @Override
    public void scanForChanges() {
        File[] newFiles = localFile.listFiles();
        ArrayList<UploadObject>objectsToAdd = new ArrayList<UploadObject>();
        
        if(newFiles!=null){
            // Folders first
            for(int i=0;i<newFiles.length;i++){
                if(!root.getParent().isBannedName(newFiles[i].getName())){
                    if(newFiles[i].isDirectory()){
                        // Check to see if this is a new directory
                        if(!containsChildDirectory(newFiles[i].getName())){
                            UploadFolder upFolder = new UploadFolder(newFiles[i], root);
                            upFolder.setContainerId(id);
                            upFolder.buildChildList();
                            objectsToAdd.add(upFolder);
                            upFolder.createRemoteFolderThenProcess(id);
                        }
                    }
                }
            }
            
            // Then documents
            for(int i=0;i<newFiles.length;i++){
                if(!root.getParent().isBannedName(newFiles[i].getName())){
                    if(newFiles[i].isFile()){
                        // Check to see if this is a new file
                        if(!containsChildFile(newFiles[i].getName())){
                            UploadFile upFile = new UploadFile(newFiles[i], root, this);
                            upFile.setContainerId(id);
                            objectsToAdd.add(upFile);
                            upFile.process();
                        }
                    }
                }
            }
        }
        
        // Go through the exising subfolders and scan those for changes
        for(UploadObject obj : children){
            if(obj instanceof UploadFolder){
                ((UploadFolder)obj).scanForChanges();
            }
        }
        children.addAll(objectsToAdd);
    }
    
    /** Have all of the child files been processed */
    public boolean allFilesProcessed(){
        boolean done = true;
        for(UploadObject obj : children){
            if(obj instanceof UploadFile){
                if(!((UploadFile)obj).isUploadProcessed()){
                    done = false;
                }
            } else {
                if(!((UploadFolder)obj).allFilesProcessed()){
                    done = false;
                }
            }
        }
        return done;
    }
    
    /** Does this folder have the correct structure for a service */
    public boolean hasBlockStructure(){
        if(getChildByName("src") instanceof UploadFolder){
            // There is a source folder
            boolean blkFound = false;
            for(UploadObject obj : children){
                if(obj instanceof UploadFile){
                    if(obj.getName().endsWith(".blk")){
                        // There is a .blk file
                        // Try and parse it
                        
                        blkFound = true;
                        
                    }
                }
            }
            return blkFound;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if(localFile!=null){
            return localFile.getName();
        } else {
            return "NO NAME";
        }
    }
    
}
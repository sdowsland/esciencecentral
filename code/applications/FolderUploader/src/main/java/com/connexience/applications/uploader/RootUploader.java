/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;
import com.connexience.applications.uploader.objects.UploadFolder;
import com.connexience.applications.uploader.tasks.ScanChanges;
import com.connexience.applications.uploader.tasks.UpdateLocalFilesFromServer;
import com.connexience.api.*;

import org.pipeline.core.xmlstorage.*;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.swing.table.TableModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 * This class represents a root folder
 * @author hugo
 */
public class RootUploader implements Runnable, XmlStorable, TableModel {
    private boolean stopFlag = false;
    private UploadFolder folder;
    private StorageClient api;
    private File targetFolder;
    private ConcurrentLinkedQueue<UploadTask> taskQueue = new ConcurrentLinkedQueue<UploadTask>();
    private ArrayList<UploadTask> taskList = new ArrayList<UploadTask>();
    private Thread uploadThread;
    private String serverFolderId;
    private int scanChangeInterval = 10;
    private long lastScanChangeTime = System.currentTimeMillis();
    private SimpleUploader parent;
    private ArrayList<TableModelListener> tableListeners = new ArrayList<TableModelListener>();
    private boolean projectUpload = false;
    private String projectId = "";
    private boolean downloadEnabled = false;
    private boolean uploadEnabled = true;
    private boolean overwriteWithNewer = false;
    private int downloadInterval = 60;
    private long lastDownloadTime = System.currentTimeMillis();
    
    public RootUploader() {
    }
    
    
    public RootUploader(File targetFolder, String serverFolderId) {
        this.targetFolder = targetFolder;
        this.serverFolderId = serverFolderId;
    }

    public void configureFromParent(){
        scanChangeInterval = parent.getDefaultScanInterval();
        downloadInterval = parent.getDefaultDownloadInterval();
    }
    
    public boolean isOverwriteWithNewer() {
        return overwriteWithNewer;
    }

    public void setOverwriteWithNewer(boolean overwriteWithNewer) {
        this.overwriteWithNewer = overwriteWithNewer;
    }

    public int getDownloadInterval() {
        return downloadInterval;
    }

    public void setDownloadInterval(int downloadInterval) {
        this.downloadInterval = downloadInterval;
    }
    
    public boolean isDownloadEnabled() {
        return downloadEnabled;
    }

    public boolean isUploadEnabled() {
        return uploadEnabled;
    }

    public void setDownloadEnabled(boolean downloadEnabled) {
        this.downloadEnabled = downloadEnabled;
    }

    public void setUploadEnabled(boolean uploadEnabled) {
        this.uploadEnabled = uploadEnabled;
    }

    public boolean isProjectUpload() {
        return projectUpload;
    }

    public void setProjectUpload(boolean projectUpload) {
        this.projectUpload = projectUpload;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setTargetFolder(File targetFolder) {
        this.targetFolder = targetFolder;
    }

    public void setServerFolderId(String serverFolderId) {
        this.serverFolderId = serverFolderId;
    }

    public UploadFolder getFolder(){
        return folder;
    }
    
    public String getServerFolderId() {
        return serverFolderId;
    }

    public int getScanChangeInterval() {
        return scanChangeInterval;
    }

    public void setScanChangeInterval(int scanChangeInterval) {
        this.scanChangeInterval = scanChangeInterval;
    }
    
    public void stopUploading(){
        stopFlag = true;
        folder.purgeTasksFromQueue();
    }
    
    public int communicatingTaskCount(){
        int count =0 ;
        for(UploadTask t : taskList){
            if(!(t instanceof ScanChanges)){
                count++;
            }
            
        }
        return count;
    }
    
    private void createApi(){
        try {
            if(parent.getDefaultPort()==443){
                api = new StorageClient(parent.getDefaultServer(), parent.getDefaultPort(), true, parent.getDefaultUserName(), parent.getDefaultPassword());
            } else {
                api = new StorageClient(parent.getDefaultServer(), parent.getDefaultPort(), false, parent.getDefaultUserName(), parent.getDefaultPassword());
            }
            api.setUploadChunkingEnabled(false);
        } catch (Exception e){
            logError("Cannot create API: " + e.getMessage());
        }        
    }
    
    public StorageClient getApi(){
        if(api!=null){
            return api;
        } else {
            createApi();
            return api;
        }
    }
    public boolean canInit(){
        if(targetFolder!=null && serverFolderId!=null){
            return true;
        } else {
            return false;
        }
    }

    /** Adjust a time to what the server time is expected to be */
    public long adjustToServerTime(long timestamp){
        return timestamp + parent.getDefaultTimeDifference();
    }
    
    public Date adjustToServerTime(Date time){
        return new Date(time.getTime() + parent.getDefaultTimeDifference());
    }
    
    public void setParent(SimpleUploader parent) {
        this.parent = parent;
    }

    public SimpleUploader getParent() {
        return parent;
    }

    public File getTargetFolder() {
        return targetFolder;
    }
    
    /** Force a remote file synchronization */
    public void syncRemote(){
        if(isDownloadEnabled()){
            folder.process();
        }
    }
    
    public void reset() {
        if(canInit()){
            if(folder!=null){
                folder.purgeTasksFromQueue();
                folder.clear();            
            }
            try {
                init();
            } catch (Exception e){
                logError("Error starting uploader: " + e.getMessage());
            }
        }
    }
    
    public void init() throws Exception {
        createApi();
        folder = new UploadFolder(targetFolder, this);
        folder.setId(serverFolderId);
        folder.buildChildList();
        if(uploadThread==null){
            uploadThread = new Thread(this);
            //uploadThread.setDaemon(true);
            uploadThread.start();
        }
        folder.process();
    }
    
    public void purgeTasksForObject(UploadObject source){
        ArrayList<UploadTask> tasksToRemove = new ArrayList<UploadTask>();
        for(UploadTask task : taskQueue){
            if(task.getSource()!=null && task.getSource().equals(source)){
                tasksToRemove.add(task);
            }
        }
        
        for(UploadTask task : tasksToRemove){
            taskQueue.remove(task);
        }
    }
    
    private void notifyTasksChanged(){
        parent.taskStatusChanged(this);
        for(TableModelListener l : tableListeners){
            l.tableChanged(new TableModelEvent(this));
        }
    }
    
    public void queueTask(UploadTask task, UploadTaskCallback callback){
        task.setApi(api);
        task.setCallback(callback);
        task.setRoot(this);
        taskQueue.add(task);
        taskList.add(task);
        notifyTasksChanged();
    }
    
    public void run(){
        UploadTaskCallback cb = new UploadTaskCallback() {

            @Override
            public void taskSucceeded(UploadTask task) {
                if(folder.hasBlockStructure()){
                    logMessage("This root uploader looks like a workflow block");
                }
            }

            @Override
            public void taskFailed(UploadTask task, String message) {

            }
        };
        
        UploadTaskCallback downloadFilesCallback = new UploadTaskCallback() {

            @Override
            public void taskSucceeded(UploadTask task) {
            }

            @Override
            public void taskFailed(UploadTask task, String message) {
                logError("Download error: " + message);
            }
        };
                
        
        while(stopFlag==false){
            UploadTask task;
            while(taskQueue.size()>0){
                task = taskQueue.poll();
                if(task!=null){
                    try {
                        task.run();
                    } catch (Exception e){
                        logError(e.getMessage());
                    }
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ie){
                    stopFlag = true;
                }
            }
            
            // Check to see if there are any new files
            if(taskQueue.size()==0 && folder.allFilesProcessed()){
                if(System.currentTimeMillis() > (lastScanChangeTime + (1000 * scanChangeInterval))){
                    lastScanChangeTime = System.currentTimeMillis();
                    ScanChanges scanTask = new ScanChanges(null, folder);
                    queueTask(scanTask, cb);
                }
            }
            
            // Check for remote files to sync
            if(taskQueue.size()==0 && downloadEnabled){
                if(System.currentTimeMillis()>(lastDownloadTime + (1000 * downloadInterval))){
                    lastDownloadTime = System.currentTimeMillis();
                    UpdateLocalFilesFromServer updateTask = new UpdateLocalFilesFromServer(folder, folder);
                    queueTask(updateTask, downloadFilesCallback);
                }
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie){
                stopFlag = true;
            }
        }
    }
    
    public void taskFinished(UploadTask task){
        taskList.remove(task);
        notifyTasksChanged();
    }
    
    public void taskStarted(UploadTask task){
        notifyTasksChanged();
    }
    
    /** Log an error message */
    public void logError(String message){
        if(parent!=null){
            parent.logError(this, message);
        } else {
            System.err.println(message);
        }
    }

    /** Log a message */
    public void logMessage(String message){
        if(parent!=null){
            parent.logMessage(message);
        } else {
            System.out.println(message);
        }
    }
    

    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("RootUploader");
        store.add("ScanChangeInterval", scanChangeInterval);
        store.add("ServerFolderID", serverFolderId);
        store.add("TargetFolder", targetFolder);
        store.add("ProjectUpload", projectUpload);
        store.add("ProjectID", projectId);
        store.add("DownloadFromServer", downloadEnabled);
        store.add("UploadToServer", uploadEnabled);
        store.add("OverwriteWithNewerFromServer", overwriteWithNewer);
        store.add("DownloadInterval", downloadInterval);
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        scanChangeInterval = store.intValue("ScanChangeInterval", 60);
        serverFolderId = store.stringValue("ServerFolderID", null);
        if(store.containsName("TargetFolder")){
            targetFolder = store.fileValue("TargetFolder");
        } else {
            targetFolder = null;
        }
        projectUpload = store.booleanValue("ProjectUpload", false);
        projectId = store.stringValue("ProjectID", "");
        overwriteWithNewer = store.booleanValue("OverwriteWithNewerFromServer", false);
        downloadEnabled = store.booleanValue("DownloadFromServer", false);
        uploadEnabled = store.booleanValue("UploadToServer", uploadEnabled);
        downloadInterval = store.intValue("DownloadInterval", 60);
    }

    // Table model code

    @Override
    public void addTableModelListener(TableModelListener l) {
        tableListeners.add(l);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex){
            case 0:
                return "Type";
            case 1:
                return "Name";
            case 2:
                return "Status";
            default:
                return "N/A";
        }
    }

    @Override
    public int getRowCount() {
        return taskList.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        UploadTask task = taskList.get(rowIndex);
        switch(columnIndex){
            case 0:
                return task.getTaskName();
            case 1:
                return task.getTaskDescription();
            case 2:
                switch(task.getStatus()){
                    case TASK_FAILED:
                        return "Failed";
                    case TASK_OK:
                        return "Ok";
                    case TASK_RUNNING:
                        return "Running";
                    case TASK_WAITING:
                        return "Waiting";
                    default:
                        return "N/A";
                }
            default:
                return "N/A";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        tableListeners.remove(l);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
    }
    
}
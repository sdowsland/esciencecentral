/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;

import com.connexience.applications.uploader.ui.UploaderSystemTray;

import org.pipeline.core.xmlstorage.*;
import org.pipeline.core.xmlstorage.prefs.*;
import org.pipeline.gui.xmlstorage.prefs.*;
import java.awt.SystemTray;
import java.util.*;
import javax.swing.UIManager;
/**
 * Main class to manage uploads to the server
 * @author hugo
 */
public class SimpleUploader implements XmlStorable {
    /** System tray icon */
    private UploaderSystemTray systemTray;

    /** Default server */
    private String defaultServer = "xpsnexus.ncl.ac.uk";
    
    /** Default port */
    private int defaultPort = 80;
    
    /** Secure connection */
    private boolean defaultSecure = false;
    
    /** Default password */
    private String defaultPassword = "";
    
    /** Default user name */
    private String defaultUserName = "";
    
    /** Default scan interval */
    private int defaultScanInterval = 10;
    
    /** Show log messages */
    private boolean defaultShowMessages = true;
    
    /** Time difference between here and the server */
    private long defaultTimeDifference = 0;
    
    /** Length of time between time checks */
    private long defaultTimeCheckInterval = 3600000;
    
    private int defaultDownloadInterval = 600;

    /** Amount of time to wait for file size to settle */
    private int defaultSettlingTime = 1000;
    
    /** List of banned names */
    private ArrayList<String> bannedNames = new ArrayList<String>();
    
    /** Thread for checking time difference */
    private TimeDifferenceCalculator timeChecker;

    /** Error manager to store error messages */
    private ErrorManager errorManager = new ErrorManager();
    
    /** Uploaders */
    ArrayList<RootUploader> uploaders = new ArrayList<RootUploader>();
    public SimpleUploader() {
        bannedNames.add(".svn");
        bannedNames.add(".DS_Store");
        timeChecker = new TimeDifferenceCalculator(this, defaultTimeCheckInterval);
        if(SystemTray.isSupported()){
            systemTray = new UploaderSystemTray(this);
        }
    }
    
    public boolean isBannedName(String name){
        return bannedNames.contains(name);
    }
    
    /** Add an uploader */
    public void addUploader(RootUploader uploader){
        uploader.setParent(this);
        uploaders.add(uploader);
    }
    
    /** Remove an uploader */
    public void removeUploader(RootUploader uploader){
        if(uploaders.contains(uploader)){
            uploaders.remove(uploader);
            uploader.stopUploading();
            try {
                save();
            } catch (Exception e){
                logError(null, "Error removing uploader: " + e.getMessage());
            }
        }
        updateSystemTray();
    }
    
    public int communicatingTaskCount(){
        int count = 0;
        for(RootUploader u : uploaders){
            count = count + u.communicatingTaskCount();
        }
        return count;
    }
    
    /** One of the uploaders task status has changed */
    public void taskStatusChanged(RootUploader uploader){
        if(systemTray!=null){
            systemTray.updateIcon();
        }
    }
    
    public boolean containsUploader(RootUploader uploader){
        return uploaders.contains(uploader);
    }
    
    /** Start the uploader */
    public void start(){
        for(int i=0;i<uploaders.size();i++){
            if(uploaders.get(i).canInit()){
                try {
                    uploaders.get(i).init();
                } catch (Exception e){
                    logError(null, "Error starting uploader: " + uploaders.get(i).getTargetFolder().getPath());
                }
            }
        }
        timeChecker = new TimeDifferenceCalculator(this, defaultTimeCheckInterval);
        timeChecker.start();
    }

    public void updateSystemTray(){
        if(systemTray!=null){
            systemTray.createUploadersMenu();
        }
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public String getDefaultUserName() {
        return defaultUserName;
    }
    
    public int getUploaderCount(){
        return uploaders.size();
    }

    public void setDefaultTimeDifference(long defaultTimeDifference) {
        this.defaultTimeDifference = defaultTimeDifference;
        if(systemTray!=null){
            systemTray.timeDifferenceChanged();
        }
    }

    public long getDefaultTimeDifference() {
        return defaultTimeDifference;
    }

    public int getDefaultDownloadInterval() {
        return defaultDownloadInterval;
    }

    public void setDefaultDownloadInterval(int defaultDownloadInterval) {
        this.defaultDownloadInterval = defaultDownloadInterval;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public void setDefaultUserName(String defaultUserName) {
        this.defaultUserName = defaultUserName;
    }
    
    public RootUploader getUploader(int index){
        return uploaders.get(index);
    }

    public String getDefaultServer() {
        return defaultServer;
    }

    public void setDefaultServer(String defaultServer) {
        this.defaultServer = defaultServer;
    }

    public int getDefaultScanInterval() {
        return defaultScanInterval;
    }

    public int getDefaultPort() {
        return defaultPort;
    }

    public void setDefaultPort(int defaultPort) {
        this.defaultPort = defaultPort;
    }

    
    public void setDefaultScanInterval(int defaultScanInterval) {
        this.defaultScanInterval = defaultScanInterval;
    }
    
    
    /** Quit the uploader */
    public void quit(){
        try {
            save();
            if(timeChecker!=null){
                timeChecker.setStopFlag(true);
            }
            System.exit(0);
        } catch (Exception e){
            System.err.println("Error saving settings: " + e.getMessage());
            System.exit(1);
        }
        
    }

    public ErrorManager getErrorManager() {
        return errorManager;
    }

    
    public void logError(RootUploader source, String message){
        if(defaultShowMessages){
            System.err.println(message);
        }
        errorManager.add(new UploaderError(source, message));
    }
    
    public void logMessage(String message){
        if(defaultShowMessages){
            System.out.println(message);
        }
    }
    
    public void loadFromPreferences() throws Exception {
        boolean needToEdit = false;
        PreferenceManager.loadPropertiesFromHomeDir(".inkspot", "SimpleUploader.xml");
        XmlDataStore systemPrefs = PreferenceManager.getSystemPropertyGroup("Uploader");
        XmlDataStore userPrefs = PreferenceManager.getEditablePropertyGroup("Settings");
        
        if(systemPrefs.containsName("Data")){
            recreateObject(systemPrefs.xmlDataStoreValue("Data"));
        } else {
            systemPrefs.add("Data", storeObject());
            PreferenceManager.saveProperties();
            needToEdit = true;
        }
        
        defaultServer = userPrefs.stringValue("DefaultServer", defaultServer);
        defaultPort = Integer.parseInt(userPrefs.stringValue("DefaultPort", "80"));
        defaultPassword = userPrefs.stringValue("DefaultPassword", defaultPassword);
        defaultUserName = userPrefs.stringValue("DefaultUserName", defaultUserName);
        defaultScanInterval = Integer.parseInt(userPrefs.stringValue("DefaultScanInterval", "240"));
        defaultSecure = userPrefs.booleanValue("DefaultSecure", defaultSecure);
        defaultShowMessages = userPrefs.booleanValue("DefaultShowMessages", defaultShowMessages);
        defaultTimeDifference = userPrefs.longValue("DefaultTimeDifference", 0);
        defaultTimeCheckInterval = userPrefs.longValue("DefaultTimeCheckInterval", 3600000);
        defaultDownloadInterval = userPrefs.intValue("DefaultDownloadInterval", 600);
        defaultSettlingTime = userPrefs.intValue("SettlingTime", 1000);

        PreferenceManager.saveProperties();
        
        if(needToEdit){
            if(systemTray!=null){
                systemTray.showSettingsWindow();
            } else {
                editSettings();
            }
        }
    }
    
    public void editSettings(){
        PreferenceManagerEditor editor = new PreferenceManagerEditor();
        editor.setAutoSave(true);
        editor.disableSystemButton();
        editor.disableLoadButton();
        editor.showEditableProperties();
        editor.setVisible(true);
    }
    
    public void save() throws Exception {
        XmlDataStore systemPrefs = PreferenceManager.getSystemPropertyGroup("Uploader");
        systemPrefs.remove("Data");
        systemPrefs.add("Data", storeObject());
        
        XmlDataStore userPrefs = PreferenceManager.getEditablePropertyGroup("Settings");
        userPrefs.add("DefaultServer", defaultServer);
        userPrefs.add("DefaultPort", Integer.toString(defaultPort));
        userPrefs.add("DefaultPassword", defaultPassword);
        userPrefs.add("DefaultUserName", defaultUserName);
        userPrefs.add("DefaultScanInterval", Integer.toString(defaultScanInterval));
        userPrefs.add("DefaultSecure", defaultSecure);
        userPrefs.add("DefaultShowMessages", defaultShowMessages);
        userPrefs.add("DefaultTimeDifference", defaultTimeDifference);
        userPrefs.add("DefaultTimeCheckInterval", defaultTimeCheckInterval);
        userPrefs.add("DefaultDownloadInterval", defaultDownloadInterval);
        PreferenceManager.saveProperties();
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("SimpleUploader");
        store.add("RootFolderCount", uploaders.size());
        for(int i=0;i<uploaders.size();i++){
            store.add("RootFolderUploader" + i, uploaders.get(i));
        }
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        int count = store.intValue("RootFolderCount", 0);
        for(int i=0;i<count;i++){
            addUploader((RootUploader)store.xmlStorableValue("RootFolderUploader" + i));
        }
    }
    
    public static void main(String[] args){
        try {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Uploader" );
            System.setProperty("com.apple.macos.useScreenMenuBar", "false" );
            System.setProperty("apple.laf.useScreenMenuBar", "false" ); // for older versions of Java
            System.setProperty("apple.awt.UIElement", "true");
        } catch ( SecurityException e ) {
        /* probably running via webstart, do nothing */
        }
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e){
            System.out.println("Cannot set system look and feel");
        }        
        SimpleUploader up = new SimpleUploader();
        try {
            up.loadFromPreferences();
        } catch (Exception e){
            System.err.println("Error loading properties: " + e.getMessage());
            System.exit(1);
        }
        up.updateSystemTray();
        up.start();
        
    }
}

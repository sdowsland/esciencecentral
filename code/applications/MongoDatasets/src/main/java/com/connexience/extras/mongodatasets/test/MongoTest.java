/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.extras.mongodatasets.test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.WriteConcern;
import com.mongodb.util.JSON;
import java.util.Date;

/**
 *
 * @author hugo
 */
public class MongoTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            MongoClient mongoClient = new MongoClient( "localhost" );

            DB db = mongoClient.getDB("connexience");
            DBCollection c = db.getCollection("data_002");

            BasicDBObject query = new BasicDBObject();
            c.remove(query);
        
            
            
            // Create the counter
            DBCollection counters = db.getCollection("counters");
            counters.remove(new BasicDBObject());
            
            BasicDBObject counter = new BasicDBObject();
            counter.append("_id", "data_002");
            counter.append("seq", 0);
            counters.insert(counter);            

            BasicDBObject update = new BasicDBObject();

            DBObject val = counters.findAndModify(new BasicDBObject("_id", "data_002"), new BasicDBObject("$inc", new BasicDBObject("seq", 1)));
            
            DBObject obj = new BasicDBObject();
            obj.put("_id", /*val.get("seq")*/1);
            obj.put("v1", "Hello");
            obj.put("_timestamp", new Date());
            c.insert(obj, WriteConcern.ACKNOWLEDGED);
            
            DBObject obj2 = new BasicDBObject();
            val = counters.findAndModify(new BasicDBObject("_id", "data_002"), new BasicDBObject("$inc", new BasicDBObject("seq", 1)));
            obj2.put("_id", /*val.get("seq")*/1);
            obj2.put("v1", "Hello");
            obj2.put("_timestamp", new Date());            
            c.insert(obj2, WriteConcern.ACKNOWLEDGED);
            
            DBCursor cursor = c.find();
            while(cursor.hasNext()){
                System.out.println(cursor.next().toString());
            }
        } catch (DuplicateKeyException dke){
            System.out.println("Key exception");
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
}

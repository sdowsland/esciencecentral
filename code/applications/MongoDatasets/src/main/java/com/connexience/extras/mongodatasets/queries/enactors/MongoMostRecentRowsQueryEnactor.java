/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets.queries.enactors;

import com.connexience.extras.mongodatasets.MongoDatasetQueryEnactor;
import com.connexience.extras.mongodatasets.MongoDatasetStorage;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.queries.MongoIndexRangeQuery;
import com.connexience.extras.mongodatasets.queries.MongoMostRecentRowsQuery;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.util.JSONContainer;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.json.JSONObject;

/**
 * The class provides the enactor for the most recent rows mongo data query.
 * @author hugo
 */
public class MongoMostRecentRowsQueryEnactor extends MongoDatasetQueryEnactor {

    public MongoMostRecentRowsQueryEnactor() {
        connectionType = CONNECTION_TYPE.NO_CONNECTION;
        cursorSupported = true;
    }

    @Override
    public JSONContainer performQuery() throws ConnexienceException {
        DBCursor cursor = getCursor();
        return MongoDatasetStorage.buildResultsFromCursor(cursor);        
    }    

    @Override
    public DBCursor getCursor() throws ConnexienceException {
        MongoDatasetStorage storage = new MongoDatasetStorage();
        DBCollection c = storage.getCollectionForItem((MongoDatasetItem)getItem());
        long size = storage.getMultipleValueDataSize(null, (MultipleValueItem)item);
        MongoMostRecentRowsQuery q = (MongoMostRecentRowsQuery)query;
        if(size>0){
            long startRow = size - q.getNumberOfRows();
            if(startRow<0){
                startRow = 0;
            }       

            DBCursor cursor = c.find(new BasicDBObject(), createKeysObject());
            cursor.skip((int)startRow);
            cursor.limit(q.getNumberOfRows());   
            return cursor;
        } else {
            DBCursor cursor = c.find(new BasicDBObject(), createKeysObject());
            return cursor;
        }
    }
}
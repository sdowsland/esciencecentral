/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets;

import com.connexience.server.ConnexienceException;
import com.mongodb.MongoClient;
import com.connexience.server.ejb.util.EJBLocator;
import com.mongodb.DB;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.log4j.Logger;

/**
 * This class holds a connection to a mongo server
 * @author hugo
 */
@Startup
@Singleton(name = "MongoDatasetConnectionBean", mappedName = "EJB/MongoDatasetConnectionBean")
public class MongoDatasetConnection {
    private static Logger logger = Logger.getLogger(MongoDatasetConnection.class);
    
    private MongoClient client;
    public static MongoDatasetConnection INSTANCE = null;
    
    public MongoDatasetConnection() {
        MongoDatasetConnection.INSTANCE = this;
    }
    
    @PostConstruct
    public void createClient(){
        logger.info("Connection Bean postconstruct");
        try{
            client = new MongoClient(EJBLocator.lookupPreferencesBean().stringValue("MongoDB", "Server", "localhost"), EJBLocator.lookupPreferencesBean().intValue("MongoDB", "Port", 27017));
        } catch(Exception e){
            logger.error("Error connecting to MongoServer: " + e.getMessage());
        }
    }
    
    public MongoClient getClient(){
        return client;
    }
}
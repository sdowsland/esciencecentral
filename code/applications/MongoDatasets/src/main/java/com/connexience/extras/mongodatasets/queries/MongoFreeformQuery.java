/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets.queries;

import com.connexience.extras.mongodatasets.MongoDatasetQuery;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.queries.enactors.MongoFreeformQueryEnactor;
import org.json.JSONObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This query passes a DBObject (represented as JSON) through to the
 * underlying MongoDB instance.
 * @author hugo
 */
public class MongoFreeformQuery extends MongoDatasetQuery {
    /** String representation of query */
    private String queryString = "{}";

    public MongoFreeformQuery() {
        label = "Freeform query";
        supportedClass = MongoDatasetItem.class;
        enactorClass = MongoFreeformQueryEnactor.class;        
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getQueryString() {
        return queryString;
    }
    
    @Override
    public JSONObject toJson() throws Exception {
        JSONObject json = super.toJson(); 
        json.put("QueryText", queryString);
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        super.readJson(json);
        if(json.has("QueryText")){
            queryString = json.getString("QueryText");
        }
    }
    
    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store); 
        queryString = store.stringValue("QueryString", "{}");
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("QueryString", queryString);
        return store;
    }        
}
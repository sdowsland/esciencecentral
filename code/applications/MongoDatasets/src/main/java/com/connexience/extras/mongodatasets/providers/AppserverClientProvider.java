/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.extras.mongodatasets.providers;

import com.connexience.extras.mongodatasets.MongoDatasetClientProvider;
import com.connexience.extras.mongodatasets.MongoDatasetConnection;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * This class provides an implementation of the mongo dataset client provider
 * interface that can supply MongoDB connections to the mongo dataset storage
 * engine when it is running in the app server.
 * @author hugo
 */
public class AppserverClientProvider implements MongoDatasetClientProvider {
    @Override
    public final MongoClient getClient() throws ConnexienceException {
        return MongoDatasetConnection.INSTANCE.getClient();
    }

    @Override
    public final DB getDB() throws ConnexienceException {
        DB db = getClient().getDB(EJBLocator.lookupPreferencesBean().stringValue("MongoDB", "Database", "connexience"));
        return db;
    }
}
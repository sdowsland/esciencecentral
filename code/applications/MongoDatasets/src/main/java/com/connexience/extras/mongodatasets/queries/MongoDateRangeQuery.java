/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets.queries;

import com.connexience.extras.mongodatasets.MongoDatasetQuery;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.queries.enactors.MongoDateRangeQueryEnactor;
import com.connexience.server.util.JSONDate;
import java.util.Date;
import org.json.JSONObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This query provides data between a range of dates
 * @author hugo
 */
public class MongoDateRangeQuery extends MongoDatasetQuery {
    private Date startDate = new Date();
    private Date endDate = new Date();

    public MongoDateRangeQuery() {
        label = "All values since date";
        supportedClass = MongoDatasetItem.class;  
        enactorClass = MongoDateRangeQueryEnactor.class;        
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("StartDate", startDate);
        store.add("EndDate", endDate);
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store);
        startDate = store.dateValue("StartDate", new Date());
        endDate = store.dateValue("EndDate", new Date());
    }

    @Override
    public JSONObject toJson() throws Exception {
        JSONObject json = super.toJson();
        json.put("StartDate", new JSONDate(startDate));
        json.put("EndDate", new JSONDate(endDate));
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        super.readJson(json);
        
        if(json.has("StartDate")){
            startDate = JSONDate.createDate(json.getJSONObject("StartDate"));
        }
        
        if(json.has("EndDate")){
            endDate = JSONDate.createDate(json.getJSONObject("EndDate"));
        }
        
    }       
    
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets.queries.enactors;

import com.connexience.extras.mongodatasets.MongoDatasetQueryEnactor;
import com.connexience.extras.mongodatasets.MongoDatasetStorage;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.queries.MongoDateRangeQuery;

import com.connexience.server.ConnexienceException;
import com.connexience.server.util.JSONContainer;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.util.Date;

/**
 * This class provides the enactor for a date range query
 * @author hugo
 */
public class MongoDateRangeQueryEnactor extends MongoDatasetQueryEnactor {

    public MongoDateRangeQueryEnactor() {
        cursorSupported = true;
    }

    @Override
    public JSONContainer performQuery() throws ConnexienceException {
        DBCursor cursor = getCursor();
        return MongoDatasetStorage.buildResultsFromCursor(cursor);
    }

    @Override
    public DBCursor getCursor() throws ConnexienceException {
        MongoDatasetStorage storage = new MongoDatasetStorage();
        DBCollection c = storage.getCollectionForItem((MongoDatasetItem)getItem());
        MongoDateRangeQuery q = (MongoDateRangeQuery)query;
        Date startDate = q.getStartDate();
        Date endDate = q.getEndDate();
        BasicDBObject query = new BasicDBObject(); 
        query.put("_timeInMillis", new BasicDBObject("$gt", startDate.getTime()).append("$lt", endDate.getTime()));
        DBCursor cursor = c.find(query, createKeysObject());
        return cursor;        
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets.queries;

import com.connexience.extras.mongodatasets.MongoDatasetQuery;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.queries.enactors.MongoMostRecentRowsQueryEnactor;
import org.json.JSONObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This query returns the latest 'n' rows from a collection
 * @author hugo
 */
public class MongoMostRecentRowsQuery extends MongoDatasetQuery {
    private int numberOfRows = 100;

    public MongoMostRecentRowsQuery() {
        label = "Most recent rows";
        supportedClass = MongoDatasetItem.class;
        enactorClass = MongoMostRecentRowsQueryEnactor.class;        
    }
    
    public int getNumberOfRows() {
        return numberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }
    
    public JSONObject toJson() throws Exception {
        JSONObject json = super.toJson(); 
        json.put("NumberOfRows", numberOfRows);
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        super.readJson(json);
        if(json.has("NumberOfRows")){
            numberOfRows = json.getInt("NumberOfRows");
        }
    }
    
    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store); 
        numberOfRows = store.intValue("NumberOfRows", 100);
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("NumberOfRows", numberOfRows);
        return store;
    }        
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.extras.mongodatasets.queries;

import com.connexience.extras.mongodatasets.MongoDatasetQuery;
import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.queries.enactors.MongoPercentRangeQueryEnactor;
import org.json.JSONObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This query allows a query to be defined in terms of percentages of data.
 * @author hugo
 */
public class MongoPercentRangeQuery extends MongoDatasetQuery {
    private double startPercentLocation = 30.0;
    private double endPercentLocation = 40.0;
    
    public MongoPercentRangeQuery() {
        label = "Percent Range";
        supportedClass = MongoDatasetItem.class;
        enactorClass = MongoPercentRangeQueryEnactor.class;            
    }

    public void setStartPercentLocation(double startPercentLocation) {
        this.startPercentLocation = startPercentLocation;
    }

    public double getEndPercentLocation() {
        return endPercentLocation;
    }

    public double getStartPercentLocation() {
        return startPercentLocation;
    }

    public void setEndPercentLocation(double endPercentLocation) {
        this.endPercentLocation = endPercentLocation;
    }
    
    public JSONObject toJson() throws Exception {
        JSONObject json = super.toJson(); 
        json.put("StartPercentLocation", startPercentLocation);
        json.put("EndPercentLocation", endPercentLocation);
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        super.readJson(json);
        if(json.has("StartPercentLocation")){
            startPercentLocation = json.getDouble("StartPercentLocation");
        }
        
        if(json.has("EndPercentLocation")){
            endPercentLocation = json.getDouble("EndPercentLocation");
        }        
    }
    
    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store); 
        startPercentLocation = store.doubleValue("StartLocation", 30.0);
        endPercentLocation = store.doubleValue("EndLocation", 40.0);
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("StartLocation", startPercentLocation);
        store.add("EndLoction", endPercentLocation);
        return store;
    }       
}
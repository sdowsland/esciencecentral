/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.datasets.DatasetQueryEnactor;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.util.List;

/**
 * This is the base class for a mongo query enactor.
 * @author hugo
 */
public abstract class MongoDatasetQueryEnactor extends DatasetQueryEnactor {
    protected boolean cursorSupported = false;
    
    public MongoDatasetStorage createStorage(){
        return new MongoDatasetStorage();
    }
    
    public MongoClient getClient(){
        return null;
    }

    /** Does this query provide a cursor that can be stepped through */
    public boolean isCursorSupported() {
        return cursorSupported;
    }
    
    /** 
     * Get a database cursor that is configured for this query. This allows the
     * workflow engine to stream data from the database without having to load it 
     * into memory all at once.
     */
    public DBCursor getCursor() throws ConnexienceException {
        throw new ConnexienceException("Cursor is not supported by this query enactor");
    }
    
    public DBObject createKeysObject(){
        BasicDBObject keyList = new BasicDBObject();
        List<String>keys = getQuery().getKeys();
        for(String key : keys){
            keyList.put(key, 1);
        }
        return keyList;
    }
}

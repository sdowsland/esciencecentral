/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.extras.mongodatasets.providers;

import com.connexience.extras.mongodatasets.MongoDatasetClientProvider;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import org.pipeline.core.xmlstorage.XmlDataStore;

/**
 * This client provider 
 * @author hugo
 */
public class ExternalClientProvider implements MongoDatasetClientProvider {
    /** Properties for creating the client */
    private XmlDataStore mongoProperties;
    private MongoClient client = null;
    
    public ExternalClientProvider(XmlDataStore mongoProperties) {
        this.mongoProperties = mongoProperties;
    }

    public ExternalClientProvider() {
    }

    public void setMongoProperties(XmlDataStore mongoProperties) {
        this.mongoProperties = mongoProperties;
    }

    @Override
    public MongoClient getClient() throws ConnexienceException {
        try {
            if(client==null){
                client = new MongoClient(mongoProperties.stringValue("Server", "localhost"), mongoProperties.intValue("Port", 27017));
            }
            return client;
        } catch (Exception e){
            throw new ConnexienceException("Error getting mongo client: " + e.getMessage(), e);
        }
    }

    @Override
    public DB getDB() throws ConnexienceException {
        DB db = getClient().getDB(mongoProperties.stringValue("Database", "connexience"));
        return db;        
    }
}
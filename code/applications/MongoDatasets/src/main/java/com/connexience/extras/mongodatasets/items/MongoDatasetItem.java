/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.extras.mongodatasets.items;

import com.connexience.server.model.datasets.DatasetCatalog;
import com.connexience.server.model.datasets.DatasetCatalogItem;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import org.json.JSONObject;

/**
 *
 * @author hugo
 */
public class MongoDatasetItem extends MultipleValueItem {
    private String indexFields = "";
    
    @Override
    public String getTypeLabel() {
        return "MongoDB Collection";
    }

    @Override
    public void register() {
        DatasetCatalog.register(new DatasetCatalogItem(getClass(), "mongo-json-data", getTypeLabel(), "MongoDB Collection"));
    }

    public void setIndexFields(String indexFields) {
        this.indexFields = indexFields;
    }

    public String getIndexFields() {
        return indexFields;
    }

    @Override
    public JSONObject toJson() throws Exception {
        JSONObject json = super.toJson(); 
        json.put("IndexFields", indexFields);
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        super.readJson(json);
        if(json.has("IndexFields")){
            indexFields = json.getString("IndexFields");
        } else {
            indexFields = "";
        }
    }
}
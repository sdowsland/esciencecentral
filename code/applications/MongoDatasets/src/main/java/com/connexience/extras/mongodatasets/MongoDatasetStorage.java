/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets;

import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.server.ConnexienceException;

import com.connexience.server.model.datasets.DatasetItemStorage;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.util.JSONContainer;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;
import com.mongodb.util.JSON;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;

/**
 * This class provides an e-SC Dataset driver for MongoDB
 * @author hugo
 */
public class MongoDatasetStorage extends DatasetItemStorage {
    private static Logger logger = Logger.getLogger(MongoDatasetStorage.class);
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static MongoDatasetClientProvider clientProvider = null;
    
    @Override
    public void appendMultipleValueData(Object session, MultipleValueItem item, Object data) throws ConnexienceException {
        if(data instanceof Data){
            // Multiple rows
            DBCollection c = getCollectionForItem(item);
            Data dataset = (Data)data;
            int rows = dataset.getLargestRows();
            DBObject objectToAdd;
            
            for(int i=0;i<rows;i++){
                objectToAdd = buildObjectFromDataRow(dataset, i);
                
                // Add ID and timestamp fields
                Date timestamp = new Date();
                objectToAdd.put("_timeInMillis", timestamp.getTime());
                objectToAdd.put("_timestamp", format.format(timestamp));

                c.insert(objectToAdd, WriteConcern.ACKNOWLEDGED);                
            }
            
        } else {
            // Single row
            DBCollection c = getCollectionForItem(item);
            DBObject objectToAdd = null;
            if(data instanceof JSONObject){
                // Add JSON object
                objectToAdd = (DBObject)JSON.parse(((JSONObject)data).toString());

            } else if(data instanceof JSONContainer){
                objectToAdd = (DBObject)JSON.parse(((JSONContainer)data).getStringData());

            } else{
                objectToAdd = (DBObject)JSON.parse(data.toString());
            }

            // Add ID and timestamp fields
            Date timestamp = new Date();
            objectToAdd.put("_timeInMillis", timestamp.getTime());
            objectToAdd.put("_timestamp", format.format(timestamp));

            c.insert(objectToAdd, WriteConcern.ACKNOWLEDGED);
        }
    }

    @Override
    public Object getMultipleValueData(Object session, MultipleValueItem item, int startIndex, int maxResults, String[] keys) throws ConnexienceException {
        if(keys==null){
            return getMultipleValueData(session, item, startIndex, maxResults);
        } else {
            DBCollection c = getCollectionForItem(item);
            DBCursor cursor = c.find(new BasicDBObject(), createKeysObject(keys));
            cursor.skip(startIndex);
            cursor.limit(maxResults);
            return MongoDatasetStorage.buildResultsFromCursor(cursor);
        }
    }

    @Override
    public Object getMultipleValueData(Object session, MultipleValueItem item, int startIndex, int maxResults) throws ConnexienceException {
        DBCollection c = getCollectionForItem(item);
        DBCursor cursor = c.find(new BasicDBObject(), new BasicDBObject());
        cursor.skip(startIndex);
        cursor.limit(maxResults);
        return MongoDatasetStorage.buildResultsFromCursor(cursor);
    }

    @Override
    public Object getMultipleValueDataRow(Object session, MultipleValueItem item, String rowIndex) throws ConnexienceException {
        DBCollection c = getCollectionForItem(item);
        BasicDBObject query = new BasicDBObject("_id", rowIndex);
        DBCursor cursor = c.find(query);
        return MongoDatasetStorage.buildResultsFromCursor(cursor);
    }

    @Override
    public Object getMultipleValueData(Object session, MultipleValueItem item) throws ConnexienceException {
        throw new ConnexienceException("Retreiving an entire collection is not supported for Mongo Datasets");
    }

    @Override
    public void updateMultipleValueData(Object session, MultipleValueItem item, String rowId, Object data) throws ConnexienceException {
        BasicDBObject query = new BasicDBObject("_id", rowId);
        DBObject objectToAdd = null;
        if(data instanceof JSONObject){
            // Add JSON object
            objectToAdd = (DBObject)JSON.parse(((JSONObject)data).toString());
            
        } else if(data instanceof JSONContainer){
            objectToAdd = (DBObject)JSON.parse(((JSONContainer)data).getStringData());
            
        } else{
            objectToAdd = (DBObject)JSON.parse(data.toString());
        }
        DBCollection c = getCollectionForItem(item);
        Date timestamp = new Date();
        objectToAdd.put("_timeInMillis", timestamp.getTime());
        objectToAdd.put("_timestamp", format.format(timestamp));        
        c.update(query, objectToAdd);
    }

    @Override
    public void removeMultipleValueDataRow(Object session, MultipleValueItem item, String rowId) throws ConnexienceException {
        DBCollection c = getCollectionForItem(item);
        BasicDBObject query = new BasicDBObject("_id", rowId);
        c.remove(query);
    }

    @Override
    public void removeMultipleValueData(Object session, MultipleValueItem item) throws ConnexienceException {
        DBCollection c = getCollectionForItem(item);
        BasicDBObject query = new BasicDBObject();
        c.remove(query);
    }

    @Override
    public long getMultipleValueDataSize(Object session, MultipleValueItem item) throws ConnexienceException {
        DBCollection c = getCollectionForItem(item);
        return c.count();
    }
    
    public DBCollection getCollectionForItem(MultipleValueItem item) throws ConnexienceException {
        try {
            MongoDatasetItem mongoItem = (MongoDatasetItem)item;
            DB db = clientProvider.getDB();
            
            String collectionName = "data_" + mongoItem.getId();
            if(!db.collectionExists(collectionName)){
                // Create the collection
                DBCollection c = db.createCollection(collectionName, new BasicDBObject());
                
                // Add indexes for the millisecond timestamp
                DBObject indexObject = new BasicDBObject();
                indexObject.put("_timeInMillis", 1);
                DBObject indexOptions = new BasicDBObject();
                indexOptions.put("name", "_timeInMillis");    
                c.createIndex(indexObject, indexOptions);
                    
                return c;
                
            } else {
                // Return existing collection
                DBCollection c = db.getCollection(collectionName);   
                return c;
            }
            
        } catch (Exception e){
            throw new ConnexienceException("Error connecting to Mongo Database: " + e.getMessage(), e);
        }
    }
    
    public static DBObject buildObjectFromDataRow(Data data, int row) throws ConnexienceException {
        BasicDBObject dbObject = new BasicDBObject();
        for(int i=0;i<data.getColumnCount();i++){
            if(!data.column(i).isMissing(row)){
                dbObject.put(data.column(i).getName(), data.column(i).getObjectValue(row));
            }
        }
        
        return dbObject;
    }
   
    
    public static JSONContainer buildResultsFromCursor(DBCursor cursor) throws ConnexienceException {
        JSONArray data = new JSONArray();
        DBObject record;
        
        try {
            while(cursor.hasNext()){
                record = cursor.next();
                data.put(new JSONObject(record.toString()));
            }
        } catch (Exception e){
            throw new ConnexienceException("Error stepping through results data: " + e.getMessage(), e);
        }
        
        try {
            JSONObject result = new JSONObject();
            result.put("data", data);
            return new JSONContainer(result);
        } catch (Exception e){
            throw new ConnexienceException("Error building results data: " + e.getMessage(), e);
        }
    }

    @Override
    public void itemSaved(MultipleValueItem item) throws ConnexienceException {
        // Get a list of indexed fields
        if(item instanceof MongoDatasetItem){
            MongoDatasetItem mongoItem = (MongoDatasetItem)item;
            String fields = mongoItem.getIndexFields();
            StringTokenizer tokens = new StringTokenizer(fields, ",");
            ArrayList<String> fieldArray = new ArrayList<>();
            while(tokens.hasMoreElements()){
                fieldArray.add(tokens.nextToken());
            }
            
            // Now build the indexes
            DBCollection c = getCollectionForItem(item);
            List<DBObject> indexes = c.getIndexInfo();
            
            
            DBObject key;
            DBObject existingIndex;
            
            // Create new ones
            for(String field : fieldArray){
                existingIndex = null;
                
                for(DBObject index : indexes){
                    Object name = index.get("name");
                    if(name!=null && field.equals(name.toString())){
                        existingIndex = index;
                    }
                }
                
                if(existingIndex==null){
                    // Need to create index
                    logger.info("Creating index for field: " + field);
                    DBObject indexObject = new BasicDBObject();
                    indexObject.put(field, 1);
                    
                    DBObject indexOptions = new BasicDBObject();
                    indexOptions.put("name", field);
                    
                    c.createIndex(indexObject, indexOptions);
                }   
            }
            
            // Remove ones that we don't need any more
            for(DBObject index : indexes){
                Object name = index.get("name");
                if(name!=null && !name.toString().contains("_id") && !name.toString().contains("_timeInMillis")){
                    if(!fieldArray.contains(name.toString())){
                        logger.info("Dropping index: " + name + " on collection: " + c.getName() + "");
                        c.dropIndex(name.toString());
                    }
                }
            }            
            
            logger.info("MongoDatasetItem saved: " + item.getName());
        }
    }

    @Override
    public void itemDeleted(MultipleValueItem item) throws ConnexienceException {
        if(item instanceof MongoDatasetItem){
            DBCollection c = getCollectionForItem(item);
            logger.info("Dropping collection: " + c.getName());
            c.drop();
        }
    }
    
    public DBObject createKeysObject(String[] keys){
        BasicDBObject keyList = new BasicDBObject();
        for(String key : keys){
            keyList.put(key, 1);
        }
        return keyList;
    }    
}
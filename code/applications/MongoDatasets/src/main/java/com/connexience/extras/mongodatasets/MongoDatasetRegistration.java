/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.extras.mongodatasets;

import com.connexience.extras.mongodatasets.items.MongoDatasetItem;
import com.connexience.extras.mongodatasets.providers.AppserverClientProvider;
import com.connexience.extras.mongodatasets.queries.MongoFreeformQuery;
import com.connexience.extras.mongodatasets.queries.MongoIndexRangeQuery;
import com.connexience.extras.mongodatasets.queries.MongoMostRecentDateQuery;
import com.connexience.extras.mongodatasets.queries.MongoMostRecentRowsQuery;
import com.connexience.extras.mongodatasets.queries.MongoPercentRangeQuery;
import com.connexience.server.ejb.ConfigReplacer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.datasets.DatasetItemStorageFactory;
import com.connexience.server.model.datasets.DatasetQueryFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Startup;
import javax.ejb.Singleton;
import org.apache.log4j.Logger;
/**
 * This class registers the extra mongo dataset functionality with the dataset
 * catalog.
 * @author hugo
 */
@Singleton
@Startup
@DependsOn({"PreferenceStore"})
public class MongoDatasetRegistration  {
    private static final Logger logger = Logger.getLogger(MongoDatasetRegistration.class);
    
    public MongoDatasetRegistration() {
    }
    
    @PostConstruct
    public void register(){
        try {
            logger.info("Registering MongoDB Dataset engine");
            
            // Register item and storage
            DatasetItemStorageFactory.register(MongoDatasetItem.class, MongoDatasetStorage.class);
            new MongoDatasetItem().register();
            ConfigReplacer.addDatasetImplementation("${MULTIPLEVALUEITEM}", "/MongoFragment.xml");
            
            // Register the query types
            DatasetQueryFactory.registerQuery("mongo-index-range", MongoIndexRangeQuery.class);
            DatasetQueryFactory.registerQuery("mongo-recent-rows", MongoMostRecentRowsQuery.class);
            DatasetQueryFactory.registerQuery("mongo-percent-range", MongoPercentRangeQuery.class);
            DatasetQueryFactory.registerQuery("mongo-data-from-date", MongoMostRecentDateQuery.class);
            DatasetQueryFactory.registerQuery("mongo-freeform-query", MongoFreeformQuery.class);
            
            // Check preferences exist and save them if not
            logger.info("Checking MongoDB preference");
            if(!EJBLocator.lookupPreferencesBean().groupHasProperty("MongoDB", "Server")){
                EJBLocator.lookupPreferencesBean().add("MongoDB", "Server", "localhost");
                logger.info(("Added MongoDB.Server=localhost"));
            }
            
            if(!EJBLocator.lookupPreferencesBean().groupHasProperty("MongoDB", "Database")){
                EJBLocator.lookupPreferencesBean().add("MongoDB", "Database", "connexience");
                logger.info(("Added MongoDB.Database=connexience"));
            }           
            
            if(!EJBLocator.lookupPreferencesBean().groupHasProperty("MongoDB", "Port")){
                EJBLocator.lookupPreferencesBean().add("MongoDB", "Port", 27017);
                logger.info(("Added MongoDB.Port=27017"));
            }                      
            
            // Setup the client provider
            MongoDatasetStorage.clientProvider = new AppserverClientProvider();
            
        } catch (Exception e){
            logger.error("Error registering MongoDB dataset engine: " + e.getMessage());
        }        
    }
    
    @PreDestroy
    public void unregister(){
        logger.info("Unregistering MongoDB engine");
        MongoDatasetStorage.clientProvider = null;
        DatasetQueryFactory.unregisterQuery("mongo-index-range");
        DatasetQueryFactory.unregisterQuery("mongo-recent-rows");
        DatasetQueryFactory.unregisterQuery("mongo-percent-range");
        DatasetQueryFactory.unregisterQuery("mongo-data-from-date");    
        DatasetQueryFactory.unregisterQuery("mongo-freeform-query");
        DatasetItemStorageFactory.unregister(MongoDatasetItem.class);
    }
}
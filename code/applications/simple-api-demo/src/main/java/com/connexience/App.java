package com.connexience;

import com.connexience.api.StorageClient;
import com.connexience.api.WorkflowClient;
import com.connexience.api.model.*;

import java.io.File;


public class App {


    public static void main(String[] args) {

        String hostname = "demo.escapp.net";
        int port = 80;
        String username = ""; //set me
        String password = ""; //set me
        String workflowId = "39687";  //The id of the workflow to run.  Get it from the browser address bar.
        File fileToUpload = new File("/Users/nsjw7/Desktop/pcadata.csv");
        String downloadLocation =   "/Users/nsjw7/Desktop/";
        try {
            // Create a new storage client with username and password
            StorageClient storageClient = new StorageClient(hostname, port, false, username, password);
            WorkflowClient wfClient = new WorkflowClient(hostname, port, false, username, password);

            // Check that the current user can be accessed
            EscUser currentUser = storageClient.currentUser();
            System.out.println(currentUser.getName());

            //Upload a file
            EscDocument document = storageClient.createDocumentInFolder(storageClient.homeFolder().getId(), fileToUpload.getName());
            storageClient.upload(document, fileToUpload);

            //create the parameters for the workflow.  This will set the Source property of the input block to the ID of the
            //newly uploaded file
            EscWorkflowParameterList workflowParameters = new EscWorkflowParameterList();

            //The 'name' of the block can be set as one of the properties of the block.  By default is is block_nnn where nnn is a number.  It needs to be unique.
            workflowParameters.addParameter("input", "Source", document.getId());

            //Start the workflow asynchronously
            EscWorkflowInvocation invocation = wfClient.executeWorkflowWithParameters(workflowId, workflowParameters);

            //Wait for completion.  This may or may not be what you want to do depending on expected runtime of the workflow
            while(!invocation.getStatus().equals("Finished") && !invocation.getStatus().equals("ExecutionError") )
            {
                invocation = wfClient.getInvocation(invocation.getId());
                System.out.println("invocation.getStatus() = " + invocation.getStatus());
                Thread.sleep(2 * 1000);
            }

            //Output the status
            System.out.println("invocation.getStatus() = " + invocation.getStatus());

            EscFolder workflowResults = storageClient.getFolder(invocation.getId());
            EscDocument[] results = storageClient.folderDocuments(workflowResults.getId());
            for(EscDocument result : results){
                storageClient.download(result, new File(downloadLocation + result.getName()));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

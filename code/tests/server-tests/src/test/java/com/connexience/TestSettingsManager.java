/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import org.apache.log4j.*;

/**
 * This simple class loads a Properties file from the .inkspot directory in the
 * home directory so that tests can use them.
 * @author hugo
 */
public class TestSettingsManager {
    private static Properties props = new Properties();
    private static Logger logger = Logger.getLogger(TestSettingsManager.class);
    
    static {
        File propertiesFile = new File(System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "tests.properties");
        if(propertiesFile.exists()){
            try {
                props.load(new FileInputStream(propertiesFile));
            } catch (Exception e){
                logger.error("Error loading tests.properties file: " + e.getMessage());
            }
        } else {
            props.put("hostname", "localhost");
            props.put("username", "user1");
            props.put("password", "password");
            props.put("httpport", "8080");
            props.put("resttestfoldername", "Test Folder");
            props.put("restdownloadfile", "data.csv");
            props.put("restuploadfile", "test.m");
            props.put("externalrestworkflowtest.workflowid", "14293");
            props.put("externalrestworkflowtest.targetfileid", "8a8082853aac9311013aae80688e0011");
            
            props.put("externallycallableworkflowtest.workflowid", "6321");
            props.put("externallycallableworkflowtest.fileid", "341");
            props.put("datadirectory", System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "testdata");
            writeProperties();
        }
    }
    
    private static void writeProperties(){
        try {
            File propertiesFile = new File(System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "tests.properties");
            props.store(new FileOutputStream(propertiesFile), "Test suite configuration");
        } catch (Exception e){
            logger.error("Error writing tests.properties file " + e.getMessage());
        }        
    }
    
    public static String getUsername(){
        return props.getProperty("username", "user1");
    }
    
    public static String getPassword(){
        return props.getProperty("password", "password");
    }
    
    public static int getHttpPort(){
        String port = props.getProperty("httpport", "8080");
        return Integer.parseInt(port);
    }
    
    public static String getHostname(){
        return props.getProperty("hostname", "localhost");
    }
    
    public static String getProperty(String name, String defaultValue){
        if(props.containsKey(name)){
            return props.getProperty(name);
        } else {
            logger.warn("tests.properties does not contain: " + name + " writing default value of: " + defaultValue);
            props.put(name, defaultValue);
            writeProperties();
            return defaultValue;
        }
    }
    
    public static File getDataDirectory(){
        File dir = new File(getProperty("datadirectory", System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "testdata"));
        if(!dir.exists()){
            dir.mkdirs();
        }
        return dir;
    }
}

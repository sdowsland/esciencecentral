/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import com.connexience.api.DatasetClient;
import com.connexience.api.model.EscDataset;
import com.connexience.api.model.EscDatasetItem;
import com.connexience.api.model.EscDatasetKeyList;
import com.connexience.api.model.json.JSONArray;
import com.connexience.api.model.json.JSONObject;
import junit.framework.TestCase;

/**
 * This class tests the external Dataset service
 * @author hugo
 */
public class ExternalRESTDatasetTest extends TestCase {
    @SuppressWarnings("unchecked")
    public void testDatasetService() throws Exception {
        DatasetClient c = new DatasetClient();
        EscDataset[] datasets = c.listDatasets();
        String datasetName = TestSettingsManager.getProperty("externalrestdatasettest.datasetname", "TestPackageData");
        
        for(int i=0;i<datasets.length;i++){
            System.out.println(datasets[i].getId() + ": " + datasets[i].getName());
            /*
            EscDatasetItem[] contents = c.listDatasetContents(datasets[i].getId());
            for(int j=0;j<contents.length;j++){
                System.out.println(datasets[i].getName() + "." + contents[j].getName());
            }
            */
            // Add an item
            if(datasets[i].getName().equals(datasetName)){
                System.out.println("Adding item to: " + datasetName);
                

                EscDatasetItem item = new EscDatasetItem();
                item.setItemType(EscDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW);
                item.setName("DataTable");
                item = c.addItemToDataset(datasets[i].getId(), item);
                System.out.println("Added item has ID=" + item.getId() + " and datasetID=" + item.getDatasetId());

                System.out.println("DataTable size: " + c.getMultipleValueDatasetItemSize(datasets[i].getId(), item.getName()));
                JSONObject json = new JSONObject();
                json.put("name", "ANAME");
                json.put("size", 56.03);
                c.appendToMultipleValueDatasetItem(datasets[i].getId(), "DataTable", json);
                
                
                System.out.println("DataTable size: " + c.getMultipleValueDatasetItemSize(datasets[i].getId(), item.getName()));
                
                // Edit an item
                JSONArray results = c.queryMultipleValueDatasetItemAsJson(datasets[i].getId(), "DataTable", 0, 1);
                if(results.length()==1){
                    JSONObject row = results.getJSONObject(0);
                    row.put("name", "NEWNAME");
                    row.put("size", 64.35);
                    c.updateMultipleValueDatasetItem(datasets[i].getId(), "DataTable", row);
                }
                
                EscDatasetItem item2 = new EscDatasetItem();
                item2.setItemType(EscDatasetItem.DATASET_ITEM_TYPE.SINGLE_ROW);
                item2.setName("ObservedData");
                item2 = c.addItemToDataset(datasets[i].getId(), item2);
                
                EscDatasetKeyList keys = new EscDatasetKeyList();
                keys.addKey("size");

                JSONArray resultsJson = c.queryMultipleValueDatasetItemAsJson(datasets[i].getId(), "DataTable", 0, 5, keys);
                for(int j=0;j<resultsJson.length();j++){
                    System.out.println(j + ": " + resultsJson.getJSONObject(j).toString());
                }
                
                JSONObject json2 = new JSONObject();
                json.put("UserID2", "jhu87");
                JSONArray names = new JSONArray();
                names.put("Sensor2");
                names.put("Sensor3");
                json.put("SensorNames", names);
                System.out.println("Sending: " + json.toString());
                
                JSONObject reparsed = new JSONObject(json.toString());
                JSONArray fetchedNames = reparsed.getJSONArray("SensorNames");
                
                c.updateSingleValueDatasetItem(datasets[i].getId(), "ObservedData", json.toString());
                        
                String data = c.querySingleValueDatasetItemAsString(datasets[i].getId(), "ObservedData");
                System.out.println("Raw data: " + data);
                
                JSONObject jsonObj = new JSONObject(data);
                System.out.println(jsonObj.toString(2));

                JSONArray snames = jsonObj.getJSONArray("SensorNames");
                for(int j=0;j<snames.length();j++){
                    System.out.println(snames.get(i));
                }
            }
        
        }
        // Try and create a dataset
        EscDataset ds2 = c.createDataset("TestCreated Data Set");

        EscDatasetItem table = new EscDatasetItem();
        table.setItemType(EscDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW);
        table.setName("MyData");
        table = c.addItemToDataset(ds2.getId(), table);
        
        
        JSONObject r1 = new JSONObject();
        r1.put("P1", 54);
        r1.put("P2", 45);
        c.appendToMultipleValueDatasetItem(ds2.getId(), table.getName(), r1);
        
        JSONObject r2 = new JSONObject();
        r2.put("P1", 54);
        r2.put("P2", 45);
        r2.put("P3", 45);
        c.appendToMultipleValueDatasetItem(ds2.getId(), table.getName(), r2);
        
        JSONObject r3 = new JSONObject();
        r3.put("P1", 54);
        r3.put("P3", 45);
        c.appendToMultipleValueDatasetItem(ds2.getId(), table.getName(), r3);

        JSONArray results = c.queryMultipleValueDatasetItemAsJson(ds2.getId(), table.getName(), 0, 100);
        for(int i=0;i<results.length();i++){
            System.out.println(results.getJSONObject(i).toString());
        }
    }
}
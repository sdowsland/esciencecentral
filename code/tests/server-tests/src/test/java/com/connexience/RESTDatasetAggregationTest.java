/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience;

import com.connexience.api.DatasetClient;
import com.connexience.api.model.EscDataset;
import com.connexience.api.model.EscDatasetItem;
import java.util.ArrayList;
import junit.framework.TestCase;

/**
 * Tests dataset aggregation
 * @author hugo
 */
public class RESTDatasetAggregationTest extends TestCase {
    public void testAggregation() throws Exception {
        DatasetClient c = new DatasetClient();
        EscDataset[] datasets = c.listDatasets();

        ArrayList<String> ids = new ArrayList<String>();
        for(EscDataset d : datasets){
            ids.add(d.getId());
            System.out.println("Adding: " + d.getId());
        }
        String[] idList = new String[ids.size()];
        ids.toArray(idList);
        EscDatasetItem[] items = c.aggregateDatasetItems(idList);
        for(EscDatasetItem i : items){
            if(i.getItemType().equals(EscDatasetItem.DATASET_ITEM_TYPE.SINGLE_ROW)){
                System.out.println(i.getName());
                System.out.println(c.querySingleValueDatasetItemAsJson(i.getDatasetId(), i.getName()));
            }
        }
    }
}
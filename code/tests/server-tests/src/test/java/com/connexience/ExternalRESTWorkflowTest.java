/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import com.connexience.api.StorageClient;
import com.connexience.api.WorkflowClient;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import com.connexience.api.model.EscWorkflowParameterList;
import junit.framework.TestCase;

/**
 * This class tests the external REST workflow API that uses the simplified data
 * model.
 * @author hugo
 */
public class ExternalRESTWorkflowTest extends TestCase {
    public void testWorkflowService() throws Exception {
        WorkflowClient c = new WorkflowClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());
        StorageClient sc = new StorageClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());
        EscWorkflow[] wfs = c.listWorkflows();
        for(int i=0;i<wfs.length;i++){
            System.out.println(wfs[i].toJsonObject().toString(2));
        }

        if(wfs.length>0){
            EscWorkflow wf = c.getWorkflow(wfs[0].getId());
            System.out.println("Fetched: " + wf.toJsonObject());
        }


        EscWorkflowInvocation invocation = c.executeWorkflow(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"));
        System.out.println("Executed: " + invocation.toJsonObject());

        EscWorkflowParameterList params = new EscWorkflowParameterList();
        params.addParameter("input", "Source", TestSettingsManager.getProperty("externalrestworkflowtest.targetfileid", "8a8082853aac9311013aae80688e0011"));

        EscWorkflowInvocation invocation2 = c.executeWorkflowWithParameters(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"), params);
        System.out.println("Executed: " + invocation2.toJsonObject());

        EscDocumentVersion v = sc.getLatestDocumentVersion(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"));
        EscWorkflowInvocation invocation3 = c.executeWorkflow(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"), v.getId());
        System.out.println("Executed: " + invocation3.toJsonObject());

        EscWorkflowInvocation invocation4 = c.executeWorkflowOnDocument(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"), TestSettingsManager.getProperty("externalrestworkflowtest.targetfileid", "8a8082853aac9311013aae80688e0011"));
        System.out.println("Executed: " + invocation4.toJsonObject());

        EscWorkflowInvocation invocation5 = c.executeWorkflowOnDocument(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"), v.getId(), TestSettingsManager.getProperty("externalrestworkflowtest.targetfileid", "8a8082853aac9311013aae80688e0011"));
        System.out.println("Executed: " + invocation5.toJsonObject());

        EscWorkflowInvocation invocation6 = c.executeWorkflowWithParameters(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"), v.getId(), params);
        System.out.println("Executed: " + invocation6.toJsonObject());

        EscWorkflowInvocation[] runs = c.listInvocationsOfWorkflow(TestSettingsManager.getProperty("externalrestworkflowtest.workflowid", "14293"));
        for(int i=0;i<runs.length;i++){
            System.out.println("Run: " + i + ": " + runs[i].getId());
            EscWorkflowInvocation inv = c.getInvocation(runs[i].getId());
            System.out.println("Retrieved: " + inv.toJsonObject());
        }

        c.terminateInvocation(invocation6.getId());        
    }
}
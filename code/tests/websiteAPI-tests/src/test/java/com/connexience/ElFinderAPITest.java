package com.connexience;


import com.connexience.server.rest.api.ILogonAPI;
import com.connexience.server.rest.api.IMessagesAPI;
import com.connexience.server.rest.api.IUsersAPI;
import com.connexience.server.rest.model.WebsiteUser;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@SuppressWarnings("unchecked")
public class ElFinderAPITest extends TestCase {

    private String baseURL = "http://localhost:8080/websiteapi/rest/";
    private String elFinderUrl = "http://localhost:8080/websiteapi/elfinder";
    private String downloadUrl = "http://localhost:8080/websiteapi/download";


    //todo: refactor to common settings class
    private String user1Id = "52";
    private String user1UserName = "simon.woodman@ncl.ac.uk";
    private String user1Password = "password";
    private String user1FirstName = "Simon";

    private String user2Id = "176";
    private String user2UserName = "h.g.hiden@ncl.ac.uk";
    private String user2Password = "password";

    DefaultHttpClient httpClient = new DefaultHttpClient();
    IUsersAPI usersAPI = null;
    ILogonAPI logonAPI = null;
    IMessagesAPI messagesAPI = null;

    static {
        // this initialization only needs to be done once per VM
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ElFinderAPITest(String testName) {
        super(testName);

        //Use the same httpClient for all of the APIs so that we share the session

        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);

        logonAPI = ProxyFactory.create(ILogonAPI.class, baseURL + "account/", clientExecutor);
        usersAPI = ProxyFactory.create(IUsersAPI.class, baseURL + "users/", clientExecutor);

    }
    public void testGetFolders() throws Exception {

        //login
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        String sessionIdWithJSON = (String) loginResponse.getEntity(String.class);
        Assert.assertNotNull(sessionIdWithJSON);
        loginResponse.releaseConnection();

        WebsiteUser user = usersAPI.getCurrentUser(false);
        String sessionId = getSessionId(sessionIdWithJSON);


        //Do a get call to elfinder to get the home directory contents
        String url = elFinderUrl + "?vol0=" + user.getHomeFolderId() + "&cmd=open&target=&init=1&tree=1&_=1361353331183";
        HttpGet httpget = new HttpGet(url);
        httpget.setHeader("Cookie", "JSESSIONID=" + sessionId + ";");
        HttpResponse response = httpClient.execute(httpget);

        //read the response
        HttpEntity entity = response.getEntity();
        BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));

        String s;
        String res = "";
        while ((s = br.readLine()) != null) {
            System.out.println("s = " + s);
            Assert.assertNotNull(s);
            Assert.assertTrue(!s.isEmpty());
            res += s;
        }

        JSONObject elfinderResponse = new JSONObject(res);
        Assert.assertNotNull(elfinderResponse);
    }

    public void testDownload() throws Exception{

        //login
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        String sessionIdWithJSON = (String) loginResponse.getEntity(String.class);
        Assert.assertNotNull(sessionIdWithJSON  );
        loginResponse.releaseConnection();

        String sessionId = getSessionId(sessionIdWithJSON);

        String url = downloadUrl + "/pcadata.txt?documentid=174";
        HttpGet httpget = new HttpGet(url);
        httpget.setHeader("Cookie", "JSESSIONID=" + sessionId + ";");
        HttpResponse response = httpClient.execute(httpget);

        Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);

    }

    private String getSessionId(String jsonSessionInfo)throws Exception
    {
        JSONObject jsonObject = new JSONObject(jsonSessionInfo);
        return jsonObject.getString("sessionId");

    }

}

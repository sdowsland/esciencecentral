/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * This simple class loads a Properties file from the .inkspot directory in the
 * home directory so that tests can use them.
 * @author hugo
 */
public class TestSettingsManager {
    private static Properties props = new Properties();
    private static Logger logger = Logger.getLogger(TestSettingsManager.class);
    
    static {
        File propertiesFile = new File(System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "tests.properties");
        if(propertiesFile.exists()){
            try {
                props.load(new FileInputStream(propertiesFile));
            } catch (Exception e){
                logger.error("Error loading tests.properties file: " + e.getMessage());
            }
        } else {
            props.put("hostname", "localhost");
            props.put("httpport", "8080");
            props.put("user1Id", "52");
            props.put("user1Username", "simon.woodman@ncl.ac.uk");
            props.put("user1Password", "password");
            props.put("user1Firstname", "Simon");
            props.put("user2Id", "176");
            props.put("user2Username", "h.g.hiden@ncl.ac.uk");
            props.put("user2Password", "password");
            props.put("downloadFileId","161");
            writeProperties();
        }
    }
    
    private static void writeProperties(){
        try {
            File propertiesFile = new File(System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "tests.properties");
            props.store(new FileOutputStream(propertiesFile), "Test suite configuration");
        } catch (Exception e){
            logger.error("Error writing tests.properties file " + e.getMessage());
        }        
    }
    
    public static String getUsername(){
        return props.getProperty("username", "user1");
    }
    
    public static String getPassword(){
        return props.getProperty("password", "password");
    }
    
    public static int getHttpPort(){
        String port = props.getProperty("httpport", "8080");
        return Integer.parseInt(port);
    }
    
    public static String getHostname(){
        return props.getProperty("hostname", "localhost");
    }
    
    public static String getProperty(String name, String defaultValue){
        if(props.containsKey(name)){
            return props.getProperty(name);
        } else {
            logger.warn("tests.properties does not contain: " + name + " writing default value of: " + defaultValue);
            props.put(name, defaultValue);
            writeProperties();
            return defaultValue;
        }
    }
    
    public static File getDataDirectory(){
        File dir = new File(getProperty("datadirectory", System.getProperty("user.home") + File.separator + ".inkspot" + File.separator + "testdata"));
        if(!dir.exists()){
            dir.mkdirs();
        }
        return dir;
    }
}

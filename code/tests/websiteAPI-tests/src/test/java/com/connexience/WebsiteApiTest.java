/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.rest.api.*;
import com.connexience.server.rest.model.*;
import com.connexience.server.rest.model.project.study.WebsiteStudy;
import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.ClientResponseFailure;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class WebsiteApiTest extends TestCase {

    private String baseURL = "http://" + TestSettingsManager.getHostname() + ":" + TestSettingsManager.getHttpPort() + "/website-api/rest/";

    private String user1Id = TestSettingsManager.getProperty("user1Id", "52");
    private String user1UserName = TestSettingsManager.getProperty("user1Username", "simon.woodman@ncl.ac.uk"); //"simon.woodman@ncl.ac.uk";
    private String user1Password = TestSettingsManager.getProperty("user1Password", "password"); //"password";
    private String user1FirstName = TestSettingsManager.getProperty("user1Firstname", "Simon"); //"Simon";

    private String user2Id = TestSettingsManager.getProperty("user2Id", "176"); //"176";
    private String user2UserName =TestSettingsManager.getProperty("user2Username", "h.g.hiden@ncl.ac.uk");// "h.g.hiden@ncl.ac.uk";
    private String user2Password = TestSettingsManager.getProperty("user2Password", "password");// "password";

    private String dataId = TestSettingsManager.getProperty("downloadFileId", "161"); //"161";


    IUsersAPI usersAPI = null;
    ILogonAPI logonAPI = null;
    IMessagesAPI messagesAPI = null;
    IStudyAPI studyAPI = null;
    ISearchAPI searchAPI = null;

    static {
        // this initialization only needs to be done once per VM
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public WebsiteApiTest(String testName) {
        super(testName);

        //Use the same httpClient for all of the APIs so that we share the session
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);

        usersAPI = ProxyFactory.create(IUsersAPI.class, baseURL + "users/", clientExecutor);
        logonAPI = ProxyFactory.create(ILogonAPI.class, baseURL + "account/", clientExecutor);
        messagesAPI = ProxyFactory.create(IMessagesAPI.class, baseURL + "messages/", clientExecutor);
        studyAPI = ProxyFactory.create(IStudyAPI.class, baseURL + "study/", clientExecutor);
        searchAPI = ProxyFactory.create(ISearchAPI.class, baseURL + "search/", clientExecutor);

    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(WebsiteApiTest.class);
    }

    @SuppressWarnings("unchecked")
    public void testLoginAndProfile() throws Exception {


        WebsiteUser user = usersAPI.getUser(user1Id);

        //check that we have the correct user
        Assert.assertEquals(user.getFirstname(), user1FirstName);

        //friends should be null before we login as we're viewing the profile as the public user
        Assert.assertNull(user.getFriends());

        //should be able to login
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        String sessionId = (String) loginResponse.getEntity(String.class);
        Assert.assertNotNull(sessionId);
        loginResponse.releaseConnection();

        //should be able to logout
        ClientResponse logoutResponse = (ClientResponse) logonAPI.logout();
        Assert.assertEquals(logoutResponse.getStatus(), Response.Status.OK.getStatusCode());
        logoutResponse.releaseConnection();

        //shouldn't be able to login with incorrect details
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, "incorrectPassword");
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
        loginResponse.releaseConnection();
        //Assert.assertFalse(loginResponse.isSuccess());

        //login again
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        user = usersAPI.getUser(user1Id);

        //should have friends and groups details
        Assert.assertNotNull(user.getFriends());
        Assert.assertNotNull(user.getGroups());

        //change the email and dashboard prefs
        String newEmail = "another.email@ncl.ac.uk";
        user.setEmail(newEmail);
        user.setDashboardPrefs("Some prefs");
        ClientResponse saveUserRes = (ClientResponse) usersAPI.saveUser(user.getId(), user);
        Assert.assertEquals(saveUserRes.getStatus(), Response.Status.OK.getStatusCode());
        saveUserRes.releaseConnection();

        //Check the email has been changed
        WebsiteUser newUser = usersAPI.getUser(user.getId());
        Assert.assertEquals(newEmail, newUser.getEmail());
        Assert.assertEquals(newUser.getDashboardPrefs(), "Some prefs");

        //change the email back
        newUser.setEmail(user1UserName);
        newUser.setDashboardPrefs("");
        ClientResponse saveUserResponse = (ClientResponse) usersAPI.saveUser(newUser.getId(), user);
        saveUserResponse.releaseConnection();

        List<WebsiteUser> users = usersAPI.getUsers(OrderBy.id, SearchOrder.DESC, 1, 10);
        Assert.assertTrue(users.size() > 0);

        List<WebsiteUser> usersOrderedBySurname = usersAPI.getUsers(OrderBy.surname, SearchOrder.ASC, 1, 10);
        Assert.assertNotSame(users.get(0), usersOrderedBySurname.get(0));

        //check that we can't change someone elses profile
        WebsiteUser hugo = usersAPI.getUser(user2Id);

        //shouldn't be able to see his friends
        Assert.assertNull(hugo.getFriends());

        hugo.setEmail("should.fail@ncl.ac.uk");
        ClientResponse res = (ClientResponse) usersAPI.saveUser(hugo.getId(), hugo);
        Assert.assertEquals(res.getStatus(), Response.Status.FORBIDDEN.getStatusCode());
        res.releaseConnection();

        WebsiteUser hugo2 = usersAPI.getUser(user2Id);
        Assert.assertEquals(hugo2.getEmail(), user2UserName);


    }

    public void testGettingCurrentUser() throws Exception {

        try {
            usersAPI.getCurrentUser(false);
            Assert.fail(); //Should throw 401 getting public user
        } catch (ClientResponseFailure expected) {
            if (expected.getResponse().getStatus() != HttpURLConnection.HTTP_UNAUTHORIZED) {
                Assert.fail();  //Should hve been 401
            }
        }

        //should be able to login
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //should be able to get the current user
        WebsiteUser user = usersAPI.getCurrentUser(false);
        Assert.assertEquals(user.getFirstname(), user1FirstName);

        //Should be able to get the metadata for the current user

        user = usersAPI.getCurrentUser(true);
        for(WebsiteMetadataItem md : user.getMetadata()){
            System.out.println(md.getName() + " : " + md.getStringValue());
        }
    }

    public void testSearchingForUsers() throws Exception {

        //should be able to login
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //should be able to get the current user
        List<WebsiteUserMin> searchResults = searchAPI.searchUsers("sim", OrderBy.firstName, SearchOrder.ASC, 1, 1000);
        Assert.assertTrue(searchResults.size() > 0);
        Assert.assertTrue(searchResults.get(0).getFirstname().equals("Simon"));


    }

    public void testIsLoggedIn() throws Exception {
        //check that we're logged out
        ClientResponse loginResponse = (ClientResponse) logonAPI.logout();
        loginResponse.releaseConnection();

        //Should return bad request when no user logged in
        ClientResponse publicUserLoggedIn = (ClientResponse) logonAPI.isNonPublicUser();
        Assert.assertEquals(publicUserLoggedIn.getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
        publicUserLoggedIn.releaseConnection();

        //login again
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        ClientResponse realUserLoggedInResponse = (ClientResponse) logonAPI.isNonPublicUser();
        Assert.assertEquals(realUserLoggedInResponse.getStatus(), Response.Status.OK.getStatusCode());
        realUserLoggedInResponse.releaseConnection();
    }

    public void testAddingMetadata() throws Exception {
        //login again
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        WebsiteUser simon = usersAPI.getUser(user1Id);
        String currentProfileText = simon.getBio();
        String newText = "Lorem fucking ipsum again";

        //change the bio text and check that it's changed
        simon.setBio(newText);
        ClientResponse saveUserResponse = (ClientResponse) usersAPI.saveUser(simon.getId(), simon);
        Assert.assertEquals(saveUserResponse.getStatus(), Response.Status.OK.getStatusCode());
        saveUserResponse.releaseConnection();

        simon = usersAPI.getUser(user1Id);
        Assert.assertEquals(simon.getBio(), newText);

        //change it back
        simon.setBio(currentProfileText);
        saveUserResponse = (ClientResponse) usersAPI.saveUser(simon.getId(), simon);
        Assert.assertEquals(saveUserResponse.getStatus(), Response.Status.OK.getStatusCode());
        saveUserResponse.releaseConnection();
    }


    public void testStudy() throws Exception {
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //Count the number of studies
        Long studyCount = studyAPI.getStudyCount();
        Assert.assertTrue(studyCount >= 0);
        Long  myStudyCount = studyAPI.getUserStudyCount();
        Assert.assertTrue(myStudyCount >= 0);
        System.out.println("myStudyCount = " + myStudyCount);

        //create a study
        String studyName = "Newcastle 85+";
        WebsiteStudy study = new WebsiteStudy();
        study.setName(studyName);
        study.setDescription("Some desciption");
        study.setStartDate(new Date());
        study.setEndDate(new Date());
        study.setId(null);

        study = studyAPI.saveStudy(study);
        Assert.assertEquals(studyName, study.getName());
        Assert.assertTrue(study.getId() > 0);

        //check that the count has been updated
        Long  newStudyCount = studyAPI.getStudyCount();
        Assert.assertTrue(newStudyCount == studyCount + 1);

        Long  myNewStudyCount = studyAPI.getUserStudyCount();
        Assert.assertTrue(myNewStudyCount == myStudyCount + 1);


        //update the study
        String newDescription = "Another Description";
        study.setDescription(newDescription);
        study = studyAPI.saveStudy(study);
        Assert.assertEquals(study.getDescription(), newDescription);

        String groupId = study.getAdminGroupId();
        WebsiteGroup adminGroup = usersAPI.getGroup(groupId);
        Assert.assertTrue(adminGroup.getMembers().size() > 0);
        Assert.assertTrue(adminGroup.getName() != null);

        //Hugo shouldn't be able to update the study
        loginResponse = (ClientResponse) logonAPI.login(user2UserName, user2Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();
        WebsiteUser hugo = usersAPI.getCurrentUser(false);
        try {
            study.setDescription("Hugo's update");
            studyAPI.saveStudy(study);
            Assert.fail("Hugo shouldn't be able to update the study");
        } catch (Exception e) {
            System.out.println();
            //do nothing
        }

        //Log back in as Simon
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //Check that we can list studies
        List<WebsiteStudy> studies = studyAPI.getStudies(1, 10);
        Assert.assertTrue(studies.size() <= 10);
        Assert.assertTrue(studies.size() > 0);
        System.out.println("studies.size() = " + studies.size());

        List<WebsiteStudy> userStudies = studyAPI.getUserStudies(1, 10);
        Assert.assertTrue(userStudies.size() > 0);

        //Make the study private
        study.setPrivateStudy(true);
        study = studyAPI.saveStudy(study);
        study = studyAPI.getStudy(study.getId());
        Assert.assertTrue(study.isPrivateStudy());

        //Log in as hugo
        loginResponse = (ClientResponse) logonAPI.login(user2UserName, user2Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //Shouldn't be able to get the study
        try {
            study = studyAPI.getStudy(study.getId());
            Assert.fail("Hugo shouldn't be able to see private study");
        } catch (Exception e) {
            //do nothing
        }

        List<WebsiteStudy> studyList = studyAPI.getStudies(1, 10000);
        Assert.assertTrue(!studyList.contains(study));

        //Log back in as Simon
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //Add Hugo as an admin
        ClientResponse addAdminResponse = (ClientResponse) studyAPI.addAdminToStudy(study.getId(), user2Id);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), addAdminResponse.getStatus());
        addAdminResponse.releaseConnection();

        //Log in as hugo
        loginResponse = (ClientResponse) logonAPI.login(user2UserName, user2Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //Check that we can now get the study
        WebsiteStudy newStudy = studyAPI.getStudy(study.getId());
        studyList = studyAPI.getStudies(1, 10000);
//        Assert.assertTrue(studyList.contains(study));
        Assert.assertEquals(study.getId(), newStudy.getId());

        //remove hugo from the admin group

        ClientResponse removeResponse = (ClientResponse) studyAPI.removeAdminFromStudy(study.getId(), hugo.getId());
        removeResponse.releaseConnection();

        //now can't get the study
        //Shouldn't be able to get the study
        try {
            study = studyAPI.getStudy(study.getId());
            Assert.fail("Hugo shouldn't be able to see private study");
        } catch (Exception e) {
            //do nothing
        }

        //Log back in as Simon
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();

        //Should not be able to add users who don't exist to the study
        addAdminResponse = (ClientResponse) studyAPI.addMemberToStudy(study.getId(), "1111111111111");
        Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), addAdminResponse.getStatus());
        addAdminResponse.releaseConnection();


        //add some data to a study
//        ClientResponse addDataResponse = (ClientResponse) studyAPI.addDataToStudy(study.getId(), dataId);
//        Assert.assertEquals(addDataResponse.getStatus(), Response.Status.OK.getStatusCode());
//        addDataResponse.releaseConnection();


//        List<WebsiteData> data = studyAPI.getStudyData(study.getId(), 1, 1000);
//        Assert.assertTrue(data.size() > 0);


        //Log back in as Simon
        loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        loginResponse.releaseConnection();


        //delete the study
        ClientResponse deleteResponse = (ClientResponse) studyAPI.deleteStudy(study.getId());
        deleteResponse.releaseConnection();

        try {
            studyAPI.getStudy(study.getId());
            Assert.fail("Study should have been deleted");
        } catch (Exception e) {
            //do nothing
        }

        studyList = studyAPI.getStudies(1, 100000);
        for (WebsiteStudy ws : studyList) {
            deleteResponse = (ClientResponse) studyAPI.deleteStudy(ws.getId());
            deleteResponse.releaseConnection();
        }

    }


}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.glacier.TreeHashGenerator;
import com.amazonaws.services.glacier.model.*;
import com.amazonaws.util.BinaryUtils;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public class UploadUtils
{
    private static final Logger logger = Logger.getLogger(UploadUtils.class.getName());

    public static String initiateUpload(AmazonGlacierClient amazonGlacierClient, String vaultName, String archiveDescription, int partSize)
    {
        String uploadId = null;
        try
        {
            InitiateMultipartUploadRequest initiateMultipartUploadRequest = new InitiateMultipartUploadRequest();
            initiateMultipartUploadRequest.withVaultName(vaultName);
            initiateMultipartUploadRequest.withArchiveDescription(archiveDescription);
            initiateMultipartUploadRequest.withPartSize(String.valueOf(partSize));

            InitiateMultipartUploadResult initiateMultipartUploadResult = amazonGlacierClient.initiateMultipartUpload(initiateMultipartUploadRequest);
            if (initiateMultipartUploadResult != null)
                uploadId = initiateMultipartUploadResult.getUploadId();
            else
                logger.warn("Unable of obtain upload id");
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ", throwable);
        }

        return uploadId;
    }

    public static List<byte[]> doUpload(AmazonGlacierClient amazonGlacierClient, String uploadId, String vaultName, InputStream inputStream, int partSize, long length)
    {
        try
        {
            byte[]       partBuffer      = new byte[Integer.valueOf(partSize)];
            List<byte[]> binaryChecksums = new LinkedList<>();

            boolean finished        = false;
            int     currentPosition = 0;
            while ((currentPosition < length) && (! finished))
            {
                int partReadLength = inputStream.read(partBuffer, 0, partBuffer.length);

                if (partReadLength != -1)
                {
                    ByteArrayInputStream partInputStream = new ByteArrayInputStream(partBuffer, 0, partReadLength);
                    String               contentRange    = String.format("bytes %s-%s/*", currentPosition, currentPosition + partReadLength - 1);
                    String               checksum        = TreeHashGenerator.calculateTreeHash(partInputStream);
                    byte[]               binaryChecksum  = BinaryUtils.fromHex(checksum);
                    binaryChecksums.add(binaryChecksum);
                    partInputStream = new ByteArrayInputStream(partBuffer, 0, partReadLength);

                    UploadMultipartPartRequest uploadMultipartPartRequest = new UploadMultipartPartRequest();
                    uploadMultipartPartRequest.withUploadId(uploadId);
                    uploadMultipartPartRequest.withVaultName(vaultName);
                    uploadMultipartPartRequest.withChecksum(checksum);
                    uploadMultipartPartRequest.withRange(contentRange);
                    uploadMultipartPartRequest.withBody(partInputStream);

                    UploadMultipartPartResult uploadMultipartPartResult = amazonGlacierClient.uploadMultipartPart(uploadMultipartPartRequest);
                    if (uploadMultipartPartResult != null)
                        System.out.println("UploadId = [" + uploadMultipartPartResult.getChecksum() + "]");
                    else
                        finished = true;

                    partInputStream.close();
                    
                    currentPosition = currentPosition + partReadLength;
                }
                else
                    finished = true;
            }
            
            return binaryChecksums;
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ", throwable);
        }
        
        return null;
    }

    public static String completeUpload(AmazonGlacierClient amazonGlacierClient, String uploadId, String vaultName, String fullChecksum, long length)
    {
        try
        {
            CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest();
            completeMultipartUploadRequest.withUploadId(uploadId);
            completeMultipartUploadRequest.withVaultName(vaultName);
            completeMultipartUploadRequest.withChecksum(fullChecksum);
            completeMultipartUploadRequest.withArchiveSize(String.valueOf(length));

            CompleteMultipartUploadResult completeMultipartUploadResult = amazonGlacierClient.completeMultipartUpload(completeMultipartUploadRequest);
            if (completeMultipartUploadResult != null)
            {
                logger.info("ArchiveId = [" + completeMultipartUploadResult.getArchiveId());
                return completeMultipartUploadResult.getArchiveId();
            }
            else
                logger.info("No 'complete multipart upload' result");
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ", throwable);
        }
        
        return null;
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.workflow;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.provenance.model.logging.events.WorkflowExecuteOperation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.SerializedInvocationMessage;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.util.RandomGUID;
import com.connexience.server.util.SerializationUtils;
import com.connexience.provenance.client.ProvenanceLoggerClient;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;
import java.util.Date;

/**
 * This bean provides the workflow enactment system bean.
 *
 * @author nhgh
 */
@Stateless
@EJB(name = "java:global/ejb/WorkflowEnactmentBean", beanInterface = WorkflowEnactmentRemote.class)
public class WorkflowEnactmentBean extends HibernateSessionContainer implements WorkflowEnactmentRemote {
    private static Logger logger = Logger.getLogger(WorkflowEnactmentBean.class);
    
    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;

    /**
     * Send the workflow invocation message to the apache message queue
     */
    private void sendMessage(WorkflowInvocationMessage message, int priority) throws Exception {
        // Get connection
        Connection connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword())
                : connectionFactory.createConnection();
        try {
            Session session = connection.createSession(true, Session.SESSION_TRANSACTED);
            Queue queue = session.createQueue("Workflow");

            MessageProducer publisher = session.createProducer(queue);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Create the message
            BytesMessage bm = session.createBytesMessage();

            byte[] data = SerializationUtils.serialize(message);
            bm.writeBytes(data);
            long ttl = 0;

            publisher.send(bm, DeliveryMode.NON_PERSISTENT, priority, ttl);

        } finally {
            connection.close();
        }
    }

    /**
     * Start a workflow running
     */
    public String startWorkflow(Ticket ticket, WorkflowInvocationMessage invocationMessage) throws ConnexienceException {
        try {
            // Get the workflow object
            WorkflowDocument document = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, invocationMessage.getWorkflowId());

            if (document != null) {
                String invocationId = "";
                // Work out the correct engine to send this workflow to
                if (document.getEngineType().equals(WorkflowDocument.DYNAMIC_ENGINE)) {
                    // DYNAMIC workflow engine

                    // Check to see if a version ID has been specified. If not,
                    // add one in
                    if (invocationMessage.getVersionId() == null || invocationMessage.getVersionId().trim().equals("")) {
                        DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, document.getId());
                        if (version != null) {
                            invocationMessage.setVersionId(version.getId());
                            invocationMessage.setUseLatest(false);
                        }
                    }

                    // Results capture options
                    invocationMessage.setDeletedOnSuccess(document.isDeletedOnSuccess());
                    invocationMessage.setOnlyFailedOutputsUploaded(document.isOnlyFailedOutputsUploaded());

                    // Single VM status
                    invocationMessage.setSingleVmMode(document.isSingleVmMode());
                    
                    // An invocation ID needs to be randomly assigned to this
                    // invocation so that it
                    // can be passed back to the client. This invocation ID is
                    // then used by the
                    // remote engine to track the workflow
                    invocationId = new RandomGUID().toString();
                    invocationMessage.setInvocationId(invocationId);

                    WorkflowInvocationFolder folder = WorkflowEJBLocator
                            .lookupWorkflowManagementBean()
                            .createInvocationFolder(ticket, invocationId, document.getId(), invocationMessage.getVersionId());
                    folder.setInvocationId(folder.getId());
                    
                    // Rename the folder if needed
                    if(invocationMessage.getResultsFolderName()!=null){
                        folder.setName(invocationMessage.getResultsFolderName());
                    }
                    
                    // Is there a parent workflow InvocationID specified. If so
                    // put the results folder into the parent folder. If the workflow
                    // is to be attached to a lock, this step is not performed
                    if(!invocationMessage.isLockMember() && invocationMessage.getParentInvocationId()!=null){
                        WorkflowInvocationFolder parent = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationMessage.getParentInvocationId());
                        if(parent!=null){
                            folder.setContainerId(parent.getId());
                        }
                    }

                    folder.setQueuedTime(new Date());
                    folder = (WorkflowInvocationFolder) savePlainObject(folder);
                    invocationMessage.setResultsFolderId(folder.getId());
                    invocationMessage.setInvocationId(folder.getId());
                    invocationId = folder.getId();

                    // Set input block if required
                    if (invocationMessage.getTargetFileId() != null) {
                        invocationMessage.setInputBlockName(document.getExternalDataBlockName());
                    }

                    // Should the folder be attached to a workflow lock
                    if (invocationMessage.isLockMember()) {
                        WorkflowEJBLocator.lookupWorkflowLockBean().attachInvocationToLock(ticket, invocationMessage.getLockId(), folder);
                    }

                    // Store the invocation message
                    try {
                        SerializedInvocationMessage storedMessage = new SerializedInvocationMessage(invocationMessage);
                        savePlainObject(storedMessage);
                    } catch (Exception e) {
                        System.out.println("Error storing invocation message: " + e.getMessage());
                    }

                    //High priority for Admin users
                    int priority = 4;
                    if(isOrganisationAdminTicket(ticket)){
                        priority = 9;
                    }

                    // Try and send an MQ message
                    sendMessage(invocationMessage, priority);

                    //log that this workflow has started executing
                    //Set up the provenance message to be sent
                    WorkflowExecuteOperation wfProvMsg = new WorkflowExecuteOperation(invocationMessage.getWorkflowId(), invocationMessage.getVersionId(), invocationId, document.getName(), ticket.getUserId(), new Date());

                    //If there is a parent workflow then add this to the provenance
                    if(invocationMessage.getParentInvocationId()!=null){
                        WorkflowInvocationFolder parent = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationMessage.getParentInvocationId());
                        if(parent!=null){
                            //Set the parent information
                            wfProvMsg.setParentInvocationId(invocationMessage.getParentInvocationId());
                            wfProvMsg.setParentWorkflowId(parent.getWorkflowId());
                            wfProvMsg.setParentWorkflowVersionId(parent.getVersionId());
                        }
                    }

                    ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                    provClient.log(wfProvMsg);

                } else {
                    // STATIC workflow engine
                    throw new ConnexienceException("Static workflows no longer supported");
                }

                // log that the workflow has started
                String versionId;
                if (invocationMessage.isUseLatest()) {
                    versionId = String.valueOf(document.getCurrentVersionNumber());
                } else {
                    versionId = invocationMessage.getVersionId();
                }
                
                // TODO: ADD LOGGING - LOG WORKFLOW STARTED
                
                return invocationId;

            } else {
                throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
            }
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error starting workflow: " + e.getMessage());
        }
    }

    @Override
    public String resubmitWorkflow(Ticket ticket, String invocationId) throws ConnexienceException {
        // Get the invocation
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
        if (invocation != null) {
            if (invocation.getInvocationStatus() == WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS) {
                SerializedInvocationMessage serializedMessage = WorkflowEJBLocator.lookupWorkflowManagementBean().getSerializedMessage(ticket, invocationId);
                if (serializedMessage != null) {
                    WorkflowInvocationMessage invocationMessage = null;
                    try {
                        invocationMessage = serializedMessage.deserializeMessage();
                    } catch (Exception e) {
                        throw new ConnexienceException("Error deserializing stored message: " + e.getMessage(), e);
                    }

                    // Get the invocation folder
                    WorkflowInvocationFolder folder = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
                    invocationMessage.setResultsFolderId(folder.getId());
                    folder.reset();
                    folder = WorkflowEJBLocator.lookupWorkflowManagementBean().saveInvocationFolder(ticket, folder);

                    // Empty the invocation folder
                    EJBLocator.lookupStorageBean().removeFolderContents(ticket, folder.getId());
                    
                    // Should the folder be attached to a workflow lock
                    if (invocationMessage.isLockMember()) {
                        // TODO: Reattach to lock
                        logger.warn("Resubmitted workflow is part of a lock. REATTACHING NOT YET IMPLEMENTED");
                    }

                    // Remove all of the service logs for the previous run
                    WorkflowEJBLocator.lookupWorkflowManagementBean().deleteServiceLogs(ticket, invocationId);

                    // Resumbit message
                    try {

                        int priority = 5;
                        if(isOrganisationAdminTicket(ticket)){
                            priority = 9;
                        }

                        sendMessage(invocationMessage, priority);
                    } catch (Exception e) {
                        throw new ConnexienceException("Error sending workflow message: " + e.getMessage(), e);
                    }

                    return folder.getInvocationId();

                } else {
                    throw new ConnexienceException("Cannot find stored message during workflow resubmit");
                }

            } else {
                throw new ConnexienceException("Workflow cannot be resubmitted before it fails or is terminated");
            }
        } else {
            throw new ConnexienceException("No such invocation");
        }
    }
}
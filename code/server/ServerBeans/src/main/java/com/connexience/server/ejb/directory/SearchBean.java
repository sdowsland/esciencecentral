/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Tag;
import com.connexience.server.model.social.TagCloudElement;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.model.workflow.WorkflowDocument;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Author: Simon
 * Date: Jun 11, 2009
 */
@Stateless
@EJB(name = "java:global/ejb/SearchBean", beanInterface = SearchRemote.class)
public class SearchBean extends HibernateSessionContainer implements SearchRemote
{

  public SearchBean()
  {
  }

  /************************************/
  /* FREE TEXT SEARCHING METHODS      */
  /************************************/

  public int countFreeTextSearch(Ticket ticket, String searchText, Vector<String> advancedOptions) throws ConnexienceException
  {
    Connection conn = null;
    try
    {

      String userId = ticket.getUserId();
      String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

      conn = getSQLConnection();

      String advOptionsString = "";
           for(String s : advancedOptions)
           {
             if(s.equals("u"))
               advOptionsString += " o.objecttype = 'User.class' OR o.objecttype = 'UserProfile.class' OR ";
             else if(s.equals("tg"))
                       advOptionsString += " o.objecttype = 'Tag.class' OR ";
             else if(s.equals("m"))
                       advOptionsString += " o.objecttype = 'TextMessage.class' OR ";
             else if(s.equals("g"))
                       advOptionsString += " o.objecttype = 'Group.class' OR ";
             else if(s.equals("a"))
                       advOptionsString += " o.objecttype = 'Application.class' OR ";
             else if(s.equals("w"))
                       advOptionsString += " o.objecttype = 'Workflow.class' OR ";
             else if(s.equals("s"))
                       advOptionsString += " o.objecttype = 'WorkflowService.class' OR ";
             else if(s.equals("d"))
                       advOptionsString += " o.objecttype = 'DocumentRecord.class' OR ";
             else if(s.equals("f"))
                       advOptionsString += " o.objecttype = 'Folder.class' OR ";
             
           }

           if(!advOptionsString.equals(""))
             advOptionsString = " AND (" + advOptionsString.substring(0, advOptionsString.length() - 3) + " ) ";


      String q = "SELECT count(*)" +
          "FROM search_union as o\n" +
          "WHERE\n" +
          "(o.creatorId = '" + userId + "'\n" +
          "OR \n" +
          "o.id IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type = 'read' OR p.type = 'write') AND  (p.principalid = '" + userId + "' OR p.principalId = '" + publicUserId + "'))\n" +
          "OR \n" +
          "o.id IN (SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userid = '" + userId + "' AND gm.groupid = perm.principalId )\n" +
          "OR \n" +
          "o.containerid IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type = 'read' OR p.type = 'write') AND  (p.principalid = '" + userId + "' OR p.principalId = '" + publicUserId + "'))\n" +
          "OR o.containerid IN (\n" +
          "SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userid = '" + userId + "' AND gm.groupid = perm.principalId )" +
          "OR o.objecttype = 'Tag.class' OR o.objecttype = 'UserProfile.class' OR o.objecttype = 'User.class') " +
           advOptionsString +
          " AND to_tsvector(coalesce(o.name, '') || ' ' ||coalesce(o.title, '') || ' ' || coalesce(o.body, '') || ' ' || coalesce(o.description, '')) @@ plainto_tsquery('" + searchText + "')";

      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(q);

      rs.next();
      int numberOfResults = rs.getInt("count");

      rs.close();
      st.close();
      conn.close();
      return numberOfResults;
    }
    catch (Exception e)
    {
      throw new ConnexienceException(e);
    }
    finally
    {
      closeJDBConnection(conn);
    }
  }

  public List freeTextSearch(Ticket ticket, String searchText, Vector<String> advancedOptions, int start, int maxResults) throws ConnexienceException
  {
    Connection conn = null;
    try
    {

      String userId = ticket.getUserId();
      String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

      conn = getSQLConnection();

      String advOptionsString = "";
      for(String s : advancedOptions)
      {
        if(s.equals("u"))
          advOptionsString += " o.objecttype = 'User.class' OR o.objecttype = 'UserProfile.class' OR ";
        else if(s.equals("tg"))
                  advOptionsString += " o.objecttype = 'Tag.class' OR ";
        else if(s.equals("m"))
                  advOptionsString += " o.objecttype = 'TextMessage.class' OR ";
        else if(s.equals("g"))
                  advOptionsString += " o.objecttype = 'Group.class' OR ";
        else if(s.equals("a"))
                  advOptionsString += " o.objecttype = 'Application.class' OR ";
        else if(s.equals("w"))
                  advOptionsString += " o.objecttype = 'Workflow.class' OR ";
        else if(s.equals("s"))
                  advOptionsString += " o.objecttype = 'WorkflowService.class' OR ";
        else if(s.equals("d"))
                  advOptionsString += " o.objecttype = 'DocumentRecord.class' OR ";
        else if(s.equals("f"))
                  advOptionsString += " o.objecttype = 'Folder.class' OR ";
      }

      if(!advOptionsString.equals(""))
        advOptionsString = " AND (" + advOptionsString.substring(0, advOptionsString.length() - 3) + " ) ";

      String q = "SELECT ts_rank_cd(to_tsvector(coalesce(o.name, '') || ' ' ||coalesce(o.title, '') || ' ' || coalesce(o.body, '') || ' ' || coalesce(o.description, '')), plainto_tsquery('" + searchText + "')) as rank,* " +
          "FROM search_union as o " +
          "WHERE " +
          "(o.creatorId = '" + userId + "' " +
          "OR  " +
          "o.id IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type = 'read' OR p.type = 'write') AND (p.principalid = '" + userId + "' OR p.principalId = '" + publicUserId + "')) " +
          "OR  " +
          "o.id IN (SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userid = '" + userId + "' AND gm.groupid = perm.principalId ) " +
          "OR  " +
          "o.containerid IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type = 'read' OR p.type = 'write') AND  (p.principalid = '" + userId + "' OR p.principalId = '" + publicUserId + "')) " +
          "OR o.containerid IN ( " +
          "SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userid = '" + userId + "' AND gm.groupid = perm.principalId )" +
          "OR o.objecttype = 'Tag.class' OR o.objecttype = 'UserProfile.class' OR o.objecttype = 'User.class') " +
          advOptionsString +
          " AND to_tsvector(coalesce(o.name, '') || ' ' ||coalesce(o.title, '') || ' ' || coalesce(o.body, '') || ' ' || coalesce(o.description, '')) @@ plainto_tsquery('" + searchText + "')  " +
          "ORDER BY rank DESC";

      if (maxResults > 0)
      {
        q += " LIMIT " + maxResults;
      }

      if (start > 0)
      {
        q += " OFFSET " + start;
      }


      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(q);
      List results = new ArrayList();

      while (rs.next())
      {
        if (rs.getString("objecttype").equals("Tag.class"))
        {
          Tag t = new Tag();
          t.setId(rs.getString("id"));
          t.setTagText(rs.getString("name"));
          results.add(t);
        }
        else if (rs.getString("objecttype").equals("User.class"))
        {
          User u = new User();
          u.setId(rs.getString("id"));
          u.setName(rs.getString("name"));
          u.setFirstName(rs.getString("title"));
          u.setSurname(rs.getString("body"));
          u.setProfileId(rs.getString("description"));
          results.add(u);
        }
        else if (rs.getString("objecttype").equals("Group.class"))
        {
          Group g = new Group();
          g.setId(rs.getString("id"));
          g.setName(rs.getString("name"));
          g.setDescription(rs.getString("description"));
          results.add(g);
        }
        else if (rs.getString("objecttype").equals("Folder.class"))
        {
          Folder f = new Folder();
          f.setId(rs.getString("id"));
          f.setName(rs.getString("name"));
          f.setDescription(rs.getString("description"));
          results.add(f);
        }
        else if (rs.getString("objecttype").equals("Workflow.class"))
        {
          WorkflowDocument w = new WorkflowDocument();
          w.setId(rs.getString("id"));
          w.setName(rs.getString("name"));
          w.setDescription(rs.getString("description"));
          results.add(w);
        }
        else if (rs.getString("objecttype").equals("DocumentRecord.class"))
        {
          DocumentRecord d = new DocumentRecord();
          d.setId(rs.getString("id"));
          d.setName(rs.getString("name"));
          d.setDescription(rs.getString("description"));
          results.add(d);
        }
        else if (rs.getString("objecttype").equals("UserProfile.class"))
        {
          UserProfile up = new UserProfile();
          up.setId(rs.getString("containerid"));
          up.setText(rs.getString("name"));
          results.add(up);
        }
        else if (rs.getString("objecttype").equals("TextMessage.class"))
        {
          TextMessage tm = new TextMessage();
          tm.setId(rs.getString("id"));
          tm.setTitle(rs.getString("title"));
          tm.setThreadId(rs.getString("containerid"));
          tm.setDescription(rs.getString("body"));
          results.add(tm);
        }
      }

      rs.close();
      st.close();

      conn.close();
      return results;
    }
    catch (Exception e)
    {
      throw new ConnexienceException(e);
    }
    finally
    {
      closeJDBConnection(conn);
    }
  }

  /*******************************/
  /** TAG SEARCHING METHODS     **/
  /*******************************/

  public List tagSearch(Ticket ticket, String searchText, int start, int maxResults) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      String userId = ticket.getUserId();
      String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

      //strip the single quotes from the search otherwise it breaks the SQL
      //it might be better to escape the quotes but I don't know how much sense this makes.  Another option would be using the Postgres full text search as above
      searchText = searchText.replace("\'", "");

      Query q = session.createQuery("FROM ServerObject AS o WHERE o.id IN (SELECT tto.serverObjectId FROM TagToObject AS tto, Tag AS t WHERE tto.tagId = t.id AND t.tagText = :searchText)" +
          "AND (o.creatorId = '" + userId + "'" +
          "          OR" +
          "          o.id IN (SELECT p.targetObjectId FROM Permission AS p WHERE (p.type = 'read' OR p.type = 'write') AND  (p.principalId = '" + userId + "' OR p.principalId = '" + publicUserId + "'))" +
          "          OR  " +
          "          o.id IN (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userId = '" + userId + "' AND gm.groupId = perm.principalId )" +
          "          OR  " +
          "          o.containerId IN (SELECT p.targetObjectId FROM Permission AS p WHERE (p.type = 'read' OR p.type = 'write') AND (p.principalId = '" + userId + "' OR p.principalId = '" + publicUserId + "'))" +
          "          OR o.containerId IN ( " +
          "          SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userId = '" + userId + "' AND gm.groupId = perm.principalId ))");
      q.setString("searchText", searchText);

      if (maxResults > 0)
      {
        q.setMaxResults(maxResults);
      }

      if (start > 0)
      {
        q.setFirstResult(start);
      }

      return q.list();

    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error searching tags for:" + searchText, e);
    }
    finally
    {
      closeSession(session);
    }
  }

  public Integer countTagSearch(Ticket ticket, String searchText) throws ConnexienceException
  {
    Connection conn = null;
    try
    {
      conn = getSQLConnection();
      //strip the single quotes from the search otherwise it breaks the SQL
      //it might be better to escape the quotes but I don't know how much sense this makes.  Another option would be using the Postgres full text search as above
      searchText = searchText.replace("\'", "");

      String userId = ticket.getUserId();
      String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

      String q = "SELECT count(*) FROM objectsflat AS o WHERE o.id IN (SELECT tto.serverobjectid FROM tagstoobjects AS tto, tags AS t WHERE tto.tagid = t.id AND t.tagtext = '" + searchText + "')" +
          "AND" +
          "(o.creatorId = '" + userId + "' " +
          "OR  " +
          "o.id IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type = 'read' OR p.type = 'write') AND (p.principalid = '" + userId + "' OR p.principalId = '" + publicUserId + "')) " +
          "OR  " +
          "o.id IN (SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND gm.userid = '" + userId + "' AND gm.groupid = perm.principalId ) " +
          "OR  " +
          "o.containerid IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type = 'read' OR p.type = 'write') AND (p.principalid = '" + userId + "' OR p.principalId = '" + publicUserId + "')) " +
          "OR o.containerid IN ( " +
          "SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND gm.userid = '" + userId + "' AND gm.groupid = perm.principalId ))";


      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(q);
      rs.next();
      int numberOfResults = rs.getInt("count");

      rs.close();
      st.close();
      conn.close();
      return numberOfResults;

    }
    catch (Exception e)
    {
      throw new ConnexienceException(e);
    }
    finally
    {
      closeJDBConnection(conn);
    }
  }


  public List<TagCloudElement> getVisibleTags(Ticket ticket, int start, int maxResults) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      String userId = ticket.getUserId();
      String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

      Query q = session.createQuery("SELECT new com.connexience.server.model.social.TagCloudElementImpl(t.tagText, count(*)) " +
          "FROM TagToObject AS tto, Tag AS t WHERE t.id = tto.tagId AND tto.serverObjectId " +
          " IN( SELECT o.id FROM ServerObject AS o WHERE o.creatorId = '" + userId + "'" +
          "          OR" +
          "          o.id IN (SELECT p.targetObjectId FROM Permission AS p WHERE (p.type = 'read' OR p.type = 'write') AND  (p.principalId = '" + userId + "' OR p.principalId = '" + publicUserId + "'))" +
          "          OR  " +
          "          o.id IN (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userId = '" + userId + "' AND gm.groupId = perm.principalId )" +
          "          OR  " +
          "          o.containerId IN (SELECT p.targetObjectId FROM Permission AS p WHERE (p.type = 'read' OR p.type = 'write') AND (p.principalId = '" + userId + "' OR p.principalId = '" + publicUserId + "'))" +
          "          OR o.containerId IN ( " +
          "          SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE (perm.type = 'read' OR perm.type = 'write') AND  gm.userId = '" + userId + "' AND gm.groupId = perm.principalId )) GROUP BY t.tagText ORDER BY count(*) DESC ");

      if (maxResults > 0)
      {
        q.setMaxResults(maxResults);
      }

      if (start > 0)
      {
        q.setFirstResult(start);
      }

      return q.list();

    }
    catch (Exception e)
    {
      System.out.println("FIX TAGS ON SQL SERVER: " + e.getMessage());
      return new ArrayList<>();
    }
    finally
    {
      closeSession(session);
    }
  }


  /********************************/
  /** GENERAL SEARCH METHODS     **/
  /********************************/

  /*
 * Method to get the top n recently created documents
 * */
  public List getRecentlyAccessedDocuments(Ticket ticket, int start, int maxResults) throws ConnexienceException
  {
    Connection conn = null;
    try
    {
      conn = getSQLConnection();
      String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

      String q = "select distinct o.name, o.id, o.timeinmillis " +
          " from objectsflat AS o " +
          " where " +
          " (o.objecttype = 'DOCUMENTRECORD' OR o.objecttype = 'DOCUMENTVERSION') " +
          " AND" +
          " (o.name <> 'workflow.xml' AND o.name <> 'report.xml') " +
          "          AND " +
          "          (o.creatorId = '" + ticket.getUserId() + "' " +
          "          OR " +
          "          o.id IN (SELECT p.targetobjectid FROM permissions AS p WHERE (p.type <> 'execute' AND p.principalid = '" + ticket.getUserId() + "' OR p.principalId = '" + publicUserId + "')) " +
          "          OR" +
          "          o.id IN (SELECT perm.targetobjectid FROM permissions AS perm, groupmembership AS gm WHERE perm.type <> 'execute' AND gm.userid = '" + ticket.getUserId() + "' AND gm.groupid = perm.principalId )) " +
          "  order by o.timeinmillis desc";


      if (maxResults > 0)
      {
        q += " LIMIT " + maxResults;
      }

      if (start > 0)
      {
        q += " OFFSET " + start;
      }
      
      Statement st = conn.createStatement();

      ResultSet rs = st.executeQuery(q);
      List results = new ArrayList();

      while (rs.next())
      {
        DocumentRecord dr = new DocumentRecord();
        dr.setId(rs.getString("id"));
        dr.setName(rs.getString("name"));
        results.add(dr);
      }

      rs.close();
      st.close();
      conn.close();

      return results;
    }
    catch (Exception e)
    {
      System.out.println("FIX RECENTLY ACCESSED DOCUMENTS ON SQL SERVER: " + e.getMessage());
      return new ArrayList();
    }
    finally
    {
      closeJDBConnection(conn);
    }
  }


}



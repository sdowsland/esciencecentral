/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.social;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.social.Event;
import com.connexience.server.model.social.event.GroupEvent;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

/**
 * This class provides a bean to manage events and group events
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/EventBean", beanInterface = EventRemote.class)
public class EventBean extends HibernateSessionContainer implements EventRemote {

    @Override
    public void deleteEvent(Ticket ticket, Event event) throws ConnexienceException {
        try {
          if(event.getCreatorId().equals(ticket.getUserId())){
                EJBLocator.lookupObjectRemovalBean().remove(ticket, (Folder) event);
            } else if(isOrganisationAdminTicket(ticket)){
                EJBLocator.lookupObjectRemovalBean().remove(ticket, (Folder) event);
            } else {
                throw new ConnexienceException("User is not allowed to delete this event");
            }

        } catch (ConnexienceException ce){
            throw new ConnexienceException("Error deleting event: " + ce.getMessage(), ce);
        }
    }

    @Override
    public List listGroupEvents(Ticket ticket, String groupId) throws ConnexienceException {
        Session session = null;
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), groupId)){
                session = getSession();
                Query q = session.createQuery("from GroupEvent as obj where obj.groupId=?");
                q.setString(0, groupId);
                return q.list();
            } else {
                throw new ConnexienceException("User is not a group member");
            }
        } catch (ConnexienceException e){
            throw new ConnexienceException(e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** List all of the events for a group in a certain timeframe */
    public List listGroupEvents(Ticket ticket, String groupId, Date startDate, Date endDate) throws ConnexienceException {
        Session session = null;
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), groupId)){
                session = getSession();
                Query q = session.createQuery("from GroupEvent as obj where obj.groupId=? and obj.startDateTimestamp>=? and obj.endDateTimestamp<=?");
                q.setString(0, groupId);
                q.setLong(1, startDate.getTime());
                q.setLong(2, endDate.getTime());
                return q.list();
            } else {
                throw new ConnexienceException("User is not a group member");
            }
        } catch (ConnexienceException e){
            throw new ConnexienceException(e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public GroupEvent saveGroupEvent(Ticket ticket, GroupEvent event) throws ConnexienceException {
        try {
            if(event.getGroupId()!=null){
                if(EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), event.getGroupId())){

                    //check that containerId is set
                    if(event.getContainerId() == null)
                    {
                      Group g = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, event.getGroupId());
                      event.setContainerId(g.getEventsFolder());
                    }
                    event = (GroupEvent)saveObject(ticket, event);

                    // Make sure the group can see this event
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, event.getGroupId(), event.getId(), Permission.READ_PERMISSION);
                    return event;
                } else {
                    throw new Exception("User is not a member of this group");
                }
            } else {
                throw new Exception("No group ID specified in event");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error saving event: " + e.getMessage(), e);
        }
    }

    /** Get a group event by ID */
    public GroupEvent getGroupEvent(Ticket ticket, String id) throws ConnexienceException {
        try {
            GroupEvent event = (GroupEvent) getObject(id, GroupEvent.class);
            if(event != null){
                if(ticket.getUserId().equals(event.getCreatorId()) || EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), event.getGroupId())){
                    return event;
                } else {
                    throw new Exception("User is not allowed to see this group");
                }
            } else {
                return null;
            }

        } catch (Exception e){
            throw new ConnexienceException("Error getting group event: "+ e.getMessage(), e);
        }
    }
}

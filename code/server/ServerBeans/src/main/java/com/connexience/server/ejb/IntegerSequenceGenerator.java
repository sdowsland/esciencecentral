/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.ejb;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.jboss.logging.Logger;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Generate an id from the Postgres database sequence and return it (Integer). This is for backwards compatability with
 * the UUIDs which have been previously used in e-SC and the fact that the id field in ServerObject is a String.
 * However, smaller ids are desirable
 * <p/>
 * Requires a esc_sequence sequence in the database.
 * <p/>
 * User: nsjw7 Date: 03/12/2012
 */
public class IntegerSequenceGenerator implements IdentifierGenerator
{

	private static Logger log = Logger.getLogger(IntegerSequenceGenerator.class);

	/**
	 * Generate an Integer id from the database sequence
	 */
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException
	{
		Connection connection = session.connection();

		try
		{

			PreparedStatement ps = connection.prepareStatement("SELECT nextval ('esc_sequence') as nextval");
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				Integer id = rs.getInt("nextval");
				return id;
			}
			else
			{
				throw new HibernateException("Unable to generate Sequence - no result returned.  Does esc_sequence exist in the db?");
			}
		}
		catch (SQLException e)
		{
			log.error(e);
			throw new HibernateException("Unable to generate Sequence");
		}
	}
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.notifications;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.notifcations.Notification;
import com.connexience.server.model.properties.PropertyItem;
import com.connexience.server.model.security.Ticket;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA. User: martyn Date: 17-Nov-2009 Time: 10:37:24
 */

@Stateless
@EJB(name = "java:global/ejb/NotificationsBean", beanInterface = NotificationsRemote.class)
public class NotificationsBean extends HibernateSessionContainer implements NotificationsRemote
{
    @Resource(mappedName = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    public ArrayList<String> getNotificationKeys()
    {
        ArrayList<String> notificationKeys = new ArrayList<>();
        notificationKeys.add(EMAIL_ON_MESSAGE_RECIEVE);
        notificationKeys.add(EMAIL_ON_WORKFLOW_COMPLETION);
        notificationKeys.add(MESSAGE_ON_WORKFLOW_COMPLETION);
        notificationKeys.add(EMAIL_ON_BLOG_POST_COMMENT);
        notificationKeys.add(MESSAGE_ON_BLOG_POST_COMMENT);
        notificationKeys.add(EMAIL_ON_BLOG_POST_COMMENT_COMMENT);
        notificationKeys.add(MESSAGE_ON_BLOG_POST_COMMENT_COMMENT);

        return notificationKeys;
    }

    public void sendNotification(Notification notification) throws ConnexienceException
    {
        Connection connection = null;
        Session session = null;
        try
        {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword())
                    : connectionFactory.createConnection();
            session = connection.createSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("notificationsQueue");
            MessageProducer sender = session.createProducer(queue);
            sender.send(session.createObjectMessage(notification));
        }
        catch (Exception e)
        {
            throw new ConnexienceException("Could not add notification to Queue", e);
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (JMSException e)
                {
                    throw new ConnexienceException("Could not add notification to Queue: Error closing Session", e);
                }
            }
        }
    }

    public boolean getNotificationValue(String notificationKey, String userId) throws ConnexienceException
    {
        try
        {
            Ticket ticket = getInternalTicket();
            PropertyItem notifcationProperty = EJBLocator.lookupPropertiesBean().getProperty(ticket, userId, "notifications", notificationKey);
            return Boolean.parseBoolean(notifcationProperty.getValue());
        }
        catch (NullPointerException e)
        {
            return false;
        }
    }

    public void setNotificationValue(String notificationKey, String userId, String value) throws ConnexienceException
    {
        Ticket ticket = getInternalTicket();
        EJBLocator.lookupPropertiesBean().setProperty(ticket, userId, "notifications", notificationKey, value);
    }
}

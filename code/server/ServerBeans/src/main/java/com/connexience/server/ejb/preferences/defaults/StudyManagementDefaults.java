/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.preferences.defaults;

import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

/**
 * Sets default system properties for the study management features.
 * @author hugo
 */
public class StudyManagementDefaults {
    public static void createDefaultProperties(){
        PreferenceManager.getSystemPropertyGroup("StudyManagement").add("AutoEnableUnpackingWorkflow", false, "Set a default unpacking workflow in scanners when created as part of a project", "");
        PreferenceManager.getSystemPropertyGroup("StudyManagement").add("DefaultUnpackingWorkflowID", "workflows-study-defaultunpack", "Database ID of the default unpacking workflow for studies", "");
        PreferenceManager.getSystemPropertyGroup("StudyManagement").add("AlwaysCreateScanner", false, "Should a scanner always be created for new studies");
        PreferenceManager.getSystemPropertyGroup("StudyManagement").add("ShowCreateScannerOption", true, "Should the create scanner option be displayed");
        PreferenceManager.getSystemPropertyGroup("StudyManagement").add("PreSelectCreateScannerOption", false, "Should the create scanner option be enabled by default");
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.MetadataQuery;
import com.connexience.server.model.metadata.MetadataQueryBuilder;
import com.connexience.server.model.metadata.MetadataSynonym;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * This bean provides metadata management functionality
 */
@Stateless
@EJB(name = "java:global/ejb/MetaDataBean", beanInterface = MetaDataRemote.class)
public class MetaDataBean extends HibernateSessionContainer implements MetaDataRemote {

    @Override
    public MetadataItem addMetadata(Ticket ticket, String objectId, MetadataItem metadata) throws ConnexienceException {
        try {
            ServerObject obj = getObject(objectId, ServerObject.class);
            assertPermission(ticket, obj, Permission.WRITE_PERMISSION);

            // Process metadata to see if there are any synonyms 
            MetadataItem processedItem = processSynonyms(metadata);

            // Remove exisitng metadata
            removeDuplicateMetadata(objectId, processedItem.getCategory(), processedItem.getName());

            // Add new metadata
            processedItem.setObjectId(objectId);
            processedItem.setUserId(ticket.getUserId());
            processedItem = (MetadataItem) savePlainObject(processedItem);
            return processedItem;
        } catch (Exception e) {
            throw new ConnexienceException("Error adding metadata to object: " + e.getMessage(), e);
        }
    }

    @Override
    public void removeMetadata(Ticket ticket, String objectId, long metadataId) throws ConnexienceException {
        Session session = null;
        try {
            ServerObject obj = getObject(objectId, ServerObject.class);
            assertPermission(ticket, obj, Permission.WRITE_PERMISSION);
            session = getSession();
            Query q = session.createQuery("from MetadataItem as obj where obj.id=:id");
            q.setLong("id", metadataId);
            MetadataItem md = (MetadataItem) q.uniqueResult();
            session.delete(md);
        } catch (Exception e) {
            throw new ConnexienceException("Error removing metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List getObjectMetadata(Ticket ticket, String objectId) throws ConnexienceException {
        Session session = null;
        try {
            ServerObject obj = getObject(objectId, ServerObject.class);
            assertPermission(ticket, obj, Permission.READ_PERMISSION);
            session = getSession();
            Query q = session.createQuery("from MetadataItem as obj where obj.objectId=:objectid");
            q.setString("objectid", objectId);
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting object metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List search(Ticket ticket, MetadataQuery query) throws ConnexienceException {
        if (query.getSize() == 0) {
            return new ArrayList();
        }

        Session session = null;
        try {
            session = getSession();
            MetadataQueryBuilder builder = new MetadataQueryBuilder(query);
            SQLQuery q = builder.createSQLQuery(session);
            List results = q.list();
            return results;
        } catch (Exception e) {
            throw new ConnexienceException("Error searching metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List search(Ticket ticket, MetadataQuery query, int startPosition, int pageSize) throws ConnexienceException {
        if (query.getSize() == 0) {
            throw new ConnexienceException("Query does not contain any search terms");
        }

        Session session = null;
        try {
            session = getSession();
            MetadataQueryBuilder builder = new MetadataQueryBuilder(query);
            SQLQuery q = builder.createSQLQuery(session);
            q.setFirstResult(startPosition);
            q.setMaxResults(pageSize);
            List results = q.list();
            return results;
        } catch (Exception e) {
            throw new ConnexienceException("Error searching metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Process matching synonyms if there are any for a piece of metadata
     */
    private MetadataItem processSynonyms(MetadataItem md) throws ConnexienceException {
        List synonyms = getSynonyms(md.getCategory(), md.getName());
        MetadataSynonym synonym;
        MetadataItem updatedItem = md;
        for (Object o : synonyms) {
            synonym = (MetadataSynonym) o;
            if (synonym.isMetadataSupported(md)) {
                updatedItem = synonym.processMetadataItem(updatedItem);
            }
        }
        return updatedItem;
    }

    /**
     * Get a list of matching synonyms
     */
    private List getSynonyms(String category, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from MetadataSynonym as obj where obj.name=:name and obj.category=:category");
            q.setString("name", name);
            q.setString("category", category);
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing synonyms: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Delete all metadata items with a matching name and category
     */
    private void removeDuplicateMetadata(String objectId, String categoryName, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("delete from metadata where objectid=:objectid and category=:category and name=:name");
            q.setString("objectid", objectId);
            q.setString("category", categoryName);
            q.setString("name", name);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error removing duplicate metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Remove all duplicate synonyms
     */
    private void removeDuplicateSynonyms(String category, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("delete from metadatasynonyms where name=:name and category=:category");
            q.setString("name", name);
            q.setString("category", category);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error removing duplicate synonyms: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public int getResultCount(Ticket ticket, MetadataQuery query) throws ConnexienceException {
        if (query.getSize() == 0) {
            throw new ConnexienceException("Query does not contain any search terms");
        }

        Session session = null;
        try {
            session = getSession();
            MetadataQueryBuilder builder = new MetadataQueryBuilder(query);
            SQLQuery q = builder.createSQLCountQuery(session);
            Number result = (Number) q.uniqueResult();

            return result.intValue();
        } catch (Exception e) {
            throw new ConnexienceException("Error searching metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listSynonyms(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from MetadataSynonym");
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing synonyms: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public MetadataSynonym addSynonym(Ticket ticket, MetadataSynonym synonym) throws ConnexienceException {
        assertAdministrator(ticket);
        removeDuplicateSynonyms(synonym.getCategory(), synonym.getName());
        try {
            synonym = (MetadataSynonym) savePlainObject(synonym);
            return synonym;
        } catch (Exception e) {
            throw new ConnexienceException("Error adding synonym: " + e.getMessage(), e);
        }
    }

    @Override
    public void removeSynonym(Ticket ticket, long id) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("delete from metadatasynonms where id=:id");
            q.setLong("id", id);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error removing synonym: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public MetadataItem getObjectMetadata(Ticket ticket, String objectId, String category, String name) throws ConnexienceException {
        Session session = null;
        try {
            ServerObject obj = getObject(objectId, ServerObject.class);
            assertPermission(ticket, obj, Permission.READ_PERMISSION);
            session = getSession();
            Query q = session.createQuery("from MetadataItem as obj where obj.objectId=:objectid AND obj.category = :category AND obj.name = :name");
            q.setString("objectid", objectId);
            q.setString("category", category);
            q.setString("name", name);
            List results = q.list();
            if (results.size() == 0) {
                return null;
            } else {
                return (MetadataItem) results.get(0);
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting object metadata: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
}
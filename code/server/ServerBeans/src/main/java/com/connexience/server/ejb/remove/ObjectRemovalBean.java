/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.remove;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.datasets.DatasetsUtils;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.FolderRemover;
import com.connexience.server.ejb.util.OrganisationFolderAccess;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.dashboard.Dashboard;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.properties.PropertyGroup;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.StoredCredentials;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.*;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.storage.migration.DataStoreMigration;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowFolderTrigger;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.List;


/**
 * This bean provides a mechanism for removing objects from the database. It provides
 * rules for various classes of object which define the actions that mush be taken
 * when specific classes of object are deleted. For example, removing Group objects
 * means that any memberships and permissions for that group must also be deleted
 * from the database.
 * <p/>
 * This class takes no account of security settings, so it is important that other
 * beans check that a user is allowed to perform an action before this bean is invoked
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/ObjectRemovalBean", beanInterface = ObjectRemovalRemote.class)
public class ObjectRemovalBean extends HibernateSessionContainer implements com.connexience.server.ejb.remove.ObjectRemovalRemote {

    /**
     * Creates a new instance of ObjectRemovalBean
     */
    public ObjectRemovalBean() {
    }


    /*
     * Utility method to remove a server object.
     *
     * Throws an exception if passed an object that there is not a method to remove in this bean
     * */
    public void remove(Ticket ticket, ServerObject so) throws ConnexienceException {
        try {
            if (ticket==null || EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.OWNER_PERMISSION)) {

                if (so instanceof Comment) {
                    remove((Comment) so);

                } else if (so instanceof Event) {
                    remove((Event) so);

                } else if (so instanceof DocumentType) {
                    remove((DocumentType) so);
                    
                } else if (so instanceof Group) {
                    remove((Group) so);

                } else if (so instanceof Folder) {
                    remove(ticket, (Folder) so);
                    
                } else if(so instanceof User){
                    remove((User)so);
                    
                } else if (so instanceof DynamicWorkflowService) {
                    remove((DynamicWorkflowService) so);
                    
                } else if (so instanceof Message) {
                    remove((Message) so);
                    
                } else if (so instanceof DocumentRecord) {
                    remove((DocumentRecord) so);
                    
                } else if (so instanceof Dataset){
                    remove((Dataset)so);
                    
                } else {
                    throw new ConnexienceException("Unknown ServerObject type.  Id=" + so.getId());
                }
            } else {
                throw new ConnexienceException("Not authorised to delete");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error removing object: " + so.getName());
        }
    }

    /**
     * Remove a property group
     */
    public void remove(PropertyGroup properties) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            // Individual properties
            deletePropertyItems(session, properties.getId());
            session.delete(properties);

        } catch (Exception e) {
            throw new ConnexienceException("Error removing property group: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }


    /**
     * Remove a Comment
     */
    public void remove(Comment comment) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            // Remove anything associated with this comment
            deleteLinks(session, comment.getId());
            deleteObjectPermissions(session, comment.getId());
            deleteObjectMetadata(session, comment.getId());
            deleteObjectKey(session, comment.getId());
            deleteObjectProperties(session, comment.getId());
            deleteTags(session, comment.getId());

            session.delete(comment);
        } catch (ConnexienceException ce) {
            throw ce;

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error deleting comment: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Remove a Event
     */
    public void remove(Event event) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            // Remove anything associated with this note
            deleteLinks(session, event.getId());
            deleteObjectPermissions(session, event.getId());
            deleteObjectMetadata(session, event.getId());
            deleteObjectKey(session, event.getId());
            deleteObjectProperties(session, event.getId());
            deleteTags(session, event.getId());

            // Remove the event data
            deleteFolderContents(event, true, session);
            
            session.delete(event);
        } catch (ConnexienceException ce) {
            throw ce;

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error deleting note: " + e);
        } finally {
            closeSession(session);
        }
    }
    
    /**
     * Remove a DocumentType
     */
    public void remove(DocumentType docType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            // Remove the type from any documents
            SQLQuery q = session.createSQLQuery("update objectsflat set documenttypeid=null where documenttypeid=?");
            q.setString(0, docType.getId());
            q.executeUpdate();

            // Remove other data
            deleteObjectPermissions(session, docType.getId());
            deleteObjectMetadata(session, docType.getId());
            deleteObjectKey(session, docType.getId());
            deleteObjectProperties(session, docType.getId());

            // Remove the actual object
            session.delete(docType);

        } catch (Exception e) {
            throw new ConnexienceException("Cannot delete DocumentType: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }


    /**
     * Remove a Folder
     */
    public void remove(Ticket ticket, Folder folder) throws ConnexienceException {
        Session session = null;
        try {
            DataStore store = getCachedDataStore(folder.getOrganisationId());

            //Cannot remove workflows folder
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            if(user.getWorkflowFolderId()!=null && user.getWorkflowFolderId().equals(folder.getId()))
            {
              throw new ConnexienceException("Cannot remove Workflows Folder");
            }
          
            if(store.isBulkDeleteSupported()){
                FolderRemover remover = new FolderRemover(folder, store);
                remover.remove();
            } else {
                session = getSession();
                remove(folder, session);
            }
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error deleting folder: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

  /**
   * Remove a Workflows Folder
   */
  public void removeWorkflowFolder(Ticket ticket, User user) throws ConnexienceException
  {
    Session session = null;
    try
    {

      //Cannot remove workflows folder
       Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, user.getWorkflowFolderId());

      DataStore store = getCachedDataStore(folder.getOrganisationId());

      if (store.isBulkDeleteSupported())
      {
        FolderRemover remover = new FolderRemover(folder, store);
        remover.remove();
      }
      else
      {
        session = getSession();
        remove(folder, session);
      }
    }
    catch (ConnexienceException ce)
    {
      throw ce;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error deleting folder: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

    /**
     * Remove a Folder
     */
    public void remove(Folder folder, Session session) throws ConnexienceException {
        // Make sure that this isn't a special folder
        if (!OrganisationFolderAccess.isSpecialFolder(folder)) {
            // Remove contents
            deleteFolderContents(folder, true, session);

            // Remove the folder
            try {
                deleteLinks(session, folder.getId());
                deleteObjectPermissions(session, folder.getId());
                deleteObjectMetadata(session, folder.getId());
                deleteObjectProperties(session, folder.getId());
                deleteFolderWorkflowTriggers(session, folder.getId());

                // Remove all of the invocation service logs if this is an invocation folder
                if (folder instanceof WorkflowInvocationFolder) {
                    WorkflowInvocationFolder invocation = (WorkflowInvocationFolder) folder;
                    deleteInvocationLogs(session, invocation.getInvocationId());
                    deleteStoredInvocationMessage(session, invocation.getInvocationId());
                }

                session.delete(folder);
            } catch (Exception e) {
                throw new ConnexienceException("Cannot delete folder: " + e.getMessage());
            }
        } else {
            throw new ConnexienceException("Cannot remove a Special Folder");
        }
    }

    /**
     * Remove a group and all of the associated memberships and permissions
     */
    public void remove(Group group) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //if the group is a project then remove the default items in the User objects
            SQLQuery removeProjectDefaultIdsQuery = session.createSQLQuery("update objectsflat set defaultprojectid = null, defaultstoragefolderid = null where defaultprojectid = :projectId");
            removeProjectDefaultIdsQuery.setString("projectId", group.getId());
            removeProjectDefaultIdsQuery.executeUpdate();

            // Delete the memberships
            SQLQuery q = session.createSQLQuery("DELETE from groupmembership WHERE groupid=?");
            q.setString(0, group.getId());
            q.executeUpdate();

            // Delete the contents of the folder
            Folder dataFolder = (Folder) getObject(group.getDataFolder(), Folder.class);
            Folder events = (Folder) getObject(group.getEventsFolder(), Folder.class);

            remove(dataFolder, session);
            remove(events, session);

            // Delete all of the group events
            Query gq = session.createQuery("from GroupEvent as obj where obj.groupId=?");
            gq.setString(0, group.getId());
            List results = gq.list();
            for (Object result : results)
            {
              remove((Event) result);
            }

            // Delete permissions
            deletePrincipalPermissions(session, group.getId());

            // Delete any keys
            deleteObjectKey(session, group.getId());

            // Remove any metadata pointing to the group
            deleteObjectMetadata(session, group.getId());

            // Remove any properties associated with the group
            deleteObjectProperties(session, group.getId());
            
            // Delete the group
            session.delete(group);

        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Remove a dynamic workflow service
     */
    public void remove(DynamicWorkflowService service) throws ConnexienceException {

        Session session = null;
        try {

            Ticket t = getInternalTicket();

            // Find the data store
            DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(t, service.getOrganisationId());

            // Remove all of the versions
            List versions = EJBLocator.lookupStorageBean().listVersions(t, service.getId());
            DocumentVersion version;
            session = getSession();
            for (Object version1 : versions)
            {
              version = (DocumentVersion) version1;
              session.delete(version);
              store.removeRecord(service, version);
            }


            // Remove all of the permissions and metadata
            deleteLinks(session, service.getId());
            deleteObjectPermissions(session, service.getId());
            deleteObjectMetadata(session, service.getId());
            deleteObjectProperties(session, service.getId());
            deleteTags(session, service.getId());

            SQLQuery q = session.createSQLQuery("DELETE from servicexml WHERE serviceid=?");
            q.setString(0, service.getId());
            q.executeUpdate();

            // Delete the service.xml records


            // Remote the document record
            session.delete(service);

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Remove a dashboard */
    @Override
    public void remove(Dataset dataset) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
                  
            deleteLinks(session, dataset.getId());
            deleteObjectPermissions(session, dataset.getId());
            deleteObjectMetadata(session, dataset.getId());
            deleteObjectProperties(session, dataset.getId());
            deleteTags(session, dataset.getId());
            
            // Remove any dashboards associated with this dataset
            Query q = session.createQuery("from Dashboard as db where db.datasetId=:datasetid");
            q.setString("datasetid", dataset.getId());
            List results = q.list();
            for(Object obj : results){
                remove((Dashboard)obj);
            }
            
            // Remove any multiple data rows
            DatasetsUtils.removeAllMultipleValueData(session, dataset);
            
            // Remove all the actial data items
            SQLQuery itemQuery = session.createSQLQuery("delete from datasetitems where datasetid=:datasetid");
            itemQuery.setString("datasetid", dataset.getId());
            itemQuery.executeUpdate();
            
            // Remove the object
            session.delete(dataset);
            
        } catch (Exception e){
            throw new ConnexienceException("Error removing dashboard: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void remove(Dashboard dashboard) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
                  
            deleteLinks(session, dashboard.getId());
            deleteObjectPermissions(session, dashboard.getId());
            deleteObjectMetadata(session, dashboard.getId());
            deleteObjectProperties(session, dashboard.getId());
            deleteTags(session, dashboard.getId());

            // Remove the object
            session.delete(dashboard);
            
        } catch (Exception e){
            throw new ConnexienceException("Error removing dataset: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    

    /**
     * Remove an entire DocumentRecord
     */
    public void remove(DocumentRecord doc) throws ConnexienceException {
        Session session = null;
        try {
            Ticket t = getInternalTicket();
            session = getSession();

            // Find the data store - only do this if this isn't a link to a document

            DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(t, doc.getOrganisationId());

            // Remove all of the versions
            List versions = EJBLocator.lookupStorageBean().listVersions(t, doc.getId());
            DocumentVersion version;

            for (Object version1 : versions) {
                version = (DocumentVersion) version1;
                session.delete(version);
                store.removeRecord(doc, version);
            }

            // Remove all of the permissions and metadata
            deleteLinks(session, doc.getId());
            deleteObjectPermissions(session, doc.getId());
            deleteObjectMetadata(session, doc.getId());
            deleteObjectProperties(session, doc.getId());
            deleteTags(session, doc.getId());
            
            if(doc instanceof WorkflowDocument){
                deleteTriggersForWorkflow(session, doc.getId());
            }
            

            // Remote the document record
            session.delete(doc);

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Remove a document version with a session
     */
    public void remove(DocumentVersion version, Session session) throws ConnexienceException {
        try {
            Ticket t = getInternalTicket();

            // Get the data store for the organisation containing the DocumentVersion
            DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(t, version.getDocumentRecordId());
            Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(t, doc.getOrganisationId());
            DataStore store = EJBLocator.lookupStorageBean().getDataStore(t, org.getDataStoreId());
            store.removeRecord(doc, version);
            session.delete(version);
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }

    /**
     * Remove a document version
     */
    public void remove(DocumentVersion version) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            remove(version, session);
        } finally {
            this.closeSession(session);
        }
    }

    /**
     * Remove all of the tags for an item
     */
    public void removeTags(Session session, String serverObjectId) {
        //get the mapping from object to com.connexience.server.social.tag
        Query q = session.createQuery("FROM TagToObject AS tto WHERE tto.serverObjectId = :soid");
        q.setString("soid", serverObjectId);

        //for each com.connexience.server.social.tag, if there is nothing still tagged with that text, remove the com.connexience.server.social.tag
        Collection tagMappings = q.list();
        for (Object o : tagMappings) {
            TagToObject tto = (TagToObject) o;
            deleteTag(session, tto);
        }
    }

    /*
     * Remove a single com.connexience.server.social.tag from a server object
     * */

    public void remove(String serverObjectId, Tag tag) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //get the com.connexience.server.social.tag mapping between this com.connexience.server.social.tag and the object
            Query q = session.createQuery("FROM TagToObject AS tto WHERE tto.tagId = :tagId AND tto.serverObjectId = :soid");
            q.setString("tagId", tag.getId());
            q.setString("soid", serverObjectId);
            TagToObject tto = (TagToObject) q.uniqueResult();

            deleteTag(session, tto);
        } catch (Exception ce) {
            throw new ConnexienceException("Error removing com.connexience.server.social.tag: ", ce);
        } finally {
            this.closeSession(session);
        }
    }


    @Override
    public void remove(WorkflowFolderTrigger trigger) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            session.delete(trigger);
        } catch (Exception e){
            throw new ConnexienceException("Error removing folder trigger: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    private void deleteFolderWorkflowTriggers(Session session, String folderId){
        SQLQuery q= session.createSQLQuery("DELETE from foldertriggers WHERE folderid=?");
        q.setString(0, folderId);
        q.executeUpdate();
    }
    
    private void deleteTriggersForWorkflow(Session session, String workflowId){
        SQLQuery q= session.createSQLQuery("DELETE from foldertriggers WHERE workflowid=?");
        q.setString(0, workflowId);
        q.executeUpdate();
        
    }
    
    private void deleteLinks(Session session, String serverObjectId)
    {
        Query q = session.createQuery("FROM Link as l where l.sourceObjectId = ? or l.sinkObjectId = ?");
        q.setString(0, serverObjectId);
        q.setString(1, serverObjectId);
        Collection links = q.list();
        for (Object o : links) {
            Link l = (Link) o;
            session.delete(l);
        }
    }

    /**
     * Delete all the contents of a Folder
     */
    private void deleteFolderContents(Folder folder, boolean recurse, Session session) throws ConnexienceException {
        List documents = null;
        // List documents
        try {
            Query q = session.createQuery("from DocumentRecord as obj where obj.containerId=?");
            q.setString(0, folder.getId());
            documents = q.list();
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }

        // Remove documents
        if (documents != null) {
          for (Object document : documents)
          {
            remove((DocumentRecord) document);
          }
        }

        // Should we recurse
        if (recurse) {
            List childFolders = null;
            Folder childFolder;
            try {
                Query q = session.createQuery("from Folder as obj where obj.containerId = ?");
                q.setString(0, folder.getId());
                childFolders = q.list();
            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage());
            }

            // Remove documents from child folders
            if (childFolders != null) {
              for (Object childFolder1 : childFolders)
              {
                childFolder = (Folder) childFolder1;
                deleteFolderContents((Folder) childFolder1, true, session);
                session.delete(childFolder);
                
              }
            }
        }
    }

    /**
     * Delete all permissions for a Principal (User / Group)
     */
    private void deletePrincipalPermissions(Session session, String principalId)
    {
        SQLQuery q = session.createSQLQuery("DELETE from permissions WHERE principalid=?");
        q.setString(0, principalId);
        q.executeUpdate();
    }

    /**
     * Delete all invocation logs
     */
    private void deleteInvocationLogs(Session session, String invocationId)
    {
        SQLQuery q = session.createSQLQuery("DELETE from workflowservicelogs WHERE invocationid=?");
        q.setString(0, invocationId);
        q.executeUpdate();
    }
    
    /** 
     * Delete a stored invocation message
     */
    private void deleteStoredInvocationMessage(Session session, String invocationId){
        SQLQuery q = session.createSQLQuery("DELETE from invocationmessages WHERE invocationid=?");
        q.setString(0, invocationId);
        q.executeUpdate();
    }
    
    /**
     * Delete permissions for an object
     */
    private void deleteObjectPermissions(Session session, String objectId)
    {
        SQLQuery q = session.createSQLQuery("DELETE from permissions WHERE targetobjectid=?");
        q.setString(0, objectId);
        q.executeUpdate();
    }

    /**
     * Delete metadata for an object
     */
    private void deleteObjectMetadata(Session session, String objectId)
    {
        SQLQuery q = session.createSQLQuery("DELETE from metadata WHERE objectid=?");
        q.setString(0, objectId);
        q.executeUpdate();
    }

    /**
     * Remove the certificate and key data for an object
     */
    private void deleteObjectKey(Session session, String objectId)
    {
        SQLQuery q = session.createSQLQuery("DELETE from keys WHERE objectid=?");
        q.setString(0, objectId);
        q.executeUpdate();
    }

    /**
     * Remove all of the properties for an object
     */
    private void deleteObjectProperties(Session session, String objectId) throws Exception {
        Query q = session.createQuery("from PropertyGroup as g where g.objectId=?");
        q.setString(0, objectId);
        List groups = q.list();
        PropertyGroup group;
        for (Object group1 : groups)
        {
          group = (PropertyGroup) group1;
          deletePropertyItems(session, group.getId());
          session.delete(group);
        }
    }

    /**
     * Remove all of the properties items for a property group
     */
    private void deletePropertyItems(Session session, long propertyGroupId)
    {
        SQLQuery q = session.createSQLQuery("DELETE from propertyitems WHERE groupid=?");
        q.setLong(0, propertyGroupId);
        q.executeUpdate();
    }

    /**
     * Utility method with expected signature for removing tags
     */
    private void deleteTags(Session session, String serverObjectId) {
        removeTags(session, serverObjectId);
    }

    /*
     * Delete a logon
     */
    private void deleteLogon(Session session, String userId) {
        SQLQuery q = session.createSQLQuery("DELETE from logondetails WHERE userid=?");
        q.setString(0, userId);
        q.executeUpdate();

        q = session.createSQLQuery("DELETE from externallogondetails WHERE userid=?");
        q.setString(0, userId);
        q.executeUpdate();
    }
    
    /**
     * Delete images for an object
     */
    private void deleteImages(Session session, String objectId){
        SQLQuery q = session.createSQLQuery("DELETE from images WHERE serverobjectid=?");
        q.setString(0, objectId);
        q.executeUpdate();
    }
    
    /**
     * Delete quota
     */
    private void deleteQuota(Session session, String userId){
        SQLQuery q = session.createSQLQuery("DELETE from quotas WHERE userid=?");
        q.setString(0, userId);
        q.executeUpdate();
    }
    
    //delete a single mapping from object to com.connexience.server.social.tag.  Cascades and deletes the com.connexience.server.social.tag if there are no more mappings pointing to it
    private void deleteTag(Session session, TagToObject tto) {
        //count the number of mappings to that com.connexience.server.social.tag
        Query countTags = session.createQuery("SELECT count(*) FROM TagToObject as tto WHERE tto.tagId = :tagId");
        countTags.setString("tagId", tto.getTagId());
        Long count = (Long) countTags.uniqueResult();

        //if there is only one mapping, delete the com.connexience.server.social.tag
        if (count == 1) {
            Query getTagQuery = session.createQuery("FROM Tag as t WHERE t.id = :tagId");
            getTagQuery.setString("tagId", tto.getTagId());
            Tag t = (Tag) getTagQuery.uniqueResult();
            session.delete(t);
        }

        //delete the mapping from object to com.connexience.server.social.tag
        session.delete(tto);
    }

    public void remove(Message m) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            deleteObjectProperties(session, m.getId());
            deleteObjectPermissions(session, m.getId());

            session.delete(m);
        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void remove(DataStoreMigration migration) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            
            // Delete all of the individual migration documents
            SQLQuery q = session.createSQLQuery("DELETE from migrationversions WHERE migrationid=?");
            q.setLong(0, migration.getId());
            q.executeUpdate();   
            
            // Delete the actual migration
            session.delete(migration);
            
        } catch (Exception e){
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void remove(RemoteFilesystemScanner scanner) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            
            // Delete all of the stored state
            SQLQuery q = session.createSQLQuery("DELETE from remotefilesystemobjects WHERE scannerid=?");
            q.setLong(0, scanner.getId());
            q.executeUpdate();
            session.delete(scanner);
        } catch (Exception e){
            throw new ConnexienceException("Error removing scanner: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void remove(User user) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            
            String homeFolderId = user.getHomeFolderId();
            Query q = session.createQuery("from Folder as obj where obj.id=:id");
            q.setString("id", homeFolderId);
            List results = q.list();
            if(results.size()>0){
                Folder home = (Folder)results.get(0);

                // Delete the folder and documents
                remove(home, session);
            }
            
            deleteLinks(session, user.getId());
            deleteObjectKey(session, user.getId());
            deleteObjectMetadata(session, user.getId());
            deleteObjectPermissions(session, user.getId());
            deleteObjectProperties(session, user.getId());
            deletePrincipalPermissions(session, user.getId());
            deleteTags(session, user.getId());
            deleteLogon(session, user.getId());
            deleteImages(session, user.getId());
            deleteQuota(session, user.getId());
            
            // Remove any leftover folders
            Query remains = session.createQuery("from Folder as obj where obj.creatorId=:id");
            remains.setString("id", user.getId());
            List remainingObjects = remains.list();
            Folder folder;
            for(Object o : remainingObjects){
                folder = (Folder)o;
                remove(folder, session);
            }
                    
            session.delete(user);
        } catch (Exception e){
            throw new ConnexienceException("Error removing user: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.RootSecurityObjectManager;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.util.ZipUtils;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.util.*;
import javax.ejb.EJBLocalHome;
import org.hibernate.cfg.Configuration;

/**
 * Provides some utilitiy methods for handling hibernate connections. Taken from
 * hibernate documentation.
 *
 * @author hugo
 */
public class HibernateUtil {
    private static Logger logger = Logger.getLogger(HibernateUtil.class);
    /**
     * Session factories
     */
    private static SessionFactory factory;
    private static Configuration config;
    
    public static String DATABASE_PRODUCT = "postgres";
    public static boolean isPostgres = false;
    
    /** Set up the session factory */
    static {
        // Make sure the esc directory exists
        File escDir = new File(System.getProperty("jboss.home.dir") + File.separator + "esc");
        if(!escDir.exists()){
            logger.info("Creating esc directory: " + escDir.getPath());
            escDir.mkdirs();
        }
                
        Connection c = null;
        String configUrl = "/hibernate/postgres/hibernate.cfg.xml";

        DatabaseMetaData dbmd = null;
        try {
            c = getSQLConnection();
            dbmd = c.getMetaData();
            String productName = dbmd.getDatabaseProductName();
            logger.info("Database product name: " + productName);
            if (productName.contains("Microsoft") && productName.contains("SQL") && productName.contains("Server")) {
                configUrl = "/hibernate/sqlserver/hibernate.cfg.xml";
                DATABASE_PRODUCT = "SqlServer";
                isPostgres = false;
                logger.info("Using: " + configUrl + " for SQL Server");
                
            } else if(productName.contains("H2")){
                configUrl = "/hibernate/h2/hibernate.cfg.xml";
                logger.info("Using: " + configUrl + " for H2 Database");
                isPostgres = false;
                DATABASE_PRODUCT = "H2";
                
            } else if (productName.equals("MySQL")) {
                configUrl = "/hibernate/mysql/hibernate.cfg.xml";
                logger.info("Using: " + configUrl + " for MySQL");
                isPostgres = false;
                DATABASE_PRODUCT = "MySql";
                
            } else {
                logger.info("Defaulting to: " + configUrl + " for PostgreSQL");
                isPostgres = true;
                DATABASE_PRODUCT = "postgres";
            }
        } catch (Exception e) {

        } finally {
            try {
                c.close();
            } catch (Exception e) {
            }
        }

        try {
            // Use the correct configuration
            config = new org.hibernate.cfg.Configuration();
            config.configure(configUrl);

            // Check for extra dataset config
            try {
                config.addInputStream(ConfigReplacer.datasetReplacer.getModifiedConfigStream());
            } catch (Exception e){
                logger.error("Error adding DatasetItem config: " + e.getMessage());
            }
            
            factory = config.buildSessionFactory();
            
        } catch (Throwable e) {
            e.printStackTrace();
            logger.error("Error creating factory: " + e.getMessage());
        }

        // Do any migrations
        performCoreMigrations();

        try {
            // Set up the root security object
            logger.info("Checking root security object");
            RootSecurityObjectManager mgr = new RootSecurityObjectManager();
            mgr.initialiseSecurityObject();

        } catch (Throwable e) {
            logger.error("Error initialising root security object: " + e.getMessage());
        }
        
        // Kick off the scheduler
        SchedulerBean.SCHEDULER_INSTANCE.startScheduler();
    }
    
    /**
     * Perform core migrations
     */
    public static void performCoreMigrations() {
        HibernateUtil.Migrator m1 = new HibernateUtil.Migrator("/com/connexience/server/ejb/sql/core");
        SequenceChecker sequenceChecker = new SequenceChecker();
        MACAddressSetter macSetter = new MACAddressSetter();
        
    }

    /**
     * Get the created session factory
     */
    public static SessionFactory getSessionFactory() {
        return factory;
    }

    public static Connection getSQLConnection() throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup("java:jboss/datasources/ConnexienceDB");
        return source.getConnection();
    }

    public String generateId() throws Exception {
        Connection connection = getSQLConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT nextval ('esc_sequence') as nextval");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("nextval");
                return String.valueOf(id);
            } else {
                throw new HibernateException("Unable to generate Sequence - no result returned.  Does esc_sequence exist in the db?");
            }
        } catch (SQLException e) {
            throw new HibernateException("Unable to generate Sequence");
        }
    }

    public static class MACAddressSetter {
        private static Logger macLogger = Logger.getLogger(MACAddressSetter.class);
        
        public MACAddressSetter(){
            try {
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(HibernateSessionContainer.INTERNAL_TICKET);
                if(org!=null){
                    if(org.getAccessKey()==null || org.getAccessKey().isEmpty()){
                        // Need to assign a new key
                        macLogger.info("Assigning Organisation AccessKey");
                        EJBLocator.lookupOrganisationDirectoryBean().resetAccessKeyToLocalMacAddress();
                    } else {
                        // Check to see if the MAC address matches the AccessKey
                        String localMac = EJBLocator.lookupPreferencesBean().getMacAddress();
                        String accessKey = org.getAccessKey();
                        if(accessKey.equals(localMac)){
                            macLogger.info("Unlocking storage");
                            EJBLocator.lookupPreferencesBean().setStorageEnabled(true);
                        } else {
                            macLogger.warn("Storage is Locked because Organisation AccessKey does not match the local MAC address");
                            EJBLocator.lookupPreferencesBean().setStorageEnabled(false);
                        }
                    }
                } else {
                    logger.info("Cannot check Organisation AccessKey: No organisation exists yet");
                }
            } catch (Exception e){
                
            }
        }
    }
    
    public static class Migrator {
        public Migrator(String resource) {
            Properties migrations = new Properties();

            // try and load the list of SQL scripts
            try {
                migrations.load(getClass().getResourceAsStream(resource + "/migrations.properties"));

            } catch (Exception e) {
                logger.error("Error loading migration properties file: " + e.getMessage());
            }

            List propertyNames = Collections.list(migrations.propertyNames());
            Collections.sort(propertyNames);

            String sqlFile;
            for (Object scriptName : propertyNames) {

                sqlFile = resource + "/" + migrations.getProperty((String) scriptName);
                Connection c = null;
                Statement s = null;

                try {
                    c = getSQLConnection();
                    s = c.createStatement();
                    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                    ZipUtils.copyInputStream(getClass().getResourceAsStream(sqlFile), outStream);
                    outStream.flush();
                    outStream.close();
                    String sql = new String(outStream.toByteArray());
                    logger.info("Performing update: " + sql);
                    s.executeUpdate(sql);

                } catch (Exception e) {
                    logger.error("Error performing migration: " + sqlFile + ": " + e.getMessage());
                } finally {
                    try {
                        s.close();
                    } catch (Exception ex) {
                    }
                    try {
                        c.close();
                    } catch (Exception ex) {
                    }
                }
            }
        }
    }

    /**
     * Create the esc_sequence if it doesn't exist
     */
    public static class SequenceChecker {
        public SequenceChecker() {
            if(isPostgres){
                Connection c = null;
                Statement s = null;

                try {
                    c = getSQLConnection();
                    s = c.createStatement();
                    ResultSet rs = s.executeQuery("SELECT count(*) FROM information_schema.sequences WHERE sequence_name = 'esc_sequence'");
                    rs.next();
                    Long numSeqs = rs.getLong(1);
                    if (numSeqs == 0) {
                        logger.info("Creating esc_sequence as it doesn't exist");
                        int result = s.executeUpdate("CREATE SEQUENCE esc_sequence INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1");
                        if (result != 0) {
                            logger.error("Unable to create esc_sequence");
                        }
                        result = s.executeUpdate("ALTER TABLE esc_sequence OWNER TO connexience");
                        if (result != 0) {
                            logger.error("Unable to change owner of esc_sequence");
                        }
                    } else {
                        logger.info("Not creating esc_sequence as it already exists");
                    }
                } catch (Exception e) {
                    logger.error("Error Checking or creating esc_sequence", e);
                } finally {
                    try {
                        s.close();
                    } catch (Exception ignored) {
                    }
                    try {
                        c.close();
                    } catch (Exception ignored) {
                    }
                }
            } else {
                Connection c = null;
                Statement s = null;

                try {
                    c = getSQLConnection();
                    s = c.createStatement();
 
                    int result = s.executeUpdate("CREATE SEQUENCE IF NOT EXISTS esc_sequence INCREMENT BY 1 START WITH 1 CACHE 1");
                    if (result != 0) {
                        logger.error("Unable to create esc_sequence");
                    }

                } catch (Exception e) {
                    logger.error("Error Checking or creating esc_sequence", e);
                } finally {
                    try {
                        s.close();
                    } catch (Exception ignored) {
                    }
                    try {
                        c.close();
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }
}
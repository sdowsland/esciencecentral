/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.smtp;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.profile.UserProfile;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: martyn
 * Date: 18-Nov-2009
 * Time: 15:02:18
 */

@Stateless
@EJB(name = "java:global/ejb/SMTPBean", beanInterface = SMTPRemote.class)
public class SMTPBean extends HibernateSessionContainer implements SMTPRemote {
    // TODO Convert back to injected JavaMail session when supported by AS7
    // @Resource(mappedName="java:/Mail")
    Session mailSession;

    public static final String DEFAULT_CONTEXT_ROOT = "http://www.esciencecentral.co.uk/beta/";

    public SMTPBean() {
        Ticket ticket = getInternalTicket();
        try {
            final Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);
            if (org != null) {
                if (org.getGmailUser() != null && org.getGmailPassword() != null) {
                    Properties props = new Properties();
                    props.put("mail.transport.protocol", "smtp");

                    props.put("mail.user", org.getGmailUser());

                    props.put("mail.smtp.host", "smtp.googlemail.com");
                    props.put("mail.smtp.port", "465");
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.ssl.enable", "true");
                    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

                    props.put("mail.from", org.getGmailUser());
                    props.put("mail.debug", "false");

                    mailSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(org.getGmailUser(), org.getGmailPassword());
                        }
                    });

                }
            }
        } catch (ConnexienceException e) {
            e.printStackTrace();
        }
    }

    public void sendMail(List<String> userIds, String subject, String content, String contentType) throws ConnexienceException {
        Ticket ticket = getInternalTicket();

        InternetAddress[] recipients = new InternetAddress[userIds.size()];
        InternetAddress ia;
        for (int i = 0; i < userIds.size(); i++) {
            try {
                UserProfile profile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, userIds.get(i));
                ia = new InternetAddress(profile.getEmailAddress());
                recipients[i] = ia;
            } catch (AddressException ignored) {
                //todo: fix blank elements in array
                System.out.println("User Id: " + recipients[i] + " does not have a valid email address");
            }

        }
        sendMail(recipients, subject, content, contentType);
    }

    public void sendMail(InternetAddress[] recipients, String subject, String content, String contentType) throws ConnexienceException {
        try {
            if (mailSession != null) {
                Message msg = new MimeMessage(mailSession);
                msg.setRecipients(javax.mail.Message.RecipientType.TO, recipients);
                msg.setSubject(subject);
                msg.setContent(content, contentType);
                msg.saveChanges();

                Transport.send(msg);
            }
            else
            {
                throw new ConnexienceException("Organisation doesn't have GMail user/password so can't send mail");
            }
        } catch (Exception e) {
            throw new ConnexienceException("Could not Send Mail", e);
        }
    }


    public void sendTextMessageReceivedEmail(Ticket ticket, String userId, TextMessage textMessage) throws ConnexienceException {
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, textMessage.getSenderId());

        String subject = user.getDisplayName() + " sent you a Message.";

        String linkToMessage = getContextRoot(getInternalTicket(), userId) + "secure/viewmessage.jsp?threadId=" + textMessage.getThreadId();

        String content = subject + "<br/><br/>";

        content += "--------------------------------<br/><br/>";
        content += textMessage.getMessage();
        content += "--------------------------------<br/><br/>";
        content += "To view this message, follow the link below: <br/>";
        content += "<a href='" + linkToMessage + "'>" + linkToMessage + "</a><br/><br/>";
        content += generateEmailFooter();

        ArrayList<String> recipients = new ArrayList<>();
        recipients.add(userId);

        sendMail(recipients, subject, content, "text/html");
    }

    public void sendResetPasswordEmail(Ticket ticket, String userId, String code, String websiteURL) throws ConnexienceException {
        if (!websiteURL.endsWith("/")) {
            websiteURL += "/";
        }
        String resetLink = websiteURL + "pages/signup/validatePasswordCode.jsp?code=" + code;

        String subject = "Password reset";

        String content = subject + "<br/><br/> To reset your password click the link below or paste it into your browser address bar" + "<br/><br/>";
        content += "<a href='" + resetLink + "'>" + resetLink + "</a><br/><br/>";
        content += "<br/><br/>" + generateEmailFooter();

        ArrayList<String> recipients = new ArrayList<>();
        recipients.add(userId);

        sendMail(recipients, subject, content, "text/html");
    }

    private String generateEmailFooter() {
        return "This is an automatically generated email, please do not reply.";
    }

    private String getContextRoot(Ticket ticket, String userId) throws ConnexienceException {
        UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, userId);
        String contextRoot = up.getDefaultDomain();
        if (contextRoot == null) {
            return DEFAULT_CONTEXT_ROOT;
        }
        return contextRoot;
    }
}

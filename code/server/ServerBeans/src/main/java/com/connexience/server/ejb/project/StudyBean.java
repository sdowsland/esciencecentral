/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.acl.AccessControlRemote;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.remove.ObjectRemovalRemote;
import com.connexience.server.ejb.social.MessageRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.Uploader;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import org.hibernate.Hibernate;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * User: nsjw7 Date: 28/05/2013 Time: 10:17
 */
@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/StudyBean", beanInterface = StudyRemote.class)
public class StudyBean extends HibernateSessionContainer implements StudyRemote {
    @EJB
    private GroupDirectoryRemote groupBean;

    @EJB
    private UserDirectoryRemote userBean;

    @EJB
    private ObjectRemovalRemote objectRemovalBean;

    @EJB
    private StorageRemote storageBean;

    @EJB
    private AccessControlRemote aclBean;

    @EJB
    private ProjectsRemote projectBean;

    @EJB
    private SubjectsRemote subjectsBean;

    @EJB
    private LoggersRemote loggersBean;

    @EJB
    private MessageRemote messageBean;

    @Inject
    private EntityManager em;

    @Override
    public Study getStudy(Ticket ticket, Integer studyId) throws ConnexienceException {
        final Project project = projectBean.getProject(ticket, studyId);
        if (!(project instanceof Study)) {
            throw new ConnexienceException("id='" + studyId + "' is a Project but not a Study.");
        }

        return (Study) project;
    }

    @Override
    public Study saveStudy(final Ticket ticket, Study study) throws ConnexienceException {
        //Manually add all of the upload users and phases as these are not passed through the API
        //Uploaders are removed from a study by a different API call
        if (study.getId() != null) {
            study.getUploaders();
            //Get the exising study from the database
            Study existingStudy = em.merge(getStudy(ticket, study.getId()));

            //Initialise the lazily loaded uploaders and phases collections
            Hibernate.initialize(existingStudy.getUploaders());
            Hibernate.initialize(existingStudy.getPhases());

            //copy the collection as otherwise it will be deleted by em.merge(study)
            Collection<Uploader> existingUploaders = new ArrayList(existingStudy.getUploaders());
            Collection<Phase> existingPhases = new ArrayList<>(existingStudy.getPhases());

            //This is necessary to get the study into the session and force the uploaders to be loaded
            // even though it will be an empty set
            study = em.merge(study);
            Hibernate.initialize(study.getUploaders());
            Hibernate.initialize(study.getPhases());

            //Re-add all of the uploaders and phases that we cached earlier
            for (Uploader uploader : existingUploaders) {
                study.addUploader(uploader);
            }

            for (Phase phase : existingPhases) {
                study.addPhase(phase);
            }
        }


        study = (Study) projectBean.saveProject(ticket, study);
        study = em.merge(study);

        //initialise the phases
        //todo: this will probably be removed when sent correctly via JSON
        if (study.getPhases() == null || study.getPhases().size() == 0) {

            createPhase(ticket, study, "Default");
            study = em.merge(study);
            em.persist(study);
        }
        return study;
    }

    @Override
    public void deleteStudy(final Ticket ticket, final Integer studyId) throws ConnexienceException {
        deleteStudy(ticket, studyId, false);
    }

    @Override
    public void deleteStudy(final Ticket ticket, final Integer studyId, final Boolean deleteFiles) throws ConnexienceException {
        projectBean.deleteProject(ticket, studyId, deleteFiles);
    }

    @Override
    public Long getPublicStudyCount(final Ticket ticket) throws ConnexienceException {
        return (Long) em.createNamedQuery("Study.public.count").getResultList().get(0);
    }

    @Override
    public List<Study> getPublicStudies(final Ticket ticket, final Integer start, final Integer maxResults, final String orderBy, final String direction) throws ConnexienceException {
        //return em.createNamedQuery("Study.public").setFirstResult(start).setMaxResults(maxResults).getResultList();

        if (orderBy.toUpperCase().equals("NAME")) {

            if(direction.toUpperCase().equals("ASC")){
                return em.createNamedQuery("Study.public.name.asc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else if(direction.toUpperCase().equals("DESC")){
                return em.createNamedQuery("Study.public.name.desc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else{
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        }
        else if (orderBy.toUpperCase().equals("DATE")) {

            if(direction.toUpperCase().equals("ASC")){
                return em.createNamedQuery("Study.public.date.asc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else if(direction.toUpperCase().equals("DESC")){
                return em.createNamedQuery("Study.public.date.desc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else{
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        }
        else{
            throw new ConnexienceException("Sorting property is required. Can be either NAME or DATE");
        }



    }

    @Override
    public Long getMemberStudyCount(Ticket ticket) {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        return (Long) em.createNamedQuery("Study.member.count").setParameter("groupMemberships", groupMemberships).getResultList().get(0);
    }

    @Override
    public Long getMemberActiveStudyCount(Ticket ticket) {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        return (Long) em.createNamedQuery("Study.member.count.active").setParameter("groupMemberships", groupMemberships).getResultList().get(0);
    }

    @Override
    public List<Study> getMemberStudies(Ticket ticket, Integer start, Integer maxResults, String orderBy, String direction) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        if (orderBy.toUpperCase().equals("NAME")) {

            if(direction.toUpperCase().equals("ASC")){
                return em.createNamedQuery("Study.member.name.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else if(direction.toUpperCase().equals("DESC")){
                return em.createNamedQuery("Study.member.name.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else{
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        }
        else if (orderBy.toUpperCase().equals("DATE")) {

            if(direction.toUpperCase().equals("ASC")){
                return em.createNamedQuery("Study.member.date.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else if(direction.toUpperCase().equals("DESC")){
                return em.createNamedQuery("Study.member.date.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else{
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        }
        else{
            throw new ConnexienceException("Sorting property is required. Can be either NAME or DATE");
        }
    }

    @Override
    public List<Study> getMemberActiveStudies(Ticket ticket, Integer start, Integer maxResults, String orderBy, String direction) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        if (orderBy.toUpperCase().equals("NAME")) {

            if(direction.toUpperCase().equals("ASC")){
                return em.createNamedQuery("Study.member.active.name.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else if(direction.toUpperCase().equals("DESC")){
                return em.createNamedQuery("Study.member.active.name.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else{
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        }
        else if (orderBy.toUpperCase().equals("DATE")) {

            if(direction.toUpperCase().equals("ASC")){
                return em.createNamedQuery("Study.member.active.date.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else if(direction.toUpperCase().equals("DESC")){
                return em.createNamedQuery("Study.member.active.date.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            }
            else{
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        }
        else{
            throw new ConnexienceException("Sorting property is required. Can be either NAME or DATE");
        }
    }

    @Override
    public Long getVisibleStudyCount(final Ticket ticket) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        return (Long) em.createNamedQuery("Study.visible.count").setParameter("groupMemberships", groupMemberships).getResultList().get(0);
    }

    @Override
    public List<Study> getVisibleStudies(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        return em.createNamedQuery("Study.visible").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
    }

    @Override
    public List<Study> getAllStudies(Ticket ticket) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            return em.createNamedQuery("Study.all.studies").getResultList();
        } else {
            throw new ConnexienceException("Only admins can list all studies");
        }
    }

    
    @Override
    public List<Study> getStudiesByExternalId(final Ticket ticket, final String externalId) throws ConnexienceException {
        List<Study> results = new ArrayList<>();

        List<Study> studies = em.createQuery("SELECT s FROM Study s WHERE s.externalId = :externalId").setParameter("externalId", externalId).getResultList();

        // add matching public projects, or projects ticket holder has access to
        for (final Study study : studies) {
            if (!study.isPrivateProject() || projectBean.isProjectMember(ticket, study)) {
                results.add(study);
            }
        }

        return results;
    }

    @Override
    public LoggerData addDataToGroup(final Ticket ticket, final Integer subjectGroupId, final String documentRecordId) throws ConnexienceException {
        final SubjectGroup subjectGroup = em.merge(subjectsBean.getSubjectGroup(ticket, subjectGroupId));

        if (subjectGroup.getLoggerDeployments().isEmpty()) {
            throw new ConnexienceException("Subject Group id='" + subjectGroupId + "' has no deployments, cannot add data.");
        }

        // add data to first deployment associated with subject group
        return addDataToDeployment(ticket, subjectGroup.getLoggerDeployments().iterator().next().getId(), documentRecordId);
    }

    @Override
    public LoggerData addDataToDeployment(final Ticket ticket, final Integer loggerDeploymentId, final String documentRecordId) throws ConnexienceException {
        final LoggerDeployment loggerDeployment = em.merge(loggersBean.getLoggerDeployment(ticket, loggerDeploymentId));

        if (loggerDeployment.getStudy() == null) {
            throw new ConnexienceException("Logger Deployment id='" + loggerDeploymentId + "' has no associated Study object, cannot add data.");
        }

        if (!projectBean.isProjectMember(ticket, loggerDeployment.getStudy())) {
            throw new ConnexienceException("You must be a member/admin of Study id='" + loggerDeployment.getStudy().getId() + "' to add data to its deployments.");
        }

        List<LoggerData> results = em.createNamedQuery("LoggerData.byEscId").setParameter("escId", documentRecordId).getResultList();

        if ((results == null) || results.isEmpty()) {
            LoggerData result = new LoggerData(documentRecordId);
            result.setLoggerDeployment(loggerDeployment);

            result = loggersBean.saveLoggerData(ticket, result);
            return result;
        } else if (results.size() > 1) {
            throw new ConnexienceException("More than one LoggerData object associated with the same DocumentRecord(id='" + documentRecordId + "')");
        } else {
            // TODO: update the LoggerData meta-data when that information is available (unused atm)
            return results.get(0);
        }
    }

    @Override
    public Collection<LoggerData> getStudyData(Ticket ticket, Integer studyId, Integer start, Integer maxResults) throws ConnexienceException {
        final Study study = getStudy(ticket, studyId);

        if (projectBean.isProjectMember(ticket, study)) {
            return em.createNamedQuery("LoggerData.byStudy").setParameter("studyId", studyId).setFirstResult(start).setMaxResults(maxResults).getResultList();
        } else {
            throw new ConnexienceException("User not a study member");
        }
    }


    @Override
    public void sendMessageToAdmins(Ticket ticket, Integer studyId, String title, String message) throws ConnexienceException {

        final Study study = getStudy(ticket, studyId);
        List<User> admins = groupBean.listGroupMembers(ticket, study.getAdminGroupId());
        for (User admin : admins) {
            messageBean.createTextMessage(ticket, admin.getId(), "", "", title, message);
        }
    }

    @Override
    public void notifyUploadFailure(Ticket ticket, Integer studyId, String uploadUsername, String filename, String errorMessage, long expectedSize, long stagedSize, long blobsize) throws ConnexienceException {

        final Study study = getStudy(ticket, studyId);

        String message = "User '" + uploadUsername + "' has attempted to upload " + filename +
                " into project " + study.getName() + ".  However, we have detected a problem with the upload:<p>" +
                "<p>" +
                "Error Message: " + errorMessage + "<p>" +
                "Expected Size: " + expectedSize + "<p>" +
                "Staged Sie: " + stagedSize + "<p>" +
                "Cloud BlobStore Size: " + blobsize + "<p>" +
                "<p>" +
                "Please ask " + uploadUsername + " to try to upload the file again or contact your system administrator. ";
        sendMessageToAdmins(ticket, studyId, "Upload Failure", message);
    }


    @Override
    public List<Phase> getPhases(final Ticket ticket, final Integer studyId) throws ConnexienceException {
        final Project project = projectBean.getProject(ticket, studyId);
        if (!(project instanceof Study)) {
            throw new ConnexienceException("id='" + studyId + "' is a Project but not a Study.");
        }

        return em.createQuery("SELECT p FROM Phase p WHERE p.study.id = :studyId")
                .setParameter("studyId", studyId)
                .getResultList();
    }

    @Override
    public List<Phase> getPhases(final Ticket ticket) throws ConnexienceException {

        return em.createQuery("SELECT p FROM Phase p")
                .getResultList();
    }

    @Override
    public List<String> getPhasesDataFolderIds(final Ticket ticket) throws ConnexienceException {

        return em.createQuery("SELECT p.dataFolderId FROM Phase p")
                .getResultList();
    }


    @Override
    public Study addPropertiesToStudy(Ticket ticket, Integer studyId, HashMap<String, String> properties) throws ConnexienceException {
        Study study = em.merge(getStudy(ticket, studyId));
        if(study!=null){
            for(String name : properties.keySet()){
                study.getAdditionalProperties().put(name, properties.get(name));
            }
            em.persist(study);
            return study;
        } else {
            return null;
        }
    }

    @Override
    public Phase addPhaseToStudy(final Ticket ticket, final Integer studyId, final String name) throws ConnexienceException {
        Study study = em.merge(getStudy(ticket, studyId));

        List<Phase> phases = em.createNamedQuery("Phase.namedPhase").setParameter("studyId", studyId).setParameter("name", name).getResultList();
        if (phases.size() > 1) {
            throw new ConnexienceException("Too many phases with name '" + name + "' in study: " + studyId);
        }

        if (study != null) {

            if (projectBean.isProjectAdmin(ticket, study)) {
                 return createPhase(ticket, study, name);
            } else {
                throw new ConnexienceException("Must be study administrator to add a phase");
            }
        } else {
            throw new ConnexienceException("Cannot find study with Id: " + studyId + " to create Phase");
        }
    }

    @Override
    public Phase editPhase(final Ticket ticket, final Integer phaseId, final String name) throws ConnexienceException {

        Phase phase = em.find(Phase.class, phaseId);
        if (phase != null) {
            if (projectBean.isProjectAdmin(ticket, phase.getStudy())) {
                phase.setName(name);

                em.persist(phase);

                return phase;
            } else {
                throw new ConnexienceException("Must be study administrator to edit a phase");
            }
        } else {
            throw new ConnexienceException("Cannot find phase with Id: " + phaseId);
        }

    }

    @Override
    public void removePhase(final Ticket ticket, final Integer phaseId, boolean removeFolders) throws ConnexienceException {

        Phase phase = em.find(Phase.class, phaseId);
        if (phase != null) {

            Study s = em.merge(phase.getStudy());

            if(removeFolders){
                EJBLocator.lookupStorageBean().removeFolderTree(ticket, phase.getDataFolderId(), true);
            }

            if (projectBean.isProjectAdmin(ticket, s)) {

                if(phase.getStudy().getPhases().size() > 1) {
                    em.remove(phase);
                }
                else{
                    throw new ConnexienceException("A Study must have at least one phase.  Please add another before removing this one.");
                }
            } else {
                throw new ConnexienceException("Must be study administrator to delete a phase");
            }
        } else {
            throw new ConnexienceException("Cannot find phase with Id: " + phaseId);
        }
    }

    @Override
    public Phase getPhaseByName(final Ticket ticket, final int studyId, final String name) {

        List<Phase> phases = em.createNamedQuery("Phase.namedPhase").setParameter("studyId", studyId).setParameter("name", name).getResultList();
        return phases.get(0);
    }

    @Override
    public boolean isPhaseUnique(final Ticket ticket, final int studyId, final String name) {
        List<Phase> phases = em.createNamedQuery("Phase.namedPhase").setParameter("studyId", studyId).setParameter("name", name).getResultList();
        return phases.size() == 0;
    }

    @Override
    public Phase createPhase(final Ticket ticket, Study study, final String name) throws ConnexienceException{

        study = em.merge(study);
        //set the properties
        Phase phase = new Phase();
        phase.setName(name);
        phase.setStudy(study);
        study.addPhase(phase);

        //Save the study and the phase
        em.persist(phase);
        em.persist(study);

        //Add a data folder for the phase
        Folder dataFolder = new Folder();
        dataFolder.setName("Phase: " + name);

        dataFolder.setProjectId(String.valueOf(study.getId()));
        dataFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, study.getDataFolderId(), dataFolder);

        phase.setDataFolderId(dataFolder.getId());
        em.persist(phase);

        // Ensure ACL is correct
        aclBean.grantAccess(ticket, study.getAdminGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
        aclBean.grantAccess(ticket, study.getAdminGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
        aclBean.grantAccess(ticket, study.getMembersGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
        aclBean.grantAccess(ticket, study.getMembersGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);


        return phase;
    }

    @Override
    public HashMap<String, String> getStudyStats(final Ticket ticket, Study study) throws ConnexienceException{
        study = em.merge(study);

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        HashMap<String, String> stats = new HashMap<>();
        stats.put("StudyName", study.getName());
        stats.put("NumPhases", String.valueOf(study.getPhases().size()));
        stats.put("NumDeployments", String.valueOf(study.getLoggerDeployments().size()));
        stats.put("StartDate", sdf.format(study.getStartDate()));
        stats.put("EndDate", sdf.format(study.getEndDate()));

        Long numParticipants = (Long) em.createNamedQuery("Subject.countExternalIdsInStudy")
                .setParameter("studyId", study.getId())
                .setParameter("externalId", study.getExternalId())
                .getResultList().get(0);
        stats.put("NumParticipants", String.valueOf(numParticipants));
        return stats;
    }

    @Override
    public HashMap<String, String> getStudyStats(Ticket ticket, int studyId) throws ConnexienceException {
        Study s = getStudy(ticket, studyId);
        return getStudyStats(ticket, s);
    }
}

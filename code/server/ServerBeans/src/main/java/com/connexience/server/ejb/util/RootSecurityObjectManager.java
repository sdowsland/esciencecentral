/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.util;

import com.connexience.server.ConnexienceException;
import com.connexience.server.crypto.ServerObjectKeyManager;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.LogonDetails;
import com.connexience.server.model.security.RootSecurityObject;
import com.connexience.server.model.security.User;
import com.connexience.server.util.RandomGUID;
import com.connexience.server.util.SignatureUtils;
import com.connexience.server.util.ZipUtils;
import java.io.File;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;
import org.apache.log4j.Logger;

/**
 * This class checks for the existence of a root security object. If it does not 
 * exist, it is created.
 * @author hugo
 */
public class RootSecurityObjectManager extends HibernateSessionContainer {
    private static Logger logger = Logger.getLogger(RootSecurityObjectManager.class);
    
    /** Creates a new instance of RootSecurityObjectManager */
    public RootSecurityObjectManager() {
    }

    /** Validate that there are a correct set of root logon objects present */
    private boolean validateRootLogon() throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from RootSecurityObject");
            List rootObjects = q.list();
            
            if(rootObjects.size()==1){
                // Correct number of objects, check other objects
                RootSecurityObject rootObject = (RootSecurityObject)rootObjects.get(0);
                
                // Check there is a user
                ServerObject user = null;
                if(rootObject.getRootUserId()!=null){
                    user = getObject(rootObject.getRootUserId(), User.class);
                    if(!(user instanceof User)){
                        // No user
                        return false;
                    }
                    
                } else {
                    // No user object
                    return false;
                }
                
                // Check there is a logon to match the user
                Query logonSearch = session.createQuery("from LogonDetails d where d.userId=?");
                logonSearch.setString(0, user.getId());
                List logons = logonSearch.list();
              return logons.size() == 1;
                
            } else {
                // Definately not right
                return false;
            }
                    
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }
   
    /** Delete all of the existing root security objects */
    private void cleanupOldSecurityObjects(Session session)
    {
        Query q = session.createQuery("from RootSecurityObject");
        List rootObjects = q.list();
        RootSecurityObject root;

        for (Object rootObject : rootObjects)
        {
          root = (RootSecurityObject) rootObject;
          if (root.getRootUserId() != null)
          {
            // TODO: Delete the logon and the user.
          }

        // Now delete
        session.delete(root);
      }
    }
    
    /** Create a new root security object and associated db objects */
    private void createRootSecurityObject(Session session) throws Exception {
        // Create object
        System.out.println("Creating root object");

        // Now create a root user object 
        User rootUser = new User();
        rootUser.setFirstName("Root");
        rootUser.setSurname("User");
        rootUser.setDescription("Root User");
        rootUser.setName("Root User");
        rootUser.setProtectedUser(true);
        session.persist(rootUser);

        // Create a logon for the user
        LogonDetails rootLogon = new LogonDetails();
        rootLogon.setUserId(rootUser.getId());

        // TODO: Get these from somewhere else
        rootLogon.setLogonName("root");
        
        String randomPassword = new RandomGUID().toString();
        logger.info("===== KEEP THIS ROOT PASSWORD:" + randomPassword + " =====");
        rootLogon.setHashedPassword(SignatureUtils.getHashedPassword(randomPassword));    //SHA-1 hash of 'password'
        ZipUtils.writeSingleLineFile(new File(System.getProperty("jboss.home.dir") + File.separator + "esc" + File.separator + "password.txt"), "===== KEEP THIS ROOT PASSWORD:" + randomPassword + " =====");
        
        session.persist(rootLogon);

        // Set up the root object
        RootSecurityObject root = new RootSecurityObject();
        root.setRootUserId(rootUser.getId());
        session.persist(root);   

        // Create keys for the user
        ServerObjectKeyManager mgr = new ServerObjectKeyManager();
        mgr.setupObjectKeyData(root);        
    }
    
    /** Initialise security object */
    public void initialiseSecurityObject() throws ConnexienceException {        
        Session session = null;
        try {
            if(!validateRootLogon()){
                session = getSession();
                cleanupOldSecurityObjects(session);
                createRootSecurityObject(session);
            }
            
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }
    
    /** Get the root logon object */
    public RootSecurityObject getRootSecurityObject() throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from RootSecurityObject");
            List l = q.list();
            if(l.size()==1){
                return (RootSecurityObject)l.get(0);
            } else {
                return null;
            }
            
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }
            
    /** Is a user the root system user */
    public final boolean isRootUser(User user) throws ConnexienceException {
        RootSecurityObject root = getRootSecurityObject();
      return root != null && root.getRootUserId().equals(user.getId());
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.datasets;

import com.connexience.server.model.datasets.DatasetsUtils;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetCatalog;
import com.connexience.server.model.datasets.DatasetConstants;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.datasets.DatasetQueryEnactor;
import com.connexience.server.model.datasets.DatasetQueryFactory;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.model.datasets.items.SingleValueItem;
import com.connexience.server.model.datasets.system.SystemDataset;
import com.connexience.server.model.datasets.system.SimpleSystemDatasetItem;
import com.connexience.server.model.datasets.util.JSONMultipleValueItemToDataConverter;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.util.StorageUtils;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jboss.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import org.json.JSONObject;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.io.CSVDataExporter;
import org.pipeline.core.data.io.JsonDataImporter;

/**
 * This bean provides the dashboard management code
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/DatasetsBean", beanInterface = DatasetsRemote.class)
public class DatasetsBean extends HibernateSessionContainer implements DatasetsRemote {
    private static Logger logger = Logger.getLogger(DatasetsBean.class);
    
    @Override
    public List listDatasets(Ticket ticket, boolean includeSystem) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            // TODO: Do query based on ticket project
            Query q = session.createQuery("from Dataset as obj where obj.creatorId=:id");
            q.setString("id", ticket.getUserId());
            List userDatasets = q.list();
            
            // Add system datasets if needed
            if(includeSystem){
                userDatasets.addAll(DatasetCatalog.listSystemDatasets());
            }
            
            return userDatasets;
        } catch (Exception e){
            throw new ConnexienceException("Error listing datasets: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Dataset getUserDatasetByName(Ticket ticket, String userId, String datasetName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dataset as obj where obj.name=:name and obj.creatorId=:userId");
            q.setString("name", datasetName);
            q.setString("userId", userId);
            List results = q.list();
            if(results.size()>0){
                Dataset d = (Dataset)results.get(0);
                assertPermission(ticket, d, Permission.READ_PERMISSION);
                return d;
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting dataset: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }

    }

    @Override
    public Dataset getNamedDatasetInFolder(Ticket ticket, String folderId, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dataset as obj where obj.containerId=:containerid and obj.name=:name");
            q.setString("containerid", folderId);
            q.setString("name", name);
            List results = q.list();
            if(results.size()>0){
                Dataset d = (Dataset)results.get(0);
                assertPermission(ticket, d, Permission.READ_PERMISSION);
                return d;
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting named dataset from folder: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    
    @Override
    public Dataset getUserDatasetByName(Ticket ticket, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dataset as obj where obj.name=:name and obj.creatorId=:userId");
            q.setString("name", name);
            q.setString("userId", ticket.getUserId());
            List results = q.list();
            if(results.size()>0){
                Dataset d = (Dataset)results.get(0);
                assertPermission(ticket, d, Permission.READ_PERMISSION);
                return d;
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting dataset: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Dataset getDataset(Ticket ticket, String id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dataset as obj where obj.id=:id");
            q.setString("id", id);
            List results = q.list();
            if(results.size()>0){
                Dataset d = (Dataset)results.get(0);
                assertPermission(ticket, d, Permission.READ_PERMISSION);
                return d;
            } else {
                // Might be a system dataset
                return DatasetCatalog.getSystemDataset(id);
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting dataset: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Dataset saveDataset(Ticket ticket, Dataset dashboard) throws ConnexienceException {
        if(!DatasetCatalog.isSystemDataset(dashboard)){
            return (Dataset)saveObjectWithAcl(ticket, dashboard);
        } else {
            throw new ConnexienceException("Cannot save a system dataset");
        }
    }

    @Override
    public void removeDataset(Ticket ticket, String id) throws ConnexienceException {
        Dataset db = getDataset(ticket, id);
        if(!DatasetCatalog.isSystemDataset(db)){
            assertPermission(ticket, db, Permission.WRITE_PERMISSION);
            
            // Drop all of the items
            List items = getDatasetItems(ticket, id);
            for(Object o : items){
                if(o instanceof DatasetItem){
                    removeDatasetItem(ticket, ((DatasetItem)o).getId());
                }
            }
            EJBLocator.lookupObjectRemovalBean().remove(db);
        } else {
            throw new ConnexienceException("Cannot remove a system dataset");
        }
    }

    @Override
    public void removeDatasetItem(Ticket ticket, long id) throws ConnexienceException {
        DatasetItem item = getDatasetItem(ticket, id);
        if(item!=null && !(item instanceof SimpleSystemDatasetItem)){
            Dataset db = getDataset(ticket, item.getDatasetId());
            assertPermission(ticket, db, Permission.WRITE_PERMISSION);
            Session session = null;
            try {
                session = getSession();
                if (item instanceof MultipleValueItem) {
                    // Remove all observations from item before deleting the item
                    DatasetsUtils.removeMultipleValueData(session, (MultipleValueItem) item);
                }              
                
                // Execute the delete callback if there is one
                if(item instanceof MultipleValueItem){
                    DatasetsUtils.itemDeleted((MultipleValueItem)item);
                }
                        
                // Do actual removal
                session.delete(item);
                
            } catch (Exception e){
                throw new ConnexienceException("Error deleting data from dataset item: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("No such dataset item");
        }
    }
    
    @Override
    public Dataset resetDataset(Ticket ticket, String id) throws ConnexienceException {
        Dataset db = getDataset(ticket, id);
        if(!DatasetCatalog.isSystemDataset(db)){
            assertPermission(ticket, db, Permission.WRITE_PERMISSION);
            List items = getDatasetItems(ticket, id);
            Session session = null;
            try {
                session = getSession();
                DatasetItem item;
                for(int i=0;i<items.size();i++){
                    item = (DatasetItem)items.get(i);
                    if(item instanceof SingleValueItem){
                        ((SingleValueItem)item).resetItem();
                        savePlainObject(item, session);
                    } else if(item instanceof MultipleValueItem){
                        // Remove all observations from item
                        DatasetsUtils.removeMultipleValueData(session, (MultipleValueItem)item);
                    }
                }
                return db;
            } catch (Exception e){
                throw new ConnexienceException("Error resetting dataset: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("Cannot reset a system dataset");
        }
    }

    @Override
    public void resetDatasetItem(Ticket ticket, long id) throws ConnexienceException {
        DatasetItem item = getDatasetItem(ticket, id);
        if(!(item instanceof SimpleSystemDatasetItem)){
            Session session = null;
            try {
                session = getSession();
                if(item instanceof SingleValueItem){
                    ((SingleValueItem)item).resetItem();
                    savePlainObject(item, session);
                } else if(item instanceof MultipleValueItem){
                    // Remove all observations from item
                    DatasetsUtils.removeMultipleValueData(session, (MultipleValueItem)item);
                }        
            } catch (Exception e){
                throw new ConnexienceException("Error resetting item: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("Cannot reset a system dataset item");
        }
    }
    
    @Override
    public List getDatasetItems(Ticket ticket, String id) throws ConnexienceException {
        Dataset db = getDataset(ticket, id);
        if(DatasetCatalog.isSystemDataset(db)){
            return ((SystemDataset)db).listItems();
        } else {
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from DatasetItem as obj where obj.datasetId=:datasetid");
                q.setString("datasetid", id);
                return q.list();
            } catch (Exception e){
                throw new ConnexienceException("Error getting dataset item by id: " + e.getMessage());
            } finally {
                closeSession(session);
            }        
        }
    }

    @Override
    public List aggregateDatasetItems(Ticket ticket, List<String> datasetIds) throws ConnexienceException {
        List results = new ArrayList();
        for(String id : datasetIds){
            try {
                results.addAll(getDatasetItems(ticket, id));
            } catch (Exception e){
                logger.error("Error joinng dataset: id " + e.getMessage());
            }
        }
        return results;
    }
    
    @Override
    public int getMultipleValueItemSize(Ticket ticket, String datasetId, String name) throws ConnexienceException {
        Session session= null;
        try {
            session = getSession();
            Query q = session.createQuery("from DatasetItem obj where obj.datasetId=:datasetid and obj.name=:name");
            q.setString("datasetid", datasetId);
            q.setString("name", name);
            Object result = q.uniqueResult();
            if(result instanceof MultipleValueItem){
                MultipleValueItem mvi = (MultipleValueItem)result;
                
                return DatasetsUtils.getMultipleValueDataSize(session, mvi);
            } else {
                throw new Exception("Object: " + name + " is not a multiple item value");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting multi value item size: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public JSONContainer queryMultipleValueItem(Ticket ticket, String datasetId, String name, int startIndex, int maxResults, String[] keys) throws ConnexienceException {
        Session session= null;
        try {
            session = getSession();
            Query q = session.createQuery("from DatasetItem obj where obj.datasetId=:datasetid and obj.name=:name");
            q.setString("datasetid", datasetId);
            q.setString("name", name);
            Object result = q.uniqueResult();
            if(result instanceof MultipleValueItem){
                MultipleValueItem mvi = (MultipleValueItem)result;
                
                Object results = DatasetsUtils.getMultipleValueData(session, mvi, startIndex, maxResults, keys);
                if(results instanceof JSONContainer){
                    return (JSONContainer)results;
                } else {
                    throw new ConnexienceException("Returned data is not a JSONContainer");
                }
            } else {
                throw new Exception("Object: " + name + " is not a multiple item value");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting multi value item size: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public JSONContainer queryMultipleValueItem(Ticket ticket, String datasetId, String name, int startIndex, int maxResults) throws ConnexienceException {
        return queryMultipleValueItem(ticket, datasetId, name, startIndex, maxResults, null);
    }

    @Override
    public JSONContainer performQuery(Ticket ticket, DatasetQuery query) throws ConnexienceException {
        Dataset ds = getDataset(ticket, query.getDatasetId());
        if(ds!=null){
            DatasetItem item = getDatasetItem(ticket, query.getDatasetId(), query.getItemName());
            if(query.isItemSupported(item)){
                DatasetQueryEnactor enactor = DatasetQueryFactory.createEnactorForQuery(query);
                if(enactor!=null){
                    // Give the enactor access to a connection
                    if(enactor.getConnectionType()==DatasetConstants.CONNECTION_TYPE.HIBERNATE_CONNECTION){
                        enactor.setSessionProvider(this);
                    } else if(enactor.getConnectionType()== DatasetConstants.CONNECTION_TYPE.JDBC_CONNECTION){
                        enactor.setConnectionProvider(this);
                    }

                    // Setup with query details and perform the query
                    enactor.setDataset(ds);
                    enactor.setItem(item);
                    enactor.setQuery(query);
                    enactor.setTicket(ticket);
                    return enactor.performQuery();

                } else {
                    throw new ConnexienceException("Could not create enactor for query");
                }

            } else {
                // Not supported
                throw new ConnexienceException("Unsupported item type: " + item.getClass().getName());
            }
            
        } else {
            throw new ConnexienceException("No such dataset: " + query.getDatasetId());
        }
    }
    
    @Override
    public DatasetItem getDatasetItem(Ticket ticket, String dashboardId, String name) throws ConnexienceException {
        if(DatasetCatalog.isSystemDataset(dashboardId)){
            // Get from the system dataset
            SystemDataset ds = (SystemDataset)DatasetCatalog.getSystemDataset(dashboardId);
            if(ds!=null){
                return ds.getItem(name);
            } else {
                throw new ConnexienceException("No such system dataset: " + dashboardId);
            }
            
        } else {
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from DatasetItem as obj where obj.datasetId=:datasetid and obj.name=:name");
                q.setString("datasetid", dashboardId);
                q.setString("name", name);
                List results = q.list();
                if(results.size()>0){
                    DatasetItem item = (DatasetItem)results.get(0);
                    Dataset db = getDataset(ticket, item.getDatasetId());
                    if(db!=null){
                        assertPermission(ticket, db, Permission.READ_PERMISSION);
                    }                
                    return item;
                } else {
                    return null;
                }
            } catch (Exception e){
                throw new ConnexienceException("Error getting dataset by id: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        }
    }

    @Override
    public DatasetItem getDatasetItem(Ticket ticket, long id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            DatasetItem item = getDashboardItem(id, session);
            if(item!=null){
                Dataset db = getDataset(ticket, item.getDatasetId());
                if(db!=null){
                    assertPermission(ticket, db, Permission.READ_PERMISSION);
                }                
                return item;
            } else {
                return null;
            }

        } catch (Exception e){
            throw new ConnexienceException("Error getting dataset by id: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    private DatasetItem getDashboardItem(long id, Session session) throws Exception {
        Query q = session.createQuery("from DatasetItem as obj where obj.id=:id");
        q.setLong("id", id);
        List results = q.list();
        if(results.size()>0){
            DatasetItem item = (DatasetItem)results.get(0);    
            return item;
        } else {
            return null;
        }
    }
    
    @Override
    public DatasetItem saveDatasetItem(Ticket ticket, DatasetItem item) throws ConnexienceException {
        try {
            Dataset db = getDataset(ticket, item.getDatasetId());
            if(!DatasetCatalog.isSystemDataset(db)){
                if(db!=null){
                    assertPermission(ticket, db, Permission.WRITE_PERMISSION);

                    if(item.getId()!=-1){
                        // Update existing
                        DatasetItem saved = (DatasetItem)savePlainObject(item);
                        
                        // Execute the save hook if there is one
                        if(saved instanceof MultipleValueItem){
                            DatasetsUtils.itemSaved((MultipleValueItem)item);
                        }
                        
                        return saved;
                    } else {
                        // Check for an existing item with the same name
                        DatasetItem existing = getDatasetItem(ticket, db.getId(), item.getName());
                        if(existing==null){
                            DatasetItem saved =  (DatasetItem)savePlainObject(item);
                            
                            // Execute the save hook if there is one
                            if(saved instanceof MultipleValueItem){
                                DatasetsUtils.itemSaved((MultipleValueItem)item);
                            }
                        
                            return saved;
                        } else {
                            throw new Exception("An item named: " + item.getName() + " already exists in dataset: " + db.getName());
                        }
                    }

                } else {
                    throw new Exception("Access denied");
                }
            } else {
                throw new ConnexienceException("Cannot save system dataset items");
            }
            
        } catch (Exception e){
            throw new ConnexienceException("Error saving dataset item: " + e.getMessage());
        }
    }

    @Override
    public DatasetItem setDatasetItemValue(Ticket ticket, long itemId, Object value) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            DatasetItem item = getDashboardItem(itemId, session);
            if(!(item instanceof SimpleSystemDatasetItem)){
                Dataset db = getDataset(ticket, item.getDatasetId());
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);

                if(item instanceof SingleValueItem){
                    // Set single value
                    ((SingleValueItem)item).setObjectValue(value);
                    savePlainObject(item, session);
                } else {
                    // Append to multiple value item
                    throw new ConnexienceException("Cannot set multiple values");
                }
                return item;       
            } else {
                throw new ConnexienceException("Cannot set system dataset item value");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error setting dataset item value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void quickUpdateDatasetItemWithValue(Ticket ticket, DatasetItem item, Object value) throws ConnexienceException {
        if(!(item instanceof SimpleSystemDatasetItem)){
            Session session = null;
            try {
                session = getSession();
                if(item instanceof SingleValueItem){
                    // Set single value
                    ((SingleValueItem)item).updateWithObjectValue(value);
                    savePlainObject(item, session);                
                } else {
                    DatasetsUtils.appendMultipleValueData(session, (MultipleValueItem)item, value);
                }
            } catch (Exception e){
                throw new ConnexienceException("Error doing quick update: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("Cannot update a system dataset item value");
        }
    }

    @Override
    public DatasetItem updateDatasetItemWithValue(Ticket ticket, long itemId, Object value) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            DatasetItem item = getDashboardItem(itemId, session);
            if(!(item instanceof SimpleSystemDatasetItem)){
                Dataset db = getDataset(ticket, item.getDatasetId());
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);

                if(item instanceof SingleValueItem){
                    // Set single value
                    ((SingleValueItem)item).updateWithObjectValue(value);
                    savePlainObject(item, session);
                } else {
                    DatasetsUtils.appendMultipleValueData(session, (MultipleValueItem)item, value);
                }
                return item;               
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error updating dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Object updateExistingMultipleValueItemRow(Ticket ticket, String datasetId, String name, String rowId, Object value) throws ConnexienceException {
        Session session = null;
        try {                
            Dataset db = getDataset(ticket, datasetId);
            DatasetItem item = getDatasetItem(ticket, datasetId, name);
            session = getSession();
            
            if(!(item instanceof SimpleSystemDatasetItem)){
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);
                if(!(item instanceof SingleValueItem)){
                    DatasetsUtils.updateMultipleValueData(session, (MultipleValueItem)item,rowId, value);
                } else {
                    throw new ConnexienceException("Cannot update a single row item like this");
                }
                return item;               
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error updating dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    @Override
    public Object updateExistingMultipleValueItemRow(Ticket ticket, String datasetId, String name, long rowId, Object value) throws ConnexienceException {
        Session session = null;
        try {                
            Dataset db = getDataset(ticket, datasetId);
            DatasetItem item = getDatasetItem(ticket, datasetId, name);
            session = getSession();
            
            if(!(item instanceof SimpleSystemDatasetItem)){
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);
                if(!(item instanceof SingleValueItem)){
                    DatasetsUtils.updateMultipleValueData(session, (MultipleValueItem)item,rowId, value);
                } else {
                    throw new ConnexienceException("Cannot update a single row item like this");
                }
                return item;               
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error updating dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void removeExistingMultipleValueItemRow(Ticket ticket, String datasetId, String itemName, long rowId) throws ConnexienceException {
        Session session = null;
        try {                
            Dataset db = getDataset(ticket, datasetId);
            DatasetItem item = getDatasetItem(ticket, datasetId, itemName);
            session = getSession();
            
            if(!(item instanceof SimpleSystemDatasetItem)){
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);
                if(!(item instanceof SingleValueItem)){
                    DatasetsUtils.removeMultipleValueDataRow(session, (MultipleValueItem)item, rowId);
                } else {
                    throw new ConnexienceException("Cannot update a single row item like this");
                }
           
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error removing dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    @Override
    public void removeExistingMultipleValueItemRow(Ticket ticket, String datasetId, String itemName, String rowId) throws ConnexienceException {
        Session session = null;
        try {                
            Dataset db = getDataset(ticket, datasetId);
            DatasetItem item = getDatasetItem(ticket, datasetId, itemName);
            session = getSession();
            
            if(!(item instanceof SimpleSystemDatasetItem)){
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);
                if(!(item instanceof SingleValueItem)){
                    DatasetsUtils.removeMultipleValueDataRow(session, (MultipleValueItem)item, rowId);
                } else {
                    throw new ConnexienceException("Cannot update a single row item like this");
                }
           
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error removing dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    /** Get a multiple value row froma multi-row item */
    public JSONContainer getMultipleValueDataRow(Ticket ticket, String datasetId, String itemName, String rowId) throws ConnexienceException {
        Session session = null;
        try {                
            Dataset db = getDataset(ticket, datasetId);
            DatasetItem item = getDatasetItem(ticket, datasetId, itemName);
            session = getSession();
            
            if(!(item instanceof SimpleSystemDatasetItem)){
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);
                if(!(item instanceof SingleValueItem)){
                    Object result = DatasetsUtils.getMultipleValueDataRow(session, (MultipleValueItem)item, rowId);
                    if(result instanceof JSONContainer){
                        return (JSONContainer)result;
                    } else {
                        throw new ConnexienceException("Returned value is not JSON data");
                    }
                } else {
                    throw new ConnexienceException("Cannot update a single row item like this");
                }
           
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error removing dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }        
    }    
    
    /** Get a multiple value row froma multi-row item */
    public JSONContainer getMultipleValueDataRow(Ticket ticket, String datasetId, String itemName, long rowId) throws ConnexienceException {
        Session session = null;
        try {                
            Dataset db = getDataset(ticket, datasetId);
            DatasetItem item = getDatasetItem(ticket, datasetId, itemName);
            session = getSession();
            
            if(!(item instanceof SimpleSystemDatasetItem)){
                assertPermission(ticket, db, Permission.WRITE_PERMISSION);
                if(!(item instanceof SingleValueItem)){
                    Object result = DatasetsUtils.getMultipleValueDataRow(session, (MultipleValueItem)item, rowId);
                    if(result instanceof JSONContainer){
                        return (JSONContainer)result;
                    } else {
                        throw new ConnexienceException("Returned value is not JSON data");
                    }
                } else {
                    throw new ConnexienceException("Cannot update a single row item like this");
                }
           
            } else {
                throw new ConnexienceException("Cannot update a system dataset item");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error removing dataset value: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }        
    }
    
    @Override
    public Object getDatasetItemValue(Ticket ticket, DatasetItem item) throws ConnexienceException {
        if(item instanceof SingleValueItem){
            item = getDatasetItem(ticket, item.getId());
            return ((SingleValueItem)item).getObjectValue();
            
        } else if(item instanceof SimpleSystemDatasetItem){
            try {
                return new JSONContainer(((SimpleSystemDatasetItem)item).getValue(ticket));
            } catch (Exception e){
                throw new ConnexienceException(e);
            }
            
        } else {
            Session session = null;
            try {
                session = getSession();
                return DatasetsUtils.getMultipleValueData(session, (MultipleValueItem)item);
            } catch (Exception e){
                throw new ConnexienceException("Error getting dataset item value: " + e.getMessage(), e); 
            } finally {
                closeSession(session);
            }
        }
    }

    @Override
    public Object getDatasetItemValue(Ticket ticket, String datasetId, String name) throws ConnexienceException {
        DatasetItem item = getDatasetItem(ticket, datasetId, name);
        if(item!=null){
            return getDatasetItemValue(ticket, item);
        } else {
            throw new ConnexienceException("No such item: " + name + " in dataset");
        }
    }

    @Override
    public long extractItemDataToDocument(Ticket ticket, String datasetId, String name, DocumentRecord doc, int blockSize) throws ConnexienceException {
        Dataset ds = getDataset(ticket, datasetId);
        DatasetItem item = getDatasetItem(ticket, ds.getId(), name);
        long rowCount = 0;
        if(item!=null){
            if(item instanceof MultipleValueItem){
                UserTransaction tx = null;
                PrintWriter writer = null;
                Session session = null;
                File tempFile = null;
                try {
                    tempFile = File.createTempFile("data", "csv");
                    //tx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                    //tx.begin();
                    session = getSession();
                    long size = getMultipleValueItemSize(ticket, datasetId, name);
                    
                    CSVDataExporter exporter = new CSVDataExporter();
                    exporter.openFile(tempFile);
                    JSONMultipleValueItemToDataConverter importer;
                    JSONObject block;
                    Data dataBlock;
                    Object blockObject;
                    
                    for(int pos = 0; pos<size ; pos+=blockSize){
                        blockObject = DatasetsUtils.getMultipleValueData(session, (MultipleValueItem)item, pos, blockSize);
                        if(blockObject instanceof JSONContainer){
                            block = ((JSONContainer)blockObject).getJSONObject();
                            importer = new JSONMultipleValueItemToDataConverter(block);
                            dataBlock = importer.convert();
                            exporter.appendData(dataBlock);
                            rowCount+=dataBlock.getSmallestRows();
                        } else {
                            throw new ConnexienceException("Data returned in extractItemDataToDocument was not a JSONContainer");
                        }
                    }


                    exporter.closeFile();
                    
                    // Write this file to storage
                    StorageUtils.upload(ticket, tempFile, doc, "Exported data");
                    //tx.commit();
                    return rowCount;
                } catch (Exception e) {
                    try {
                        //tx.rollback();
                    } catch(Exception ex){
                        logger.error("Error rolling back transaction in extractItemDataToDocument: " + e.getMessage(), ex);
                    }
                    throw new ConnexienceException("Error querying data: " + e.getMessage(), e);
                } finally {
                    closeSession(session);
                    if(tempFile!=null && tempFile.exists()){
                        if(!tempFile.delete()){
                            tempFile.deleteOnExit();
                        }
                    }
                }
            } else {
                return 0;
            }
        } else {
            throw new ConnexienceException("Item: " + name + " does not exist in selected dataset");
        }
    }
    
    
}
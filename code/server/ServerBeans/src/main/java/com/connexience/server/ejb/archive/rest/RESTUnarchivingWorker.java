/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.rest;

import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.storage.DocumentVersionManager;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.storage.DataStore;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@MessageDriven(mappedName="queue/RESTUnarchivingQueue", activationConfig =
{
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/RESTUnarchivingQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class RESTUnarchivingWorker extends HibernateSessionContainer implements MessageListener
{
    private static final Logger logger = Logger.getLogger(RESTUnarchivingWorker.class.getName());

    @Resource
    private MessageDrivenContext messageDrivenContext;

    /**
     * Creates a new instance of RESTUnarchivingWorker
     */
    public RESTUnarchivingWorker()
    {
    }

    public void onMessage(Message message)
    {
        logger.info("RESTUnarchivingWorker: onMessage");
        try
        {
            int opcode = message.getIntProperty(MessageUtils.OPCODE_PROPERTYNAME);

            if (opcode == MessageUtils.UNARCHIVE_REQUEST_OPCODEVALUE)
            {
                String serviceURL  = message.getStringProperty(MessageUtils.SERVICEURL_PROPERTYNAME);
                String documentId  = message.getStringProperty(MessageUtils.DOCUMENTID_PROPERTYNAME);
                String dataStoreId = message.getStringProperty(MessageUtils.DATASTOREID_PROPERTYNAME);

                logger.debug("Message received:");
                logger.debug("    serviceURL  = [" + serviceURL + "]");
                logger.debug("    documentId  = [" + documentId + "]");
                logger.debug("    dataStoreId = [" + dataStoreId + "]");

                DocumentRecord documentRecord       = null;
                ServerObject   documentRecordObject = getObject(documentId, DocumentRecord.class);
                if (documentRecordObject instanceof DocumentRecord)
                    documentRecord = (DocumentRecord) documentRecordObject;
                else
                    logger.warn("Unable to get document record (" + documentId + ")");

                DataStore    dataStore       = null;
                ServerObject dataStoreObject = getObject(dataStoreId, DataStore.class);
                if (dataStoreObject instanceof DataStore)
                    dataStore = (DataStore) dataStoreObject;
                else
                    logger.warn("Unable to get data store (" + dataStoreId + ")");

                if ((documentRecord != null) && (dataStore != null))
                {
                    try
                    {
                        DocumentVersionManager documentVersionManager = new DocumentVersionManager(documentRecord);

                        List<DocumentVersion> documentVersions = new LinkedList<>();
                        Session sessionList = null; // TODO: Improve session management
                        try
                        {
                            sessionList      = getSession();
                            documentVersions = documentVersionManager.getVersions(sessionList);
                        }
                        finally
                        {
                            closeSession(sessionList);
                        }

                        boolean archivingSuccess = true;
                        for (DocumentVersion documentVersion: documentVersions)
                        {
                            String            query      = URLEncoder.encode("documentId", "UTF-8") + "=" + URLEncoder.encode(documentId, "UTF-8") + "&" + URLEncoder.encode("versionId", "UTF-8") + "=" + URLEncoder.encode(documentVersion.getId(), "UTF-8"); 
                            URL               url        = new URL(serviceURL + "?" + query);
                            logger.debug("url = " + url);
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("GET");
                            connection.setRequestProperty("Content-Type", "application/octet-stream");
                            connection.setDoOutput(true);
                            connection.setDoInput(true);
                            connection.setUseCaches(false);

                            InputStream connectionStream = connection.getInputStream();
                            dataStore.readFromStream(documentRecord, documentVersion, connectionStream, documentVersion.getSize());
                            connectionStream.close();

                            logger.debug("response code     = " + connection.getResponseCode());

                            archivingSuccess = archivingSuccess && (connection.getResponseCode() == 200);
                        }

                        Session sessionUpdate = null; // TODO: Improve session management
                        try
                        {
                            sessionUpdate = getSession();
                            if (archivingSuccess)
                                documentRecord.setCurrentArchiveStatus(DocumentRecord.UNARCHIVED_ARCHIVESTATUS);
                            else
                                documentRecord.setCurrentArchiveStatus(DocumentRecord.UNARCHIVING_ERROR_ARCHIVESTATUS);
                            savePlainObject(documentRecord, sessionUpdate);
                        }
                        finally
                        {
                            closeSession(sessionUpdate);
                        }
                    }
                    catch (Throwable throwable)
                    {
                        logger.warn("Problem downloading document version: ", throwable);
                        throw throwable;
                    }
                }
            }
            else
                logger.warn("Message with unexpected opcode: " + opcode);
        }
        catch (JMSException jmsException)
        {
            jmsException.printStackTrace();
            messageDrivenContext.setRollbackOnly();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            messageDrivenContext.setRollbackOnly();
        }
    }
}

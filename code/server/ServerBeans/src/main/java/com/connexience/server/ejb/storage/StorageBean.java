/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ConnexienceSecurityException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.acl.AccessControlRemote;
import com.connexience.server.ejb.remove.ObjectRemovalRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.OrganisationFolderAccess;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.archive.ArchiveStore;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.document.UncommittedVersion;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.metadata.MetadataQuery;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.study.Phase;
import com.connexience.server.model.security.*;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.util.PathSplitter;
import com.connexience.server.util.StorageUtils;
import java.io.FileNotFoundException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jboss.logging.Logger;
import org.json.JSONObject;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJBLocalHome;


/**
 * This EJB provides document storage functions to the system.
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/StorageBean", beanInterface = StorageRemote.class)
@SuppressWarnings("unchecked")
public class StorageBean extends HibernateSessionContainer implements com.connexience.server.ejb.storage.StorageRemote
{
    private static final Logger _Logger = Logger.getLogger(StorageBean.class);
    private ObjectMapper mapper;

    /** Creates a new instance of StorageBean */
    public StorageBean() {
        mapper = new ObjectMapper();
        mapper.enableDefaultTyping();
    }

    /** Get a document type by mime type */
    public DocumentType getDocumentTypeByMime(Ticket ticket, String mime) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from DocumentType as obj where obj.mimeType=? and obj.organisationId=?");
                q.setString(0, mime);
                q.setString(1, ticket.getOrganisationId());
                List results = q.list();
                if (results.size() > 0) {
                    DocumentType type = (DocumentType) results.get(0);
                    assertPermission(ticket, type, Permission.READ_PERMISSION);
                    return type;
                } else {
                    return null;
                }
            } catch (ConnexienceException ce) {
                throw ce;
            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get a document type by extension */
    public DocumentType getDocumentTypeByExtension(Ticket ticket, String extension) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from DocumentType as obj where obj.extension=? and obj.organisationId=?" );
                q.setString(0, extension.toLowerCase());
                q.setString(1, ticket.getOrganisationId());
                List results = q.list();
                if (results.size() > 0) {
                    DocumentType type = (DocumentType) results.get(0);
                    assertPermission(ticket, type, Permission.READ_PERMISSION);
                    return type;
                } else {
                    return null;
                }
            } catch (ConnexienceException ce) {
                throw ce;
            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** List document types for the ticket organisation */
    public List listDocumentTypes(Ticket ticket) throws ConnexienceException {

        if (ticket.isAssociatedWithNonRootOrg()) {
            Session session = null;
            List results = null;
            try {
                session = getSession();
                Query q = session.createQuery("from DocumentType as obj");
                results = q.list();
            } catch (Exception e) {
                throw new ConnexienceException("Error listing DocumentTypes: " + e.getMessage());
            } finally {
                closeSession(session);
            }

            if (results != null) {
                //return narrowListWithAcl(ticket, results);
                return results;
            } else {
                throw new ConnexienceException("Error listing DocumentTypes: no results returned");
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get a specific document type */
    public DocumentType getDocumentType(Ticket ticket, String documentTypeId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            DocumentType docType = (DocumentType) getObject(documentTypeId, DocumentType.class);
            if (docType != null) {
                assertPermission(ticket, docType, Permission.READ_PERMISSION);
                return docType;
            } else {
                throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Remove a document type */
    public void removeDocumentType(Ticket ticket, String documentTypeId) throws ConnexienceException {
        DocumentType docType = (DocumentType) getObject(documentTypeId, DocumentType.class);
        if (docType != null) {
            assertPermission(ticket, docType, Permission.WRITE_PERMISSION);
            EJBLocator.lookupObjectRemovalBean().remove(docType);
        } else {
            throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
        }
    }

    /** Save a document type */
    public DocumentType saveDocumentType(Ticket ticket, DocumentType type) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            OrganisationFolderAccess folders = new OrganisationFolderAccess(ticket.getOrganisationId());
            DocumentTypesFolder folder = folders.getDocumentTypesFolder();
            if (folder != null) {
                type.setContainerId(folder.getId());
                return (DocumentType) saveObjectWithAcl(ticket, type);
            } else {
                throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /**
     * Get the next Version object for a document record. What gets returned depends on the
     * settings in the DocumentRecord. If the document is versioned a new version is returned. If
     * there is only a single version, the existing version returned if there is one, otherwise
     * a new version is created. This is controlled by the DocumentVersionManager object
     */
    public DocumentVersion createNextVersion(Ticket ticket, String documentId) throws ConnexienceException {

        ServerObject docObj = (DocumentRecord) getObject(documentId, DocumentRecord.class);
        if (docObj instanceof DocumentRecord) {
            assertPermission(ticket, docObj, Permission.WRITE_PERMISSION);
            DocumentVersionManager manager = new DocumentVersionManager((DocumentRecord) docObj);
            Session session = null;
            try {
                session = getSession();
                return manager.createVersion(session);

            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage(), e);
            } finally {
                closeSession(session);
            }

        } else {
            throw new ConnexienceException("No such document");
        }
    }

    /** Update a version object to the database */
    @Override
    public DocumentVersion updateVersion(Ticket ticket, DocumentVersion version) throws ConnexienceException {
        String recordId = version.getDocumentRecordId();
        DocumentRecord docObject = (DocumentRecord) getObject(recordId, DocumentRecord.class);
        if (docObject != null) {
            assertPermission(ticket, docObject, Permission.WRITE_PERMISSION);

            //updatet the size of the latest version
            docObject.setCurrentVersionSize(version.getSize());
            docObject.setCurrentVersionTimestamp(version.getTimestamp());
                    
            saveObject(ticket, docObject);

            return (DocumentVersion) savePlainObject(version);
        } else {
            throw new ConnexienceException("Object is not a document");
        }
    }

    @Override
    public DocumentVersion getVersion(Ticket ticket, String versionId) throws ConnexienceException {
        Session session = null;
        DocumentVersion docVersion;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersion as dv where dv.id=:versionId");
            q.setString("versionId", versionId);
            List results = q.list();
            if (results.size() > 0) {
                docVersion = (DocumentVersion) results.get(0);
            } else {
                docVersion = null;
            }
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }

        // Check for read access on the document
        if (docVersion != null) {
            DocumentRecord docObject = (DocumentRecord) getObject(docVersion.getDocumentRecordId(), DocumentRecord.class);
            assertPermission(ticket, docObject, Permission.READ_PERMISSION);
            return docVersion;
        } else {
            return null;
        }
    }

    
    /** Get a version by number from the database */
    public DocumentVersion getVersion(Ticket ticket, String documentId, int version) throws ConnexienceException {

        Session session = null;
        DocumentVersion docVersion;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersion as dv where dv.documentRecordId=? and dv.versionNumber=?");
            q.setString(0, documentId);
            q.setInteger(1, version);
            List<DocumentVersion> results = q.list();
            if (results.size() > 0) {
                docVersion = results.get(0);
            } else {
                docVersion = null;
            }
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }

        // Check for read access on the document
        if (docVersion != null) {
            DocumentRecord docObject = (DocumentRecord) getObject(docVersion.getDocumentRecordId(), DocumentRecord.class);
            assertPermission(ticket, docObject, Permission.READ_PERMISSION);
            return docVersion;
        } else {
            return null;
        }

    }

    /** Get a specific version from the database */
    public DocumentVersion getVersion(Ticket ticket, String documentId, String versionId) throws ConnexienceException {

        Session session = null;
        DocumentVersion version;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersion as dv where dv.id=?");
            q.setString(0, versionId);
            List<DocumentVersion> results = q.list();
            if (results.size() > 0) {
                version = results.get(0);
            } else {
                version = null;
            }
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }

        // Check for read access on the document
        if (version != null) {
            DocumentRecord docObject = (DocumentRecord) getObject(version.getDocumentRecordId(), DocumentRecord.class);
            assertPermission(ticket, docObject, Permission.READ_PERMISSION);
            return version;
        } else {
            throw new ConnexienceException("No such version");
        }
    }

    /** Get the latest versions of a set of document records */
    public List getLatestVersions(Ticket ticket, List documentIds) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            String documentId;
            DocumentRecord doc;
            ServerObject docObject;

            DocumentVersionManager versionManager = new DocumentVersionManager();
            List versionList = new ArrayList();

            for (Object documentId1 : documentIds) {
                documentId = documentId1.toString();

                docObject = getObject(documentId, DocumentRecord.class);
                if (docObject instanceof DocumentRecord) {
                    doc = (DocumentRecord) docObject;
                    versionManager.setDocument(doc);
                    versionList.add(versionManager.getLatestVersion(session));
                }
            }

            return versionList;
        } catch (Exception e) {
            throw new ConnexienceException("Error listing document versions: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Get the latest version of a DocumentRecord */
    public DocumentVersion getLatestVersion(Ticket ticket, String documentId) throws ConnexienceException {
        return getLatestVersion(ticket, documentId, Permission.READ_PERMISSION);
    }

    @Override
    public void removeVersion(Ticket ticket, String documentId, String versionId) throws ConnexienceException {
        DocumentRecord doc = getDocumentRecord(ticket, documentId);
        if(doc!=null){
            assertPermission(ticket, doc, Permission.WRITE_PERMISSION);
            DocumentVersion v = getVersion(ticket, versionId);
            if(v.getDocumentRecordId().equals(doc.getId())){
                EJBLocator.lookupObjectRemovalBean().remove(v);
            } else {
                throw new ConnexienceException("Specified version does not belong to specified document");
            }
            
        } else {
            throw new ConnexienceException("Object is not a document");
        }
    }

    
    /** Get the latest version of a DocumentRecord assuming the user has the supplied permission */
    public DocumentVersion getLatestVersion(Ticket ticket, String documentId, String permissionTypeToAssert) throws ConnexienceException {

        DocumentRecord docObj = (DocumentRecord) getObject(documentId, DocumentRecord.class);
        if (docObj != null) {
            assertPermission(ticket, docObj, permissionTypeToAssert);
            DocumentVersionManager manager = new DocumentVersionManager(docObj);
            Session session = null;
            try {
                session = getSession();
                return manager.getLatestVersion(session);

            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage());
            } finally {
                closeSession(session);
            }

        } else {
            throw new ConnexienceException("Object is not a document");
        }

    }

    @Override
    public String getLatestVersionId(String documentId) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;

        try {
            c = getSQLConnection();
            s = c.prepareStatement("select id from documentversions where documentrecordid=? order by versionnumber desc limit 1");
            s.setString(1, documentId);
            r = s.executeQuery();
            if (r.next()) {
                return r.getString(1);
            } else {
                throw new ConnexienceException("No versions for document: " + documentId);
            }
        } catch (ConnexienceException x) {
            throw x;
        } catch (Exception e) {
            throw new ConnexienceException("Error reading document record: " + documentId + ": " + e.getMessage(), e);
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }
        }
    }


    /** List the versions for a document */
    public List listVersions(Ticket ticket, String documentId) throws ConnexienceException {

        // Locally available
        DocumentRecord docObj = (DocumentRecord) getObject(documentId, DocumentRecord.class);
        if (docObj != null) {
            assertPermission(ticket, docObj, Permission.READ_PERMISSION);
            DocumentVersionManager manager = new DocumentVersionManager(docObj);;

            Session session = null;
            try {
                session = getSession();
                return manager.getVersions(session);

            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage());
            } finally {
                closeSession(session);
            }

        } else {
            throw new ConnexienceException("Object is not a document");
        }

    }

    /** Save a datastore */
    public DataStore saveDataStore(Ticket ticket, DataStore store) throws ConnexienceException {

        // Does the store exist
        if (ticket.isAssociatedWithNonRootOrg()) {
            if (store.getId() != null) {
                if (store.getOrganisationId() == null) {
                    store.setOrganisationId(ticket.getOrganisationId());
                }

                // Make sure org is correct and save
                if (ticket.getOrganisationId().equals(store.getOrganisationId())) {
                    assertPermission(ticket, store, Permission.WRITE_PERMISSION);
                    return (DataStore) saveObject(ticket, store);

                } else {
                    throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
                }

            } else {
                // If not, create and make sure the org id is correct
                if (store.getOrganisationId() == null) {
                    store.setOrganisationId(ticket.getOrganisationId());
                }

                // Make sure org is correct and save
                if (ticket.getOrganisationId().equals(store.getOrganisationId())) {
                    return (DataStore) saveObjectWithAcl(ticket, store);
                } else {
                    throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
                }
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the datastore for an organisation */
    public DataStore getOrganisationDataStore(Ticket ticket, String organisationId) throws ConnexienceException {

        Organisation org = getCachedOrganisation(organisationId);
        String storeId = org.getDataStoreId();
        if (storeId != null) {
            if (organisationId.equals(ticket.getOrganisationId()) || ticket.isSuperTicket()) {
                DataStore storeObj = (DataStore) getObject(storeId, DataStore.class);
                if (storeObj != null) {
                    // Set read / write enabled flag
                    storeObj.setWriteEnabled(EJBLocator.lookupPreferencesBean().isStorageEnabled());
                    return storeObj;
                } else {
                    throw new ConnexienceException("Object is not a datastore");
                }
            } else {
                // Not the same organisation
                throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
            }
        } else {
            return null;
        }

    }

    /** Get a datastore */
    public DataStore getDataStore(Ticket ticket, String storeId) throws ConnexienceException {

        DataStore storeObj = (DataStore) getObject(storeId, DataStore.class);
        if (storeObj != null) {
            if (ticket.isSuperTicket() || storeObj.getOrganisationId().equals(ticket.getOrganisationId())) {
                storeObj.setWriteEnabled(EJBLocator.lookupPreferencesBean().isStorageEnabled());
                return storeObj;
            } else {
                throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
            }
        } else {
            return null;
        }
    }

    /** Save an archivestore */
    public ArchiveStore saveArchiveStore(Ticket ticket, ArchiveStore store) throws ConnexienceException {

        // Does the store exist
        if (ticket.isAssociatedWithNonRootOrg()) {
            if (store.getId() != null) {
                if (store.getOrganisationId() == null) {
                    store.setOrganisationId(ticket.getOrganisationId());
                }

                // Make sure org is correct and save
                if (ticket.getOrganisationId().equals(store.getOrganisationId())) {
                    assertPermission(ticket, store, Permission.WRITE_PERMISSION);
                    return (ArchiveStore) saveObject(ticket, store);

                } else {
                    throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
                }

            } else {
                // If not, create and make sure the organisation id is correct
                if (store.getOrganisationId() == null) {
                    store.setOrganisationId(ticket.getOrganisationId());
                }

                // Make sure organisation is correct and save
                if (ticket.getOrganisationId().equals(store.getOrganisationId())) {
                    return (ArchiveStore) saveObjectWithAcl(ticket, store);
                } else {
                    throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
                }
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the archivestore for an organisation */
    public ArchiveStore getOrganisationArchiveStore(Ticket ticket, String organisationId) throws ConnexienceException {

        Organisation org = getCachedOrganisation(organisationId);
        String storeId = org.getArchiveStoreId();
        if (storeId != null) {
            if (organisationId.equals(ticket.getOrganisationId()) || ticket.isSuperTicket()) {
                ArchiveStore storeObj = (ArchiveStore) getObject(storeId, DataStore.class);
                if (storeObj != null) {
                    return storeObj;
                } else {
                    throw new ConnexienceException("Object is not a datastore");
                }
            } else {
                // Not the same organisation
                throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
            }
        } else {
            return null;
        }

    }

    /** Get an archivestore */
    public ArchiveStore getArchiveStore(Ticket ticket, String storeId) throws ConnexienceException {

        ArchiveStore storeObj = (ArchiveStore) getObject(storeId, ArchiveStore.class);
        if (storeObj != null) {
            if (ticket.isSuperTicket() || storeObj.getOrganisationId().equals(ticket.getOrganisationId())) {
                return storeObj;
            } else {
                throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
            }
        } else {
            return null;
        }
    }

    /** Get the top level folders for an organisation that a user can see */
    public List getTopLevelFolders(Ticket ticket) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            OrganisationFolderAccess folderAccess = new OrganisationFolderAccess(ticket.getOrganisationId());

            ArrayList folders = new ArrayList();
            Folder folder;

            // Users folder
            folder = folderAccess.getUsersFolder();
            folders.add(folder);


            // Groups folder
            folder = folderAccess.getGroupsFolder();
            folders.add(folder);

            // Data folder
            folder = folderAccess.getDataFolder();
            folders.add(folder);

            // Document types folder
            folder = folderAccess.getDocumentTypesFolder();
            folders.add(folder);

            // Services folder
            folder = folderAccess.getServicesFolder();
            folders.add(folder);

            // Applicataions folder
            folder = folderAccess.getApplicationsFolder();
            folders.add(folder);
            return narrowListWithAcl(ticket, folders);

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the top level data folder for the ticket organisation */
    public DataFolder getDataFolder(Ticket ticket) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            OrganisationFolderAccess folders = new OrganisationFolderAccess(ticket.getOrganisationId());
            DataFolder folder = folders.getDataFolder();

            // Only security check that is done with the data folder is to
            // verify that the caller is in the same organisation.
            if (folder.getOrganisationId().equals(ticket.getOrganisationId())) {
                return folder;
            } else {
                throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the parent hierarchy for a folder */
    @Override
    public List getParentHierarchy(Ticket ticket, String folderId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            OrganisationFolderAccess folders = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder f = getFolder(ticket, folderId);
            return folders.getParentHierarchy(folderId);
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Add a child to a folder */
    public Folder addChildFolder(Ticket ticket, String parentId, Folder child) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            // Set permissions if this is a new folder
            boolean setPermissions;
            if(child.getId()==null || child.getId().isEmpty()){
                setPermissions = true;
            } else {
                setPermissions = false;
            }

            OrganisationFolderAccess folderAccess = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder parent = folderAccess.getFolder(parentId);
            if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, parent, Permission.WRITE_PERMISSION) || EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, parent, Permission.ADD_PERMISSION)) {
                child = folderAccess.addChildFolder(ticket, parent, child);

                // Propagate permissions to this folder from the parent
                if (setPermissions) {
                    PermissionList p = EJBLocator.lookupAccessControlBean().getObjectPermissions(getInternalTicket(), parentId);
                    PermissionList childPermissions = p.createCopyForObject(child.getId());
                    EJBLocator.lookupAccessControlBean().setObjectPermissions(getInternalTicket(), child.getId(), childPermissions);
                }

                return child;
            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, parent));
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Remove all of the documents from a folder. This does not remove child folders */
    public void removeFolderDocumentRecords(Ticket ticket, String folderId) throws ConnexienceException {
        List documents = getFolderDocumentRecords(ticket, folderId);
        ObjectRemovalRemote removalBean = EJBLocator.lookupObjectRemovalBean();
        AccessControlRemote aclBean = EJBLocator.lookupAccessControlBean();

        DocumentRecord record;
        for (Object document : documents) {
            record = (DocumentRecord) document;
            if (aclBean.canTicketAccessResource(ticket, record, Permission.WRITE_PERMISSION)) {
                removalBean.remove(record);
            } else {
                //do nothing
            }
        }
    }
    
    /** Remove all of the documents and child folders */
    @Override
    public void removeFolderContents(Ticket ticket, String folderId) throws ConnexienceException {
        removeFolderDocumentRecords(ticket, folderId);
        
        List children = getChildFolders(ticket, folderId);
        for(Object o : children){
            if(o instanceof Folder){
                removeFolderTree(ticket, ((Folder)o).getId());
            }
        }
    }

    public void removeFolderTree(Ticket ticket, String folderId) throws ConnexienceException {
        removeFolderTree(ticket, folderId, false);
    }

        /** Remove all child folders from the specified folder downwards including workflow and phase folders */
    @Override
    public void removeFolderTree(Ticket ticket, String folderId, boolean force) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            Folder folder = getFolder(ticket, folderId);
            if (folder != null) {
                User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
                if (!force && folder.getId().equals(u.getWorkflowFolderId())) {
                    throw new ConnexienceException("Cannot delete workflow folder");
                }

                List<String> phases = EJBLocator.lookupStudyBean().getPhasesDataFolderIds(ticket);
                if(!force && phases.contains(folderId)){
                    throw new ConnexienceException("Cannot delete folder for a phase");
                }
                else {
                    assertPermission(ticket, folder, Permission.WRITE_PERMISSION);
                    EJBLocator.lookupObjectRemovalBean().remove(ticket, folder);
                }
            } else {
                throw new ConnexienceException(ConnexienceException.NO_FOLDER_SPECIFIED_MESSAGE);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get a named DocumentRecord from a Folder */
    public DocumentRecord getNamedDocumentRecord(Ticket ticket, String folderId, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentRecord as obj where obj.name=? and obj.containerId=?");
            q.setString(0, name);
            q.setString(1, folderId);
            List results = q.list();
            if (results.size() > 0) {
                DocumentRecord doc = (DocumentRecord) results.get(0);
                assertPermission(ticket, doc, Permission.READ_PERMISSION);
                return doc;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Get a named Folder from a Folder */
    public Folder getNamedFolder(Ticket ticket, String folderId, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Folder as obj where obj.name=? and obj.containerId=?");
            q.setString(0, name);
            q.setString(1, folderId);
            List results = q.list();
            if (results.size() > 0) {
                Folder f = (Folder) results.get(0);
                assertPermission(ticket, f, Permission.READ_PERMISSION);
                return f;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }


    /** Get the contents of a folder. This returns document records within a folder */
    public List getFolderDocumentRecords(Ticket ticket, String folderId) throws ConnexienceException {
        List results = new ArrayList();
        Folder f = getFolder(ticket, folderId);
        if (f instanceof SearchFolder) {
            // TODO: Add search folder implementation
            try {
                SearchFolder sf = (SearchFolder) f;
                if (sf.getQueryJson() != null && !sf.getQueryJson().isEmpty()) {
                    MetadataQuery q = new MetadataQuery();
                    JSONObject queryJson = new JSONObject(sf.getQueryJson());
                    q.readJson(queryJson);
                    return EJBLocator.lookupMetaDataBean().search(ticket, q);
                } else {
                    // No query
                    return new ArrayList();
                }
            } catch (Exception e) {
                throw new ConnexienceException("Error parsing metadata query: " + e.getMessage(), e);
            }
        } else {
            List objects = EJBLocator.lookupObjectDirectoryBean().getAllContainedObjectsUserHasAccessTo(ticket, folderId, null, 0, 0);
            for (Object object : objects) {
                if (object instanceof DocumentRecord) {
                    results.add(object);
                }
            }
            return results;
        }
    }

    /** Get everything in a folder */
    public List getAllFolderContents(Ticket ticket, String folderId) throws ConnexienceException {
        Folder f = getFolder(ticket, folderId);
        if (f instanceof SearchFolder) {
            // TODO: Add search folder implementation
            try {
                SearchFolder sf = (SearchFolder) f;
                if (sf.getQueryJson() != null && !sf.getQueryJson().isEmpty()) {
                    MetadataQuery q = new MetadataQuery();
                    JSONObject queryJson = new JSONObject(sf.getQueryJson());
                    q.readJson(queryJson);
                    List results = EJBLocator.lookupMetaDataBean().search(ticket, q);

                    // Set the container ID for the documents
                    for (int i = 0; i < results.size(); i++) {
                        ((ServerObject) results.get(i)).setContainerId(folderId);
                    }
                    return results;

                } else {
                    // No query
                    return new ArrayList();
                }
            } catch (Exception e) {
                throw new ConnexienceException("Error parsing metadata query: " + e.getMessage(), e);
            }

        } else {
            return EJBLocator.lookupObjectDirectoryBean().getAllContainedObjectsUserHasAccessTo(ticket, folderId, null, 0, 0);
        }
    }

    /** Get the child folders of a folder */
    public List getChildFolders(Ticket ticket, String folderId) throws ConnexienceException {
        OrganisationFolderAccess folderAccess = new OrganisationFolderAccess(ticket.getOrganisationId());
        Folder folder = folderAccess.getFolder(folderId);

        if (folder != null) {
            // Local folder
            assertPermission(ticket, folder, Permission.READ_PERMISSION);
            /*
            List contents = folderAccess.getChildFolders(folder);
            return narrowListWithAcl(contents);
             *
             */
            List results = new ArrayList();

            List objects = EJBLocator.lookupObjectDirectoryBean().getAllContainedObjectsUserHasAccessTo(ticket, folderId, null, 0, 0);
            for (Object object : objects) {
                if (object instanceof Folder) {
                    results.add(object);
                }
            }
            return results;
        } else {
            throw new ConnexienceException("Cannot find folder");
        }

    }

    /** Move a Folder to a new parent */
    public void moveFolder(Ticket ticket, String sourceFolderId, String targetFolderId, String targetProjectId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            // Get the folder
            OrganisationFolderAccess access = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder folder = access.getFolder(sourceFolderId);
            Folder target = access.getFolder(targetFolderId);
            if (OrganisationFolderAccess.isSpecialFolder(folder)) {
                throw new ConnexienceException("Cannot move a special folder");
            }

            if (OrganisationFolderAccess.isSpecialFolder(target)) {
                throw new ConnexienceException("Cannot move a folder into a special folder");
            }

            assertPermission(ticket, folder, Permission.WRITE_PERMISSION);
            assertPermission(ticket, target, Permission.WRITE_PERMISSION);

            // Is there an existing folder
            Folder existingFolder = access.getNamedChildFolder(targetFolderId, folder.getName().trim());
            if (existingFolder == null) {
                folder.setContainerId(target.getId());
                folder = (Folder) saveObject(ticket, folder);

                folder = (Folder) checkProjectId(ticket, folder, targetProjectId);

            } else {
                throw new ConnexienceException("Cannot move folder. Folder named: " + folder.getName().trim() + " already exists");
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Move a document to a new parent */
    public void moveDocument(Ticket ticket, String documentId, String targetFolderId, String targetProjectId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            OrganisationFolderAccess access = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder target = access.getFolder(targetFolderId);
            DocumentRecord doc = getDocumentRecord(ticket, documentId, Permission.WRITE_PERMISSION);

            if (OrganisationFolderAccess.isSpecialFolder(target)) {
                throw new ConnexienceException("Cannot move document to a special folder");
            }

            assertPermission(ticket, target, Permission.WRITE_PERMISSION);

            doc.setContainerId(target.getId());
            doc = (DocumentRecord) saveObject(ticket, doc);

            doc = (DocumentRecord) checkProjectId(ticket, doc, targetProjectId);
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Rename a Folder */
    public void renameFolder(Ticket ticket, String folderId, String name) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            // Get the folder
            OrganisationFolderAccess access = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder folder = access.getFolder(folderId);
            if (OrganisationFolderAccess.isSpecialFolder(folder)) {
                throw new ConnexienceException("Cannot rename a special folder");
            }
            assertPermission(ticket, folder, Permission.WRITE_PERMISSION);

            // Is there an existing folder
            Folder existingFolder = access.getNamedChildFolder(folderId, name.trim());
            if (existingFolder == null) {
                folder.setName(name.trim());
                saveObject(ticket, folder);
            } else {
                throw new ConnexienceException("Cannot rename folder. Folder named: " + name.trim() + " already exists");
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }


    /** Get a DocumentRecord object from the database */
    public DocumentRecord getDocumentRecord(Ticket ticket, String recordId) throws ConnexienceException {
        return getDocumentRecord(ticket, recordId, Permission.READ_PERMISSION);
    }

    /**
     * Get a DocumentRecord assuming the user has the permission type passed in.  Used in workflow blocks to get scripts that have
     * an execute permission
     */
    public DocumentRecord getDocumentRecord(Ticket ticket, String recordId, String permissionTypeToAssert) throws ConnexienceException {
        ServerObject object = this.getObject(recordId, DocumentRecord.class);
        if (object instanceof DocumentRecord) {
            assertPermission(ticket, object, permissionTypeToAssert);
            return (DocumentRecord) object;
        } else {
            throw new ConnexienceException("Object is not a Document Record");
        }
    }

    /** Remove a DocumentRecord from the database */
    public void removeDocumentRecord(Ticket ticket, String recordId) throws ConnexienceException {
        DocumentRecord object = (DocumentRecord) this.getObject(recordId, DocumentRecord.class);
        if (object != null) {
            assertPermission(ticket, object, Permission.WRITE_PERMISSION);
            if(EJBLocator.lookupPreferencesBean().isStorageEnabled()){
                EJBLocator.lookupObjectRemovalBean().remove(object);
            } else {
                throw new ConnexienceException("Storage System is Read-Only");
            }
        } else {
            throw new ConnexienceException("Object is not a Document Record");
        }

    }

    /** Save a document record to the database */
    public DocumentRecord saveDocumentRecord(Ticket ticket, DocumentRecord record) throws ConnexienceException {
        if (record.getContainerId() != null) {
            return (DocumentRecord) saveObjectWithAcl(ticket, record);
        } else {
            throw new ConnexienceException(ConnexienceException.NO_FOLDER_SPECIFIED_MESSAGE);
        }
    }

    @Override
    public ServerObject getOrCreateResourceAtURI(Ticket ticket, String baseFolderId, String uri, boolean makePublic) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            Session session = null;
            try {
                session = getSession();
                OrganisationFolderAccess folderAccess = new OrganisationFolderAccess(ticket.getOrganisationId());

                // Work through the child folders
                Folder currentParent = null;
                if (baseFolderId != null) {
                    // A root folder has been specified
                    currentParent = folderAccess.getFolder(baseFolderId);
                } else {
                    // Use the organisation base folder
                    currentParent = folderAccess.getDataFolder(session);
                }

                Folder childFolder;
                ServerObject resource = null;

                PathSplitter splitter = new PathSplitter(uri);
                List<String> folderNames = splitter.getPathElements();

                for (int i = 0; i < folderNames.size(); i++) {
                    // Is there a child folder with the correct name
                    childFolder = folderAccess.getNamedChildFolder(currentParent.getId(), folderNames.get(i), session);
                    if (childFolder != null) {
                        currentParent = childFolder;

                        // Is this the last item
                        if (i == folderNames.size() - 1) {
                            resource = childFolder;
                        }

                    } else {
                        // Not a folder, try and get as a DocumentRecord if it is the last element in the
                        // chain
                        if (i == folderNames.size() - 1) {
                            resource = StorageUtils.getOrCreateDocumentRecord(ticket, currentParent.getId(), folderNames.get(i));
                            if(makePublic){
                                EJBLocator.lookupAccessControlBean().grantAccess(ticket, getPublicUserId(ticket.getOrganisationId()), resource.getId(), Permission.READ_PERMISSION);
                            }
                            
                        } else {
                            // Need to make a folder
                            childFolder = new Folder();
                            childFolder.setName(folderNames.get(i));
                            childFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, currentParent.getId(), childFolder);
                            if(makePublic){
                                EJBLocator.lookupAccessControlBean().grantAccess(ticket, getPublicUserId(ticket.getOrganisationId()), childFolder.getId(), Permission.READ_PERMISSION);
                            }
                            currentParent = childFolder;
                        }

                    }
                }

                // Check permission
                if (resource != null) {
                    assertPermission(ticket, resource, Permission.READ_PERMISSION);
                    return resource;
                } else {
                    return null;
                }

            } catch (ConnexienceException ce) {
                throw ce;
            } catch (Exception e) {
                throw new ConnexienceException("Error looking up folder: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    
    /**
     * Get a folder based upon a URI string. This is derived from the Data folder
     * of the current organisation
     */
    public ServerObject getResourceByURI(Ticket ticket, String baseFolderId, String uri, String permissionType) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            Session session = null;
            try {
                session = getSession();
                OrganisationFolderAccess folderAccess = new OrganisationFolderAccess(ticket.getOrganisationId());

                // Work through the child folders
                Folder currentParent = null;
                if (baseFolderId != null) {
                    // A root folder has been specified
                    currentParent = folderAccess.getFolder(baseFolderId);
                } else {
                    // Use the organisation base folder
                    currentParent = folderAccess.getDataFolder(session);
                }

                Folder childFolder;
                ServerObject resource = null;

                PathSplitter splitter = new PathSplitter(uri);
                List<String> folderNames = splitter.getPathElements();

                for (int i = 0; i < folderNames.size(); i++) {
                    // Is there a child folder with the correct name
                    childFolder = folderAccess.getNamedChildFolder(currentParent.getId(), folderNames.get(i), session);
                    if (childFolder != null) {
                        currentParent = childFolder;

                        // Is this the last item
                        if (i == folderNames.size() - 1) {
                            resource = childFolder;
                        }

                    } else {
                        // Not a folder, try and get as a DocumentRecord if it is the last element in the
                        // chain
                        if (i == folderNames.size() - 1) {
                            resource = getNamedDocumentRecord(ticket, currentParent.getId(), folderNames.get(i));
                        } else {
                            throw new FileNotFoundException();
                        }

                    }
                }

                // Check permission
                if (resource != null) {
                    assertPermission(ticket, resource, permissionType);
                    return resource;
                } else {
                    return null;
                }

            } catch (ConnexienceException ce) {
                throw ce;
            } catch (FileNotFoundException fnfe){
                return null;
            } catch (Exception e) {
                throw new ConnexienceException("Error looking up folder: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get a specific folder by ID */
    public Folder getFolder(Ticket ticket, String folderId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            // Get the folder
            OrganisationFolderAccess access = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder folder = access.getFolder(folderId);
            if(folder!=null){
                assertPermission(ticket, folder, Permission.READ_PERMISSION);
            }
            return folder;
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get a folder by ShortName */
    public Folder getFolderByShortName(Ticket ticket, String folderShortName) throws ConnexienceException {
        OrganisationFolderAccess access = new OrganisationFolderAccess(ticket.getOrganisationId());
        Folder folder = access.getFolderByShortName(folderShortName);
        if (folder != null) {
            assertPermission(ticket, folder, Permission.READ_PERMISSION);
            return folder;
        } else {
            return null;
        }
    }

    /** Get the home folder for a User */
    public Folder getHomeFolder(Ticket ticket, String userId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
            if (user.getHomeFolderId() != null) {
                return getFolder(ticket, user.getHomeFolderId());
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the workflow folder for a User */
    public Folder getWorkflowFolder(Ticket ticket, String userId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
            if (user.getWorkflowFolderId() != null) {
                return getFolder(ticket, user.getWorkflowFolderId());
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }


    /** Get the list of links folders that are created in groups */
    public List<LinksFolder> listGroupLinkFolders(Ticket ticket, String groupId) throws ConnexienceException {
        Group group = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, groupId);
        assertPermission(ticket, group, Permission.READ_PERMISSION);
        Session session = null;

        try {
            session = getSession();
            return session.createQuery("FROM LinksFolder AS l WHERE l.groupId = :groupId").setString("groupId", groupId).list();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting linkGroupFolder ", e);
        } finally {
            closeSession(session);
        }
    }

    public Folder updateFolder(Ticket ticket, Folder folder) throws ConnexienceException {
        return (Folder) saveObjectWithAcl(ticket, folder);
    }

    /** Get the total size of the users files */
    public long getUserStorageUsage(Ticket ticket, String userId) throws ConnexienceException {
        ResultSet r = null;
        PreparedStatement s = null;
        Connection c = null;

        if (ticket.isSuperTicket() || ticket.getUserId().equals(userId)) {
            try {
                c = getSQLConnection();
                s = c.prepareStatement("select sum(size) from documentversions where userid=?");
                s.setString(1, userId);
                r = s.executeQuery();
                if (r.next()) {
                    return r.getLong(1);
                } else {
                    return -1;
                }

            } catch (Exception e) {
                throw new ConnexienceException("Error determinining user storage usage: " + e.getMessage());
            } finally {
                try {
                    r.close();
                } catch (Exception ignored) {
                }
                try {
                    s.close();
                } catch (Exception ignored) {
                }
                try {
                    c.close();
                } catch (Exception ignored) {
                }
            }
        } else {
            // Can only access your own storage details unless superuser
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }


    /**
     * Get the top 5 count of file types stored by the user.
     *
     * @param ticket User ticket
     * @return HashMap of File type (String) -> Number of files (Long)
     * @throws ConnexienceException Something broke
     */
    public HashMap<String, Long> getFileTypesForUser(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            HashMap<String, Long> fileTypes = new HashMap<>();

            Query numNoTypeQuery = session.createQuery("SELECT count(d.id) FROM DocumentRecord AS d WHERE d.creatorId = :userId AND d.documentTypeId IS NULL");
            numNoTypeQuery.setString("userId", ticket.getUserId());
            Long numNoType = (Long) numNoTypeQuery.uniqueResult();
            fileTypes.put("Unknown", numNoType);


            Query topTypesQuery = session.createSQLQuery("select d.name, count(d.name) from objectsflat as d, objectsflat as o " +
                    " where d.id = o.documenttypeid " +
                    " and d.objecttype = 'DOCUMENTTYPE' " +
                    " and o.objecttype = 'DOCUMENTRECORD' " +
                    " and o.creatorid = :userId " +
                    " group by d.name " +
                    " order by count(d.name) DESC");
            topTypesQuery.setString("userId", ticket.getUserId());
            topTypesQuery.setMaxResults(5);

            List<Object[]> topTypes = topTypesQuery.list();
            for (Object[] type : topTypes) {
                fileTypes.put((String) type[0], ((Number) type[1]).longValue());
            }


            return fileTypes;
        } catch (Exception e) {
            throw new ConnexienceException("Error getting data types for user ", e);
        } finally {
            closeSession(session);
        }
    }

    /** Make a folder a template folder which can be copied when a user registers */
    public TemplateFolder createTemplateFolder(Ticket ticket, Folder folder, String description) throws ConnexienceException {

        if (folder != null) {
            TemplateFolder templateFolder = new TemplateFolder();
            templateFolder.setFolderId(folder.getId());
            templateFolder.setName(folder.getName());
            templateFolder.setDescription(description);
            String defaultDomain = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, ticket.getUserId()).getDefaultDomain();
            templateFolder.setDomain(defaultDomain);
            savePlainObject(templateFolder);
            return templateFolder;
        } else {
            throw new ConnexienceException("Cannot set template folder when folder is null");
        }
    }

    /**
     * Get a list of the template folders for a particular domain
     *
     * @param ticket ticket
     * @param domain the domain of the user so that we can have different templates in an installation
     * @return List of template folders
     * @throws ConnexienceException something went wrong
     */
    public List<TemplateFolder> listTemplateFolders(Ticket ticket, String domain) throws ConnexienceException {
        Session session = null;
        try {
            if (domain == null) domain = "";
            session = getSession();
            Query q = session.createQuery("FROM TemplateFolder WHERE domain = :domain");
            q.setString("domain", domain);
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting TemplateFolders ", e);
        } finally {
            closeSession(session);
        }
    }

    //** Get all of the template folders */
    public List<TemplateFolder> listTemplateFolders(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM TemplateFolder");
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting TemplateFolders ", e);
        } finally {
            closeSession(session);
        }
    }

    /** Get a template folder by id */
    public TemplateFolder getTemplateFolder(Ticket ticket, String id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM TemplateFolder WHERE id = :id");
            q.setString("id", id);
            return (TemplateFolder) q.uniqueResult();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting TemplateFolder ", e);
        } finally {
            closeSession(session);
        }
    }

    /** Remove a template folder */
    public void deleteTemplateFolder(Ticket ticket, String id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM TemplateFolder WHERE id = :id");
            q.setString("id", id);
            TemplateFolder tf = (TemplateFolder) q.uniqueResult();

            if (tf != null) {
                session.delete(tf);
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting TemplateFolder ", e);
        } finally {
            closeSession(session);
        }
    }

    /** Add the contents of a template folder to a user's directory.  This will add a folder in the user's home directory */
    @Override
    public boolean addTemplateFolderToUsersSpace(Ticket currentUserTicket, String userId, TemplateFolder templateFolder) throws ConnexienceException {

        //Get the folder which the template folder represents
        Folder actualFolder = (Folder) getObject(templateFolder.getFolderId(), Folder.class);

        //Create a ticket for the user who owns the template folder.  This is necessary as we need their identity to copy the files.
        String userIdWhoOwnsFolder = actualFolder.getCreatorId();
        Ticket folderOwnerTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userIdWhoOwnsFolder);

        //Get the user's home folder where we're going to copy the data
        User userToCopyTo = EJBLocator.lookupUserDirectoryBean().getUser(currentUserTicket, userId);
        Folder homeFolder = EJBLocator.lookupStorageBean().getHomeFolder(currentUserTicket, currentUserTicket.getUserId());

        //Do the actual copy
        try {
            StorageUtils.copyFolderContentsAndReplaceWorkflowIds(folderOwnerTicket, actualFolder, userToCopyTo, homeFolder, templateFolder.getName());
        } catch (ConnexienceSecurityException e) {
            throw new ConnexienceException(e);
        }

        return false;
    }


    /**
     * Check the project Id of an object and change it if necessary.  Used when moving objects in Finder
     *
     * @param ticket          User's ticket
     * @param obj             ServerObject to check
     * @param targetProjectId Id to change it to.
     * @return object passed in but with the project Id changed if necessary
     * @throws ConnexienceException Something went wrong
     */

    public ServerObject checkProjectId(Ticket ticket, ServerObject obj, String targetProjectId) throws ConnexienceException {
        StorageRemote storageBean = EJBLocator.lookupStorageBean();
        AccessControlRemote acBean = EJBLocator.lookupAccessControlBean();

        //the current OldProject Id will be null if there is not one set
        String sourceProjectId = obj.getProjectId();
        if (sourceProjectId == null) {
            sourceProjectId = "";
        }

        //if it has changed then change the object and add a permission so that the project can read it
        //NB. This is not removed - design decision, can be explicitly removed by user
        if (!sourceProjectId.equals(targetProjectId)) {
            obj.setProjectId(targetProjectId);
            obj = saveObject(ticket, obj);

            if (targetProjectId != null && !targetProjectId.isEmpty()) {
                acBean.grantAccess(ticket, targetProjectId, obj.getId(), Permission.WRITE_PERMISSION);
                acBean.grantAccess(ticket, targetProjectId, obj.getId(), Permission.READ_PERMISSION);
            }
        }

        //recursion to set for all children and subfolders etc
        if (obj instanceof Folder) {
            Folder thisFolder = storageBean.getFolder(ticket, obj.getId());

            //check all of the docs in this folder
            List<DocumentRecord> docs = storageBean.getFolderDocumentRecords(ticket, obj.getId());
            for (DocumentRecord doc : docs) {
                checkProjectId(ticket, doc, targetProjectId);
            }

            //recurse into subfolders
            List<Folder> childFolders = storageBean.getChildFolders(ticket, thisFolder.getId());
            for (Folder child : childFolders) {
                checkProjectId(ticket, child, targetProjectId);
            }
        }

        return obj;
    }


    @Override
    public UncommittedVersion createUncommittedVersion(Ticket ticket, String documentId) throws ConnexienceException
    {
        ServerObject docObj = getObject(documentId, DocumentRecord.class);
        if (docObj == null || !(docObj instanceof DocumentRecord)) {
            throw new ConnexienceException("No such document");
        }
        assertPermission(ticket, docObj, Permission.WRITE_PERMISSION);

        Session session = null;
        try {
            session = getSession();
            UncommittedVersion version = new UncommittedVersion();
            version.setUserId(ticket.getUserId());
            version.setDocumentRecordId(documentId);
            version.setTimestamp(new java.util.Date());
            return (UncommittedVersion)savePlainObject(version, session);
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }


    @Override
    public void removeUncommittedVersion(Ticket ticket, String versionId) throws ConnexienceException
    {
        if (versionId != null) {
            Session session = null;
            try {
                UncommittedVersion v = new UncommittedVersion();
                v.setId(versionId);
                session = getSession();
                try {
                    session.delete(v);
                } catch (Exception x) {
                    _Logger.warn("Couldn't delete uncommitted version: " + versionId, x);
                }
            } catch (Exception x) {
                throw new ConnexienceException("Exception when removing a version for the UncommittedVersions table", x);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("versionId cannot be null");
        }
    }


    @Override
    public UncommittedVersion getUncommittedVersion(Ticket ticket, String versionId) throws ConnexienceException
    {
        UncommittedVersion version = null;
        Session session = null;
        try {
            session = getSession();
            Query q;
            q = session.createQuery("FROM UncommittedVersion WHERE id = :id");
            q.setString("id", versionId);
            List<UncommittedVersion> results = q.list();
            if (results.size() > 0)
                version = results.get(0);
            else
                version = null;
        } catch (Exception e) {
            throw new ConnexienceException("Error getting uncommitted version: " + versionId, e);
        } finally {
            closeSession(session);
        }

        // Check for read access on the document
        if (version != null) {
            ServerObject docObject = getObject(version.getDocumentRecordId(), DocumentRecord.class);
            assertPermission(ticket, docObject, Permission.READ_PERMISSION);
            return version;
        } else {
            throw new ConnexienceException("No such version");
        }
    }


    @Override
    public UncommittedVersion updateUncommittedVersion(Ticket ticket, UncommittedVersion version) throws ConnexienceException
    {
        ServerObject docObj = getObject(version.getDocumentRecordId(), DocumentRecord.class);
        if (docObj == null || !(docObj instanceof DocumentRecord)) {
            throw new ConnexienceException("No such document");
        }
        assertPermission(ticket, docObj, Permission.WRITE_PERMISSION);

        version.setUserId(ticket.getUserId());
        // Update timestamp to make the blob looking alive.
        version.setTimestamp(new java.util.Date());
        return (UncommittedVersion)savePlainObject(version);
    }


    /**
     * This operation is not for public use as it does not check READ_PERMISSIONs of any of the document versions
     * returned.
     */
    @Override
    public List<UncommittedVersion> listUncommittedVersions(Ticket ticket, Date olderThan) throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();
            Query q;
            if (olderThan != null) {
                q = session.createQuery("FROM UncommittedVersion WHERE timestamp < :time");
                q.setTimestamp("time", olderThan);
            } else {
                q = session.createQuery("FROM UncommittedVersion");
            }
            List<UncommittedVersion> result = q.list();
            return result;
        } catch (Exception e) {
            throw new ConnexienceException("Error listing uncommitted versions", e);
        } finally {
            closeSession(session);
        }
    }
}

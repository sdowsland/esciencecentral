/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.sqs.AmazonSQSClient;
import org.apache.log4j.Logger;

public class ClientUtils
{
    private static final Logger logger = Logger.getLogger(ClientUtils.class.getName());

    public static AmazonGlacierClient obtainGlacierClient(String accessKey, String secretKey, String domainName)
    {
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);

        AmazonGlacierClient amazonGlacierClient = new AmazonGlacierClient(awsCredentials);
        amazonGlacierClient.setEndpoint("https://glacier." + domainName + ".amazonaws.com/");

        return amazonGlacierClient;
    }

    public static AmazonSQSClient obtainSQSClient(String accessKey, String secretKey, String domainName)
    {
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);

        AmazonSQSClient amazonSQSClient = new AmazonSQSClient(awsCredentials);
        amazonSQSClient.setEndpoint("https://sqs." + domainName + ".amazonaws.com/");

        return amazonSQSClient;
    }

    public static void shutdownGlacierClient(AmazonGlacierClient amazonGlacierClient)
    {
        amazonGlacierClient.shutdown();
    }

    public static void shutdownSQSClient(AmazonSQSClient amazonSQSClient)
    {
        amazonSQSClient.shutdown();
    }
    
    public static String secretPrefix(String message)
    {
        if (message != null)
            return message.substring(0, 4) + "....";
        else
            return null;
    }
}

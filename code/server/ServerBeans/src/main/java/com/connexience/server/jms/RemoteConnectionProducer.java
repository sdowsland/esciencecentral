/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.jms;

import com.connexience.server.ConnexienceException;
import org.apache.log4j.Logger;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import javax.jms.ConnectionFactory;
import java.util.HashMap;
import java.util.Map;

@Alternative
public class RemoteConnectionProducer
{
    private static final Logger logger = Logger.getLogger(RemoteConnectionProducer.class);

    @Produces
    @InkspotConnectionFactory
    public ConnectionFactory createConnectionFactory() throws ConnexienceException
    {
        String hostname = JMSProperties.getHostname();

        logger.info("Using remote connection factory '" + hostname + ":5445'");

        Map<String, Object> params = new HashMap<>();
        params.put(TransportConstants.HOST_PROP_NAME, hostname);
        params.put(TransportConstants.PORT_PROP_NAME, 5445);

        TransportConfiguration configuration = new TransportConfiguration(NettyConnectorFactory.class.getName(), params);
        return (ConnectionFactory) HornetQJMSClient.createConnectionFactoryWithHA(JMSFactoryType.CF, configuration);
    }
}
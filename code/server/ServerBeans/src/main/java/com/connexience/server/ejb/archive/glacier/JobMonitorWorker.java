/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.archive.GlacierArchiveStore;
import org.apache.log4j.Logger;

import javax.jms.*;
import javax.naming.InitialContext;
import java.util.List;

public class JobMonitorWorker extends Thread
{
    private static final Logger logger = Logger.getLogger(JobMonitorWorker.class.getName());

    private static final long DEFAULT_TIMEOUT = 30 * 60 * 1000; // 30min

    private volatile boolean done;
    private long             timeout = DEFAULT_TIMEOUT;

    private String accessKey;
    private String secretKey;
    private String domainName;
    private String vaultName;
    private String queueURL;

    private Queue                  unarchivingQueue       = null;
    private QueueConnectionFactory queueConnectionFactory = null;

    public JobMonitorWorker(String name, GlacierArchiveStore glacierArchiveStore)
    {
        super(name);

        accessKey  = glacierArchiveStore.getAccessKey();
        secretKey  = glacierArchiveStore.getSecretKey();
        domainName = glacierArchiveStore.getDomainName();
        vaultName  = glacierArchiveStore.getVaultName();
        queueURL   = glacierArchiveStore.getQueueURL();
    }

    public void run()
    {
        logger.info("Job Monitor Worker: run, start");

        try
        {
            initQueues();

            done = false;
            while (! done)
            {
                // Look for completed jobs
                try
                {
                    AmazonSQSClient amazonSQSClient = ClientUtils.obtainSQSClient(accessKey, secretKey, domainName);
                    try
                    {
                        logger.debug("Check For Job Completions");
                        List<JobUtils.JobCompletion> jobCompletions = JobUtils.checkJobCompletions(amazonSQSClient, queueURL);
                        for (JobUtils.JobCompletion jobCompletion: jobCompletions)
                        {
                            logger.info("Processing Job Completion");
                            QueueConnection queueConnection = queueConnectionFactory.createQueueConnection();
                            queueConnection.start();
                            Session         queueSession    = queueConnection.createSession(false, QueueSession.AUTO_ACKNOWLEDGE);
                            MessageProducer messageProducer = queueSession.createProducer(unarchivingQueue);

                            Message message = queueSession.createMessage();

                            message.setIntProperty(MessageUtils.OPCODE_PROPERTYNAME, MessageUtils.DOWNLOAD_REQUEST_OPCODEVALUE);
                            message.setStringProperty(MessageUtils.ACCESSKEY_PROPERTYNAME, accessKey);
                            message.setStringProperty(MessageUtils.SECRETKEY_PROPERTYNAME, secretKey);
                            message.setStringProperty(MessageUtils.DOMAINNAME_PROPERTYNAME, domainName);
                            message.setStringProperty(MessageUtils.VAULTNAME_PROPERTYNAME, vaultName);
                            message.setStringProperty(MessageUtils.DOWNLOADID_PROPERTYNAME, jobCompletion.downloadId);
                            message.setStringProperty(MessageUtils.QUEUEURL_PROPERTYNAME, queueURL);
                            message.setStringProperty(MessageUtils.RECEIPTHANDLE_PROPERTYNAME, jobCompletion.receiptHandle);

                            messageProducer.send(message);

                            messageProducer.close();
                            queueSession.close();
                            queueConnection.close();
                        }
                    }
                    finally
                    {
                        ClientUtils.shutdownSQSClient(amazonSQSClient);
                    }

                    if (! done)
                    {
                        logger.info("Job Monitor Worker: run, sleeping");
                        Thread.sleep(timeout);
                    }
                }
                catch (InterruptedException interruptedException)
                {
                }
                catch (Throwable throwable)
                {
                    logger.error("Job Monitor Worker: Loop problem " + throwable);
                }
            }
        }
        catch (Throwable throwable)
        {
            logger.error("Job Monitor Worker: Problem " + throwable);
        }

        logger.info("Job Monitor Worker: run, end");
    }

    public void shutdown()
        throws InterruptedException
    {
        logger.debug("Job Monitor Worker: shutdown, start");
        done = true;
        interrupt();
        join();
        logger.debug("Job Monitor Worker: shutdown, end");
    }

    public long getTimeout()
    {
        return timeout;
    }

    public void setTimeout(long timeout)
    {
        this.timeout = timeout;
    }

    private void initQueues()
        throws ConnexienceException
    {
        try
        {
            InitialContext initialContext = new InitialContext();

            unarchivingQueue       = (Queue) initialContext.lookup("queue/AWSGlacierUnarchivingQueue");
            queueConnectionFactory = (QueueConnectionFactory) initialContext.lookup("ConnectionFactory");
        }
        catch (Throwable throwable)
        {
            logger.warn("Unable to obtain AWS Glacier queues or queue connection factory", throwable);
            unarchivingQueue       = null;
            queueConnectionFactory = null;
            throw new ConnexienceException("Unable to obtain AWS Glacier queues or queue connection factory", throwable);
        }
    }
}

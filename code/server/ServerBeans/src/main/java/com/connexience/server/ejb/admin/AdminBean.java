/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.ejb.admin;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.datasets.DatasetsRemote;
import com.connexience.server.ejb.properties.PropertiesRemote;
import com.connexience.server.ejb.storage.MetaDataRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.study.Phase;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.project.study.SubjectGroup;
import com.connexience.server.model.properties.PropertyItem;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Comment;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.util.RandomGUID;
import com.connexience.server.util.SerializationUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.engine.WorkflowInvocation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;

/**
 * Author: Simon Date: Jun 3, 2009
 * <p/>
 * This bean deals with adding and removing tags from objects
 */
@Stateless
@SuppressWarnings("unchecked")
@EJB(name = "java:global/ejb/AdminBean", beanInterface = AdminRemote.class)
public class AdminBean extends HibernateSessionContainer implements AdminRemote {

    @Inject
    private EntityManager em;

    public void createInboxFolders(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        Collection allUsers = EJBLocator.lookupOrganisationDirectoryBean().listOrganisationUsers(adminTicket, 0, 0);
        for (Object o : allUsers) {

            User user = (User) o;

            if (!user.getId().equals(org.getDefaultUserId())) {
                System.out.println("user.getName() = " + user.getName());

                //get a user ticket so that we appear to be acting as them
                Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(user.getId());

                if (user.getWebFolderId().equals(user.getHomeFolderId())) {
                    //create a new web folder
                    Folder webFolder = new Folder();
                    webFolder.setName("Web Folder");
                    webFolder.setCreatorId(user.getId());
                    webFolder.setContainerId(org.getDataFolderId());
                    webFolder.setDescription("Web folder for " + user.getName());
                    webFolder.setOrganisationId(org.getId());
                    webFolder = (Folder) saveObject(ticket, webFolder);
                    user.setWebFolderId(webFolder.getId());
                    user = (User) saveObject(ticket, user);
                }

                //create an inbox and sent messages folder if there isn't one
                if (user.getInboxFolderId() == null || user.getInboxFolderId().equals("")) {
                    Folder inbox = new Folder();
                    inbox.setName("Inbox");
                    inbox.setCreatorId(user.getId());
                    inbox.setContainerId(user.getWebFolderId());
                    inbox.setDescription("Inbox folder for " + user.getName());
                    inbox.setOrganisationId(org.getId());
                    inbox = (Folder) saveObject(ticket, inbox);
                    user.setInboxFolderId(inbox.getId());
                    user = (User) saveObject(ticket, user);
                }

                if (user.getSentMessagesFolderId() == null || user.getSentMessagesFolderId().equals("")) {
                    Folder sentFolder = new Folder();
                    sentFolder.setName("Sent Messages");
                    sentFolder.setCreatorId(user.getId());
                    sentFolder.setContainerId(user.getWebFolderId());
                    sentFolder.setDescription("Sent Messages folder for " + user.getName());
                    sentFolder.setOrganisationId(org.getId());
                    sentFolder = (Folder) saveObject(ticket, sentFolder);
                    user.setSentMessagesFolderId(sentFolder.getId());

                    saveObject(ticket, user);
                }
            }
        }
    }

    public void moveWorkflowFolders(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        Collection allUsers = EJBLocator.lookupOrganisationDirectoryBean().listOrganisationUsers(adminTicket, 0, 0);
        for (Object o : allUsers) {

            //get a user ticket so that we appear to be acting as them
            User user = (User) o;

            if (!user.getId().equals(org.getDefaultUserId()) && user.getHomeFolderId() != null && !user.getHomeFolderId().equals("")) {
                System.out.println("user.getName() = " + user.getName());

                Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(user.getId());

                //make sure there is a workflows folder for this user
                Folder usersWorkflowFolder = getOrCreateWorkflowFolder(ticket, user);

                //get all of the workflows
                Collection workflows = EJBLocator.lookupObjectDirectoryBean().getOwnedObjects(ticket, ticket.getUserId(), WorkflowDocument.class, 0, 0);
                for (Object w : workflows) {
                    WorkflowDocument workflow = (WorkflowDocument) w;

                    //look to see if there is a folder to hold this workflow
                    Folder thisWorkflowFolder = null;
                    Collection currentWorkflowFolders = EJBLocator.lookupStorageBean().getChildFolders(ticket, usersWorkflowFolder.getId());
                    for (Object r : currentWorkflowFolders) {
                        Folder f = (Folder) r;
                        if (f.getName().equals(workflow.getName())) {
                            thisWorkflowFolder = f;
                            break;
                        }
                    }

                    if (thisWorkflowFolder == null) {
                        //create a new folder
                        thisWorkflowFolder = new Folder();
                        thisWorkflowFolder.setContainerId(usersWorkflowFolder.getId());
                        thisWorkflowFolder.setCreatorId(ticket.getUserId());
                        thisWorkflowFolder.setDescription(workflow.getName() + " data folder");
                        thisWorkflowFolder.setName(workflow.getName());
                        thisWorkflowFolder.setOrganisationId(ticket.getOrganisationId());
                        //adding it as a child saves it
                        thisWorkflowFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, usersWorkflowFolder.getId(), thisWorkflowFolder);
                    }

                    //move each of the invocation folders for this workflow to the new folder
                    Collection invocationFolders = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(ticket, workflow.getId());
                    for (Object q : invocationFolders) {
                        WorkflowInvocationFolder invocationFolder = (WorkflowInvocationFolder) q;

                        //move the folder
                        invocationFolder.setContainerId(thisWorkflowFolder.getId());

                        //change the name
                        invocationFolder.setName(DateFormat.getDateTimeInstance().format(invocationFolder.getInvocationDate()));

                        //save the folder
                        saveObject(ticket, invocationFolder);
                    }

                    //move the workflow to the allworkflows folder
                    workflow.setContainerId(usersWorkflowFolder.getId());
                    saveObject(ticket, workflow);
                }

                //move all of the orphaned workflows
                Collection homeFolderChildren = EJBLocator.lookupStorageBean().getChildFolders(ticket, user.getHomeFolderId());
                for (Object homeFolderChild : homeFolderChildren) {
                    if (homeFolderChild instanceof WorkflowInvocationFolder) {
                        WorkflowInvocationFolder wif = (WorkflowInvocationFolder) homeFolderChild;

                        //look to see if there is a folder to hold this workflow
                        Folder thisWorkflowFolder = null;
                        Collection currentWorkflowFolders = EJBLocator.lookupStorageBean().getChildFolders(ticket, usersWorkflowFolder.getId());
                        for (Object r : currentWorkflowFolders) {
                            Folder f = (Folder) r;
                            if (f.getName().equals("Orphan workflows")) {
                                thisWorkflowFolder = f;
                                break;
                            }
                        }

                        if (thisWorkflowFolder == null) {
                            //create a new folder
                            thisWorkflowFolder = new Folder();
                            thisWorkflowFolder.setContainerId(usersWorkflowFolder.getId());
                            thisWorkflowFolder.setCreatorId(ticket.getUserId());
                            thisWorkflowFolder.setDescription("Orphan workflows data folder");
                            thisWorkflowFolder.setName("Orphan workflows");
                            thisWorkflowFolder.setOrganisationId(ticket.getOrganisationId());
                            //adding it as a child saves it
                            thisWorkflowFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, usersWorkflowFolder.getId(), thisWorkflowFolder);
                        }

                        //move the folder
                        wif.setContainerId(thisWorkflowFolder.getId());

                        //save the folder
                        saveObject(ticket, wif);
                    }
                }
            }
        }
    }

    public void moveNotes(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        Session session = null;
        try {
            session = getSession();

            Query q = session.createQuery("FROM Comment as c");
            Collection allComments = q.list();
            for (Object o : allComments) {
                Comment c = (Comment) o;
                c.setContainerId(c.getPostId());
                saveObject(adminTicket, c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeSession(session);
        }
    }

    public void moveProfileText(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        Connection conn = null;
        try {

            conn = getSQLConnection();
            Collection allUsers = EJBLocator.lookupOrganisationDirectoryBean().listOrganisationUsers(adminTicket, 0, 0);
            for (Object o : allUsers) {

                //get a user ticket so that we appear to be acting as them
                User user = (User) o;

                if (!user.getId().equals(org.getDefaultUserId())) {
                    System.out.println("Changing profile text for " + user.getDisplayName());

                    Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(user.getId());

                    UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, user.getId());
                    if (up == null) {
                        System.out.println("Error updating user profile for: " + user.getDisplayName() + ".  No " +
                                "profile");
                    } else {
                        String profileText = "";
                        Statement st = conn.createStatement();
                        ResultSet rs = st.executeQuery("SELECT * FROM userprofiles AS up WHERE up.id = '" + user.getProfileId() + "' ");
                        rs.next();
                        String address = rs.getString("address");
                        String interests = rs.getString("interests");
                        String awards = rs.getString("awards");
                        String eductation = rs.getString("education");

                        if (address != null && !address.equals("")) {
                            profileText += "<p>Address: <br/>" + address + "</p>";
                        }
                        if (interests != null && !interests.equals("")) {
                            profileText += "<p>Interests: <br/>" + interests + "</p>";
                        }
                        if (eductation != null && !eductation.equals("")) {
                            profileText += "<p>Education: <br/>" + eductation + "</p>";
                        }
                        if (awards != null && !awards.equals("")) {
                            profileText += "<p>Awards: <br/>" + awards + "</p>";
                        }

                        up.setText(profileText);
                        savePlainObject(up);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {

            }
        }
    }

    private Folder getOrCreateWorkflowFolder(Ticket ticket, User user) throws ConnexienceException {

        if (user.getWorkflowFolderId() == null) {
            //create a workflow folder for the user
            Folder folder = new Folder();
            folder.setName("Workflows");
            folder.setCreatorId(user.getId());
            folder.setContainerId(user.getHomeFolderId());
            folder.setDescription("Workflow folder for " + user.getName());
            folder.setOrganisationId(user.getOrganisationId());
            folder = (Folder) saveObject(ticket, folder);
            user.setWorkflowFolderId(folder.getId());
            saveObject(ticket, user);

            return folder;
        } else {
            return EJBLocator.lookupStorageBean().getFolder(ticket, user.getWorkflowFolderId());
        }
    }

    public List<User> mapUsernameToEmail(Ticket ticket) throws ConnexienceException {
        String emailAddress;
        UserProfile userProfile;
        String logonName;
        ArrayList<User> usersWithInvalidEmail = new ArrayList<>();

        for (User user : getAllUsers(ticket)) {
            userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, user.getId());
            emailAddress = userProfile.getEmailAddress();
            if (!isValidEmailAddress(emailAddress)) {
                logonName = EJBLocator.lookupUserDirectoryBean().getLogonName(ticket, user.getId());
                if (isValidEmailAddress(logonName)) {
                    userProfile.setEmailAddress(logonName);
                    savePlainObject(userProfile);
                } else {
                    usersWithInvalidEmail.add(user);
                }
            }
        }
        return usersWithInvalidEmail;
    }

    public List<User> getAllUsers(Ticket ticket) throws ConnexienceException {
        Organisation organisation = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
        printUsers(EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, organisation.getDefaultGroupId()));
        return EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, organisation.getDefaultGroupId());
    }

    private void printUsers(List<User> users) {
        for (User user : users) {
            System.out.println(user.getId());
        }
    }

    public void sendEmailToAllUsers(Ticket ticket, String subject, String content, String contentType) throws ConnexienceException {
        EJBLocator.lookupSMTPBean().sendMail(getUserIds(getAllUsersWithValidEmail(ticket)), subject, content, contentType);
    }

    public List<User> getAllUsersWithValidEmail(Ticket ticket) throws ConnexienceException {
        ArrayList<User> users = new ArrayList<>();
        UserProfile profile;
        for (User user : getAllUsers(ticket)) {
            profile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, user.getId());
            if (isValidEmailAddress(profile.getEmailAddress())) {
                users.add(user);
            }
        }
        return users;
    }

    private List<String> getUserIds(List<User> users) {
        ArrayList<String> userIds = new ArrayList<>();
        for (User user : users) {
            userIds.add(user.getId());
        }
        return userIds;
    }

    private boolean isValidEmailAddress(String emailAddress) {
        try {
            new InternetAddress(emailAddress, true);
        } catch (AddressException ignored) {
            return false;
        } catch (NullPointerException ignored) {
            return false;
        }
        return true;
    }

    /**
     * Method to add workflow invocation to users watch proeprty
     */
    public String addWorkflowFavourite(Ticket ticket, String workflowId) throws ConnexienceException {
        PropertiesRemote props = EJBLocator.lookupPropertiesBean();

        PropertyItem workflowWatches = props.getProperty(ticket, ticket.getUserId(), "workflows", "favourites");
        String favsString = "";
        if (workflowWatches == null) {
            favsString = workflowId + " ";
        } else {
            //if the blog id isn't already there then add it
            if (!workflowWatches.getValue().contains(workflowId)) {
                favsString += workflowWatches.getValue() + workflowId + " ";
            } else {
                favsString = workflowWatches.getValue();
            }
        }
        props.setProperty(ticket, ticket.getUserId(), "workflows", "favourites", favsString);
        return favsString;
    }

    /**
     * Method to remove workflow invocation to users watch proeprty
     */
    public void removeWorkflowFavourite(Ticket ticket, String workflowId) throws ConnexienceException {
        PropertiesRemote props = EJBLocator.lookupPropertiesBean();

        PropertyItem workflowWatches = props.getProperty(ticket, ticket.getUserId(), "workflows", "favourites");
        String favsString = workflowWatches.getValue();

        if (favsString != null) {
            //if the blog id isn't already there then add it
            if (favsString.contains(workflowId)) {
                String before = favsString.substring(0, favsString.indexOf(workflowId));
                String after = favsString.substring(favsString.indexOf(workflowId) + workflowId.length());
                favsString = before.trim() + " " + after.trim() + " ";
                favsString = favsString.trim() + " ";
                props.setProperty(ticket, ticket.getUserId(), "workflows", "favourites", favsString);
            }
        }
    }

    public void addHashKeyForUsers(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        Collection allUsers = EJBLocator.lookupOrganisationDirectoryBean().listOrganisationUsers(adminTicket, 0, 0);
        for (Object o : allUsers) {

            User user = (User) o;
            // Set the hash key if it does not exist
            if (user.getHashKey() == null) {
                user.setHashKey(new RandomGUID().valueAfterMD5);
                saveObject(adminTicket, user);
            }
        }
    }

    public int fixUserHomeFolderOwnser(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        int numFixed = 0;
        Session session = null;
        try {
            session = getSession();

            Collection allUsers = EJBLocator.lookupOrganisationDirectoryBean().listOrganisationUsers(adminTicket, 0, 0);
            for (Object o : allUsers) {

                User user = (User) o;
                Folder f = (Folder) getObject(user.getHomeFolderId(), Folder.class);
                if (f != null) {
                    if (!f.getCreatorId().equals(user.getId())) {
                        f.setCreatorId(user.getId());
                        saveObject(adminTicket, f);
                        numFixed++;
                    }
                }
            }
        } finally {
            closeSession(session);
        }
        return numFixed;
    }

    @Override
    public void migrateStorage(Ticket adminTicket, DataStore newStore) throws ConnexienceException {
        assertAdministrator(adminTicket);
        Session session = null;
        int migrationCount = 0;

        DataStore oldStore = EJBLocator.lookupStorageBean().getOrganisationDataStore(adminTicket, adminTicket.getOrganisationId());
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersion");
            List versions = q.list();
            DocumentVersion v;
            DocumentRecord r;

            for (Object o : versions) {
                v = (DocumentVersion) o;

                r = new DocumentRecord();
                r.setOrganisationId(adminTicket.getOrganisationId());
                r.setId(v.getDocumentRecordId());

                System.out.println("Migrating document: " + r.getId() + " v: " + v.getId());
                v = newStore.readFromStream(r, v, oldStore.getInputStream(r, v), v.getSize());
                System.out.println("Migrated: " + v.getSize() + " bytes");
                migrationCount++;
            }
            System.out.println("Migrated: " + migrationCount + " documents");
        } catch (Exception e) {
            throw new ConnexienceException("Error migrating storate: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    public void migrateProjects(Ticket adminTicket) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(adminTicket, adminTicket.getOrganisationId());
        User adminUser = EJBLocator.lookupUserDirectoryBean().getUser(adminTicket, adminTicket.getUserId());

        //must be organisation admin to be able to do this
        if (!EJBLocator.lookupGroupDirectoryBean().listGroupMembers(adminTicket, org.getAdminGroupId()).contains(adminUser)) {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }

        Connection conn = null;
        try {

            conn = getSQLConnection();

            Statement projectsSt = conn.createStatement();
            ResultSet projects = projectsSt.executeQuery("SELECT id AS projectid, name AS name, " +
                    "datafolder AS datafolder, creatorid AS creatorid FROM objectsflat AS obj WHERE obj.objecttype = " +
                    "'PROJECT'");
            while (projects.next()) {
                String projectId = projects.getString("projectid");
                String dataFolderId = projects.getString("datafolder");
                String creatorId = projects.getString("creatorid");
                if (dataFolderId == null || dataFolderId.equals("")) {
                    throw new Exception("Cannot find data folder for " + projects.getString("name"));
                } else {
                    Statement usersSt = conn.createStatement();
                    ResultSet usersRs = usersSt.executeQuery("SELECT homefolderid as homefolderid FROM objectsflat " +
                            "WHERE id = '" + creatorId + "'");
                    usersRs.next();
                    String homefolderid = usersRs.getString("homefolderid");

                    Statement updateProjectSt = conn.createStatement();
                    updateProjectSt.executeUpdate("UPDATE objectsflat SET containerid = '" + homefolderid + "'  WHERE " +
                            "id='" + dataFolderId + "'");

                    System.out.println("Updating project: " + projectId + " with dataFolder: " + dataFolderId + " to " +
                            "have containerId: " + homefolderid);

                    updateProjectSt.close();

                    usersRs.close();
                    usersSt.close();
                }
            }
            projects.close();
            projectsSt.close();
        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception ignored) {

            }
        }
    }


    public void addPhaseToStudy(Ticket adminTicket) throws ConnexienceException {
        Connection conn = null;
        try {

            conn = getSQLConnection();

            Statement studiesWithoutPhasesSt = conn.createStatement();
            ResultSet studiesWithoutPhases = studiesWithoutPhasesSt.executeQuery("SELECT id AS studyid FROM projects AS s WHERE s.id NOT IN (SELECT study_id FROM phases)");
            while (studiesWithoutPhases.next()) {

                int studyId = studiesWithoutPhases.getInt("studyid");

                Study study = EJBLocator.lookupStudyBean().getStudy(adminTicket, studyId);
                if(study == null){
                    throw new Exception("Can't find study with Id " + studyId);
                }

                System.out.println("Found study without phase: " + studyId);

                //create a new folder for the data in the phase
                Folder phaseFolder = new Folder();
                phaseFolder.setName("Phase: Default");
                phaseFolder.setContainerId(study.getDataFolderId());
                phaseFolder = EJBLocator.lookupStorageBean().addChildFolder(adminTicket, study.getDataFolderId(), phaseFolder);

                Statement nextValSt = conn.createStatement();
                ResultSet nextPhaseIdRS = nextValSt.executeQuery("SELECT nextval('esc_sequence') AS nv");
                nextPhaseIdRS.next();
                long newPhaseId = nextPhaseIdRS.getLong("nv");
                nextValSt.close();


                Statement createPhaseSt = conn.createStatement();
                createPhaseSt.executeUpdate("insert into phases (id, name, study_id, datafolderid) values (" + newPhaseId + ", 'Default', " + studyId + ", " + phaseFolder.getId() + ")");
                createPhaseSt.close();

                System.out.println("Created new phase: " + newPhaseId);

                Statement updateStudySt = conn.createStatement();
                ResultSet topLevelGroups = updateStudySt.executeQuery("SELECT id AS groupId FROM subjectgroups AS sg WHERE sg.study_id = " + studyId);
                while (topLevelGroups.next()) {
                    int topLevelGroupId = topLevelGroups.getInt("groupId");

                    System.out.println("Found top level group without phase: " + topLevelGroupId);

                    Statement addPhaseToGroupSt = conn.createStatement();
                    addPhaseToGroupSt.executeUpdate("UPDATE subjectgroups  SET phase_id = " + newPhaseId + " WHERE id = " + topLevelGroupId);
                    addPhaseToGroupSt.close();

                    System.out.println("Set PhaseId: " + newPhaseId + " on Group: " + topLevelGroupId);

                }
                topLevelGroups.close();
                updateStudySt.close();
            }

            //Add folders to studies which have a phase but the phase has no data folder.  Then move the groups inside
             //todo: add folder for phase
            Session session = null;
            try {
                session = getSession();
                javax.persistence.Query q = em.createQuery("from Phase p");
                List<Phase> allPhases = q.getResultList();
                for(Phase p : allPhases){


                    if(p.getDataFolderId() == null){
                        System.out.println("Found Phase without folder. " + p.getStudy().getName() + ":" + p.getName());
                        Folder phaseDataFolder = new Folder();
                        phaseDataFolder.setName("Phase: " + p.getName());
                        phaseDataFolder.setContainerId(p.getStudy().getDataFolderId());
                        phaseDataFolder = EJBLocator.lookupStorageBean().addChildFolder(adminTicket, p.getStudy().getDataFolderId(), phaseDataFolder);
                        System.out.println("Added folder: " + phaseDataFolder.getId());

                        p.setDataFolderId(phaseDataFolder.getId());
                        em.persist(p);
                        System.out.println("Added Phase data folder for " + p.getName() + ". Folder Id: " + phaseDataFolder.getId());
                        for(SubjectGroup sg : EJBLocator.lookupSubjectsBean().getSubjectGroups(adminTicket, p.getStudy().getId())){
                              if(sg.getParent() == null){
                                  System.out.println("Found top level group for phase: " + sg.getDisplayName());

                                  //top level groups
                                  Folder groupDataFolder = EJBLocator.lookupStorageBean().getFolder(adminTicket, sg.getDataFolderId());

                                  //change the container to the the phase data folder
                                  groupDataFolder.setContainerId(phaseDataFolder.getId());
                                  saveObject(adminTicket, groupDataFolder);
                                  System.out.println("Moved " + groupDataFolder.getId() + " into " + phaseDataFolder.getId());
                              }
                        }
                    }
                }


            } catch (Exception e){
                throw new ConnexienceException("Error adding phases to study: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }




        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

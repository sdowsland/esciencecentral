/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.archive.GlacierArchiveStore;
import com.connexience.server.model.archive.glacier.ArchiveMap;
import com.connexience.server.model.document.DocumentRecord;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Singleton
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@EJB(name = "java:global/ejb/ArchiveMapBean", beanInterface = ArchiveMapRemote.class)
public class ArchiveMapBean extends HibernateSessionContainer implements com.connexience.server.ejb.archive.glacier.ArchiveMapRemote {
    private static final Logger logger = Logger.getLogger(ArchiveMapBean.class.getName());

    public ArchiveMap createArchiveMapAsUploading(String documentRecordId, String documentVersionId, String dataStoreId, String archiveStoreId, String uploadId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            ArchiveMap archiveMap = new ArchiveMap();
            archiveMap.setDocumentRecordId(documentRecordId);
            archiveMap.setDocumentVersionId(documentVersionId);
            archiveMap.setDataStoreId(dataStoreId);
            archiveMap.setArchiveStoreId(archiveStoreId);
            archiveMap.setArchiveId(null);
            archiveMap.setUploadId(uploadId);
            archiveMap.setDownloadId(null);
            archiveMap.setSHA256TreeHash(null);
            archiveMap.setStatus(ArchiveMap.UPLOADING_STATUS);

            return (ArchiveMap) savePlainObject(archiveMap, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem creating ArchiveMap Uploading: [" + documentRecordId + "][" + documentVersionId + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public ArchiveMap updateArchiveMapToReuploaded(ArchiveMap archiveMap, String uploadId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            archiveMap.setArchiveId(null);
            archiveMap.setUploadId(uploadId);
            archiveMap.setDownloadId(null);
            archiveMap.setSHA256TreeHash(null);
            archiveMap.setStatus(ArchiveMap.UPLOADING_STATUS);

            return (ArchiveMap) savePlainObject(archiveMap, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update ArchiveMap Uploaded: [" + archiveMap.getDocumentRecordId() + "][" + archiveMap.getDocumentVersionId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public ArchiveMap updateArchiveMapToUploaded(ArchiveMap archiveMap, String archiveId, String fullChecksum)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            archiveMap.setArchiveId(archiveId);
            archiveMap.setUploadId(null);
            archiveMap.setDownloadId(null);
            archiveMap.setSHA256TreeHash(fullChecksum);
            archiveMap.setStatus(ArchiveMap.UPLOADED_STATUS);

            return (ArchiveMap) savePlainObject(archiveMap, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update ArchiveMap Uploaded: [" + archiveMap.getDocumentRecordId() + "][" + archiveMap.getDocumentVersionId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public ArchiveMap updateArchiveMapToDownloading(ArchiveMap archiveMap, String downloadId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            archiveMap.setUploadId(null);
            archiveMap.setDownloadId(downloadId);
            archiveMap.setStatus(ArchiveMap.DOWNLOADING_STATUS);

            return (ArchiveMap) savePlainObject(archiveMap, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update ArchiveMap Downloading: [" + archiveMap.getDocumentRecordId() + "][" + archiveMap.getDocumentVersionId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public ArchiveMap updateArchiveMapToDownloaded(ArchiveMap archiveMap)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            archiveMap.setUploadId(null);
            archiveMap.setDownloadId(null);
            archiveMap.setStatus(ArchiveMap.DOWNLOADED_STATUS);

            return (ArchiveMap) savePlainObject(archiveMap, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update ArchiveMap Downloaded: [" + archiveMap.getDocumentRecordId() + "][" + archiveMap.getDocumentVersionId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public void deleteArchiveMap(ArchiveMap archiveMap)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            session.delete(archiveMap);
        } catch (Exception exception) {
            throw new ConnexienceException("Problem deleting ArchiveMap: [" + archiveMap.getDocumentRecordId() + "][" + archiveMap.getDocumentVersionId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public List<ArchiveMap> getArchivesForDocumentRecord(String documentRecordId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();
            Query query = session.createQuery("FROM ArchiveMap AS obj WHERE obj.documentRecordId=?");
            query.setString(0, documentRecordId);

            return query.list();
        } catch (Exception exception) {
            throw new ConnexienceException("Problem getting ArchiveMap: [" + documentRecordId + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public ArchiveMap getArchiveMapForDocumentRecordAndDocumentVersion(String documentRecordId, String documentVersionId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();
            Query query = session.createQuery("FROM ArchiveMap AS obj WHERE obj.documentRecordId=? AND obj.documentVersionId=?");
            query.setString(0, documentRecordId);
            query.setString(1, documentVersionId);

            List<ArchiveMap> results = query.list();

            if (results.size() > 0)
                return results.get(0);
            else
                return null;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem getting ArchiveMap: [" + documentRecordId + "][" + documentVersionId + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public ArchiveMap getArchiveMapForDownload(String downloadId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();
            Query query = session.createQuery("FROM ArchiveMap AS obj WHERE obj.downloadId=?");
            query.setString(0, downloadId);

            List<ArchiveMap> results = query.list();

            if (results.size() > 0)
                return results.get(0);
            else
                return null;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem getting ArchiveMap: [" + downloadId + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public List<GlacierArchiveStore> getGlacierArchiveStores()
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();
 
            Query query = session.createQuery("FROM GlacierArchiveStore AS obj");

            return query.list();
        } catch (Exception exception) {
            throw new ConnexienceException("Problem getting GlacierArchiveStores", exception);
        } finally {
            closeSession(session);
        }
    }

    public DocumentRecord updateDocumentRecordToArchivingFailed(DocumentRecord documentRecord)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            documentRecord.setCurrentArchiveStatus(DocumentRecord.ARCHIVING_ERROR_ARCHIVESTATUS);

            return (DocumentRecord) savePlainObject(documentRecord, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update DocumentRecord Archiving Failed: [" + documentRecord.getId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public DocumentRecord updateDocumentRecordToUnarchivingFailed(DocumentRecord documentRecord)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            documentRecord.setCurrentArchiveStatus(DocumentRecord.UNARCHIVING_ERROR_ARCHIVESTATUS);

            return (DocumentRecord) savePlainObject(documentRecord, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update DocumentRecord Unarchiving Failed: [" + documentRecord.getId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public DocumentRecord checkAndUpdateDocumentRecordIfUploadComplete(DocumentRecord documentRecord)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("SELECT COUNT(*) FROM ArchiveMap AS obj WHERE obj.documentRecordId=? AND obj.status!=?");
            query.setString(0, documentRecord.getId());
            query.setInteger(1, ArchiveMap.UPLOADED_STATUS);

            if (((Long) query.uniqueResult()) == 0)
            {
                documentRecord.setCurrentArchiveStatus(DocumentRecord.ARCHIVED_ARCHIVESTATUS);
                savePlainObject(documentRecord, session);
            }
            else
            {
                documentRecord.setCurrentArchiveStatus(DocumentRecord.ARCHIVING_ERROR_ARCHIVESTATUS);
                savePlainObject(documentRecord, session);
            }

            return (DocumentRecord) savePlainObject(documentRecord, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update DocumentRecord Archiving Failed: [" + documentRecord.getId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public DocumentRecord checkAndUpdateDocumentRecordIfDownloadComplete(DocumentRecord documentRecord)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("SELECT COUNT(*) FROM ArchiveMap AS obj WHERE obj.documentRecordId=? AND obj.status!=?");
            query.setString(0, documentRecord.getId());
            query.setInteger(1, ArchiveMap.DOWNLOADED_STATUS);

            if (((Long) query.uniqueResult()) == 0)
            {
                documentRecord.setCurrentArchiveStatus(DocumentRecord.UNARCHIVED_ARCHIVESTATUS);
                savePlainObject(documentRecord, session);
            }
            else
            {
                documentRecord.setCurrentArchiveStatus(DocumentRecord.UNARCHIVING_ERROR_ARCHIVESTATUS);
                savePlainObject(documentRecord, session);
            }

            return (DocumentRecord) savePlainObject(documentRecord, session);
        } catch (ConnexienceException connexienceException) {
            throw connexienceException;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem update DocumentRecord Unarchiving Failed: [" + documentRecord.getId() + "]", exception);
        } finally {
            closeSession(session);
        }
    }

    public List<DocumentRecord> getOrphanUnarchivingDocumentRecord(String archiveStoreId)
        throws ConnexienceException
    {
        Session session = null;
        try {
            session = getSession();

            Query listQuery = session.createQuery("FROM ArchiveMap AS obj WHERE obj.archiveStoreId=? AND obj.status=?");
            listQuery.setString(0, archiveStoreId);
            listQuery.setInteger(1, ArchiveMap.DOWNLOADING_STATUS);

            Set<String> orphanUnarchivingDocumentRecordIds = new HashSet<>();
            for (ArchiveMap archiveMap: (List<ArchiveMap>) listQuery.list())
                orphanUnarchivingDocumentRecordIds.add(archiveMap.getDocumentRecordId());

            List<DocumentRecord> orphanUnarchivingDocumentRecords = new LinkedList<>();
            for (String orphanUnarchivingDocumentRecordId: orphanUnarchivingDocumentRecordIds)
            {
                Query objectQuery = session.createQuery("FROM DocumentRecord AS obj WHERE obj.id=?");

                orphanUnarchivingDocumentRecords.addAll((List<DocumentRecord>) objectQuery.list());
            }

            return orphanUnarchivingDocumentRecords;
        } catch (Exception exception) {
            throw new ConnexienceException("Problem getting Unarchiving Document Record", exception);
        } finally {
            closeSession(session);
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.social;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.social.Comment;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

//import com.connexience.server.ejb.social.util.*;

/**
 * This bean provides management of comments that can be attached to ServerObjects. It
 * contains code taken from the BlogBean and makes it applicable to any ServerObject.
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/CommentBean", beanInterface = CommentRemote.class)
public class CommentBean extends HibernateSessionContainer implements CommentRemote {

    @Override
    public Comment createComment(Ticket ticket, String objectId, String text) throws ConnexienceException {
        return createComment(ticket, objectId, text, null);
    }

    @Override
    public Comment createComment(Ticket ticket, String objectId, String text, String authorName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (ticket != null) {
                ServerObject obj = getObject(objectId, ServerObject.class);
                if (obj != null) {

                    session = getSession();
                    Comment c = new Comment();
                    c.setTimestamp(new Date());
                    c.setObjectId(objectId);
                    c.setText(text);

                    //if the author name is null, set it to the username from the ticket
                    if (authorName == null) {
                        authorName = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getName();
                    }
                    c.setAuthorName(authorName);
                    c.setContainerId(obj.getId());

                    c.setName("Comment");
                    c.setDescription("Comment on " + obj.getName());
                    c = (Comment) saveObject(ticket, c);

                    //EJBLocator.lookupNotificationsBean().sendNotification(new BlogPostCommentNotification(ticket, obj.getCreatorId(), c));
                    return c;
                } else {
                    throw new ConnexienceException("Cannot find object with id: " + objectId);
                }
            } else {
                throw new ConnexienceException("Must supply a valid ticket to create a comment");
            }
        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        } catch (Exception e) {
            throw new ConnexienceException("Error creating comment: ", e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void deleteComment(Ticket ticket, String commentId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (ticket != null) {
                Comment c = getComment( commentId, session);
                if (c != null) {
                    ServerObject obj = getObject(commentId, ServerObject.class);
                    if (obj != null) {
                        //only the comment poster or the blog owner can edit the comments
                        //in this case the comment poster has write access so let them delete
                        if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, c, Permission.OWNER_PERMISSION) || EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, obj, Permission.OWNER_PERMISSION)) {
                            EJBLocator.lookupObjectRemovalBean().remove(c);
                        } else {
                            throw new ConnexienceException("Error deleting comment: Not authorised to delete this comment");
                        }
                    } else {
                        //if object is null remove comment
                        EJBLocator.lookupObjectRemovalBean().remove(c);
                    }
                } else {
                    throw new ConnexienceException("Comment is null");
                }
            } else {
                throw new ConnexienceException("Must supply a valid ticket to delete a comment");
            }
        } finally {
            closeSession(session);
        }

    }

    @Override
    public Comment getComment(Ticket ticket, String commentId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Comment c = (Comment) getObject(commentId, Comment.class);
            if (c != null) {
                ServerObject obj = getObject(c.getObjectId(), ServerObject.class);
                if (obj != null && (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, c, Permission.READ_PERMISSION) || EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, obj, Permission.READ_PERMISSION))) {
                    return c;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (ConnexienceException ce) {
            ce.printStackTrace();
            throw ce;
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List getComments(Ticket ticket, String objectId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ServerObject obj = getObject(objectId, ServerObject.class);
            if (obj != null) {
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, obj, Permission.READ_PERMISSION)) {
                    return getComments( objectId, session);
                } else {
                    throw new ConnexienceException("Not authorised to view comments");
                }
            } else {
                throw new ConnexienceException("Object doesn't exist");
            }

        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        } catch (Exception e) {
            throw new ConnexienceException("Error listing comments: ", e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Long getNumberOfComments(Ticket ticket, String objectId) {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("SELECT count(*) FROM Comment as c WHERE c.containerId = ?");
            q.setString(0, objectId);
            Long numRead = (Long) q.list().iterator().next();

            if (numRead == null) {
                return 0L;
            } else {
                return numRead;
            }

        } catch (Exception ignored) {
            return 0L;
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Comment updateComment(Ticket ticket, String commentId, String text) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            Comment c = getComment( commentId, session);
            if (c != null) {

                ServerObject obj = getObject(c.getObjectId(), ServerObject.class);

                //only the comment poster or the blog owner can edit the comments
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, c, Permission.WRITE_PERMISSION) || obj.getCreatorId().equals(ticket.getUserId())) {
                    c.setText(text);
                    c = (Comment) saveObject(ticket, c);
                    return c;
                } else {
                    throw new ConnexienceException("Error updating comment: Not authorised to update that object");
                }
            } else {
                throw new ConnexienceException("Error updating comment: Comment does not exist");
            }
        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        } catch (Exception e) {
            throw new ConnexienceException("Error updating object: ", e);
        } finally {
            closeSession(session);
        }
    }
    
  private Comment getComment( String commentId, Session session) throws ConnexienceException
  {
    try
    {
      Query q = session.createQuery("from Comment as c where c.id = ?");
      q.setString(0, commentId);
      return (Comment) q.uniqueResult();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting comment: " + e.getMessage());
    }
  }    
  
  private List getComments(String objectId, Session session) throws ConnexienceException
  {
    try
    {
      Query q = session.createQuery("from Comment as c where c.objectId = ? order by c.timestamp asc");
      q.setString(0, objectId);
      return q.list();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing comments: " + e.getMessage());
    }
  }  
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}

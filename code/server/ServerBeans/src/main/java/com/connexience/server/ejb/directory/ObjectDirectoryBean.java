/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 AS published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This Interface can be used to query the system and retrieve sets of objects that satisfy certain parameters
 * For instance, all the objects a user has access to.  All the objects that are shared with a user etc.
 * <p/>
 * Author: Simon
 * Date: Jan 29, 2009
 */
@Stateless
@EJB(name = "java:global/ejb/ObjectDirectoryBean", beanInterface = ObjectDirectoryRemote.class)
public class ObjectDirectoryBean extends HibernateSessionContainer implements ObjectDirectoryRemote {

    /**
     * get the server object with a particular id
     */
    public ServerObject getServerObject(Ticket ticket, String id, Class type) throws ConnexienceException {
        try {
            //get the object but check the user is allowed to read it
            ServerObject obj = getObject(id, type);
            if (id != null) {
                assertPermission(ticket, obj, Permission.READ_PERMISSION);
                return obj;
            } else {
                return null;
            }
        } catch (ConnexienceException e) {
            if (e.getMessage().equals(ConnexienceException.ACCESS_DENIED_MESSAGE)) {
                throw e;
            } else {
                return null;
            }
        }
    }

    /**
     * Get all the server objects of a particular type that are owned by a user.
     */
    public List getOwnedObjects(Ticket ticket, String userId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "FROM " + type.getSimpleName() + " AS so WHERE so.creatorId = :userId  ORDER BY so.timeInMillis ASC";
            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }

            q.setString("userId", userId);
            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }


    /**
     * Get all the server objects of a particular type that are owned by a user.
     */
    public List getOwnedObjectsOrderByTimeDesc(Ticket ticket, String userId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "FROM " + type.getSimpleName() + " AS so WHERE so.creatorId = :userId ORDER BY so.timeInMillis DESC";
            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }

            q.setString("userId", userId);
            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }


    /** 
     * Get all objects owned by a project
    */
    public List getProjectObjects(Ticket ticket, Integer projectId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "FROM " + type.getSimpleName() + " AS so WHERE so.projectId = :projectId";
            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }

            q.setString("projectId", ticket.getDefaultProjectId());
            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }        
    }
    
    /** 
     * Get all the server objects of a particular type that are owned by a user.
     */
    public List getProjectObjectsOrderByTimeDesc(Ticket ticket, String projectId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "FROM " + type.getSimpleName() + " AS so WHERE so.projectId = :projectId ORDER BY so.timeInMillis DESC";
            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }

            q.setString("projectId", ticket.getDefaultProjectId());
            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }


    /**
     * Get the numeber of server objects of a particular type that are owned by a user.
     */
    public long getNumberOfOwnedObjects(Ticket ticket, String userId, Class type) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "SELECT count(id) FROM " + type.getSimpleName() +" AS so WHERE so.creatorId = :userId";
            Query q = session.createQuery(s);
            q.setString("userId", userId);

            return (Long) q.list().iterator().next();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }


    /**
     * Get the numeber of server objects of a particular type that are owned by a user.
     */
    public long getNumberOfProjectObjects(Ticket ticket, String projectId, Class type) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "SELECT count(id) FROM " + type.getSimpleName() + " AS so WHERE so.projectId = :projectId";
            Query q = session.createQuery(s);
            q.setString("projectId", ticket.getDefaultProjectId());

            return (Long) q.list().iterator().next();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the server objects that a user can see - owns or has access to.
     */
    public List getAllContainedObjectsUserHasAccessTo(Ticket ticket, String containerId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //Get a list of projects and add it to the query if there are any.  Must not be added if the set is empty
            // AS it will cause an exception in the qeury
            String projectsObjectQuery = "";
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, 0);

            if (projects.size() > 0) {
                projectsObjectQuery = " OR so.projectId IN :projectIds ";
            }

            String s;
            if (type != null) {
                s = "FROM "+ type.getSimpleName() + " AS so WHERE so.containerId = :containerId "
                        + "AND (so.creatorId = :userId "
                        + "OR (so.id IN (SELECT soc.id FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE (p.principalId = :userId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                        + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId ))"
                        + projectsObjectQuery
                        + " OR (so.containerId IN (SELECT soc.containerId FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :userId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId) "
                        + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId )"
//                        + projectsContainerQuery
                        + ")) ORDER BY so.timeInMillis ASC";

            } else {
                s = "FROM ServerObject AS so WHERE so.containerId = :containerId "
                        + "AND (so.creatorId = :userId "
                        + "OR (so.id IN (SELECT soc.id FROM ServerObject AS soc, Permission AS p WHERE (p.principalId = :userId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                        + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId ))"
                        + projectsObjectQuery
                        + " OR (so.containerId IN (SELECT soc.containerId FROM ServerObject AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :userId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId) "
                        + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId )"
//                        + projectsContainerQuery
                        + ")) ORDER BY so.timeInMillis ASC";
            }


            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("containerId", containerId);
            q.setString("userId", ticket.getUserId());
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));

            //If there are projects then set the parameter list with the list of projects
            if (projects.size() > 0) {
                List<String> projectIds = new ArrayList<>();
                for (Project project : projects) {
                    projectIds.add(String.valueOf(project.getId()));
                }
                q.setParameterList("projectIds", projectIds);
//                q.setParameterList("projectContainerIds", projectIds);
            }

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Gets the objects that a user has any access to. This method also includes an additional query fragment for extra filtering
     */
    public List getAllObjectUserHasAccessToWithAdditionalConditions(Ticket ticket, String userId, Class type, int start, int maxResults, String addtionalQueryFragment) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //Get a list of projects and add it to the query if there are any.  Must not be added if the set is empty
            // AS it will cause an exception in the qeury
            String projectsObjectQuery = "";
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, 0);

            if (projects.size() > 0) {
                projectsObjectQuery = " OR so.projectId IN :projectIds ";
            }

            String s = "FROM "+ type.getSimpleName() + " AS so WHERE "
                    + "(so.creatorId = :userId "
                    + "OR (so.id IN (SELECT soc.id FROM " + type.getSimpleName() + " AS soc, Permission AS p WHERE (p.principalId = :userId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                    + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId ))"
                    + projectsObjectQuery
                    + " OR (so.containerId IN (SELECT soc.containerId FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :userId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId) "
                    + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId )"
                    + ")) AND " + addtionalQueryFragment + "  ORDER BY so.timeInMillis ASC";

            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("userId", userId);
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));

            //If there are projects then set the parameter list with the list of projects
            if (projects.size() > 0) {
                List<String> projectIds = new ArrayList<>();
                for (Project project : projects) {
                    projectIds.add(String.valueOf(project.getId()));
                }
                q.setParameterList("projectIds", projectIds);
            }

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }         
    }
    
    /**
     * Get the server objects that a user can see - owns or has access to.
     */
    public List getAllObjectsUserHasAccessTo(Ticket ticket, String userId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //Get a list of projects and add it to the query if there are any.  Must not be added if the set is empty
            // AS it will cause an exception in the qeury
            String projectsObjectQuery = "";
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, 0);

            if (projects.size() > 0) {
                projectsObjectQuery = " OR so.projectId IN :projectIds ";
            }

            String s = "FROM "+ type.getSimpleName() + " AS so WHERE "
                    + "(so.creatorId = :userId "
                    + "OR (so.id IN (SELECT soc.id FROM " + type.getSimpleName() + " AS soc, Permission AS p WHERE (p.principalId = :userId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                    + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId ))"
                    + projectsObjectQuery
                    + " OR (so.containerId IN (SELECT soc.containerId FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :userId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId) "
                    + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId )"
                    + ")) ORDER BY so.timeInMillis ASC";

            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("userId", userId);
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));

            //If there are projects then set the parameter list with the list of projects
            if (projects.size() > 0) {
                List<String> projectIds = new ArrayList<>();
                for (Project project : projects) {
                    projectIds.add(String.valueOf(project.getId()));
                }
                q.setParameterList("projectIds", projectIds);
            }

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the server objects that a user can see - owns or has a type of access to.
     */
    public List getAllObjectsUserHasAccessTo(Ticket ticket, String userId, Class type, String accessType, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //Get a list of projects and add it to the query if there are any.  Must not be added if the set is empty
            // AS it will cause an exception in the qeury
            String projectsObjectQuery = "";
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, 0);

            if (projects.size() > 0) {
                projectsObjectQuery = " OR so.projectId IN :projectIds ";
            }

            String s = "FROM " + type.getSimpleName() + " AS so WHERE "
                    + " (so.creatorId = :userId "
                    + " OR (so.id IN (SELECT soc.id FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE (p.principalId = :userId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId AND p.type = :accessType)"
                    + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId AND perm.type = :accessType)) "
                    + projectsObjectQuery
                    + " OR (so.containerId IN (SELECT soc.containerId FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :userId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId) "
                    + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :userId AND gm.groupId = perm.principalId )"
                    + ")) ORDER BY so.timeInMillis ASC";
            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("accessType", accessType);
            q.setString("userId", userId);
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));

            //If there are projects then set the parameter list with the list of projects
            if (projects.size() > 0) {
                List<String> projectIds = new ArrayList<>();
                for (Project project : projects) {
                    projectIds.add(String.valueOf(project.getId()));
                }
                q.setParameterList("projectIds", projectIds);
            }

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the number of server objects that a user can see - owns or has access to.
     */
    public long getNumberOfObjectsUserHasAccessTo(Ticket ticket, String userId, Class type) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            //Cannot use a COUNT(*) here AS there is a bug in hibernate when using <joined-subclass>.  Is in theory fixed in 3.2.1 but still doesn't work
            //http://opensource.atlassian.com/projects/hibernate/browse/HHH-1631?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel
            Collection c = getAllObjectsUserHasAccessTo(ticket, userId, type, 0, 0);
            return c.size();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the number of server objects that a user can see - owns or has a type of access to.
     */
    public long getNumberOfObjectsUserHasAccessTo(Ticket ticket, String userId, Class type, String accessType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            //Can't use COUNT(*) - see above.
            Collection c = getAllObjectsUserHasAccessTo(ticket, userId, type, accessType, 0, 0);
            return c.size();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /*
     * THIS METHOD WILL NOT DEAL WITH SUPPLYING SUPERCLASSES AS THE TYPE PARAMETER
     */
    @Override
    public long getNumberOfObjects(String type) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("SELECT COUNT(id) FROM objectsflat WHERE objecttype=?");
            s.setString(1, type);
            r = s.executeQuery();
            if (r.next()) {
                return r.getLong(1);
            } else {
                throw new Exception("No data");
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error counting object: " + e.getMessage(), e);
        } finally {
            try {
                r.close();
            } catch (Exception ignored) {
            }
            try {
                s.close();
            } catch (Exception ignored) {
            }
            try {
                c.close();
            } catch (Exception ignored) {
            }
        }


    }


    /**
     * Get the server objects that a user has had shared with them.
     */
    public List getSharedObjectsUserHasAccessTo(Ticket ticket, String userId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //Get a list of projects and add it to the query if there are any.  Must not be added if the set is empty
            // AS it will cause an exception in the qeury
            String projectsObjectQuery = "";
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, 0);

            if (projects.size() > 0) {
                projectsObjectQuery = " OR so.projectId IN :projectIds ";
            }

            String s = "FROM " + type.getSimpleName() + " AS so WHERE "
                    + " (so.creatorId = :theirId)  "
                    + "AND ( (so.id IN (SELECT soc.id FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                    + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId ))"
                    + projectsObjectQuery
                    + " OR (so.containerId IN (SELECT soc.containerId FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId)"
                    + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId )"
                    + ")) ORDER BY so.timeInMillis ASC";

            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("theirId", userId);
            q.setString("myId", ticket.getUserId());
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));

            //If there are projects then set the parameter list with the list of projects
            if (projects.size() > 0) {
                List<String> projectIds = new ArrayList<>();
                for (Project project : projects) {
                    projectIds.add(String.valueOf(project.getId()));
                }
                q.setParameterList("projectIds", projectIds);
            }

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the server objects that a user has explicitly shared with them.  Does not include objects whose container is shared or when shared with a project
     */
    public List getExplicitlySharedObjectsUserHasAccessTo(Ticket ticket, String userId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            String s = "FROM " + type.getSimpleName() + " AS so WHERE "
                    + " (so.creatorId = :theirId)  "
                    + "AND ( (so.id IN (SELECT soc.id FROM ServerObject AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                    + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId ))) ORDER BY so.timeInMillis ASC";

            Query q = session.createQuery(s);
            // q.setCacheable(true);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("theirId", userId);
            q.setString("myId", ticket.getUserId());
            //q.setString("publicUser", EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId());
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));
            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List getExplicitlySharedObjectsUserHasAccesTo(Ticket ticket, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "FROM " + type.getSimpleName() + " AS so WHERE "
                    + " ( (so.id IN (SELECT soc.id FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                    + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId ))) ORDER BY so.timeInMillis ASC";

            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("myId", ticket.getUserId());
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));
            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }


    /**
     * Get the objects that a user has access to whose container has a certain id.
     */
    public List getContainedObjectsUserHasAccessTo(Ticket ticket, String containerId, Class type, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //Get a list of projects and add it to the query if there are any.  Must not be added if the set is empty
            // AS it will cause an exception in the qeury
            String projectsObjectQuery = "";
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, 0);

            if (projects.size() > 0) {
                projectsObjectQuery = " OR so.projectId IN :projectIds ";
            }


            String s;
            if (type != null) {
                s = "FROM " + type.getSimpleName() + " AS so WHERE "
                        + " (so.containerId = :containerId)  "
                        + "AND ( (so.id IN (SELECT soc.id FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                        + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId ))"
                        + projectsObjectQuery
                        + "OR (so.containerId IN (SELECT soc.containerId FROM "+ type.getSimpleName() +" AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId)"
                        + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId )"
                        + ")) ORDER BY so.timeInMillis ASC";

            } else {
                s = "FROM ServerObject AS so WHERE (so.containerId = :containerId) "
                        + "AND ( (so.id IN (SELECT soc.id FROM ServerObject AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.id = p.targetObjectId)"
                        + " OR so.id in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId ))"
                        + projectsObjectQuery
                        + "OR (so.containerId IN (SELECT soc.containerId FROM ServerObject AS soc, Permission AS p WHERE p.type='read' AND (p.principalId = :myId OR p.principalId = :publicUser) AND soc.containerId = p.targetObjectId)"
                        + " OR so.containerId in (SELECT perm.targetObjectId FROM Permission AS perm, GroupMembership AS gm WHERE gm.userId = :myId AND gm.groupId = perm.principalId )"
                        + ")) ORDER BY so.timeInMillis ASC";

            }

            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }
            q.setString("containerId", containerId);
            q.setString("myId", ticket.getUserId());
            q.setString("publicUser", getPublicUserId(ticket.getOrganisationId()));

            //If there are projects then set the parameter list with the list of projects
            if (projects.size() > 0) {
                List<String> projectIds = new ArrayList<>();
                for (Project project : projects) {
                    projectIds.add(String.valueOf(project.getId()));
                }
                q.setParameterList("projectIds", projectIds);
            }

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting social objects: " + e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get an image for a serverobject that has a particular type
     */
    public ImageData getImageForServerObject(Ticket ticket, String serverObjectId, String type) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "FROM ImageData AS i WHERE i.serverObjectId = :soid ";
            if (type != null) {
                s += " AND i.type = :type";
            }

            Query q = session.createQuery(s);
            // q.setCacheable(true);

            q.setString("soid", serverObjectId);
            if (type != null) {
                q.setString("type", type);
            }
            List results = q.list();
            if (results.size() > 0) {
                return (ImageData) results.iterator().next();
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("User:" + ticket.getUserId() + " Error getting image for soid:" + serverObjectId + " of type:" + type, e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get an image with a particular id
     */
    public ImageData getImageFromId(Ticket ticket, String imageId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            Query q = session.createQuery("FROM ImageData AS i WHERE i.id = :id ");
            q.setString("id", imageId);
            // q.setCacheable(true);

            List results = q.list();
            if (results.size() > 0) {
                return (ImageData) results.iterator().next();
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("User:" + ticket.getUserId() + " Error getting image with id:" + imageId, e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get an image associated with a server object.  Will return the first image found even if there are more than one
     */
    public ImageData getImageForServerObject(Ticket ticket, String serverObjectId) throws ConnexienceException {
        return getImageForServerObject(ticket, serverObjectId, null);
    }

    /**
     * Set an image for a server object
     */
    public ImageData setImageForServerObject(Ticket ticket, String serverObjectId, byte[] data, String type) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ImageData existingImage = getImageForServerObject(ticket, serverObjectId, type);
            if (existingImage != null) {
                existingImage.setData(data);
                session.saveOrUpdate(existingImage);
                return existingImage;
            } else {
                ImageData image = new ImageData();
                image.setServerObjectId(serverObjectId);
                image.setType(type);
                image.setData(data);
                session.persist(image);
                return image;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("User:" + ticket.getUserId() + " Error saving image for soid:" + serverObjectId + " of type:" + type, e);
        } finally {
            closeSession(session);
        }
    }

    public int changeObjectId(Ticket ticket, String originalId, String targetId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set id=:newid WHERE id=:originalid");
            q.setParameter("newid", targetId);
            q.setParameter("originalid", originalId);
            return q.executeUpdate();

        } catch (Exception e) {
            throw new ConnexienceException("Error changing object id: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }


    public long getNumberOfSharedFiles(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("select count(distinct(o.id)) FROM objectsflat AS o, permissions AS p " +
                    " WHERE " +
                    " o.id = p.targetobjectid and o.creatorid= :userId " +
                    " AND p.principalId <> :publicUserId " +
                    " and p.universal IS NOT true");
            q.setString("userId", userId);
            q.setString("publicUserId", getCachedOrganisation(ticket.getOrganisationId()).getDefaultUserId());
            return ((BigInteger) q.list().iterator().next()).longValue();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting number of objects owned" + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    public long getNumberOfPublicObjects(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("select count(distinct(o.id)) FROM objectsflat AS o, permissions AS p " +
                    " WHERE o.id = p.targetobjectid and o.creatorid= :userId " +
                    " AND p.principalId = :publicUserId");
            q.setString("userId", userId);
            q.setString("publicUserId", getCachedOrganisation(ticket.getOrganisationId()).getDefaultUserId());

            return ((BigInteger) q.list().iterator().next()).longValue();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting number of objects owned" + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }


}

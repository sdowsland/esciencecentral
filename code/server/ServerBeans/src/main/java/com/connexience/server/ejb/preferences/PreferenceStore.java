/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.preferences;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.performance.client.PerformanceLoggerClient;
import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.server.ejb.preferences.defaults.AccessControlDefaults;
import com.connexience.server.ejb.preferences.defaults.StudyManagementDefaults;
import com.connexience.server.ejb.preferences.defaults.WebsiteDefaults;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.log4j.Logger;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;
/**
 * This class loads preferences using the preference manager
 * @author hugo
 */
@Startup
@Singleton
public class PreferenceStore {
    private static Logger logger = Logger.getLogger(PreferenceStore.class);
    private String macAddress = "";
    private boolean storageEnabled = false;

    private String macToString(byte[] mac)
    {
        if (mac == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
        }
        return sb.toString();
    }


    @PostConstruct
    public void init(){
        logger.info("Loading server preferences file");
        createDefaultProperties();
        if (!PreferenceManager.loadPropertiesFromHomeDir(".inkspot", "server.xml")) {
            createDefaultProperties();
            PreferenceManager.saveProperties();
        } else {
            PreferenceManager.saveProperties();
        }

        logger.info("Determining MAC address...");
        macAddress = null;
        // Try the usual way...
        try {
            InetAddress ip = InetAddress.getLocalHost();
            logger.info("Using IP address: " + ip.getHostAddress() + " to determine MAC");
            NetworkInterface intf = NetworkInterface.getByInetAddress(ip);
            if (intf != null) {
                byte[] mac = intf.getHardwareAddress();
                macAddress = macToString(mac);
            }
        } catch (UnknownHostException | SocketException x) {
            logger.warn("Error determining the MAC address", x);
        }

        // If the usual way fails, pick whatever MAC you can find...
        if (macAddress == null) {
            logger.warn("Cannot get the MAC address based on the local host name. Trying another way...");
            try {
                Enumeration<NetworkInterface> intfs = NetworkInterface.getNetworkInterfaces();
                while (intfs.hasMoreElements() && macAddress == null) {
                    NetworkInterface intf = intfs.nextElement();
                    try {
                        byte[] mac = intf.getHardwareAddress();
                        macAddress = macToString(mac);
                        logger.info("Using MAC of interface: " + intf);
                    } catch (SocketException x) {
                        logger.warn("Cannot access MAC address of interface: " + intf, x);
                    }
                }
            } catch (SocketException x) {
                logger.error("Cannot ", x);
            }
        }

        if (macAddress != null) {
            logger.info("MAC: " + macAddress);
        } else {
            macAddress = "";
            logger.error("Unable to find any hardware interface with a MAC address. Check you network configuration -- storage will not work properly.");
        }
    }

    @PreDestroy
    public void terminate(){
        if(PreferenceManager.isLoadPerformed()){
            logger.info("Saving server preferences file");
            saveProperties();
        }
    }
    
    public String getMacAddress(){
        return macAddress;
    }
    
    public void saveProperties(){
        PreferenceManager.saveProperties();
    }
    
    public XmlDataStore getPropertyGroup(String groupName) {
        return PreferenceManager.getSystemPropertyGroup(groupName);
    }
    
    public List<String> listPropertyGroupNames(){
        return PreferenceManager.getSystemPropertyGroupNames();
    }
    
    public XmlDataStore getAllProperties() throws Exception {
        return PreferenceManager.getAllProperties();
    }

    public void setStorageEnabled(boolean storageEnabled) {
        this.storageEnabled = storageEnabled;
    }

    public boolean isStorageEnabled() {
        return storageEnabled;
    }
    
    private void createDefaultProperties(){
        // Scheduler properties
        SchedulerBean.createDefaultProperties();
        
        // Performance logging
        PerformanceLoggerClient.createDefaultProperties();
        
        // Provenance capture
        ProvenanceLoggerClient.createDefaultProperties();
        
        // Access control defaults
        AccessControlDefaults.createDefaultProperties();
        
        // Study management
        StudyManagementDefaults.createDefaultProperties();
        
        // Website
        WebsiteDefaults.createDefaultProperties();
    }
}

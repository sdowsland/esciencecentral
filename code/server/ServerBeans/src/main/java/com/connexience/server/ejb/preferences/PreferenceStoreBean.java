/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.preferences;

import com.connexience.server.ConnexienceException;
import org.jboss.logging.Logger;
import org.pipeline.core.xmlstorage.XmlDataStore;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;
import java.util.Date;
import java.util.List;
/**
 * This EJB provides access to the preference store
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/PreferenceStoreBean", beanInterface = PreferenceStoreRemote.class)
public class PreferenceStoreBean implements PreferenceStoreRemote {
    private static Logger logger = Logger.getLogger(PreferenceStoreBean.class);
    
    @EJB
    private PreferenceStore preferences;
    
    @Override
    public XmlDataStore getPropertyGroup(String groupName) {
        return preferences.getPropertyGroup(groupName);
    }

    @Override
    public void savePropertyGroup(String groupName, XmlDataStore store) {
        try {
            preferences.getPropertyGroup(groupName).copyProperties(store);
            preferences.saveProperties();
        } catch (Exception e){
            logger.error("Error setting property group: " + e.getMessage());
        }
    }

    @Override
    public List<String> listPropertyGroupNames() {
        return preferences.listPropertyGroupNames();
    }

    @Override
    public void add(String groupName, String propertyName, Date value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, File value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, String value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, XmlDataStore value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, boolean value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, double value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, int value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public void add(String groupName, String propertyName, long value) {
        preferences.getPropertyGroup(groupName).add(propertyName, value);
        preferences.saveProperties();
    }

    @Override
    public int intValue(String groupName, String propertyName, int defaultValue) {
        return preferences.getPropertyGroup(groupName).intValue(propertyName, defaultValue);
    }

    @Override
    public double doubleValue(String groupName, String propertyName, double defaultValue) {
        return preferences.getPropertyGroup(groupName).doubleValue(propertyName, defaultValue);
    }

    @Override
    public String stringValue(String groupName, String propertyName, String defaultValue) {
        return preferences.getPropertyGroup(groupName).stringValue(propertyName, defaultValue);
    }

    @Override
    public long longValue(String groupName, String propertyName, long defaultValue) {
        return preferences.getPropertyGroup(groupName).longValue(propertyName, defaultValue);
    }

    @Override
    public XmlDataStore xmlDataStoreValue(String groupName, String propertyName) throws ConnexienceException{
        try {
            return preferences.getPropertyGroup(groupName).xmlDataStoreValue(propertyName);
        } catch (Exception e){
            throw new ConnexienceException("Error getting xmlDataStoreValue: " + e.getMessage(), e);
        }
    }

    @Override
    public Date dateValue(String groupName, String propertyName, Date defaultValue) {
        return preferences.getPropertyGroup(groupName).dateValue(propertyName, defaultValue);
    }

    @Override
    public File fileValue(String groupName, String propertyName, File defaultValue) {
        return preferences.getPropertyGroup(groupName).fileValue(propertyName, defaultValue.getPath());
    }

    @Override
    public boolean groupHasProperty(String groupName, String propertyName) {
        return preferences.getPropertyGroup(groupName).containsName(propertyName);
    }

    @Override
    public boolean booleanValue(String groupName, String propertyName, boolean defaultValue) {
        return preferences.getPropertyGroup(groupName).booleanValue(propertyName, defaultValue);
    }

    @Override
    public String getMacAddress() {
        return preferences.getMacAddress();
    }

    @Override
    public void setStorageEnabled(boolean storageEnabled) {
        preferences.setStorageEnabled(storageEnabled);
    }

    @Override
    public boolean isStorageEnabled() {
        return preferences.isStorageEnabled();
    }

    @Override
    public XmlDataStore getAllProperties() throws ConnexienceException {
        try {
            return preferences.getAllProperties();
        } catch (Exception e){
            throw new ConnexienceException("Error getting all properties: " + e.getMessage(), e);
        }
    }
}
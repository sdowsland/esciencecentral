/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.preferences.defaults;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

/**
 * This class can create defaults for access control management
 * @author hugo
 */
public class AccessControlDefaults {
    public static void createDefaultProperties(){
        PreferenceManager.getSystemPropertyGroup("Security").add("EnforcePasswordExpiry", false);
        PreferenceManager.getSystemPropertyGroup("Security").add("KeepPasswordHistory", false);
        PreferenceManager.getSystemPropertyGroup("Security").add("PasswordHistoryLength", 5);
        PreferenceManager.getSystemPropertyGroup("Security").add("MaximumPasswordAge", 90);
        PreferenceManager.getSystemPropertyGroup("Security").add("LockoutAfterNFailedAttempts", false);
        PreferenceManager.getSystemPropertyGroup("Security").add("LockoutThreshold", 5);
        PreferenceManager.getSystemPropertyGroup("Security").add("LockInactiveAccounts", false);
        PreferenceManager.getSystemPropertyGroup("Security").add("InactivityThreshold", 30);
        PreferenceManager.getSystemPropertyGroup("Security").add("AllowUserRegistrations", true);
        PreferenceManager.getSystemPropertyGroup("Security").add("AllowUserPasswordReminders", true);
        PreferenceManager.getSystemPropertyGroup("Security").add("AllowRememberMe", true);
        PreferenceManager.getSystemPropertyGroup("Security").add("EnforceComplexityRules", false);
        PreferenceManager.getSystemPropertyGroup("Security").add("NumberOfComplexityCharacteristics", 0);
        PreferenceManager.getSystemPropertyGroup("Security").add("MinimumPasswordLength", 4);
        PreferenceManager.getSystemPropertyGroup("Security").add("MaximumPasswordLength", 20);
        PreferenceManager.getSystemPropertyGroup("Security").add("AutoUnlockFailedLogons", false);
        PreferenceManager.getSystemPropertyGroup("Security").add("AutoUnlockMinutes", 30);
        
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.util.StorageUtils;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * This task periodically scans the database for documents that do not contain
 * an MD5 hash and calculates them in batches.
 * @author hugo
 */
public class PopulateMD5s extends SchedulerTask {
    Logger logger = Logger.getLogger(PopulateMD5s.class);
    private Ticket adminTicket = null;
    
    public PopulateMD5s(SchedulerBean parentBean) {
        super(parentBean);
        setEnabled(true);
        setName("PopulateMD5s");
        setRepeating(false);
        setStartDelayed(false);
    }
    
    @Override
    public void run() {
        logger.info("Running PopulateMD5s task");
        if(parentBean.defaultOrganisationExists()){
            if(adminTicket==null){
                try {
                    adminTicket = parentBean.getDefaultOrganisationAdminTicket();
                } catch (Exception e){
                    logger.error("Error creating admin ticket: " + e.getMessage());
                }
            }     
            
            if(adminTicket!=null){
                Session session = null;
                try {
                    session = parentBean.getSession();
                    Query q = session.createQuery("from DocumentVersion as v where v.md5 is null");
                    List results = q.list();
                    parentBean.closeSession(session);
                    
                    DocumentVersion v;
                    DocumentRecord doc;
                    for(Object o : results){
                        v = (DocumentVersion)o;
                        if(v.getMd5()==null){
                            doc = null;
                            try {
                                doc = EJBLocator.lookupStorageBean().getDocumentRecord(adminTicket, v.getDocumentRecordId());
                            } catch (Exception e){
                                logger.error("Cannot obtain DocumentRecord for version: " + v.getId());
                            }
                            
                            try {
                                if(doc!=null){
                                    v = StorageUtils.populateWithMD5(adminTicket, doc, v);
                                    v = (DocumentVersion)parentBean.savePlainObject(v);
                                    logger.info("Calculated MD5 for: " + doc.getName() + ": V=" + v.getVersionNumber() + " MD5=" + v.getMd5());
                                } else {
                                    logger.error("Unconnected DocumentVersion: " + v.getDocumentRecordId());
                                    
                                    // Remove the document version
                                    try {
                                        doc = new DocumentRecord();
                                        doc.setOrganisationId(adminTicket.getOrganisationId());
                                        doc.setId(v.getDocumentRecordId());
                                        DataStore store = parentBean.getCachedDataStore(adminTicket.getOrganisationId());
                                        if(store!=null){
                                            try {
                                                store.removeRecord(doc, v);
                                            } catch (Exception e){
                                                logger.error("Error removing unconnected record from storage: " + e.getMessage());
                                            }
                                        }
                                        
                                        // Remove this object from the data store
                                        session = parentBean.getSession();
                                        session.delete(v);
                                        logger.info("Unconnected DocumentVersion removed from database");
                                    } catch (Exception e){
                                        logger.error("Exception cleaning up unconnected document: " + e.getMessage());
                                    } finally {
                                        parentBean.closeSession(session);
                                    }
                                }
                            } catch (Exception localException){

                                logger.error("Error calculating MD5 for document [DR:" + doc.getId() + "|DV:" + v.getId() + "]: " + localException.getMessage());
                            }
                        }
                    }

                } catch (Exception e){
                    logger.error("Error in main MD5 loop: " + e.getMessage());
                }
            } else {
                logger.error("No admin ticket available in PopulateMD5s task");
            }
        }
    }
}
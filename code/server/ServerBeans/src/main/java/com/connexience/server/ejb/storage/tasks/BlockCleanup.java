package com.connexience.server.ejb.storage.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.UncommittedVersion;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.storage.FileDataStore;
import com.connexience.server.util.StorageUtils;
import org.jboss.logging.Logger;

import java.util.Calendar;
import java.util.List;


public class BlockCleanup extends SchedulerTask
{
    private static final Logger _Logger = Logger.getLogger(BlockCleanup.class);
    private static final int CLEANUP_TIMEOUT_FIELD = Calendar.DAY_OF_YEAR;
    private static final int CLEANUP_TIMEOUT_VALUE = 5;


    private Ticket ticket;
    private FileDataStore fileStore;
    private boolean cleanLocalCache;

    public BlockCleanup(SchedulerBean parentBean)
    {
        super(parentBean);
        repeating = true;
        startDelayed = true;
        startDelay = 60 * 1000; // start after one minute
        repeatInterval = 24 * 3600 * 1000; // repeat every day
        //repeatInterval = 60 * 1000; // test
        name = "Scan for outdated and uncommited document versions to remove unnecessary blocks.";
    }


    @Override
    public synchronized void run() {
        if (!enabled) {
            parentBean.removeTask(this);
            return;
        }

        _Logger.info("Running BlockCleanup task");
        if(parentBean.defaultOrganisationExists()){
            try {
                if (ticket == null) {
                    ticket = parentBean.getDefaultOrganisationAdminTicket();
                    DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());
                    // If the store does not support chunked upload the local cache
                    // needs to be cleaned
                    cleanLocalCache = !store.isChunkedUploadSupported();
                    // FileDataStore is a special kind of a data store that needs additional
                    // care
                    if (store instanceof FileDataStore) {
                        fileStore = (FileDataStore)store;
                    }
                }

                Calendar c = Calendar.getInstance();
                c.add(CLEANUP_TIMEOUT_FIELD, -CLEANUP_TIMEOUT_VALUE);

                List<UncommittedVersion> migrations = EJBLocator.lookupStorageBean().listUncommittedVersions(ticket, c.getTime());
                for (UncommittedVersion version : migrations) {
                    _Logger.infof("Cleaning uncommitted version for document %s, version %s", version.getDocumentRecordId(), version.getId());
                    EJBLocator.lookupStorageBean().removeUncommittedVersion(ticket, version.getId());

                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, version.getDocumentRecordId());
                    if (cleanLocalCache) {
                        StorageUtils.cleanupVersionIfUncommitted(ticket, doc, version);
                    }
                    if (fileStore != null) {
                        fileStore.cleanupVersionIfUncommitted(doc, version);
                    }
                }
            } catch (Exception x) {
                _Logger.error("Error in the BlockCleanup task: ", x);
            }
        }
    }

}

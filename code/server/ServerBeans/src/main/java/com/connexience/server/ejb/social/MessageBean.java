/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.social;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.notifcations.TextMessageNotification;
import com.connexience.server.model.notifcations.messages.NotificationMessage;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.requests.Request;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;

/**
 * Author: Simon
 * Date: Jul 17, 2009
 */
@Stateless
@EJB(name = "java:global/ejb/MessageBean", beanInterface = MessageRemote.class)
public class MessageBean extends HibernateSessionContainer implements MessageRemote
{
  /**
   * Create a message from this user to a list of other users
   */
  public TextMessage createTextMessage(Ticket ticket, String thisRecipeint, String allRecipients, String threadId, String title, String text) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      User sender = (User) getObject(ticket.getUserId(), User.class);
      User receiver = (User) getObject(thisRecipeint, User.class);

      if (sender == null || receiver == null || receiver.getInboxFolderId() == null)
      {
        throw new ConnexienceException("Cannot send message from: " + ticket.getUserId() + " to " + thisRecipeint);
      }

      //create a new message
      TextMessage m = new TextMessage();
      m.setContainerId(receiver.getInboxFolderId());
      m.setCreatorId(receiver.getId());
      m.setDescription("Message from " + sender.getName() + " to " + receiver.getName());
      m.setMessage(text);
      m.setOrganisationId(ticket.getOrganisationId());
      m.setRecipientId(allRecipients.trim());
      m.setSenderId(sender.getId());
      m.setTitle(title);
      m.setRead(false);

      m = (TextMessage) saveObject(ticket, m);

      //if a threadId is passed in, set it
      if (threadId != null && !threadId.equals(""))
      {
        m.setThreadId(threadId);
      }
      else
      {
        m.setThreadId(m.getId());
      }

      EJBLocator.lookupNotificationsBean().sendNotification(new TextMessageNotification(ticket, thisRecipeint, m));
      return (TextMessage) saveObject(ticket, m);

    }
    catch (ConnexienceException ce)
    {
      throw ce;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error sending message: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Add a message to a users sent messages folder
   */
  public TextMessage addSentTextMessageToFolder(Ticket ticket, String receiverIds, String threadId, String title, String text) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      User sender = (User) getObject(ticket.getUserId(), User.class);

      if (sender == null)
      {
        throw new ConnexienceException("Cannot save sent message to user: " + ticket.getUserId());
      }

      //create a new message
      TextMessage m = new TextMessage();
      m.setContainerId(sender.getSentMessagesFolderId());
      m.setCreatorId(sender.getId());
      m.setDescription("Message from " + sender.getName() + " to " + receiverIds);
      m.setMessage(text);
      m.setOrganisationId(ticket.getOrganisationId());
      m.setRecipientId(receiverIds.trim());
      m.setSenderId(sender.getId());
      m.setTitle(title);
      m.setRead(true);

      m = (TextMessage) saveObject(ticket, m);

      //if a threadId is passed in, set it
      if (threadId != null)
      {
        m.setThreadId(threadId);
      }
      else
      {
        m.setThreadId(m.getId());
      }

      return (TextMessage) saveObject(ticket, m);

    }
    catch (ConnexienceException ce)
    {
      throw ce;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error creating message: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }


  /**
   * Get all the messages in this folder
   */
  public Collection<Message> getMessages(Ticket ticket, String userId, String folderId, int start, int maxResults) throws ConnexienceException
  {
    Session session = null;
    try
    {
      User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
      if (u == null || folderId == null)
      {
        throw new ConnexienceException("Invalid User: " + userId);
      }

      Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
      if (folder == null)
      {
        throw new ConnexienceException("Invalid folder for user: " + userId);
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, folder, Permission.READ_PERMISSION))
      {
        session = getSession();

        /*
        The following *could* be done in SQL but the query is complex and involves a number of joins.
        This method will be more efficient until the inbox gets too large.  The old query is below but may be
        optimisable by someone who's SQL is better.

        HQL:
        Query q = session.createQuery("from Message as m WHERE m.id IN (SELECT tm.id FROM TextMessage as tm WHERE tm.timestamp = (SELECT max(tm2.timestamp) FROM TextMessage as tm2 WHERE tm.threadId = tm2.threadId AND tm2.containerId = :folder))" +
            "or m.id IN(SELECT r.id FROM Request as r WHERE r.status = 0 AND r.containerId = :folder2)" + " or m.id IN(SELECT nm.id FROM NotificationMessage as nm WHERE nm.containerId = :folder2)" +
            " ORDER BY m.timestamp DESC");

        SQL:
        SELECT from objectsflat AS m where objecttype = 'TEXTMESSAGE' AND m.id IN(
	      SELECT tm.id FROM objectsflat as tm WHERE tm.objecttype = 'TEXTMESSAGE' AND tm.timestamp = (
	      SELECT max(tm2.timestamp) FROM objectsflat as tm2 WHERE tm2.objecttype = 'TEXTMESSAGE' AND tm2.threadid = tm.threadid AND tm2.containerid = '43e14ff522a2fa650122a2faf54a0002')) ORDER BY m.timestamp DESC;
         */

        //get all the messages that are in the user's inbox
        Query allMessagesQuery = session.createQuery("from Message as m where m.containerId = :folder ORDER BY m.timestamp DESC");
        @SuppressWarnings("unchecked")
        List<Message> allMessages = allMessagesQuery.setParameter("folder", folderId).list();

        //Add the most recent message in each thread to a list which will be returned
        List<Message> mostRecentMessages = new ArrayList<>();
        Set<String> idsAdded = new TreeSet<>();

        for(Message message: allMessages)
        {
          if(message instanceof TextMessage)
          {
            //index on threadId for Text Messages
            if(!idsAdded.contains(((TextMessage)message).getThreadId()))
            {
              mostRecentMessages.add(message);
              idsAdded.add(((TextMessage)message).getThreadId());
            }
          }
          else if(message instanceof Request || message instanceof NotificationMessage)
          {
            //index on id for requests and notifications as these aren't threaded
            if(!idsAdded.contains(message.getId()))
            {
              mostRecentMessages.add(message);
              idsAdded.add(message.getId());
            }
          }
        }

        //trim the list if the user asks for fewer messages
        if(maxResults <= 0)
        {
          maxResults = mostRecentMessages.size();
        }
        if(start <= 0)
        {
          start = 0;
        }

        List<Message> messagesToReturn = new ArrayList<>();
        for(int i = start; i < start + maxResults && i < mostRecentMessages.size(); i++)
        {
          messagesToReturn.add(mostRecentMessages.get(i));
        }
        
        return messagesToReturn;
      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, folder));
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing messages: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get the number of Messages the user has.  Threads will only be counted once
   * @param ticket Ticket for the user
   * @param userId user id of the user
   * @param folderId folder to use as the container
   * @return number of messages
   * @throws ConnexienceException something went wrong...
   */
  public int getNumberOfMessages(Ticket ticket, String userId, String folderId) throws ConnexienceException
  {
    return getMessages(ticket, userId, folderId, 0, 0).size();
  }

  /**
   * Get the TextMessages in this folder
   */
  public Collection<Message> getTextMessages(Ticket ticket, String userId, String folderId, int start, int maxResults) throws ConnexienceException
  {
    Session session = null;
    try
    {
      User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
      if (u == null || folderId == null)
      {
        throw new ConnexienceException("Invalid User: " + userId);
      }

      Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
      if (folder == null)
      {
        throw new ConnexienceException("Invalid folder for user: " + userId);
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, folder, Permission.READ_PERMISSION))
      {
        session = getSession();

        Query q = session.createQuery("from TextMessage as tm WHERE tm.timestamp = (SELECT max(tm2.timestamp) FROM TextMessage as tm2 WHERE tm.threadId = tm2.threadId AND tm2.containerId = :inbox) ORDER BY tm.timestamp DESC");
        q.setString("inbox", folderId);

        if (start > 0)
        {
          q.setFirstResult(start);
        }

        if (maxResults > 0)
        {
          q.setMaxResults(maxResults);
        }
        return q.list();
      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, folder));
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing messages: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }


  /**
   * Get the number of unread messages - text messages and requests
   */
  public Long getNumberOfUnreadMessages(Ticket ticket) throws ConnexienceException
  {
    Session session = null;
    try
    {
      Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getInboxFolderId());
      if (folder == null)
      {
        throw new ConnexienceException("Invalid Inbox for user: " + ticket.getUserId());
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, folder, Permission.READ_PERMISSION))
      {
        session = getSession();

        //add the text message threads and the requests to get the total
        Query q = session.createQuery("select count(DISTINCT tm.threadId) from TextMessage as tm WHERE tm.read = :read AND tm.containerId = :inboxId");
        q.setBoolean("read", false);
        q.setString("inboxId", folder.getId());
        Long tmCount = (Long) q.iterate().next();

        Query q2 = session.createQuery("SELECT count(*) FROM Request as r WHERE r.status = :status AND r.containerId = :inboxId");
        q2.setInteger("status", Request.REQUEST_PENDING);
        q2.setString("inboxId", folder.getId());
        Long reqCount = (Long) q2.iterate().next();

        Query q3 = session.createQuery("SELECT count(*) FROM NotificationMessage as nm WHERE nm.isRead = :read AND nm.containerId = :inboxId");
        q3.setString("inboxId", folder.getId());
        q3.setBoolean("read", false);
        Long nmCount = (Long) q3.iterate().next();
        return tmCount + reqCount + nmCount;

      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, folder));
      }
    }
    catch (Exception e)
    {
      //throw new ConnexienceException("Error getting message count: ", e);
      return 0L;
    }
    finally
    {
      closeSession(session);
    }
  }

  public Long getNumberOfTextMessgesInFolder(Ticket ticket, String folderId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
      if (folder == null)
      {
        throw new ConnexienceException("Invalid Inbox for user: " + ticket.getUserId());
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, folder, Permission.READ_PERMISSION))
      {
        session = getSession();
        Query q = session.createQuery("select count(DISTINCT tm.threadId) from TextMessage as tm WHERE tm.containerId = :folderId");
        q.setString("folderId", folderId);
        return (Long) q.iterate().next();
      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, folder));
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting message count: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }

  public Long getNumberOfUnreadTextMessgesInFolder(Ticket ticket, String folderId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
      if (folder == null)
      {
        throw new ConnexienceException("Invalid Inbox for user: " + ticket.getUserId());
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, folder, Permission.READ_PERMISSION))
      {
        session = getSession();
        Query q = session.createQuery("select count(DISTINCT tm.threadId) from TextMessage as tm WHERE tm.containerId = :folderId and tm.read = :read");
        q.setString("folderId", folderId);
        q.setBoolean("read", false);
        return (Long) q.iterate().next();
      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, folder));
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting message count: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }

  public Long getNumberOfMessagesInThread(Ticket ticket, String threadId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("SELECT count(*) from TextMessage as tm WHERE tm.threadId = :threadid AND tm.creatorId = :userId");
      q.setString("threadid", threadId);
      q.setString("userId", ticket.getUserId());
      return (Long) q.iterate().next();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting thread count: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get the messages in a thread
   */
  public List<Message> getMessageThread(Ticket ticket, String threadId, int start, int maxResults) throws ConnexienceException
  {
    Session session = null;
    try
    {
      User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
      if (u == null)
      {
        throw new ConnexienceException("Invalid User: " + ticket.getUserId());
      }

      Folder inbox = EJBLocator.lookupStorageBean().getFolder(ticket, u.getInboxFolderId());
      if (inbox == null)
      {
        throw new ConnexienceException("Invalid Inbox for user: " + ticket.getUserId());
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, inbox, Permission.READ_PERMISSION))
      {
        session = getSession();
        Query q = session.createQuery("from TextMessage as tm WHERE tm.threadId = :threadid AND tm.creatorId = :userId ORDER BY tm.timestamp ASC");
        q.setString("threadid", threadId);
        q.setString("userId", ticket.getUserId());

        if (start > 0)
        {
          q.setFirstResult(start);
        }

        if (maxResults > 0)
        {
          q.setMaxResults(maxResults);
        }

        return q.list();
      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, inbox));
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing messages: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }


  /**
   * Remove a message
   * */

  /**
   * Mark a message as read
   */
  public void markThreadAsRead(Ticket ticket, String threadId) throws ConnexienceException
  {
    Session session = null;
    Connection conn = null;
    try
    {
      User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
      if (u == null)
      {
        throw new ConnexienceException("Invalid User: " + ticket.getUserId());
      }

      Folder inbox = EJBLocator.lookupStorageBean().getFolder(ticket, u.getInboxFolderId());
      if (inbox == null)
      {
        throw new ConnexienceException("Invalid Inbox for user: " + ticket.getUserId());
      }

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, inbox, Permission.READ_PERMISSION))
      {
        String q = "UPDATE objectsflat SET read = true WHERE threadid = '" + threadId + "' AND creatorid='" + ticket.getUserId() + "'";
        conn = getSQLConnection();

        Statement st = conn.createStatement();
        st.executeUpdate(q);
        st.close();
      }
      else
      {
        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, inbox));
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error marking as read: ", e);
    }
    finally
    {
      closeSession(session);
      closeJDBConnection(conn);
    }
  }

  public void addNotificationMessageToInbox(Ticket ticket, String userId, Message message) throws ConnexienceException
  {
    User receiver = (User) getObject(userId, User.class);

    message.setContainerId(receiver.getInboxFolderId());
    message.setRecipientId(userId);
    message.setCreatorId(userId);
    saveObject(ticket, message);
  }

  public void markNotificationMessageRead(Ticket ticket, NotificationMessage nm) throws ConnexienceException
  {
    if (!ticket.isSuperTicket())
    {
      if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, nm, Permission.READ_PERMISSION))
      {
        throw new ConnexienceException("Can not access resource " + nm.getDescription() + " " + nm.getId());
      }
    }
    nm.setIsRead(true);
    saveObject(ticket, nm);
  }


}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.workflow;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.WorkflowLockOperation;
import com.connexience.provenance.model.logging.events.WorkflowUnlockOperation;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.model.workflow.notification.WorkflowLockMember;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

/**
 * This bean manages the subworkflow co-ordination lock tables.
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/WorkflowLockBean", beanInterface = WorkflowLockRemote.class)
public class WorkflowLockBean extends HibernateSessionContainer implements WorkflowLockRemote {
    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;


    @Inject
    @Named("WorkflowControlTopic")
    private String workflowControlTopicName;
    

    @Override
    public WorkflowLockMember attachInvocationToLock(Ticket ticket, long lockId, WorkflowInvocationFolder invocation) throws ConnexienceException {
        if(invocation.getCreatorId().equals(ticket.getUserId())){
            WorkflowLock lock = getLock(ticket, lockId);
            if (lock != null) {
                WorkflowLockMember member = new WorkflowLockMember();
                member.setEngineId(invocation.getEngineId());
                member.setInvocationId(invocation.getInvocationId());
                member.setInvocationStatus(invocation.getInvocationStatus());
                member.setLockId(lock.getId());
                member.setInvocationFolderId(lock.getInvocationFolderId());
                member.setStartTime(invocation.getInvocationDate().getTime());
                member = (WorkflowLockMember)savePlainObject(member);

                // Move the invocation folder
                invocation.setContainerId(member.getInvocationFolderId());
                WorkflowEJBLocator.lookupWorkflowManagementBean().saveInvocationFolder(ticket, invocation);

                // Update the lock
                touchLock(lockId);
                
                return member;
            } else {
                throw new ConnexienceException("No such lock");
            }
        } else {
            throw new ConnexienceException("Only invocation owners can attach invocations to a lock");
        }
    }

    @Override
    public WorkflowLock getLock(Ticket ticket, long lockId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowLock as lock where lock.id=?");
            q.setLong(0, lockId);
            List results = q.list();
            if(results.size()>0){
                WorkflowLock lock = (WorkflowLock)results.get(0);
                if(lock.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){
                    return lock;
                } else {
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
                
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting workflow lock: " +e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public WorkflowLock createWorkflowLock(Ticket ticket, WorkflowInvocationFolder parentWorkflow) throws ConnexienceException {
        if(parentWorkflow.getCreatorId().equals(ticket.getUserId())){
            WorkflowLock lock = new WorkflowLock();
            lock.setComments("Workflow lock");
            lock.setEngineId(parentWorkflow.getEngineId());
            lock.setInvocationId(parentWorkflow.getInvocationId());
            lock.setStatus(WorkflowLock.LOCK_FILLING);
            lock.setUserId(ticket.getUserId());
            lock.setInvocationFolderId(parentWorkflow.getId());
            lock.setWorkflowName(EJBLocator.lookupObjectInfoBean().getObjectName(ticket, parentWorkflow.getWorkflowId()));
            lock = (WorkflowLock)savePlainObject(lock);

            WorkflowLockOperation lockOperation = new WorkflowLockOperation();
            lockOperation.setLockId(String.valueOf(lock.getId()));
            lockOperation.setInvocationId(parentWorkflow.getInvocationId());
            lockOperation.setWorkflowId(parentWorkflow.getId());
            lockOperation.setVersionId(parentWorkflow.getVersionId());

            ProvenanceLoggerClient provenanceLoggerClient = new ProvenanceLoggerClient();
            provenanceLoggerClient.log(lockOperation);
            return lock;
        } else {
            throw new ConnexienceException("Only owners can create workflow locks");
        }
    }

    @Override
    public List getLockMembers(Ticket ticket, long lockId) throws ConnexienceException {
        WorkflowLock lock = getLock(ticket, lockId);
        if(lock!=null){
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from WorkflowLockMember as m where m.lockId=?");
                q.setLong(0, lockId);
                return q.list();
            } catch (Exception e){
                throw new ConnexienceException("Error listing lock members: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("No such lock");
        }
    }

    @Override
    public boolean isLockFinished(Ticket ticket, long lockId) throws ConnexienceException {
        WorkflowLock lock = getLock(ticket, lockId);
        if(lock!=null){
            Connection c = null;
            PreparedStatement s = null;
            ResultSet r = null;
            try {
                c = getSQLConnection();
                s = c.prepareStatement("SELECT COUNT(id) FROM workflowlockmembers WHERE lockid=? AND (invocationstatus=? OR invocationstatus=? OR invocationstatus=?)");
                s.setLong(1, lockId);
                s.setInt(2, WorkflowInvocationFolder.INVOCATION_RUNNING);
                s.setInt(3, WorkflowInvocationFolder.INVOCATION_WAITING);
                s.setInt(4, WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER);
                r = s.executeQuery();
                if(r.next()){
                    int remaining = r.getInt(1);
                    if(remaining==0){
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    throw new ConnexienceException("Error counting outstanding invocations");
                }
            } catch (Exception e){
                throw new ConnexienceException("Error checking if lock has finished: " + e.getMessage(), e);
            } finally {
                try {r.close();}catch(Exception e){}
                try {s.close();}catch(Exception e){}
                try {c.close();}catch(Exception e){}
            }
        } else {
            throw new ConnexienceException("No such lock");
        }
    }

    @Override
    public int getNumberOfRemainingInvocationsInLock(Ticket ticket, long lockId) throws ConnexienceException {
        WorkflowLock lock = getLock(ticket, lockId);
        if(lock!=null){
            Connection c = null;
            PreparedStatement s = null;
            ResultSet r = null;
            try {
                c = getSQLConnection();
                s = c.prepareStatement("SELECT COUNT(id) FROM workflowlockmembers WHERE lockid=? AND (invocationstatus=? OR invocationstatus=? OR invocationstatus=?)");
                s.setLong(1, lockId);
                s.setInt(2, WorkflowInvocationFolder.INVOCATION_RUNNING);
                s.setInt(3, WorkflowInvocationFolder.INVOCATION_WAITING);
                s.setInt(4, WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER);
                
                r = s.executeQuery();
                if(r.next()){
                    int remaining = r.getInt(1);
                    return remaining;
                } else {
                    throw new ConnexienceException("Error counting outstanding invocations");
                }
            } catch (Exception e){
                throw new ConnexienceException("Error checking if lock has finished: " + e.getMessage(), e);
            } finally {
                try {r.close();}catch(Exception e){}
                try {s.close();}catch(Exception e){}
                try {c.close();}catch(Exception e){}
            }
        } else {
            throw new ConnexienceException("No such lock");
        }
    }    
    
    @Override
    public int getNumberOfFailedInvocationsInLock(Ticket ticket, long lockId) throws ConnexienceException {
        WorkflowLock lock = getLock(ticket, lockId);
        if(lock!=null){
            Connection c = null;
            PreparedStatement s = null;
            ResultSet r = null;
            try {
                c = getSQLConnection();
                s = c.prepareStatement("SELECT COUNT(id) FROM workflowlockmembers WHERE lockid=? AND invocationstatus=?");
                s.setLong(1, lockId);
                s.setInt(2, WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS);
                r = s.executeQuery();
                if(r.next()){
                    return r.getInt(1);
                } else {
                    throw new ConnexienceException("Error counting invocation errors");
                }
            } catch (Exception e){
                throw new ConnexienceException("Error counting number of failed invocations in lock: " + e.getMessage(), e);
            } finally {
                try {r.close();}catch(Exception e){}
                try {s.close();}catch(Exception e){}
                try {c.close();}catch(Exception e){}
            }
        } else {
            throw new ConnexienceException("No such lock");
        }
    }

    @Override
    public void notifyAllFinishedLockHolders(Ticket ticket) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            List locks = listAllLocks(ticket);
            WorkflowLock lock;
            for(int i=0;i<locks.size();i++){
                lock = (WorkflowLock)locks.get(i);
                if(isLockFinished(ticket, lock.getId())){
                    notifyLockHolderOfCompletion(ticket, lock.getId());
                }
            }
        }
    }

    @Override
    public List listAllLocks(Ticket ticket) throws ConnexienceException {
        Session session = null;
        if(isOrganisationAdminTicket(ticket)){
            try {
                session = getSession();
                Query q = session.createQuery("from WorkflowLock");
                return q.list();
            } catch (Exception e){
                throw new ConnexienceException("Error listing all locks: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
           throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }
    }

    @Override
    public List listUserLocks(Ticket ticket, String userId) throws ConnexienceException {
        if(ticket.getUserId().equals(userId) || isOrganisationAdminTicket(ticket)){
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from WorkflowLock as lock where lock.userId=?");
                q.setString(0, userId);
                return q.list();
            } catch (Exception e){
                throw new ConnexienceException("Error listing user locks: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public List listInvocationLocks(Ticket ticket, String invocationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowLock as lock where lock.invocationId=?");
            q.setString(0, invocationId);
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing invocation locks: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    
    @Override
    public void notifyLockHolderOfCompletion(Ticket ticket, long lockId) throws ConnexienceException {
        WorkflowLock lock = getLock(ticket, lockId);
        if(lock!=null){
            // Send a JMS Message
            try {
                sendLockFinishedMessage(ticket, lock);

                WorkflowInvocationFolder folder = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, lock.getInvocationId());

                //send a provenance message to say the lock has been removed
                WorkflowUnlockOperation unlockOperation = new WorkflowUnlockOperation();
                unlockOperation.setLockId(String.valueOf(lockId));
                unlockOperation.setWorkflowId(folder.getWorkflowId());
                unlockOperation.setVersionId(folder.getVersionId());
                unlockOperation.setInvocationId(folder.getInvocationId());

                ProvenanceLoggerClient provenanceLoggerClient = new ProvenanceLoggerClient();
                provenanceLoggerClient.log(unlockOperation);

            } catch (Exception e){
                System.out.printf("Error sending JMS message to the %s topic: %s", workflowControlTopicName, e.getMessage());
            }
        } else {
            throw new ConnexienceException("No such lock");
        }
    }

    @Override
    public void removeWorkflowLock(Ticket ticket, long lockId) throws ConnexienceException {
        WorkflowLock lock = getLock(ticket, lockId);
        if(lock!=null){
            if(lock.getUserId().equals(ticket.getUserId()) || EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                

                List members = getLockMembers(ticket, lockId);
                Session session = null;
                try {
                    session = getSession();
                    for(Object o : members){
                        session.delete(o);
                    }
                    session.delete(lock);
                } catch (Exception e){
                    throw new ConnexienceException("Error deleting lock members: " + e.getMessage(), e);
                } finally {
                    closeSession(session);
                }
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } else {
            throw new ConnexienceException("No such lock");
        }
    }

    @Override
    public WorkflowLock saveWorkflowLock(Ticket ticket, WorkflowLock lock) throws ConnexienceException {
        WorkflowLock existing = getLock(ticket, lock.getId());
        if(existing!=null){
            existing.setComments(lock.getComments());
            existing.setEngineId(lock.getEngineId());
            existing.setInvocationId(lock.getInvocationId());
            existing.setStatus(lock.getStatus());
            existing.setUserId(lock.getUserId());
            existing.setContextId(lock.getContextId());
            existing.setLastChangeTime(new Date());
            existing.setAllowFailedSubworkflows(lock.isAllowFailedSubworkflows());
            existing.setPauseOnFailedSubworkflows(lock.isPauseOnFailedSubworkflows());
            return (WorkflowLock)savePlainObject(existing);
        } else {
            return (WorkflowLock)savePlainObject(lock);
        }
    }

    @Override
    public void updateLockMember(Ticket ticket, WorkflowInvocationFolder invocation) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowLockMember as m where m.invocationId=?");
            q.setString(0, invocation.getInvocationId());
            List results = q.list();
            if(results.size()>0){
                WorkflowLockMember member = (WorkflowLockMember)results.get(0);
                member.setEngineId(invocation.getEngineId());
                member.setInvocationId(invocation.getInvocationId());
                member.setInvocationStatus(invocation.getInvocationStatus());
                savePlainObject(member, session);
                session.flush();
                
                // Update the last access time of the lock
                touchLock(session, member.getLockId());
                
                // Should the lock holder be notified
                if(invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_FINISHED_OK || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS){                    
                    // Tell the lock holder that things have finished
                    notifyLockHolderIfComplete(ticket, member.getLockId(), false);
                }
            }
        } catch (Exception e){
            throw new ConnexienceException("Error updating lock member: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void notifyLockHolderIfComplete(Ticket ticket, long lockId, boolean forceDelivery) throws ConnexienceException {
        if(isLockFinished(ticket, lockId)){
            WorkflowLock lock = getLock(ticket, lockId);
            if(lock!=null){
                int failedInvocations = getNumberOfFailedInvocationsInLock(ticket, lockId);
                if(failedInvocations==0){
                    // Notify because no failures
                    notifyLockHolderOfCompletion(ticket, lockId);
                } else {
                    // Check to see if we are pausing on failures
                    if(forceDelivery==true){
                        notifyLockHolderOfCompletion(ticket, lockId);
                    } else {
                        // Only deliver if the pause flag is not set
                        if(lock.isPauseOnFailedSubworkflows()==false){
                            notifyLockHolderOfCompletion(ticket, lockId);
                        }
                    }
                }
                
            } else {
                throw new ConnexienceException("No such lock");
            }
        }
    }

    @Override
    public boolean isInvocationLockMember(Ticket ticket, String invocationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowLockMember as m where m.invocationId=:invocationid");
            q.setString("invocationid", invocationId);
            List results = q.list();
            if(results.size()>0){
                return true;
            } else {
                return false;
            }   
        } catch (Exception e){
            throw new ConnexienceException("Error in isInvocationLockMember: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    /**
     * Send the workflow invocation message to the apache message queue
     */
    private void sendLockFinishedMessage(Ticket ticket, WorkflowLock lock) throws Exception {
        // Get connection
        javax.jms.Connection connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
        try {
            javax.jms.Session session = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Topic topic = session.createTopic(workflowControlTopicName);

            MessageProducer publisher = session.createProducer(topic);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Create the message
            TextMessage tm = session.createTextMessage();
            tm.setText("LockCompleted");
            tm.setLongProperty("LockID", lock.getId());
            tm.setStringProperty("ContextID", lock.getContextId());
            tm.setStringProperty("EngineID", lock.getEngineId());
            tm.setStringProperty("InvocationID", lock.getInvocationId());
            int failures = getNumberOfFailedInvocationsInLock(ticket, lock.getId());
            tm.setIntProperty("Failures", failures);
            
            publisher.setTimeToLive((3600 * 1000));
            publisher.send(tm);
        } finally {
            connection.close();
        }
    }
    
    private void touchLock(long lockId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            touchLock(session, lockId);
        } catch (Exception e){
            throw new ConnexienceException("Error touching lock: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    /** Set the last change time of a lock */
    private void touchLock(Session session, long lockId) throws ConnexienceException {
        try {
            SQLQuery q = session.createSQLQuery("update workflowlocks set lastchangemillis=:lc where id=:lockid");
            q.setLong("lc", new Date().getTime());
            q.setLong("lockid", lockId);
            q.executeUpdate();
        } catch (Exception e){
            throw new ConnexienceException("Error touching lock: " + e.getMessage(), e);
        }
    }
}
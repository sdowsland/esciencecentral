/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;


import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * This class does the version management for DocumentRecords. It manages the
 * process of creating new versions and deleting versions if there are more than
 * the maximum allowed number.
 * @author nhgh
 */
public class DocumentVersionManager {
    /** DocumentRecord being processed */
    private DocumentRecord document;
    
    /** Creates a new instance of DocumentVersionManager */
    public DocumentVersionManager(DocumentRecord document) {
        this.document = document;
    }

    public DocumentVersionManager() {
    }

    public void setDocument(DocumentRecord document){
        this.document = document;
    }

    /** List all of the version ids for a document record and sort by version number. This
     * does a plain SQL query on the database */
    public List<String> getVersionIds() throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        
        try {
            c = getJdbcConnexienceDB().getConnection();  
            s = c.prepareStatement("SELECT id, versionnumber FROM documentversions WHERE documentrecordid=? ORDER BY versionnumber");
            s.setString(1, document.getId());
            r = s.executeQuery();
            ArrayList<String>ids = new ArrayList<>();
            while(r.next()){
                ids.add(r.getString(1));
            }
            return ids;
            
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        } finally {
            try {r.close();}catch(Exception ignored){}
            try {s.close();}catch(Exception ignored){}
            try {c.close();}catch(Exception ignored){}
        }
    }
    
    /** Get the DocumentVersions for the DocumentRecord */
    public List getVersions(Session session) throws ConnexienceException {
        try {
            Query q = session.createQuery("from DocumentVersion as dv where dv.documentRecordId=? order by dv.versionNumber");
            q.setString(0, document.getId());
          return q.list();
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        }
    }
    
    /** Get the current number of documents. This just does a plain SQL query
     * on the database and doesn't use hibernate. This means that the database
     * must support the count(...) sql function */
    public int getVersionCount() throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        
        try {
            c = getJdbcConnexienceDB().getConnection();  
            s = c.prepareStatement("SELECT COUNT(id) FROM documentversions WHERE documentrecordid=?");
            s.setString(1, document.getId());
            r = s.executeQuery();
            if(r.next()){
                return r.getInt(1);
            } else {
                return 0;
            }
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        } finally {
            try {r.close();}catch(Exception ignored){}
            try {s.close();}catch(Exception ignored){}
            try {c.close();}catch(Exception ignored){}
        }
    }
    
    /** Create a new DocumentVersion object for the DocumentRecord. This will
     * not have signatures etc in place because they need to be added during the
     * upload process. This method checks to see if the DocumentRecord supports
     * versioning. If it doesn't it returns the single DocumentVersion object 
     * for that document. If it does, it returns a new DocumentVersion with an 
     * incremented version number. If the DocumentRecord restricts the maximum
     * number of allowed versions, the excess versions are removed */
    public DocumentVersion createVersion(Session session) throws ConnexienceException {
        DocumentVersion version = null;
        
        try {
            if(document.isVersioned()){               
                // Versioned document - get all of the version ids. If the number
                // of allowed versions is limited, check to see if documents need to
                // be deleted. Otherwise increment the versions and pass back the 
                // DocumentVersion object
                if(document.isLimitVersions()){                    
                    // Limited version count
                    int versionCount = getVersionCount();
                    if(versionCount>=document.getMaxVersions()){
                        // Need to trim versions - get all the versions and build
                        // a list of versions to remove
                        List versions = getVersions(session);
                        List versionsToDelete = new ArrayList();
                        
                        // The lower version numbers are at the top of
                        // the list
                        while(versions.size()>=document.getMaxVersions()){
                            versionsToDelete.add(versions.remove(0));
                        }
                        /*
                        int versionsToRemove = versions.size() - document.getMaxVersions();
                        for(int i=0;i<versionsToRemove;i++){
                            versionsToDelete.add(versions.get(i));
                        }
                        */
                        DocumentVersion deleteVersion;
                        
                        // Now remove the objects from the database
                        for (Object aVersionsToDelete : versionsToDelete)
                        {
                          deleteVersion = (DocumentVersion) aVersionsToDelete;
                          EJBLocator.lookupObjectRemovalBean().remove(deleteVersion);
                        }
                    }
                }
                
                // Create a new version - Increment the version number in the document
                document.setCurrentVersionNumber(document.getCurrentVersionNumber() + 1);
                session.update(document);      
                version = new DocumentVersion();
                version.setDocumentRecordId(document.getId());
                version.setTimestampDate(new java.util.Date());
                version.setVersionNumber(document.getCurrentVersionNumber());
                session.persist(version);
                session.flush();
                return version;
                
            } else {
                // Single version - if there is already a version, delete the signatures
                // and pass back the empty version
                version = getLatestVersion(session);
                if(version!=null){
                    // Version found - remove existing signatures
                    version.setTimestampDate(new java.util.Date());
                    version.setVersionNumber(document.getCurrentVersionNumber());
                    session.update(version);
                    session.flush();
                    return version;
                    
                } else {
                    // Nothing there yet
                    version = new DocumentVersion();
                    version.setDocumentRecordId(document.getId());
                    version.setTimestampDate(new java.util.Date());
                    version.setVersionNumber(document.getCurrentVersionNumber());
                    session.persist(version);
                    session.flush();
                    return version;
                }                
            }
            
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        }
    }
    
    /** Get the latest version of a document */
    public DocumentVersion getLatestVersion(Session session) throws ConnexienceException {
        try {
            Query q = session.createQuery("from DocumentVersion as dv where dv.documentRecordId=? order by dv.versionNumber desc");
            q.setString(0, document.getId());
            q.setMaxResults(1);
            return (DocumentVersion)q.uniqueResult();
        } catch (Exception e){
            return null;
        }
    }
    
    /** Get a specified DocumentVersion by ID */
    public DocumentVersion getVersion(String versionId, Session session) throws ConnexienceException {
        try {
            Query q = session.createQuery("from DocumentVersion as dv where dv.id=?");
            q.setString(0, versionId);
            List results = q.list();
            if(results.size()>0){
                DocumentVersion dv = (DocumentVersion)results.get(0);
                if(dv.getDocumentRecordId().equals(document.getId())){
                    return dv;
                } else {
                    throw new ConnexienceException("Incorrect version for document");
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }
    
    /** Save a version back in the database. This is done when the signatures have changed */
    public void updateVersion(DocumentVersion version, Session session)
    {
        session.saveOrUpdate(version);
    }
    
    /*
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        
        try {
            c = ConnexienceDB.getConnection();  
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage());
        } finally {
            try {r.close();}catch(Exception e){}
            try {s.close();}catch(Exception e){}
            try {c.close();}catch(Exception e){}            
        }
     */

    /** Get a connection data source */
    private DataSource getJdbcConnexienceDB() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:jboss/datasources/ConnexienceDB");
    }
}

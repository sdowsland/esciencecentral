/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.migration.DataStoreMigration;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Check all of the data migrations that may be outstanding
 * @author hugo
 */
public class CheckMigrations extends SchedulerTask {
    private static Logger logger = Logger.getLogger(CheckMigrations.class);
    
    public CheckMigrations(SchedulerBean parentBean){
        super(parentBean);
        repeating = true;
        startDelayed = true;
        startDelay = 25000;
        repeatInterval = 10000;
        name = "Scan for migration documents";
    }
    
    @Override
    public synchronized void run() {
        if(enabled && parentBean.defaultOrganisationExists()){
            try {
                Ticket ticket = parentBean.getDefaultOrganisationAdminTicket();
                List migrations = EJBLocator.lookupDataStoreMigrationBean().listMigrations(ticket);
                DataStoreMigration migration;
                for(Object o : migrations){
                    migration = (DataStoreMigration)o;
                    if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){
                        int count = EJBLocator.lookupDataStoreMigrationBean().addNewMigrationDocuments(ticket, migration.getId());
                        if(count>0){
                            logger.info("Adding: " + count + " new documents for migration: " + migration.getId());
                        }
                    }
                }
            } catch (Exception e){
                logger.error("Error checking migrations: ", e);
            }
        }
    }
}
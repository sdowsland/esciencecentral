/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerDeployment;
import com.connexience.server.model.project.study.Phase;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.project.study.Subject;
import com.connexience.server.model.project.study.SubjectGroup;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.workflow.types.WorkflowStudy;
import com.connexience.server.workflow.types.WorkflowStudyDeployment;
import com.connexience.server.workflow.types.WorkflowStudySubjectGroup;
import com.connexience.server.workflow.types.WorkflowStudyLogger;
import com.connexience.server.workflow.types.WorkflowStudyObject;
import com.connexience.server.workflow.types.WorkflowStudyPhase;
import com.connexience.server.workflow.types.WorkflowStudySubject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.Query;

/**
 * This bean provides external access to key study data
 * @author hugo
 */
@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/WorkflowStudyBean", beanInterface = WorkflowStudyRemote.class)
public class WorkflowStudyBean implements WorkflowStudyRemote {
    @Inject
    private EntityManager em;
    
    @EJB
    private StudyRemote studyBean;    
    
    @EJB
    private LoggersRemote loggersBean;
    
    @EJB
    private SubjectsRemote subjectBean;
    
    @EJB
    private ProjectsRemote projectBean;
    
    @Override
    public WorkflowStudy getStudy(Ticket ticket, int studyId) throws ConnexienceException {
        Study s = studyBean.getStudy(ticket, studyId);
        em.merge(s);
        WorkflowStudy ws = new WorkflowStudy();
        ws.setAdminGroupId(s.getAdminGroupId());
        ws.setDataFolderId(s.getDataFolderId());
        ws.setDescription(s.getDescription());
        ws.setEndDate(s.getEndDate());
        ws.setExternalId(s.getExternalId());
        ws.setId(s.getId());
        ws.setMembersGroupId(s.getMembersGroupId());
        ws.setOwnerId(s.getOwnerId());
        ws.setStartDate(s.getStartDate());
        ws.setStudyId(s.getId());
        ws.setWorkflowFolderId(s.getWorkflowFolderId());
        ws.copyInAddtionalProperties(s.getAdditionalProperties());
        ws.setName(s.getName());
        return ws;
    }

    @Override
    public List<WorkflowStudyPhase> listStudyPhases(Ticket ticket, int studyId) throws ConnexienceException {
        Study s = studyBean.getStudy(ticket, studyId);
        em.merge(s);
        List<Phase> phases = s.getPhases();
        WorkflowStudyPhase wsp;
        
        ArrayList<WorkflowStudyPhase> wsPhases = new ArrayList<>();
        for(Phase p : phases){
            wsp = createWorkflowStudyPhase(p, s);
            wsPhases.add(wsp);
        }
        return wsPhases;
    }

    @Override
    public List<WorkflowStudySubjectGroup> listPhaseGroups(Ticket ticket, int phaseId) throws ConnexienceException {
        Phase p = em.find(Phase.class, phaseId);
        if(p!=null){
            Study s = p.getStudy();
            if(projectBean.isProjectMember(ticket, s)) {
                ArrayList<WorkflowStudySubjectGroup> results = new ArrayList<>();

                List<SubjectGroup> groups = em.createNamedQuery("SubjectGroup.findTopLevelGroups")
                        .setParameter("phaseId", phaseId)
                        .getResultList();

                for(SubjectGroup g : groups){
                    results.add(createWorkflowStudySubjectGroup(g));
                }
                return results;
            } else {
                throw new ConnexienceException("User is not a study member");
            }
        } else {
            throw new ConnexienceException("No such phase: " + phaseId);
        }
    }

    @Override
    public List<WorkflowStudySubjectGroup> listChildGroups(Ticket ticket, int parentGroupId) throws ConnexienceException {
        SubjectGroup sg = em.find(SubjectGroup.class, parentGroupId);
        if(sg!=null){
            Study s = sg.getPhase().getStudy();
            if(projectBean.isProjectMember(ticket, s)){
                ArrayList<WorkflowStudySubjectGroup> results = new ArrayList<>();
                List<SubjectGroup> groups = sg.getChildren();
                for(SubjectGroup g : groups){
                    results.add(createWorkflowStudySubjectGroup(g));
                }
                return results;
            } else {
                throw new ConnexienceException("User is not a study member");
            }
        } else {
            throw new ConnexienceException("No such subject group: " + parentGroupId);
        }
    }

    @Override
    public List<WorkflowStudySubject> listGroupSubjects(Ticket ticket, int parentGroupId) throws ConnexienceException {
        SubjectGroup sg = em.find(SubjectGroup.class, parentGroupId);
        if(sg!=null){
            Study s = sg.getPhase().getStudy();
            if(projectBean.isProjectMember(ticket, s)){
                ArrayList<WorkflowStudySubject> results = new ArrayList<>();
                List<Subject> subjects = sg.getSubjects();
                for(Subject sub : subjects){
                    WorkflowStudySubject ws = new WorkflowStudySubject();
                    ws.setId(sub.getId());
                    ws.setParentId(sg.getId());
                    ws.setStudyId(s.getId());
                    ws.setExternalId(sub.getExternalId());
                    ws.copyInAddtionalProperties(sub.getAdditionalProperties());
                    results.add(ws);
                }
                return results;
            
            } else {
                throw new ConnexienceException("User is not a study member");
            }
        } else {
            throw new ConnexienceException("No such subject group: " + parentGroupId);
        }
    }

    @Override
    public List<WorkflowStudyDeployment> listGroupDeployments(Ticket ticket, int parentGroupId) throws ConnexienceException {
        SubjectGroup sg = em.find(SubjectGroup.class, parentGroupId);
        if(sg!=null){
            Study s = sg.getPhase().getStudy();
            if(projectBean.isProjectMember(ticket, s)){
                Collection<LoggerDeployment> deployments = loggersBean.getLoggerDeployments(ticket, parentGroupId);
                ArrayList<WorkflowStudyDeployment> results = new ArrayList<>();
                for(LoggerDeployment d : deployments){
                    WorkflowStudyDeployment wsd = new WorkflowStudyDeployment();
                    wsd.setDataFolderId(d.getDataFolderId());
                    wsd.setEndDate(d.getEndDate());
                    wsd.setId(d.getId());
                    wsd.setLoggerId(d.getLogger().getId());
                    wsd.setParentId(sg.getId());
                    wsd.setStartDate(d.getStartDate());
                    wsd.setStudyId(s.getId());
                    results.add(wsd);
                }
                
                return results;
            
            } else {
                throw new ConnexienceException("User is not a study member");
            }
        } else {
            throw new ConnexienceException("No such subject group: " + parentGroupId);
        }
    }

    @Override
    public WorkflowStudyLogger getLogger(Ticket ticket, int loggerId) throws ConnexienceException {
        Logger logger = em.find(Logger.class, loggerId);
        if(logger!=null){
            WorkflowStudyLogger wsl = new WorkflowStudyLogger();
            wsl.setId(logger.getId());
            wsl.setLocation(logger.getLocation());
            wsl.setName(logger.getLoggerType().getName());
            wsl.setManufacturer(logger.getLoggerType().getManufacturer());
            return wsl;
        } else {
            throw new ConnexienceException("No such logger: " + loggerId);
        }
    }
    
    @Override
    public WorkflowStudyObject getStudyObjectAssociatedWithFolder(Ticket ticket, String folderId) throws ConnexienceException {
        // Search subject groups first
        Query subjectQuery = em.createNamedQuery("SubjectGroup.findByFolderId");
        subjectQuery.setParameter("folderId", folderId);
        List<SubjectGroup> groups = subjectQuery.getResultList();
        
        if(groups.size()>0){
            SubjectGroup g = groups.get(0);
            em.merge(g);
            Study s = g.getPhase().getStudy();
            if(EJBLocator.lookupProjectsBean().isProjectMember(ticket, s)){
                return createWorkflowStudySubjectGroup(groups.get(0));
            } else {
                throw new ConnexienceException("User is not a study member");
            }
            
        } else {
            // If no results, search the phases
            Query phaseQuery = em.createNamedQuery("Phase.findFromFolderId");
            phaseQuery.setParameter("folderId", folderId);
            List<Phase> phases = phaseQuery.getResultList();
            if(phases.size()>0){
                Phase p = phases.get(0);
                em.merge(p);
                Study s = p.getStudy();
                if(EJBLocator.lookupProjectsBean().isProjectMember(ticket, s)){
                    return createWorkflowStudyPhase(p, s);
                } else {
                    throw new ConnexienceException("User is not a study member");
                }
            } else {
                return null;
            }            
        }
    }

    @Override
    public void updateStudyObjectProperties(Ticket ticket, WorkflowStudyObject object) throws ConnexienceException {
        if(object instanceof WorkflowStudySubject){
            WorkflowStudySubject wss = (WorkflowStudySubject)object;
            EJBLocator.lookupSubjectsBean().addPropertiesToSubject(ticket, wss.getId(), wss.getAdditionalProperties());
            
        } else if(object instanceof WorkflowStudySubjectGroup){
            WorkflowStudySubjectGroup wssg = (WorkflowStudySubjectGroup)object;
            EJBLocator.lookupSubjectsBean().addPropertiesToSubjectGroup(ticket, wssg.getId(), wssg.getAdditionalProperties());

        } else if(object instanceof WorkflowStudyDeployment){
            WorkflowStudyDeployment wsd = (WorkflowStudyDeployment)object;
            EJBLocator.lookupLoggersBean().updateLoggerDeploymentAdditionalProperties(ticket, wsd.getId(), wsd.getAdditionalProperties());

            throw new ConnexienceException("Unsupported study object type:");
        }
    }
    

    
    private WorkflowStudySubjectGroup createWorkflowStudySubjectGroup(SubjectGroup g){
        WorkflowStudySubjectGroup wsg = new WorkflowStudySubjectGroup();
        wsg.setId(g.getId());
        if(g.getParent()!=null){
            wsg.setParentId(g.getParent().getId());
        }
        wsg.setStudyId(g.getPhase().getStudy().getId());
        wsg.setCategoryName(g.getCategoryName());
        wsg.setDataFolderId(g.getDataFolderId());
        wsg.setDisplayName(g.getDisplayName());
        wsg.copyInAddtionalProperties(g.getAdditionalProperties());
        return wsg;
    }
    
    private WorkflowStudyPhase createWorkflowStudyPhase(Phase p, Study s){
        WorkflowStudyPhase wsp = new WorkflowStudyPhase();
        wsp.setDataFolderId(p.getDataFolderId());
        wsp.setId(p.getId());
        wsp.setName(p.getName());
        wsp.setParentId(s.getId());
        wsp.setStudyId(s.getId());        
        return wsp;
    }
}
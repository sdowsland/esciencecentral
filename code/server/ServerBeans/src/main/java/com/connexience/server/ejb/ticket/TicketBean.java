/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.ticket;

import com.connexience.server.ConnexienceException;
import com.connexience.server.crypto.ServerObjectKeyManager;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.project.ProjectsRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.RootSecurityObjectManager;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.security.*;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.*;

/**
 * This bean issues Ticket objects, associates them with a user, signs them and
 * stores them in the logontickets table using Hibernate. This bean manages tickets
 * using the following techniques.
 * <p/>
 * When a user tries to assign a ticket, he first creates an empty ticket
 * <p/>
 * This ticket is signed and saved in the database.
 * <p/>
 * The ticket is then associated with the user's containing organisation (i.e. the one
 * matching organisationId). If no such organisation exists, no association is created.
 * This association is saved in the database.
 * <p/>
 * The ticket is then associated with the user's default group (if that group is associated with
 * the default organisation). If not, go group association is created. This association is saved
 * in the database.
 * <p/>
 * A signed TicketData object is then returned to the client.
 * <p/>
 * When a user attempts to add another group to an existing ticket:
 * <p/>
 * The ticket is retrieved from the database
 * <p/>
 * The requested group is retrieved from the database
 * <p/>
 * If the user is a member of the requested group and the requested group is a member of the
 * organisation, an associated is created and saved in the database.
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/TicketBean", beanInterface = TicketRemote.class)
public class TicketBean extends HibernateSessionContainer implements TicketRemote {

	@EJB
	ProjectsRemote projectsBean;

    /**
     * Creates a new instance of TicketBean
     */
    public TicketBean() {
        super();
    }

    /**
     * Acquire a normal ticket
     */
    private Ticket createStandardTicket(User user) throws ConnexienceException {
        // Get the organisation that the user is a member of
        String organisationId = user.getOrganisationId();
        if (organisationId != null) {
            // Try and find the certificate data
            ServerObjectKeyManager keyManager = new ServerObjectKeyManager();
            KeyData key = keyManager.getObjectKeyData(organisationId);
            if (key != null) {
                Ticket ticket = new Ticket();
                ticket.setUserId(user.getId());
                ticket.setOrganisationId(user.getOrganisationId());
                ticket.setLastAccessTime(new Date());
                ticket.setSuperTicket(false);

                // Save the ticket in the database and get the ID
                ticket = (Ticket) savePlainObject(ticket);

                // Assign the default group to this ticket if there is one
                if (user.getDefaultGroupId() != null) {
                    TicketGroup group = new TicketGroup();
                    group.setTicketId(ticket.getId());
                    group.setGroupId(user.getDefaultGroupId());
                    savePlainObject(group);
                }

                return ticket;

            } else {
                throw new ConnexienceException("Organisation has no signing keys");
            }
        } else {
            throw new ConnexienceException("Cannot locate user's organisation");
        }
    }

    /**
     * Acquire a root ticket
     */
    private Ticket createRootTicket(User user) throws ConnexienceException {
        // Get the root security object
        RootSecurityObjectManager mgr = new RootSecurityObjectManager();
        RootSecurityObject rootObject = mgr.getRootSecurityObject();

        // Get the keys for the root security object
        ServerObjectKeyManager keyManager = new ServerObjectKeyManager();
        KeyData key = keyManager.getObjectKeyData(rootObject.getId());
        if (key != null) {
            Ticket ticket = new Ticket();
            ticket.setUserId(user.getId());
            ticket.setOrganisationId(Ticket.ROOT_ORGANISATION_ID);
            ticket.setLastAccessTime(new Date());
            ticket.setSuperTicket(true);

            // Save the ticket in the database and get the ID
            ticket = (Ticket) savePlainObject(ticket);
            return ticket;
        } else {
            throw new ConnexienceException("No keys available for root logon");
        }
    }

    /**
     * Create a ticket for a username. This is used by the web pages, as the user has already
     * logged on using their browser.
     */
    public WebTicket createWebTicket(String username) throws ConnexienceException {
        User user = EJBLocator.lookupUserDirectoryBean().getUserFromLogonName(username);

        // Is this the root user
        boolean root = new RootSecurityObjectManager().isRootUser(user);
        if (!root) {
            WebTicket ticket = new WebTicket();
            ticket.setId("");
            ticket.setUserId(user.getId());
            ticket.setOrganisationId(user.getOrganisationId());
            ticket.setLastAccessTime(new Date());
            ticket.setSuperTicket(false);
            ticket.setGroupIds(new String[]{user.getDefaultGroupId()});

            Folder defaultFolder = (Folder) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, user.getDefaultStorageFolderId(), Folder.class);
            if (defaultFolder != null) {
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, defaultFolder, Permission.READ_PERMISSION)) {
                    ticket.setDefaultStorageFolderId(user.getDefaultStorageFolderId());
                }
            }

            try {
                String defaultProjectId = user.getDefaultProjectId();
                if (defaultProjectId != null) {
                    Study study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.valueOf(user.getDefaultProjectId()));
                    if (projectsBean.isProjectMember(ticket, study)) {
                        ticket.setDefaultProjectId(defaultProjectId);
                        ticket.setDefaultStorageFolderId(study.getDataFolderId());
                    }
                    else{
                        throw new ConnexienceException("User " + user.getDisplayName() + " trying to set project " + study.getName() + " as default project but is not member");
                    }
                }
            } catch (ConnexienceException ignored) {
            }

            // TODO: ADD LOGGING - USER LOG IN VIA WEB

            return ticket;
        } else {
            throw new ConnexienceException("Cannot logon root user");
        }
    }

    /**
     * Create a ticket for a user given a database id. This is used by the rememberMe cookie
     */
    public WebTicket createWebTicketForDatabaseId(String id) throws ConnexienceException {
        User user = EJBLocator.lookupUserDirectoryBean().getUser(getInternalTicket(), id);

        // Is this a root user
        if (user != null) {
            boolean root = new RootSecurityObjectManager().isRootUser(user);
            if (!root) {
                WebTicket ticket = new WebTicket();
                ticket.setId("");
                ticket.setUserId(user.getId());
                ticket.setOrganisationId(user.getOrganisationId());
                ticket.setLastAccessTime(new Date());
                ticket.setSuperTicket(false);
                ticket.setGroupIds(new String[]{user.getDefaultGroupId()});

                Folder defaultFolder = (Folder) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, user.getDefaultStorageFolderId(), Folder.class);
                if (defaultFolder != null) {
                    if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, defaultFolder, Permission.READ_PERMISSION)) {
                        ticket.setDefaultStorageFolderId(user.getDefaultStorageFolderId());
                    }
                }

                try {
                    String defaultProjectId = user.getDefaultProjectId();
                    if (defaultProjectId != null) {
                        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.valueOf(user.getDefaultProjectId()));
                        if (projectsBean.isProjectMember(ticket, study)) {
                            ticket.setDefaultProjectId(defaultProjectId);
                            ticket.setDefaultStorageFolderId(study.getDataFolderId());
                        }
                        else{
                            throw new ConnexienceException("User " + user.getDisplayName() + " trying to set project " + study.getName() + " as default project but is not member");
                        }
                    }
                } catch (ConnexienceException ignored) {
                    //project no longer exists
                }

                // TODO: ADD LOGGING - USER LOGIN
                return ticket;
            } else {
                WebTicket ticket = new WebTicket();
                ticket.setId("");
                ticket.setUserId(new RootSecurityObjectManager().getRootSecurityObject().getRootUserId());
                ticket.setSuperTicket(true);
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);
                ticket.setOrganisationId(org.getId());
                return ticket;
            }

        } else {
            throw new ConnexienceException("No such user id");
        }
    }

    /**
     * Create a ticket for a username and password. This assigns the default group and organisation membership.
     */
    public Ticket acquireTicket(String username, String password) throws ConnexienceException {
        User user = EJBLocator.lookupUserDirectoryBean().authenticateUser(username, password);

        // Is this the root user
        boolean root = new RootSecurityObjectManager().isRootUser(user);
        if (!root) {
            return createStandardTicket(user);
        } else {
            return createRootTicket(user);
        }
    }

    /**
     * Create a ticket with specified groups
     */
    public Ticket acquireTicket(String username, String password, List groupIds) throws ConnexienceException {
        Ticket ticket = acquireTicket(username, password);
        Iterator it = groupIds.iterator();
        while (it.hasNext()) {
            acquireGroup(ticket, (String) it.next());
        }
        return ticket;
    }

    /**
     * Lookup a physical ticket object in the database
     */
    public Ticket getTicket(String ticketId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Ticket as t where t.id=?");
            q.setString(0, ticketId);
            List tickets = q.list();
            if (tickets.size() > 0) {
                return (Ticket) tickets.get(0);
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Cannot find ticket: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * List the principals that a ticket has registered for. The list returned contains
     * the User as the first entry and all of the registered groups as the remaining entries
     */
    public List listTicketPrincipals(String ticketId) throws ConnexienceException {
        Session session = null;
        try {
            Ticket ticket = getTicket(ticketId);

            session = getSession();

            // Get the user
            Query q = session.createQuery("from User as obj where obj.id=?");
            q.setString(0, ticket.getUserId());
            List users = q.list();

            // Get the groups
            q = session.createQuery("from TicketGroup as obj where obj.ticketId=?");
            q.setString(0, ticketId);
            List ticketGroups = q.list();

            // Create a query to get the actual groups
            Query groupQuery = session.createQuery("from Group as obj where obj.id=?");
            TicketGroup tg;

            for (int i = 0; i < ticketGroups.size(); i++) {
                tg = (TicketGroup) ticketGroups.get(i);
                groupQuery.setString(0, tg.getGroupId());
                users.add((Group) groupQuery.uniqueResult());
            }
            return users;

        } catch (Exception e) {
            throw new ConnexienceException("Error listing ticket principals: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * List the groups that a ticket is currently associated with
     */
    public List listTicketGroups(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from TicketGroup as tg where tg.ticketId=?");
            q.setString(0, ticket.getId());
            List ticketGroups = q.list();
            ArrayList groups = new ArrayList();

            Query groupQuery = session.createQuery("from Group as obj where obj.id=?");
            TicketGroup tg;
            Group group;
            List results;

            for (int i = 0; i < ticketGroups.size(); i++) {
                tg = (TicketGroup) ticketGroups.get(i);
                groupQuery.setString(0, tg.getGroupId());
                results = groupQuery.list();
                if (results.size() > 0) {
                    groups.add((Group) results.get(0));
                }
            }
            return groups;

        } catch (Exception e) {
            throw new ConnexienceException("Error listing ticket groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * List the IDs of all of the ticket principals
     */
    public String[] listTicketPrincipalIds(Ticket ticket) throws ConnexienceException {
        String[] groupIds = listTicketGroupIds(ticket);
        String[] principalIds = new String[groupIds.length + 1];
        principalIds[0] = ticket.getUserId();
        for (int i = 0; i < groupIds.length; i++) {
            principalIds[i + 1] = groupIds[i];
        }
        return principalIds;
    }

    /**
     * List the group ids associated with a ticket as a String[] array
     */
    public String[] listTicketGroupIds(Ticket ticket) throws ConnexienceException {
        if (ticket instanceof WebTicket) {

            Session session = null;
            try {
                //get the group memberships from the database
                //todo: this used to retrieve the group ids from the web ticket but changed so that joinGroup works.  Check that the groupIds need to be stored in the ticket
                session = getSession();
                Query q = session.createQuery("FROM GroupMembership as gm WHERE gm.userId = :userId");
                q.setString("userId", ticket.getUserId());
                Collection gms = q.list();
                String[] groupIds = new String[gms.size()];
                int i = 0;
                for (Object o : gms) {
                    groupIds[i] = ((GroupMembership) o).getGroupId();
                    i++;
                }
                return groupIds;
            } finally {
                closeSession(session);
            }
        } else {
            // Local ticket
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from TicketGroup as tg where tg.ticketId=?");
                q.setString(0, ticket.getId());
                List ticketGroups = q.list();
                String[] ids = new String[ticketGroups.size()];
                for (int i = 0; i < ticketGroups.size(); i++) {
                    ids[i] = ((TicketGroup) ticketGroups.get(i)).getGroupId();
                }
                return ids;

            } catch (Exception e) {
                throw new ConnexienceException(e.getMessage());
            } finally {
                closeSession(session);
            }
        }
    }

    /**
     * Does a ticket have a group associated with it
     */
    public boolean ticketHasGroup(Ticket ticket, String groupId) throws ConnexienceException {
        // Local ticket
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from TicketGroup as tg where tg.ticketId = ? and tg.groupId = ?");
            q.setString(0, ticket.getId());
            q.setString(1, groupId);
            List groups = q.list();
            if (groups.size() > 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error checking ticket group membership: " + e.getMessage());
        } finally {
            closeSession(session);
        }

    }

    /**
     * Add a group to a ticket
     */
    public void acquireGroup(Ticket ticketData, String groupId) throws ConnexienceException {
        try {
            Ticket ticket = getTicket(ticketData.getId());
            if (EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), groupId)) {
                if (!ticketHasGroup(ticket, groupId)) {
                    TicketGroup tg = new TicketGroup();
                    tg.setTicketId(ticketData.getId());
                    tg.setGroupId(groupId);
                    savePlainObject(tg);
                }

            } else {
                throw new Exception("User is not a group member");
            }

        } catch (Exception e) {
            throw new ConnexienceException("Cannot acquire group: " + e.getMessage());
        }
    }


    /**
     * Create a public ticket
     */
    public WebTicket createPublicWebTicket() throws ConnexienceException {
        Organisation defaultOrg = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(getInternalTicket());
        if (defaultOrg != null) {
            String userId = defaultOrg.getDefaultUserId();
            return createWebTicketForDatabaseId(userId);
        } else {
            return null;
        }
    }

    /**
     * Create a web ticket with a username and password
     */
    public WebTicket createWebTicket(String username, String password) throws ConnexienceException {
        User user = EJBLocator.lookupUserDirectoryBean().authenticateUser(username, password);
        // Is this a root user
        if (user != null) {
            boolean root = new RootSecurityObjectManager().isRootUser(user);
            if (!root) {
                WebTicket ticket = new WebTicket();
                ticket.setId("");
                ticket.setUserId(user.getId());
                ticket.setOrganisationId(user.getOrganisationId());
                ticket.setLastAccessTime(new Date());
                ticket.setSuperTicket(false);
                ticket.setGroupIds(new String[]{user.getDefaultGroupId()});
                Folder defaultFolder = (Folder) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, user.getDefaultStorageFolderId(), Folder.class);
                if (defaultFolder != null) {
                    if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, defaultFolder, Permission.READ_PERMISSION)) {
                        ticket.setDefaultStorageFolderId(user.getDefaultStorageFolderId());
                    }
                }

                try {
                    String defaultProjectId = user.getDefaultProjectId();
                    if (defaultProjectId != null) {
                        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.valueOf(user.getDefaultProjectId()));
                        if (projectsBean.isProjectMember(ticket, study)) {
                            ticket.setDefaultProjectId(defaultProjectId);
                            ticket.setDefaultStorageFolderId(study.getDataFolderId());
                        }
                        else{
                            throw new ConnexienceException("User " + user.getDisplayName() + " trying to set project " + study.getName() + " as default project but is not member");
                        }
                    }
                } catch (ConnexienceException ignored) {
                }
                // TODO: ADD LOGGING - USER LOGIN

                return ticket;
            } else {
                WebTicket ticket = new WebTicket();
                ticket.setId("");
                ticket.setUserId(new RootSecurityObjectManager().getRootSecurityObject().getRootUserId());
                ticket.setSuperTicket(true);
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);
                ticket.setOrganisationId(org.getId());
                return ticket;
            }

        } else {
            throw new ConnexienceException("No such user id");
        }
    }

    /**
     * Allow an admin user to change users.  Returns a ticket with the other users credentials.
     */
    public com.connexience.server.model.security.WebTicket switchUsers(Ticket ticket, String otherUserId) throws ConnexienceException {
        User admin = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
        User other = EJBLocator.lookupUserDirectoryBean().getUser(ticket, otherUserId);

        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);
        Collection adminUsers = EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, org.getAdminGroupId());
        if (adminUsers.contains(admin)) {
            boolean root = new RootSecurityObjectManager().isRootUser(admin);
            if (!root) {
                WebTicket otherTicket = new WebTicket();
                otherTicket.setId("");
                otherTicket.setUserId(other.getId());
                otherTicket.setOrganisationId(other.getOrganisationId());
                otherTicket.setLastAccessTime(new Date());
                otherTicket.setSuperTicket(false);
                otherTicket.setGroupIds(new String[]{other.getDefaultGroupId()});

                Folder defaultFolder = (Folder) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, other.getDefaultStorageFolderId(), Folder.class);
                if (defaultFolder != null) {
                    if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, defaultFolder, Permission.READ_PERMISSION)) {
                        otherTicket.setDefaultStorageFolderId(other.getDefaultStorageFolderId());
                    }
                }

                try {
                    String defaultProjectId = other.getDefaultProjectId();
                    if (defaultProjectId != null) {
                        Study study = EJBLocator.lookupStudyBean().getStudy(otherTicket, Integer.valueOf(other.getDefaultProjectId()));
                        if (projectsBean.isProjectMember(ticket, study)) {
                            otherTicket.setDefaultProjectId(defaultProjectId);
                            otherTicket.setDefaultStorageFolderId(study.getDataFolderId());
                        }
                        else{
                            throw new ConnexienceException("User " + other.getDisplayName() + " trying to set project " + study.getName() + " as default project but is not member");
                        }
                    }
                } catch (ConnexienceException ignored) {

                }
                return otherTicket;
            } else {
                throw new ConnexienceException("Root cannot log on to the web server");
            }
        } else {
            throw new ConnexienceException(ticket.getUserId() + " is not authorised to switch users");
        }
    }

    /**
     * Set a value that allows a user to login automatically
     */
    public String addRememberMe(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Date date = new Date();
            Calendar c = new GregorianCalendar();
            c.setTime(date);
            c.add(Calendar.YEAR, 1);

            RememberMeLogin r = new RememberMeLogin(ticket.getUserId(), UUID.randomUUID().toString(), c.getTime());
            savePlainObject(r);

            return r.getCookieId();
        } catch (Exception e) {
            throw new ConnexienceException("Error adding 'Remember Me' for user: " + ticket.getUserId(), e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Check whether a user supplied UUID is in the rememberMe logins
     */
    public RememberMeLogin checkRememberMe(String cookieId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            RememberMeLogin r = (RememberMeLogin) session.createQuery("FROM RememberMeLogin AS r WHERE r.cookieId = :cookieId").setString("cookieId", cookieId).uniqueResult();
            if (r != null) {
                //if the rememberMe has expired, delete it and return false
                if (r.getExpiryDate().before(new Date())) {
                    deleteRememberMe(getInternalTicket(), cookieId);
                    return null;
                } else {
                    return r;
                }
            } else {
                return null; //there is no RememberMe fo this UUID
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error checking 'Remember Me' for cookieId: " + cookieId, e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Delete a remembered login
     */
    public void deleteRememberMe(Ticket ticket, String cookieId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            RememberMeLogin r = null;
            //Super user can delete any UUIDs
            if (isSuperTicket(ticket)) {
                r = (RememberMeLogin) session.createQuery("FROM RememberMeLogin AS r WHERE r.cookieId = :cookieId").setString("cookieId", cookieId).uniqueResult();
            } else {
                r = (RememberMeLogin) session.createQuery("FROM RememberMeLogin AS r WHERE r.cookieId = :cookieId AND r.userId = :userId").setString("userId", ticket.getUserId()).setString("cookieId", cookieId).uniqueResult();
            }

            if (r != null) {
                session.delete(r);
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error deleting 'Remember Me' for user: " + ticket.getUserId() + " with cookieId: " + cookieId, e);
        } finally {
            closeSession(session);
        }
    }

    public ExternalLogonDetails addExternalLogon(String userId, String externalUserId) throws ConnexienceException {
        return addExternalLogon(userId, externalUserId, "");
    }

    public ExternalLogonDetails addExternalLogon(String userId, String externalUserId, String provider) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ExternalLogonDetails external = getExternalLogon(externalUserId);

            // Details not found
            if (external == null) {
                // add the new details
                external = new ExternalLogonDetails(userId, externalUserId);

                // set provider if specified
                if (!"".equals(provider))
                    external.setProvider(provider);

                external = (ExternalLogonDetails) savePlainObject(external);
                return external;
            } else {
                if (!external.getUserId().equals(userId) && external.getProvider().equals(provider)) {
                    throw new ConnexienceException(String.format("External logon for given external ID already defined for different user (provider='%s').", external.getProvider()));
                } else {
                    System.err.printf("Attempt to add duplicate external logon details for user='%s', extId='%s', provider='%s'%n", userId, externalUserId, provider);
                    return external;
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error adding external logon for user: " + userId + " with externalId: " + externalUserId, e);
        } finally {
            closeSession(session);
        }
    }

    public ExternalLogonDetails getExternalLogon(String externalUserId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return (ExternalLogonDetails) session.createQuery("FROM ExternalLogonDetails AS e WHERE e.externalUserId = :extId").setString("extId", externalUserId).uniqueResult();
        } catch (Exception e) {
            throw new ConnexienceException("Error getting external logon for externalId: " + externalUserId, e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public LogonDetails getLogonByLogonName(String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from LogonDetails as l where lower(l.logonName)=:logonname");
            q.setString("logonname", userId.toLowerCase());
            Object obj = q.uniqueResult();
            if (obj instanceof LogonDetails) {
                return (LogonDetails) obj;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error looking up logon details: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public LogonDetails getLogonByUserId(String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from LogonDetails as l where l.userId=:userid");
            q.setString("userid", userId.toLowerCase());
            Object obj = q.uniqueResult();
            if (obj instanceof LogonDetails) {
                return (LogonDetails) obj;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error looking up logon details: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    
    @Override
    public LogonDetails saveLogon(LogonDetails logon) throws ConnexienceException {
        return (LogonDetails)savePlainObject(logon);
    }
    
    
    public ExternalLogonDetails getExternalLogon(String externalUserId, String provider) throws ConnexienceException {
        // TODO: Refactor getExternalLogon(String eID) to call this method.

        Session session = null;

        try {
            session = getSession();
            return (ExternalLogonDetails) session.createQuery("FROM ExternalLogonDetails AS e WHERE e.externalUserId = :extId AND e.provider = :provider").setString("extId", externalUserId).setString("provider", provider).uniqueResult();
        } catch (Exception e) {
            throw new ConnexienceException(String.format("Error getting external logon for externalId='%s', provider='%s'.", externalUserId, provider), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void lockAccount(Ticket ticket, String userId, String reason) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            if(!ticket.getUserId().equals(userId)){
                Session session = null;
                try {
                    session = getSession();
                    Query q = session.createQuery("from LogonDetails as obj where obj.userId=:userid");
                    q.setString("userid", userId);
                    List results = q.list();
                    if(results.size()==1){
                        LogonDetails details = (LogonDetails)results.get(0);
                        details.setEnabled(false);
                        details.setLockTime(new Date());
                        if(reason.equals(LogonDetails.LOCKED_BY_ADMINISTRATOR) || reason.equals(LogonDetails.LOCKED_DUE_TO_FAILED_LOGONS) || reason.equals(LogonDetails.LOCKED_DUE_TO_INACTIVITY) || reason.equals(LogonDetails.LOCKED_DUE_TO_PASSWORD_EXPIRY)){
                            details.setLockReason(reason);
                        } else {
                            details.setLockReason(LogonDetails.LOCKED_BY_ADMINISTRATOR);
                        }
                            
                        savePlainObject(details, session);
                    }
                } catch (Exception e){
                    throw new ConnexienceException("Error locking account: " + e.getMessage(), e);
                } finally {
                    closeSession(session);
                }

            } else {
                throw new ConnexienceException("Cannot lock own account");
            }
            
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void unlockAccount(Ticket ticket, String userId) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            if(!ticket.getUserId().equals(userId)){
                Session session = null;
                try {
                    session = getSession();
                    Query q = session.createQuery("from LogonDetails as obj where obj.userId=:userid");
                    q.setString("userid", userId);
                    List results = q.list();
                    if(results.size()==1){
                        LogonDetails details = (LogonDetails)results.get(0);
                        details.setEnabled(true);
                        details.setLockTime(null);
                        details.setLastLogonAttempt(new Date());    // This resets the inactivity timer
                        if(details.getLockReason().equals(LogonDetails.LOCKED_DUE_TO_PASSWORD_EXPIRY)){
                            // Reset the password creation time so it doesn't get locked straight away
                            details.setCreationTime(new Date());
                        }
                        
                        details.setLockReason(LogonDetails.NOT_LOCKED);
                        
                        savePlainObject(details, session);
                    }
                } catch (Exception e){
                    throw new ConnexienceException("Error locking account: " + e.getMessage(), e);
                } finally {
                    closeSession(session);
                }

            } else {
                throw new ConnexienceException("Cannot unlock own account");
            }
            
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public String getHashedRootPassword() throws ConnexienceException {
        try {
            RootSecurityObjectManager mgr = new RootSecurityObjectManager();
            RootSecurityObject rootObject = mgr.getRootSecurityObject();
            LogonDetails details = EJBLocator.lookupTicketBean().getLogonByUserId(rootObject.getRootUserId());
            if(details!=null){
                return details.getHashedPassword();
            } else {
                throw new Exception("Cannot get root logon details object");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting hashed root password: " + e.getMessage());
        }
    }
}

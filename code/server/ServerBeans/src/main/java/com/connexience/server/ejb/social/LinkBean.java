/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.social;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Link;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

/**
 * Author: Simon
 * Date: 15-Jul-2008
 */
@Stateless
@EJB(name = "java:global/ejb/LinkBean", beanInterface = LinkRemote.class)
public class LinkBean extends HibernateSessionContainer implements LinkRemote
{

  /**
   * Create a link between two objects in the social networking system.
   * The caller must have write access to the source object and read access to the sink object
   * It is not possible to link the same object to itself
   */
  public Link createLink(Ticket ticket, ServerObject sourceObject, ServerObject sinkObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //Can't link an object to itself
      if (!sourceObject.getId().equals(sinkObject.getId()))
      {

        //treat profiles differently - don't check for access rights as they are public anyway
        if (sinkObject instanceof User)
        {
          Link l = getLink(ticket, sourceObject, sinkObject);
          if (l != null)
          {
            return l;
          }
          else
          {
            Link link = new Link();
            link.setSourceObjectId(sourceObject.getId());
            link.setSinkObjectId(sinkObject.getId());
            link.setName("Link");
            link.setDescription("Link from " + sourceObject.getName() + " to " + sinkObject.getName());
            link.setContainerId(EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getWebFolderId());

            return (Link) saveObject(ticket, link);
          }
        }

        //must own object to link from it
        //        if (AccessControlUtil.isAuthorised(ticket, sourceObject, SNPermission.WRITE, session) && AccessControlUtil.isAuthorised(ticket, sinkObject, SNPermission.READ, session))
        if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sourceObject, Permission.READ_PERMISSION) && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sinkObject, Permission.READ_PERMISSION))
        {
          Link l = getLink(ticket, sourceObject, sinkObject);
          if (l != null)
          {
            return l;
          }
          else
          {
            Link link = new Link();
            link.setSourceObjectId(sourceObject.getId());
            link.setSinkObjectId(sinkObject.getId());
            link.setName("Link");
            link.setDescription("Link from " + sourceObject.getName() + " to " + sinkObject.getName());

            return (Link) saveObject(ticket, link);
          }
        }
        else
        {
          throw new ConnexienceException("User " + ticket.getUserId() + " does not own have access to create link");
        }
      }
      else
      {
        throw new ConnexienceException("Cannot link the same object to itself");
      }
    }
    catch (ConnexienceException ce)
    {
      throw ce;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error creating link: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }


  /**
   * Get all of the objects that are linked to from the source object that we have permission to read.
   * Returns a collection of SocialObjects rather than ids
   */
  public Collection<ServerObject> getLinkedSourceObjects(Ticket ticket, ServerObject sourceObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("from ServerObject as so where so.id in " +
          "(select l.sinkObjectId from Link as l where l.sourceObjectId= :sourceObjectId)");
      q.setParameter("sourceObjectId", sourceObject.getId());

      //todo: could probably be changed to narrowListWithACL(...)
      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        ServerObject so = (ServerObject) iterator.next();
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return objects;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting social objects: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get all of the objects of a particular type that are linked to from the source object that we have permission to read.
   */
  public Collection getLinkedSourceObjects(Ticket ticket, ServerObject sourceObject, Class type) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //For some reason if you build the query using named (or numbered) parameters it doesn't work
      //This looks like a bug in hibernate?

      String s = "from ServerObject as so where so.class = " + type.getName() + " and so.id in " +
          "(select l.sinkObjectId from Link as l where l.sourceObjectId= :soid)";
      Query q = session.createQuery(s);
      q.setString("soid", sourceObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        ServerObject so = (ServerObject) iterator.next();
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return objects;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting social objects: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get all of the objects of a particular type that are linked to from the source object that we have permission to read.
   * //TODO: should move the pruning into the database
   */
  public Collection getLinkedSourceObjects(Ticket ticket, ServerObject sourceObject, Class type, int start, int maxResults) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //For some reason if you build the query using named (or numbered) parameters it doesn't work
      //This looks like a bug in hibernate?

      String s = "from ServerObject as so where so.class = " + type.getName() + " and so.id in " +
          "(select l.sinkObjectId from Link as l where l.sourceObjectId= :soid)";
      Query q = session.createQuery(s);
      q.setString("soid", sourceObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        ServerObject so = (ServerObject) iterator.next();
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }

      Object[] allObjects = objects.toArray();
      Collection prunedObjects = new Vector();
      for (int i = start; i < start + maxResults && i < allObjects.length; i++)
      {
        prunedObjects.add(allObjects[i]);
      }

      return prunedObjects;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error getting social objects: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get the number of objects of a particular type that are linked to from the source object that we have permission to read.
   */
  public Long getNumberOfLinkedSourceObjects(Ticket ticket, ServerObject sourceObject, Class type) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //For some reason if you build the query using named (or numbered) parameters it doesn't work
      //This looks like a bug in hibernate?

      String s = "FROM ServerObject as so where so.class = " + type.getName() + " and so.id in " +
          "(select l.sinkObjectId from Link as l where l.sourceObjectId= :soid)";
      Query q = session.createQuery(s);
      q.setString("soid", sourceObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        ServerObject so = (ServerObject) iterator.next();
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return (long) objects.size();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error getting social objects: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }


  /**
   * Get all of the objects that link to a particular object
   */
  public Collection getLinkedSinkObjects(Ticket ticket, ServerObject sinkObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("from ServerObject as so where so.id in " +
          "(select l.sourceObjectId from Link as l where l.sinkObjectId= :sinkObjectId)");
      q.setParameter("sinkObjectId", sinkObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        ServerObject so = (ServerObject) iterator.next();
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return objects;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting social objects: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }


  public Collection getLinkedSinkObjects(Ticket ticket, ServerObject sinkObject, Class type) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //For some reason if you build the query using named (or numbered) parameters it doesn't work
      //This looks like a bug in hibernate?

      String s = "from ServerObject as so where so.class = " + type.getName() + " and so.id in " +
          "(select l.sourceObjectId from Link as l where l.sinkObjectId= :sinkObjectId)";
      Query q = session.createQuery(s);
      q.setString("sinkObjectId", sinkObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        ServerObject so = (ServerObject) iterator.next();
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return objects;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting social objects: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get a collection of links that emanate from the source object.  Links contain the id of the sinkObject
   */
  public Collection getSourceLinks(Ticket ticket, ServerObject sourceObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("from Link as l where l.sourceObjectId=?");
      q.setString(0, sourceObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        Link l = (Link) iterator.next();
        ServerObject sinkObject = getObject(l.getSinkObjectId(), ServerObject.class);
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sinkObject, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return objects;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing links: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get a collection of links that link to the object
   */
  public Collection getSinkLinks(Ticket ticket, ServerObject sinkObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("from Link as l where l.sinkObjectId=?");
      q.setString(0, sinkObject.getId());

      Collection objects = q.list();
      for (Iterator iterator = objects.iterator(); iterator.hasNext();)
      {
        Link l = (Link) iterator.next();
        ServerObject sourceObject = getObject(l.getSourceObjectId(), ServerObject.class);
        if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sourceObject, Permission.READ_PERMISSION))
        {
          iterator.remove();
        }
      }
      return objects;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing links: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get a link, if one exists between the source and the sink object
   */
  public Link getLink(Ticket ticket, ServerObject sourceObject, ServerObject sinkObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sourceObject, Permission.READ_PERMISSION) && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sinkObject, Permission.READ_PERMISSION))
      {
        Query q = session.createQuery("from Link as l where l.sourceObjectId = :source and l.sinkObjectId = :sink");
        q.setString("source", sourceObject.getId());
        q.setString("sink", sinkObject.getId());
        return (Link) q.uniqueResult();
      }
      else
      {
        return null;
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting social objects: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get a Link from its id
   */

  public Link getLink(Ticket ticket, String id) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("from Link as l where l.id = :id");
      q.setString("id", id);
      Link l = (Link) q.uniqueResult();
      ServerObject source = getObject(l.getSourceObjectId(), ServerObject.class);
      ServerObject sink = getObject(l.getSinkObjectId(), ServerObject.class);

      if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, source, Permission.READ_PERMISSION) && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, sink, Permission.READ_PERMISSION))
      {
        return l;
      }
      else
      {
        return null;
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error getting link: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Remove a link.  To be able to remove it you must either own it, or own the sink object that it links to
   * Owning the link implies owning the source object
   */
  public void removeLink(Ticket ticket, String id) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Link link = getLink(ticket, id);
      if (link != null)
      {
        ServerObject sinkObject = getObject(link.getSinkObjectId(), ServerObject.class);
        ServerObject sourceObject = getObject(link.getSourceObjectId(), ServerObject.class);

        //if either of the linked objects are null remove the link
        if (sourceObject == null || sinkObject == null)
        {
          session.delete(link);
        }
        //if we are the owner of the link
        else if (link.getCreatorId().equals(ticket.getUserId()))
        {
          session.delete(link);
        }

        //if we are the owner of the source object
        else if (sourceObject.getCreatorId().equals(ticket.getUserId()))
        {
          session.delete(link);
        }

        //if we are the owner of the sink object
        else if (sinkObject.getCreatorId().equals(ticket.getUserId()))
        {
          session.delete(link);
        }

        //special case for friend links when the people were created by the desktop client
        else if(sinkObject instanceof User && sourceObject instanceof User)
        {
          if(sourceObject.getId().equals(ticket.getUserId()) || sinkObject.getId().equals(ticket.getId()))
          {
            session.delete(link);
          }
        }

        else
        {
          throw new ConnexienceException("Cannot remove link you don't own");
        }

        //if source and sink are users, log that they are no longer friends
        if(sinkObject instanceof User && sourceObject instanceof User)
        {
          //todo: log that they are no longer friends
        }
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error removing link: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Remove all links that an object is the source of.
   */
  public void removeSourceLinks(Ticket ticket, ServerObject sourceObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //only get the objects we are able to see
      Collection links = getSourceLinks(ticket, sourceObject);
      for (Object o : links)
      {
        Link link = (Link) o;
        removeLink(ticket, link.getId());
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error removing link: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Remove all links that an object is the sink of
   */
  public void removeSinkLinks(Ticket ticket, ServerObject sinkObject) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      //only get the links that we can see
      Collection links = getSinkLinks(ticket, sinkObject);
      for (Object o : links)
      {
        Link link = (Link) o;
        removeLink(ticket, link.getId());
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error removing link: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Remove all links referring to an object
   */
  public void removeLinks(Ticket ticket, ServerObject object) throws ConnexienceException
  {
    removeSinkLinks(ticket, object);
    removeSourceLinks(ticket, object);
  }

}

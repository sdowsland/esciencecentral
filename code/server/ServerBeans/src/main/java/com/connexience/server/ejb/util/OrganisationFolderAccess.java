/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.util;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides access to all of the folders within an organisation.
 *
 * @author hugo
 */
public class OrganisationFolderAccess extends HibernateSessionContainer {

    /**
     * Organisation to check
     */
    private Organisation organisation = null;
    /**
     * Organisation id
     */
    private String organisationId;

    /**
     * Creates a new instance of OrganisationFolderAccess
     */
    public OrganisationFolderAccess(Organisation organisation) {
        this.organisation = organisation;
    }

    /**
     * Check an organisation with an existing session
     */
    private void checkOrganisation(Session session) throws ConnexienceException {
        if (organisation == null) {
            Organisation org = (Organisation)getObject(organisationId, Organisation.class);
            if (org !=null) {
                organisation =  org;
            } else {
                throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
            }
        }
    }

    /**
     * Check that this access object has an organisation present. If not, load it
     */
    private void checkOrganisation() throws ConnexienceException {
        if (organisation == null) {
          Organisation org = (Organisation)getObject(organisationId, Organisation.class);
          if (org !=null) {
                organisation =  org;
            } else {
                throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
            }
        }
    }

    /**
     * Add a child folder to an existing folder. This method does ACL checking
     */
    public Folder addChildFolder(Ticket ticket, Folder parent, Folder child) throws ConnexienceException {
        if (!(parent instanceof UsersFolder) && !(parent instanceof GroupsFolder) && !(parent instanceof ProjectsFolder)) {
            child.setContainerId(parent.getId());
            child.setOrganisationId(parent.getOrganisationId());
            child.setProjectId(parent.getProjectId());
            return (Folder) saveObject(ticket, child);
        } else {
            throw new ConnexienceException("Cannot add children to special folder");
        }
    }

    /**
     * Get all the contents of a Folder that are not themselves folders
     */
    public List getFolderContents(Folder folder) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from ServerObject as obj where obj.containerId=? ORDER BY lower(obj.name) ASC");
            q.setString(0, folder.getId());
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing folder contents: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get all of the DocumentRecords for a folder
     */
    public List getFolderDocumentRecords(Folder folder) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentRecord as record where record.containerId=? ORDER BY lower(record.name) ASC");
            q.setString(0, folder.getId());
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing folder documents: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a named child folder from an existing folder using a session
     */
    public Folder getNamedChildFolder(String parentId, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return getNamedChildFolder(parentId, name, session);
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a named child folder from an existing folder. If nothing exists, null is returned
     */
    public Folder getNamedChildFolder(String parentId, String name, Session session) throws ConnexienceException {
        try {
            Query q = session.createQuery("from Folder as obj where obj.containerId=? and obj.name=?");
            q.setString(0, parentId);
            q.setString(1, name);
            List results = q.list();
            if (results.size() > 0) {
                return (Folder) results.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting child folder: " + e.getMessage());
        }
    }

    /**
     * Get all of the child folders for a folder
     */
    public List getChildFolders(Folder folder) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Folder as f where f.containerId=? ORDER BY lower(f.name) ASC");
            q.setString(0, folder.getId());
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing child folders: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get all of the child folders in a heirarchy as a list
     */
    public List getChildHeirarchyAsList(Folder folder) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ArrayList folders = new ArrayList();
            addChildFoldersToList(folders, session, folder);
            return folders;
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Recursive method to add to a child heirarchy list
     */
    private void addChildFoldersToList(List folders, Session session, Folder folder) throws ConnexienceException {
        try {
            Query q = session.createQuery("from Folder as f where f.containerId=?");
            q.setString(0, folder.getId());
            List results = q.list();
            Folder f;
          for (Object result : results)
          {
            f = (Folder) result;
            folders.add(f);
            addChildFoldersToList(folders, session, f);
          }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }

    /**
     * Creates a new instance of OrganisationFolderAccess
     */
    public OrganisationFolderAccess(String organisationId)
    {
        this.organisationId = organisationId;
    }

    /**
     * Does a specified folder exist
     */
    public boolean folderExists(String folderId)
    {
        try {
          return getFolder(folderId) != null;
        } catch (Exception ignored) {
            return false;
        }
    }

    /**
     * Does the current organisation contain a specified folder
     */
    public boolean organisationContainsFolder(String folderId) throws ConnexienceException {
        Folder folder =(Folder) getObject(folderId, Folder.class);
        if (folder != null) {
          return folder.getOrganisationId().equals(organisationId);
        } else {
            return false;
        }
    }


    /**
     * Is a folder located in the child hierarchy of a parent folder
     */
    public List getParentHierarchy(String folderId) throws ConnexienceException {
        ArrayList results = new ArrayList();
        try {
            Folder f;
            f = getFolder(folderId);
            
            while(!f.getContainerId().equals(organisationId) && f!=null){
                results.add(f);
                if(f.getContainerId()!=null && !f.getContainerId().trim().isEmpty()){
                    f = getFolder(f.getContainerId());
                } else {
                    f = null;
                }
            }
            return results;
        } catch (Exception e){
            throw new ConnexienceException("Error getting folder hierarchg: " + e.getMessage(), e);
        }
    }

    /**
     * Get a specified folder
     */
    public Folder getFolder(String folderId) throws ConnexienceException
    {
        ServerObject sObj = getObject(folderId, Folder.class);
        if (sObj == null) {
            return null;
        } else if (sObj instanceof Folder) {
            Folder folder =(Folder)sObj;
            if (folder.getOrganisationId().equals(organisationId)) {
                return folder;
            } else {
                throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
            }
        } else {
            // This may happen if someone asked for id which does not refer to a folder
            throw new ConnexienceException("%s: expected %s, actual: %s", ConnexienceException.INVALID_SERVER_OBJECT_TYPE, Folder.class.getName(), sObj.getClass().getName());
        }
    }

    /** Get a folder by short name */
    public Folder getFolderByShortName(String shortName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Folder as obj where obj.shortName = ?");
            List results = q.list();
            if(results.size()>0){
                return (Folder)results.get(0);
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting folder by short name: " + e.getMessage(), e);

        } finally {
            closeSession(session);
        }
    }

    /**
     * Create all of the special folders
     * @param ticket
     */
    public void createSpecialFolders(Ticket ticket) throws ConnexienceException {
        createUsersFolder(ticket);
        createGroupsFolder(ticket);
        createDataFolder(ticket);
        createDocumentTypesFolder(ticket);
        createServicesFolder(ticket);
        createApplicationsFolder(ticket);
    }

    /**
     * Does the data folder exist
     */
    public boolean dataFolderExists() throws ConnexienceException {
      return getDataFolder() != null;
    }

    /**
     * Create the document types folder
     * @param ticket
     */
    public void createDocumentTypesFolder(Ticket ticket) throws ConnexienceException {
        checkOrganisation();
        try {
            DocumentTypesFolder folder = getDocumentTypesFolder();
            if (folder == null) {
                folder = new DocumentTypesFolder();
                folder.setName("Document Types");
                folder.setOrganisationId(organisation.getId());
                folder.setDescription("Document types recognised by the organisation");
                folder.setContainerId(organisation.getId());
                folder = (DocumentTypesFolder) saveObject(ticket, folder);

                // Set the folder id in the organisation
                organisation.setDocumentTypesFolderId(folder.getId());
                organisation = (Organisation) saveObject(ticket, organisation);
            }

        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }

    /**
     * Create the data folder
     * @param ticket
     */
    public void createDataFolder(Ticket ticket) throws ConnexienceException {
        checkOrganisation();
        try {
            DataFolder folder = getDataFolder();
            if (folder == null) {
                folder = new DataFolder();
                folder.setName("Data");
                folder.setOrganisationId(organisation.getId());
                folder.setDescription("Organisation Documents and Data");
                folder.setContainerId(organisation.getId());
                folder = (DataFolder) saveObject(ticket, folder);

                // Set the folder id in the organisation
                organisation.setDataFolderId(folder.getId());
                organisation = (Organisation) saveObject(ticket, organisation);
            }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }

    /**
     * Create the services folder
     * @param ticket
     */
    public void createServicesFolder(Ticket ticket) throws ConnexienceException {
        checkOrganisation();
      ServicesFolder folder = getServicesFolder();
      if (folder == null) {
          folder = new ServicesFolder();
          folder.setName("Services");
          folder.setOrganisationId(organisation.getId());
          folder.setDescription("Workflow Services Definitions");
          folder.setContainerId(organisation.getId());
          folder = (ServicesFolder) saveObject(ticket, folder);

          // Set the folder id in the organisation
          organisation.setServicesFolderId(folder.getId());
          organisation = (Organisation) saveObject(ticket, organisation);
      }
    }

    /**
     * Create the services folder
     * @param ticket
     */
    public void createApplicationsFolder(Ticket ticket) throws ConnexienceException {
        checkOrganisation();
      ApplicationsFolder folder = getApplicationsFolder();
      if (folder == null) {
          folder = new ApplicationsFolder();
          folder.setName("Applications");
          folder.setOrganisationId(organisation.getId());
          folder.setDescription("External Application Definitions");
          folder.setContainerId(organisation.getId());
          folder = (ApplicationsFolder) saveObject(ticket, folder);

          // Set the folder id in the organisation
          organisation.setApplicationsFolderId(folder.getId());
          organisation = (Organisation) saveObject(ticket, organisation);
      }
    }

    /**
     * Get the applications folder
     */
    public ApplicationsFolder getApplicationsFolder() throws ConnexienceException {
        checkOrganisation();
        ApplicationsFolder folder =(ApplicationsFolder) getObject(organisation.getApplicationsFolderId(), ApplicationsFolder.class);
        if (folder !=null) {
            return folder;
        } else {
            return null;
        }
    }

    /**
     * Does the applications folder exist
     */
    public boolean applicationsFolderExists() throws ConnexienceException {
      return getApplicationsFolder() != null;
    }

    /**
     * Get the services folder
     */
    public ServicesFolder getServicesFolder() throws ConnexienceException {
        checkOrganisation();
        ServicesFolder folder =(ServicesFolder) getObject(organisation.getServicesFolderId(), ServicesFolder.class);
        if (folder !=null) {
            return folder;
        } else {
            return null;
        }
    }

    /**
     * Does the services folder exist
     */
    public boolean servicesFolderExists() throws ConnexienceException {
      return getServicesFolder() != null;
    }


    /**
     * Get the document types folder
     */
    public DocumentTypesFolder getDocumentTypesFolder() throws ConnexienceException {
        checkOrganisation();
        DocumentTypesFolder object = (DocumentTypesFolder) getObject(organisation.getDocumentTypesFolderId(),DocumentTypesFolder.class);
        if (object!=null) {
            return  object;
        } else {
            return null;
        }
    }

    /**
     * Get the data folder with an existing session
     */
    public DataFolder getDataFolder(Session session) throws ConnexienceException {
        checkOrganisation(session);
      return (DataFolder)getObject(organisation.getDataFolderId(), DataFolder.class);
    }

    /**
     * Get the data folder
     */
    public DataFolder getDataFolder() throws ConnexienceException {
        checkOrganisation();
        return (DataFolder)getObject(organisation.getDataFolderId(), DataFolder.class);
    }

    /**
     * User Folder exists
     */
    public boolean usersFolderExists() throws ConnexienceException {
        try {
          return getUsersFolder() != null;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }

    /**
     * Get the users folder for an organisation
     */
    public UsersFolder getUsersFolder() throws ConnexienceException {
        checkOrganisation();
        return (UsersFolder) getObject(organisation.getUserFolderId(), UsersFolder.class);
    }

    /**
     * Create the users folder for an organisation
     * @param ticket
     */
    public void createUsersFolder(Ticket ticket) throws ConnexienceException {
        checkOrganisation();

        // Create the folder
        UsersFolder folder;

        try {
            folder = getUsersFolder();
            if (folder == null) {
                folder = new UsersFolder();
                folder.setName("Users");
                folder.setOrganisationId(organisation.getId());
                folder.setDescription("User accounts");
                folder.setContainerId(organisation.getId());
                folder = (UsersFolder) saveObject(ticket, folder);

                // Set this ID in the organisation
                organisation.setUserFolderId(folder.getId());
                organisation = (Organisation) saveObject(ticket, organisation);
            }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }

        // Now assign all of the users to that folder
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from User as u where u.organisationId = ?");
            q.setString(0, organisation.getId());
            List users = q.list();
            User user;

          for (Object user1 : users)
          {
            user = (User) user1;
            user.setContainerId(folder.getId());
            session.update(user);
          }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Does the Groups Folder exists
     */
    public boolean groupsFolderExists() throws ConnexienceException {
        try {
          return getGroupsFolder() != null;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }
    }

    /**
     * Get the users folder for an organisation
     */
    public GroupsFolder getGroupsFolder() throws ConnexienceException {
        checkOrganisation();
        return (GroupsFolder) getObject(organisation.getGroupFolderId(), GroupsFolder.class);
    }

    /**
     * Create the groups folder for an organisation
     * @param ticket
     */
    public void createGroupsFolder(Ticket ticket) throws ConnexienceException {
        checkOrganisation();

        // Create the folder
        GroupsFolder folder;

        try {
            folder = getGroupsFolder();
            if (folder == null) {
                folder = new GroupsFolder();
                folder.setName("Groups");
                folder.setOrganisationId(organisation.getId());
                folder.setDescription("Group details");
                folder.setContainerId(organisation.getId());
                folder = (GroupsFolder) saveObject(ticket, folder);

                // Set the folder id in the organisation
                organisation.setGroupFolderId(folder.getId());
                organisation = (Organisation) saveObject(ticket, organisation);
            }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        }

        // Now assign all of the users to that folder
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Group as g where g.organisationId = ?");
            q.setString(0, organisation.getId());
            List groups = q.list();
            Group group;

          for (Object group1 : groups)
          {
            group = (Group) group1;
            group.setContainerId(folder.getId());
            session.update(group);
          }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Is a Folder a special Folder
     */
    public static boolean isSpecialFolder(Folder f) {
        if (f instanceof DataFolder) {
            return true;
        } else if (f instanceof GroupsFolder) {
            return true;
        } else if (f instanceof ProjectsFolder) {
            return true;
        } else if (f instanceof UsersFolder) {
            return true;
        } else if (f instanceof DocumentTypesFolder) {
            return true;
        } else if (f instanceof ServicesFolder) {
            return true;
        } else return f instanceof ApplicationsFolder;
    }
}

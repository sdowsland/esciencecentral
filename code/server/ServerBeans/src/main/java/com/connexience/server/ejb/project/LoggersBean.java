/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.acl.AccessControlRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.ConversionWorkflow;
import com.connexience.server.model.project.FileType;
import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerConfiguration;
import com.connexience.server.model.project.study.LoggerData;
import com.connexience.server.model.project.study.LoggerDeployment;
import com.connexience.server.model.project.study.LoggerType;
import com.connexience.server.model.project.study.Sensor;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.project.study.SubjectGroup;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.exception.ConstraintViolationException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/LoggersBean", beanInterface = LoggersRemote.class)
public class LoggersBean extends HibernateSessionContainer implements LoggersRemote
{
	@Inject
	private EntityManager em;

	@EJB
	private ProjectsRemote projectsBean;

	@EJB
	private StudyRemote studyBean;

	@EJB
	private SubjectsRemote subjectBean;

	@EJB
	private AccessControlRemote aclBean;

	@Override
	public LoggerType getLoggerType(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final LoggerType loggerType = em.find(LoggerType.class, id);

		if (loggerType == null)
		{
			throw new ConnexienceException("LoggerType id='" + id + "' not found in DB.");
		}

		return loggerType;
	}

	@Override
	public LoggerType saveLoggerType(final Ticket ticket, final LoggerType loggerType) throws ConnexienceException
	{
		if (loggerType.getId() == null)
		{
			em.persist(loggerType);

			//create a Default configuration if one doesn't exist.
			Collection<LoggerConfiguration> currentConfigurations = getLoggerConfigurationsByType(ticket, loggerType.getId());
			if(currentConfigurations.size() == 0){
				LoggerConfiguration defaultConfig = new LoggerConfiguration("Default");
				defaultConfig.setLoggerType(loggerType);
				defaultConfig.setDescription("Default configuration for " + loggerType.getName());
				em.persist(defaultConfig);
			}
			return loggerType;
		}
		else
		{
			return em.merge(loggerType);
		}
	}

	@Override
	public void deleteLoggerType(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, id);

		em.remove(loggerType);
	}

	@Override
	public Long getLoggerTypeCount(final Ticket ticket)
	{
		return (Long) em.createQuery("SELECT COUNT(*) FROM LoggerType l")
				.getResultList()
				.get(0);
	}

	@Override
	public Collection<LoggerType> getLoggerTypes(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException
	{
		final Collection<LoggerType> loggerType = em.createQuery("SELECT l from LoggerType l ORDER BY UPPER(l.name)")
				.setFirstResult(start)
				.setMaxResults(maxResults)
				.getResultList();

		if (loggerType == null)
		{
			return new ArrayList<>();
		}

		return loggerType;
	}

	@Override
	public Sensor getSensor(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final Sensor sensor = em.find(Sensor.class, id);

		if (sensor == null)
		{
			throw new ConnexienceException("Sensor id='" + id + "' not found in DB.");
		}

		return sensor;
	}

	@Override
	public Sensor saveSensor(final Ticket ticket, final Sensor sensor)
	{
		if (sensor.getId() == null)
		{
			em.persist(sensor);
			return sensor;
		}
		else
		{
			return em.merge(sensor);
		}
	}

	@Override
	public void deleteSensor(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final Sensor sensor = getSensor(ticket, id);

		em.remove(sensor);
	}

	@Override
	public FileType getFileType(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final FileType fileType = em.find(FileType.class, id);

		if (fileType == null)
		{
			throw new ConnexienceException("FileType id='" + id + "' not found in DB.");
		}

		Hibernate.initialize(fileType.getConversionWorkflows());

		return fileType;
	}

	@Override
	public FileType saveFileType(final Ticket ticket, final FileType fileType)
	{
		if (fileType.getId() == null)
		{
			em.persist(fileType);
			return fileType;
		}
		else
		{
			return em.merge(fileType);
		}
	}

	@Override
	public void deleteFileType(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final FileType fileType = getFileType(ticket, id);

		try
		{
			em.remove(fileType);
		}
		catch (ConstraintViolationException e)
		{
			throw new ConnexienceException("Unable to remove File Type as it is associated with " + fileType.getSensors().size() + " Sensor(s)");
		}
	}

	@Override
	public Long getFileTypeCount(final Ticket ticket)
	{
		return (Long) em.createQuery("SELECT COUNT(*) FROM FileType f")
				.getResultList()
				.get(0);
	}

	@Override
	public Collection<FileType> getFileTypes(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException
	{
		final Collection<FileType> fileTypes = em.createQuery("SELECT f from FileType f")
				.setFirstResult(start)
				.setMaxResults(maxResults)
				.getResultList();

		if (fileTypes == null)
		{
			return new ArrayList<>();
		}

		return fileTypes;
	}

	@Override
	public ConversionWorkflow getConversionWorkflow(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final ConversionWorkflow conversionWorkflow = em.find(ConversionWorkflow.class, id);

		if (conversionWorkflow == null)
		{
			throw new ConnexienceException("ConversionWorkflow id='" + id + "' not found in DB.");
		}

		return conversionWorkflow;
	}

	@Override
	public ConversionWorkflow saveConversionWorkflow(final Ticket ticket, final ConversionWorkflow conversionWorkflow)
	{
		if (conversionWorkflow.getId() == null)
		{
			em.persist(conversionWorkflow);
			return conversionWorkflow;
		}
		else
		{
			return em.merge(conversionWorkflow);
		}
	}

	@Override
	public void deleteConversionWorkflow(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final ConversionWorkflow conversionWorkflow = getConversionWorkflow(ticket, id);

		em.remove(conversionWorkflow);
	}

	@Override
	public Logger getLogger(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final Logger logger = em.find(Logger.class, id);

		if (logger == null)
		{
			throw new ConnexienceException("Logger id='" + id + "' not found in DB.");
		}

		return logger;
	}

	@Override
	public List<Logger> getAvailableLoggersByType(final Ticket ticket, final Integer loggerTypeId, final Date startDate, final Date endDate) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, loggerTypeId);

		final List<Logger> loggers = new ArrayList<>(loggerType.getLoggers());

		final Collection<LoggerDeployment> loggerDeployments = em.createQuery("SELECT l FROM LoggerDeployment l WHERE l.logger.loggerType.id = :loggerTypeId AND (l.startDate <= :endDate AND l.endDate >= :startDate)")
				.setParameter("loggerTypeId", loggerTypeId)
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
				.getResultList();

        for (final LoggerDeployment loggerDeployment : loggerDeployments)
		{
			loggers.remove(loggerDeployment.getLogger());
		}

		return loggers;
	}

	@Override
	public Integer getDeployedLoggersCountByType(final Ticket ticket, final Integer loggerTypeId, final Date startDate, final Date endDate) throws ConnexienceException
	{
		final Long count = (Long) em.createQuery("SELECT COUNT(DISTINCT l.logger.id) FROM LoggerDeployment l WHERE l.logger.loggerType.id = :loggerTypeId AND (l.startDate <= :endDate AND l.endDate >= :startDate)")
				.setParameter("loggerTypeId", loggerTypeId)
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				.getSingleResult();

		return count.intValue();
	}

	@Override
	public List<LoggerDeployment> undeployLoggersByStudy(final Ticket ticket, final Integer studyId) throws ConnexienceException
	{
		Study study = em.merge(studyBean.getStudy(ticket, studyId));

		for (final LoggerDeployment loggerDeployment : study.getLoggerDeployments())
		{
			loggerDeployment.setActive(false);
			em.merge(loggerDeployment);
		}

		study = em.merge(study);

		return study.getLoggerDeployments();
	}

	@Override
	public Logger saveLogger(final Ticket ticket, final Logger logger) throws ConnexienceException
	{
		try
		{
			if (logger.getId() == null)
			{
				em.persist(logger);
				return logger;
			}
			else
			{
				return em.merge(logger);
			}
		}
		catch (ConstraintViolationException e)
		{
			throw new ConnexienceException(e);
		}
	}

	@Override
	public void deleteLogger(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final Logger logger = getLogger(ticket, id);

		em.remove(logger);
	}

	@Override
	public Long getLoggerCount(final Ticket ticket)
	{
		return (Long) em.createQuery("SELECT COUNT(*) FROM Logger l")
				.getResultList()
				.get(0);
	}

	@Override
	public Long getLoggerCount(final Ticket ticket, final Integer loggerTypeId)
	{
		return (Long) em.createQuery("SELECT COUNT(*) FROM Logger l WHERE l.loggerType.id = :loggerTypeId")
				.setParameter("loggerTypeId", loggerTypeId)
				.getResultList()
				.get(0);
	}

	@Override
	public Collection<Logger> getLoggers(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException
	{
		final Collection<Logger> loggers = em.createQuery("SELECT l from Logger l")
				.setFirstResult(start)
				.setMaxResults(maxResults)
				.getResultList();

		if (loggers == null)
		{
			return new ArrayList<>();
		}

		return loggers;
	}

	@Override
	public Collection<Logger> getLoggersByType(final Ticket ticket, final Integer loggerTypeId) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, loggerTypeId);

		final Collection<Logger> loggers = em.createQuery("SELECT l FROM Logger l WHERE l.loggerType = :loggerType")
				.setParameter("loggerType", loggerType)
				.getResultList();

		if (loggers == null)
		{
			return new ArrayList<>();
		}

		return loggers;
	}

	@Override
	public Collection<Logger> getLoggersBySerial(final Ticket ticket, final String serial) throws ConnexienceException
	{
		final Collection<Logger> loggers = em.createQuery("SELECT l FROM Logger l WHERE l.serialNumber = :serial")
				.setParameter("serial", serial)
				.getResultList();

		if (loggers == null)
		{
			return new ArrayList<>();
		}

		return loggers;
	}

	@Override
	public LoggerConfiguration getLoggerConfiguration(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final LoggerConfiguration loggerConfiguration = em.find(LoggerConfiguration.class, id);

		if (loggerConfiguration == null)
		{
			throw new ConnexienceException("LoggerConfiguration id='" + id + "' not found in DB.");
		}

		return loggerConfiguration;
	}

	@Override
	public LoggerConfiguration getLoggerDeploymentConfiguration(final Ticket ticket, final Integer deploymentId) throws ConnexienceException
	{
		LoggerDeployment loggerDeployment= em.find(LoggerDeployment.class, deploymentId);
		return loggerDeployment.getLoggerConfiguration();
	}

	@Override
	public LoggerConfiguration saveLoggerConfiguration(final Ticket ticket, final LoggerConfiguration loggerConfiguration)
	{
		if (loggerConfiguration.getId() == null)
		{
			em.persist(loggerConfiguration);
			return loggerConfiguration;
		}
		else
		{
			return em.merge(loggerConfiguration);
		}
	}

	@Override
	public void deleteLoggerConfiguration(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final LoggerConfiguration loggerConfiguration = getLoggerConfiguration(ticket, id);

		em.remove(loggerConfiguration);
	}

	@Override
	public Long getLoggerConfigurationCount(final Ticket ticket)
	{
		return (Long) em.createQuery("SELECT COUNT(*) FROM LoggerConfiguration l")
				.getResultList()
				.get(0);
	}

	@Override
	public Collection<LoggerConfiguration> getLoggerConfigurations(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException
	{
		final Collection<LoggerConfiguration> loggerConfigurations = em.createQuery("SELECT l from LoggerConfiguration l")
				.setFirstResult(start)
				.setMaxResults(maxResults)
				.getResultList();

		if (loggerConfigurations == null)
		{
			return new ArrayList<>();
		}

		return loggerConfigurations;
	}

	@Override
	public Collection<LoggerConfiguration> getLoggerConfigurationsByType(final Ticket ticket, final Integer loggerTypeId) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, loggerTypeId);

		final Collection<LoggerConfiguration> loggerConfigurations = em.createQuery("SELECT l FROM LoggerConfiguration l WHERE loggerType = :loggerType")
				.setParameter("loggerType", loggerType)
				.getResultList();

		if (loggerConfigurations == null)
		{
			return new ArrayList<>();
		}

		return loggerConfigurations;
	}

	@Override
	public LoggerDeployment getLoggerDeployment(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final LoggerDeployment loggerDeployment = em.find(LoggerDeployment.class, id);

		if (loggerDeployment == null)
		{
			throw new ConnexienceException("LoggerDeployment id='" + id + "' not found in DB.");
		}

		return loggerDeployment;
	}

	@Override
	public LoggerDeployment getLoggerDeployment(Ticket ticket, String studyExternalId, String phase, String loggerSerialNumber) throws ConnexienceException
	{
		List<LoggerDeployment> results = em.createQuery("SELECT  l FROM LoggerDeployment l WHERE l.study.externalId = :studyExternalId AND l.subjectGroup.phase.name = :phase AND l.logger.serialNumber = :loggerSerialNumber AND l.active = true")
				.setParameter("studyExternalId", studyExternalId)
				.setParameter("phase", phase)
				.setParameter("loggerSerialNumber", loggerSerialNumber)
				.getResultList();

		if (results == null || results.isEmpty())
		{
			return null;
		}

		if (results.size() > 1)
		{
			throw new ConnexienceException("More than 1 active LoggerDeployment for Serial='" + loggerSerialNumber + "' in Study id='" + studyExternalId + "'");
		}

		return results.get(0);
	}

	@Override
	public Collection<LoggerDeployment> getLoggerDeployments(Ticket ticket, Integer subjectGroupId) throws ConnexienceException {
		List<LoggerDeployment> results = em.createQuery("SELECT  l FROM LoggerDeployment l WHERE l.subjectGroup.id = :subjectGroupId ")
				.setParameter("subjectGroupId", subjectGroupId)
				.getResultList();

		if (results == null || results.isEmpty())
		{
			return null;
		}

		return results;
	}

	@Override
	public Collection<LoggerDeployment> getActiveDeploymentsForLogger(Ticket ticket, String loggerSerialNumber) throws ConnexienceException {
		List<LoggerDeployment> results = em.createQuery("SELECT  l FROM LoggerDeployment l WHERE l.logger.serialNumber = :loggerSerialNumber AND l.active = true")
				.setParameter("loggerSerialNumber", loggerSerialNumber)
				.getResultList();

		if (results == null || results.isEmpty())
		{
			return null;
		}

		return results;
	}

	@Override
	public Collection<LoggerDeployment> getLoggerDeploymentsByLoggerId(Ticket ticket, int loggerId) throws ConnexienceException {
		List<LoggerDeployment> results = em.createQuery("SELECT  l FROM LoggerDeployment l WHERE l.logger.id= :loggerId")
				.setParameter("loggerId", loggerId)
				.getResultList();

		if (results == null || results.isEmpty())
		{
			return null;
		}

		return results;
	}



	@Override
	public LoggerDeployment saveLoggerDeployment(final Ticket ticket, final LoggerDeployment loggerDeployment) throws ConnexienceException
	{
		if (StringUtils.isBlank(loggerDeployment.getDataFolderId()))
		{
			SubjectGroup sg = loggerDeployment.getSubjectGroup();
			if (sg == null)
			{
				throw new ConnexienceException("Cannot save LoggerDeployment without a defined SubjectGroup");
			}

			String parentFolder = sg.getDataFolderId();
			if (StringUtils.isBlank(parentFolder))
			{
				throw new ConnexienceException("Cannot save LoggerDeployment, associated SubjectGroup (" + loggerDeployment.getSubjectGroup().getDisplayName() + ") has no defined data folder.");
			}

			Folder dataFolder = new Folder();
			dataFolder.setName(generateFolderName(loggerDeployment));

			dataFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, parentFolder, dataFolder);

			loggerDeployment.setDataFolderId(dataFolder.getId());

			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getAdminGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getAdminGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getMembersGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getMembersGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
		}
		else
		{
			Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, loggerDeployment.getDataFolderId());

			dataFolder.setName(generateFolderName(loggerDeployment));

			em.merge(dataFolder);

			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getAdminGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getAdminGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getMembersGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
			aclBean.grantAccess(ticket, loggerDeployment.getStudy().getMembersGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
		}

		if (loggerDeployment.getId() == null)
		{
			em.persist(loggerDeployment);
			return loggerDeployment;
		}
		else
		{
			return em.merge(loggerDeployment);
		}
	}

	@Override
	public LoggerDeployment updateLoggerDeploymentAdditionalProperties(final Ticket ticket, final Integer deploymentId, final Map<String, String> additionalProperties) throws ConnexienceException{

		final LoggerDeployment loggerDeployment = getLoggerDeployment(ticket, deploymentId);

		loggerDeployment.setAdditionalProperties(additionalProperties);

		return em.merge(loggerDeployment);
	}

	@Override
	public void deleteLoggerDeployment(final Ticket ticket, final Integer id) throws ConnexienceException
	{
		final LoggerDeployment loggerDeployment = getLoggerDeployment(ticket, id);

		em.remove(loggerDeployment);
	}

	@Override
	public LoggerData getLoggerData(final Ticket ticket, final Integer loggerDataId) throws ConnexienceException
	{
		final LoggerData loggerData = em.find(LoggerData.class, loggerDataId);

		if (loggerData == null)
		{
			throw new ConnexienceException("No Logger Data with id='" + loggerDataId + "' could be found.");
		}

		final LoggerDeployment loggerDeployment = loggerData.getLoggerDeployment();

		if (loggerDeployment == null)
		{
			throw new ConnexienceException("Retrieved Logger Data has no (null) Logger Deployment");
		}

		final Study study = loggerDeployment.getStudy();

		if (study == null)
		{
			throw new ConnexienceException("Retrieved Logger Data has no (null) Study (associated with LoggerDeployment)");
		}

		if (study.isPrivateProject() && !projectsBean.isProjectMember(ticket, study))
		{
			throw new ConnexienceException("You do not have permission to access the associated Study");
		}

		return loggerData;
	}

	public List<LoggerData> getLoggerDataByStudy(final Ticket ticket, final Integer studyId) throws ConnexienceException
	{
		final Study study = em.merge(studyBean.getStudy(ticket, studyId));

		if (study.isPrivateProject() && !projectsBean.isProjectMember(ticket, study))
		{
			throw new ConnexienceException("You do not have permission to access the associated Study");
		}

		final List<LoggerData> results = em.createNamedQuery("LoggerData.byStudy")
				.setParameter("studyId", studyId)
				.getResultList();

		if (results == null)
		{
			return new ArrayList<>();
		}

		return results;
	}

	@Override
	public LoggerData saveLoggerData(final Ticket ticket, final LoggerData loggerData) throws ConnexienceException
	{
		if (loggerData.getId() == null)
		{
			em.persist(loggerData);
			return loggerData;
		}
		else
		{
			return em.merge(loggerData);
		}
	}

	@Override
	public Long getLoggerDeploymentCount(final Ticket ticket)
	{
		return (Long) em.createQuery("SELECT COUNT(*) FROM LoggerDeployment l")
				.getResultList()
				.get(0);
	}

	@Override
	public Collection<LoggerDeployment> getLoggerDeployments(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException
	{
		final Collection<LoggerDeployment> loggerDeployments = em.createQuery("SELECT l from LoggerDeployment l")
				.setFirstResult(start)
				.setMaxResults(maxResults)
				.getResultList();

		if (loggerDeployments == null)
		{
			return new ArrayList<>();
		}

		return loggerDeployments;
	}

	@Override
	public Collection<LoggerDeployment> getLoggerDeploymentsByStudy(final Ticket ticket, final Integer studyId) throws ConnexienceException
	{
		final Study study = studyBean.getStudy(ticket, studyId);

		final Collection<LoggerDeployment> loggerDeployments = em.createQuery("SELECT l FROM LoggerDeployment l WHERE l.study = :study")
				.setParameter("study", study)
				.getResultList();

		if (loggerDeployments == null)
		{
			return new ArrayList<>();
		}

		return loggerDeployments;
	}

	@Override
	public Collection<LoggerDeployment> getLoggerDeploymentsByStudy(final Ticket ticket, final Integer studyId, final boolean isActive) throws ConnexienceException
	{
		final Study study = studyBean.getStudy(ticket, studyId);

		final Collection<LoggerDeployment> loggerDeployments = em.createQuery("SELECT l FROM LoggerDeployment l WHERE l.study = :study AND l.active = :active")
				.setParameter("study", study)
				.setParameter("active", isActive)
				.getResultList();

		if (loggerDeployments == null)
		{
			return new ArrayList<>();
		}

		return loggerDeployments;
	}

	@Override
	public Collection<LoggerDeployment> getLoggerDeploymentsInPhase(final Ticket ticket, final Integer studyId, final Integer phaseId) throws ConnexienceException
	{
		final Study study = studyBean.getStudy(ticket, studyId);

		final Collection<LoggerDeployment> loggerDeployments = em.createQuery("SELECT l FROM LoggerDeployment l WHERE l.study = :study AND l.subjectGroup.phase.id = :phaseId")
				.setParameter("study", study)
				.setParameter("phaseId", phaseId)
				.getResultList();

		if (loggerDeployments == null)
		{
			return new ArrayList<>();
		}

		return loggerDeployments;
	}


	@Override
	public FileType createFileType(final Ticket ticket, final String name) throws ConnexienceException
	{
		return saveFileType(ticket, new FileType(name));
	}

	@Override
	public ConversionWorkflow createConversionWorkflow(final Ticket ticket, final Integer fileTypeId, final String escWorkflowDocumentId) throws ConnexienceException
	{
		final FileType fileType = getFileType(ticket, fileTypeId);

		final ConversionWorkflow conversionWorkflow = new ConversionWorkflow(escWorkflowDocumentId);

		fileType.addConversionWorkflow(conversionWorkflow);

		saveFileType(ticket, fileType);
		return saveConversionWorkflow(ticket, conversionWorkflow);
	}

	@Override
	public LoggerType createLoggerType(final Ticket ticket, final String manufacturer, final String modelName) throws ConnexienceException
	{
		return saveLoggerType(ticket, new LoggerType(manufacturer, modelName));
	}

	@Override
	public Sensor createSensor(final Ticket ticket, final Integer loggerTypeId, final String sensorName) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, loggerTypeId);

		final Sensor sensor = new Sensor(sensorName);

		loggerType.addSensor(sensor);

		saveLoggerType(ticket, loggerType);
		return saveSensor(ticket, sensor);
	}

	@Override
	public LoggerConfiguration createLoggerConfiguration(final Ticket ticket, final Integer loggerTypeId, final String configurationName) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, loggerTypeId);

		final LoggerConfiguration loggerConfiguration = new LoggerConfiguration(configurationName);

		loggerConfiguration.setLoggerType(loggerType);

		saveLoggerType(ticket, loggerType);
		return saveLoggerConfiguration(ticket, loggerConfiguration);
	}

	@Override
	public Logger createLogger(final Ticket ticket, final Integer loggerTypeId, final String serialNumber) throws ConnexienceException
	{
		final LoggerType loggerType = getLoggerType(ticket, loggerTypeId);

		List<Logger> existing = em.createQuery("FROM Logger WHERE loggerType.id = :loggerTypeId AND serialNumber = :serialNumber")
				.setParameter("serialNumber", serialNumber)
				.setParameter("loggerTypeId", loggerTypeId)
				.getResultList();

		if(existing.size() > 0){
			throw new ConnexienceException("Logger with Serial Number='" + serialNumber + "' of type (LoggerType id='" + loggerTypeId + "') already exists.");
		}

		final Logger logger = new Logger(serialNumber);

		logger.setLoggerType(loggerType);

		return saveLogger(ticket, logger);
	}

	@Override
	public Logger createLogger(final Ticket ticket, final Integer loggerTypeId, final String serialNumber, final String location) throws ConnexienceException
	{

		Logger logger = createLogger(ticket, loggerTypeId, serialNumber);
		logger.setLocation(location);
		return saveLogger(ticket, logger);
	}

	@Override
	public LoggerDeployment createLoggerDeployment(final Ticket ticket, final Integer studyId, final Integer loggerId, final Integer loggerConfigurationId, final Integer subjectGroupId) throws ConnexienceException
	{
		long startTime = System.currentTimeMillis();
		final Study study = em.merge(studyBean.getStudy(ticket, studyId));

		final Logger logger = getLogger(ticket, loggerId);

		final LoggerConfiguration loggerConfiguration = getLoggerConfiguration(ticket, loggerConfigurationId);

		final SubjectGroup subjectGroup = em.merge(subjectBean.getSubjectGroup(ticket, subjectGroupId));

		final LoggerDeployment loggerDeployment = new LoggerDeployment(logger, loggerConfiguration, subjectGroup, study.getStartDate(), study.getEndDate());

		loggerDeployment.setStudy(study);

		loggerDeployment.setActive(true);

		saveLogger(ticket, logger);

		saveLoggerConfiguration(ticket, loggerConfiguration);

		subjectBean.saveSubjectGroup(ticket, subjectGroup);

		return saveLoggerDeployment(ticket, loggerDeployment);
	}

	@Override
	public LoggerDeployment createLoggerDeployment(final Ticket ticket, Study study, final Integer loggerId, final Integer loggerConfigurationId, final Integer subjectGroupId, final Date startDate, final Date endDate) throws ConnexienceException
	{
		study = em.merge(study);

		final Logger logger = getLogger(ticket, loggerId);

		final LoggerConfiguration loggerConfiguration = getLoggerConfiguration(ticket, loggerConfigurationId);

		final SubjectGroup subjectGroup = em.merge(subjectBean.getSubjectGroup(ticket, subjectGroupId));

		final LoggerDeployment loggerDeployment = new LoggerDeployment(logger, loggerConfiguration, subjectGroup, startDate, endDate);

		loggerDeployment.setStudy(study);

		loggerDeployment.setActive(true);

		return saveLoggerDeployment(ticket, loggerDeployment);
	}

	@Override
	public Sensor addFileTypeToSensor(final Ticket ticket, final Integer sensorId, final Integer fileTypeId) throws ConnexienceException
	{
		final Sensor sensor = getSensor(ticket, sensorId);
		final FileType fileType = getFileType(ticket, fileTypeId);

		sensor.setFileType(fileType);

		saveFileType(ticket, fileType);
		return saveSensor(ticket, sensor);
	}

	@Override
	public LoggerDeployment isLoggerDeployedToGroup(final Ticket ticket, final String serialNumber, final Integer groupId) throws ConnexienceException{

		SubjectGroup group = subjectBean.getSubjectGroup(ticket, groupId);

		group = em.merge(group);

		//Check that there is a deployment for this group
		List<LoggerDeployment> deployments = group.getLoggerDeployments();

		for (LoggerDeployment deployment : deployments) {
			if (deployment.getLogger().getSerialNumber().equals(serialNumber)) {
				return deployment;
			}
		}
		return null;
	}

	private String generateFolderName(final LoggerDeployment loggerDeployment)
	{
		final StringBuilder result = new StringBuilder("Logger: ");

		result.append(loggerDeployment.getLogger().getSerialNumber());

		if (StringUtils.isNotBlank(loggerDeployment.getLoggerConfiguration().getName()))
		{
			result.append(" (Config: ")
					.append(loggerDeployment.getLoggerConfiguration().getName())
					.append(")");
		}

		return result.toString();
	}
}

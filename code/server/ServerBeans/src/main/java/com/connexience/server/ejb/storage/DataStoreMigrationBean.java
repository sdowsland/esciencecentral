/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.storage.migration.DataStoreMigration;
import com.connexience.server.model.storage.migration.DocumentVersionMigration;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/** 
 * This bean manages the process of migrating data from one data store to another
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/DataStoreMigrationBean", beanInterface = DataStoreMigrationRemote.class)
public class DataStoreMigrationBean extends HibernateSessionContainer implements DataStoreMigrationRemote {
    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;

    
    @Override
    public List listMigrations(Ticket ticket) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DataStoreMigration");
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing migrations: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public DataStoreMigration createMigration(Ticket ticket, DataStore newStore) throws ConnexienceException {
        assertAdministrator(ticket);
        DataStoreMigration migration = new DataStoreMigration();
        try {
            migration.setTargetDataStore(newStore);
        } catch (Exception e){
            throw new ConnexienceException("Error saving new datastore information: " + e.getMessage());
        }
        migration.setUserId(ticket.getUserId());
        migration = (DataStoreMigration)savePlainObject(migration);
        addNewMigrationDocuments(ticket, migration.getId());
        return migration;
    }

    @Override
    public DataStoreMigration saveMigration(Ticket ticket, DataStoreMigration migration) throws ConnexienceException {
        assertAdministrator(ticket);
        return (DataStoreMigration)savePlainObject(migration);
    }

    @Override
    public DataStoreMigration getMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DataStoreMigration as m where m.id=:id");
            q.setLong("id", migrationId);
            return (DataStoreMigration)q.uniqueResult();
        } catch (Exception e){
            throw new ConnexienceException("Error getting migration: " + migrationId + ": " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    @Override
    public void startMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        DataStoreMigration migration = getMigration(ticket, migrationId);
        if(migration.getStatus()==DataStoreMigration.MIGRATION_STOPPED){
            migration.setStatus(DataStoreMigration.MIGRATION_RUNNING);
            migration = saveMigration(ticket, migration);
            
            // Send the next JMS message for this migration
            sendJMSMessageForNextDocumentInMigration(ticket, migration.getId());
        }
    }

    @Override
    public void stopMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        DataStoreMigration migration = getMigration(ticket, migrationId);
        if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){
            migration.setStatus(DataStoreMigration.MIGRATION_STOPPED);
            migration = saveMigration(ticket, migration);
        }
    }

    @Override
    public int addNewMigrationDocuments(Ticket ticket, long migrationId) throws ConnexienceException {
        int size = getNumberOfUnmigratedDocuments(ticket, migrationId);
        try {
            sendRebuildVersionListMessage(ticket, migrationId);
        } catch (Exception e){
            throw new ConnexienceException("Error sending JMS message in addNewMigrationDocuments: " + e.getMessage(), e);
        }
        return size;
    }

    @Override
    public int getNumberOfUnmigratedDocuments(Ticket ticket, long migrationId) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        int size = 0;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("select count(id) from documentversions where documentversions.id not in(select versionid from migrationversions where migrationversions.migrationid=?);");
            s.setLong(1, migrationId);
            r = s.executeQuery();
            if(r.next()){
                size = r.getInt(1);
            } else {
                size = 0;
            }
            return size;
        } catch (Exception e){
            throw new ConnexienceException("Error adding migration documents: " + e.getMessage(), e);
        } finally {
            try {r.close();} catch (Exception e){}
            try {s.close();} catch (Exception e){}
            try {c.close();} catch (Exception e){}
        }
    }
    
    
    @Override
    public boolean sendJMSMessageForNextDocumentInMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersionMigration as v where v.migrationId=:migrationId and v.copyStatus=:copyStatus");
            q.setLong("migrationId", migrationId);
            q.setInteger("copyStatus", DocumentVersionMigration.WAITING_FOR_COPY);
            q.setMaxResults(1);
            List results = q.list();
            if(results.size()>0){
                Object v = results.get(0);
                if(v instanceof DocumentVersionMigration){
                    DocumentVersionMigration dvm = (DocumentVersionMigration)v;
                    dvm.setCopyStatus(DocumentVersionMigration.IN_JMS_QUEUE);
                    session.saveOrUpdate(dvm);
                    session.flush();
                    
                    // Send the message
                    try {
                        sendMigrationTextMessage(migrationId, dvm.getId(), dvm.getDocumentID(), dvm.getVersionId(), dvm.getOrganisationId());
                    } catch (Exception e){
                        // Error sending to JMS queue
                        dvm.setCopyStatus(DocumentVersionMigration.COPY_FAILED);
                        dvm.setCopyErrorMessage("Error sending message to JMS queue: " + e.getMessage());
                        session.saveOrUpdate(dvm);
                        session.flush();
                    }
                    
                    // Send the JMS message
                    return true;
                } else {
                    return false;
                }
                
            } else {
                return false;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error generating next migration JMS message: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void deleteMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        DataStoreMigration migration = getMigration(ticket, migrationId);
        EJBLocator.lookupObjectRemovalBean().remove(migration);
    }

    @Override
    public void deleteDocumentsForMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("delete from migrationversions where migrationid=?");
            q.setLong(0, migrationId);
            q.executeUpdate();
        } catch (Exception e){
            throw new ConnexienceException("Error deleting documents for migration: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    @Override
    public int getNumberOfDocumentsInMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        int size = 0;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("select count(id) from migrationversions where migrationversions.migrationid=?");
            s.setLong(1, migrationId);
            r = s.executeQuery();
            if(r.next()){
                size = r.getInt(1);
            } else {
                size = 0;
            }
            return size;
        } catch (Exception e){
            throw new ConnexienceException("Error adding migration documents: " + e.getMessage(), e);
        } finally {
            try {r.close();} catch (Exception e){}
            try {s.close();} catch (Exception e){}
            try {c.close();} catch (Exception e){}
        }
    }

    @Override
    public int getNumberOfDocumentsInMigrationWithStatus(Ticket ticket, long migrationId, int status) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;
        int size = 0;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("select count(id) from migrationversions where migrationversions.migrationid=? and migrationversions.copystatus=?");
            s.setLong(1, migrationId);
            s.setInt(2, status);
            r = s.executeQuery();
            if(r.next()){
                size = r.getInt(1);
            } else {
                size = 0;
            }
            return size;
        } catch (Exception e){
            throw new ConnexienceException("Error adding migration documents: " + e.getMessage(), e);
        } finally {
            try {r.close();} catch (Exception e){}
            try {s.close();} catch (Exception e){}
            try {c.close();} catch (Exception e){}
        }
    }

    @Override
    public List getFailedDocumentsInMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersionMigration obj where obj.migrationId=:migrationid and obj.copyStatus=:copystatus");
            q.setLong("migrationid", migrationId);
            q.setInteger("copystatus", DocumentVersionMigration.COPY_FAILED);
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error getting failed documents for migtation: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void resetInProgressDocumentMigrations(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update migrationversions set copystatus=:status, copyerrormessage='' where (migrationid=:migrationid and copystatus=:failstatus)");
            q.setInteger("status", DocumentVersionMigration.WAITING_FOR_COPY);
            q.setLong("migrationid", migrationId);
            q.setInteger("failstatus", DocumentVersionMigration.IN_JMS_QUEUE);
            q.executeUpdate();
        } catch (Exception e){
            throw new ConnexienceException("Error retrying failed document migrations: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    
    @Override
    public void retryFailedDocumentsForMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update migrationversions set copystatus=:status, copyerrormessage='' where (migrationid=:migrationid and copystatus=:failstatus)");
            q.setInteger("status", DocumentVersionMigration.WAITING_FOR_COPY);
            q.setLong("migrationid", migrationId);
            q.setInteger("failstatus", DocumentVersionMigration.COPY_FAILED);
            q.executeUpdate();

            // Set all queued message back to waiting
            SQLQuery jmsQuery = session.createSQLQuery("update migrationversions set copystatus=:status, copyerrormessage='' where (migrationid=:migrationid and copystatus=:jmsstatus)");
            jmsQuery.setInteger("status", DocumentVersionMigration.WAITING_FOR_COPY);
            jmsQuery.setLong("migrationid", migrationId);
            jmsQuery.setInteger("jmsstatus", DocumentVersionMigration.IN_JMS_QUEUE);
            jmsQuery.executeUpdate();
            
        } catch (Exception e){
            throw new ConnexienceException("Error retrying failed document migrations: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void finishMigration(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        DataStoreMigration migration = getMigration(ticket, migrationId);
        if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){
            DataStore store = null;
            try {
                store = migration.getTargetDataStore();
            } catch (Exception e){
                throw new ConnexienceException("Error recreating data store: " + e.getMessage(), e);
            }

            Session session = null;
            try {
                if(store!=null){
                    Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
                    DataStore orgStore = EJBLocator.lookupStorageBean().getDataStore(ticket, org.getDataStoreId());

                    int unmigratedDocuments = getNumberOfUnmigratedDocuments(ticket, migrationId);
                    if(unmigratedDocuments==0){
                        session = getSession();
                        
                        // Set some properties in the store
                        store.setOrganisationId(org.getId());
                        store.setName("Data store for: " + org.getName());
                        store.setCreatorId(getCachedRootSecurityObject().getRootUserId());
                        store.setWriteEnabled(EJBLocator.lookupPreferencesBean().isStorageEnabled());   // Set writable status
                        store =(DataStore)savePlainObject(store, session);
                        org.setDataStoreId(store.getId());
                        flushCaches();
                        org = (Organisation)savePlainObject(org, session);
                        flushCaches();
                        migration.setStatus(DataStoreMigration.MIGRATION_FINISHED);
                        savePlainObject(migration, session);
                        session.delete(orgStore);
                        deleteMigration(ticket, migrationId);
                    } else {
                        throw new ConnexienceException("There are still: " + unmigratedDocuments + " for this migration");
                    }
                } else {
                    throw new ConnexienceException("Migration does not have a valid data store");
                }
            } catch (Exception e){
                throw new ConnexienceException("Error finishing migration: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("Migration is not currently running");
        }
    }
    
    /** Send a control text message to all workflow engines */
    private void sendMigrationTextMessage(long migrationId, long dvmId, String documentId, String versionId, String organisationId) throws Exception {
        javax.jms.Connection connection = null;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
            javax.jms.Session jmsSession = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Queue queue = jmsSession.createQueue("DataMigrationQueue");

            MessageProducer publisher = jmsSession.createProducer(queue);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            // Create the message
            TextMessage tm = jmsSession.createTextMessage();
            tm.setText("MigrateVersion");
            tm.setLongProperty("migrationId", migrationId);
            tm.setLongProperty("documentVersionMigrationId", dvmId);
            tm.setStringProperty("documentId", documentId);
            tm.setStringProperty("versionId", versionId);
            tm.setStringProperty("organisationId", organisationId);
            publisher.send(tm);
        } catch (Exception e){
            throw new ConnexienceException("Error sending control message: " + e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println("Error closing JMS connection: " + e.getMessage());
            }
        }           
    }    
    
    /** Send a control text message to all workflow engines */
    private void sendRebuildVersionListMessage(Ticket ticket, long migrationId) throws Exception {
        javax.jms.Connection connection = null;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
            javax.jms.Session jmsSession = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Queue queue = jmsSession.createQueue("DataMigrationQueue");

            MessageProducer publisher = jmsSession.createProducer(queue);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            // Create the message
            TextMessage tm = jmsSession.createTextMessage();
            tm.setText("CreateDocumentMigrations");
            tm.setLongProperty("migrationId", migrationId);
            tm.setStringProperty("userId", ticket.getUserId());
            tm.setStringProperty("organisationid", ticket.getOrganisationId());
 
            publisher.send(tm);
        } catch (Exception e){
            throw new ConnexienceException("Error sending control message: " + e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println("Error closing JMS connection: " + e.getMessage());
            }
        }           
    }        

    @Override
    public DocumentVersionMigration getDocumentVersionMigration(Ticket ticket, long documentVersionMigrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersionMigration as dvm where dvm.id=:id");
            q.setLong("id", documentVersionMigrationId);
            return (DocumentVersionMigration)q.uniqueResult();
        } catch (Exception e){
            throw new ConnexienceException("Error getting DocumentVersionMigration: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public DocumentVersionMigration saveDocumentVersionMigration(Ticket ticket, DocumentVersionMigration dvm) throws ConnexienceException {
        assertAdministrator(ticket);
        try {
            return (DocumentVersionMigration)savePlainObject(dvm);
        } catch (Exception e){
            throw new ConnexienceException("Error saving DocumentVersionMigration: " + e.getMessage(), e);
        }
    }
}
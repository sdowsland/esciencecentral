/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.storage.DataStoreMigrationRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.migration.DataStoreMigration;
import org.apache.log4j.Logger;

import java.util.List;


/**
 * This tasks starts the migrations running if there are any
 * @author hugo
 */
public class StartMigrations extends SchedulerTask {
    private static Logger logger = Logger.getLogger(StartMigrations.class);
    
    public StartMigrations(SchedulerBean parentBean){
        super(parentBean);
        startDelayed = true;
        startDelay = 20000;
        repeating = false;
        name = "Start data migration system";
    }
    
    @Override
    public synchronized void run() {
        if(enabled){
            try {
                logger.info("Running StartMigrations task");
                Ticket ticket = parentBean.getDefaultOrganisationAdminTicket();
                DataStoreMigrationRemote iface = EJBLocator.lookupDataStoreMigrationBean();
                List migrations = iface.listMigrations(ticket);
                DataStoreMigration migration;
                for(Object o : migrations){
                    migration = (DataStoreMigration)o;
                    logger.info("Resetting failed documents for migration: " + migration.getId());
                    iface.retryFailedDocumentsForMigration(ticket, migration.getId());
                        
                    logger.info("Resetting documents that were left in the JMS queue for migration: " + migration.getId());
                    iface.resetInProgressDocumentMigrations(ticket, migration.getId());
                    
                    if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){
                        logger.info("Sending next document JMS message for migration: " + migration.getId());
                        iface.sendJMSMessageForNextDocumentInMigration(ticket, migration.getId());
                    }
                }
            } catch (Exception e){
                logger.error("Error checking migrations: " + e.getMessage());
            }
        }
        removeFromParent();
    }    
    
}

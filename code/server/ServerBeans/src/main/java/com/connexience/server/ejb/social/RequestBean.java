/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.social;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Link;
import com.connexience.server.model.social.requests.FriendRequest;
import com.connexience.server.model.social.requests.JoinGroupRequest;
import com.connexience.server.model.social.requests.Request;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * This EJB handles the processing of the various requests supported by the social
 * networking system. These include requests for friendship, links to groups and
 * applications etc.
 * <p/>
 * SJW: At present the sematics of friends requests are that requests are deleted after they are accepted or rejected.
 * On acceptance two links are created between the two profiles.  To remove the connection, remove the links between the profiles.
 * It is not possible for a one way connection at present.
 * <p/>
 * There are subtle semantics of keeping requests and marking them as rejected/approved.  For instance, what should happen
 * in Request(A, B); reject(b) - leaves request.REJECTED.  Does A see this?  Can they make another request?  Can B make a
 * request to A?
 * <p/>
 * At present this isn't a problem because the requests are deleted but this might change in the future.
 *
 * @author nsjw7
 */
@Stateless
@EJB(name = "java:global/ejb/RequestBean", beanInterface = RequestRemote.class)
public class RequestBean extends HibernateSessionContainer implements RequestRemote
{


  /**
   * Post a new request object. This is saved and then will be visible to the
   * target profile the next time they check
   */
  public FriendRequest postFriendRequest(Ticket ticket, FriendRequest request) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      UserDirectoryRemote userDirectory = EJBLocator.lookupUserDirectoryBean();

      User source = userDirectory.getUser(ticket, request.getSenderId());
      User sink = userDirectory.getUser(ticket, request.getRecipientId());
      User publicUser = getPublicUser(ticket);

      //check that neither is the public user
      if (!source.getId().equals(publicUser.getId()) && !sink.getId().equals(publicUser.getId()))
      {
        //check that profiles exist and are unique
        if (source != null && sink != null && source != sink)
        {

          //check user owns source profile
          if (source.getId().equals(ticket.getUserId()))
          {

            FriendRequest r = friendRequestExists(ticket, request.getSenderId(), request.getRecipientId());
            Request invR = friendRequestExists(ticket, request.getRecipientId(), request.getSenderId());

            LinkBean linkBean = new LinkBean();
            Link link = linkBean.getLink(ticket, source, sink);
            if (link != null)
            {
              return null;
            }

            //check to see if a request exists.  If so, return the request
            if (r != null)
            {
              return r;
            }

            //if inverse request exists then accept it and return null as request has been deleted
            else if (invR != null)
            {
              acceptRequest(ticket, invR.getId());
              return null;
            }
            else //set the details and return the request
            {
              request.setTimestamp(new Date());
              request.setStatus(Request.REQUEST_PENDING);
              request.setContainerId(sink.getInboxFolderId());
              request.setName("Friend Request");
              request.setDescription("Request from: " + source.getName() + " to " + sink.getName());
              request.setRecipientId(sink.getId());
              request.setSenderId(source.getId());
              return (FriendRequest) saveObject(ticket, request);
            }
          }
          else
          {
            throw new ConnexienceException("User does not match request source. User: " + ticket.getUserId() + " req.source: " + source.getId());
          }
        }
        else
        {
          throw new ConnexienceException("Invalid request. Source: " + source + ". Sink: " + sink);
        }
      }
      else
      {
        throw new ConnexienceException("Cannot connect to/from public user");
      }
    }
    catch (Exception
        e)
    {
      throw new ConnexienceException("Error posting request: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get all of the friend requests with a certain status sent by a user
   */

  public List listSentRequests(Ticket ticket, int status) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("from FriendRequest as r where r.senderId =? and r.status=?");
      q.setString(0, ticket.getUserId());
      q.setInteger(1, status);
      return q.list();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing requests: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get all the friend requests that have been sent to a user
   */
  public List listReceivedFriendRequests(Ticket ticket, int status) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("from FriendRequest as r where r.recipientId = :targetId and r.status= :status");
      q.setString("targetId", ticket.getUserId());
      q.setInteger("status", status);
      return q.list();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing requests: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get all the requests that have been sent to a user
   */
  public List listReceivedRequests(Ticket ticket, int status) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("from Request as r where r.recipientId = :userId and r.status= :status");
      q.setString("userId", ticket.getUserId());
      q.setInteger("status", status);
      return q.list();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing requests: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Get all the requests that have been sent to a user
   */
  public Long getNumberOfReceivedRequests(Ticket ticket, int status) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("SELECT count(*) from Request as r where r.recipientId = :userId and r.status= :status");
      q.setString("userId", ticket.getUserId());
      q.setInteger("status", status);
      return (Long) q.iterate().next();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing requests: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }


  /**
   * Accept a request
   */
  public void acceptRequest(Ticket ticket, String requestId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("from Request as r where r.id=?");
      q.setString(0, requestId);
      List results = q.list();
      if (results.size() > 0)
      {

        Object o = results.get(0);
        if (o instanceof FriendRequest)
        {

          FriendRequest request = (FriendRequest) o;

          // Make sure the request has been sent to the right user
          if (request.getRecipientId().equals(ticket.getUserId()))
          {
            //get the profiles, set the request to be accepted and create the link
            User source = EJBLocator.lookupUserDirectoryBean().getUser(ticket, request.getSenderId());
            User sink = EJBLocator.lookupUserDirectoryBean().getUser(ticket, request.getRecipientId());


            LinkRemote lRemote = new LinkBean();
            lRemote.createLink(ticket, source, sink);
            lRemote.createLink(ticket, sink, source);

            //delete the request as it has been accepted
            session.delete(request);
            session.flush();

          }
          else
          {
            throw new Exception("Cannot accept a request for another user");
          }
        }
        else if (o instanceof JoinGroupRequest)
        {
          JoinGroupRequest jr = (JoinGroupRequest) o;

          //add the user to the group
          GroupDirectoryRemote groupRemote = EJBLocator.lookupGroupDirectoryBean();
          groupRemote.addUserToGroup(ticket, jr.getSenderId(), jr.getGroupId());

          //delete the request
          session.delete(jr);

          //don't need to add news as this is done in the addUserToGroup method
        }
      }
      else
      {
        throw new Exception("invalid requestID");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error accepting request: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Reject a request
   */
  public void rejectRequest(Ticket ticket, String requestId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("from Request as r where r.id=?");
      q.setString(0, requestId);
      List results = q.list();
      if (results.size() > 0)
      {

        Object o = results.get(0);
        if (o instanceof FriendRequest)
        {
          FriendRequest request = (FriendRequest) o;

          // Make sure the request has been sent to the right user
          if (request.getRecipientId().equals(ticket.getUserId()))
          {
            //delete the request as it has been rejected
            session.delete(request);
          }
          else
          {
            throw new Exception("Cannot reject a request for another user");
          }
        }
        else if (o instanceof JoinGroupRequest)
        {
          //if the request has been sent to the group admin delete the request
          JoinGroupRequest jr = (JoinGroupRequest) o;
          Group g = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, jr.getGroupId());
          if (ticket.getUserId().equals(g.getCreatorId()))
          {
            session.delete(jr);
          }
        }
      }
      else
      {
        throw new Exception("invalid request Id");
      }
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error rejecting request: " + e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /**
   * Return the number of outstanding requests for a user
   */
  public int outstandingRequests(Ticket ticket) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
//      SQLQuery q = session.createSQLQuery("select count(id) from requests_new as r, messages as m where r.id = m.id AND m.recipientid=? and r.status=?");
      SQLQuery q = session.createSQLQuery("select count(id) from objectsflat as o where o.recipientid=? and r.status=?");
      q.setString(0, ticket.getUserId());
      q.setInteger(1, Request.REQUEST_PENDING);
      BigInteger value = (BigInteger) q.uniqueResult();
      return value.intValue();

    }
    catch (Exception e)
    {
      throw new ConnexienceException("Cannot count outstanding requests: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }

  /*
 * Method to determine whether a friend request already exists
 * */
  public FriendRequest friendRequestExists(Ticket ticket, String sourceId, String sinkId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("from FriendRequest as r WHERE r.senderId = :sourceId AND r.recipientId = :targetId");
      q.setString("sourceId", sourceId);
      q.setString("targetId", sinkId);
      return (FriendRequest) q.uniqueResult();
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error finding requests: " + e.getMessage());
    }
    finally
    {
      closeSession(session);
    }
  }


  /*
 * Method to determine whether a user has already requested to join a group
 * */
  public boolean joinGroupRequestExists(Ticket ticket, String userId, String groupId) throws ConnexienceException
  {

    Session session = null;
    try
    {
      session = getSession();
      Query q = session.createQuery("from JoinGroupRequest as jr WHERE jr.senderId = :userId AND jr.groupId = :groupId");
      q.setString("userId", userId);
      q.setString("groupId", groupId);
      return q.uniqueResult() != null;
    }
    catch (Exception e)
    {
      throw new ConnexienceException("Error listing requests: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }


}


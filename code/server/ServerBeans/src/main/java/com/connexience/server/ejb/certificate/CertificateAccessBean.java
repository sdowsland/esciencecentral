/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.certificate;

import com.connexience.server.ConnexienceException;
import com.connexience.server.crypto.ServerObjectKeyManager;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.KeyData;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.util.ByteArrayCompare;
import com.connexience.server.util.SignatureUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;

/**
 * This EJB provides means to access X509 certificates for objects, check whether certificates
 * belong to who they say they do etc.
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/CertificateAccessBean", beanInterface = CertificateAccessRemote.class)
public class CertificateAccessBean extends HibernateSessionContainer implements CertificateAccessRemote {
    /** Key manager object for getting information from the database */
    private ServerObjectKeyManager keyManager;
    
    /** Creates a new instance of CertificateAccessBean */
    public CertificateAccessBean() {
        keyManager = new ServerObjectKeyManager();
    }
    
    /** Get the signing certificate relating to a server object if one exists */
    public X509Certificate getCertificate(Ticket ticket, String objectId) throws ConnexienceException {
        ServerObject obj = getObject(objectId, ServerObject.class);
        if(obj!=null){
            if(!ticket.isSuperTicket()){
                assertPermission(ticket, obj, Permission.READ_PERMISSION);
            }
            KeyData data = keyManager.getObjectKeyData(obj);
            if(data!=null){
                return data.getCertificate();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /** Get a certificate for a User */
    public X509Certificate getUserCertificate(String userId) throws ConnexienceException {
        User user = (User) getObject(userId, User.class);
        if(user != null){
            KeyData data = keyManager.getObjectKeyData(userId);
            if(data!=null){
                return data.getCertificate();
            } else {
                throw new ConnexienceException("User has no certificate");
            }
        } else {
            throw new ConnexienceException("Object is not a user");
        }
    }

    /** Get the KeyStore data for an object */
    public byte[] getKeyStoreData(Ticket ticket, String objectId) throws ConnexienceException {
        // Only provide a keystore to the calling user
        if(ticket.getUserId().equals(objectId)){
            ServerObjectKeyManager mgr = new ServerObjectKeyManager();
            KeyData kd = mgr.getObjectKeyData(objectId);
            if(kd!=null){
                return kd.getKeyStoreData();
            } else {
                throw new ConnexienceException("No key exists for object");
            }
                    
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    /** Verify whether a certificate matches the database certificate for a server object */
    public boolean certificateBelongsToObject(Ticket ticket, String objectId, X509Certificate userCert) throws ConnexienceException {
        ServerObject obj = getObject(objectId, ServerObject.class);
        if(obj!=null){
            assertPermission(ticket, obj, Permission.READ_PERMISSION);
            KeyData data = keyManager.getObjectKeyData(objectId);
            if(data!=null){
                X509Certificate dbCert = data.getCertificate();
                if(dbCert!=null){
                    try {
                        return SignatureUtils.certificatesMatch(dbCert, userCert);
                    } catch (Exception ignored){
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /** Verify whether the MD5 hash of a certificate matches the database version */
    public boolean validateCertificateMD5Hash(Ticket ticket, String objectId, byte[] md5Hash) throws ConnexienceException {
        ServerObject obj = getObject(objectId, ServerObject.class);
        if(obj!=null){
            assertPermission(ticket, obj, Permission.READ_PERMISSION);
            KeyData data = keyManager.getObjectKeyData(objectId);
            if(data!=null){
                X509Certificate dbCert = data.getCertificate();
                if(dbCert!=null){
                    try {
                        MessageDigest md = MessageDigest.getInstance("MD5");
                        byte[] dbHash = md.digest(dbCert.getEncoded());
                        return ByteArrayCompare.isIdentical(dbHash, md5Hash);

                    } catch (Exception e){
                        System.out.println("Error checking hash: " + e.getMessage());
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

	public byte[] getOrganisationKey() throws ConnexienceException {
		final Ticket ticket = getDefaultOrganisationAdminTicket();
		final Organisation defaultOrganisation = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);

		ServerObjectKeyManager mgr = new ServerObjectKeyManager();
		KeyData kd = mgr.getObjectKeyData(defaultOrganisation.getId());

		if(kd!=null){
			return kd.getKeyStoreData();
		} else {
			throw new ConnexienceException("No key exists for default organisation.");
		}
	}
}

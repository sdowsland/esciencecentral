/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.RootSecurityObjectManager;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.*;
import com.connexience.server.model.storage.DataStore;
import java.net.ConnectException;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;

/**
 * This is the base class for all the ejbs that use hibernate for object
 * persistence. This class also handles creation of log messages, as it is
 * more efficient to do this rather than look up a logging bean using
 * Naming each time a log message is required.
 *
 * @author hugo
 */
public class HibernateSessionContainer implements HibernateSessionProvider, SQLConnectionProvider {
    private static Logger logger = Logger.getLogger(HibernateSessionContainer.class);
    
    /** Internal ticket that is used by internally made calls */
    public static final Ticket INTERNAL_TICKET = new Ticket();

    static {
        INTERNAL_TICKET.setOrganisationId(Ticket.ROOT_ORGANISATION_ID);
        INTERNAL_TICKET.setId("0000");
        INTERNAL_TICKET.setSuperTicket(true);
        INTERNAL_TICKET.setStorable(false);
        INTERNAL_TICKET.setUserId("INTERNAL_TICKET");
    }

    /** Set this flag to enable / disable all news posts */
    protected static boolean enableTiming = false;

    /** Enable / disable hibernate session watching */
    protected static boolean enableSessionWatching = false;

    private long timeStore = 0L;

    /** Cache of public user IDs for organisations */
    private static final ConcurrentHashMap<String, Organisation> organisationCache = new ConcurrentHashMap<>();

    /** Cache of data stores for organisations */
    private static final ConcurrentHashMap<String, DataStore> datastoreCache = new ConcurrentHashMap<>();

    /** Cached default organisation */
    private static Organisation cachedDefaultOrganisation = null;

    /** Cached root securty object */
    private static RootSecurityObject cachedRootSecurityObject = null;

    /** Creates a new instance of HibernateSessionContainer */
    public HibernateSessionContainer() {
    }

    /** Get the internal ticket */
    protected final Ticket getInternalTicket() {
        return INTERNAL_TICKET;
    }

    /** Open a new session object */
    @Override
    public Session getSession() {
        if (enableTiming) {
            timeStore = System.currentTimeMillis();
        }
        Session session = HibernateUtil.getSessionFactory().openSession();
        if (enableSessionWatching) {
            HibernateSessionWatcher.monitor(session);
        }
        return new AutoFlushSession(session);
    }

//    protected Session getProvenanceSession() {
//        if (enableTiming) {
//            timeStore = System.currentTimeMillis();
//        }
//        Session session = HibernateUtil.getProvenanceSessionFactory().openSession();
//        if (enableSessionWatching) {
//            HibernateSessionWatcher.monitor(session);
//        }
//        return new AutoFlushSession(session);
//    }

    /** Close a session */
    @Override
    public void closeSession(Session session) {
        if (session != null) {
            try {
                if (enableTiming) {
                    Long finishTime = System.currentTimeMillis();

                    Long startTime = timeStore;

                    //reset
                    timeStore = 0L;

                    @SuppressWarnings({"ThrowableInstanceNeverThrown"})
                    Throwable t = new Throwable();
                    StackTraceElement[] elements = t.getStackTrace();

                    String callerMethodName = elements[1].getMethodName();
                    String callerClassName = elements[1].getClassName();

                    System.err.println("TIMING: " + callerClassName + ":" + callerMethodName + ", " + (finishTime - startTime));
                }
                session.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /** Close a JDBC connection */
    protected final void closeJDBConnection(Connection conn) {
        if (conn != null) {
            try {
                if (enableTiming) {
                    Long finishTime = System.currentTimeMillis();
                    Long startTime = timeStore;

                    //reset
                    timeStore = 0L;

                    @SuppressWarnings({"ThrowableInstanceNeverThrown"})
                    Throwable t = new Throwable();
                    StackTraceElement[] elements = t.getStackTrace();

                    String callerMethodName = elements[1].getMethodName();
                    String callerClassName = elements[1].getClassName();

                    System.err.println("TIMING: " + callerClassName + ":" + callerMethodName + ", ^^^" + (finishTime - startTime) + "^^^");
                }
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public ServerObject getObject(String id, Class type) throws ConnexienceException {
        if (id != null) {
            Session session = null;
            try {
                session = getSession();
                @SuppressWarnings({"JpaQlInspection"})
                Query q = session.createQuery("FROM " + type.getName() + " AS obj where obj.id= :id ");
                q.setString("id", id);
                ServerObject obj = (ServerObject) q.uniqueResult();

                if (obj != null && type.isAssignableFrom(obj.getClass())) {
                    return obj;
                } else {
                    /*
                    System.out.println("Cannot find object of type " + type.getName() + " with Id " + id);
                    @SuppressWarnings({"ThrowableInstanceNeverThrown"})
                    Throwable t = new Throwable();
                    t.printStackTrace();
                    *
                    */
                    return getObject(id);
                }
            } catch (Exception e) {
                throw new ConnexienceException("Error retrieving object: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            return null;
        }
    }

    /** Get a ServerObject by id */
    private ServerObject getObject(String id) throws ConnexienceException {
        if (id != null) {
            Session session = null;
            try {
                session = getSession();

                Query q = session.createQuery("from ServerObject as obj where obj.id=?");
                q.setString(0, id);
                return (ServerObject) q.uniqueResult();
            } catch (Exception e) {
                throw new ConnexienceException("Error retrieving object: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            return null;
        }
    }

    /** Save a general object */
    public final Object savePlainObject(Object object) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(object);
            return object;
        } catch (Exception e) {
            throw new ConnexienceException("Error saving object: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Save a general object */
    public final Object savePlainObject(Object object, Session session) throws ConnexienceException {
        try {
            session.saveOrUpdate(object);
            session.flush();
            return object;
        } catch (Exception e) {
            throw new ConnexienceException("Error saving object: " + e.getMessage());
        }
    }

    /** Save an object and perform all the access control checks */
    protected final ServerObject saveObjectWithAcl(Ticket ticket, ServerObject object) throws ConnexienceException {
        if (ticket != null) {

            // Set the right organisation ID
            if (!ticket.isSuperTicket()) {
                // Set organisation id if not present
                if (object.getOrganisationId() == null || object.getOrganisationId().isEmpty()) {
                    object.setOrganisationId(ticket.getOrganisationId());
                } else {
                    if (!object.getOrganisationId().equals(ticket.getOrganisationId())) {
                        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
                    }
                }

                // Set the creator ID if not present
                if (object.getCreatorId() == null || object.getCreatorId().isEmpty()) {
                    object.setCreatorId(ticket.getUserId());
                }

            } else {
                // If this is a super ticket, make sure that the organisation is set correctly
                if (ticket.getOrganisationId().equals(Ticket.ROOT_ORGANISATION_ID)) {
                    if (!(object instanceof Organisation)) {
                        throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
                    }
                } else {
                    if (object.getOrganisationId() == null || object.getOrganisationId().isEmpty()) {
                        object.setOrganisationId(ticket.getOrganisationId());
                    } else {
                        if (!object.getOrganisationId().equals(ticket.getOrganisationId())) {
                            throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
                        }
                    }
                }
            }

            if (object.getId() != null && !object.getId().isEmpty()) {
                // Existing object
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, object, Permission.WRITE_PERMISSION)) {
                    return saveObject(ticket, object);
                } else {
                    throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
                }

            } else {
                // New object
                if (EJBLocator.lookupAccessControlBean().canTicketAddResource(ticket, object)) {
                    return saveObject(ticket, object);
                } else {
                    throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
                }
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_USER_LOGGED_IN_MESSAGE);
        }
    }

    /** Save an object */
    protected final ServerObject saveObject(Ticket ticket, ServerObject object) throws ConnexienceException {
        Session session = null;
        try {

            session = getSession();
            if (object.getId() != null && !object.getId().isEmpty()) {
                object = (ServerObject) session.merge(object);
                session.flush();  //flush the session so that the version number is updated for logging

                return object;
            } else {
                // Set the creator ID if there is a ticket
                if (object.getCreatorId() == null || object.getCreatorId().isEmpty()) {
                    if (ticket != null) {
                        object.setCreatorId(ticket.getUserId());
                        if (ticket.isAssociatedWithNonRootOrg()) {
                            // If this is a super ticket, allow the organisation ID to be different from
                            // the ticket organisation id
                            if (ticket.isSuperTicket()) {
                                // Only set an organisation ID if there isn't already one
                                if (object.getOrganisationId() == null) {
                                    object.setOrganisationId(ticket.getOrganisationId());
                                }
                            } else {
                                object.setOrganisationId(ticket.getOrganisationId());
                            }
                        }
                    } else {
                        object.setCreatorId(INTERNAL_TICKET.getUserId());
                    }
                }


                session.persist(object);
                session.flush();

                //Set the project if it's set in the ticket
                if (ticket.getDefaultProjectId() != null) {
                    EJBLocator.lookupStorageBean().checkProjectId(ticket, object, ticket.getDefaultProjectId());
                }

                return object;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error saving object: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the organisationId of the current ticket
     *
     * @param ticket
     */
    protected final String getTicketOrgansiationId(Ticket ticket) {
        if (ticket != null) {
            return ticket.getOrganisationId();
        } else {
            return "";
        }
    }

    /**
     * Get the user id of the current ticket
     *
     * @param ticket
     */
    protected final String getTicketUserId(Ticket ticket) {
        if (ticket != null) {
            return ticket.getUserId();
        } else {
            return "";
        }
    }

    /**
     * Is the current ticket a super ticket
     *
     * @param ticket
     */
    protected final boolean isSuperTicket(Ticket ticket) {
        return ticket != null && ticket.isSuperTicket();
    }

    /**
     * Is the current ticket an organisation admin user
     *
     * @param ticket
     */
    protected final boolean isOrganisationAdminTicket(Ticket ticket) throws ConnexienceException {
        if (ticket.getUserId().equals(INTERNAL_TICKET.getUserId())) {
            return true;
        } else {
            return EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId());
        }
    }
    
    /**
     * Is a userId an admin for the default organisation
     */
    public final boolean isUserOrganisationAdmin(String userId) throws ConnexienceException {
        Organisation org = getCachedDefaultOrganisation();
        return EJBLocator.lookupUserDirectoryBean().isUserGroupMember(INTERNAL_TICKET, userId, org.getAdminGroupId());
    }

    /**
     * Get the public user for this organisation
     *
     * @param ticket
     */
    protected final User getPublicUser(Ticket ticket) throws ConnexienceException {
        try {
            String defaultUserId = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();
            return EJBLocator.lookupUserDirectoryBean().getUser(ticket, defaultUserId);
        } catch (ConnexienceException e) {
            throw new ConnexienceException("Unable to get default User", e);
        }
    }

    /**
     * Assert a specific type of access on an object. Method is silent if there
     * is a permission, and throws an Exception if denied
     */
    protected final void assertPermission(Ticket ticket, ServerObject object, String accessType) throws ConnexienceException {
        if (object != null) {
            if (ticket != null) {
                if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, object, accessType)) {
                    System.err.println("User: " + ticket.getUserId() + " denied " + accessType + " to " + object.getName() + "(" + object.getId() + ")");
                    throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
                }
            } else {
                throw new ConnexienceException(ConnexienceException.NO_USER_LOGGED_IN_MESSAGE);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
        }
    }

    /** Assert that a ticket is an administrator */
    protected final void assertAdministrator(Ticket ticket) throws ConnexienceException {
        if(!ticket.isSuperTicket()){
            if (!isOrganisationAdminTicket(ticket)) {
                throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
            }
        }
    }

    /** Narrow a list of object based on ACL restrictions */
    public List narrowListWithAcl(Ticket ticket, List originalResults) throws ConnexienceException {
        return EJBLocator.lookupAccessControlBean().filterList(originalResults, ticket, Permission.READ_PERMISSION);
    }

    /** Create an admin ticket for the default organisation */
    public final Ticket getDefaultOrganisationAdminTicket() throws ConnexienceException {
        Organisation defaultOrg = getCachedDefaultOrganisation();
        RootSecurityObject root = getCachedRootSecurityObject();
        WebTicket t = new WebTicket();
        t.setUserId(root.getRootUserId());
        t.setOrganisationId(defaultOrg.getId());
        t.setGroupIds(new String[]{defaultOrg.getAdminGroupId()});        
        t.setSuperTicket(true);
        return t;
    }

    /** Get a database connection */
    public Connection getSQLConnection() throws Exception {
        return getSQLConnection("java:jboss/datasources/ConnexienceDB");
    }

    @Override
    public Connection getSQLConnection(String datasourceName) throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup(datasourceName);
        if (enableTiming) {
            timeStore = System.currentTimeMillis();
        }
        return source.getConnection();
    }

    public Connection getProvenanceSQLConnection() throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup("java:jboss/datasources/ProvDB");
        if (enableTiming) {
            timeStore = System.currentTimeMillis();
        }
        return source.getConnection();
    }


    /** Get the public user id of an organisation */
    public String getPublicUserId(String organisationId) throws ConnexienceException {
        Organisation org = getCachedOrganisation(organisationId);
        return org.getDefaultUserId();
    }

    /** Get the admin user id for an organisation */
    public String getOrganisationAdminGroupId(String organisationId) throws ConnexienceException {
        Organisation org = getCachedOrganisation(organisationId);
        return org.getAdminGroupId();
    }

    /** Remove a public user id from the cache */
    public void removePublicUserId(String organisationId) {
        HibernateSessionContainer.organisationCache.remove(organisationId);
    }

    public void flushCaches() {
        HibernateSessionContainer.organisationCache.clear();
        HibernateSessionContainer.datastoreCache.clear();
        cachedDefaultOrganisation = null;
        cachedRootSecurityObject = null;
    }

    /** Get a cached organisation object */
    public Organisation getCachedOrganisation(String organisationId) throws ConnexienceException {
        if (HibernateSessionContainer.organisationCache.containsKey(organisationId)) {
            return HibernateSessionContainer.organisationCache.get(organisationId);
        } else {
            ServerObject obj = getObject(organisationId, Organisation.class);
            if (obj instanceof Organisation) {
                HibernateSessionContainer.organisationCache.put(organisationId, (Organisation) obj);
                return (Organisation) obj;

            } else {
                throw new ConnexienceException("Object is not an organisastion");
            }
        }
    }

    /** Is there a default organisation */
    public boolean defaultOrganisationExists(){
        if(HibernateSetup.isConfigured()){
            try {
                Organisation org = getCachedDefaultOrganisation();
                if(org!=null){
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e){
                logger.error("Error trying to access organisation: " + e.getMessage());
                return false;
            }
        } else {
            return false;
        }
    }
    
    /** Get the cached default organisation */
    public Organisation getCachedDefaultOrganisation() throws ConnexienceException {
        if (cachedDefaultOrganisation != null) {
            return cachedDefaultOrganisation;
        } else {
            cachedDefaultOrganisation = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(INTERNAL_TICKET);
            return cachedDefaultOrganisation;
        }
    }

    /** Get the cached root security object */
    public static RootSecurityObject getCachedRootSecurityObject() throws ConnexienceException {
        if (cachedRootSecurityObject != null) {
            return cachedRootSecurityObject;
        } else {
            RootSecurityObjectManager mgr = new RootSecurityObjectManager();
            cachedRootSecurityObject = mgr.getRootSecurityObject();
            return cachedRootSecurityObject;
        }
    }

    /** Get the cached root security object */


    /** Get a cached organisation data store */
    public DataStore getCachedDataStore(String organisationId) throws ConnexienceException {
        if (HibernateSessionContainer.datastoreCache.containsKey(organisationId)) {
            return HibernateSessionContainer.datastoreCache.get(organisationId);
        } else {
            Organisation org = getCachedOrganisation(organisationId);
            String storeId = org.getDataStoreId();
            ServerObject obj = getObject(storeId, DataStore.class);
            if (obj instanceof DataStore) {
                ((DataStore)obj).setWriteEnabled(EJBLocator.lookupPreferencesBean().isStorageEnabled());
                HibernateSessionContainer.datastoreCache.put(organisationId, (DataStore) obj);
                return (DataStore) obj;
            } else {
                throw new ConnexienceException("Object is not a data store");
            }
        }
    }
}

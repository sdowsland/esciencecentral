/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scanner;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.scanner.RemoteFilesystemObject;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import java.util.HashMap;
import java.util.Iterator;
import org.hibernate.Session;
import org.hibernate.Query;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.TextMessage;
import org.hibernate.SQLQuery;

/**
 * This bean provides management functions for the remote filesystem scanner
 * code.
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/ScannerBean", beanInterface = ScannerRemote.class)
public class ScannerBean extends HibernateSessionContainer implements ScannerRemote {
    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;


    @Inject
    @Named("RemoteFilesystemScannerQueue")
    private String importQueueName;
    
    @Override
    public RemoteFilesystemScanner saveScanner(Ticket ticket, RemoteFilesystemScanner scanner) throws ConnexienceException {
        if(scanner.getUserId()==null || scanner.getUserId().isEmpty()){
            scanner.setUserId(ticket.getUserId());
        }
        
        if(scanner.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){
            return (RemoteFilesystemScanner)savePlainObject(scanner);
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void removeScanner(Ticket ticket, long id) throws ConnexienceException {
        RemoteFilesystemScanner scanner = getScanner(ticket, id);
        if(scanner!=null){
            if(scanner.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){
                EJBLocator.lookupObjectRemovalBean().remove(scanner);
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        }
        
    }

    @Override
    public void resetScanner(Ticket ticket, long id) throws ConnexienceException {
        RemoteFilesystemScanner scanner = getScanner(ticket, id);
        if(scanner.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){
            Session session = null;
            try {
                session = getSession();
                SQLQuery q = session.createSQLQuery("DELETE from remotefilesystemobjects WHERE scannerid=?");
                q.setLong(0, scanner.getId());
                q.executeUpdate();            
            } catch (Exception e){
                throw new ConnexienceException("Error resetting scanner state: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public List listScanners(Ticket ticket) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from RemoteFilesystemScanner");
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing all scanners: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listScannersForTicket(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from RemoteFilesystemScanner as obj where obj.userId=:userid");
            q.setString("userid", ticket.getUserId());
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing all scanners: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public RemoteFilesystemScanner getScanner(Ticket ticket, long id) throws ConnexienceException {        
        Session session = null;
        try {    
            session = getSession();
            Query q = session.createQuery("from RemoteFilesystemScanner as obj where obj.id=:id");
            q.setLong("id", id);
            List results = q.list();
            if(results.size()>0){
                RemoteFilesystemScanner scanner = (RemoteFilesystemScanner)results.get(0);
                if(scanner.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){               
                    return scanner;
                } else {
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting scanner: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listFilesystemObjects(Ticket ticket, long scannerId) throws ConnexienceException {
        RemoteFilesystemScanner scanner = getScanner(ticket, scannerId);
        if(scanner!=null){
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("from RemoteFilesystemObject as obj where obj.scannerId=:scannerid");
                q.setLong("scannerid", scannerId);
                return q.list();
            } catch (Exception e){
                throw new ConnexienceException("Error listing filesystem object: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("No such scanner: " + scannerId);
        }
    } 

    @Override
    public RemoteFilesystemObject saveFilesystemObject(Ticket ticket, RemoteFilesystemObject fsObject) throws ConnexienceException {
        RemoteFilesystemScanner scanner = getScanner(ticket, fsObject.getScannerId());
        if(scanner.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){               
            return (RemoteFilesystemObject)savePlainObject(fsObject);
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }

    }

    @Override
    public RemoteFilesystemObject getFileSystemObjectWithoutSecurity(long fsObjectId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from RemoteFilesystemObject as obj where obj.id=:id");
            q.setLong("id", fsObjectId);
            List results = q.list();
            if(results.size()>0){
                return (RemoteFilesystemObject)results.get(0);
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting filesystem object: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public RemoteFilesystemObject saveFileSystemObjectWithoutSecurity(RemoteFilesystemObject fsObject) throws ConnexienceException {
        return (RemoteFilesystemObject)savePlainObject(fsObject);
    }

    @Override
    public void removeFileSystemObjectWithoutSecurity(long fsObjectId) throws ConnexienceException {
        RemoteFilesystemObject fsObject = getFileSystemObjectWithoutSecurity(fsObjectId);
        if(fsObject!=null){
            Session session = null;
            try {
                session = getSession();
                session.delete(fsObject);
            } catch (Exception e){
                throw new ConnexienceException("Error removing remote filesystem object: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        }
    }
    
    @Override
    public void removeFilesystemObject(Ticket ticket, long id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q1 = session.createQuery("from RemoteFilesystemObject as obj where obj.id=:id");
            q1.setLong("id", id);
            RemoteFilesystemObject fsObj = (RemoteFilesystemObject)q1.uniqueResult();
            
            Query q2 = session.createQuery("from RemoteFilesystemscanner as obj where obj.id=:id");
            q2.setLong("id", fsObj.getScannerId());
            RemoteFilesystemScanner scanner = (RemoteFilesystemScanner)q2.uniqueResult();
            if(scanner.getUserId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)){               
                session.delete(fsObj);
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } catch (Exception e){
            throw new ConnexienceException("Error removing filesystem object: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void sendUploadMessage(Ticket ticket, long fsObjectId) throws ConnexienceException {
        javax.jms.Connection connection = null;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
            javax.jms.Session jmsSession = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Queue queue = jmsSession.createQueue(importQueueName);

            MessageProducer publisher = jmsSession.createProducer(queue);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            // Create the message
            TextMessage tm = jmsSession.createTextMessage();
            tm.setText("IMPORTFILE");
            tm.setStringProperty("UserID", ticket.getUserId());
            tm.setLongProperty("ObjectID", fsObjectId);
            publisher.setTimeToLive((3600 * 1000));
            publisher.send(tm);
            
        } catch (Exception e){
            throw new ConnexienceException("Error file import message message: " + e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println("Error closing JMS connection: " + e.getMessage());
            }
        }      
    }

    @Override
    public void resendAllUploadMessages() throws ConnexienceException {
        try {
            Ticket adminTicket = getDefaultOrganisationAdminTicket();
            List scanners = listScanners(adminTicket);
            List fsObjects;
            
            RemoteFilesystemScanner scanner;
            RemoteFilesystemObject fsObject;
            
            for(Object o : scanners){
                scanner = (RemoteFilesystemScanner)o;
                fsObjects = listFilesystemObjects(adminTicket, scanner.getId());
                for(Object fso : fsObjects){
                    fsObject = (RemoteFilesystemObject)fso;
                    if(fsObject.getStatus()!=null && fsObject.getStatus().equals(RemoteFilesystemObject.QUEUED)){
                        // Only resend queued messages
                        sendUploadMessage(adminTicket, fsObject.getId());
                    }
                }
            }
        } catch (Exception e){
            throw new ConnexienceException("Error resending upload messages: " + e.getMessage(), e);
        }
    }

    @Override
    public void scanForChanges(RemoteFilesystemScanner scanner) throws ConnexienceException {
        javax.jms.Connection connection = null;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
            javax.jms.Session jmsSession = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Queue queue = jmsSession.createQueue(importQueueName);

            MessageProducer publisher = jmsSession.createProducer(queue);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            // Create the message
            TextMessage tm = jmsSession.createTextMessage();
            tm.setText("SCANCHANGES");
            tm.setStringProperty("UserID", scanner.getUserId());
            tm.setLongProperty("ScannerID", scanner.getId());
            publisher.setTimeToLive((3600 * 1000));
            publisher.send(tm);
            
        } catch (Exception e){
            throw new ConnexienceException("Error file import message message: " + e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println("Error closing JMS connection: " + e.getMessage());
            }
        }     
    }
}
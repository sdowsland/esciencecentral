/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.acl.AccessControlException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.OrganisationFolderAccess;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.LinksFolder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.security.*;
import com.connexience.server.model.social.OldProject;
import com.connexience.server.model.social.profile.GroupProfile;
import com.connexience.server.model.social.requests.JoinGroupRequest;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This EJB performs group management functions
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/GroupDirectoryBean", beanInterface = GroupDirectoryRemote.class)
public class GroupDirectoryBean extends HibernateSessionContainer implements com.connexience.server.ejb.directory.GroupDirectoryRemote {

    /**
     * Creates a new instance of GroupDirectoryBean
     */
    public GroupDirectoryBean() {
        super();
    }

    /**
     * Remove a group
     */
    public void removeGroup(Ticket ticket, String groupId) throws ConnexienceException {
        Group group = (Group) getObject(groupId, Group.class);
        if (group != null) {
            assertPermission(ticket, group, Permission.WRITE_PERMISSION);
            EJBLocator.lookupObjectRemovalBean().remove(group);
        } else {
            throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
        }
    }

    /**
     * Save a group
     */
    public Group saveGroup(Ticket ticket, Group group) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            // Assign the organisation details
            if (ticket.isAssociatedWithNonRootOrg()) {

                //if we are updating the group, check the access permissions first.  If
                if (group.getId() != null) {
                    Group dbgroup = (Group) getObject(group.getId(), Group.class);
                    if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, dbgroup, Permission.WRITE_PERMISSION)) {
                        throw new ConnexienceException("Not authorised to edit group");
                    }
                }

                group.setOrganisationId(getTicketOrgansiationId(ticket));

                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(getInternalTicket(), getTicketOrgansiationId(ticket));
                OrganisationFolderAccess folders = new OrganisationFolderAccess(org);
                group.setContainerId(folders.getGroupsFolder().getId());
                group.setCreatorId(ticket.getUserId());

                //add a profile if one doesn't exist
                if (group.getProfileId() == null) {
                    GroupProfile gp = new GroupProfile();
                    gp = (GroupProfile) savePlainObject(gp);
                    group.setProfileId(gp.getId());
                }
            } else {
                throw new ConnexienceException("Logon not associated with an organisation");
            }

            group = (Group) saveObjectWithAcl(ticket, group);


            if (group.getDataFolder() == null) {
                LinksFolder dataFolder = new LinksFolder();
                dataFolder.setContainerId(group.getId());
                dataFolder.setName(group.getName());
                dataFolder.setGroupId(group.getId());
                dataFolder = (LinksFolder) saveObject(ticket, dataFolder);

                group.setDataFolder(dataFolder.getId());
                group = (Group) saveObject(ticket, group);

                EJBLocator.lookupAccessControlBean().grantAccess(ticket, group.getId(), dataFolder.getId(), Permission.READ_PERMISSION);
                EJBLocator.lookupAccessControlBean().grantAccess(ticket, group.getId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
            }
            if (group.getEventsFolder() == null) {
                LinksFolder eventsFolder = new LinksFolder();
                eventsFolder.setContainerId(group.getId());
                eventsFolder.setName("Events");
                eventsFolder.setGroupId(group.getId());
                eventsFolder = (LinksFolder) saveObject(ticket, eventsFolder);

                group.setEventsFolder(eventsFolder.getId());
                group = (Group) saveObject(ticket, group);

                EJBLocator.lookupAccessControlBean().grantAccess(ticket, group.getId(), eventsFolder.getId(), Permission.READ_PERMISSION);
                EJBLocator.lookupAccessControlBean().grantAccess(ticket, group.getId(), eventsFolder.getId(), Permission.WRITE_PERMISSION);
            }

            //Join the Group Creator to the group
            addUserToGroup(ticket, ticket.getUserId(), group.getId());

            //grant access on the group (which is a folder) to the members of the group
            EJBLocator.lookupAccessControlBean().grantAccess(ticket, group.getId(), group.getId(), Permission.READ_PERMISSION);

            // TODO: ADD LOGGING - SAVE / CREATE GROUP
            return group;
        } finally {
            closeSession(session);
        }


    }

    /**
     * Save a group profile
     */
    public GroupProfile saveGroupProfile(Ticket ticket, GroupProfile groupProfile, String groupId) throws ConnexienceException {
        Session session = null;

        //can save the group profile if we have write access to the group
        try {
            session = getSession();
            Group group = (Group) getObject(groupId, Group.class);
            if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, group, Permission.WRITE_PERMISSION)) {
                return (GroupProfile) savePlainObject(groupProfile);
            } else {
                throw new ConnexienceException("Not authorised to update group");
            }

        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * Search for groups
     */
    public List searchForGroups(Ticket ticket, String searchText) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Group as obj where obj.name like ?");
            q.setString(0, "%" + searchText + "%");
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error searching for groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a group by Id
     */
    public Group getGroup(Ticket ticket, String groupId) throws ConnexienceException {
        Group obj = (Group) getObject(groupId, Group.class);
        if (obj != null) {
            return obj;
        } else {
            throw new ConnexienceException("Cannot retrieve group: Specified object is not a group");
        }
    }

    /**
     * Get a group by name
     */
    @Override
    public Group getGroupByName(Ticket ticket, String groupName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Group as obj where obj.name=:name");
            q.setString("name", groupName);
            List results = q.list();
            if(results.size()>0){
                Group g = (Group)results.get(0);
                assertPermission(ticket, g, Permission.READ_PERMISSION);
                return g;
            } else {
                return null;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting group by name: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    
    /**
     * Add a user to a group
     */
    public void addUserToGroup(Ticket ticket, String userId, String groupId) throws ConnexienceException {
        try {
            User user = (User) getObject(userId, User.class);
            Group group = (Group) getObject(groupId, Group.class);
            User groupAdminUser = (User) getObject(group.getCreatorId(), User.class);

            if (user != null && group != null && groupAdminUser != null) {
                //if the ticket is a superticket or if the group doesn't approve joins or the ticket is the group owner then add the user to the group
                //otherwise send a request
                if (ticket.isSuperTicket() || !group.isAdminApproveJoin() || ticket.getUserId().equals(group.getCreatorId()) || isOrganisationAdminTicket(ticket)) {
                    // Create a new group membership object for the user
                    if (!EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, userId, groupId)) {
                        Session session = getSession();
                        GroupMembership membership = new GroupMembership();
                        membership.setGroupId(groupId);
                        membership.setUserId(userId);
                        session.persist(membership);

                        closeSession(session);
                    }
                } else {
                    JoinGroupRequest jr = new JoinGroupRequest();
                    jr.setContainerId(groupAdminUser.getInboxFolderId());
                    jr.setGroupId(group.getId());
                    jr.setSenderId(user.getId());
                    jr.setRecipientId(groupAdminUser.getId());
                    jr.setName("Join Request");
                    jr.setDescription(user.getName() + " requests to join " + group.getName());
                    saveObject(ticket, jr);
                }
            } else {
                if (user == null) {
                    throw new ConnexienceException("Cannot find user: " + userId + " to add to group");
                } else if (group == null) {
                    throw new ConnexienceException("Cannot find group: " + groupId + " to add user");
                } else if (groupAdminUser == null) {
                    throw new ConnexienceException("Cannot find group admin user for group: " + groupId);
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error adding user to group: " + e.getMessage());
        }
    }

    /**
     * Remove a user from a group
     */
    public void removeUserFromGroup(Ticket ticket, String userId, String groupId) throws ConnexienceException {
        try {
            Group g = (Group) getObject(groupId, Group.class);

            //the group owner can't leave the group - they can remove it though
            if (!userId.equals(g.getCreatorId())) {
                // get the group membership object for the user
                if (EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, userId, groupId)) {
                    Session session = getSession();
                    Query q = session.createQuery("from GroupMembership as g where g.userId=? and g.groupId=?");
                    q.setString(0, userId);
                    q.setString(1, groupId);
                    GroupMembership membership = (GroupMembership) q.uniqueResult();
                    session.delete(membership);
                    closeSession(session);
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error adding user to group: " + e.getMessage());
        }

    }

    /**
     * List all of the users that are part of a specific group
     */
    public List listGroupMembers(Ticket ticket, String groupId) throws ConnexienceException {
        Group g = (Group) getObject(groupId, Group.class);
        if (g != null) {
            if (g.isNonMembersList() || EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), groupId)) {
                Session session = null;
                try {
                    session = getSession();
                    Query q2 = session.createQuery("FROM User as u WHERE u.id IN (SELECT gm.userId FROM GroupMembership AS gm WHERE gm.groupId = :groupId)");
                    q2.setString("groupId", groupId);

                    return q2.list();
                } finally {
                    closeSession(session);
                }
            } else {
                throw new AccessControlException("Non members cannot list the members of this group");
            }
        } else {
            return new ArrayList();
        }
    }

    @Override
    public int getNumberOfGroupMembers(Ticket ticket, String groupId) throws ConnexienceException {
        List gm = listGroupMembers(getInternalTicket(), groupId);
        return gm.size();
    }

    /**
     * List all of the users that are part of a specific group
     */
    public List listGroupMembers(Ticket ticket, String groupId, int start, int numResults) throws ConnexienceException {
        Group g = (Group) getObject(groupId, Group.class);
        if (g != null) {
            if (g.isNonMembersList() || EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), groupId)) {

                Session session = getSession();
                try {
                    Query q2 = session.createQuery("FROM User as u WHERE u.id IN (SELECT gm.userId FROM GroupMembership AS gm WHERE gm.groupId = :groupId)");
                    q2.setString("groupId", groupId);
                    q2.setFirstResult(start);
                    if (numResults > 0) {
                        q2.setMaxResults(numResults);
                    }
                    return q2.list();
                } finally {
                    closeSession(session);
                }
            } else {
                throw new AccessControlException("Non members cannot list the members of this group");
            }
        } else {
            return new ArrayList();
        }
    }

    /**
     * List all of the users that are part of a specific group
     */
    public Long numberOfGroupMembers(Ticket ticket, String groupId) throws ConnexienceException {
        Session session = null;
        try {
            Group g = (Group) getObject(groupId, Group.class);
            if (g != null) {
                session = getSession();
                Query q = session.createQuery("SELECT COUNT (*) from GroupMembership as g where g.groupId=?");
                q.setString(0, groupId);

                return (Long) q.list().iterator().next();
            } else {
                throw new Exception("No such group");
            }

        } catch (Exception e) {
            throw new ConnexienceException("Cannot list group members: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * List groups - left for backwards compatibility with AxisWSDirectory
     */
    public List listGroups(Ticket ticket) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q = session.createQuery("from Group as obj");
                return q.list();

            } catch (Exception e) {
                throw new ConnexienceException("Error listing groups: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /**
     * List groups - left for backwards compatibility with AxisWSDirectory
     */
    public List listNonProtectedGroups(Ticket ticket) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q = session.createQuery("from Group as obj WHERE obj.protectedGroup = FALSE ");
                return q.list();

            } catch (Exception e) {
                throw new ConnexienceException("Error listing groups: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /**
     * Get the group statistics
     */
    public HashMap getGroupStatistics(Ticket ticket, String groupId) throws ConnexienceException {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        HashMap<String, Integer> stats = new HashMap<>();
        try {
            c = getSQLConnection();

            // Number of shared documents
            s = c.prepareStatement("select count(id) from objectsflat where objecttype='DOCUMENTRECORDLINK' and containerid in (select id from objectsflat where (objecttype='LINKSFOLDER' or objecttype='GROUPEVENT') and groupid=?)");
            s.setString(1, groupId);
            r = s.executeQuery();
            if (r.next()) {
                stats.put("Documents", r.getInt(1));
            } else {
                stats.put("Documents", 0);
            }
            r.close();
            s.close();

            // Number of members
            s = c.prepareStatement("select count(id) from groupmembership where groupid=?");
            s.setString(1, groupId);
            r = s.executeQuery();
            if (r.next()) {
                stats.put("Members", r.getInt(1));
            } else {
                stats.put("Members", 0);
            }
            r.close();
            s.close();

            // Number of events
            s = c.prepareStatement("select count(id) from objectsflat where objecttype='GROUPEVENT' and groupid=?");
            s.setString(1, groupId);
            r = s.executeQuery();
            if (r.next()) {
                stats.put("Events", r.getInt(1));
            } else {
                stats.put("Events", 0);
            }
            r.close();
            s.close();
            return stats;
        } catch (Exception e) {
            throw new ConnexienceException("Error getting group statistics: " + e.getMessage(), e);
        } finally {
            try {
                r.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                s.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Change the default storage folder for a user.  Will change the Id in the ticket and the user object.
     *
     * @throws ConnexienceException
     */
    public Boolean changeDefaultStorageFolder(WebTicket ticket, String folderId) throws ConnexienceException {

        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        if (folderId == null || folderId.equals("null")) {
            ticket.setDefaultStorageFolderId(null);
            ticket.setDefaultProjectId(null);

            user.setDefaultStorageFolderId(null);
            user.setDefaultProjectId(null);

            EJBLocator.lookupUserDirectoryBean().saveUser(ticket, user);
            return true;
        } else {
            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);

            if (f != null) {
                //Must have write perms
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, f, Permission.WRITE_PERMISSION)) {

                    Study study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.parseInt(f.getProjectId()));
                    if (study.getDataFolderId().equals(folderId)) {

                        //check that the folder is either a user home folder or the data folder for a project
                        ticket.setDefaultStorageFolderId(f.getId());
                        ticket.setDefaultProjectId(String.valueOf(study.getId()));

                        user.setDefaultStorageFolderId(f.getId());
                        user.setDefaultProjectId(String.valueOf(study.getId()));

                        EJBLocator.lookupUserDirectoryBean().saveUser(ticket, user);
                        return true;
                    } else {
                        throw new ConnexienceException("Default folder must be either user home folder or project data folder");
                    }
                } else {
                    throw new ConnexienceException("User must be able to write to default storage location. User" + ticket.getUserId() + " folder: " + f.getId());
                }
            } else {
                throw new ConnexienceException("Folder with Id: " + folderId + " doesn't exist");
            }
        }
    }
}

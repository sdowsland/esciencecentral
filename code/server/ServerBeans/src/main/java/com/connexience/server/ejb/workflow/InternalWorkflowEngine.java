/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.workflow;

import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.HibernateUtil;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.util.EarExtractor;
import com.connexience.server.util.RegistryUtil;
import com.connexience.server.util.SerializationUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.api.ApiProvider;
import com.connexience.server.workflow.cloud.CloudWorkflowEngine;
import com.connexience.server.workflow.cloud.JMSAttachThread;
import com.connexience.server.workflow.cloud.WorkflowControlJMSReceiver;
import com.connexience.server.workflow.cloud.WorkflowJMSListener;
import com.connexience.server.workflow.cloud.execution.CloudWorkflowExecutionEngine;
import com.connexience.server.workflow.cloud.execution.CloudWorkflowExecutionEngineListener;
import com.connexience.server.workflow.cloud.library.ServiceLibrary;
import com.connexience.server.workflow.cloud.library.ServiceLibraryContainer;
import com.connexience.server.workflow.engine.WorkflowInvocation;
import com.connexience.server.workflow.rpc.RandomGUID;
import com.connexience.server.workflow.util.ZipUtils;
import static com.connexience.server.workflow.util.ZipUtils.copyInputStream;
import org.apache.log4j.Logger;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;
import org.hornetq.jms.client.HornetQConnectionFactory;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jms.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javax.ejb.EJBLocalHome;

/**
 * This class provides a workflow engine that sits inside the ApplicationServer
 * @author hugo
 */
@Startup
@Singleton
public class InternalWorkflowEngine extends HibernateSessionContainer implements CloudWorkflowExecutionEngineListener, MessageListener, ServiceLibraryContainer, WorkflowJMSListener {
    public static InternalWorkflowEngine ENGINE_INSTANCE = null;
    public static final boolean startInternalEngine = true;   
    
    static Logger logger = Logger.getLogger(CloudWorkflowEngine.class);
    /** API Provider that creates the appropriate type of API link objects
     * to talk to the central server. These are typically RMI links for normal
     * operation within the cloud, but can be RPC backed links when the engine
     * is being used as a debugging environment in the rich client. */
    private ApiProvider apiProvider;

    /** Cloud execution engine that runs the actual workflows */
    private CloudWorkflowExecutionEngine engine;

    /** Location to unpack services and dependencies to */
    private File libraryDirectory;

    /** JMS Message connection */
    private Connection connection = null;

    /** JMS Session */
    private Session session = null;

    /** JMS Workflow message topic */
    private Queue workflowQueue = null;

    /** JMS Workflow message consumer */
    private MessageConsumer consumer = null;

    /** Library of installed services and dependencies */
    private ServiceLibrary serviceLibrary;

    /** IS the JMS Thread attached */
    private volatile boolean jmsAttached = false;
    
    /** Thread to re-attach JMS */
    private JMSAttachThread jmsAttacher = null;
    
    /** Control JMS attacher */
    private JMSAttachThread controlAttacher = null;
    
    /** Control message receiver */
    private WorkflowControlJMSReceiver controlReceiver = null;
    
    /** Is the engine running */
    private boolean engineRunning = false;
    
    @PostConstruct
    public void init(){
        ENGINE_INSTANCE = this;
    }
    
    @PreDestroy
    public void terminate(){
        if(startInternalEngine){
            logger.info("Terminating internal workflow engine");     
            if(getExecutionEngine()!=null){
                getExecutionEngine().interactiveShutdown(0);
            }
        } else {
            logger.info("Internal engine not started, so not shutting it down");
        }
        engineRunning = false;
    }

    public InternalWorkflowEngine() {
    }

    public boolean isEngineRunning() {
        return engineRunning;
    }
    
    public void startInternalWorkflowEngine() throws RemoteException {
        // Are we using the H2 database
        boolean usingH2 = false;
        if(HibernateUtil.DATABASE_PRODUCT.equals("H2")){
            usingH2 = true;
        }
        
        // Check to see if the internal engine should be started
        if(startInternalEngine==true && usingH2==true && engineRunning==false){
            logger.info("Starting internal workflow engine");
            String baseDir = System.getProperty("jboss.home.dir") + File.separator + "esc";
            
            try {
                createWorkflowFolders(baseDir);
            } catch (Exception e){
                throw new RemoteException("Error creating workflow engine directories: " + e.getMessage());
            }
            
            try {
                Ticket t = getDefaultOrganisationAdminTicket();
                new WorkflowDeploymentUtils().recreateCoreLibrary(t, getPublicUser(t));
            } catch (Exception e){
                logger.error("Error recreating core library: " + e.getMessage());
            }

            
            setLibraryDirectory(new File(baseDir + File.separator + "workflow" + File.separator + "library"));

            apiProvider = new ApiProvider();

            apiProvider.setHostName("localhost");
            apiProvider.setHttpPort(8080);
            apiProvider.setServerContext("/workflow");        
            apiProvider.setUseJMS(false);
            
            serviceLibrary = new ServiceLibrary(this);
            String invocationDir = baseDir + File.separator + "workflow" + File.separator + "invocations";
            engine = new CloudWorkflowExecutionEngine(null, invocationDir, serviceLibrary, apiProvider, this, "IP:" + getServerIp());

            engine.setMaxConcurrentServiceInvocations(4);
            engine.setMaxConcurrentWorkflows(10);
            engine.addCloudWorkflowExecutionEngineListner(this);
            engine.setDebuggingAllowed(true);

            // Workflow lock receiver
            controlReceiver = new WorkflowControlJMSReceiver(engine);
            try {
                try {
                    RegistryUtil.unregisterFromRegistry("CloudWorkflowEngine");
                } catch (Exception e){}

                try {
                    RegistryUtil.unregisterFromRegistry("APIBroker");
                } catch (Exception e){}

                RegistryUtil.registerToRegistry("CloudWorkflowEngine", this.getExecutionEngine(), true);
                RegistryUtil.registerToRegistry("APIBroker", this.getExecutionEngine().getExecutionEngine(), true);
                try {
                    this.getExecutionEngine().getServiceLibrary().flushLibrary();
                } catch (Exception e){
                    logger.error("Error emptying library directory", e);
                }

                String hostname  = "localhost";
                int port         = 5445;
                String user      = "connexience";
                String password  = "1234";
                String queueName = "Workflow";
                String topicName = "WorkflowControl";
                // buffer size 'null' means the default value
                Integer bufSize  = 0;
                this.startJMSAttacherThread(hostname, port, user, password, queueName, topicName, bufSize);
            } catch (Exception e){
                logger.error("Error setting up internal workflow engine: " + e.getMessage(), e);
            }       
            engineRunning = true;
        } else {
            logger.info("Not starting internal workflow engine");
        }
    }

    /** Create the workflow folders */
    private void createWorkflowFolders(String baseDir) throws IOException {
        File dataDir = new File(baseDir);
        
        File workflowDir = new File(dataDir, "workflow");
        if(!workflowDir.exists()){
            logger.info("Creating workflow directory");
            workflowDir.mkdir();
        }
        
        File invocationsDir = new File(workflowDir, "invocations");
        if(!invocationsDir.exists()){
            logger.info("Creating invocations directory");
            invocationsDir.mkdir();
        }
        
        File libraryDir = new File(workflowDir, "library");
        if(!libraryDir.exists()){
            logger.info("Creating library dir");
            libraryDir.mkdir();
        }
        
        File staticDir = new File(workflowDir, "static");
        if(!staticDir.exists()){
            logger.info("Creating static dir");
            staticDir.mkdir();
        }
    }
    
    /** Get the server IP address */
    private String getServerIp() {
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
            return ip;
        } catch (Exception e){
            logger.error("Cannot access IP address information: " + e.getMessage(), e);
            logger.warn("Used localhost(127.0.0.1) for IP address");
            return "127.0.0.1";
        }
    }
    
    /** Get the actual execution engine */
    public CloudWorkflowExecutionEngine getExecutionEngine(){
        return engine;
    }

    /** Get the library directory */
    public File getLibraryDirectory(){
        return libraryDirectory;
    }

    /** Set the library directory */
    public void setLibraryDirectory(File libraryDirectory){
        this.libraryDirectory = libraryDirectory;
    }

    @Override
    public void engineShutdownSignalReceived(CloudWorkflowExecutionEngine source, boolean interactive, int exitCode) {
        logger.info("Engine shutdown signal received");
        detachJms();
        PreferenceManager.saveProperties();
        
        // Unregister from RMI registry
        try {
            RegistryUtil.unregisterFromRegistry("CloudWorkflowEngine");
        } catch (Exception e){
            logger.error("Error unregistering control RMI", e);
        }

        // Unregister from RMI registry
        try {
            RegistryUtil.unregisterFromRegistry("APIBroker");
        } catch (Exception e){
            logger.error("Error unregistering APIBroker RMI", e);
        }      
        
        // Exit
        if(!interactive){
            logger.info("Exiting");
        }
    }


    public void invocationFinished(WorkflowInvocation invocation) {
    }

    @Override
    public void invocationStarted(WorkflowInvocation invocation) {
    }
    
    /** Wait until the queue gets down to size */
    private void waitForQueueToShrink(){
        int size = engine.getJobQueueSize();
        if(size>=engine.getMaxConcurrentWorkflows()){
            while(engine.getJobQueueSize()>=engine.getMaxConcurrentWorkflows()){
                try {
                    Thread.sleep(10);
                } catch (Exception e){
                    logger.error("Thread sleep error", e);
                }
            }
        }
    }

    /** Process a JMS workflow start message */
    public void onMessage(Message message) {
        logger.info("JMS Message received");
        if(message instanceof BytesMessage){
            BytesMessage bm = (BytesMessage)message;
            try {
                bm.reset();
                byte[] data = new byte[(int)bm.getBodyLength()];
                bm.readBytes(data);
                Object payload = SerializationUtils.deserialize(data);
                if(payload instanceof WorkflowInvocationMessage){
                    WorkflowInvocationMessage invocationMessage = (WorkflowInvocationMessage)payload;
                    logger.info("Workflow Invocation message found for invocation. InvocationID=" + invocationMessage.getInvocationId());
                    engine.startWorkflow(invocationMessage);
                    message.acknowledge();
                }
            } catch (JMSException jmse){
                logger.error("JMS Exception", jmse);
            } catch (IOException ioe){
                logger.error("IO Exception", ioe);
            } catch (ClassNotFoundException cnfe){
                logger.error("Class not found", cnfe);
            }
        }
        waitForQueueToShrink();
    }

    /** Start the JMS Attachement thread */
    public void startJMSAttacherThread(String hostname, int port, String user, String password, String queueName, String lockTopicName, Integer bufferSize){
        // Start the workflow queue attacher thread
        jmsAttacher = new JMSAttachThread(this, hostname, port, user, password, queueName, bufferSize);
        jmsAttacher.start();
        
        // Start the workflow lock topic attacher thread
        controlAttacher = new JMSAttachThread(controlReceiver, hostname, port, user, password, lockTopicName, bufferSize);
        controlAttacher.start();
        controlReceiver.setJmsAttacher(controlAttacher);
    }

    public JMSAttachThread getAttacherThread() {
        return jmsAttacher;
    }

    public boolean isJmsAttached() {
        return jmsAttached;
    }
    
    /** Attach to the JMS server */
    public synchronized void attachJms(String hostname, int port, String user, String password, String queueName, Integer bufferSize) throws Exception
    {
        jmsAttached = true;
        try {
            logger.info("Attaching JMS to: "  + hostname + ":" + port + " on queue: " + queueName);
            Map<String, Object> params = new HashMap<>();
            params.put(TransportConstants.HOST_PROP_NAME, hostname);
            params.put(TransportConstants.PORT_PROP_NAME, port);

            TransportConfiguration configuration = new TransportConfiguration(NettyConnectorFactory.class.getName(), params);
            HornetQConnectionFactory factory = HornetQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.CF, configuration);
			if (bufferSize != null) {
				logger.info("Consumer window size: " + bufferSize);
				factory.setConsumerWindowSize(bufferSize.intValue());
			}
			else
				logger.info("Default consumer window size");
			
            connection = factory.createConnection(user, password);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            workflowQueue = session.createQueue(queueName);
            consumer = session.createConsumer(workflowQueue);
            consumer.setMessageListener(this);
            connection.setExceptionListener(new ExceptionListener() {

                @Override
                public void onException(JMSException jmse) {
                    if(jmse.getErrorCode().equals("DISCONNECT")){
                        logger.info("JMS Detached");
                        jmsAttached = false;
                        jmsAttacher.setInterval(10000);
                    } else {
                        logger.error("Unrecognised JMS Error code: " + jmse.getErrorCode());
                    }
                }
            });
            connection.start();
            logger.info("JMS Attached: " + queueName);
        } catch (Exception e){
            jmsAttached = false;
            throw e;
        }
    }
    
    /** Close the JMS Connection */
    public void detachJms() {
        try {
            logger.info("Detaching JMS: Workflow");
            jmsAttacher.stop();
            connection.stop();
            connection.close();
            logger.info("JMS Detached: Workflow");
            jmsAttached = false;
            
            controlReceiver.detachJms();
            
        } catch (Exception e){
            logger.error("Error detaching JMS: " + e.getMessage());
        }
    }

    /** Set the API Provider */
    public void setApiProvider(ApiProvider apiProvider){
        this.apiProvider = apiProvider;
    }

    /** Get the API Provider */
    public ApiProvider getApiProvider(){
        return apiProvider;
    }
    
    /** Copy the data from one stream to another */
    private static final void copyInputStream(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[4096];
        int len;

        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }
        out.flush();
        in.close();
    }    
    
    /** Write a file to an output stream */
    private static void copyFileToOutputStream(File file, OutputStream stream) throws IOException {
        copyInputStream(new FileInputStream(file), stream);
    }    
}
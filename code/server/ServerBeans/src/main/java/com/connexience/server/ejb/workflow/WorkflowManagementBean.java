/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.workflow;


import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.*;
import com.connexience.server.model.workflow.control.WorkflowEngineRecord;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.model.workflow.notification.WorkflowLockMember;
import com.connexience.server.util.SerializationUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.blocks.processor.DataProcessorBlock;
import com.connexience.server.workflow.util.WorkflowPreviewUploader;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;
import org.pipeline.core.drawing.BlockModel;
import org.pipeline.core.drawing.model.DefaultDrawingModel;
import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamReader;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamWriter;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlStorableDataObject;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.util.*;
/**
 * This bean provides workflow management tools for loading and saving workflows
 *
 * @author nhgh
 */
@Stateless
@EJB(name = "java:global/ejb/WorkflowManagementBean", beanInterface = WorkflowManagementRemote.class)
public class WorkflowManagementBean extends HibernateSessionContainer implements WorkflowManagementRemote {
    private static Logger logger =  Logger.getLogger(WorkflowManagementBean.class);
    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;


    @Inject
    @Named("WorkflowControlTopic")
    private String workflowControlTopicName;


    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")

    /** List all workflows owned by a ticket */
    public List listWorkflows(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowDocument as obj where obj.creatorId=?" );
            q.setString(0, ticket.getUserId());
            return narrowListWithAcl(ticket, q.list());
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error listing workflows: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Get a specific workflow document record */
    public WorkflowDocument getWorkflowDocument(Ticket ticket, String documentId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            WorkflowDocument wdoc = (WorkflowDocument) getObject(documentId, WorkflowDocument.class);
            if (wdoc != null) {
                assertPermission(ticket, wdoc, Permission.READ_PERMISSION);
                return wdoc;
            } else {
                return null;
            }
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error listing workflows: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Save a specific workflow document record */
    public WorkflowDocument saveWorkflowDocument(Ticket ticket, WorkflowDocument document) throws ConnexienceException {

        WorkflowDocument workflow = (WorkflowDocument) saveObjectWithAcl(ticket, document);
        return workflow;
    }

    /** Delete a specific workflow document record */
    public void deleteWorkflowDocument(Ticket ticket, String documentId) throws ConnexienceException {
        Session session = null;
        try {
            WorkflowDocument workflowObj = (WorkflowDocument) getObject(documentId, WorkflowDocument.class);
            assertPermission(ticket, workflowObj, Permission.WRITE_PERMISSION);
            EJBLocator.lookupObjectRemovalBean().remove(workflowObj);
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error listing workflows: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Upload some binary data for a workflow document record */
    public DocumentVersion uploadWorkflowDocumentData(Ticket ticket, String documentId, byte[] workflowDocumentData) throws ConnexienceException {
        try {
            WorkflowDocument wdoc = (WorkflowDocument) getObject(documentId, WorkflowDocument.class);
            if (wdoc != null) {
                assertPermission(ticket, wdoc, Permission.WRITE_PERMISSION);
                DocumentVersion workflowVersion = StorageUtils.upload(ticket, workflowDocumentData, wdoc, "Workflow data");

                //log that the workflow has been saved in the graphDb
                DocumentRecord workflowDoc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, workflowVersion.getDocumentRecordId());
//                WorkflowSaveOperation saveOp = new WorkflowSaveOperation(documentId, workflowVersion.getId(), workflowVersion.getVersionNumber(), workflowDoc.getName(), ticket.getUserId(), new Date(System.currentTimeMillis()));
//                IProvenanceLogger provClient = new ProvenanceLoggerClient();
//                provClient.log(saveOp);
                return workflowVersion;

            } else {
                throw new ConnexienceException("Object is not a workflow");
            }

        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        }
    }

    /** Get the binary data for a workflow document record */
    public byte[] getLatestWorkflowDocumentData(Ticket ticket, String documentId) throws ConnexienceException {
        try {
            WorkflowDocument wdoc = (WorkflowDocument) getObject(documentId, WorkflowDocument.class);
            if (wdoc != null) {
                assertPermission(ticket, wdoc, Permission.READ_PERMISSION);
                return StorageUtils.download(ticket, wdoc, null);
            } else {
                throw new ConnexienceException("Object is not a workflow");
            }
        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        }
    }

    /** Get data for a specific workflow document version */
    public byte[] getWorkflowDocumentData(Ticket ticket, String documentId, String versionId) throws ConnexienceException {
        try {
            WorkflowDocument wdoc = (WorkflowDocument) getObject(documentId, WorkflowDocument.class);
            if (wdoc != null) {
                assertPermission(ticket, wdoc, Permission.READ_PERMISSION);
                DocumentVersion version = EJBLocator.lookupStorageBean().getVersion(ticket, documentId, versionId);
                return StorageUtils.download(ticket, wdoc, version);
            } else {
                throw new ConnexienceException("Object is not a workflow");
            }
        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        }
    }

    /** Create an invocation folder */
    public WorkflowInvocationFolder createInvocationFolder(Ticket ticket, String invocationId, String workflowId, String versionId) throws ConnexienceException {

        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                WorkflowInvocationFolder folder = new WorkflowInvocationFolder();

                WorkflowDocument workflow = getWorkflowDocument(ticket, workflowId);
                Folder container = null;

                // Create a results folder
                Folder allWorkflowsFolder;
                if (ticket.getDefaultProjectId() != null) {
                    Project project = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(ticket.getDefaultProjectId()));
//                    OldProject oldProject = (OldProject) EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, ticket.getDefaultProjectId());
                    allWorkflowsFolder = EJBLocator.lookupStorageBean().getFolder(ticket, project.getWorkflowFolderId());
                } else {
                    //get the user's workflow folder
                    allWorkflowsFolder = EJBLocator.lookupStorageBean().getWorkflowFolder(ticket, ticket.getUserId());
                }

                if (allWorkflowsFolder == null) {
                    //create a workflow folder for the user
                    User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
                    allWorkflowsFolder = new Folder();
                    allWorkflowsFolder.setName("Workflows");
                    allWorkflowsFolder.setCreatorId(ticket.getUserId());
                    allWorkflowsFolder.setContainerId(user.getHomeFolderId());
                    allWorkflowsFolder.setDescription("Workflow folder for " + user.getName());
                    allWorkflowsFolder.setOrganisationId(user.getOrganisationId());
                    allWorkflowsFolder = (Folder) saveObject(ticket, allWorkflowsFolder);
                    user.setWorkflowFolderId(allWorkflowsFolder.getId());
                    saveObject(ticket, user);
                }

                //get the folder for this workflow if it exists as a sub folder of the worklfow folder
                //can't do a contains() as folder will match on more than the name
                List<Folder> childFolders = EJBLocator.lookupStorageBean().getChildFolders(ticket, allWorkflowsFolder.getId());
                for (Folder childFolder : childFolders) {
                    if (childFolder.getName().equals(workflow.getName())) {
                        container = childFolder;
                        break;
                    }
                }

                //if the container is null then there is no folder for this workflow within the workflows folder
                if (container == null) {
                    container = new Folder();
                    container.setContainerId(allWorkflowsFolder.getId());
                    container.setCreatorId(ticket.getUserId());
                    container.setDescription(workflow.getName() + " data folder");
                    container.setName(workflow.getName());
                    container.setOrganisationId(ticket.getOrganisationId());
                    container = EJBLocator.lookupStorageBean().addChildFolder(ticket, allWorkflowsFolder.getId(), container);
                }

                //add the folder that will contain this invocation
                if (container != null) {
                    folder.setContainerId(container.getId());
                    folder.setCreatorId(ticket.getUserId());
                    folder.setDescription("Workflow data folder");
                    folder.setWorkflowId(workflow.getId());
                    folder.setInvocationId(invocationId);
                    folder.setVersionId(versionId);
                    folder.setName(DateFormat.getDateTimeInstance().format(new Date()));  //add time
                    folder.setOrganisationId(ticket.getOrganisationId());
                    folder.setProjectId(ticket.getDefaultProjectId());
                    folder = (WorkflowInvocationFolder) EJBLocator.lookupStorageBean().addChildFolder(ticket, container.getId(), folder);
                    return folder;
                } else {
                    throw new ConnexienceException("Cannot locate folder to house workflow invocation data");
                }
            } catch (ConnexienceException ce) {
                throw new ConnexienceException(ce);
            } catch (Exception e) {
                throw new ConnexienceException("Error creating invocation folder: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the folder associated with an invocation */
    private WorkflowInvocationFolder getInvocationFolder2(Ticket ticket, String invocationId) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q = session.createQuery("from WorkflowInvocationFolder as obj where obj.invocationId=? ");
                q.setString(0, invocationId);

                List results = q.list();
                if (results.size() > 0) {
                    WorkflowInvocationFolder folder = (WorkflowInvocationFolder) results.get(0);
                    assertPermission(ticket, folder, Permission.READ_PERMISSION);
                    return folder;
                } else {
                    return null;
                }

            } catch (ConnexienceException ce) {
                throw new ConnexienceException(ce);
            } catch (Exception e) {
                throw new ConnexienceException("Error getting workflow folder: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }

    }

    public WorkflowInvocationFolder getInvocationFolder(Ticket ticket, String invocationId) throws ConnexienceException {
        try {
            WorkflowInvocationFolder folder = (WorkflowInvocationFolder) getObject(invocationId, WorkflowInvocationFolder.class);
            if (folder != null) {
                if (!folder.getCreatorId().equals(ticket.getUserId())) {
                    assertPermission(ticket, folder, Permission.READ_PERMISSION);
                }
                return folder;
            } else {
                return null;
            }
        } catch (Exception e) {
            // Fall back to old method
            return getInvocationFolder2(ticket, invocationId);
        }
    }

    /** Save changes to an invocation folder */
    public WorkflowInvocationFolder saveInvocationFolder(Ticket ticket, WorkflowInvocationFolder folder) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            folder = (WorkflowInvocationFolder) saveObjectWithAcl(ticket, folder);

            // Update the lock membership if required
            WorkflowEJBLocator.lookupWorkflowLockBean().updateLockMember(ticket, folder);
            return folder;
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Get the currently executing block */
    public String getCurrentBlockForInvocation(Ticket ticket, String invocationId) throws ConnexienceException {
        WorkflowInvocationFolder folder = getInvocationFolder(ticket, invocationId);
        return folder.getCurrentBlockId();
    }

    /** Get the status of an invocation */
    public int getInvocationStatus(Ticket ticket, String invocationId) throws ConnexienceException {
        WorkflowInvocationFolder folder = getInvocationFolder(ticket, invocationId);
        return folder.getInvocationStatus();
    }

    /** Get the number of invocations of a workflow */
    @Override
    public int getNumberOfInvocations(Ticket ticket, String workflowId) throws ConnexienceException {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("SELECT COUNT(id) FROM objectsflat WHERE objecttype='WORKFLOWINVOCATION' AND creatorid=? AND workflowid=? AND (invocationstatus=? OR invocationstatus=? OR invocationstatus=?)");
            s.setString(1, ticket.getUserId());
            s.setString(2, workflowId);
            s.setInt(3, WorkflowInvocationFolder.INVOCATION_RUNNING);
            s.setInt(4, WorkflowInvocationFolder.INVOCATION_WAITING);
            s.setInt(5, WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER);
            r = s.executeQuery();
            if (r.next()) {
                return r.getInt(1);
            } else {
                throw new ConnexienceException("No data returned from database");
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting number of invocations: " + e.getMessage(), e);
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }
        }
    }


    /** Get all of the invocations associated with a workflow for a user */
    @SuppressWarnings("unchecked")
    public List<WorkflowInvocationFolder> getInvocationFolders(Ticket ticket, String workflowId) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q = session.createQuery("from WorkflowInvocationFolder as obj where obj.workflowId=? and (obj.creatorId=? OR obj.projectId=?) order by obj.invocationDate");
                q.setString(0, workflowId);
                q.setString(1, ticket.getUserId());
                q.setString(2, ticket.getDefaultProjectId());

                List<?> results = q.list();
                return (List<WorkflowInvocationFolder>)narrowListWithAcl(ticket, results);
            } catch (ConnexienceException ce) {
                throw new ConnexienceException(ce);
            } catch (Exception e) {
                throw new ConnexienceException("Error getting workflow folder: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** List the available dynamic workflow services */
    public List listDynamicWorkflowServices(Ticket ticket) throws ConnexienceException {
        return EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, ticket.getUserId(), DynamicWorkflowService.class, 0, 0);
    }

    /** List all of the dynamic workflow libraries that a user has access to */
    public List listDynamicWorkflowLibraries(Ticket ticket) throws ConnexienceException {
        return EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, ticket.getUserId(), DynamicWorkflowLibrary.class, 0, 0);
    }

    /**
     * Save a new / updated dynamic workflow service. This method does not update the service.xml data
     * this is done when some new data is acutally uploaded
     */
    public DynamicWorkflowService saveDynamicWorkflowService(Ticket ticket, DynamicWorkflowService service) throws ConnexienceException {
        service = (DynamicWorkflowService) saveObjectWithAcl(ticket, service);
        return service;
    }

    /** Save a dynamic workflow library object */
    public DynamicWorkflowLibrary saveDynamicWorkflowLibrary(Ticket ticket, DynamicWorkflowLibrary library) throws ConnexienceException {
        library = (DynamicWorkflowLibrary) saveObjectWithAcl(ticket, library);

        return library;
    }

    /** Get the service XML for a dynamic workflow service */
    public String getDynamicWorkflowServiceXML(Ticket ticket, String serviceId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            DynamicWorkflowService obj = (DynamicWorkflowService) getObject(serviceId, DynamicWorkflowService.class);
            if (obj != null) {
                assertPermission(ticket, obj, Permission.READ_PERMISSION);
                DocumentVersion latestVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, serviceId);

                Query q = session.createQuery("from ServiceXML as obj where obj.serviceId=? and obj.versionId=?");
                q.setString(0, serviceId);
                q.setString(1, latestVersion.getId());
                List results = q.list();
                if (results.size() > 0) {
                    return ((ServiceXML) results.get(0)).getXmlData();
                } else {
                    throw new ConnexienceException("No service.xml found for version");
                }

            } else {
                throw new ConnexienceException("Object specified is not a workflow service");
            }
        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        } finally {
            closeSession(session);
        }
    }

    /** Get the service XML for a specific version of a dynamic workflow service */
    public String getDynamicWorkflowServiceXML(Ticket ticket, String serviceId, String versionId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            DynamicWorkflowService obj = (DynamicWorkflowService) getObject(serviceId, DynamicWorkflowService.class);
            if (obj != null) {
                assertPermission(ticket, obj, Permission.READ_PERMISSION);
                Query q = session.createQuery("from ServiceXML as obj where obj.serviceId=? and obj.versionId=?");
                q.setString(0, serviceId);
                q.setString(1, versionId);
                List results = q.list();
                if (results.size() > 0) {
                    return ((ServiceXML) results.get(0)).getXmlData();
                } else {
                    throw new ConnexienceException("No service.xml found for version");
                }

            } else {
                throw new ConnexienceException("Object specified is not a workflow service");
            }
        } catch (ConnexienceException ce) {
            throw new ConnexienceException(ce);
        } finally {
            closeSession(session);
        }
    }

    /** Shortcut for getting dynamic service xml and document version together This is used by the workflow engine and bypasses some checks */
    public Object[] getDynamicWorkflowServiceXmlAndVersion(Ticket ticket, String serviceId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DocumentVersion as dv where dv.documentRecordId=? order by dv.versionNumber desc");
            q.setString(0, serviceId);
            q.setMaxResults(1);
            DocumentVersion v = (DocumentVersion) q.uniqueResult();

            q = session.createQuery("from ServiceXML as svc where svc.serviceId=? and svc.versionId=?");
            q.setString(0, serviceId);
            q.setString(1, v.getId());
            ServiceXML svc = (ServiceXML) q.uniqueResult();
            return new Object[]{svc, v};
        } catch (Exception e) {
            throw new ConnexienceException("Error getting service xml and version: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Get a dynamic workflow service library */
    public DynamicWorkflowLibrary getDynamicWorkflowLibrary(Ticket ticket, String id) throws ConnexienceException {
        DynamicWorkflowLibrary obj = (DynamicWorkflowLibrary) getObject(id, DynamicWorkflowLibrary.class);
        if (obj != null) {
            assertPermission(ticket, obj, Permission.READ_PERMISSION);
            return obj;
        } else {
            return null;
        }
    }

    /** Get the latest version of a dynamic workflow service by ID */
    public DynamicWorkflowService getDynamicWorkflowService(Ticket ticket, String serviceId) throws ConnexienceException {
        DynamicWorkflowService obj = (DynamicWorkflowService) getObject(serviceId, DynamicWorkflowService.class);
        if (obj != null) {
            assertPermission(ticket, obj, Permission.READ_PERMISSION);
            return obj;
        } else {
            return null;
        }
    }

    /** Get a dynamic workflow service library by library name */
    public DynamicWorkflowLibrary getDynamicWorkflowLibraryByLibraryName(Ticket ticket, String libraryName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from DynamicWorkflowLibrary as obj where obj.libraryName=? ");
            q.setString(0, libraryName);
            List results = q.list();
            if (results.size() > 0) {
                DynamicWorkflowLibrary library = (DynamicWorkflowLibrary) results.get(0);
                assertPermission(ticket, library, Permission.READ_PERMISSION);
                return library;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting dynamic service library by name: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Update the service xml data for a service that has already been uploaded and change the category */
    public void updateServiceXml(Ticket ticket, String serviceId, String versionId, String newCategory) throws ConnexienceException {
        DynamicWorkflowService service = getDynamicWorkflowService(ticket, serviceId);
        assertPermission(ticket, service, Permission.WRITE_PERMISSION);
        if (service != null) {
            Session session = null;
            try {
                session = getSession();

                // Is there an existing service xml object
                ServiceXML xmlObject;
                BlockHelpHtml helpObject;

                Query q = session.createQuery("from ServiceXML as obj where obj.serviceId=? and obj.versionId=?");
                q.setString(0, serviceId);
                q.setString(1, versionId);
                List results = q.list();
                if (results.size() > 0) {
                    // Existing object
                    xmlObject = (ServiceXML) results.get(0);

                } else {
                    // New object
                    xmlObject = new ServiceXML();
                    xmlObject.setServiceId(serviceId);
                    xmlObject.setVersionId(versionId);
                }

                DocumentVersion version = EJBLocator.lookupStorageBean().getVersion(ticket, serviceId, versionId);
                InputStream stream = StorageUtils.getInputStream(ticket, service, version);
                XMLDataExtractor extractor = new XMLDataExtractor(new String[]{"service.xml", "help.html", "icon.jpg", "icon.png", "icon.gif", "README.md"});
                extractor.extractXmlData(stream);
                stream.close();

                // Block has to have a service.xml file
                if (extractor.entryPresent("service.xml")) {
                    xmlObject.setXmlData(extractor.getEntry("service.xml"));
                    savePlainObject(xmlObject);

                    // Update the category
                    try {
                        if (newCategory == null) {
                            String category = xmlObject.getCategory();
                            if (category != null) {
                                service.setCategory(category);
                                saveObject(ticket, service);
                            }
                        } else {
                            String oldCategory = xmlObject.getCategory();
                            if (oldCategory != null) {
                                xmlObject.changeCategory(newCategory);
                                xmlObject = (ServiceXML) savePlainObject(xmlObject);
                            }
                            String category = xmlObject.getCategory();
                            service.setCategory(category);
                            saveObject(ticket, service);
                        }
                    } catch (Exception ignored) {
                        //do nothing
                    }
                } else {
                    throw new ConnexienceException("Uploaded service did not contain a service.xml file");
                }

                // Save block's help if present
                if (extractor.entryPresent("README.md") || extractor.entryPresent("help.html")) {
                    q = session.createQuery("from BlockHelpHtml as obj where obj.serviceId=? and obj.versionId=?");
                    q.setString(0, serviceId);
                    q.setString(1, versionId);
                    results = q.list();
                    if (results.size() > 0) {
                        // Existing object
                        helpObject = (BlockHelpHtml) results.get(0);

                    } else {
                        // New object
                        helpObject = new BlockHelpHtml();
                        helpObject.setServiceId(serviceId);
                        helpObject.setVersionId(versionId);
                    }

                    String data = null;
                    if (extractor.entryPresent("README.md")) { // Give priority to README.md
                        try {
                            PegDownProcessor peg = new PegDownProcessor(Extensions.TABLES | Extensions.SMARTYPANTS);
                            data = peg.markdownToHtml(extractor.getEntry("README.md"));
                        } catch (Exception x) {
                            logger.warn("Service: " + serviceId + ": cannot convert README.md to HTML: " + x);
                        }
                    }
                    if (data == null) { // But use help.html if README.md is not available or cannot be converted
                        data = extractor.getEntry("help.html");
                    }

                    helpObject.setHtmlData(data);
                    savePlainObject(helpObject);
                }

                // Save the icon file if present
                if(extractor.entryPresent("icon.jpg")){
                    EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, serviceId, extractor.getEntryAsBytes("icon.jpg"), ImageData.WORKFLOW_BLOCK_ICON);
                } else if(extractor.entryPresent("icon.png")){
                    EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, serviceId, extractor.getEntryAsBytes("icon.png"), ImageData.WORKFLOW_BLOCK_ICON);
                } else if(extractor.entryPresent("icon.gif")){
                    EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, serviceId, extractor.getEntryAsBytes("icon.gif"), ImageData.WORKFLOW_BLOCK_ICON);
                }

                // Send a message to the engines to say that a library has changed
                try {
                    HashMap<String,Object> props = new HashMap<>();
                    props.put("serviceName", service.getName());
                    props.put("serviceId", service.getId());
                    props.put("serviceVersionId", version.getId());
                    sendControlTextMessage("ServiceChanged", props);                
                } catch (Exception e){
                    throw new ConnexienceException("Error sending service changed message to engines");
                }
            } catch (ConnexienceException ce) {
                throw new ConnexienceException(ce);
            } catch (IOException ioe) {
                throw new ConnexienceException("IOException reading service.xml: " + ioe.getMessage(), ioe);
            } finally {
                closeSession(session);
            }

        } else {
            throw new ConnexienceException("No such service");
        }
    }

    /** Update the service xml data for a service that has already been uploaded */
    public void updateServiceXml(Ticket ticket, String serviceId, String versionId) throws ConnexienceException {
        updateServiceXml(ticket, serviceId, versionId, null);
    }

    public void updateLibraryXml(Ticket ticket, String libraryId, String versionId) throws ConnexienceException {
        DynamicWorkflowLibrary library = getDynamicWorkflowLibrary(ticket, libraryId);
        assertPermission(ticket, library, Permission.WRITE_PERMISSION);
        if (library != null) {
            Session session = null;
            try {
                session = getSession();


                DocumentVersion version = EJBLocator.lookupStorageBean().getVersion(ticket, libraryId, versionId);
                InputStream stream = StorageUtils.getInputStream(ticket, library, version);
                XMLDataExtractor extractor = new XMLDataExtractor(new String[]{"library.xml"});
                extractor.extractXmlData(stream);
                stream.close();

                if (extractor.allDataPresent()) {
                    String libraryXml = extractor.getEntry("library.xml");
                    LibraryXmlParser parser = new LibraryXmlParser(libraryXml);
                    parser.parse();

                    library.setLibraryName(parser.getLibraryName());
                    library = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(ticket, library);
                    
                    // Send a message to the engines to say that a library has changed
                    HashMap<String,Object> props = new HashMap<>();
                    props.put("libraryName", library.getName());
                    props.put("libraryDocumentId", library.getId());
                    props.put("libraryVersionId", version.getId());
                    sendControlTextMessage("LibraryChanged", props);
                    
                } else {
                    throw new ConnexienceException("Uploaded service did not contain a library.xml file");
                }
            } catch (ConnexienceException ce) {
                throw new ConnexienceException(ce);
            } catch (IOException ioe) {
                throw new ConnexienceException("IOException reading library.xml: " + ioe.getMessage(), ioe);
            } catch (Exception e) {
                throw new ConnexienceException("Error processing library.xml: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }

        } else {
            throw new ConnexienceException("No such service");
        }
    }

    /** List all of the visible workflow engines */
    public List listDynamicWorkflowEngines(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowEngineRecord");
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing dynamic workflow engines: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Get a dynamic workflow engine by its string ID */
    private WorkflowEngineRecord getDynamicWorkflowEngineRecord(String workflowEngineId, Session session) throws ConnexienceException {
        try {
            Query q = session.createQuery("from WorkflowEngineRecord obj where obj.engineId=?");
            q.setString(0, workflowEngineId);
            List results = q.list();
            if (results.size() > 0) {
                return (WorkflowEngineRecord) results.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting workflow engine record: " + e.getMessage(), e);
        }
    }

    /** Remove a dynamic workflow engine by numberical ID */
    public void removeDynamicWorkflowEngine(long id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("DELETE from workflowengines WHERE id=?");
            q.setLong(0, id);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error removing workflow engine record: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public WorkflowEngineRecord getDynamicWorkflowEngine(Ticket ticket, long id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowEngineRecord as obj where obj.id=?");
            q.setLong(0, id);
            List results = q.list();
            if (results.size() > 0) {
                return (WorkflowEngineRecord) results.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting workflow engine record: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void createDyamicWorkflowEngineRecord(String engineId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowEngineRecord as obj where obj.engineId=?");
            q.setString(0, engineId);
            List results = q.list();
            if (results.size() == 0) {
                WorkflowEngineRecord record = new WorkflowEngineRecord();
                record.setEngineId(engineId);
                savePlainObject(record);
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting workflow engine record: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void removeDyamicWorkflowEngineRecord(String engineId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowEngineRecord as obj where obj.engineId=?");
            q.setString(0, engineId);
            List results = q.list();
            Object obj;
            while (results.size() > 0) {
                obj = results.get(0);
                results.remove(0);
                session.delete(obj);
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting workflow engine record: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Set the ip address of the engine where a workflow is executing */
    public void setDynamicWorkflowEngineForInvocation(Ticket ticket, String invocationId, String engineId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set engineid=:engineid where objecttype='WORKFLOWINVOCATION' and invocationid=:invocationid and creatorid=:creatorid");
            q.setString("engineid", engineId);
            q.setString("creatorid", ticket.getUserId());
            q.setString("invocationid", invocationId);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error updating invocation status: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Set the current block for an invocation */
    @Override
    public void setCurrentBlockForInvocation(Ticket ticket, String invocationId, String contextId, int percentComplete) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set currentblockid=?, percentcomplete=?, totalbytestostream=0, bytesstreamed=0 where objecttype='WORKFLOWINVOCATION' and invocationid=? and creatorid=? and invocationstatus!=?");
            q.setString(0, contextId);
            q.setInteger(1, percentComplete);
            q.setString(2, invocationId);
            q.setString(3, ticket.getUserId());
            
            // Only set current block if the invocation is not debugging
            q.setInteger(4, WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error updating invocation status: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void logWorkflowExecutionStarted(Ticket ticket, String invocationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set executionstarttime=? where objecttype='WORKFLOWINVOCATION' and invocationid=? and creatorid=?");
            q.setTimestamp(0, new Date());
            q.setString(1, invocationId);
            q.setString(2, ticket.getUserId());
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error setting workflow start time: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void logWorkflowComplete(Ticket ticket, String invocationId, String status) throws ConnexienceException {
         Session session = null;
        try {

            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set executionendtime=?, message=? where objecttype='WORKFLOWINVOCATION' and invocationid=? and creatorid=?");
            q.setTimestamp(0, new Date());
            q.setString(1, status);
            q.setString(2, invocationId);
            q.setString(3, ticket.getUserId());
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error setting workflow end time: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void logWorkflowDequeued(Ticket ticket, String invocationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set dequeuedtime=? where objecttype='WORKFLOWINVOCATION' and invocationid=? and creatorid=?");
            q.setTimestamp(0, new Date());
            q.setString(1, invocationId);
            q.setString(2, ticket.getUserId());
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error setting workflow start time: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Set the status of a workflow */
    public void setWorkflowStatus(Ticket ticket, String invocationId, int status, String message) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set invocationstatus=:status, message=:message where objecttype='WORKFLOWINVOCATION' and invocationid=:invocationid and creatorid=:creatorid");
            q.setString("invocationid", invocationId);
            q.setString("creatorid", ticket.getUserId());
            q.setString("message", message);
            q.setInteger("status", status);
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error updating invocation status: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }        
    }
    
    /** Do a simple database update to set the progress status of a block */
    public void setInvocationStatus(Ticket ticket, String invocationId, String contextId, long bytesToStream, long bytesStreamed) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            SQLQuery q = session.createSQLQuery("update objectsflat set streamingprogressknown=?, totalbytestostream=?, bytesstreamed=?, currentblockid=? where objecttype='WORKFLOWINVOCATION' and invocationid=? and creatorid=?");
            if (bytesToStream == 0 && bytesStreamed == 0) {
                q.setBoolean(0, false);
            } else {
                q.setBoolean(0, true);
            }
            q.setLong(1, bytesToStream);
            q.setLong(2, bytesStreamed);
            q.setString(3, contextId);
            q.setString(4, invocationId);
            q.setString(5, ticket.getUserId());
            q.executeUpdate();
        } catch (Exception e) {
            throw new ConnexienceException("Error updating invocation status: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    private void terminateInvocation(Ticket ticket, String invocationId, WorkflowTerminationItem terminationItem) throws ConnexienceException {
        WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
        terminationItem.setId(invocation.getId());
        terminationItem.setName(invocation.getName());

        boolean hasAccess = false;
        try {
            if (ticket.getUserId().equals(invocation.getCreatorId())) {
                hasAccess = true;
            } else {
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
                if (EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, ticket.getUserId(), org.getAdminGroupId())) {
                    hasAccess = true;
                }
            }
        } catch (Exception ignored) {
            hasAccess = false;
        }

        // Check it hasn't already finished
        boolean finished = false;
        if (invocation.getInvocationStatus() == WorkflowInvocationFolder.INVOCATION_FINISHED_OK || invocation.getInvocationStatus() == WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS) {
            terminationItem.setTerminatedOk(false);
            terminationItem.setMessage("Workflow has already finished");
            finished = true;
        }

        if (hasAccess) {

            // Only make the actual call if the workflow hasn't finished
            if (!finished) {
                try {
                    // Set the status in the database so that an engine will not execute this workflow
                    invocation.setInvocationStatus(WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS);
                    invocation.setMessage("Workflow terminated");
                    invocation = saveInvocationFolder(ticket, invocation);
                    
                    // Set the lock status if the invocation is in a queue
                    WorkflowEJBLocator.lookupWorkflowLockBean().updateLockMember(ticket, invocation);
                    
                    // Send the message
                    HashMap<String,Object> props = new HashMap<>();
                    props.put("InvocationID", invocation.getInvocationId());
                    sendControlTextMessage("TerminateInvocation", props);
                    terminationItem.setTerminatedOk(true);
                    terminationItem.setMessage("");
                    
                } catch (Exception e) {
                    terminationItem.setTerminatedOk(false);
                    terminationItem.setMessage("Error terminating item: " + e.getMessage());
                }
            }

            // Are there any locks left for the workflow
            List locks = null;
            try {
                locks = WorkflowEJBLocator.lookupWorkflowLockBean().listInvocationLocks(ticket, invocationId);
            } catch (Exception e) {
                terminationItem.setSubWorkflowsProcessingError(true);
                terminationItem.setSubWorkflowProcessingMessage("Error accessing subworkflow lock: " + e.getMessage());
            }

            if (locks != null) {
                WorkflowLock lock;
                WorkflowLockMember member;
                List lockMembers;
                WorkflowTerminationItem subItem;

                for (int i = 0; i < locks.size(); i++) {
                    lock = (WorkflowLock) locks.get(i);
                    lockMembers = null;
                    try {
                        lockMembers = WorkflowEJBLocator.lookupWorkflowLockBean().getLockMembers(ticket, lock.getId());
                    } catch (Exception e) {
                        terminationItem.setSubWorkflowsProcessingError(true);
                        terminationItem.setSubWorkflowProcessingMessage("Error listing subworkflow items: " + e.getMessage());
                    }

                    terminationItem.setSubWorkflowsProcessingError(false);
                    terminationItem.setSubWorkflowProcessingMessage("");

                    if (lockMembers != null) {
                        for (int j = 0; j < lockMembers.size(); j++) {
                            subItem = terminationItem.createChild();
                            try {
                                member = (WorkflowLockMember) lockMembers.get(j);
                                terminateInvocation(ticket, member.getInvocationId(), subItem);
                            } catch (Exception e) {
                                subItem.setTerminatedOk(false);
                                subItem.setMessage("Error terminating subworkflow: " + e.getMessage());
                            }
                        }
                    }
                    
                    // Remove the lock
                    WorkflowEJBLocator.lookupWorkflowLockBean().removeWorkflowLock(ticket, lock.getId());
                }
            }

        } else {
            terminationItem.setTerminatedOk(false);
            terminationItem.setMessage("User is not allowed to terminate this workflow");
        }
    }

    /** Terminate a workflow invocation */
    public WorkflowTerminationReport terminateInvocation(Ticket ticket, String invocationId) throws ConnexienceException {
        WorkflowTerminationReport report = new WorkflowTerminationReport();
        terminateInvocation(ticket, invocationId, report.getTopItem());
        return report;
    }

    /** Terminate all workflows on the system */
    @Override
    public void terminateAllWorkflows(Ticket ticket) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            // Kill all workflows that are holding a lock
            List locks = WorkflowEJBLocator.lookupWorkflowLockBean().listAllLocks(ticket);
            WorkflowLock lock;
            for(Object o : locks){
                lock = (WorkflowLock)o;
                try {
                    terminateInvocation(ticket, lock.getInvocationId());
                } catch (Exception e){
                    logger.error("Error terminating invocation: " + lock.getInvocationId() + ": " + e.getMessage());
                }
            }
            
            // Kill everything else
            List invocations = getAllInvocationFolders(ticket, -1, true);
            WorkflowInvocationFolder invocation;
            for(Object o : invocations){
                invocation = (WorkflowInvocationFolder)o;
                if(invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_RUNNING || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER){
                    if(!WorkflowEJBLocator.lookupWorkflowLockBean().isInvocationLockMember(ticket, invocation.getInvocationId())){
                        // Only kill if not in a lock
                        try {
                            terminateInvocation(ticket, invocation.getInvocationId());
                        } catch (Exception e){
                            logger.error("Error terminating invocation: " + invocation.getInvocationId() + ": " + e.getMessage());
                        }
                            
                    }
                }
            }
            
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    /** Terminate all workflows for a user */
    public void terminateAllWorkflowsForUser(Ticket ticket) throws ConnexienceException {
        // Kill all locked workflows held by this user
        List locks = WorkflowEJBLocator.lookupWorkflowLockBean().listUserLocks(ticket, ticket.getUserId());
        WorkflowLock lock;
        for(Object o : locks){
            lock = (WorkflowLock)o;
            try {
                terminateInvocation(ticket, lock.getInvocationId());
            } catch (Exception e){
                logger.error("Error terminating invocation: " + lock.getInvocationId() + ": " + e.getMessage());
            }
        }        
        
        // Kill all workflows running by themselves
        List invocations = WorkflowEJBLocator.lookupWorkflowManagementBean().getAllInvocationFoldersForUser(ticket, -1, true);
        WorkflowInvocationFolder invocation;
        for(Object o : invocations){
            invocation = (WorkflowInvocationFolder)o;
            if(invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_RUNNING || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER){
                if(!WorkflowEJBLocator.lookupWorkflowLockBean().isInvocationLockMember(ticket, invocation.getInvocationId())){
                    // Only kill if not in a lock
                    try {
                        terminateInvocation(ticket, invocation.getInvocationId());
                    } catch (Exception e){
                        logger.error("Error terminating invocation: " + invocation.getInvocationId() + ": " + e.getMessage());
                    }

                }
            }
        }        
    }
        
    /** Set the output data for a workflow invocation */
    public void updateServiceLog(Ticket ticket, String invocationId, String contextId, String outputData, String statusText, String statusMessage) throws ConnexienceException {
        //WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
        //if (invocation != null) {
        //    if (invocation.getCreatorId().equals(ticket.getUserId())) {
                WorkflowServiceLog logMessage = new WorkflowServiceLog();
                logMessage.setContextId(contextId);
                logMessage.setInvocationId(invocationId);
                logMessage.setOutputText(outputData);
                logMessage.setStatusText(statusText);
                logMessage.setStatusMessage(statusMessage);
                savePlainObject(logMessage);
        //    } else {
        //        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, invocation));
        //    }
        //}
    }

    /** Set the output data for a workflow invocation */
    public void updateServiceLogMessage(Ticket ticket, String invocationId, String contextId, String statusText, String statusMessage) throws ConnexienceException {
        //WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
        //if (invocation != null) {
        //    if (invocation.getCreatorId().equals(ticket.getUserId())) {
                WorkflowServiceLog logMessage = getServiceLog(ticket, invocationId, contextId);
                if (logMessage != null) {
                    logMessage.setStatusText(statusText);
                    logMessage.setStatusMessage(statusMessage);
                    savePlainObject(logMessage);
                } else {
                    logMessage = new WorkflowServiceLog();
                    logMessage.setContextId(contextId);
                    logMessage.setInvocationId(invocationId);
                    logMessage.setStatusText(statusText);
                    logMessage.setStatusMessage(statusMessage);
                    logMessage.setOutputText("");
                    savePlainObject(logMessage);
                }
        //    } else {
        //        throw new ConnexienceException(ConnexienceException.formatMessage(ticket, invocation));
        //    }
        //}
    }

    /** Get the service log for an block in an invocation */
    public WorkflowServiceLog getServiceLog(Ticket ticket, String invocationId, String contextId) throws ConnexienceException {
        WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
        Session session = null;
        if (invocation != null) {
            if (invocation.getCreatorId().equals(ticket.getUserId())) {
                try {
                    session = getSession();
                    Query q = session.createQuery("from WorkflowServiceLog as obj where obj.invocationId=? and obj.contextId=?");
                    q.setString(0, invocationId);
                    q.setString(1, contextId);
                    List results = q.list();
                    if (results.size() > 0) {
                        return (WorkflowServiceLog) results.get(0);
                    } else {
                        return null;
                    }
                } catch (Exception e) {
                    throw new ConnexienceException("Error accessing service log: " + e.getMessage(), e);
                } finally {
                    closeSession(session);
                }
            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, invocation));
            }
        } else {
            throw new ConnexienceException("No such execution report data");
        }
    }

    /** Get all of the service logs for an invocation */
    public List getServiceLogs(Ticket ticket, String invocationId) throws ConnexienceException {
        WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
        Session session = null;
        if (invocation != null) {
            if (invocation.getCreatorId().equals(ticket.getUserId())) {
                try {
                    session = getSession();
                    Query q = session.createQuery("from WorkflowServiceLog as obj where obj.invocationId=?");
                    q.setString(0, invocationId);
                    return q.list();
                } catch (Exception e) {
                    throw new ConnexienceException("Error accessing service log: " + e.getMessage(), e);
                } finally {
                    closeSession(session);
                }
            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, invocation));
            }
        } else {
            throw new ConnexienceException("No such execution report data");
        }
    }


    public BlockHelpHtml getDynamicServiceHelp(Ticket ticket, String serviceId, String versionId) throws ConnexienceException {
        if (versionId == null) {
            DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, serviceId);
            versionId = version.getId();
        }
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from BlockHelpHtml as obj where obj.serviceId=? and obj.versionId=?");
            q.setString(0, serviceId);
            q.setString(1, versionId);
            List results = q.list();
            if (results.size() > 0) {
                return (BlockHelpHtml) results.get(0);
            } else {
                BlockHelpHtml html = new BlockHelpHtml();
                html.setServiceId(serviceId);
                html.setVersionId(versionId);
                html.setHtmlData("<h1>No help available</h1>");
                return html;
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting service help information", e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List getAllInvocationFolders(Ticket ticket, int filterStatus, boolean filterSuceeded) throws ConnexienceException {

        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q;
                if (filterSuceeded) {
                    if (filterStatus != -1) {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.invocationStatus=? and obj.invocationStatus <>" + WorkflowInvocationFolder.INVOCATION_FINISHED_OK + " order by obj.invocationDate");
                        q.setInteger(0, filterStatus);
                    } else {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.invocationStatus <>" + WorkflowInvocationFolder.INVOCATION_FINISHED_OK + " order by obj.invocationDate");
                    }
                } else {
                    if (filterStatus != -1) {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.invocationStatus=? order by obj.invocationDate");
                        q.setInteger(0, filterStatus);
                    } else {
                        q = session.createQuery("from WorkflowInvocationFolder as obj order by obj.invocationDate");
                    }
                }
                return q.list();

            } catch (Exception e) {
                throw new ConnexienceException("Error getting workflow folder: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }        
    }

    
    @Override
    public List getAllInvocationFoldersForUser(Ticket ticket, int filterStatus, boolean filterSuceeded) throws ConnexienceException {

        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q;
                if (filterSuceeded) {
                    if (filterStatus != -1) {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.creatorId=? and obj.invocationStatus=? and obj.invocationStatus <>" + WorkflowInvocationFolder.INVOCATION_FINISHED_OK + " order by obj.invocationDate");
                        q.setString(0, ticket.getUserId());
                        q.setInteger(1, filterStatus);
                    } else {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.creatorId=? and obj.invocationStatus <>" + WorkflowInvocationFolder.INVOCATION_FINISHED_OK + " order by obj.invocationDate");
                        q.setString(0, ticket.getUserId());
                    }
                } else {
                    if (filterStatus != -1) {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.creatorId=? and obj.invocationStatus=? order by obj.invocationDate");
                        q.setString(0, ticket.getUserId());
                        q.setInteger(1, filterStatus);
                    } else {
                        q = session.createQuery("from WorkflowInvocationFolder as obj where obj.creatorId=? order by obj.invocationDate");
                        q.setString(0, ticket.getUserId());
                    }
                }
                return q.list();

            } catch (Exception e) {
                throw new ConnexienceException("Error getting workflow folder: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /**
     * Get the workflow statistics for a partocular user that can be displayed on the front page
     *
     * @param ticket Ticket for the user who is logged in
     * @return HashMap that has the status and number of workflows in that status
     * @throws ConnexienceException Something went wrong
     */
    public HashMap<String, Long> getWorkflowStats(Ticket ticket) throws ConnexienceException {
        Session session = null;
        HashMap<String, Long> resultMap = new HashMap<>();
        try {
            session = getSession();
            Query q = session.createQuery("SELECT w.invocationStatus, count(w.invocationStatus) FROM WorkflowInvocationFolder AS w WHERE w.creatorId = :userId GROUP BY w.invocationStatus");
            q.setString("userId", ticket.getUserId());
            @SuppressWarnings("unchecked")
            List<Object[]> results = q.list();
            for (Object[] result : results) {
                if (result[0].equals(WorkflowInvocationFolder.INVOCATION_FINISHED_OK)) {
                    resultMap.put("Success", (Long) result[1]);
                } else if (result[0].equals(WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS)) {
                    resultMap.put("Failed", (Long) result[1]);
                } else if (result[0].equals(WorkflowInvocationFolder.INVOCATION_RUNNING)) {
                    resultMap.put("Running", (Long) result[1]);
                } else if (result[0].equals(WorkflowInvocationFolder.INVOCATION_WAITING)) {
                    resultMap.put("Waiting", (Long) result[1]);
                }

            }
            return resultMap;
        } catch (Exception e) {
            throw new ConnexienceException("Error getting stats folder: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public SerializedInvocationMessage getSerializedMessage(Ticket ticket, String invocationId) throws ConnexienceException {
        WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
        if (invocation != null) {
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("FROM SerializedInvocationMessage as msg where msg.invocationId=?");
                q.setParameter(0, invocationId);
                return (SerializedInvocationMessage) q.uniqueResult();
            } catch (Exception e) {
                throw new ConnexienceException("Error getting serialized invocation message: " + e.getMessage(), e);
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException("No such invocation");
        }
    }

    @Override
    public void deleteServiceLogs(Ticket ticket, String invocationId) throws ConnexienceException {
        Session session = null;
        try {
            WorkflowInvocationFolder invocation = getInvocationFolder(ticket, invocationId);
            if (invocation.getCreatorId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)) {
                session = getSession();
                SQLQuery q = session.createSQLQuery("DELETE from workflowservicelogs WHERE invocationid=?");
                q.setString(0, invocationId);
                q.executeUpdate();
            } else {
                throw new Exception("Access denied");
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error deleting service logs: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listWorkflowEngineIds(Ticket ticket) throws ConnexienceException {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;

        if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
            try {
                c = getSQLConnection();
                s = c.prepareStatement("select distinct engineid from objectsflat where objecttype='WORKFLOWINVOCATION'");
                r = s.executeQuery();
                ArrayList<String> results = new ArrayList<>();
                while (r.next()) {
                    results.add(r.getString(1));
                }
                return results;

            } catch (Exception e) {
                throw new ConnexienceException("Error listing workflow engines: " + e.getMessage(), e);
            } finally {
                try {
                    r.close();
                } catch (Exception e) {
                }
                try {
                    s.close();
                } catch (Exception e) {
                }
                try {
                    c.close();
                } catch (Exception e) {
                }
            }

        } else {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }
    }

    @Override
    public void deleteWorkflowFolderTrigger(Ticket ticket, long triggerId) throws ConnexienceException {
        WorkflowFolderTrigger trigger = getWorkflowFolderTrigger(ticket, triggerId);
        EJBLocator.lookupObjectRemovalBean().remove(trigger);
    }

    @Override
    public List listFolderTriggers(Ticket ticket, String folderId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowFolderTrigger as obj where obj.folderId=?"); // and obj.ownerId=?");
            q.setString(0, folderId);
            q.setString(1, ticket.getUserId());
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing workflow folder triggers: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listUserTriggers(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowFolderTrigger as obj where obj.ownerId=?");
            q.setString(0, ticket.getUserId());
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing workflow folder triggers: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listAllTriggers(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowFolderTrigger as obj");
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing workflow folder triggers: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }


    @Override
    public void runTriggersForDocument(Ticket ticket, DocumentRecord document) throws ConnexienceException {
        try {
            List triggers = WorkflowEJBLocator.lookupWorkflowManagementBean().listAllTriggers(ticket);

            WorkflowFolderTrigger trigger;
            for(int i=0;i<triggers.size();i++){
                trigger = (WorkflowFolderTrigger)triggers.get(i);

                // Does the name match the trigger
                if(trigger.isEnabled()){
                    if(trigger.nameMatches(document.getName())){
                        // Is this a subdirectory
                        if(trigger.isSubdirectoryTrigger()){
                            // Check to see if the container of the document is contained in the
                            // trigger folder hierarchy
                            List hierarchy = EJBLocator.lookupStorageBean().getParentHierarchy(ticket, document.getContainerId());
                            boolean inHierarchy = false;
                            Folder f;
                            for(int j=0;j<hierarchy.size();j++){
                                f = (Folder)hierarchy.get(j);
                                if(f.getId().equals(trigger.getFolderId())){
                                    inHierarchy = true;
                                }
                            }

                            // Run workflow
                            if(inHierarchy){
                                runWorkflow(ticket, trigger.getWorkflowId(), document);
                            }
                        } else {
                            // Only check this directory
                            if(trigger.getFolderId().equals(document.getContainerId())){
                                // Run the workflow
                                runWorkflow(ticket, trigger.getWorkflowId(), document);
                            }
                        }
                    }
                }
            }

        } catch (Exception e){
            System.out.println("Error running workflow triggers: " + e.getMessage());
        }

    }

    private void runWorkflow(Ticket ticket, String workflowId, DocumentRecord document) throws ConnexienceException {
        WorkflowInvocationMessage startMessage = new WorkflowInvocationMessage();
        WorkflowDocument workflowDoc = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, workflowId);
        
        // Check to see if the container has a project ID
        Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, document.getContainerId());

        Ticket clonedTicket = null;
        try {
            clonedTicket = (Ticket)SerializationUtils.deserialize(SerializationUtils.serialize(ticket));
        } catch (Exception e){
            throw new ConnexienceException("Error copying ticket in runWorkflow: " + e.getMessage(), e);
        }
        
        if(parent!=null && parent.getProjectId()!=null && !parent.getProjectId().isEmpty()){
            int projectId = Integer.parseInt(parent.getProjectId());
            Project p = EJBLocator.lookupProjectsBean().getProject(ticket, projectId);
            if(p!=null){
                clonedTicket.setDefaultProjectId(parent.getProjectId());
                clonedTicket.setDefaultStorageFolderId(p.getDataFolderId());
            }
        }
        
        startMessage.setTargetFileId(document.getId());
        startMessage.setUseLatest(true);
        startMessage.setWorkflowId(workflowId);
        startMessage.setInputBlockName(workflowDoc.getExternalDataBlockName());
        startMessage.setTicket(clonedTicket);
        WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(clonedTicket, startMessage);
    }

    @Override
    public WorkflowFolderTrigger saveWorkflowFolderTrigger(Ticket ticket, WorkflowFolderTrigger trigger) throws ConnexienceException {
        try {
            if (trigger.getOwnerId() != null && !trigger.getOwnerId().isEmpty()) {
                // Already has owner ID
                if (trigger.getOwnerId().equals(ticket.getUserId()) || isOrganisationAdminTicket(ticket)) {
                    return (WorkflowFolderTrigger) savePlainObject(trigger);
                } else {
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
            } else {
                trigger.setOwnerId(ticket.getUserId());
                return (WorkflowFolderTrigger) savePlainObject(trigger);
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error deleting workflow folder trigger: " + e.getMessage());
        }
    }

    @Override
    public WorkflowFolderTrigger getWorkflowFolderTrigger(Ticket ticket, long triggerId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowFolderTrigger as obj where obj.id=?");
            q.setLong(0, triggerId);
            List r = q.list();
            if (r.size() > 0) {
                WorkflowFolderTrigger t = (WorkflowFolderTrigger) r.get(0);
                if (t.getOwnerId().equals(ticket.getUserId())) {
                    return t;
                } else {
                    if (isOrganisationAdminTicket(ticket)) {
                        return t;
                    } else {
                        throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
                    }
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error deleting workflow folder trigger: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Replace the IDs of services and documents within a workflow.
     * Replaces
     *
     * 1) service ID if it is in the Map.
     * 2) DocumentWrapper and FolderWrapper if they are properties of a service
     * */
    public void replaceWorkflowServiceIds(Ticket ticket, WorkflowDocument workflow, HashMap<String, String> serviceIdsMap) throws ConnexienceException {
        try {

            //Get an in memory representation of the workflow
            XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(StorageUtils.getInputStream(ticket, workflow, null));
            XmlDataStore drawingData = reader.read();
            DefaultDrawingModel drawing = new DefaultDrawingModel();
            drawing.recreateObject(drawingData);


            Enumeration blocks = drawing.blocks();
            BlockModel block;
            DataProcessorBlock processor;
            boolean changed = false;
            XmlDataStore props;
            XmlDataObject property;
            Enumeration propertyEnum;

            while (blocks.hasMoreElements()) {
                block = (BlockModel) blocks.nextElement();
                if (block instanceof DataProcessorBlock) {
                    processor = (DataProcessorBlock) block;

                    // Fix the service ID
                    if (serviceIdsMap.containsKey(processor.getServiceId())) {
                        processor.setServiceId(serviceIdsMap.get(processor.getServiceId()));
                        changed = true;
                    }

                    // Search properties for any document id / folder id references
                    props = processor.getEditableProperties();
                    propertyEnum = props.elements();
                    while (propertyEnum.hasMoreElements()) {
                        property = (XmlDataObject) propertyEnum.nextElement();
                        if (property instanceof XmlStorableDataObject) {
                            Object value = ((XmlStorableDataObject) property).getValue();

                            //Change the Document which a property points to
                            if (value instanceof DocumentRecord) {
                                String oldId = ((DocumentRecord) value).getId();

                                if (serviceIdsMap.containsKey(oldId)) {
                                    ((DocumentRecord) value).setId(serviceIdsMap.get(oldId));
                                    ((XmlStorableDataObject) property).repersistObject();
                                }
                            } else if (value instanceof Folder) {

                                //Change the folder which an ID points to
                                String oldId = ((Folder) value).getId();
                                if (serviceIdsMap.containsKey(oldId)) {
                                    ((Folder) value).setId(serviceIdsMap.get(oldId));
                                    ((XmlStorableDataObject) property).repersistObject();
                                }
                            }
                        }
                    }
                }
            }

            //If anything has changed, create a new version of the workflow
            if (changed) {
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                XmlDataStoreStreamWriter writer = new XmlDataStoreStreamWriter(drawing.storeObject());
                writer.write(outStream);
                StorageUtils.upload(ticket, outStream.toByteArray(), workflow, "Replaced service IDs");
            }
        } catch (XmlStorageException xmlse) {
            throw new ConnexienceException("Error loading drawing data", xmlse);
        }
    }


    /** Get an image for the workflow */
    public void updateWorkflowImage(Ticket ticket, String workflowId) throws ConnexienceException {
        WorkflowPreviewUploader imageGenerator = new WorkflowPreviewUploader(ticket, workflowId);
        imageGenerator.generatePreview();
    }

    /** Clear out the list of workflow engines and send a rebuild message to the workflow control
     * topic listeners */
    public void reRegisterWorkflowEngines(Ticket ticket) throws ConnexienceException {
        assertAdministrator(ticket);

        // Remove all of the workflow engine records
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("delete from WorkflowEngineRecord");
            q.executeUpdate();
        } catch (Exception e){
            throw new ConnexienceException("Error removing workflow engine records");
        } finally {
            closeSession(session);
        }

        // Send the message
        try {
            sendControlTextMessage("ReRegister", null);
        } catch (Exception e){
            throw new ConnexienceException("Error sending re-register message");
        }      
    }

    @Override
    public void resetEngineRmiCommunications(Ticket ticket) throws ConnexienceException {
        assertAdministrator(ticket);
        try {
            sendControlTextMessage("ResetRMIComms", null);
        } catch (Exception e){
            throw new ConnexienceException("Error sending RMI reset message");
        }
    }

    @Override
    public void resetEngineRmiCommumications() throws ConnexienceException {
        try {
            sendControlTextMessage("ResetRMIComms", null);
        } catch (Exception e){
            throw new ConnexienceException("Error sending RMI reset message");
        }
    }
    
    /** Send a control text message to all workflow engines */
    private void sendControlTextMessage(String text, HashMap<String,Object> props) throws Exception {
        javax.jms.Connection connection = null;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
            javax.jms.Session jmsSession = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Topic topic = jmsSession.createTopic(workflowControlTopicName);

            MessageProducer publisher = jmsSession.createProducer(topic);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            // Create the message
            TextMessage tm = jmsSession.createTextMessage();
            tm.setText(text);

            if(props!=null){
                Iterator<String> names = props.keySet().iterator();
                String name;
                Object value;
                while(names.hasNext()){
                    name = names.next();
                    value = props.get(name);
                    if(value instanceof Integer){
                        tm.setIntProperty(name, (Integer)value);
                        
                    } else if(value instanceof Long){
                        tm.setLongProperty(name, (Long)value);
                        
                    } else if(value instanceof String){
                        tm.setStringProperty(name, (String)value);
                        
                    } else if(value instanceof Double){
                        tm.setDoubleProperty(name, (Double)value);
                        
                    } else if(value instanceof Boolean){
                        tm.setBooleanProperty(name, (Boolean)value);
                        
                    } else {
                        logger.info("Unsupported ControlMessage property: " + name);
                    }
                    
                }
            }

            publisher.setTimeToLive((3600 * 1000));
            publisher.send(tm);
        } catch (Exception e){
            throw new ConnexienceException("Error sending control message: " + e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println("Error closing JMS connection: " + e.getMessage());
            }
        }           
    }
    
    @Override
    public void workflowTerminatedByEngine(String invocationId) throws ConnexienceException {
        logger.info("Workflow: " + invocationId + " terminated by engine");
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(getDefaultOrganisationAdminTicket(), invocationId);
        if(invocation!=null){
            // Do the operations as the workflow owner
            Ticket ticket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(invocation.getCreatorId());
            if(ticket!=null){
                // Does this invocation hold a lock
                List locks = WorkflowEJBLocator.lookupWorkflowLockBean().listInvocationLocks(ticket, invocationId);
                if(locks.size()>0){
                    // Terminate again so that the lock members are also killed
                    WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, invocationId);
                }
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }   
        }
    }    

    @Override
    public void flushEngineInformationCaches(Ticket ticket) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            try {
                sendControlTextMessage("ClearInformationCache", new HashMap<String, Object>());
            } catch (Exception e){
                throw new ConnexienceException("Error flushing engine information caches", e);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void refetchServerProperties(Ticket ticket) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            try {
                sendControlTextMessage("FetchServerProperties", new HashMap<String, Object>());
            } catch (Exception e){
                throw new ConnexienceException("Error Resending server properties", e);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void recreateCoreLibrary(Ticket ticket) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            new WorkflowDeploymentUtils().recreateCoreLibrary(ticket, getPublicUser(ticket));
            flushEngineInformationCaches(ticket);
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public List listAllVisibleExternallyCallableWorkflows(Ticket ticket) throws ConnexienceException {
        List results = EJBLocator.lookupObjectDirectoryBean().getAllObjectUserHasAccessToWithAdditionalConditions(ticket, ticket.getUserId(), WorkflowDocument.class, 0, 0, "so.externalService=true");
        return results;
    }
    
    @Override
    public List listExternallyCallableWorkflows(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from WorkflowDocument as obj where obj.creatorId=:creatorid and obj.externalService=true");
            q.setString("creatorid", ticket.getUserId());
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing externally callable workflows: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public HashMap<String, Class> getExternallyCallableWorkflowParameters(Ticket ticket, String workflowId) throws ConnexienceException {
        return getExternallyCallableWorkflowParameters(ticket, workflowId, null);
    }

    @Override
    public List<CallableWorkflowParameterDescription> getExternallyCallableWorkflowParameterDescriptions(Ticket ticket, String workflowId, String versionId) throws ConnexienceException {
        WorkflowDocument doc = getWorkflowDocument(ticket, workflowId);
        if(doc!=null){
            DefaultDrawingModel drawing = null;
            try {
                DocumentVersion version = null;
                if (versionId != null) {
                    version = EJBLocator.lookupStorageBean().getVersion(ticket, doc.getId(), versionId);
                }
                //Get an in memory representation of the workflow
                XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(StorageUtils.getInputStream(ticket, doc, version));
                XmlDataStore drawingData = reader.read();
                drawing = new DefaultDrawingModel();
                drawing.recreateObject(drawingData);
            } catch (Exception e){
                throw new ConnexienceException("Error parsing workflow data: " + e.getMessage(), e);
            }

            ArrayList<CallableWorkflowParameterDescription> requiredParameters = new ArrayList<>();
            Enumeration blocks = drawing.blocks();
            BlockModel block;
            DataProcessorBlock processor;
            Enumeration properties;
            XmlDataObject property;

            while(blocks.hasMoreElements()){
                block = (BlockModel)blocks.nextElement();
                if(block instanceof DataProcessorBlock){
                    processor = (DataProcessorBlock)block;
                    properties = processor.getEditableProperties().elements();
                    while(properties.hasMoreElements()){
                        property = (XmlDataObject)properties.nextElement();
                        if(property.isExposedProperty()){
                            CallableWorkflowParameterDescription d = new CallableWorkflowParameterDescription();
                            d.setName(property.getExposedName());
                            d.setDescription(property.getDescription());
                            if(property instanceof XmlStorableDataObject){
                                // Complex property
                                try {
                                    d.setComplexParameter(true);
                                    d.setParameterClass(Class.forName(((XmlStorableDataObject)property).getClassName()));
                                    d.setDefaultValue("");
                                    d.setOptionListPresent(false);
                                    requiredParameters.add(d);
                                } catch (Exception e){}
                            } else {
                                // Simple property
                                try {
                                    d.setParameterClass(property.getValue().getClass());
                                    d.setComplexParameter(false);

                                    // Sort out the default / current value
                                    if(property.getDefaultValue()!=null && !property.getDefaultValue().toString().isEmpty()){
                                        d.setDefaultValue(property.getDefaultValue().toString());
                                    } else {
                                        // Use the current value if there is one
                                        if(property.getValue()!=null){
                                            d.setDefaultValue(property.getValue().toString());
                                        } else {
                                            d.setDefaultValue("");
                                        }
                                        
                                    }

                                    // Options
                                    if(property.hasOptions()){
                                        d.setOptions(property.getOptions());
                                        d.setOptionListPresent(true);
                                    } else {
                                        d.setOptionListPresent(false);
                                    }
                                    requiredParameters.add(d);
                                } catch (Exception e){
                                        d.setDefaultValue("");
                                }
                            }
                            
                        }
                    }
                }
            }
            return requiredParameters;
        } else {
            throw new ConnexienceException("No such workflow: " + workflowId);
        }
    }

    @Override
    public List<CallableWorkflowParameterDescription> getExternallyCallableWorkflowParameterDescriptions(Ticket ticket, String workflowId) throws ConnexienceException {
        return getExternallyCallableWorkflowParameterDescriptions(ticket, workflowId, null);
    }

    @Override
    public HashMap<String, Class> getExternallyCallableWorkflowParameters(Ticket ticket, String workflowId, String versionId) throws ConnexienceException {
        WorkflowDocument doc = getWorkflowDocument(ticket, workflowId);
        if(doc!=null){
            DefaultDrawingModel drawing = null;
            try {
                DocumentVersion version = null;
                if (versionId != null) {
                    version = EJBLocator.lookupStorageBean().getVersion(ticket, doc.getId(), versionId);
                }
                //Get an in memory representation of the workflow
                XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(StorageUtils.getInputStream(ticket, doc, version));
                XmlDataStore drawingData = reader.read();
                drawing = new DefaultDrawingModel();
                drawing.recreateObject(drawingData);
            } catch (Exception e){
                throw new ConnexienceException("Error parsing workflow data: " + e.getMessage(), e);
            }

            HashMap<String, Class> requiredParameters = new HashMap<>();
            Enumeration blocks = drawing.blocks();
            BlockModel block;
            DataProcessorBlock processor;
            Enumeration properties;
            XmlDataObject property;

            while(blocks.hasMoreElements()){
                block = (BlockModel)blocks.nextElement();
                if(block instanceof DataProcessorBlock){
                    processor = (DataProcessorBlock)block;
                    properties = processor.getEditableProperties().elements();
                    while(properties.hasMoreElements()){
                        property = (XmlDataObject)properties.nextElement();
                        if(property.isExposedProperty()){
                            if(property instanceof XmlStorableDataObject){
                                // Complex property
                                try {
                                    requiredParameters.put(property.getExposedName(), Class.forName(((XmlStorableDataObject)property).getClassName()));
                                } catch (Exception e){}
                            } else {
                                // Simple property
                                requiredParameters.put(property.getExposedName(), property.getValue().getClass());
                            }
                        }
                    }
                }
            }
            return requiredParameters;
        } else {
            throw new ConnexienceException("No such workflow: " + workflowId);
        }
    }


    @Override
    public HashMap<String, WorkflowExtParameterDesc> getExternallyCallableWorkflowParametersEx(Ticket ticket, String workflowId, String versionId) throws ConnexienceException
    {
        WorkflowDocument doc = getWorkflowDocument(ticket, workflowId);
        if (doc == null) {
            throw new ConnexienceException("No such workflow: " + workflowId);
        }

        DefaultDrawingModel drawing;
        try {
            DocumentVersion version = null;
            if (versionId != null) {
                version = EJBLocator.lookupStorageBean().getVersion(ticket, doc.getId(), versionId);
            }
            //Get an in memory representation of the workflow
            XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(StorageUtils.getInputStream(ticket, doc, version));
            XmlDataStore drawingData = reader.read();
            drawing = new DefaultDrawingModel();
            drawing.recreateObject(drawingData);
        } catch (Exception e){
            throw new ConnexienceException("Error parsing workflow data: " + e.getMessage(), e);
        }

        HashMap<String, WorkflowExtParameterDesc> returnValue = new HashMap<>();
        Enumeration blocks = drawing.blocks();

        while (blocks.hasMoreElements()) {
            BlockModel block = (BlockModel)blocks.nextElement();
            if (block instanceof DataProcessorBlock) {
                DataProcessorBlock processor = (DataProcessorBlock)block;
                Enumeration properties = processor.getEditableProperties().elements();
                while (properties.hasMoreElements()) {
                    XmlDataObject property = (XmlDataObject)properties.nextElement();
                    if (property.isExposedProperty()) {
                        WorkflowExtParameterDesc desc =
                                new WorkflowExtParameterDesc(
                                        property.getExposedName(),
                                        null,
                                        property.getValue(),
                                        property.getDescription(),
                                        property.getOptions());

                        if (property instanceof XmlStorableDataObject) {
                            // Complex property
                            try {
                                desc.setType(Class.forName(((XmlStorableDataObject)property).getClassName()));
                            } catch (Exception e) {
                                logger.error(e);
                            }
                        } else {
                            // Simple property
                            desc.setType(property.getValue().getClass());
                        }

                        if (returnValue.containsKey(property.getExposedName())) {
                            logger.warn(String.format("Duplicated exposed name %s for block %s", property.getExposedName(), block.getName()));
                        } else {
                            returnValue.put(property.getExposedName(), desc);
                        }
                    }
                }
            }
        }

        return returnValue;
    }


    @Override
    public void terminateWorkflowEngines(Ticket ticket, int exitCode, boolean waitForWorkflows) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            try {
                HashMap<String,Object> params = new HashMap<>();
                params.put("waitForWorkflows", waitForWorkflows);
                params.put("exitCode", exitCode);
                sendControlTextMessage("ExitEngine", params);
            } catch (Exception e){
                throw new ConnexienceException("Error exiting workflow engines: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    
}
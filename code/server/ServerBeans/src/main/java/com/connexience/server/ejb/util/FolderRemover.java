/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.util;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.storage.DataStore;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * This class removes folders and their contents from the system. The following
 * sequence of events is needed to remove a folder:
 * 
 * 1) List the ids and types of everything that is a child of the folder
 * 2) For each document, delete all of the versions and all of the links etc
 * 3) For each folder, recurse and continue
 * 
 * @author hugo
 */
public class FolderRemover {
    /** Folder to remove */
    private Folder folder;

    /** Organisation data store containing everything */
    private DataStore store;
    
    /** Database connection */
    Connection c = null;
    
    /** List of actual document files to delete if the query works ok */
    private ArrayList<String> documentsToDelete = new ArrayList<>();
    
    /** Folder contents query */
    PreparedStatement documentsQuery;
    
    /** Folder document removal query */
    PreparedStatement documentRemovalQuery;
    
    /** Subfolders query */
    PreparedStatement subfoldersQuery;
    
    /** Query to remove subfolders */
    PreparedStatement subfoldersRemovalQuery;
    
    /** Delete all document versions contained in a folder query */
    PreparedStatement versionRemovalQuery;
    
    /** Query to remove all tags from folder contents */
    PreparedStatement tagRemovalQuery;
    
    /** Query to remove all object property items from folder contents */
    PreparedStatement propertyItemsRemovalQuery;
    
    /** Query to remove all of the property groups from folder contents */
    PreparedStatement propertyGroupsRemovalQuery;
    
    /** Query to remove all of the metadata associated with a folder */
    PreparedStatement metaDataRemovalQuery;
    
    /** Query to remove all of the permissions associated with a folder */
    PreparedStatement permissionRemovalQuery;
    
    /** Query to remove invocation servicew logs */
    PreparedStatement invocationLogRemovalQuery;
    
    /** Workflow invocation message removal query */
    PreparedStatement invocationMessageRemovalQuery;

    /** Link removal */
    PreparedStatement linkRemovalQuery;

    public FolderRemover(Folder folder, DataStore store) {
        this.folder = folder;
        this.store = store;
    }
    
    public void remove() throws ConnexienceException {
        try {
            c = getSQLConnection();
            documentsQuery = c.prepareStatement("select id from objectsflat where containerid=? and (objecttype='DOCUMENTRECORD' or objecttype='DOCUMENTRECORDLINK' or objecttype='WORKFLOWDOCUMENTRECORD' or objecttype='DYNAMICSERVICES' or objecttype='DYNAMICLIBRARIES')");
            documentRemovalQuery = c.prepareStatement("delete from objectsflat where containerid=? and (objecttype='DOCUMENTRECORD' or objecttype='DOCUMENTRECORDLINK' or objecttype='WORKFLOWDOCUMENTRECORD' or objecttype='DYNAMICSERVICES' or objecttype='DYNAMICLIBRARIES')");
            subfoldersQuery = c.prepareStatement("select id from objectsflat where containerid=? and (objecttype like '%FOLDER%' or objecttype='WORKFLOWINVOCATION' or objecttype='EXTERNALOBJECT' or objecttype='EVENT' or objecttype='GROUPEVENT' or objecttype='BOOK' or objecttype='PROJECT' or objecttype='PROJECTITEM' or objecttype='PROJECTTASK')");
            subfoldersRemovalQuery = c.prepareStatement("delete from objectsflat where containerid=? and (objecttype like '%FOLDER%' or objecttype='WORKFLOWINVOCATION' or objecttype='EXTERNALOBJECT' or objecttype='EVENT' or objecttype='GROUPEVENT' or objecttype='BOOK' or objecttype='PROJECT' or objecttype='PROJECTITEM' or objecttype='PROJECTTASK')");
            versionRemovalQuery = c.prepareStatement("delete from documentversions where id in (select documentversions.id from (objectsflat INNER JOIN documentversions ON objectsflat.id = documentversions.documentrecordid ) where containerid=?)");
            tagRemovalQuery = c.prepareStatement("delete from tagstoobjects where id in (select tagstoobjects.id from (objectsflat INNER JOIN tagstoobjects ON objectsflat.id = tagstoobjects.serverobjectid ) where containerid=?)");
            propertyItemsRemovalQuery = c.prepareStatement("delete from propertyitems where groupid in (select propertygroups.id from ( objectsflat inner join propertygroups on objectsflat.id=propertygroups.objectid) where containerid=?)");
            propertyGroupsRemovalQuery = c.prepareStatement("delete from propertygroups where id in(select propertygroups.id from ( objectsflat inner join propertygroups on objectsflat.id=propertygroups.objectid) where containerid=?)");
            metaDataRemovalQuery = c.prepareStatement("delete from metadata where id in (select metadata.id from (objectsflat INNER JOIN metadata ON objectsflat.id = metadata.objectid ) where containerid=?)");
            permissionRemovalQuery = c.prepareStatement("delete from permissions where id in (select permissions.id from (objectsflat INNER JOIN permissions ON objectsflat.id = permissions.targetobjectid ) where containerid=?)");
            invocationLogRemovalQuery = c.prepareStatement("DELETE from workflowservicelogs WHERE invocationid=?");
            invocationMessageRemovalQuery = c.prepareStatement("DELETE from invocationmessages WHERE invocationid=?");
            linkRemovalQuery = c.prepareStatement("delete from objectsflat where objecttype  = 'LINK' and sourceobjectid IN (select id from objectsflat where containerid=? and (objecttype='DOCUMENTRECORD' or objecttype='DOCUMENTRECORDLINK' or objecttype='WORKFLOWDOCUMENTRECORD' or objecttype='DYNAMICSERVICES' or objecttype='DYNAMICLIBRARIES'))OR sinkobjectid IN(select id from objectsflat where containerid=? and (objecttype='DOCUMENTRECORD' or objecttype='DOCUMENTRECORDLINK' or objecttype='WORKFLOWDOCUMENTRECORD' or objecttype='DYNAMICSERVICES' or objecttype='DYNAMICLIBRARIES'))");
            documentsToDelete.clear();
            String organisationId = folder.getOrganisationId();
            emptyAndRemoveFolder(folder.getId());
            store.bulkDelete(organisationId, documentsToDelete);
            
        } catch (Exception e){
            throw new ConnexienceException(e.getMessage(), e);
        } finally {
            try {c.close();} catch (Exception e){}
        }
    }
    
    private void emptyAndRemoveFolder(String folderId) throws Exception {
        emptyFolder(folderId);
        removeEmptyFolder(folderId);
    }
    
    private void emptyFolder(String folderId) throws Exception {
        ResultSet docIds = null;
        ResultSet subFolderIds = null;
        try {
            // Remove properties
            propertyItemsRemovalQuery.setString(1, folderId);
            propertyItemsRemovalQuery.executeUpdate();
            propertyGroupsRemovalQuery.setString(1, folderId);
            propertyGroupsRemovalQuery.executeUpdate();
            
            metaDataRemovalQuery.setString(1, folderId);
            metaDataRemovalQuery.executeUpdate();
            
            permissionRemovalQuery.setString(1,folderId);
            permissionRemovalQuery.executeUpdate();

            permissionRemovalQuery.setString(1, folderId);
            permissionRemovalQuery.executeUpdate();
            
            tagRemovalQuery.setString(1, folderId);
            tagRemovalQuery.executeUpdate();

            linkRemovalQuery.setString(1, folderId);
            linkRemovalQuery.setString(2, folderId);
            linkRemovalQuery.executeUpdate();

            invocationLogRemovalQuery.setString(1, folderId);
            invocationLogRemovalQuery.executeUpdate();
            
            invocationMessageRemovalQuery.setString(1, folderId);
            invocationMessageRemovalQuery.executeUpdate();
            
            // List the document IDs
            documentsQuery.setString(1, folderId);
            docIds = documentsQuery.executeQuery();
            while(docIds.next()){
                documentsToDelete.add(docIds.getString(1));
            }
            int rows;
            
            // Delete the versions from the database
            versionRemovalQuery.setString(1, folderId);
            rows = versionRemovalQuery.executeUpdate();
            
            // Delete the actual documents
            documentRemovalQuery.setString(1, folderId);
            rows = documentRemovalQuery.executeUpdate();
            
            // Get subfolders and empty them in turn
            subfoldersQuery.setString(1, folderId);
            subFolderIds = subfoldersQuery.executeQuery();
            ArrayList<String>ids = new ArrayList<>();
            
            while(subFolderIds.next()){
                ids.add(subFolderIds.getString(1));
            }
            
            for(String id:ids){
                emptyFolder(id);
                removeEmptyFolder(id);
            }
            
            // Delete the subfolders themselves
            subfoldersRemovalQuery.setString(1, folderId);
            subfoldersRemovalQuery.executeUpdate();
            
            
        } catch (Exception e){
            throw e;
        } finally {
            try {docIds.close();} catch (Exception e){}
            try {subFolderIds.close();} catch (Exception e){}
        }
    }
    
    private void removeEmptyFolder(String folderId) throws Exception {
        Statement removalStatement = null;
        try {
            removalStatement = c.createStatement();
            
            // Remove the permissions
            removalStatement.executeUpdate("delete from permissions where targetobjectid='" + folderId + "'");
            
            // Remove the property items
            removalStatement.executeUpdate("delete from propertyitems where groupid in (select propertygroups.id from propertygroups where objectid='" + folderId + "')");
            
            // Remove the property groups
            removalStatement.executeUpdate("delete from propertygroups where objectid='" + folderId + "'");
            
            // Remove the tags
            removalStatement.executeUpdate("delete from tagstoobjects where serverobjectid='" + folderId + "'");
            
            // Remove the workflow griggers
            removalStatement.executeUpdate("DELETE from foldertriggers WHERE folderid='" + folderId + "'");
            
            // Remove the actual folder
            removalStatement.executeUpdate("delete from objectsflat where id='" + folderId + "'");
            
            
        } catch (Exception e){
            throw e;
        } finally {
            try {removalStatement.close();} catch (Exception e){}
        }
    }
    
    private Connection getSQLConnection() throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup("java:jboss/datasources/ConnexienceDB");
        return source.getConnection();
    }    
}
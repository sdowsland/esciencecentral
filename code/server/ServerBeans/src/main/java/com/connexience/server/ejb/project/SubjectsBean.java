/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.acl.AccessControlRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.study.Phase;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.project.study.Subject;
import com.connexience.server.model.project.study.SubjectGroup;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/SubjectsBean", beanInterface = SubjectsRemote.class)
public class SubjectsBean extends HibernateSessionContainer implements SubjectsRemote {
    @EJB
    private StudyRemote studyBean;

    @EJB
    private AccessControlRemote aclBean;

    @EJB
    private StorageRemote storageBean;

    @Inject
    private EntityManager em;

    @Override
    public SubjectGroup getSubjectGroup(final Ticket ticket, final Integer subjectGroupId) throws ConnexienceException {
        final SubjectGroup subjectGroup = em.find(SubjectGroup.class, subjectGroupId);

        if (subjectGroup == null) {
            throw new ConnexienceException("SubjectGroup id='" + subjectGroupId + "' not found in DB.");
        }

        return subjectGroup;
    }

    @Override
    public SubjectGroup saveSubjectGroup(final Ticket ticket, final SubjectGroup subjectGroup) throws ConnexienceException {
        if (StringUtils.isBlank(subjectGroup.getDataFolderId())) {
            String parentFolder = "";

            //Sanity check
            if (subjectGroup.getPhase() == null || subjectGroup.getPhase().getStudy() == null) {
                throw new ConnexienceException("Cannot save SubjectGroup without an associated Study.");
            }

            //Set the parent folder
            if (subjectGroup.getParent() != null && StringUtils.isNotBlank(subjectGroup.getParent().getDataFolderId())) {
                parentFolder = subjectGroup.getParent().getDataFolderId();
            } else if (subjectGroup.getPhase() != null && StringUtils.isNotBlank(subjectGroup.getPhase().getDataFolderId())) {
                parentFolder = subjectGroup.getPhase().getDataFolderId();
            } else {
                throw new ConnexienceException("Could not find data folder from Study or parent SubjectGroup");
            }

            //Set the data folder
            Folder dataFolder = new Folder();
            dataFolder.setName(generateFolderName(subjectGroup));
            dataFolder.setProjectId(String.valueOf(subjectGroup.getPhase().getStudy().getId()));
            dataFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, parentFolder, dataFolder);
            subjectGroup.setDataFolderId(dataFolder.getId());

            // Ensure ACL is correct
            aclBean.grantAccess(ticket, subjectGroup.getPhase().getStudy().getAdminGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
            aclBean.grantAccess(ticket, subjectGroup.getPhase().getStudy().getAdminGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
            aclBean.grantAccess(ticket, subjectGroup.getPhase().getStudy().getMembersGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
            aclBean.grantAccess(ticket, subjectGroup.getPhase().getStudy().getMembersGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);


            em.persist(subjectGroup);
            return subjectGroup;
        } else {
            return em.merge(subjectGroup);
        }
    }

    @Override
    public void deleteSubjectGroup(final Ticket ticket, final Integer subjectGroupId) throws ConnexienceException {
        deleteSubjectGroup(ticket, subjectGroupId, false);
    }

    @Override
    public void deleteSubjectGroup(final Ticket ticket, final Integer subjectGroupId, final Boolean deleteFiles) throws ConnexienceException {
        final SubjectGroup subjectGroup = getSubjectGroup(ticket, subjectGroupId);

        if (deleteFiles) {
            storageBean.removeFolderTree(ticket, subjectGroup.getDataFolderId());
        }

        em.remove(subjectGroup);
    }

    @Override
    public Collection<SubjectGroup> getSubjectGroups(final Ticket ticket, final Integer studyId) throws ConnexienceException {

        return em.createQuery("SELECT s FROM SubjectGroup s WHERE s.phase.study.id = :studyId")
                .setParameter("studyId", studyId)
                .getResultList();
    }


    @Override
    public Collection<SubjectGroup> getPhaseSubjectGroups(final Ticket ticket, final Integer phaseId) throws ConnexienceException {

        return em.createQuery("SELECT s FROM SubjectGroup s WHERE s.phase.id = :phaseId")
                .setParameter("phaseId", phaseId)
                .getResultList();
    }

    @Override
    public Collection<SubjectGroup> getChildSubjectGrous(final Ticket ticket, final Integer phaseId, final Integer parentGroupId) throws ConnexienceException {

        if(parentGroupId == null){
            return em.createQuery("SELECT s FROM SubjectGroup s WHERE s.phase.id = :phaseId AND s.parent IS NULL")
                    .setParameter("phaseId", phaseId)
                    .getResultList();
        }
        else{
            return em.createQuery("SELECT s FROM SubjectGroup s WHERE s.phase.id = :phaseId AND s.parent.id = :parentId")
                    .setParameter("phaseId", phaseId)
                    .setParameter("parentId", parentGroupId)
                    .getResultList();

        }
    }

    @Override
    public Subject getSubject(final Ticket ticket, final Integer subjectId) throws ConnexienceException {
        final Subject subject = em.find(Subject.class, subjectId);

        if (subject == null) {
            throw new ConnexienceException("Subject id='" + subjectId + "' not found in DB.");
        }

        return subject;
    }

    @Override
    public Subject getSubjectInStudyByExternalId(Ticket ticket, Integer studyId, String externalId) throws ConnexienceException {
        Query q = em.createNamedQuery("Subject.countExternalIdsInStudy", Long.class);
        q.setParameter("studyId", studyId);
        q.setParameter("externalId", externalId);
        Long count = (Long) q.getResultList().get(0);
        if (count > 0) {
            Query q2 = em.createNamedQuery("Subject.getSubjectInStudyByExternalId", Subject.class);
            q2.setParameter("studyId", studyId);
            q2.setParameter("externalId", externalId);
            List results = q2.getResultList();
            if (results.size() > 0) {
                return (Subject) results.get(0);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Subject saveSubject(final Ticket ticket, final Subject subject) {
        if (subject.getId() == null) {
            em.persist(subject);
            return subject;
        } else {
            return em.merge(subject);
        }
    }

    @Override
    public void deleteSubject(final Ticket ticket, final Integer subjectId) throws ConnexienceException {
        final Subject subject = getSubject(ticket, subjectId);

        em.remove(subject);
    }

    @Override
    public Collection<Subject> getSubjects(final Ticket ticket, final Integer studyId) throws ConnexienceException {
        final Study study = studyBean.getStudy(ticket, studyId);

        return em.createQuery("SELECT s FROM Subject s WHERE s.subjectGroup.phase.study = :study")
                .setParameter("study", study)
                .getResultList();
    }

    @Override
    public Collection<Subject> getSubjectsInPhase(final Ticket ticket, final Integer studyId, final Integer phaseId) throws ConnexienceException {
        final Study study = studyBean.getStudy(ticket, studyId);
        final Phase phase = em.find(Phase.class, phaseId);

        return em.createQuery("SELECT s FROM Subject s WHERE s.subjectGroup.phase.study = :study AND s.subjectGroup.phase = :phase")
                .setParameter("study", study)
                .setParameter("phase", phase)
                .getResultList();
    }

    @Override
    public Collection<Subject> getSubjectsInGroup(final Ticket ticket, final Integer subjectGroupId) throws ConnexienceException {

        return em.createQuery("SELECT s FROM Subject s WHERE s.subjectGroup.id= :subjectGroupId")
                .setParameter("subjectGroupId", subjectGroupId)
                .getResultList();
    }

    /***********************************************/
    /******* CREATING TOP LEVEL GROUPS *************/
    /***********************************************/

    @Override
    public SubjectGroup createSubjectGroup(final Ticket ticket, Integer studyId, final String groupName, final String categoryName, final String phaseName, final HashMap<String, String> additionalProperties) throws ConnexienceException {
        SubjectGroup group = createSubjectGroup(ticket, studyId, groupName, categoryName, phaseName);

        // Add the extra properties
        for (String name : additionalProperties.keySet()) {
            group.putAdditionalProperty(name, additionalProperties.get(name));
        }

        em.persist(group);
        return group;
    }

    @Override
    public SubjectGroup createSubjectGroup(final Ticket ticket, Integer studyId, final String groupName, final Integer phaseId) throws ConnexienceException {

        final SubjectGroup subjectGroup = new SubjectGroup(groupName);

        Phase phase = em.find(Phase.class, phaseId);

        phase.addSubjectGroup(subjectGroup);
        em.persist(phase);

        return saveSubjectGroup(ticket, subjectGroup);
    }


    @Override
    public SubjectGroup createSubjectGroup(final Ticket ticket, Integer studyId, final String groupName, final String categoryName, final String phaseName) throws ConnexienceException {
        final Study study = em.find(Study.class, studyId);

        List<Phase> phases = em.createNamedQuery("Phase.namedPhase").setParameter("studyId", studyId).setParameter("name", phaseName).getResultList();
        if (phases.size() > 1) {
            throw new ConnexienceException("Too many phases with name '" + phaseName + "' in study: " + studyId);
        }

        Phase phase;
        if (phases.size() == 0) {
            phase = EJBLocator.lookupStudyBean().createPhase(ticket, study, phaseName);
            phase = em.merge(phase);
        } else {
            phase = phases.get(0);
        }

        SubjectGroup group = createSubjectGroup(ticket, studyId, groupName, phase.getId());
        group.setPhase(phase);
        group.setCategoryName(categoryName);
        phase.addSubjectGroup(group);

        em.persist(phase);
        em.persist(group);

        return group;
    }

    /***********************************************/
    /**** CREATING CHILD GROUPS             ********/
    /***********************************************/

    @Override
    public SubjectGroup createChildGroup(final Ticket ticket, Integer studyId, Integer parentSubjectGroupId, final String groupName, final String categoryName, final String phaseName, final HashMap<String, String> additionalProperties) throws ConnexienceException {

        SubjectGroup group = createChildGroup(ticket, studyId, parentSubjectGroupId, groupName, categoryName, phaseName);
        group.setAdditionalProperties(additionalProperties);

        em.persist(group);
        return group;
    }


    @Override
    public SubjectGroup createChildGroup(final Ticket ticket, Integer studyId, Integer parentSubjectGroupId, final String groupName, final Integer phaseId) throws ConnexienceException {

        final SubjectGroup parentGroup = getSubjectGroup(ticket, parentSubjectGroupId);
        final Study study = em.find(Study.class, studyId);
        final Phase phase = em.find(Phase.class, phaseId);

        final SubjectGroup childGroup = new SubjectGroup(groupName);

        parentGroup.addChild(childGroup);
        childGroup.setPhase(phase);
        phase.addSubjectGroup(childGroup);

        em.persist(phase);
        em.persist(childGroup);
        em.persist(parentGroup);

        return saveSubjectGroup(ticket, childGroup);
    }

    /*
     * Create a child group with a phase name
     */
    @Override
    public SubjectGroup createChildGroup(final Ticket ticket, Integer studyId, Integer parentSubjectGroupId, final String groupName, final String categoryName, final String phaseName) throws ConnexienceException {

        final Study study = em.find(Study.class, studyId);

        //Get the phase
        List<Phase> phases = em.createNamedQuery("Phase.namedPhase").setParameter("studyId", studyId).setParameter("name", phaseName).getResultList();
        if (phases.size() > 1) {
            throw new ConnexienceException("Too many phases with name '" + phaseName + "' in study: " + studyId);
        }
        Phase phase;

        if (phases.size() == 0) {
            phase = EJBLocator.lookupStudyBean().createPhase(ticket, study, phaseName);
            phase = em.merge(phase);
        } else {
            phase = phases.get(0);
        }

        //Create the groups
        final SubjectGroup childGroup = new SubjectGroup(groupName);
        final SubjectGroup parentGroup = getSubjectGroup(ticket, parentSubjectGroupId);

        //Wire up
        parentGroup.addChild(childGroup);
        childGroup.setPhase(phase);
        childGroup.setCategoryName(categoryName);
        phase.addSubjectGroup(childGroup);

        //Save
        em.persist(phase);
        em.persist(childGroup);
        em.persist(parentGroup);

        return saveSubjectGroup(ticket, childGroup);
    }


    /***********************************************/
    /**** CREATING SUBJECTS                 ********/
    /***********************************************/

    @Override
    public Subject createSubject(final Ticket ticket, Integer parentSubjectGroupId, final String subjectReadableId) throws ConnexienceException {
        final SubjectGroup parentGroup = getSubjectGroup(ticket, parentSubjectGroupId);

        final Subject subject = new Subject(subjectReadableId);

        parentGroup.addSubject(subject);

        saveSubjectGroup(ticket, parentGroup);
        return saveSubject(ticket, subject);
    }

    @Override
    public Subject createSubject(Ticket ticket, Integer parentSubjectGroupId, String subjectReadableId, HashMap<String, String> additionalProperties) throws ConnexienceException {
        final SubjectGroup parentGroup = getSubjectGroup(ticket, parentSubjectGroupId);

        final Subject subject = new Subject(subjectReadableId);

        // Add the additional properties
        for (String name : additionalProperties.keySet()) {
            subject.putAdditionalProperty(name, additionalProperties.get(name));
        }
        parentGroup.addSubject(subject);

        saveSubjectGroup(ticket, parentGroup);
        return saveSubject(ticket, subject);
    }


    @Override
    public SubjectGroup addGroupSubject(final Ticket ticket, final Integer parentSubjectGroupId, final Integer subjectId) throws ConnexienceException {
        final SubjectGroup subjectGroup = getSubjectGroup(ticket, parentSubjectGroupId);
        final Subject subject = getSubject(ticket, subjectId);

        subjectGroup.addSubject(subject);

        saveSubject(ticket, subject);
        return saveSubjectGroup(ticket, subjectGroup);
    }

    @Override
    public SubjectGroup removeGroupSubject(final Ticket ticket, final Integer parentSubjectGroupId, final Integer subjectId) throws ConnexienceException {
        final SubjectGroup subjectGroup = getSubjectGroup(ticket, parentSubjectGroupId);
        final Subject subject = getSubject(ticket, subjectId);

        subjectGroup.removeSubject(subject);

        saveSubject(ticket, subject);
        return saveSubjectGroup(ticket, subjectGroup);
    }


    @Override
    public Collection<Subject> getSubjectFromProperty(final Ticket ticket, final String propertyName, final String propertyValue) throws ConnexienceException {

        return em.createNativeQuery("SELECT * FROM subjects WHERE id IN(SELECT subject_id FROM subject_additionalproperties " +
                " WHERE additionalproperties_key = :propName " +
                " AND additionalproperties = :value)", Subject.class)
                .setParameter("propName", propertyName)
                .setParameter("value", propertyValue)
                .getResultList();
    }

    @Override
    public boolean isPropertyUnique(final Ticket ticket, final String propertyName, final String propertyValue) throws ConnexienceException {

        Collection<Integer> subjectIds = em.createNativeQuery("SELECT subject_id FROM subject_additionalproperties " +
                " WHERE additionalproperties_key = :propName " +
                " AND additionalproperties = :value")
                .setParameter("propName", propertyName)
                .setParameter("value", propertyValue)
                .getResultList();

        return subjectIds.size() == 0;
    }

    @Override
    public Subject addPropertiesToSubject(Ticket ticket, Integer subjectId, HashMap<String, String> properties) throws ConnexienceException {
        Subject subject = em.merge(getSubject(ticket, subjectId));
        if (subject != null) {
            for (String name : properties.keySet()) {
                subject.getAdditionalProperties().put(name, properties.get(name));
            }
            em.persist(subject);
            return subject;
        } else {
            return null;
        }
    }

    @Override
    public SubjectGroup addPropertiesToSubjectGroup(Ticket ticket, Integer groupId, HashMap<String, String> properties) throws ConnexienceException {
        SubjectGroup group = em.merge(getSubjectGroup(ticket, groupId));
        if(group!=null){
            for (String name : properties.keySet()) {
                group.getAdditionalProperties().put(name, properties.get(name));
            }
            em.persist(group);
            return group;            
        } else {
            return null;
        }
    }

    private String generateFolderName(final SubjectGroup subjectGroup) {
        return "Group: " + subjectGroup.getDisplayName();
    }
}

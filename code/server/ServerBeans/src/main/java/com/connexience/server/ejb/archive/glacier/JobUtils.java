/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.LinkedList;
import java.util.List;

public class JobUtils
{
    private static final Logger logger = Logger.getLogger(JobUtils.class.getName());
    
    private static final int MAXNUMBEROFJOBCOMPLETIONS = 6;
    
    public static class JobCompletion
    {
        public JobCompletion(String downloadId, String receiptHandle)
        {
            this.downloadId    = downloadId;
            this.receiptHandle = receiptHandle;
        }

        public String downloadId;
        public String receiptHandle;
    }

    public static List<JobCompletion> checkJobCompletions(AmazonSQSClient amazonSQSClient, String queueURL)
    {
        List<JobCompletion> jobCompletions = new LinkedList<>();

        try
        {
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.withQueueUrl(queueURL);
            receiveMessageRequest.withMaxNumberOfMessages(MAXNUMBEROFJOBCOMPLETIONS);

            ReceiveMessageResult receiveMessageResult = amazonSQSClient.receiveMessage(receiveMessageRequest);
            
            if (receiveMessageResult != null)
            {
                List<Message> messages = receiveMessageResult.getMessages();

                if (messages != null)
                {
                    for (Message message: messages)
                    {
                        try
                        {
                            logger.debug("Message: \"" + message.getBody() + "\"");
                            
                            ObjectMapper objectMapper            = new ObjectMapper();
                            JsonFactory  jsonFactory             = objectMapper.getJsonFactory();
                            JsonParser   messageJsonParser       = jsonFactory.createJsonParser(message.getBody());
                            JsonNode     messageJsonNode         = objectMapper.readTree(messageJsonParser);
                            String       jobMessage              = messageJsonNode.get("Message").getTextValue();
                            JsonParser   jobDesciptionJsonParser = jsonFactory.createJsonParser(jobMessage);
                            JsonNode     jobDesciptionJsonNode   = objectMapper.readTree(jobDesciptionJsonParser);
                            String       downloadId              = jobDesciptionJsonNode.get("JobId").getTextValue();
                            String       action                  = jobDesciptionJsonNode.get("Action").getTextValue();
                            String       statusCode              = jobDesciptionJsonNode.get("StatusCode").getTextValue();

                            if ((downloadId != null) && (action != null) && action.equals("ArchiveRetrieval") && (statusCode != null) && statusCode.equals("Succeeded"))
                            {
                                JobCompletion jobCompletion = new JobCompletion(downloadId, message.getReceiptHandle());

                                jobCompletions.add(jobCompletion);
                            }
                            else
                                logger.info("Unproceesing message (" + downloadId + "," + action +"," + statusCode + ")");
                        }
                        catch (Throwable throwable)
                        {
                            logger.error("Problem while proceesing job completion message", throwable);
                        }
                    }
                }
                else
                    logger.warn("Obtained job completions, but empty");
            }
            else
                logger.warn("Unable of obtain job completions");
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ", throwable);
        }

        return jobCompletions;
    }

    public static String deleteJobCompletion(AmazonSQSClient amazonSQSClient, String queueUrl, String receiptHandle)
    {
        try
        {
            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest();
            deleteMessageRequest.withQueueUrl(queueUrl);
            deleteMessageRequest.withReceiptHandle(receiptHandle);

            amazonSQSClient.deleteMessage(deleteMessageRequest);
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ", throwable);
        }
        
        return null;
    }
}

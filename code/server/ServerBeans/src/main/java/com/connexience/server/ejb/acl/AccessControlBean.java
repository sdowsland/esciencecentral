/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.acl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.OrganisationFolderAccess;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.PermissionList;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This bean can make access control decisions for objects within the database
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/AccessControlBean", beanInterface = AccessControlRemote.class)
public class AccessControlBean extends HibernateSessionContainer implements AccessControlRemote {

    /** Creates a new instance of AccessControlBean */
    public AccessControlBean() {
    }

    /**
     * Grant a principal access to a resource. Access is granted if the ticket making
     * the call has write permission on the object in question
     */
    public void grantAccess(Ticket ticket, String principalId, String resourceId, String accessType) throws ConnexienceException {
        ServerObject resource = getObject(resourceId, ServerObject.class);
        if (resource != null) {
            if (canTicketAccessResource(ticket, resource, Permission.WRITE_PERMISSION)) {
                addPermission(principalId, resource, accessType);

                ServerObject grantee = getObject(principalId, ServerObject.class); //User or group
                ServerObject granter = getObject(ticket.getUserId(), User.class);

                // TODO: ADD LOGGING - ACCESS GRANTED
            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, resource));
            }
        } else {
            throw new ConnexienceException("No such resource");
        }
    }

    /**
     * Grant universal access to a resource. Access is granted if the ticket making
     * the call has write permission on the object in question
     */
    public void grantUniversalAccess(Ticket ticket, String userId, String resourceId, String accessType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ServerObject resource = getObject(resourceId, ServerObject.class);
            if (resource != null) {
                if (canTicketAccessResource(ticket, resource, Permission.WRITE_PERMISSION)) {
                    Query q = session.createQuery("from Permission as p WHERE p.targetObjectId= :targetObjectId and p.type= :type and p.universal = :universal");
                    q.setString("targetObjectId", resource.getId());
                    q.setString("type", accessType);
                    q.setBoolean("universal", true);
                    List l = q.list();
                    if (l.size() == 0) {
                        Permission p = new Permission();
                        p.setPrincipalId(userId);
                        p.setTargetObjectId(resource.getId());
                        p.setType(accessType);
                        p.setUniversal(true);
                        session.persist(p);

                        // TODO: ADD LOGGING - UNIVERSAL ACCESS GRANTED

                    }
                } else {
                    throw new ConnexienceException(ConnexienceException.formatMessage(ticket, resource));
                }

            } else {
                throw new ConnexienceException("No such resource");
            }
        } catch (ConnexienceException ce) {
            ce.printStackTrace();
            throw ce;
        } finally {
            closeSession(session);
        }
    }

    /**
     * Add a permission
     *
     * @param principalId user/groupId
     * @param resource    resourceId
     * @param accessType  READ/WRITE
     * @throws ConnexienceException something went wrong
     */
    private void addPermission(String principalId, ServerObject resource, String accessType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (!permissionExists(session, principalId, resource, accessType)) {
                Permission p = new Permission();
                p.setPrincipalId(principalId);
                p.setTargetObjectId(resource.getId());
                p.setType(accessType);
                session.persist(p);
            }
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Does a permission exist
     *
     * @param session     s
     * @param principalId user/group
     * @param resource    resource
     * @param accessType  r/w
     * @return true if the principal can access the resource
     */
    private boolean permissionExists(Session session, String principalId, ServerObject resource, String accessType) {
        Query q = session.createQuery("from Permission as p where p.principalId=? and p.targetObjectId=? and p.type=?");
        q.setString(0, principalId);
        q.setString(1, resource.getId());
        q.setString(2, accessType);
        List l = q.list();
        return l.size() > 0;
    }

    /**
     * Does a permission exist
     *
     * @param principalId user/group
     * @param resource    resource
     * @param accessType  r/w
     * @return true if the principal can access the resource
     */
    public boolean permissionExists(String principalId, ServerObject resource, String accessType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return permissionExists(session, principalId, resource, accessType);
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Revoke a specific permission from an object */
    public void revokePermission(Ticket ticket, String principalId, String resourceId, String accessType) throws ConnexienceException {
        ServerObject resource = getObject(resourceId, ServerObject.class);

        if (resource != null) {
            Session session = null;

            if (canTicketAccessResource(ticket, resource, Permission.WRITE_PERMISSION)) {
                try {
                    session = getSession();
                    SQLQuery q = session.createSQLQuery("delete from permissions where principalid=? and targetobjectid=? and type=?");
                    q.setString(0, principalId);
                    q.setString(1, resourceId);
                    q.setString(2, accessType);

                    q.executeUpdate();
                } catch (Exception e) {
                    throw new ConnexienceException(e.getMessage());
                } finally {
                    closeSession(session);
                }

            } else {
                throw new ConnexienceException("Access denied revoking permissions on " + resourceId);
            }

        } else {
            throw new ConnexienceException("No such resource");
        }
    }


    /** Remove all permissions for a specified principal */
    public void revokeAllPermissions(Ticket ticket, String principalId, String resourceId) throws ConnexienceException {
        ServerObject resource = getObject(resourceId, ServerObject.class);

        if (resource != null) {
            Session session = null;

            if (canTicketAccessResource(ticket, resource, Permission.WRITE_PERMISSION)) {
                try {
                    session = getSession();
                    @SuppressWarnings("unchecked")
                    List<Permission> perms = session.createQuery("FROM Permission AS p WHERE p.principalId = :principalId AND p.targetObjectId = :obejctId")
                            .setString("principalId", principalId)
                            .setString("obejctId", resourceId)
                            .list();

                    for (Permission p : perms) {
                        revokePermission(ticket, p.getPrincipalId(), p.getTargetObjectId(), p.getType());
                    }
                } catch (Exception e) {
                    throw new ConnexienceException(e.getMessage());
                } finally {
                    closeSession(session);
                }

            } else {
                throw new ConnexienceException("Access denied revoking all permissions for " + principalId + " on " + resourceId);
            }

        } else {
            throw new ConnexienceException("No such resource");
        }
    }

    /** Remove all permissions */
    public void revokeAllPermissions(Ticket ticket, String resourceId) throws ConnexienceException {
        ServerObject resource = getObject(resourceId, ServerObject.class);

        if (resource != null) {
            Session session = null;

            if (canTicketAccessResource(ticket, resource, Permission.WRITE_PERMISSION)) {
                try {
                    session = getSession();
                    SQLQuery q = session.createSQLQuery("delete from permissions where targetobjectid=?");
                    q.setString(0, resourceId);
                    q.executeUpdate();
                } catch (Exception e) {
                    throw new ConnexienceException(e.getMessage());
                } finally {
                    closeSession(session);
                }
            } else {
                throw new ConnexienceException("Access denied revoking all permissions on " + resourceId);
            }

        } else {
            throw new ConnexienceException("No such resource");
        }
    }

    /** Propagate security settings from a folder down to all the child folders */
    public void propagatePermissions(Ticket ticket, String folderId) throws ConnexienceException {
        if (ticket.isAssociatedWithNonRootOrg()) {
            OrganisationFolderAccess folders = new OrganisationFolderAccess(ticket.getOrganisationId());
            Folder topFolder = folders.getFolder(folderId);

            if (ticket.isSuperTicket() || isOrganisationAdminTicket(ticket) || canTicketAccessResource(ticket, topFolder, Permission.WRITE_PERMISSION)) {
                // Only organisation admins or the root user can propagate permissions
                PermissionList permissions = this.getObjectPermissions(ticket, folderId);

                List allFolders = folders.getChildHeirarchyAsList(topFolder);
                PermissionList folderPermissions;
                Folder folder;

                for (Object allFolder : allFolders) {
                    folder = (Folder) allFolder;
                    folderPermissions = permissions.createCopyForObject(folder.getId());
                    setObjectPermissions(getInternalTicket(), folder.getId(), folderPermissions);

                    //don't log share event as it is done in setObjectPermissions
                }

            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, topFolder));
            }

        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /** Can a ticket add a new resource to the database */
    public boolean canTicketAddResource(Ticket ticket, ServerObject object) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return canTicketAddResource(session, ticket, object);
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Can a ticket add a new resource to the database
     *
     * @param object object
     */
    public boolean canTicketAddResource(Session session, Ticket ticket, ServerObject object) throws ConnexienceException {
        if (ticket.isSuperTicket()) {
            // Super ticket or object owner can do anything
            return true;
        } else {
            // If the object is an organisation, behaviour is different. It can be added
            // without a container id and an organisation id set
            if (object instanceof Organisation) {
                // TODO: Check to see if the user can add an organisation
                return true;

            } else {
                if (object.getOrganisationId() != null && object.getOrganisationId().equals(ticket.getOrganisationId())) {
                    // If the user is an organisation admin, then allow
                    if (isOrganisationAdminTicket(ticket)) {
                        return true;
                    }

                    // Can the user access the parent folder
                    if (object.getContainerId() != null) {
                        Folder parentFolder = EJBLocator.lookupStorageBean().getFolder(ticket, object.getContainerId());
                        return canTicketAccessResource(session, ticket, parentFolder, Permission.WRITE_PERMISSION) || canTicketAccessResource(session, ticket, parentFolder, Permission.ADD_PERMISSION);
                    } else {
                        // Not adding to a folder
                        return true;
                    }

                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Can a ticket read a resource. This is the main entry point for external callers,
     * it merely creates a Session object and delegates to a private method
     */
    public boolean canTicketAccessResource(Ticket ticket, ServerObject object, String accessType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return canTicketAccessResource(session, ticket, object, accessType);
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }

    }

    /**
     * Can a ticket access a resource
     *
     * @param session    hibernate session
     * @param ticket     user's ticket
     * @param object     object trying to access
     * @param accessType r/w access
     * @return true if they can access, false otherwise
     * @throws ConnexienceException something went wrong
     */
    public boolean canTicketAccessResource(Session session, Ticket ticket, ServerObject object, String accessType) throws ConnexienceException {
        // Main Access decision logic
        PermissionList permissions;

        // Check to see everything exists
        if (session == null || ticket == null || object == null) {
            return false;
        }

        // Is the object the same as the user - i.e. User accessing themselves
        if (object.getId() != null && ticket.getUserId() != null) {
            if (ticket.getUserId().equals(object.getId())) {
                return true;
            }
        } else {
            return ticket.getUserId() == null && ticket.isSuperTicket();
        }

        // Otherwise do more checks
        if (ticket.isSuperTicket() || ticket.getUserId().equals(object.getCreatorId())) {
            // Always grant super ticket, and object owners
            return true;
        } else if (object.getId().equals(ticket.getUserId()) && accessType.equals(Permission.READ_PERMISSION)) {
            // All users can read their own records
            return true;
        } else {
            // Not a super ticket, do checks. First check - same organisation
            if (object.getOrganisationId() != null && object.getOrganisationId().equals(ticket.getOrganisationId())) {
                // Is the user a member of the administrator group for this organisation
                if (isOrganisationAdminTicket(ticket)) {
                    return true;
                }

                // Check to see if we're accessing a folder
                if (object instanceof Folder) {
                    // Check to see if it is a public folder and the access
                    // required is read access. If it is, then access is granted
                    if (((Folder) object).isPublicVisible() && accessType.equals(Permission.READ_PERMISSION)) {
                        return true;
                    }
                }

                if (isObjectPublic(session, ticket, object.getId(), accessType)) {
                    return true;
                }

                // Object is a folder, a blogpost (get the containing blog), or object is a plain object with no permissions
                String folderId = null;

                if (object instanceof Folder) {
                    folderId = object.getId();
                } else {
                    // Get the parent folder
                    if (object.getContainerId() != null) {
                        folderId = object.getContainerId();
                    }
                }


                if(folderId !=null)
                {// If the user has the correct level of access to the containing folder, i.e. explicit permissions or creator
                    ServerObject container = getObject(folderId, ServerObject.class);

                    permissions = getObjectPermissionsForTicket(session, folderId, ticket);
                    if (permissions.permissionExists(accessType)) {
                        return true;
                    } else if (container.getCreatorId().equals(ticket.getUserId())) {
                        return true;
                    }

                    //if the container has a project id set then check to see if the user is a project admin or member
                    if(container.getProjectId() != null && !container.getProjectId().equals(""))
                    {
                        Project containersProject = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(container.getProjectId()));
                        if(containersProject != null){
                            if(EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, containersProject)){
                                return true;
                            }
                            if(EJBLocator.lookupProjectsBean().isProjectMember(ticket, containersProject)){
                                return true;
                            }
                        }
                    }
                }

                //does the user have an explicit permission on that object?
                permissions = getObjectPermissionsForTicket(session, object.getId(), ticket);
                if (permissions.permissionExists(accessType)) {
                    return true;
                }


                //if the object has a project id set then check to see if the user is a member or admin of that project.
                if(object.getProjectId() != null && !object.getProjectId().equals(""))
                {
                    Project objectsProject = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(object.getProjectId()));
                    if(objectsProject != null){
                        if(EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, objectsProject)){
                            return true;
                        }
                        if(EJBLocator.lookupProjectsBean().isProjectMember(ticket, objectsProject)){
                            return true;
                        }
                    }
                }

            return false;


            } else {
                // Not same organisation. For now return false, but:
                // TODO: Go to trusted organisation to check permission
                return false;
            }
        }
    }

    /** Get all of the permissions for a specific object */
    public PermissionList getObjectPermissions(Ticket ticket, String objectId) throws ConnexienceException {
        ServerObject object = getObject(objectId, ServerObject.class);
        if (object != null) {
            if (canTicketAccessResource(ticket, object, Permission.READ_PERMISSION)) {
                Session session = null;
                try {
                    session = getSession();
                    Query q = session.createQuery("from Permission as p where p.targetObjectId=?");
                    q.setString(0, objectId);
                    return new PermissionList(objectId, q.list());
                } catch (Exception e) {
                    throw new ConnexienceException(e.getMessage());
                } finally {
                    closeSession(session);
                }
            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
            }
        } else {
            throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
        }
    }

    /** Set the permissions for an object */
    public void setObjectPermissions(Ticket ticket, String objectId, PermissionList permissions) throws ConnexienceException {
        ServerObject object = getObject(objectId, ServerObject.class);
        if (object != null) {
            if (canTicketAccessResource(ticket, object, Permission.WRITE_PERMISSION)) {
                // Delete existing permissions
                deleteObjectPermissions(object);

                // Create new permissions
                Session session = null;
                try {
                    session = getSession();
                    for (int i = 0; i < permissions.getSize(); i++) {
                        session.save(permissions.getPermission(i));

                        // TODO: ADD LOGGING - PERMISSIONS CHANGED
                    }
                } catch (Exception e) {
                    throw new ConnexienceException("Error setting object permissions: " + e.getMessage());
                } finally {
                    closeSession(session);
                }

            } else {
                throw new ConnexienceException(ConnexienceException.formatMessage(ticket, object));
            }

        } else {
            throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
        }
    }

    /**
     * Is a ticket an administrator for an organisation. This method creates it's own session
     * @param ticket ticket
     * @param organisationId organisation
     * @return true if the ticket is an admin
     * @throws ConnexienceException something broke
     */
    /*
      private boolean isTicketOrganisationAdmin(Ticket ticket, String organisationId) throws ConnexienceException
      {
        String groupId = getOrganisationAdminGroupId(organisationId);
        return groupId != null && EJBLocator.lookupTicketBean().ticketHasGroup(ticket, groupId);

        Session session = null;
        try {
            session = getSession();
            return isTicketOrganisationAdmin(session, ticket, organisationId);
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }

      }
    */

    /**
     * Is a ticket an administrator for an organisation
     *
     * @param session    sesssion
     * @param ticket     ticket
     * @param objectId   obejctId
     * @param accessType w/r access
     * @return true if the object is public
     * @throws ConnexienceException something broke
     */
    private boolean isObjectPublic(Session session, Ticket ticket, String objectId, String accessType) throws ConnexienceException {
        String publicUserId = getCachedOrganisation(ticket.getOrganisationId()).getDefaultUserId();
        Query q = session.createQuery("FROM Permission AS p WHERE p.targetObjectId = :objectId AND p.principalId = :publicUserId AND p.type =:accessType");
        q.setString("objectId", objectId);
        q.setString("publicUserId", publicUserId);
        q.setString("accessType", accessType);
        List results = q.list();
        return results.size() > 0;
    }

    /** Check to see if there is a specific permission for a ticket to access an object */
    public PermissionList getObjectPermissionsForTicket(Ticket ticket, String objectId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return getObjectPermissionsForTicket(session, objectId, ticket);
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Check to see if there is a specific permission for a ticket to access an object
     *
     * @param session  session
     * @param objectId objectId
     * @param ticket   ticket
     * @return permissions for the current object
     * @throws ConnexienceException something broke
     */
    private PermissionList getObjectPermissionsForTicket(Session session, String objectId, Ticket ticket) throws ConnexienceException {
        PermissionList permissions = new PermissionList();
        permissions.setObjectId(objectId);

        String publicUserId = getCachedOrganisation(ticket.getOrganisationId()).getDefaultUserId();

        // Permissions for all of the principals attached to a ticket
        Query q = session.createQuery("from Permission as p where (p.targetObjectId= :targetObjectId and p.principalId= :principalId) OR (p.universal = :universalStatus AND p.targetObjectId = :targetObjectId) OR (p.targetObjectId = :targetObjectId AND p.principalId = :publicUserId)");
        q.setString("targetObjectId", objectId);
        q.setString("publicUserId", publicUserId);
        q.setBoolean("universalStatus", true);

        // Permissions for all the ticket groups
        String[] principalIds = EJBLocator.lookupTicketBean().listTicketPrincipalIds(ticket);

        // Get all of the permissions for each principal
        for (String principalId : principalIds) {
            q.setString("principalId", principalId);
            permissions.addPermissions(q.list());
        }
        return permissions;
    }

    /**
     * Delete all permissions for an object
     *
     * @param object esrver object
     * @throws ConnexienceException something went wrong
     */
    private void deleteObjectPermissions(ServerObject object) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            //todo: log unshare event

            SQLQuery q = session.createSQLQuery("DELETE from permissions WHERE targetobjectid=?");
            q.setString(0, object.getId());
            q.executeUpdate();

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Narrow a list of objects using the Access Control rules */
    public List filterList(List objects, Ticket ticket, String permissionType) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            return filterList(objects, ticket, permissionType, session);
        } finally {
            closeSession(session);
        }
    }

    /**
     *
     */

    /**
     * Narrow a list of objects using access control rules with an existing session
     *
     * @param objects        objects
     * @param ticket         ticket
     * @param permissionType w/r access
     * @param session        session
     * @return filtered list
     * @throws ConnexienceException something broke
     */
    public List filterList(List objects, Ticket ticket, String permissionType, Session session) throws ConnexienceException {
        List<Object> results = new ArrayList<>();
        try {
            for (Object object : objects) {
                if (object != null) {
                    if (canTicketAccessResource(session, ticket, (ServerObject) object, permissionType)) {
                        results.add(object);
                    }
                }
            }
            return results;
        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException(e.getMessage());
        }
    }

    /*
   * Get a list of the users who are connected to the current user and do not have access to the current object
   * */
    public List getUsersWithoutPermission(Ticket ticket, String serverId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (ticket != null) {
                Query q = session.createQuery("from User as up WHERE (up.id IN (SELECT l.sinkObjectId FROM Link as l WHERE l.sourceObjectId = :userId)) AND  (up.id NOT IN (SELECT perm.principalId from Permission as perm WHERE perm.targetObjectId = :serverId))");
                q.setString("userId", ticket.getUserId());
                q.setString("serverId", serverId);
                return q.list();
            } else {
                throw new ConnexienceException("Must supply valid ticket");
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error listing users ", e);
        } finally {
            closeSession(session);
        }
    }

    /*
   * Get a list of the groups that the current user is a member which do not have access to the obbect
   * */
    public Collection getGroupsWithoutPermission(Ticket ticket, String serverId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (ticket != null) {
                Query q = session.createQuery("from Group as g WHERE g.protectedGroup = :protectedStatus AND (g.id NOT IN (SELECT perm.principalId from Permission as perm WHERE perm.targetObjectId = :serverId)) AND (g.id IN (SELECT gm.groupId FROM GroupMembership as gm WHERE gm.userId = :userId))");
                q.setString("serverId", serverId);
                q.setString("userId", ticket.getUserId());
                q.setBoolean("protectedStatus", false);
                return q.list();
            } else {
                throw new ConnexienceException("Must supply valid ticket");
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error listing groups ", e);
        } finally {
            closeSession(session);
        }
    }
}

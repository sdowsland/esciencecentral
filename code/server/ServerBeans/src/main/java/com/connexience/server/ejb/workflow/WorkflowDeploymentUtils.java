/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.workflow;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.provenance.model.logging.events.ServiceSaveOperation;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.model.workflow.XMLDataExtractor;
import com.connexience.server.util.DigestBuilder;
import com.connexience.server.util.EarExtractor;
import com.connexience.server.util.StorageUtils;
import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.server.workflow.service.DataProcessorServiceDefinition;
import com.connexience.server.workflow.util.ZipUtils;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamReader;

/**
 * This class contains code to deploy workflow services and libraries into the
 * server from a local disk folder.
 * @author hugo
 */
public class WorkflowDeploymentUtils {
    private static Logger logger = Logger.getLogger(WorkflowDeploymentUtils.class);
    
    /** Deploy workflow library zip file into the server. This is used when dropping new libraries
     * into the library deployment directory */
    public static void deployLibraryFile(File zipFile, Ticket ticket, Ticket publicUserTicket) throws ConnexienceException {
        if(zipFile.exists() && zipFile.canRead() && !zipFile.getName().startsWith(".")){
            // Valid service zip, work out what to do with it
            User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            Folder uploadUserHome;
            if(u.getHomeFolderId()!=null && !u.getHomeFolderId().isEmpty()){
                uploadUserHome = EJBLocator.lookupStorageBean().getFolder(ticket, u.getHomeFolderId());
            } else {
                uploadUserHome = EJBLocator.lookupStorageBean().getDataFolder(ticket);
            }
            
            // Create the library folder
            Folder librariesFolder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, uploadUserHome.getId(), "Libraries");
            if(librariesFolder==null){
                librariesFolder = new Folder();
                librariesFolder.setName("Libraries");
                librariesFolder.setDescription("Automatically created by library deployer");
                librariesFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, uploadUserHome.getId(), librariesFolder);
                EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), librariesFolder.getId(), Permission.READ_PERMISSION);
            } else {
                if(!EJBLocator.lookupAccessControlBean().canTicketAccessResource(publicUserTicket, librariesFolder, Permission.READ_PERMISSION)){
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), librariesFolder.getId(), Permission.READ_PERMISSION);
                    logger.info("Fixed permission on libraries folder");
                }
            }            
        }
    }
    
    /** Deploy a workflow service zip file from a directory into the server. This is used when
     * dropping a new service into the service deployment directory */
    public static void deployServiceFile(File zipFile, Ticket ticket, Ticket publicUserTicket) throws ConnexienceException {
        if(zipFile.exists() && zipFile.canRead() && !zipFile.getName().startsWith(".")){
            // Process the zip file to see if we can access the service.xml document
            XMLDataExtractor extractor = new XMLDataExtractor(new String[]{"service.xml"});
            extractor.extractXmlData(zipFile);
            if(extractor.allDataPresent()){
                String serviceName = zipFile.getName();
                try {
                    DataProcessorServiceDefinition def = new DataProcessorServiceDefinition();
                    def.loadXmlString(extractor.getEntry("service.xml"));
                    serviceName = def.getName();
                    logger.debug("Found service name: "+ serviceName);
                } catch (Exception e){
                    throw new ConnexienceException("Error parsing service.xml file: " + e.getMessage());
                }
                
                // Valid service zip, work out what to do with it
                User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
                Folder uploadUserHome;
                if(u.getHomeFolderId()!=null && !u.getHomeFolderId().isEmpty()){
                    uploadUserHome = EJBLocator.lookupStorageBean().getFolder(ticket, u.getHomeFolderId());
                } else {
                    uploadUserHome = EJBLocator.lookupStorageBean().getDataFolder(ticket);
                }

                Folder serviceFolder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, uploadUserHome.getId(), "Services");
                if(serviceFolder==null){
                    serviceFolder = new Folder();
                    serviceFolder.setName("Services");
                    serviceFolder.setDescription("Automatically created by block deployer");
                    serviceFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, uploadUserHome.getId(), serviceFolder);
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), serviceFolder.getId(), Permission.READ_PERMISSION);
                } else {
                    if(!EJBLocator.lookupAccessControlBean().canTicketAccessResource(publicUserTicket, serviceFolder, Permission.READ_PERMISSION)){
                        EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), serviceFolder.getId(), Permission.READ_PERMISSION);
                        logger.info("Fixed permission on services folder");
                    }
                }

                // Is there an existing document here
                DocumentRecord doc = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, serviceFolder.getId(), serviceName);
                DynamicWorkflowService service = null;
                boolean doUpdate = false;

                if(doc!=null){
                    // Existing document
                    if(doc instanceof DynamicWorkflowService){
                        service = (DynamicWorkflowService)doc;
                        if(service.getTimeInMillis()!=zipFile.lastModified()){
                            // Need to update
                            service.setTimeInMillis(zipFile.lastModified());
                            service = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(ticket, service);
                            doUpdate = true;
                        }

                    } else {
                        throw new ConnexienceException("There is already a document: " + serviceName + " which is not a service");
                    }

                } else {
                    // New document
                    service = new DynamicWorkflowService();
                    service.setName(serviceName);
                    service.setContainerId(serviceFolder.getId());
                    service = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(ticket, service);

                    // Force the creation time to match the zipfile time
                    service.setTimeInMillis(zipFile.lastModified());
                    service = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(ticket, service);
                    doUpdate = true;
                }

                if(service!=null){
                    if(doUpdate){
                        logger.info("Processing service: " + serviceName);
                        DocumentVersion v = StorageUtils.upload(ticket, zipFile, service, "Uploaded by workflow deployer");
                        WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceXml(ticket, service.getId(), v.getId());
                        EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), service.getId(), Permission.READ_PERMISSION);

                        // Capture provenance
                        ServiceSaveOperation saveOp = new ServiceSaveOperation(service.getId(), v.getId(),v.getVersionNumber(), service.getName(),  ticket.getUserId(), new Date(System.currentTimeMillis()));
                        saveOp.setProjectId(ticket.getDefaultProjectId());
                        ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                        provClient.log(saveOp);    

                        logger.info("Service uploaded: " + serviceName);
                    } else {
                        // Make sure it is public
                        if(!EJBLocator.lookupAccessControlBean().canTicketAccessResource(publicUserTicket, doc, Permission.READ_PERMISSION)){
                            logger.info("Setting ACL on service: " + serviceName);
                            EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), service.getId(), Permission.READ_PERMISSION);
                        }
                    }
                }
            } else {
                throw new ConnexienceException("service.xml was not present in the service zip file");
            }

        } else {
            throw new ConnexienceException("Cannot read service zip file");
        }
        
    }
    
    /** Is a file a workflow document */
    public static boolean isFileWorkflowDocument(File file){
        if(file.length()<2000000){
            // Reasonable size
            FileInputStream stream = null;
            try {
                stream = new FileInputStream(file);
                XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(stream);
                XmlDataStore store = reader.read();

                // Look for valid drawing attributes
                if(store.containsName("BlockCount") && store.containsName("LinkCount")){
                    return true;
                } else {
                    return false;
                }

            } catch (Exception e){
                return false;
            } finally {
                try {stream.close();}catch(Exception e){}
            }
        } else {
            // Far too big to be a drawing
            return false;
        }
    }    
    
    public File createCoreLibraryZipFile() throws Exception {

        // Create a new core library
        String libraryDir = System.getProperty("jboss.server.temp.dir") + File.separator + "corelibrary";
        File libDir = new File(libraryDir);
        if(libDir.exists()){
            ZipUtils.removeDirectory(libDir);
        }

        if(!libDir.exists()){
            libDir.mkdirs();
        }

        String workingDir = System.getProperty("jboss.server.temp.dir") + File.separator + "corelibrary" + File.separator + "contents";

        File wd = new File(workingDir);
        if(!wd.exists()){
            wd.mkdirs();
        }

        String deployDir = System.getProperty("jboss.server.base.dir") + File.separator + "deployments";
        String earName = "esc.ear";

        logger.info("Building core library in: " + workingDir);
        logger.info("Using library files for core library from: " + deployDir + ": " + earName);


        EarExtractor extractor = new EarExtractor(earName, deployDir, workingDir + File.separator + "lib");
        extractor.addNamePattern("*server-common*.jar");
        extractor.addNamePattern("*workflow-engine*.jar");
        extractor.addNamePattern("*log4j-*.jar");

        ArrayList<File> extractedFiles = extractor.extract();

        // Copy in the xml files
        File libraryXml = new File(workingDir, "library.xml");
        ZipUtils.copyStreamToFile(getClass().getResourceAsStream("/corelibrary/library.xml"), libraryXml);

        File dependenciesXml = new File(workingDir, "dependencies.xml");
        ZipUtils.copyStreamToFile(getClass().getResourceAsStream("/corelibrary/dependencies.xml"), dependenciesXml);

        File zipFile = new File(libraryDir, "corelibrary.zip");
        ZipUtils.zip(new File(workingDir), zipFile);
        return zipFile;
    }
    
    public void updateCoreLibraryIfNeeded(Ticket ticket, User publicUser){
        FileInputStream stream = null;
        try {
            File zipFile = createCoreLibraryZipFile();
            DynamicWorkflowLibrary core= WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowLibraryByLibraryName(ticket, "core");
            boolean updateNeeded = false;
            if(core==null){
                // Have to do an update because it doesn't exist
                logger.info("Updating core library because it doesn't exist");
                recreateCoreLibrary(ticket, publicUser, zipFile);
                
            } else {
                // Need to check MD5s
                DocumentVersion v = EJBLocator.lookupStorageBean().getLatestVersion(ticket, core.getId());
                if(v!=null){
                    stream = new FileInputStream(zipFile);
                    String zipMd5 = DigestBuilder.calculateMD5(stream);
                    if(!zipMd5.equals(v.getMd5())){
                        // Different contents
                        logger.info("Different MD5 for core library: " + v.getMd5() + " : " + zipMd5);
                        recreateCoreLibrary(ticket, publicUser, zipFile);
                    } else {
                        // No change
                        logger.info("Core library up to date");
                    }
                    
                } else {
                    // No versions - do update
                    logger.info("No versions of core library - doing update");
                    recreateCoreLibrary(ticket, publicUser, zipFile);
                }
            }
        } catch (Exception e){
            logger.error("Error updating core library: " + e.getMessage());
        } finally {
            if(stream!=null){
                try {stream.close();}catch(Exception e){}
            }
        }
    }
    
    public void recreateCoreLibrary(Ticket ticket, User publicUser){
        try {
            File zipFile = createCoreLibraryZipFile();
            recreateCoreLibrary(ticket, publicUser, zipFile);
        } catch (Exception e){
            logger.error("Error recreating core library: " + e.getMessage());
        }
    }
    /** Create the core library */
    public void recreateCoreLibrary(Ticket ticket, User publicUser, File zipFile){
        try {
            //Ticket ticket = getDefaultOrganisationAdminTicket();
            DynamicWorkflowLibrary corelib = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowLibraryByLibraryName(ticket, "core");
            if(corelib!=null){
                // Remove existing core library
                EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, corelib.getId());
            }
            
            User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            Folder uploadUserHome;
            if(u.getHomeFolderId()!=null && !u.getHomeFolderId().isEmpty()){
                uploadUserHome = EJBLocator.lookupStorageBean().getFolder(ticket, u.getHomeFolderId());
            } else {
                uploadUserHome = EJBLocator.lookupStorageBean().getDataFolder(ticket);
            }
            
            corelib = new DynamicWorkflowLibrary();
            corelib.setName("corelibrary.zip");
            corelib.setLibraryName("core");
            corelib.setContainerId(uploadUserHome.getId());
            corelib.setCreatorId(u.getId());
            corelib.setOrganisationId(ticket.getOrganisationId());
            corelib = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(ticket, corelib);
            StorageUtils.upload(ticket, zipFile, corelib, "Core library created by internal workflow engine");
            //User publicUser = getPublicUser(ticket);
            EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getId(), corelib.getId(), Permission.READ_PERMISSION);
            
        } catch (Exception e){
            logger.error("Error creating workflow core library: " + e.getMessage());
        }
    }    
}

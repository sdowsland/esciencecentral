/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.properties;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.properties.PropertyGroup;
import com.connexience.server.model.properties.PropertyItem;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * This EJB provides the functionality to support property storage and retrieval.
 *
 * @author nhgh
 */
@Stateless
@EJB(name = "java:global/ejb/PropertiesBean", beanInterface = PropertiesRemote.class)
public class PropertiesBean extends HibernateSessionContainer implements PropertiesRemote {

    public PropertiesBean() {
        super();
    }

    /**
     * List the system property groups
     */
    public List getSystemPropertyGroups(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from PropertyGroup as obj where obj.objectProperty=:objectProperty ");
            q.setBoolean("objectProperty", false);
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing system property groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get all of the property groups for an object
     */
    public List getObjectPropertyGroups(Ticket ticket, String objectId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ServerObject object = getObject(objectId, ServerObject.class);
            assertPermission(ticket, object, Permission.READ_PERMISSION);
            Query q = session.createQuery("from PropertyGroup as obj where obj.objectId=?");
            q.setString(0, objectId);
            PropertyGroup group;
            List results = q.list();
            ArrayList populatedList = new ArrayList();

          for (Object result : results)
          {
            group = (PropertyGroup) result;
            populatePropertyGroup(group, session);
            populatedList.add(group);
          }
            return populatedList;

        } catch (Exception e) {
            throw new ConnexienceException("Error getting object property groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a single property for an object
     */
    public PropertyItem getProperty(Ticket ticket, String objectId, String propertyGroupName, String propertyName) throws ConnexienceException {

        Session session = null;
        try {
            session = getSession();
            ServerObject object = getObject(objectId, ServerObject.class);
            if (!ticket.isSuperTicket()) {
                assertPermission(ticket, object, Permission.READ_PERMISSION);
            }

            long id = getPropertyGroupId(objectId, propertyGroupName);
            if (id != -1) {
                return getPropertyValue(session, id, propertyName);
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting object property groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a specific property group
     */
    public PropertyGroup getPropertyGroup(Ticket ticket, long propertyGroupId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from PropertyGroup as obj where obj.id=?");
            q.setLong(0, propertyGroupId);
            List results = q.list();
            if (results.size() > 0) {
                PropertyGroup group = (PropertyGroup) results.get(0);
                if (group.isObjectProperty()) {
                    ServerObject obj = getObject(group.getObjectId(), ServerObject.class);
                    assertPermission(ticket, obj, Permission.READ_PERMISSION);
                }
                populatePropertyGroup(group, session);
                return group;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting property group: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a single property group for an object
     */
    public PropertyGroup getPropertyGroup(Ticket ticket, String objectId, String propertyGroupName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            ServerObject object = getObject(objectId, ServerObject.class);
            assertPermission(ticket, object, Permission.READ_PERMISSION);
            Query q = session.createQuery("from PropertyGroup as obj where obj.objectId=? and obj.name=? ");
            q.setString(0, objectId);
            q.setString(1, propertyGroupName);
            List results = q.list();
            if (results.size() > 0) {
                PropertyGroup group = (PropertyGroup) results.get(0);
                populatePropertyGroup(group, session);
                return group;
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error getting object property groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the value of a single system property
     */
    public PropertyItem getSystemProperty(Ticket ticket, String propertyGroupName, String propertyName) throws ConnexienceException {
        Session session = null;

        try {
            session = getSession();
            long id = getSystemPropertyGroupId(ticket, propertyGroupName);
            if (id != -1) {
                return getPropertyValue(session, id, propertyName);
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting object property groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Remove a single property from the database
     */
    public void removeProperty(Ticket ticket, long propertyId) throws ConnexienceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Remove a group of properties
     */
    public void removePropertyGroup(Ticket ticket, long propertyGroupId) throws ConnexienceException {
        Session session = null;
        try {
            PropertyGroup group = getPropertyGroup(ticket, propertyGroupId);
            session = getSession();

            // Check permissions
            if (group.isObjectProperty()) {
                ServerObject obj = getObject(group.getObjectId(), ServerObject.class);
                assertPermission(ticket, obj, Permission.WRITE_PERMISSION);
            } else {
                if (!ticket.isSuperTicket()) {
                    throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
                }
            }

            // Remove all of the individual properties
            EJBLocator.lookupObjectRemovalBean().remove(group);

        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error deleting property group" + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Save a group of properties including the individual items
     */
    public PropertyGroup savePropertyGroup(Ticket ticket, PropertyGroup propertyGroup) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (propertyGroup.getOrganisationId() == null) {
                propertyGroup.setOrganisationId(ticket.getOrganisationId());
            } else {
                // Check from the same organisation
                if (!propertyGroup.getOrganisationId().equals(ticket.getOrganisationId())) {
                    throw new ConnexienceException(ConnexienceException.INCORRECT_ORGANIATION_MESSAGE);
                }
            }

            // Is this an object property or a system property
            if (propertyGroup.isObjectProperty()) {
                // Check for write permission on the object
                ServerObject obj = getObject(propertyGroup.getObjectId(), ServerObject.class);
                assertPermission(ticket, obj, Permission.WRITE_PERMISSION);

            } else {
                // Check this is an admin ticket
                if (!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, getObject(ticket.getOrganisationId(), Organisation.class), Permission.WRITE_PERMISSION)) {
                    throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
                }
            }

            // Save the group
            PropertyGroup saved = (PropertyGroup) savePlainObject(propertyGroup, session);
            Enumeration<PropertyItem> properties = propertyGroup.getProperties().elements();
            Hashtable<String, PropertyItem> copyProperties = new Hashtable<>();
            PropertyItem item;
            PropertyItem copy;

            // Delete the existing properties for this group
            SQLQuery q = session.createSQLQuery("delete from propertyitems where groupid=?");
            q.setLong(0, saved.getId());
            q.executeUpdate();
            
            
            // Now save the new properties
            while (properties.hasMoreElements()) {
                item = (PropertyItem)properties.nextElement();
                item = (PropertyItem) savePlainObject(item.copyWithoutId(), session);
                copy = new PropertyItem(item);
                copyProperties.put(copy.getName(), copy);
            }
            saved.setProperties(copyProperties);
            return saved;

        } catch (ConnexienceException ce) {
            throw ce;

        } catch (Exception e) {
            throw new ConnexienceException("Cannot save property group: " + e.getMessage());
        } finally {
            closeSession(session);
        }

    }

    /**
     * Set a property for an object
     */
    public void setProperty(Ticket ticket, String objectId, String propertyGroupName, String propertyName, String propertyValue) throws ConnexienceException {
        Session session = null;
        try {
            // Does the group exist
            long id = getPropertyGroupId(objectId, propertyGroupName);
            PropertyGroup group;
            if (id == -1) {
                // Create it if not
                group = new PropertyGroup();
                group.setDescription("Property group: " + propertyGroupName);
                group.setName(propertyGroupName);
                group.setObjectId(objectId);
                group.setObjectProperty(true);
                group.setOrganisationId(ticket.getOrganisationId());
                group = savePropertyGroup(ticket, group);
            } else {
                // NEED TO GET THE UNPOPULATED GROUP
                group = getUnpopulatedPropertyGroup(id);
            }

            // Get a named property in this group
            PropertyItem item;
            if (group != null) {
                session = getSession();
                item = getPropertyValue(session, group.getId(), propertyName);
                if (item != null) {
                    item.setValue(propertyValue);
                    savePlainObject(item, session);
                } else {
                    item = new PropertyItem();
                    item.setGroupId(group.getId());
                    item.setName(propertyName);
                    item.setValue(propertyValue);
                    savePlainObject(item);
                }
            }

        } catch (ConnexienceException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConnexienceException("Error setting property: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Set the value of a system level property
     */
    public void setSystemProperty(Ticket ticket, String propertyGroupName, String propertyName, String propertyValue) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg() && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, getObject(ticket.getOrganisationId(), Organisation.class), Permission.WRITE_PERMISSION)) {
            try {
                // Does the group exist
                long id = getSystemPropertyGroupId(ticket, propertyGroupName);

                PropertyGroup group;
                if (id == -1) {
                    // Create it if not
                    group = new PropertyGroup();
                    group.setDescription("Property group: " + propertyGroupName);
                    group.setName(propertyGroupName);
                    group.setObjectId("");
                    group.setObjectProperty(false);
                    group.setOrganisationId(ticket.getOrganisationId());
                    group = savePropertyGroup(ticket, group);
                } else {
                    // NEED TO GET THE UNPOPULATED GROUP
                    group = getUnpopulatedPropertyGroup(id);
                }

                // Get a named property in this group
                PropertyItem item;
                if (group != null) {
                    session = getSession();
                    item = getPropertyValue(session, group.getId(), propertyName);
                    if (item != null) {
                        item.setValue(propertyValue);
                        savePlainObject(item, session);
                    } else {
                        item = new PropertyItem();
                        item.setGroupId(group.getId());
                        item.setName(propertyName);
                        item.setValue(propertyValue);
                        savePlainObject(item);
                    }
                }

            } catch (ConnexienceException ce) {
                throw ce;
            } catch (Exception e) {
                throw new ConnexienceException("Error setting property: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ADMIN_ONLY);
        }
    }

    /** Remove a property from a group by name */
    public void removeProperty(Ticket ticket, String objectId, String propertyGroupName, String propertyName) throws ConnexienceException {
        Session session = null;
        try {
            PropertyItem property = getProperty(ticket, objectId, propertyGroupName, propertyName);
            if(property!=null){
                session = getSession();
                session.delete(property);
            }
        } catch (ConnexienceException ce){
            throw ce;
        } catch (Exception e){
            throw new ConnexienceException("Error removing property: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get a specific property group that is unpopulated
     */
    private PropertyGroup getUnpopulatedPropertyGroup(long propertyGroupId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from PropertyGroup as obj where obj.id=?");
            q.setLong(0, propertyGroupId);
            List results = q.list();
            if (results.size() > 0) {
                return (PropertyGroup) results.get(0);
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting property group: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Populate a property group with properties
     */
    private void populatePropertyGroup(PropertyGroup group, Session session)
    {
        Query q = session.createQuery("from PropertyItem as obj where obj.groupId=? order by obj.name");
        q.setLong(0, group.getId());
        List results = q.list();
        PropertyItem item;
        Hashtable<String, PropertyItem> properties = new Hashtable<>();
      for (Object result : results)
      {
        item = (PropertyItem) result;
        properties.put(item.getName(), item);
      }
        group.setProperties(properties);
    }

    /**
     * Get the database id for a property group
     */
    private long getSystemPropertyGroupId(Ticket ticket, String propertyGroupName) throws Exception {
        ResultSet r = null;
        PreparedStatement s = null;
        Connection c = null;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("select id from propertygroups where name=? and organisationid=? and objectproperty=?");
            s.setString(1, propertyGroupName);
            s.setString(2, ticket.getOrganisationId());
            s.setBoolean(3, false);
            
            r = s.executeQuery();
            if (r.next()) {
                return r.getLong(1);
            } else {
                return -1;
            }
        } finally {
            try {
                r.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
            try {
                s.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
            try {
                c.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
    }

    /**
     * Get the id of a property group for a specific object
     */
    private long getPropertyGroupId(String objectId, String propertyGroupName) throws Exception {
        ResultSet r = null;
        PreparedStatement s = null;
        Connection c = null;
        try {
            // Removed "and organisationid=?" from the select query since for notifications, the NotificationMDB needs to access
            // a users properties.  This can not be done with the internal ticket with this line in the query since the internalTicket organisation
            // is not the same as the users

            c = getSQLConnection();
            s = c.prepareStatement("select id from propertygroups where name=? and objectproperty=? and objectid=?");
            s.setString(1, propertyGroupName);
            s.setBoolean(2, true);
            s.setString(3, objectId);
            r = s.executeQuery();
            if (r.next()) {
                return r.getLong(1);
            } else {
                return -1;
            }

         } finally {
            try {
                r.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
            try {
                s.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
            try {
                c.close();
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
    }

    /**
     * Get the value of a property using a raw database connection
     */
    private PropertyItem getPropertyValue(Session s, long propertyGroupId, String propertyName)
    {
            Query q = s.createQuery("from PropertyItem as obj where obj.groupId=? and obj.name=?");
            q.setLong(0, propertyGroupId);
            q.setString(1, propertyName);
            List results = q.list();
            if (results.size() > 0) {
                return (PropertyItem) results.get(0);
            } else {
                return null;
            }
    }
}

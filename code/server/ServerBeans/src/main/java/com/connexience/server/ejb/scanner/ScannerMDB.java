/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scanner;

import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.scanner.RemoteFilesystemObject;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import java.util.Date;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.log4j.Logger;

/**
 * This message driven bean imports a single file from a remote filesystem into
 * the local system.
 * @author hugo
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/remotefileimports")
}, mappedName = "queue/remotefileimports")
public class ScannerMDB extends HibernateSessionContainer implements MessageListener {
    private static Logger logger = Logger.getLogger(ScannerMDB.class);
    
    @Override
    public void onMessage(Message msg) {
        if(msg instanceof TextMessage){
            TextMessage tm = (TextMessage)msg;
            try {
                if(tm.getText().equals("IMPORTFILE")){
                    // Suck in a remote file
                    String userId = tm.getStringProperty("UserID");
                    long objectId = tm.getLongProperty("ObjectID");
                    
                    Ticket userTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                    
                    RemoteFilesystemObject fsObject = EJBLocator.lookupScannerBean().getFileSystemObjectWithoutSecurity(objectId);
                    if(fsObject!=null && fsObject.getStatus()!=null && fsObject.getStatus().equals(RemoteFilesystemObject.QUEUED)){
                        // Get the scanner
                        RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(userTicket, fsObject.getScannerId());
                        if(scanner!=null){
                            // Do the upload
                            String documentId = scanner.importRemoteFile(userTicket, fsObject);
                            
                            // Remove the file and its record in the database if needed
                            if(scanner.isDeleteUploadedFiles()){
                                scanner.removeRemoteFile(userTicket, fsObject);
                                EJBLocator.lookupScannerBean().removeFileSystemObjectWithoutSecurity(fsObject.getId());
                            }
                            
                            // Execute the workflow on the file
                            if(documentId!=null && !documentId.isEmpty() && scanner.isAutomaticWorkflowEnabled() && scanner.getWorkflowId()!=null && !scanner.getWorkflowId().isEmpty()){
                                // Switch the ticket to have the scanners project ID if it is a project scanner
                                if(scanner.isStudyScanner()){
                                    Project p = EJBLocator.lookupProjectsBean().getProject(userTicket, (int)scanner.getStudyId());
                                    userTicket.setDefaultProjectId(Integer.toString(p.getId()));
                                    userTicket.setDefaultStorageFolderId(p.getDataFolderId());
                                }
                                
                                WorkflowInvocationMessage invocationMsg = new WorkflowInvocationMessage(userTicket, scanner.getWorkflowId());
                                invocationMsg.setTargetFileId(documentId);
                                fsObject.setInvocationId(WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(userTicket, invocationMsg));
                                fsObject = EJBLocator.lookupScannerBean().saveFileSystemObjectWithoutSecurity(fsObject);
                            }
                            
                        } else {
                            logger.error("Cannot locate scanner: " + fsObject.getScannerId());
                        }
                        
                    } else {
                        logger.info("Filesystem object no longer in database or not in correct state: " + objectId);
                    }
                        
                } else if(tm.getText().equals("SCANCHANGES")){
                    // Scan remote file system for changes
                    String userId = tm.getStringProperty("UserID");
                    long scannerID = tm.getLongProperty("ScannerID");
                    Ticket userTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                    RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(userTicket, scannerID);
                    if(scanner!=null){
                        scanner.setLastScanTime(new Date());
                        scanner = EJBLocator.lookupScannerBean().saveScanner(userTicket, scanner);
                        scanner.scanForChanges(userTicket);                        
                    }
                }
                
            } catch (Exception e){
                logger.error("Error processing message: " + e.getMessage());
            }
        }
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.security.Ticket;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * This bean provides basic information about objects such as name and description. It
 * bypasses Hibernate and just uses a basic SQL select statment to pull the relevant
 * information back from the database.
 *
 * @author hugo
 */
@javax.ejb.Stateless
@EJB(name = "java:global/ejb/ObjectInfoBean", beanInterface = ObjectInfoRemote.class)
public class ObjectInfoBean extends HibernateSessionContainer implements com.connexience.server.ejb.directory.ObjectInfoRemote, com.connexience.server.ejb.directory.ObjectInfoLocal {

    @Resource(mappedName = "java:jboss/datasources/ConnexienceDB")
    private DataSource ConnexienceDB;

    /**
     * Creates a new instance of ObjectInfoBean
     */
    public ObjectInfoBean() {
        super();
    }

    /**
     * Does an organisation contain a specific object
     */
    public boolean containsObject(Ticket ticket, String objectId) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        Statement s = null;

        try {
            c = ConnexienceDB.getConnection();
            s = c.createStatement();
            if (ticket.isAssociatedWithNonRootOrg()) {
                r = s.executeQuery("SELECT name FROM objectsflat WHERE id='" + objectId + "' and organisationid='" + ticket.getOrganisationId() + "'");
            } else {
                r = s.executeQuery("SELECT name FROM objectsflat WHERE id='" + objectId + "'");
            }
            if (r.next()) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting object name: " + e.getMessage());
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }

        }
    }

    /**
     * Get the names of a collection of objects
     */
    public String[] getObjectNames(Ticket ticket, String[] objectIds) throws ConnexienceException {

        Connection c = null;
        ResultSet r = null;
        PreparedStatement s = null;

        String[] results = new String[objectIds.length];

        try {
            c = ConnexienceDB.getConnection();
            if (ticket.isAssociatedWithNonRootOrg()) {
                s = c.prepareStatement("SELECT name FROM objectsflat WHERE id=? and organisationid=?");
            } else {
                s = c.prepareStatement("SELECT name FROM objectsflat WHERE id=?");
            }

            for (int i = 0; i < objectIds.length; i++) {
                s.setString(1, objectIds[i]);
                if (ticket.isAssociatedWithNonRootOrg()) {
                    s.setString(2, ticket.getOrganisationId());
                }
                r = s.executeQuery();
                if (r.next()) {
                    results[i] = r.getString(1);
                } else {
                    results[i] = "UNKNOWN";
                }
            }
            return results;

        } catch (Exception e) {
            throw new ConnexienceException("Error getting object name: " + e.getMessage());
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }

        }
    }

    /**
     * Get the name of an object
     */
    public String getObjectName(Ticket ticket, String objectId) throws ConnexienceException {
        Connection c = null;
        ResultSet r = null;
        Statement s = null;

        try {
            c = ConnexienceDB.getConnection();
            s = c.createStatement();

            if (ticket.isAssociatedWithNonRootOrg()) {
                r = s.executeQuery("SELECT name FROM objectsflat WHERE id='" + objectId + "' and organisationid='" + ticket.getOrganisationId() + "'");
            } else {
                r = s.executeQuery("SELECT name FROM objectsflat WHERE id='" + objectId + "'");
            }

            if (r.next()) {
                return r.getString("name");
            } else {
                return "UNKNOWN";
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting object name: " + e.getMessage());
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Get the name of an object without bothering to filter by organisation
     */
    private String getLocalObjectName(Connection c, String objectId) throws Exception {
        Statement s = null;
        ResultSet r = null;
        try {
            s = c.createStatement();
            r = s.executeQuery("select name from objectsflat where id='" + objectId + "'");
            if (r.next()) {
                return r.getString(1);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.crypto.ServerObjectKeyManager;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.HibernateUtil;
import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.OrganisationFolderAccess;
import com.connexience.server.ejb.util.RootSecurityObjectManager;
import com.connexience.server.model.archive.RESTArchiveStore;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.storage.FileDataStore;
import java.io.File;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jboss.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

/**
 * This class provides a directory of organisations that are present
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/OrganisationDirectoryBean", beanInterface = OrganisationDirectoryRemote.class)
public class OrganisationDirectoryBean extends HibernateSessionContainer implements com.connexience.server.ejb.directory.OrganisationDirectoryRemote {
    private static Logger logger = Logger.getLogger(OrganisationDirectoryBean.class);
    /**
     * Creates a new instance of OrganisationDirectorybeanBean
     */
    public OrganisationDirectoryBean() {
        super();
    }

    /**
     * Save an organisation to the database
     */
    public Organisation saveOrganisation(Ticket ticket, Organisation org) throws ConnexienceException {

        // Is this a new object
        boolean newOrg = false;
        if (org.getId() == null) {
            newOrg = true;
        }

        Organisation saved;
        if (newOrg) {
            // Newly created organisation - only root can do this
            //Session session = null;
            if (ticket.isSuperTicket()) {
                /*
                try {
                    session = getSession();
                    session.persist(org);
                    org.setOrganisationId(org.getId());
                    org.setContainerId(org.getId());
                    session.update(org);
                    saved = org;

                } catch (Exception e) {
                    throw new ConnexienceException("Error saving organisation: " + e.getMessage());
                } finally {
                    closeSession(session);
                }
                 */

                saved = (Organisation) saveObject(ticket, org);
                saved.setOrganisationId(saved.getId());
                saved.setContainerId(saved.getId());
                saved = (Organisation) saveObject(ticket, saved);
            } else {
                throw new ConnexienceException("Only ROOT can create new organisations");
            }

        } else {
            // Update and save existing organisation
            saved = (Organisation) saveObjectWithAcl(ticket, org);
        }

        // Create Keys for the organisation if they don't exist
        ServerObjectKeyManager keyMan = new ServerObjectKeyManager();
        keyMan.setupObjectKeyData(saved);

        // Create the special folders for the organisation. These will only
        // be created if they do not already exist
        OrganisationFolderAccess folders = new OrganisationFolderAccess(saved);
        folders.createSpecialFolders(ticket);
        removePublicUserId(saved.getId());

        // Remove the organisation from the cache
        flushCaches();
        return saved;
    }

    /**
     * List all of the organisations
     */
    public List listOrganisations(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Organisation as obj");
            return q.list();

        } catch (Exception e) {
            throw new ConnexienceException("Error listing organisations: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Search for organisations
     */
    public List searchOrganisations(Ticket ticket, String searchText) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Organisation as obj where obj.name like ?");
            q.setString(0, "%" + searchText + "%");
            return q.list();

        } catch (Exception e) {
            throw new ConnexienceException("Error searching for organisations: " + e.getMessage());
        } finally {
            closeSession(session);
        }

    }

    /**
     * Get an organisation by id
     */
    public Organisation getOrganisation(Ticket ticket, String organisationId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Organisation as obj where obj.id=?");
            q.setString(0, organisationId);
            return (Organisation) q.uniqueResult();
        } catch (Exception e) {
            throw new ConnexienceException("Error retrieving organisation: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get an organisation by name
     */
    public Organisation getOrganisationByName(Ticket ticket, String name) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Organisation as obj where obj.name=?");
            q.setString(0, name);
            Object obj = q.uniqueResult();
            if (obj != null) {
                return (Organisation) obj;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error finding organisation: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * List all of the groups in an organisation
     */
    public List listOrganisationGroups(Ticket ticket, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q = session.createQuery("from Group as obj");
                q.setFirstResult(start);
                if (maxResults > 0) {
                    q.setMaxResults(maxResults);
                }
                return q.list();

            } catch (Exception e) {
                throw new ConnexienceException("Error listing groups: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /**
     * List all of the groups in an organisation
     * Kept for backwards compatibility
     */
    public List listOrganisationGroups(Ticket ticket, String organisationId) throws ConnexienceException {

        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Group as obj ");
            return q.list();

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }

    }

    /**
     * List all of the groups in an organisation
     */
    public List listOrganisationNonProtectedGroups(Ticket ticket, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        if (ticket.isAssociatedWithNonRootOrg()) {
            try {
                session = getSession();
                Query q = session.createQuery("from Group as obj WHERE obj.protectedGroup = false ORDER BY obj.name ASC");
                q.setFirstResult(start);
                if (maxResults > 0) {
                    q.setMaxResults(maxResults);
                }
                return q.list();

            } catch (Exception e) {
                throw new ConnexienceException("Error listing groups: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.NO_ORGANISATION_SPECIFIED_MESSAGE);
        }
    }

    /**
     * List all of the users in an organisation
     */
    public List listOrganisationUsers(Ticket ticket, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from User as obj  ORDER BY obj.id ASC");

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }

            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }


    /**
     * List all of the users in an organisation
     */
    public List listNonProtectedOrganisationUsers(Ticket ticket, int start, int maxResults) throws ConnexienceException {
        return listNonProtectedOrganisationUsers(ticket, OrderBy.id, SearchOrder.ASC, start, maxResults);
    }


    public List listNonProtectedOrganisationUsers(Ticket ticket, OrderBy orderBy, SearchOrder ascDesc, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            String s = "from User as obj WHERE obj.protectedUser IS false ORDER BY obj." + orderBy + " " + ascDesc;
            Query q = session.createQuery(s);

            q.setFirstResult(start);
            if (maxResults > 0) {
                q.setMaxResults(maxResults);
            }

            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Get the number of users within an organisation
     */
    public int numberOfOrganisationUsers(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if (ticket != null) {
                Query q = session.createQuery("from User as obj " );
                return q.list().size();
            } else {
                throw new ConnexienceException("Must supply valid ticket");
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error listing users ", e);
        } finally {
            closeSession(session);
        }
    }

    /**
     * get the number of groups in an organisation
     */
    public int numberOfOrganisationGroups(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Group as obj ");

            return q.list().size();
        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);

        }
    }

    /**
     * get the number of non protected groups in an organisation
     */
    public int numberOfOrganisationNonProtectedGroups(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Group as obj WHERE obj.protectedGroup = false " );

            return q.list().size();
        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);

        }
    }

    /**
     * Get the default organisation. This is currently just the first in the database
     */
    public Organisation getDefaultOrganisation(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Organisation");
            List results = q.list();
            if (results.size() > 0) {
                return (Organisation) results.get(0);
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error getting default organisation: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /**
     * Set up a new organisation
     */
    public Organisation setupNewOrganisation(String name, String storageDir, String awsAccessKey, String awsSecretKey, String awsDomainName, String awsGlacierVaultName,
                                             String adminGroup, String userGroup,
                                             String adminFirstname, String adminLastname, String adminUsername, String adminPassword,
                                             String gmailUser, String gmailPassword) throws ConnexienceException {
        // Create a root ticket
        RootSecurityObjectManager mgr = new RootSecurityObjectManager();
        Ticket rootTicket = new Ticket();
        rootTicket.setUserId(mgr.getRootSecurityObject().getRootUserId());
        rootTicket.setOrganisationId(Ticket.ROOT_ORGANISATION_ID);
        rootTicket.setSuperTicket(true);
        // Is there already an organisation
        Organisation existingOrg = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(rootTicket);
        if(existingOrg!=null){
            throw new ConnexienceException("An Organisation: " + existingOrg.getName() + " already exists");
        }

                
        // Create the new organisation
        Organisation no = new Organisation();
        no.setName(name);
        
        no.setCreatorId(rootTicket.getUserId());
        Organisation org = (Organisation) saveObject(rootTicket, no);
        org.setContainerId(org.getId());
        org.setOrganisationId(org.getId());
        org.setCreatorId(rootTicket.getUserId());
        org.setGmailUser(gmailUser);
        org.setGmailPassword(gmailPassword);
        org.setAccessKey(EJBLocator.lookupPreferencesBean().getMacAddress());
        org = (Organisation) savePlainObject(org);

        // Create Keys for the organisation if they don't exist
        ServerObjectKeyManager keyMan = new ServerObjectKeyManager();
        keyMan.setupObjectKeyData(org);

        // Make sure the correct folders exist 
        ApplicationsFolder appFolder = new ApplicationsFolder();
        appFolder.setName("Applications");
        appFolder.setContainerId(org.getId());
        appFolder.setOrganisationId(org.getId());
        appFolder.setCreatorId(rootTicket.getUserId());
        appFolder = (ApplicationsFolder) savePlainObject(appFolder);
        org.setApplicationsFolderId(appFolder.getId());

        DataFolder dataFolder = new DataFolder();
        dataFolder.setName("Data");
        dataFolder.setContainerId(org.getId());
        dataFolder.setOrganisationId(org.getId());
        dataFolder.setCreatorId(rootTicket.getUserId());
        dataFolder = (DataFolder) savePlainObject(dataFolder);
        org.setDataFolderId(dataFolder.getId());

        DocumentTypesFolder docTypesFolder = new DocumentTypesFolder();
        docTypesFolder.setName("Document Types");
        docTypesFolder.setContainerId(org.getId());
        docTypesFolder.setOrganisationId(org.getId());
        docTypesFolder.setCreatorId(rootTicket.getUserId());
        docTypesFolder = (DocumentTypesFolder) savePlainObject(docTypesFolder);
        org.setDocumentTypesFolderId(docTypesFolder.getId());

        GroupsFolder groupsFolder = new GroupsFolder();
        groupsFolder.setName("Groups");
        groupsFolder.setContainerId(org.getId());
        groupsFolder.setOrganisationId(org.getId());
        groupsFolder.setCreatorId(rootTicket.getUserId());
        groupsFolder = (GroupsFolder) savePlainObject(groupsFolder);
        org.setGroupFolderId(groupsFolder.getId());

        UsersFolder usersFolder = new UsersFolder();
        usersFolder.setName("Users");
        usersFolder.setContainerId(org.getId());
        usersFolder.setOrganisationId(org.getId());
        usersFolder.setCreatorId(rootTicket.getUserId());
        usersFolder = (UsersFolder) savePlainObject(usersFolder);
        org.setUserFolderId(usersFolder.getId());

        org = (Organisation) savePlainObject(org);

        // Create the admin group
        Group admins = new Group();
        admins.setName(adminGroup);
        admins.setOrganisationId(org.getId());
        admins.setContainerId(groupsFolder.getId());
        admins.setCreatorId(rootTicket.getUserId());
        admins.setProtectedGroup(true);
        admins = (Group) savePlainObject(admins);

        // Create the Users group
        Group users = new Group();
        users.setName(userGroup);
        users.setOrganisationId(org.getId());
        users.setCreatorId(rootTicket.getUserId());
        users.setContainerId(groupsFolder.getId());
        users.setProtectedGroup(true);
        users = (Group) savePlainObject(users);

        // Create the StorageAPI group
        Group storageApi = new Group();
        storageApi.setName("StorageAPI");
        storageApi.setOrganisationId(org.getId());
        storageApi.setCreatorId(rootTicket.getUserId());
        storageApi.setContainerId(groupsFolder.getId());
        storageApi.setProtectedGroup(true);
        storageApi = (Group)savePlainObject(storageApi);
        
        // Create the StorageAPI group
        Group workflowApi = new Group();
        workflowApi.setName("WorkflowAPI");
        workflowApi.setOrganisationId(org.getId());
        workflowApi.setCreatorId(rootTicket.getUserId());
        workflowApi.setContainerId(groupsFolder.getId());
        workflowApi.setProtectedGroup(true);
        workflowApi = (Group)savePlainObject(workflowApi);
        
        // Create the StorageAPI group
        Group datasetApi = new Group();
        datasetApi.setName("DatasetAPI");
        datasetApi.setOrganisationId(org.getId());
        datasetApi.setCreatorId(rootTicket.getUserId());
        datasetApi.setContainerId(groupsFolder.getId());
        datasetApi.setProtectedGroup(true);
        datasetApi = (Group)savePlainObject(datasetApi);
        
        // Create the public user group
        Group publicGroup = new Group();
        publicGroup.setName("Public");
        publicGroup.setOrganisationId(org.getId());
        publicGroup.setContainerId(groupsFolder.getId());
        publicGroup.setCreatorId(rootTicket.getUserId());
        publicGroup.setProtectedGroup(true);
        publicGroup = (Group) savePlainObject(publicGroup);

        // Create the public user
        User publicUser = new User();
        publicUser.setFirstName("Public");
        publicUser.setSurname("User");
        publicUser.setName("Public User");
        publicUser.setDefaultGroupId(publicGroup.getId());
        publicUser.setOrganisationId(org.getId());
        publicUser.setContainerId(usersFolder.getId());
        publicUser.setProtectedUser(true);
        publicUser = (User) savePlainObject(publicUser);
        publicUser.setCreatorId(publicUser.getId());
        publicUser = (User) savePlainObject(publicUser);

        // Create the data store
        FileDataStore store = new FileDataStore();
        
        if(HibernateUtil.isPostgres){
            // Absoulte directory for postgres
            store.setDirectory(storageDir);
        } else {
            // Store within JBoss for H2 databsae
            File baseDir = new File(System.getProperty("jboss.home.dir") + File.separator + "esc");
            if(!baseDir.exists()){
                baseDir.mkdirs();
            }
            File storagePath = new File(baseDir, storageDir);
            store.setDirectory(storagePath.getPath());
        }
        
        store.setOrganisationId(org.getId());
        store.setName("File data store");
        store.setCreatorId(rootTicket.getUserId());
        store = (FileDataStore) savePlainObject(store);
/* Also change createorganisation.jsp to match */
/*
        // Create the archive store
        GlacierArchiveStore archive = null;
        try {
            EJBLocator.lookupJobMonitorBean().shutdownWorkers();
            EJBLocator.lookupUnarchiveJobMonitorBean().shutdownWorkers();
            archive = new GlacierArchiveStore();
            archive.setOrganisationId(org.getId());
            archive.setName("Glacier archive store");
            archive.setCreatorId(rootTicket.getUserId());
            archive.setAccessKey(awsAccessKey);
            archive.setSecretKey(awsSecretKey);
            archive.setDomainName(awsDomainName);
            archive.setVaultName(awsGlacierVaultName);
            archive.setupArchive();
            archive = (GlacierArchiveStore) savePlainObject(archive);
            EJBLocator.lookupJobMonitorBean().startWorkers();
            EJBLocator.lookupUnarchiveJobMonitorBean().startWorkers();
        } catch (Throwable throwable){
            System.out.println("Unable to create AWS Glacier archive store: " + throwable.getMessage());
        }
*/
/* Also change createorganisation.jsp to match */
        // Create the archive store
        RESTArchiveStore archive = null;
        try {
            archive = new RESTArchiveStore();
            archive.setOrganisationId(org.getId());
            archive.setName("REST archive store");
            archive.setCreatorId(rootTicket.getUserId());
            archive.setServiceURL(awsDomainName);
            archive = (RESTArchiveStore) savePlainObject(archive);
        } catch (Throwable throwable) {
            System.out.println("Unable to create REST archive store: " + throwable.getMessage());
        }

        // Save everything
        org.setDataStoreId(store.getId());
        if (archive != null)
            org.setArchiveStoreId(archive.getId());
        org.setAdminGroupId(admins.getId());
        org.setPublicGroupId(publicGroup.getId());
        org.setDefaultGroupId(users.getId());
        org.setDefaultUserId(publicUser.getId());
        org = (Organisation) savePlainObject(org);

        // Set permissions
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), appFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), appFolder.getId(), Permission.ADD_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), appFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), appFolder.getId(), Permission.ADD_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), appFolder.getId(), Permission.WRITE_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), dataFolder.getId(), Permission.ADD_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), dataFolder.getId(), Permission.READ_PERMISSION);

        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicGroup.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicUser.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), docTypesFolder.getId(), Permission.WRITE_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), docTypesFolder.getId(), Permission.ADD_PERMISSION);

        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), groupsFolder.getId(), Permission.ADD_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), groupsFolder.getId(), Permission.ADD_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicGroup.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicUser.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);

        //allow everyone to read the public user
        EJBLocator.lookupAccessControlBean().grantUniversalAccess(rootTicket, users.getId(), publicUser.getId(), Permission.READ_PERMISSION);


        // Create some datatypes
        try {
            Properties props = new Properties();
            props.load(getClass().getResourceAsStream("/com/connexience/server/ejb/mime/mimetypes.properties"));
            Enumeration names = props.propertyNames();
            String propertyName;
            String propertyValue;
            DocumentType docType;
            while (names.hasMoreElements()) {
                propertyName = (String) names.nextElement();
                propertyValue = props.getProperty(propertyName);
                if (propertyValue != null) {
                    docType = new DocumentType(propertyValue, propertyName);
                    docType.setOrganisationId(org.getId());
                    docType.setCreatorId(rootTicket.getUserId());
                    docType.setContainerId(org.getDocumentTypesFolderId());
                    savePlainObject(docType);
                }
            }
        } catch (Exception e) {
            System.out.println("Error opening mimetypes list: " + e.getMessage());
        }

        //Create an admin account that can log into the WebAdmin webapp
        User adminUser = new User();
        adminUser.setName(adminUsername);
        adminUser.setFirstName(adminFirstname);
        adminUser.setSurname(adminLastname);
        adminUser = EJBLocator.lookupUserDirectoryBean().createAccount(adminUser, adminUsername, adminPassword);
        EJBLocator.lookupGroupDirectoryBean().addUserToGroup(rootTicket, adminUser.getId(), admins.getId());

        //Make the admins group owned by the admin so that other people can join it
        admins.setCreatorId(adminUser.getId());
        savePlainObject(admins);

        // Make sure the root user has the correct folder set up
        User root = EJBLocator.lookupUserDirectoryBean().getUser(rootTicket, rootTicket.getUserId());
        root.setHomeFolderId(dataFolder.getId());
        savePlainObject(root);
        
        // Kick off any scheduled tasks
        if(SchedulerBean.SCHEDULER_INSTANCE!=null){
            SchedulerBean.SCHEDULER_INSTANCE.rescheduleSingleTasks();
        }
        return org;
    }

    @Override
    public void resetAccessKeyToLocalMacAddress() throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(HibernateSessionContainer.INTERNAL_TICKET);
        if(org!=null){
            org.setAccessKey(EJBLocator.lookupPreferencesBean().getMacAddress());
            org = EJBLocator.lookupOrganisationDirectoryBean().saveOrganisation(HibernateSessionContainer.INTERNAL_TICKET, org);
            EJBLocator.lookupPreferencesBean().setStorageEnabled(true);
            flushCaches();
            logger.info("Organisation AccessKey reset to: " + org.getAccessKey());
        }        
    }    
}
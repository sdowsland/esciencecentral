/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.storage.migration.DataStoreMigration;
import com.connexience.server.model.storage.migration.DocumentVersionMigration;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.Asynchronous;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.List;


/**
 * This bean listens for data migration messages
 * @author hugo
 */
@MessageDriven(activationConfig =
        {
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/migrations"),
                @ActivationConfigProperty(propertyName="transactionTimeout", propertyValue="3600")
        }, mappedName = "queue/migrations")
public class DataStoreMigrationMDB extends HibernateSessionContainer implements MessageListener {
    private static Logger logger = Logger.getLogger(DataStoreMigrationMDB.class);
    
    @Override
    public void onMessage(Message msg) {
        try {
            if(msg instanceof TextMessage){
                TextMessage tm = (TextMessage)msg;
                if(tm.getText().equals("MigrateVersion")){
                    // Do a migration
                    logger.info("Migrating document version");
                    long migrationId = tm.getLongProperty("migrationId");
                    long documentVersionMigrationId = tm.getLongProperty("documentVersionMigrationId");
                    String documentId = tm.getStringProperty("documentId");
                    String versionId = tm.getStringProperty("versionId");
                    String organisationId = tm.getStringProperty("organisationId");
                    migrateDocument(migrationId, documentVersionMigrationId, documentId, versionId, organisationId);
            
                } else if(tm.getText().equals("CreateDocumentMigrations")){
                    // Create the list of documents to update
                    long migrationId = tm.getLongProperty("migrationId");
                    addNewMigrationDocuments(getDefaultOrganisationAdminTicket(), migrationId);
                }
            }
            
        } catch (Exception e){
            logger.error("Error processing data store migration text message: " + e.getMessage());
        }
    }
    
    private void migrateDocument(long migrationId, long documentVersionMigrationId, String documentId, String versionId, String organisationId){
        DocumentRecord record = new DocumentRecord();
        DocumentVersion version = new DocumentVersion();
        
        record.setId(documentId);
        record.setOrganisationId(organisationId);
        
        version.setId(versionId);
        version.setDocumentRecordId(documentId);
        Ticket ticket = getInternalTicket();
        DataStoreMigration migration = null;
        DataStore targetStore = null;
        
        DocumentVersionMigration dvm = null;
        
        try {
            migration = EJBLocator.lookupDataStoreMigrationBean().getMigration(ticket, migrationId);
        } catch (Exception e){
            logger.error("Cannot access migration object");
        }
        
        if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){
            try {
                targetStore = migration.getTargetDataStore();
                if(targetStore.getOrganisationId()==null || targetStore.getOrganisationId().isEmpty()){
                    targetStore.setOrganisationId(ticket.getOrganisationId());
                }
                dvm = EJBLocator.lookupDataStoreMigrationBean().getDocumentVersionMigration(ticket, documentVersionMigrationId);        
                dvm.setCopyStatus(DocumentVersionMigration.COPYING);
                dvm = EJBLocator.lookupDataStoreMigrationBean().saveDocumentVersionMigration(ticket, dvm);
                
            } catch (Exception e){
                logger.error("Cannot create a target datastore to migration document version: " + e.getMessage());
            }

            if(targetStore!=null){
                try {
                    DataStore sourceStore = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, organisationId);
                    
                    // Get the existing document version
                    DocumentVersion oldVersion = EJBLocator.lookupStorageBean().getVersion(ticket, versionId);
                    
                    // Do the copy
                    targetStore.readFromStream(record, version, sourceStore.getInputStream(record, version), oldVersion.getSize());
                    
                    dvm.setCopyStatus(DocumentVersionMigration.COPY_DONE);
                    EJBLocator.lookupDataStoreMigrationBean().saveDocumentVersionMigration(ticket, dvm);
                    
                } catch (Exception e){
                    // Remove any data from the data store
                    if(targetStore!=null){
                        try {
                            targetStore.removeRecord(record, version);
                        } catch (Exception ex){
                            logger.error("Error removing data from target data store when trying to rollback document migration: " + ex.getMessage());
                        }
                    }
                    logger.error("Error migrating document [DR:" + record.getId() + "|DV:" + version.getId() + "]: "  + e.getMessage());
                    dvm.setCopyStatus(DocumentVersionMigration.COPY_FAILED);
                    dvm.setCopyErrorMessage(e.getMessage());
                    try {
                        EJBLocator.lookupDataStoreMigrationBean().saveDocumentVersionMigration(ticket, dvm);
                    } catch (Exception ex){}
                }
            } else {
                dvm.setCopyStatus(DocumentVersionMigration.COPY_FAILED);
                dvm.setCopyErrorMessage("No data store configured for migration");
                try {
                    EJBLocator.lookupDataStoreMigrationBean().saveDocumentVersionMigration(ticket, dvm);
                } catch (Exception ex){}            
            }

            // Send the next JMS message for this migration
            try {
                EJBLocator.lookupDataStoreMigrationBean().sendJMSMessageForNextDocumentInMigration(ticket, migrationId);
            } catch (Exception e){
                logger.error("Error calling sendJMSMessageForNextDocumentInMigration: " + e.getMessage());
            }
        } else {
            // Could not migrate because the migration isn't running
            try {
                dvm = EJBLocator.lookupDataStoreMigrationBean().getDocumentVersionMigration(ticket, documentVersionMigrationId);  
                dvm.setCopyStatus(DocumentVersionMigration.COPY_FAILED);
                dvm.setCopyErrorMessage("Migration not running");
                EJBLocator.lookupDataStoreMigrationBean().saveDocumentVersionMigration(ticket, dvm);
            } catch (Exception e){
                logger.error("Error setting failed message in migration: " + e.getMessage());
            }
        }
    }
    
    private int addNewMigrationDocuments(Ticket ticket, long migrationId) throws ConnexienceException {
        assertAdministrator(ticket);
        Session session = null;
        try {
            session = getSession();
            SQLQuery q =session.createSQLQuery("select * from documentversions where documentversions.id not in(select versionid from migrationversions where migrationversions.migrationid=:migrationId);");
            q.addEntity(DocumentVersion.class);
            q.setLong("migrationId", migrationId);
            List unreferencedVersions = q.list();
            DocumentVersion v;
            DocumentVersionMigration mv;
            ArrayList<DocumentVersionMigration> migrations = new ArrayList<>();
            
            int counter = 0;
            for(Object o : unreferencedVersions){
                v = (DocumentVersion)o;
                mv = new DocumentVersionMigration();
                mv.setDocumentID(v.getDocumentRecordId());
                mv.setCopyStatus(DocumentVersionMigration.WAITING_FOR_COPY);
                mv.setMigrationId(migrationId);
                mv.setOrganisationId(ticket.getOrganisationId());
                mv.setVersionId(v.getId());
                counter++;
                migrations.add(mv);
                        
            }
            saveMigrationList(migrations);
            
            if(counter>0){
                EJBLocator.lookupDataStoreMigrationBean().sendJMSMessageForNextDocumentInMigration(ticket, migrationId);
            }
            return counter;
        } catch (Exception e){
            throw new ConnexienceException("Error adding data to migration list: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
    
    @Asynchronous
    public void saveMigrationList(List<DocumentVersionMigration> migrations){
        Session session = null;
        try {
            session = getSession();
            for(DocumentVersionMigration v : migrations){
                session.save(v);
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            closeSession(session);
        }
    }    
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb;

import com.connexience.server.ejb.HibernateUtil;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.net.URL;
import org.apache.log4j.Logger;
/**
 * This class can replace marked sections of a hibernate configuration file.
 * 
 * @author hugo
 */
public class ConfigReplacer {
    private static Logger logger = Logger.getLogger(ConfigReplacer.class);
    
    public static ConfigReplacer datasetReplacer = new ConfigReplacer("/hibernate/common/DatasetItem.hbm.xml");
    
    /**
     * Original config resource file
     */
    private String configResourceName;

    /**
     * Replacements
     */
    private HashMap<String, String> replacements = new HashMap<String, String>();

    public ConfigReplacer(String configResourceName) {
        this.configResourceName = configResourceName;
    }

    public void addSection(String sectionTerm, String sectionResource) {
        replacements.put(sectionTerm, sectionResource);
    }

    public InputStream getModifiedConfigStream() throws Exception {
        URL u = getClass().getResource(configResourceName);
        if(u!=null){
            logger.info("Config URL: " + u);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(buffer);
            logger.info("Locating: " + configResourceName);
            InputStream resourceStream = u.openStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceStream));

            if (reader != null) {
                String line;
                while ((line = reader.readLine()) != null) {
                    // Check for presence of a replacement section
                    for (String key : replacements.keySet()) {
                        if (line.contains(key)) {
                            // Add replacement here
                            insertResource(writer, replacements.get(key));

                        }
                    }

                    // Add line to output
                    writer.println(line);
                    writer.flush();
                   
                }
            }
            return new ByteArrayInputStream(buffer.toByteArray());
        } else {
            throw new Exception("No such config resource: " + configResourceName);
        }
    }

    private void insertResource(PrintWriter writer, String resourceName) throws Exception {
        BufferedReader reader = null;
        try {
            URL u = getClass().getResource(resourceName);
            if(u!=null){
                InputStream resourceStream = u.openStream();
                reader = new BufferedReader(new InputStreamReader(resourceStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    writer.println(line);
                }
            } else {
                logger.error("Cannot locate additional resource: " + resourceName);
            }
        } catch (Exception e) {
            logger.error("Error inserting resource: " + e.getMessage());
        } finally {
            if (reader != null) {
                reader.close();
            }
            writer.flush();
        }
    }
    
    /**
     * Add a dataset implementation
     *
     */
    public static void addDatasetImplementation(String sectionFlag, String resourceName){
        logger.info("Adding dataset implementation: " + resourceName + " at: " + sectionFlag);
        datasetReplacer.addSection(sectionFlag, resourceName);
    }    
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.crypto.ServerObjectKeyManager;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.OrganisationFolderAccess;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.folder.Folder;
import com.connexience.provenance.model.logging.events.UserCreateOperation;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.*;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.util.RandomGUID;
import com.connexience.server.util.SignatureUtils;
import com.connexience.provenance.client.ProvenanceLoggerClient;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jboss.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.*;

/**
 * This class provides a user directory that allows users and groups to be enumerated
 *
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/UserDirectoryBean", beanInterface = UserDirectoryRemote.class)
public class UserDirectoryBean extends HibernateSessionContainer implements com.connexience.server.ejb.directory.UserDirectoryRemote {
    private static Logger logger = Logger.getLogger(UserDirectoryBean.class);
    /** Creates a new instance of UserDirectoryBean */
    public UserDirectoryBean() {
    }

    /** Save a user */
    public User saveUser(Ticket ticket, User user) throws ConnexienceException {

        if (ticket.isAssociatedWithNonRootOrg()) {
            user.setOrganisationId(ticket.getOrganisationId());
        } else {
            throw new ConnexienceException("Logon is not associated with an organisation");
        }

        // Place the user into the correct folder of the parent organisation
        // if there is one
        if (user.getOrganisationId() != null) {
            Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, user.getOrganisationId());
            OrganisationFolderAccess folderAccess = new OrganisationFolderAccess(org);
            if (folderAccess.usersFolderExists()) {
                user.setContainerId(folderAccess.getUsersFolder().getId());
            }
        }

        // Create a signing key if not already present
        if (user.getHashKey() == null) {
            user.setHashKey(new RandomGUID().valueAfterMD5);
        }

        // Set name if not already there
        user.setName(user.getFirstName() + " " + user.getSurname());
        User saved = (User) saveObjectWithAcl(ticket, user);

        // Set up the keys
        ServerObjectKeyManager mgr = new ServerObjectKeyManager();
        mgr.setupObjectKeyData(user);

        UserCreateOperation op = new UserCreateOperation();
        op.setUsername(user.getDisplayName());
        op.setUserId(user.getId());
        op.setTimestamp(new Date());
        op.setProjectId(ticket.getDefaultProjectId());

        ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
        provClient.log(op);

        return saved;
    }

    /** Change a password for a user */
    public void changePassword(Ticket ticket, String userId, String password) throws ConnexienceException {

        // Do some checks
        if (!getTicketUserId(ticket).equals(userId) && !isSuperTicket(ticket)) {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }

        if (userHasLogon(userId)) {
            LogonDetails logon = getUserLogonDetails(userId);
            logon.setHashedPassword(SignatureUtils.getHashedPassword(password));
            this.savePlainObject(logon);

        } else {
            throw new ConnexienceException("User does not currently have a login account");
        }
    }

    /** Set password for user */
    public void setPassword(Ticket ticket, String userId, String logonName, String password) throws ConnexienceException {
        // Do some checks
        User user = getUser(getInternalTicket(), userId);
        if (ticket.getUserId().equals(userId) || EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), user.getOrganisationId())) {

            if (userHasLogon(userId)) {
                deleteUserLogon(ticket, userId);
            }
            Session session = null;
            try {
                session = getSession();
                LogonDetails logon = new LogonDetails();
                logon.setUserId(userId);
                logon.setLogonName(logonName);

                logon.setHashedPassword(SignatureUtils.getHashedPassword(password));
                session.persist(logon);
                
            } catch (Exception e) {
                throw new ConnexienceException("Error setting password: " + e.getMessage());
            } finally {
                closeSession(session);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    /** Does a user already have a logon */
    public boolean userHasLogon(String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from LogonDetails as l where lower(l.userId)= :userId");
            q.setString("userId", userId.toLowerCase());
            List l = q.list();
            return l.size() > 0;

        } catch (Exception e) {
            throw new ConnexienceException("Error checking user logon: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Delete a logon for a user */
    public void deleteUserLogon(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from LogonDetails l where l.userId=?");
            q.setString(0, userId);
            List logons = q.list();

            for (Object logon : logons) {
                session.delete(logon);
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error deleting logon: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** Find a user from a logon name */
    public User getUserFromLogonName(String logonName) throws ConnexienceException {
        LogonDetails logon = getLogonDetailsByLogonName(logonName);
        if (logon != null) {
            return getUser(getInternalTicket(), logon.getUserId());
        } else {
            throw new ConnexienceException("No such logon");
        }
    }

    /** Validate a logon and return the user object */
    public User authenticateUser(String logonName, String password) throws ConnexienceException {

        LogonDetails logon = getLogonDetailsByLogonName(logonName);
        if (logon != null) {
            if (logon.getHashedPassword().equals(SignatureUtils.getHashedPassword(password))) {
                return getUser(getInternalTicket(), logon.getUserId());
            } else {
                throw new ConnexienceException("Cannot authenticate user");
            }

        } else {
            throw new ConnexienceException("Cannot authenticate user");
        }

    }

    /** Validate a logon and return the user object */
    public User authenticateUserFromId(String userId, String password) throws ConnexienceException {

        LogonDetails logon = getUserLogonDetails(userId);
        if (logon != null) {
            if (logon.getHashedPassword().equals(SignatureUtils.getHashedPassword(password))) {
                return getUser(getInternalTicket(), logon.getUserId());
            } else {
                throw new ConnexienceException("Cannot authenticate user");
            }
        } else {
            throw new ConnexienceException("Cannot authenticate user");
        }
    }



    public String getLogonName(Ticket ticket, String userId) throws ConnexienceException {
        try {
            return getUserLogonDetails(userId).getLogonName();
        } catch (ConnexienceException e) {
            return "Error Getting logon name: " + e.getMessage();
        }
    }

    public void setLogonName(Ticket ticket, String userId, String logonName) throws ConnexienceException {
        //only admin or the same user can change their logon name
        if (ticket.isSuperTicket() || ticket.getUserId().equals(userId)) {
            LogonDetails ld = getUserLogonDetails(userId);
            if (ld != null) //user may only have external logon details
            {
                ld.setLogonName(logonName);
                savePlainObject(ld);
            }
        }
    }

    /** Get logon details for a user */
    private LogonDetails getUserLogonDetails(String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from LogonDetails as l where l.userId=?");
            q.setString(0, userId);
            Object obj = q.uniqueResult();
            if (obj instanceof LogonDetails) {
                return (LogonDetails) obj;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error looking up logon details: ", e);
        } finally {
            closeSession(session);
        }
    }

    /** Get logon details by name */
    private LogonDetails getLogonDetailsByLogonName(String logonName) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from LogonDetails as l where lower(l.logonName)=?");
            q.setString(0, logonName.toLowerCase());
            Object obj = q.uniqueResult();
            if (obj instanceof LogonDetails) {
                return (LogonDetails) obj;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error looking up logon details: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** List all of the groups as GroupMembership objects that a User is a member of */
    public List listGroupMembershipForUser(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from GroupMembership as obj where obj.userId=?");
            q.setString(0, userId);
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing groups: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** List the groups that a user is a member of */
    public List listUserGroups(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            User user = (User) getObject(userId, User.class);
            if (user != null) {
                assertPermission(ticket, user, Permission.READ_PERMISSION);
                session = getSession();
                Query q = session.createQuery("from GroupMembership as obj where obj.userId=?");
                q.setString(0, userId);
                List membership = q.list();
                GroupMembership gm;
                List results;
                Query groupQuery = session.createQuery("from Group as obj where obj.id=?");
                ArrayList<Group> groups = new ArrayList<>();

                for (Object aMembership : membership) {
                    gm = (GroupMembership) aMembership;
                    groupQuery.setString(0, gm.getGroupId());
                    results = groupQuery.list();
                    if (results.size() > 0) {
                        groups.add((Group) results.get(0));
                    }
                }
                return groups;

            } else {
                return new ArrayList();
            }

        } catch (Exception e) {
            throw new ConnexienceException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    /** List the Ids of all of the projects that a User is a member of */
    public List<String> listProjectsForUser(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("SELECT gm.groupId FROM GroupMembership as gm, OldProject as p where gm.userId=? AND p.id = gm.groupId");
            q.setString(0, ticket.getUserId());
            return q.list();
        } catch (Exception e) {
            throw new ConnexienceException("Error listing projects: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }


    /** Is one of the user groups a member of an organisation */
    public boolean isUserOrganisationMember(Ticket ticket, String userId, String organisationId) throws ConnexienceException {
        User u = (User) getObject(userId, User.class);
        if (u != null) {
            assertPermission(ticket, u, Permission.READ_PERMISSION);
            return u.getOrganisationId().equals(organisationId);
        } else {
            throw new ConnexienceException(ConnexienceException.OBJECT_NOT_FOUND_MESSAGE);
        }
    }

    /** Is a user a member of a specified group */
    public boolean isUserGroupMember(Ticket ticket, String userId, String groupId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from GroupMembership as gm where gm.userId=? and gm.groupId=?");
            q.setString(0, userId);
            q.setString(1, groupId);
            List membership = q.list();
            return membership.size() > 0;
        } catch (Exception e) {
            throw new ConnexienceException("Error checking user group membership: " + e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    @Override
    public boolean isUserOrganisationAdmin(Ticket ticket, String userId, String organisationId) throws ConnexienceException {
        Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
        // Only work for admin user
        return ticket.isSuperTicket() || EJBLocator.lookupUserDirectoryBean().isUserGroupMember(ticket, userId, org.getAdminGroupId());
    }

    public List searchForUsers(Ticket ticket, String searchString, OrderBy orderBy, SearchOrder ascDesc, int start, int maxResults) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from User as obj where (lower(obj.name) like ? " +
                    "or lower(obj.firstName) like ? " +
                    "or lower(obj.surname) like ? ) " +
                    "AND obj.protectedUser IS false " +
                    " ORDER BY obj." + orderBy + " " + ascDesc);
            q.setString(0, "%" + searchString.toLowerCase() + "%");
            q.setString(1, "%" + searchString.toLowerCase() + "%");
            q.setString(2, "%" + searchString.toLowerCase() + "%");
            if (maxResults > 0) {
                q.setFetchSize(maxResults);
            }
            if (start > 0) {
                q.setFirstResult(start);
            }
            return q.list();

        } catch (Exception e) {
            throw new ConnexienceException("Error searching for users: " + e.getMessage());
        } finally {
            closeSession(session);
        }}


    /** Search for a user using first name and surname match */
    public List searchForUsers(Ticket ticket, String searchString, int start, int maxResults) throws ConnexienceException {
        return searchForUsers(ticket, searchString, OrderBy.id, SearchOrder.ASC, start, maxResults);
    }

    /**
     * Get a specific user
     * No security as all users can read the users folder anyway
     * This uses HQL directly as it is slightly more efficient and this code is executed very frequently
     */
    public User getUser(Ticket ticket, String userId) throws ConnexienceException {
        if (userId != null) {
            Session session = null;
            try {
                session = getSession();
                Query q = session.createQuery("FROM User AS u where u.id= :id ");
                q.setString("id", userId);
                return (User) q.uniqueResult();
            } catch (Exception e) {
                throw new ConnexienceException("Cannot find user", e);
            } finally {
                closeSession(session);
            }
        } else {
            return null;
        }
    }

    /** Get the name of a user - no security as everyone can get the name of a user */
    public String getUserName(Ticket ticket, String userId) throws ConnexienceException {

        if (ticket.getId() != null && ticket.getId().equals(getInternalTicket().getId())) {
            return "";
        }

        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM User as u where u.id = :userId");
            q.setString("userId", userId);
            List results = q.list();
            if (results.size() > 0) {
                return ((User) results.get(0)).getDisplayName();
            } else {
                return "UnknownUser";
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    public User createAccountForExternalLogon(User user, String externalUserId, String defaultDomain) throws ConnexienceException {
        return createAccount(user, externalUserId, "", defaultDomain, true, "");
    }

    public User createAccountForExternalLogon(User user, String externalUserId, String defaultDomain, String externalProvider) throws ConnexienceException {
        return createAccount(user, externalUserId, "", defaultDomain, true, externalProvider);
    }

    public User createAccount(User user, String logonName, String password) throws ConnexienceException {
        return createAccount(user, logonName, password, "http://www.esciencecentral.co.uk/esc/");
    }

    public User createAccount(User user, String logonName, String password, String defaultDomain) throws ConnexienceException {
        return createAccount(user, logonName, password, defaultDomain, false, "");
    }

    /** Create a new Account for a user with specified logon details and user object */
    private User createAccount(User user, String logonName, String password, String defaultDomain, boolean externalAccount, String externalProvider) throws ConnexienceException {
        Ticket ticket = getInternalTicket();
        Session session = null;
        try {
            // Get the first organisation in the system. TODO: This needs to be changed
            List organisations = EJBLocator.lookupOrganisationDirectoryBean().listOrganisations(getInternalTicket());
            if (organisations.size() > 0) {
                Organisation org = (Organisation) organisations.get(0);
                if (org.getDefaultGroupId() != null) {
                    Group group = EJBLocator.lookupGroupDirectoryBean().getGroup(getInternalTicket(), org.getDefaultGroupId());
                    if (group != null) {
                        // Does the user exist
                        LogonDetails existing = getLogonDetailsByLogonName(logonName);
                        if (existing == null) {
                            // Save the user
                            user.setDefaultGroupId(group.getId());
                            user.setOrganisationId(org.getId());
                            user.setContainerId(org.getUserFolderId());
                            user.setName(logonName);
                            user.setDescription("User " + logonName);
                            user = (User) saveObject(ticket, user);

                            // Set up the keys
                            ServerObjectKeyManager mgr = new ServerObjectKeyManager();
                            mgr.setupObjectKeyData(user);

                            //either add a logonDetails incl password or an externalLogonDetails
                            if (!externalAccount) {
                                // Set the password for the user
                                setPassword(getInternalTicket(), user.getId(), logonName, password);
                            } else {
                                //set an external logon

                                if (!"".equals(externalProvider))
                                    EJBLocator.lookupTicketBean().addExternalLogon(user.getId(), logonName, externalProvider);
                                else
                                    EJBLocator.lookupTicketBean().addExternalLogon(user.getId(), logonName);
                            }

                            // Add the user to the default group
                            EJBLocator.lookupGroupDirectoryBean().addUserToGroup(getInternalTicket(), user.getId(), group.getId());

                            // Create a home folder for the user using the logon name
                            Folder homeFolder = new Folder();
                            homeFolder.setName(logonName + " Home Folder");
                            homeFolder.setCreatorId(user.getId());
                            homeFolder.setContainerId(org.getDataFolderId());
                            homeFolder.setDescription("Home folder for " + logonName);
                            homeFolder.setOrganisationId(org.getId());
                            homeFolder = (Folder) saveObject(ticket, homeFolder);
                            user.setHomeFolderId(homeFolder.getId());

                            //create a workflow folder for the user
                            Folder workflowFolder = new Folder();
                            workflowFolder.setName("Workflows");
                            workflowFolder.setCreatorId(user.getId());
                            workflowFolder.setContainerId(homeFolder.getId());
                            workflowFolder.setDescription("Workflow folder for " + logonName);
                            workflowFolder.setOrganisationId(org.getId());
                            workflowFolder = (Folder) saveObject(ticket, workflowFolder);
                            user.setWorkflowFolderId(workflowFolder.getId());

                            Folder webFolder = new Folder();
                            webFolder.setName("Web Folder");
                            webFolder.setCreatorId(user.getId());
                            webFolder.setContainerId(org.getDataFolderId());
                            webFolder.setDescription("Web folder for " + logonName);
                            webFolder.setOrganisationId(org.getId());
                            webFolder = (Folder) saveObject(ticket, webFolder);
                            user.setWebFolderId(webFolder.getId());

                            //create an inbox folder for the user
                            Folder inbox = new Folder();
                            inbox.setName("Inbox");
                            inbox.setCreatorId(user.getId());
                            inbox.setContainerId(webFolder.getId());
                            inbox.setDescription("Inbox folder for " + logonName);
                            inbox.setOrganisationId(org.getId());
                            inbox = (Folder) saveObject(ticket, inbox);
                            user.setInboxFolderId(inbox.getId());

                            Folder sentFolder = new Folder();
                            sentFolder.setName("Sent Messages");
                            sentFolder.setCreatorId(user.getId());
                            sentFolder.setContainerId(webFolder.getId());
                            sentFolder.setDescription("Sent Messages folder for " + logonName);
                            sentFolder.setOrganisationId(org.getId());
                            sentFolder = (Folder) saveObject(ticket, sentFolder);
                            user.setSentMessagesFolderId(sentFolder.getId());

                            Folder externalObjectsFolder = new Folder();
                            externalObjectsFolder.setName("External Objects");
                            externalObjectsFolder.setCreatorId(user.getId());
                            externalObjectsFolder.setDescription("External objects for " + logonName);
                            externalObjectsFolder.setContainerId(webFolder.getId());
                            externalObjectsFolder.setOrganisationId(org.getId());
                            externalObjectsFolder = (Folder) saveObject(ticket, externalObjectsFolder);
                            user.setExternalObjectsFolderId(externalObjectsFolder.getId());

                            user.setCreatorId(user.getId());
                            user = (User) saveObject(ticket, user);

                            UserProfile profile = EJBLocator.lookupUserDirectoryBean().getUserProfile(getInternalTicket(), user.getId());
                            profile.setEmailAddress(logonName);
                            profile.setDefaultDomain(defaultDomain);
                            saveUserProfile(getInternalTicket(), user.getId(), profile);

                            WebTicket newUsersTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(user.getId());
                            EJBLocator.lookupAccessControlBean().grantUniversalAccess(getInternalTicket(), user.getId(), user.getId(), Permission.READ_PERMISSION);

                            UserCreateOperation op = new UserCreateOperation();
                            op.setUsername(user.getDisplayName());
                            op.setTimestamp(new Date());
                            op.setUserId(user.getId());
                            op.setProjectId(ticket.getDefaultProjectId());

                            ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                            provClient.log(op);
                            return user;
                        } else {
                            throw new Exception("Logon name already exists");
                        }
                    } else {
                        throw new Exception("Specified default group does not exist");
                    }
                } else {
                    throw new Exception("No default group defined");
                }

            } else {
                throw new Exception("No organisations");
            }

        } catch (Exception e) {
            throw new ConnexienceException("Error creating user account: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    /** Get the user  from the profileId */
    public User getUserFromProfileId(Ticket ticket, String profileId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM User as u where u.profileId = :profileId");
            q.setString("profileId", profileId);
            return (User) q.uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    /** Get the user profile for a user */
    public UserProfile getUserProfile(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        User u = (User) getObject(userId, User.class);
        try {
            if (u != null) {
                // If a profile doesn't exist for the user, create one.
                if (u.getProfileId() == null || u.getProfileId().equals("")) {
                    UserProfile up = new UserProfile();

                    up = (UserProfile) savePlainObject(up);
                    u.setProfileId(up.getId());
                    saveObject(ticket, u);

                    return up;
                } else {
                    session = getSession();
                    Query q = session.createQuery("FROM UserProfile as up WHERE up.id = :profileId");
                    q.setString("profileId", u.getProfileId());
                    return (UserProfile) q.uniqueResult();
                }

            } else {
                throw new ConnexienceException("Cannot find user");
            }
        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    public UserProfile saveUserProfile(Ticket ticket, String userId, UserProfile profile) throws ConnexienceException {
        User u = (User) getObject(userId, User.class);
        try {
            if (u != null) {
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, u, Permission.WRITE_PERMISSION) || ticket.isSuperTicket()) {
                    profile = (UserProfile) savePlainObject(profile);

                    u.setProfileId(profile.getId());
                    u = (User) saveObject(ticket, u);

                    return profile;
                } else {
                    throw new ConnexienceException("Access denied saving user profile for user " + userId);
                }
            } else {
                throw new ConnexienceException("Cannot find user");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException(e);
        }
    }

    /**
     * Gets a user based on their email address.  Used in the linking of accounts to external logons
     *
     * @param email email address of the user
     * @return User if one exists with a email address set in their profile, name or logon details
     * @throws ConnexienceException
     */
    public User getUserFromEmailAddress(String email) throws ConnexienceException {
        //todo: users may have same email address?  If changed in profile
        Session session = null;
        try {
            session = getSession();
            User user = (User) session.createQuery("FROM User AS u where u.name = :email").setString("email", email).uniqueResult();
            if (user != null) {
                return user;
            }
            LogonDetails l = (LogonDetails) session.createQuery("FROM LogonDetails AS l WHERE LOWER(l.logonName) = :email").setString("email", email.toLowerCase()).uniqueResult();
            if (l != null) {
                return (User) session.createQuery("FROM User AS u WHERE u.id = :userId").setString("userId", l.getUserId()).uniqueResult();
            }
            return null;
        } catch (Exception e) {
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    /** Method to get a list of users who have shared content with the current user. */
    public List getUsersWithSharedContent(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM User as u WHERE u.firstName != 'Root' AND u.id != :userId "
                    + "AND (u.id IN (SELECT so.creatorId FROM ServerObject as so, Permission as p WHERE (p.targetObjectId = so.id AND (p.principalId = :userId OR p.principalId = :publicUser "
                    + "OR (p.principalId IN (SELECT gm.groupId FROM GroupMembership as gm,Group as g WHERE g.id = gm.groupId AND gm.userId = :userId AND g.protectedGroup = false))))))");
            q.setString("userId", ticket.getUserId());
            q.setString("publicUser", EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId());


            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }
    }

    /*
   * Method to get the content of a particular type that has been shared by a particular user.
   * */
    public List getSharedContent(Ticket ticket, String userId, Class type) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM ServerObject  as so WHERE so.creatorId = :theirId AND so.class = :type "
                    + "AND so.id IN (SELECT p.targetObjectId FROM Permission as p, ServerObject as so2 WHERE (p.targetObjectId = so2.id AND p.principalId = :myId) "
                    + "OR p.principalId IN (SELECT gm.groupId FROM GroupMembership as gm WHERE gm.userId = :myId))");

            q.setText("type", type.getName());
            q.setString("myId", ticket.getUserId());
            q.setString("theirId", userId);

            return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException(e);
        } finally {
            closeSession(session);
        }

    }

    public Collection getFriends(Ticket ticket, User user, Session session) throws ConnexienceException {
        try {
            Query q = session.createQuery("from User as u WHERE (u.id IN (SELECT l.sinkObjectId FROM Link as l WHERE l.sourceObjectId = :userId))");
            q.setString("userId", user.getId());
            return q.list();

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting friends", e);
        }
    }

    public long getNumberOfFriends(Ticket ticket, User user) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("SELECT count(id) from User as u WHERE (u.id IN (SELECT l.sinkObjectId FROM Link as l WHERE l.sourceObjectId = :userId))");
            q.setString("userId", user.getId());
            return (Long) q.list().iterator().next();

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error getting friends", e);
        } finally {
            closeSession(session);
        }
    }

    public String addPasswordRecoveryCode(Ticket adminTicket, String userId) throws ConnexienceException {
        if (adminTicket.isSuperTicket()) {
            //Generate a new unique code
            String passwordRecoveryCode = UUID.randomUUID().toString();

            //add it to the database
            PasswordRecoveryCode code = new PasswordRecoveryCode(userId, passwordRecoveryCode);
            code = (PasswordRecoveryCode) savePlainObject(code);
            return code.getCode();
        } else {
            throw new ConnexienceException("Adding password code must be done by admin ticket");
        }
    }

    public boolean validatePasswordRecoveryCode(String code) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("FROM PasswordRecoveryCode WHERE code = :code");
            q.setString("code", code);

            PasswordRecoveryCode codeFromDB = (PasswordRecoveryCode) q.uniqueResult();
            if (codeFromDB != null) {
                //If the code is still valid - db after now
                if (codeFromDB.getExpiry().after(new Date())) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error validating password recovery code", e);
        } finally {
            closeSession(session);
        }
    }

    public boolean changePasswordFromCode(String code, String newPassword) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            //get the code form the database
            Query q = session.createQuery("FROM PasswordRecoveryCode p WHERE p.code = :code");
            q.setString("code", code);
            PasswordRecoveryCode codeFromDB = (PasswordRecoveryCode) q.uniqueResult();

            //if it's a valid code
            if (codeFromDB != null) {
                //If the code is still valid - db after now
                if (codeFromDB.getExpiry().after(new Date())) {
                    //Change the password with an adminTicket
                    Ticket adminTicket = getInternalTicket();
                    changePassword(adminTicket, codeFromDB.getUserId(), newPassword);

                    //delete the codes for this user from the db
                    Query delQ = session.createQuery("DELETE PasswordRecoveryCode p WHERE p.userId = :userId");
                    delQ.setString("userId", codeFromDB.getUserId());
                    delQ.executeUpdate();

                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnexienceException("Error changing password from recovery code", e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void deleteAccount(Ticket ticket, String userId) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            User u = getUser(ticket, userId);
            if(!ticket.getUserId().equals(userId)){
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
                if(!isUserGroupMember(ticket, userId, org.getAdminGroupId())){
                    Ticket userTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(userId);
                    
                    // Terminate all workflows
                    try {
                        WorkflowEJBLocator.lookupWorkflowManagementBean().terminateAllWorkflowsForUser(userTicket);
                    } catch (Exception e){
                        logger.error("Error terminating workflows in deleteAccount: " + e.getMessage());
                    }
                    
                    // Delete all datasets
                    List datasets = EJBLocator.lookupDatasetsBean().listDatasets(userTicket, false);
                    for(Object o : datasets){
                        try {
                            EJBLocator.lookupDatasetsBean().removeDataset(userTicket, ((Dataset)o).getId());
                        } catch (Exception e){
                            logger.error("Error removing dataset in deleteAccount: " + e.getMessage());
                        }
                    }
                    
                    // Remove the user and any associated data
                    EJBLocator.lookupObjectRemovalBean().remove(u);
                    
                    // Get any remaining objects owned by the user
                    List remains = EJBLocator.lookupObjectDirectoryBean().getOwnedObjects(ticket, userId, ServerObject.class, 0, 0);
                    for(Object o : remains){
                        try {
                            EJBLocator.lookupObjectRemovalBean().remove(null, (ServerObject)o);
                        } catch (Exception e){
                            logger.error("Error removing object in deleteAccount: " + o.getClass().getSimpleName() + ": " + e.getMessage());
                        }
                    }
                    
                } else {
                    throw new ConnexienceException("Cannot remove administrators. Change group membership first");
                }                
            } else {
                throw new ConnexienceException("Cannot remove your own account");
            }
        } else {
            throw new ConnexienceException("Only administrators can delete accounts");
        }
    }

    @Override
    public void updatePasswordHistory(String userId, String hashedPassword, int maxHistoryLength) throws ConnexienceException {
        PasswordHistoryItem item = new PasswordHistoryItem();
        item.setHashedPassword(hashedPassword);
        item.setTimestamp(new Date());
        item.setUserId(userId);
        savePlainObject(item);
        trimPasswordHistory(userId, maxHistoryLength);
    }

    @Override
    public void trimPasswordHistory(String userId, int maxHistoryLength) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            // Maintain the length            
            Query q = session.createQuery("from PasswordHistoryItem as obj where obj.userId=:userid order by obj.timestamp DESC");
            q.setString("userid", userId);
            List results = q.list();
            if(results.size()>maxHistoryLength){
                // Need to trim
                ArrayList objectsToDelete = new ArrayList();
                for(int i=maxHistoryLength;i<results.size();i++){
                    objectsToDelete.add(results.get(i));
                }
                
                for(Object o : objectsToDelete){
                    session.delete(o);
                }
            }
            
        } catch (Exception e){
            throw new ConnexienceException("Error trimming password history: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    
    @Override
    public boolean isHashedPasswordInHistory(String userId, String hashedPassword) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from PasswordHistoryItem as obj where obj.userId=:userid and obj.hashedPassword=:hashedpassword");
            q.setString("userid", userId);
            q.setString("hashedpassword", hashedPassword);
            List results = q.list();
            if(results.size()>0){
                return true;
            } else {
                return false;
            }
                    
        } catch (Exception e){
            throw new ConnexienceException("Error checking password history: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.archive.ArchiveStore;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import java.util.LinkedList;
import java.util.List;

/**
 * This EJB provides document archive functions to the system.
 * @author swheater
 */
@Startup
@Singleton
@DependsOn("HibernateSetup")
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@EJB(name = "java:global/ejb/ArchiveBean", beanInterface = ArchiveRemote.class)
public class ArchiveBean extends HibernateSessionContainer implements com.connexience.server.ejb.archive.ArchiveRemote {
    /** Logger */
    private static final Logger logger = Logger.getLogger(ArchiveBean.class.getName());

    /**
     * Creates a new instance of ArchiveBean
     */
    public ArchiveBean() {
    }

    @PostConstruct
    public void startup() {
        logger.info("Archive Bean: startup");
        Session session = null;
        try {            
            session = getSession();

            // Restart archiving
            try {
                for (DocumentRecord documentRecord: getDocumentRecordByArchiveStatus(DocumentRecord.ARCHIVING_ARCHIVESTATUS, session)) {
                    ServerObject orgObj = getObject(documentRecord.getOrganisationId(), Organisation.class);
                    if (orgObj instanceof Organisation) {
                        Organisation org = (Organisation) orgObj;

                        ServerObject asObj = getObject(org.getArchiveStoreId(), ArchiveStore.class);
                        if (asObj instanceof ArchiveStore) {
                            ArchiveStore as = (ArchiveStore) asObj;

                            as.startArchiving(documentRecord.getId(), org.getDataStoreId());
                        } else
                            throw new ConnexienceException("Document is not suitable for archiving, due to unknown ArchiveStore (" + org.getArchiveStoreId() + ")");
                    } else
                        throw new ConnexienceException("Document is not suitable for archiving, due to unknown Organisation (" + documentRecord.getOrganisationId() + ")");
                }
            } catch (Throwable throwable) {
                logger.error("Error starting archiving", throwable);
            }
        }
        finally {
            closeSession(session);
        }

        try {            
            EJBLocator.lookupJobMonitorBean().startWorkers();
            EJBLocator.lookupUnarchiveJobMonitorBean().startWorkers();
        } catch (Throwable throwable) {
            logger.error("Error stopping archiving", throwable);
        }
    }

    @PreDestroy
    public void shutdown() {
        logger.info("Archive Bean: shutdown");

        try {            
            EJBLocator.lookupJobMonitorBean().shutdownWorkers();
            EJBLocator.lookupUnarchiveJobMonitorBean().shutdownWorkers();
        } catch (Throwable throwable) {
            logger.error("Error restart archiving", throwable);
        }
    }
 
    @Override
    public ServerObject startArchiving(Ticket ticket, String documentId) throws ConnexienceException {

        ServerObject docObj = getObject(documentId, DocumentRecord.class);
        if (docObj instanceof DocumentRecord) {
            DocumentRecord doc = (DocumentRecord) docObj;
            assertPermission(ticket, doc, Permission.WRITE_PERMISSION);

            Session session = null;
            try {
                session = getSession();

                if ((doc.getCurrentArchiveStatus() == DocumentRecord.UNARCHIVED_ARCHIVESTATUS) || (doc.getCurrentArchiveStatus() == DocumentRecord.ARCHIVING_ERROR_ARCHIVESTATUS)) {
                    ServerObject orgObj = getObject(doc.getOrganisationId(), Organisation.class);
                    if (orgObj instanceof Organisation) {
                        Organisation org = (Organisation) orgObj;

                        ServerObject asObj = getObject(org.getArchiveStoreId(), ArchiveStore.class);
                        if (asObj instanceof ArchiveStore) {
                            ArchiveStore as = (ArchiveStore) asObj;

                            as.startArchiving(documentId, org.getDataStoreId());
                        } else
                            throw new ConnexienceException("Document is not suitable for archiving, due to unknown ArchiveStore (" + org.getArchiveStoreId() + ")");
                    } else
                        throw new ConnexienceException("Document is not suitable for archiving, due to unknown Organisation (" + doc.getOrganisationId() + ")");

                    doc.setCurrentArchiveStatus(DocumentRecord.ARCHIVING_ARCHIVESTATUS);
                    saveObject(ticket, doc);
                }
                else
                    throw new ConnexienceException("Document is not suitable for archiving, due to archive status (" + doc.getCurrentArchiveStatus() + ")");

                return doc;
            } catch (ConnexienceException connexienceException) {
                throw connexienceException;
            } catch (Exception exception) {
                throw new ConnexienceException(exception.getMessage(), exception);
            } finally {
                closeSession(session);
            }
        } else {
            ServerObject folderObj = getObject(documentId, Folder.class);
            if (folderObj instanceof Folder) {
                Folder folder = (Folder) folderObj;
                assertPermission(ticket, folder, Permission.WRITE_PERMISSION);

                try {
                    List childFolders = EJBLocator.lookupStorageBean().getChildFolders(ticket, documentId);
                    for (Object childFolder: childFolders)
                        try {
                            startArchiving(ticket, ((ServerObject) childFolder).getId());
                        } catch (Throwable throwable) { }

                    List folderDocumentRecords = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, documentId);
                    for (Object folderDocumentRecord: folderDocumentRecords)
                        try {
                            startArchiving(ticket, ((ServerObject) folderDocumentRecord).getId());
                        } catch (Throwable throwable) { }

                    return folder;
                } catch (ConnexienceException connexienceException) {
                    throw connexienceException;
                } catch (Exception exception) {
                    throw new ConnexienceException(exception.getMessage(), exception);
                }
            }
            else
                throw new ConnexienceException("No such document or folder (" + documentId + ")");
        }
    }

    @Override
    public ServerObject startUnarchiving(Ticket ticket, String documentId) throws ConnexienceException {

        ServerObject docObj = getObject(documentId, DocumentRecord.class);
        if (docObj instanceof DocumentRecord) {
            DocumentRecord doc = (DocumentRecord) docObj;
            assertPermission(ticket, doc, Permission.WRITE_PERMISSION);

            Session session = null;
            try {
                session = getSession();

                if ((doc.getCurrentArchiveStatus() == DocumentRecord.ARCHIVED_ARCHIVESTATUS) || (doc.getCurrentArchiveStatus() == DocumentRecord.UNARCHIVING_ERROR_ARCHIVESTATUS)) {
                    ServerObject orgObj = getObject(doc.getOrganisationId(), Organisation.class);
                    if (orgObj instanceof Organisation) {
                        Organisation org = (Organisation) orgObj;
                
                        ServerObject asObj = getObject(org.getArchiveStoreId(), ArchiveStore.class);
                        if (asObj instanceof ArchiveStore) {
                            ArchiveStore as = (ArchiveStore) asObj;

                            as.startUnarchiving(documentId, org.getDataStoreId());
                        } else
                            throw new ConnexienceException("Document is not suitable for unarchiving, due to unknown ArchiveStore (" + org.getArchiveStoreId() + ")");
                    } else
                        throw new ConnexienceException("Document is not suitable for unarchiving, due to unknown Organisation (" + doc.getOrganisationId() + ")");

                    doc.setCurrentArchiveStatus(DocumentRecord.UNARCHIVING_ARCHIVESTATUS);
                    saveObject(ticket, doc);
                } else
                   throw new ConnexienceException("Document is not suitable for unarchiving, due to archive status (" + doc.getCurrentArchiveStatus() + ")");

                return doc;
            } catch (ConnexienceException connexienceException) {
                throw connexienceException;
            } catch (Exception exception) {
                throw new ConnexienceException(exception.getMessage(), exception);
            } finally {
                closeSession(session);
            }

        } else {
            ServerObject folderObj = getObject(documentId, Folder.class);
            if (folderObj instanceof Folder) {
                Folder folder = (Folder) folderObj;
                assertPermission(ticket, folder, Permission.WRITE_PERMISSION);
    
                try {
                    List childFolders = EJBLocator.lookupStorageBean().getChildFolders(ticket, documentId);                    
                    for (Object childFolder: childFolders)
                        try {
                            startUnarchiving(ticket, ((ServerObject) childFolder).getId());
                        } catch (Throwable throwable) { }
    
                    List folderDocumentRecords = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, documentId);
                    for (Object folderDocumentRecord: folderDocumentRecords)
                        try {
                            startUnarchiving(ticket, ((ServerObject) folderDocumentRecord).getId());
                        } catch (Throwable throwable) { }
    
                    return folder;
                } catch (ConnexienceException connexienceException) {
                    throw connexienceException;
                } catch (Exception exception) {
                    throw new ConnexienceException(exception.getMessage(), exception);
                }
            } else
                throw new ConnexienceException("No such document (" + documentId + ")");
        }
    }

    /** Get all DocumentRecords of specified current archive status */
    private List<DocumentRecord> getDocumentRecordByArchiveStatus(int currentArchiveStatus, Session session) throws ConnexienceException {
        List<DocumentRecord> documentRecord = new LinkedList<>();

        Query q = session.createQuery("from DocumentRecord as obj where obj.currentArchiveStatus=?");
        q.setInteger(0, currentArchiveStatus);

        return q.list();
    }
}
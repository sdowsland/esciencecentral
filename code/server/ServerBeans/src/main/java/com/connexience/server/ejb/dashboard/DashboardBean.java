/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.dashboard;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.dashboard.Dashboard;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * This class provides the dashboard management bean
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/DashboardBean", beanInterface = DashboardRemote.class)
public class DashboardBean extends HibernateSessionContainer implements DashboardRemote {

    @Override
    public List listUserDashboards(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dashboard as db where db.creatorId=:creatorid");
            q.setString("creatorid", ticket.getUserId());
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing user dashboards: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List listDashboardsForDataset(Ticket ticket, String datasetId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dashboard as db where db.creatorId=:creatorid and db.datasetId=:datasetid");
            q.setString("creatorid", ticket.getUserId());
            q.setString("datasetid", datasetId);
            return q.list();
        } catch (Exception e){
            throw new ConnexienceException("Error listing dashboards: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    
    @Override
    public List listSharedDashboards(Ticket ticket) throws ConnexienceException {
        return EJBLocator.lookupObjectDirectoryBean().getSharedObjectsUserHasAccessTo(ticket, ticket.getUserId(), Dashboard.class, 0, 0);
    }

    @Override
    public Dashboard saveDashboard(Ticket ticket, Dashboard dashboard) throws ConnexienceException {
        return (Dashboard)saveObjectWithAcl(ticket, dashboard);
    }

    @Override
    public Dashboard getDashboard(Ticket ticket, String dashboardId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from Dashboard as db where db.id=:id");
            q.setString("id", dashboardId);
            Object o = q.uniqueResult();
            if(o instanceof Dashboard){
                return (Dashboard)o;
            } else {
                throw new ConnexienceException("Object is not a dashboard");
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting dashboard: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    } 

    @Override
    public void removeDashboard(Ticket ticket, String dashboardId) throws ConnexienceException {
        Dashboard db = getDashboard(ticket, dashboardId);
        assertPermission(ticket, db, Permission.WRITE_PERMISSION);
        EJBLocator.lookupObjectRemovalBean().remove(db);
    }
}
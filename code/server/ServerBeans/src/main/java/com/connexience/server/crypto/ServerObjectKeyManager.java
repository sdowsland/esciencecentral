/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.crypto;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.KeyData;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * This class provides key management functionality for loading 
 * saving keys and retrieving X509 signing certificates.
 * @author hugo
 */
public class ServerObjectKeyManager extends HibernateSessionContainer {
    
    /** Creates a new instance of ServerObjectKeyManager */
    public ServerObjectKeyManager() {
    }
    
    /** Get the KeyData for a ServerObject */
    public KeyData getObjectKeyData(String objectId) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q = session.createQuery("from KeyData as kd where kd.objectId=?");
            q.setString(0, objectId);
            
            List keys = q.list();
            if(keys.size()>0){
                return (KeyData)keys.get(0);
            } else {
                return null;
            }
            
        } catch (Exception e){
            throw new ConnexienceException("Error getting ServerObject key data: " + e.getMessage());
        } finally {
            closeSession(session);
        }        
    }
    
    /** Get the KeyData for a ServerObject */
    public KeyData getObjectKeyData(ServerObject object) throws ConnexienceException {
        return getObjectKeyData(object.getId());
    }
    
    /** Create KeyData for a ServerObject */
    public KeyData createKeyData(ServerObject object) throws ConnexienceException {
        
        KeyData data = getObjectKeyData(object);
        if(data==null){
            data = new KeyData();
            data.setObjectId(object.getId());
            String objectDn = "cn=" + object.getId() + ",dc=Objects,ou=Connexience";
            String issuerDn = "dn=Connexience Issuer,ou=Core Server,dc=Connexience";
            
            // TODO: Set from the orgaisation object
            int validity = 10;
            try {
                data.initialiseKeys(objectDn, issuerDn, validity);
            } catch (Exception e){
                throw new ConnexienceException("Error initialising key data: " + e.getMessage());
            }
            return data;
            
        } else {
            return data;
        }
    }
    
    /** Save key data for an object */
    public KeyData saveKeyData(KeyData data) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            if(data.getId()!=null){
                session.merge(data);
                return data;
            } else {
                session.persist(data);
                return data;
            }
        } catch (Exception e){
            throw new ConnexienceException("Error saving key data: " + e.getMessage());
        } finally {
            closeSession(session);
        }        
    }
    
    /** Create and save some key data for an object */
    public void setupObjectKeyData(ServerObject object) throws ConnexienceException {
        KeyData data = getObjectKeyData(object);
        if(data==null){
            data = createKeyData(object);
            saveKeyData(data);
        }
    }
}

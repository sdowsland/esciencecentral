/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler;

import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.scheduler.tasks.*;
import com.connexience.server.ejb.storage.tasks.BlockCleanup;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;


/**
 * This singleton EJB runs the scheduler that takes care of performing
 * various housekeeping tasks.
 *
 * @author hugo
 */
@Startup
@Singleton(name = "SchedulerBean")
@EJB(name = "java:global/ejb/SchedulerBean", beanInterface = SchedulerRemote.class)
public class SchedulerBean extends HibernateSessionContainer implements SchedulerRemote {
    private static Logger logger = Logger.getLogger(SchedulerBean.class);

    /**
     * List of tasks that are scheduled
     */
    private ArrayList<SchedulerTask> tasks = new ArrayList<>();

    /**
     * Instance
     */
    public static SchedulerBean SCHEDULER_INSTANCE = null;

    /**
     * Create default properties for the scheduler
     */
    public static void createDefaultProperties() {
        PreferenceManager.getSystemPropertyGroup("Scheduler").add("StartScheduler", true);
        XmlDataStore taskStore = new XmlDataStore();
        taskStore.add("RunMigrations", true);
        PreferenceManager.getSystemPropertyGroup("Scheduler").add("Tasks", taskStore);
    }

    @PostConstruct
    void init() {
        logger.info("Instantiated scheduler");
        SchedulerBean.SCHEDULER_INSTANCE = this;
    }

    public void startScheduler() {
        logger.info("Started scheduler");
        CheckMigrations migrations = new CheckMigrations(this);
        DeployWorkflowServices serviceDeployer = new DeployWorkflowServices(this);
        DeploySampleData samplesDeployer = new DeploySampleData(this);
        addTask(migrations);
        addTask(serviceDeployer);
        addTask(samplesDeployer);
        addTask(new BlockCleanup(this));
        addTask(new ScanRemoteFilesystems(this));

//        try {
//            Constructor c = Class.forName("com.inkspot.server.AccountMaintenance").getConstructor(SchedulerBean.class);
//            addTask((SchedulerTask) c.newInstance(this));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        rescheduleSingleTasks();
    }

    @PreDestroy
    public void shutdownTimer() {
        logger.info("Stopping scheduler");
        logger.info("Cancelling scheduler tasks");
        for (TimerTask t : tasks) {
            t.cancel();
        }
    }

    /**
     * Re-run the single tasks
     */
    public void rescheduleSingleTasks() {
        addTask(new StartMigrations(this));
        addTask(new StartInternalWorkflowEngine(this));
        addTask(new CheckConfigFolder(this));
        addTask(new PopulateMD5s(this));
        addTask(new AddCurrentVersionTimestamp(this));
        addTask(new EnableLinks(this));
    }

    /**
     * Add a task to this scheduler
     */
    public void addTask(SchedulerTask task) {
        tasks.add(task);
        task.scheduleTask();
    }

    /**
     * Add a runnable class to the scheduler
     */
    @Override
    public void addRunnable(String taskName){
        try {
            Constructor c = Class.forName(taskName).getConstructor(SchedulerBean.class);
            addTask((SchedulerTask) c.newInstance(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeAllNamedTasks(String taskName) {
        List<SchedulerTask> tasksToRemove = new ArrayList<>();
        for(SchedulerTask task : tasks){
            if(task.getName().equals(taskName)){
                tasksToRemove.add(task);
            }
        }
        
        for(SchedulerTask t : tasksToRemove){
            removeTask(t);
        }
    }

    
    /**
     * Task has finished
     */
    public void taskFinished(SchedulerTask task) {
        tasks.remove(task);
    }

    /**
     * Remove a task from this scheduler
     */
    public void removeTask(SchedulerTask task) {
        task.cancel();
        tasks.remove(task);
    }

    public int getTaskCount() {
        return tasks.size();
    }

    public SchedulerTask getTask(int index) {
        return tasks.get(index);
    }
}
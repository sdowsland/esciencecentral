/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.project.Uploader;
import com.connexience.server.util.SecureHashUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import java.util.Collection;
import java.util.List;

@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/UploaderBean", beanInterface = UploaderRemote.class)
public class UploaderBean extends HibernateSessionContainer implements UploaderRemote
{
	@EJB
	private ProjectsRemote projectsBean;

	@EJB
	private StudyRemote studyBean;

	@EJB
	private UserDirectoryRemote userBean;

	@Inject
	private EntityManager em;

	@Override
	public Uploader getUploader(final Ticket ticket, final Integer uploaderId) throws ConnexienceException
	{
		final Uploader uploader = em.find(Uploader.class, uploaderId);

		if (uploader == null)
		{
			throw new ConnexienceException("Uploader id='" + uploaderId + "' not found in DB.");
		}

		return uploader;
	}

	@Override
	public Uploader saveUploader(final Ticket ticket, final Uploader uploader) throws ConnexienceException
	{
		if (uploader.getId() == null)
		{
			em.persist(uploader);

            return uploader;
		}
		else
		{
			return em.merge(uploader);
		}
	}

	@Override
	public void deleteUploader(final Ticket ticket, final Integer uploaderId) throws ConnexienceException
	{
		final Uploader uploader = getUploader(ticket, uploaderId);

		// Study "owns" the relationship, remove from each study
		for (final Project project : uploader.getProjects())
		{
			project.getUploaders().remove(uploader);
			em.merge(project);
		}

		em.remove(uploader);
	}

	@Override
	public Integer getUploaderCount(final Ticket ticket)
	{
		return (Integer) em.createNamedQuery("Uploader.public.count").getResultList().get(0);
	}

	@Override
	public Collection<Uploader> getUploaders(final Ticket ticket, final Integer start, final Integer maxResults)
	{
		return em.createNamedQuery("Uploader.public")
			.setFirstResult(start)
			.setMaxResults(maxResults)
			.getResultList();
	}

	@Override
	public Uploader createUploader(final Ticket ticket, final String username, final String password) throws ConnexienceException
	{
		if (fetchByUsername(username) == null)
		{
			return saveUploader(ticket, new Uploader(username, SecureHashUtils.generateSaltedHash(password)));
		}
		else
		{
			throw new ConnexienceException("Username '" + username + "' already exists.");
		}
	}

	@Override
	public Uploader createAddUploader(final Ticket ticket, final Integer projectId, final String username, final String password) throws ConnexienceException
	{
		final Uploader uploader = createUploader(ticket, username, password);

		addUploader(ticket, projectId, uploader.getId());

		return uploader;
	}

	@Override
	public Uploader addUploader(final Ticket ticket, final Integer projectId, final Integer uploaderId) throws ConnexienceException
	{
		final Study study = em.merge(studyBean.getStudy(ticket, projectId));
		final Uploader uploader = getUploader(ticket, uploaderId);

		if (projectsBean.isProjectAdmin(ticket, study))
		{
			study.addUploader(uploader);

			em.merge(study);
			return em.merge(uploader);
		}
		else
		{
			throw new ConnexienceException("You must be a Study administrator to modify its Uploaders");
		}
	}

	@Override
	public Uploader removeUploader(final Ticket ticket, final Integer projectId, final Integer uploaderId) throws ConnexienceException
	{
		final Study study = em.merge(studyBean.getStudy(ticket, projectId));
		final Uploader uploader = getUploader(ticket, uploaderId);

		if (projectsBean.isProjectAdmin(ticket, study))
		{
			study.removeUploader(uploader);

			em.merge(study);
			return em.merge(uploader);
		}
		else
		{
			throw new ConnexienceException("You must be a Study administrator to modify its Uploaders.");
		}
	}

	@Override
	public Uploader authenticateUploader(final String username, final String password) throws ConnexienceException
	{
		final Uploader user = fetchByUsername(username);

		if (user != null && SecureHashUtils.check(password, user.getHashedPassword()))
		{
			return user;
		}
		else
		{
			throw new ConnexienceException("Could not authenticate with the provided credentials.");
		}
	}

	@Override
	public Uploader setPassword(final Ticket ticket, final Integer uploaderId, final String password) throws ConnexienceException
	{
		final Uploader uploader = getUploader(ticket, uploaderId);

		uploader.setHashedPassword(SecureHashUtils.generateSaltedHash(password));

		return saveUploader(ticket, uploader);
	}

	private Uploader fetchByUsername(final String username)
	{
		final List<Uploader> resultList = em.createQuery("SELECT u FROM Uploader u WHERE u.username = :username")
			.setParameter("username", username)
			.setMaxResults(1)
			.getResultList();

		if (resultList.size() == 1)
		{
			return resultList.get(0);
		}
		else
		{
			return null;
		}
	}
}

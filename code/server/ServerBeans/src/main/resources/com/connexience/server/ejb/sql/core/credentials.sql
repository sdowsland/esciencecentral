update remotefilesystemscanners
	set enabled = FALSE,
	credentialsid = ''
	where
	objecttype <> 'DISKSCANNER'
	AND credentialsid IS NOT NULL
	AND credentialsid <> ''
	AND credentialsid NOT IN (
		SELECT id from credentials);

-- Drop the phase column as we've added a new table for the
-- phases of a study
ALTER TABLE projects DROP COLUMN phase;
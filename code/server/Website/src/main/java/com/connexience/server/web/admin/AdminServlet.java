/*
 * AdminServlet.java
 */
package com.connexience.server.web.admin;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.LogonEvent;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.TemplateFolder;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.quota.UserQuota;
import com.connexience.server.model.scanner.RemoteFilesystemObject;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.scanner.filesystems.AzureScanner;
import com.connexience.server.model.scanner.filesystems.DiskScanner;
import com.connexience.server.model.scanner.filesystems.S3Scanner;
import com.connexience.server.model.security.*;
import com.connexience.server.model.security.credentials.AmazonCredentials;
import com.connexience.server.model.security.credentials.AzureCredentials;
import com.connexience.server.model.social.OldProject;
import com.connexience.server.model.storage.AzureBlobStore;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.model.storage.FileDataStore;
import com.connexience.server.model.storage.S3DataStore;
import com.connexience.server.model.storage.migration.DataStoreMigration;
import com.connexience.server.model.storage.migration.DocumentVersionMigration;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.control.IWorkflowEngineControl;
import com.connexience.server.model.workflow.control.WorkflowEngineRecord;
import com.connexience.server.model.workflow.control.WorkflowEngineStatusData;
import com.connexience.server.model.workflow.control.WorkflowInvocationRecord;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.model.workflow.notification.WorkflowLockMember;
import com.connexience.server.util.DiskSpaceFormatter;
import com.connexience.server.util.JSONDate;
import com.connexience.server.util.JSONXmlDataStoreConverter;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.WorkflowUtils;
import com.connexience.server.web.APIUtils;
import com.connexience.server.web.util.WebUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJBLocalHome;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.jboss.logging.Logger;

/**
 * This servlet deals with Admin requests for managing services and hosts
 *
 * @author hugo
 */
public class AdminServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(AdminServlet.class);

    private DateFormat format = DateFormat.getDateTimeInstance();

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;
        HttpSession session = request.getSession();

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    }

                    if (method.equalsIgnoreCase("searchForUsers")) {
                        // Search for users with name field
                        searchForUsers(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("getUserByLogon")) {
                        // Lookup a user by e-mail
                        getUserByLogon(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("getUserAndQuota")){
                        // Get a user and associated quota
                        getUserAndQuota(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("setUserQuota")){
                        // Assign a storage quota
                        setUserQuota(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("setUserPassword")) {
                        // Change the password / logon for a user
                        setUserPassword(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("switchUser")) {
                        // Change the logged on user
                        switchUser(ticket, dataObject, resultObject, request, response);

                    } else if(method.equalsIgnoreCase("getGroupMembershipForUser")){
                        // Get the membership data for a user
                        getGroupMembershipForUser(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("createUser")){
                        // Create a new account
                        createUser(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("saveGroupMembershipForUser")){
                        // Update the group membership for a user
                        saveGroupMembershipForUser(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("listWorkflowEngines")) {
                        // List the workflow engines
                        listWorkflowEngines(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("fetchEngineStatus")) {
                        // Get the status data for a workflow engine
                        fetchEngineStatus(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("terminateInvocation")) {
                        // Kill an invocation
                        terminateInvocation(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("attachWorkflowEngineJMS")) {
                        // Attach JMS Listener for a workflow engine
                        attachWorkflowEngineJMS(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("detachWorkflowEngineJMS")) {
                        // Detach JMS listener
                        detachWorkflowEngineJMS(ticket, dataObject, resultObject);


                    } else if (method.equalsIgnoreCase("listTemplateFolders")) {
                        // Create new promotional codes
                        listTemplateFolders(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("saveTemplateFolder")) {
                        // List all of the products
                        saveTemplateFolder(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("deleteTemplateFolder")) {
                        // List all of the products
                        deleteTemplateFolder(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("reRegisterEngines")) {
                        // Rebuild the workflow engine list
                        reRegisterWorkflowEngines(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("resetEngineRmiCommunications")){
                        // Reset enngines to use their default RMI comms
                        resetEngineRmiCommunications(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("notifyAllFinishedLockHolders")){
                        // Send completion messages for all finished locks
                        notifyAllFinishedLockHolders(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listWorkflowLocks")){
                        // List the workflow locks
                        listWorkflowLocks(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getWorkflowLockMembers")){
                        // Get the individual members of a workflow lock
                        getWorkflowLockMembers(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("deleteWorkflowLock")){
                        // Remove a workflow lock
                        deleteWorkflowLock(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("attemptLockRedelivery")){
                        // Try and redeliver a lock completed message
                        attemptLockRedelivery(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("killAllWorkflows")){
                        // Kill every running workflow
                        killAllWorkflows(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("killAllUserWorkflows")){
                        // Kill every workflow owned by the logged in user
                        killAllUserWorkflows(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getSystemDatastoreProperties")){
                        // Get the data store properties
                        getSystemDatastoreProperties(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("setSystemDatastoreProperties")){
                        // Set the data store properties
                        setSystemDatastoreProperties(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("createMigration")){
                        // Create a new migration
                        createMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("startMigration")){
                        // Start a new migration
                        startMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("stopMigration")){
                        // Stop a migration
                        stopMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getMigration")){
                        // Get a migration object
                        getMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("setMigrationDataStoreProperties")){
                        // Set the properties of the migration data store
                        setMigrationDataStoreProperties(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("resetMigrationDataStore")){
                        // Setup a migration with a new data store
                        resetMigrationDataStore(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("deleteMigration")){
                        // Delete a migration
                        deleteMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("retryFailedDocumentsForMigration")){
                        // Try sending failed migrations again
                        retryFailedDocumentsForMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getMigrationState")){
                        // Get the status data for a migration
                        getMigrationState(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listMigrations")){
                        // List all of the migrations
                        listMigrations(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("finishMigration")){
                        // Finish a migration and switch data store
                        finishMigration(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("rebuildCoreLibrary")){
                        // Rebuild the core library
                        rebuildCoreLibrary(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("flushEngineInformationCaches")){
                        // Clear the information caches from the workflow engines
                        flushEngineInformationCaches(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listAllScanners")){
                        // List all of the remote filesystem scanners
                        listAllScanners(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listUserScanners")){
                        listUserScanners(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("executeScanner")){
                        executeScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("createScanner")){
                        createScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("deleteScanner")){
                        deleteScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getScanner")){
                        getScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("setScannerFolder")){
                        setScannerFolder(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("setScannerProperties")){
                        setScannerProperties(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("enableScanner")){
                        enableScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("disableScanner")){
                        disableScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listScannerFiles")){
                        listScannerFiles(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("resetScanner")){
                        resetScanner(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("deleteUser")){
                        deleteUser(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listCredentialTypes")){
                        listCredentialTypes(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("saveCredentials")){
                        saveCredentials(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("deleteCredentials")){
                        deleteCredentials(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listCredentials")){
                        listCredentials(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("createCredentials")){
                        createCredentials(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getCredentials")){
                        getCredentials(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("resetAccessKey")){
                        resetAccessKey(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("listPropertyGroups")){
                        // List all of the system property groups
                        listPropertyGroups(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("getPropertyGroup")){
                        // Get a specific property group as a JSON object
                        getPropertyGroup(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("savePropertyGroup")){
                        // Save a property group back to disk
                        savePropertyGroup(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("lockAccount")){
                        lockAccount(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("unlockAccount")){
                        unlockAccount(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("exitAllEngines")){
                        exitAllEngines(ticket, dataObject, resultObject);

                    }


                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Administration Servlet";
    }

    private void detachWorkflowEngineJMS(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                long id = dataObject.getLong("databaseId");
                WorkflowEngineRecord engine = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowEngine(ticket, id);
                IWorkflowEngineControl ctl = WorkflowUtils.connectToWorkflowEngine(ticket, engine);
                ctl.disconnectJms();
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void attachWorkflowEngineJMS(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                long id = dataObject.getLong("databaseId");
                WorkflowEngineRecord engine = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowEngine(ticket, id);
                IWorkflowEngineControl ctl = WorkflowUtils.connectToWorkflowEngine(ticket, engine);
                ctl.connectJms();
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void terminateInvocation(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("invocationId");
            WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, invocationId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void fetchEngineStatus(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                long id = dataObject.getLong("databaseId");
                WorkflowEngineRecord engine = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowEngine(ticket, id);
                IWorkflowEngineControl ctl = WorkflowUtils.connectToWorkflowEngine(ticket, engine);

                WorkflowEngineStatusData status = ctl.getStatusData();

                JSONObject statusJson = new JSONObject();
                statusJson.put("diskSize", new DiskSpaceFormatter(status.getDiskSize()).format());
                statusJson.put("diskFree", new DiskSpaceFormatter(status.getFreeSpace()).format());
                statusJson.put("diskUsed", new DiskSpaceFormatter(status.getDiskSize() - status.getFreeSpace()).format());
                statusJson.put("diskPercent", (int) (((double) (status.getDiskSize() - status.getFreeSpace()) / (double) status.getDiskSize()) * 100.0));
                statusJson.put("workflowCount", status.getWorkflowCount());
                statusJson.put("workflowsSucceeded", status.getTotalWorkflowsSucceeded());
                statusJson.put("workflowsFailed", status.getTotalWorkflowsFailed());
                statusJson.put("workflowsStarted", status.getTotalWorkflowsStarted());
                statusJson.put("workflowCapacity", status.getWorkflowCapacity());
                statusJson.put("workflowPercent", (int) (((double) status.getWorkflowCount() / (double) status.getWorkflowCapacity()) * 100.0));
                statusJson.put("engineStartTime", format.format(status.getEngineStartTime()));
                statusJson.put("databaseId", id);

                JSONArray invocationsArray = new JSONArray();
                for (int i = 0; i < status.getInvocations().size(); i++) {
                    invocationsArray.put(createInvocationJson(status.getInvocations().get(i)));
                }
                statusJson.put("invocations", invocationsArray);
                resultObject.put("engineStatus", statusJson);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private JSONObject createInvocationJson(WorkflowInvocationRecord invocation) throws Exception {
        JSONObject invocationJson = new JSONObject();
        invocationJson.put("invocationId", invocation.getInvocationId());
        invocationJson.put("startTime", format.format(invocation.getStartTime()));
        invocationJson.put("name", invocation.getWorkflowName());
        invocationJson.put("running", invocation.isRunning());
        return invocationJson;
    }

    private void listWorkflowEngines(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {

            List engines = WorkflowEJBLocator.lookupWorkflowManagementBean().listDynamicWorkflowEngines(ticket);

            JSONArray engineList = new JSONArray();
            JSONObject engineJson = new JSONObject();
            WorkflowEngineRecord engine;

            for (int i = 0; i < engines.size(); i++) {
                engine = (WorkflowEngineRecord) engines.get(i);

                engineJson = new JSONObject();
                engineJson.put("databaseId", engine.getId());
                if (engine.isStandalone()) {
                    engineJson.put("hostIPAddress", engine.getIPAddress());
                }
                engineList.put(engineJson);
            }

            resultObject.put("engines", engineList);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }



    /**
     * Switch the user context
     */
    private void switchUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpServletRequest request, HttpServletResponse response) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                HttpSession session = request.getSession(false);
                if (session != null) {
                    String userId = dataObject.getString("userId");
                    SessionUtils.login(request, userId);
                    APIUtils.setSuccess(resultObject);
                    LogonEvent logonEvent = new LogonEvent(LogonEvent.IMPERSONATE_LOGON, ((User)request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
                    ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                    client.log(logonEvent);
                } else {
                    APIUtils.setError(resultObject, "No session is available");
                }

            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Kill every running workflow
     */
    private void killAllWorkflows(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().terminateAllWorkflows(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Kill every workflow for the logged on user */
    private void killAllUserWorkflows(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().terminateAllWorkflowsForUser(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Set the password for a user
     */
    private void setUserPassword(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                String userId = dataObject.getString("userId");
                String newPassword = dataObject.getString("password");
                String logon = dataObject.getString("logon");
                User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
                if (u != null) {
                    EJBLocator.lookupUserDirectoryBean().setPassword(ticket, userId, logon, newPassword);
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "No such user");
                }
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Set the quota for a user */
    private void setUserQuota(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                String userId = dataObject.getString("userId");
                boolean enforce = dataObject.getBoolean("enabled");
                String size = dataObject.getString("size");
                long quotaSize = Integer.parseInt(size) * 1048576;

                User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
                if(u!=null){
                    UserQuota q = EJBLocator.lookupQuotaBean().getOrCreateUserQuota(ticket, userId);
                    q.setStorageQuotaEnforced(enforce);
                    q.setStorageQuota(quotaSize);
                    EJBLocator.lookupQuotaBean().saveUserQuota(ticket, q);
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "No such user");
                }
            } else {
                APIUtils.setError(resultObject, "User is not an administrator");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get a user and quota */
    private void getUserAndQuota(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                String userId = dataObject.getString("userId");
                User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
                if(u!=null){
                    UserQuota q = EJBLocator.lookupQuotaBean().getOrCreateUserQuota(ticket, userId);
                    JSONObject userJson = createUserJson(u);
                    userJson.put("quotaEnforced", q.isStorageQuotaEnforced());
                    userJson.put("storageQuota", q.getStorageQuota() / 1048576);
                    resultObject.put("user", userJson);
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "No such user");
                }

            } else {
                APIUtils.setError(resultObject, "User is not an administrator");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Update the group membership for a user
     */
    private void saveGroupMembershipForUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String userId = dataObject.getString("userId");
            JSONArray newMembership = dataObject.getJSONArray("newMembership");
            JSONArray oldMembership = dataObject.getJSONArray("oldMembership");

            ArrayList<String> newMembershipList = new ArrayList<>();
            ArrayList<String> oldMembershipList = new ArrayList<>();

            for(int i=0;i<newMembership.length();i++){
                newMembershipList.add(newMembership.getString(i));
            }

            for(int i=0;i<oldMembership.length();i++){
                oldMembershipList.add(oldMembership.getString(i));
            }

            // Join groups
            for(String id : newMembershipList){
                if(!oldMembershipList.contains(id)){
                    EJBLocator.lookupGroupDirectoryBean().addUserToGroup(ticket, userId, id);
                }
            }

            // Leave groups
            for(String id : oldMembershipList){
                if(!newMembershipList.contains(id)){
                    EJBLocator.lookupGroupDirectoryBean().removeUserFromGroup(ticket, userId, id);
                }
            }

            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    /**
     * Get the group membership for a user
     */
    private void getGroupMembershipForUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            // Get all of the groups
            String userId = dataObject.getString("userId");
            List groups = EJBLocator.lookupGroupDirectoryBean().listGroups(ticket);
            JSONObject groupJson;
            JSONArray groupsList = new JSONArray();
            Group group;

            for(int i=0;i<groups.size();i++){
                group = (Group)groups.get(i);
                groupJson = new JSONObject();
                groupJson.put("id", group.getId());
                groupJson.put("name", group.getName());
                if(group instanceof OldProject){
                    groupJson.put("isProject", true);
                } else {
                    groupJson.put("isProject", false);
                }
                groupsList.put(groupJson);
            }
            resultObject.put("groups", groupsList);

            // Get the user groups
            List userGroups = EJBLocator.lookupUserDirectoryBean().listGroupMembershipForUser(ticket, userId);
            JSONArray userGroupsList = new JSONArray();
            for(int i=0;i<userGroups.size();i++){
                userGroupsList.put(((GroupMembership)userGroups.get(i)).getGroupId());
            }
            resultObject.put("membership", userGroupsList);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    /**
     * Get a user by email / logon
     */
    private void getUserByLogon(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                String logon = dataObject.getString("logon");
                User u = EJBLocator.lookupUserDirectoryBean().getUserFromLogonName(logon);
                if (u != null) {
                    JSONObject userJson = new JSONObject();
                    userJson.put("id", u.getId());
                    userJson.put("firstName", u.getFirstName());
                    userJson.put("surname", u.getSurname());
                    userJson.put("displayName", u.getDisplayName());
                    userJson.put("name", u.getName());
                    userJson.put("logon", logon);
                    resultObject.put("user", userJson);
                } else {
                    APIUtils.setError(resultObject, "No such logon: " + logon);
                }
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Search for users based on name
     */
    private void searchForUsers(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String searchText = dataObject.getString("searchText");
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                List results = EJBLocator.lookupUserDirectoryBean().searchForUsers(ticket, searchText, 0, 0);
                JSONArray users = new JSONArray();
                User u;
                JSONObject userJson;
                LogonDetails logon;

                for (int i = 0; i < results.size(); i++) {
                    u = (User) results.get(i);
                    try {
                        users.put(createUserJson(u));
                    } catch (Exception e) {
                        System.out.println("Unable to find logon details for user: " + u.getId() + " " + u.getDisplayName());
                    }
                }
                resultObject.put("users", users);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private JSONObject createUserJson(User u) throws Exception {
        JSONObject userJson = new JSONObject();
        userJson.put("id", u.getId());
        userJson.put("firstName", u.getFirstName());
        userJson.put("surname", u.getSurname());
        userJson.put("displayName", u.getDisplayName());
        userJson.put("name", u.getName());
        LogonDetails logon = EJBLocator.lookupTicketBean().getLogonByUserId(u.getId());
        if(logon!=null){
            userJson.put("logon", logon.getLogonName());
            userJson.put("enabled", logon.isEnabled());
            userJson.put("lockReason", logon.getLockReason());
        } else {
            userJson.put("enabled", true);
            userJson.put("logon", "");
        }
        return userJson;
    }

    /**
     * List all of the template folders in the system
     */
    private void listTemplateFolders(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
                JSONArray templateFoldersJSON = new JSONArray();

                List<TemplateFolder> templateFolders = EJBLocator.lookupStorageBean().listTemplateFolders(ticket);
                for (TemplateFolder templateFolder : templateFolders) {
                    JSONObject templateFolderJSON = new JSONObject();
                    templateFolderJSON.put("id", templateFolder.getId());
                    templateFolderJSON.put("folderId", templateFolder.getFolderId());
                    templateFolderJSON.put("name", templateFolder.getName());
                    templateFolderJSON.put("description", templateFolder.getDescription());

                    templateFoldersJSON.put(templateFolderJSON);
                }

                resultObject.put("templateFolders", templateFoldersJSON);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void saveTemplateFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {

                String folderId = dataObject.getString("folderId");
                String description = dataObject.getString("description");

                Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                if (folder == null) {
                    APIUtils.setError(resultObject, "Folder does not exist");
                }

                EJBLocator.lookupStorageBean().createTemplateFolder(ticket, folder, description);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void deleteTemplateFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {

                String id = dataObject.getString("id");
                TemplateFolder tf = EJBLocator.lookupStorageBean().getTemplateFolder(ticket, id);
                if (tf != null) {
                    EJBLocator.lookupStorageBean().deleteTemplateFolder(ticket, id);
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "TemplateFolder does not exist");
                }
            } else {
                APIUtils.setError(resultObject, "User is not an Administrator");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void reRegisterWorkflowEngines(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().reRegisterWorkflowEngines(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void resetEngineRmiCommunications(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().resetEngineRmiCommunications(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void exitAllEngines(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            int exitCode = dataObject.getInt("exitCode");
            boolean waitForWorkflows = dataObject.getBoolean("waitForWorkflows");
            WorkflowEJBLocator.lookupWorkflowManagementBean().terminateWorkflowEngines(ticket, exitCode, waitForWorkflows);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void rebuildCoreLibrary(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().recreateCoreLibrary(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void flushEngineInformationCaches(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().flushEngineInformationCaches(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void notifyAllFinishedLockHolders(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowLockBean().notifyAllFinishedLockHolders(ticket);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listWorkflowLocks(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List locks;
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                locks = WorkflowEJBLocator.lookupWorkflowLockBean().listAllLocks(ticket);
            } else {
                locks = WorkflowEJBLocator.lookupWorkflowLockBean().listUserLocks(ticket, ticket.getUserId());
            }

            JSONArray locksJson = new JSONArray();
            JSONObject lockJson;
            WorkflowLock lock;
            for(int i=0;i<locks.size();i++){
                lock = (WorkflowLock)locks.get(i);
                lockJson = new JSONObject();
                lockJson.put("id", lock.getId());
                lockJson.put("user", EJBLocator.lookupObjectInfoBean().getObjectName(ticket, ticket.getUserId()));
                lockJson.put("engineId", lock.getEngineId());
                lockJson.put("status", lock.getStatus());
                lockJson.put("workflowName", lock.getWorkflowName());
                if(lock.getLastChangeTime()!=null){
                    lockJson.put("lastChange", format.format(lock.getLastChangeTime()));
                } else {
                    lockJson.put("lastChange", "Unknown");
                }

                if(lock.getStatus().equals(WorkflowLock.LOCK_FILLING)){
                    lockJson.put("statusMessage", "Building");

                } else if(lock.getStatus().equals(WorkflowLock.LOCK_FINISHED)){
                    lockJson.put("statusMessage", "Completed");

                } else if(lock.getStatus().equals(WorkflowLock.LOCK_WAITING)){
                    lockJson.put("statusMessage", "Executing");

                } else if(lock.getStatus().equals(WorkflowLock.LOCK_ERROR)){
                    lockJson.put("statusMessage", "Lock error");

                } else {
                    lockJson.put("statusMessage", "Unknown status");
                }
                locksJson.put(lockJson);
            }

            resultObject.put("locks", locksJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getWorkflowLockMembers(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long lockId = dataObject.getLong("lockId");
            WorkflowLock lock = WorkflowEJBLocator.lookupWorkflowLockBean().getLock(ticket, lockId);
            if(lock!=null){
                List members = WorkflowEJBLocator.lookupWorkflowLockBean().getLockMembers(ticket, lockId);
                WorkflowLockMember member;
                JSONObject memberJson;
                JSONArray membersJson = new JSONArray();
                for(int i=0;i<members.size();i++){
                    member = (WorkflowLockMember)members.get(i);
                    memberJson = new JSONObject();
                    memberJson.put("startTime", format.format(new Date(member.getStartTime())));
                    memberJson.put("invocationId", member.getInvocationId());
                    memberJson.put("engineId", member.getEngineId());
                    memberJson.put("status", member.getInvocationStatus());
                    switch(member.getInvocationStatus()){
                        case WorkflowInvocationFolder.INVOCATION_FINISHED_OK:
                            memberJson.put("statusMessage", "Finished OK");
                            break;

                        case WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS:
                            memberJson.put("statusMessage", "Finished with ERROR");
                            break;

                        case WorkflowInvocationFolder.INVOCATION_RUNNING:
                            memberJson.put("statusMessage", "Running");
                            break;

                        case WorkflowInvocationFolder.INVOCATION_WAITING:
                            memberJson.put("statusMessage", "In Queue");
                            break;

                        case WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER:
                            memberJson.put("statusMessage", "Debuging Wait");
                            break;

                        default:
                            memberJson.put("statusMessage", "Unknown status");
                    }
                    membersJson.put(memberJson);

                }
                resultObject.put("members", membersJson);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such workflow lock");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void deleteWorkflowLock(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long lockId = dataObject.getLong("lockId");
            WorkflowEJBLocator.lookupWorkflowLockBean().removeWorkflowLock(ticket, lockId);

            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void attemptLockRedelivery(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long lockId = dataObject.getLong("lockId");
            WorkflowEJBLocator.lookupWorkflowLockBean().notifyLockHolderIfComplete(ticket, lockId, true);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getSystemDatastoreProperties(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());
            resultObject.put("properties", store.toJson());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void setSystemDatastoreProperties(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());
            if(dataObject.has("properties")){
                store.readJson(dataObject.getJSONObject("properties"));
                store = EJBLocator.lookupStorageBean().saveDataStore(ticket, store);
            }
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void createMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {

            String storeType = dataObject.getString("storeType");
            DataStore store = null;
            if(storeType.equalsIgnoreCase("file")){
                // File data store
                store = new FileDataStore();

            } else if(storeType.equalsIgnoreCase("s3")){
                // Amazon store
                store = new S3DataStore();

            } else if(storeType.equalsIgnoreCase("azure")){
                // Windows Azure store
                store = new AzureBlobStore();

            } else {
                // Default to file
                store = new FileDataStore();
            }
            store.setWriteEnabled(true);    // Make sure the new store is write enabled
            DataStoreMigration migration = EJBLocator.lookupDataStoreMigrationBean().createMigration(ticket, store);
            resultObject.put("migration", createMigrationJson(migration));
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private JSONObject createCredentialsJson(StoredCredentials credentials) throws Exception {
        JSONObject json = new JSONObject();
        json.put("_type", "Credentials");
        json.put("creatorId", credentials.getCreatorId());
        json.put("organisationId", credentials.getOrganisationId());
        json.put("id", credentials.getId());
        json.put("name", credentials.getName());
        json.put("value", credentials.getName());
        json.put("properties", credentials.toJson());
        return json;
    }

    private JSONObject createMigrationJson(DataStoreMigration migration) throws Exception {
        JSONObject migrationJson = new JSONObject();
        DataStore targetStore = migration.getTargetDataStore();
        if(targetStore instanceof FileDataStore){
            migrationJson.put("storeType", "file");

        } else if(targetStore instanceof S3DataStore){
            migrationJson.put("storeType", "file");

        } else if(targetStore instanceof AzureBlobStore){
            migrationJson.put("storeType", "file");

        } else {
            migrationJson.put("storeType", "file");
        }
        migrationJson.put("userId", migration.getUserId());
        migrationJson.put("id", migration.getId());
        migrationJson.put("status", migration.getStatus());

        switch(migration.getStatus()){
            case DataStoreMigration.MIGRATION_FINISHED:
                migrationJson.put("statusMessage", "Finished");
                break;

            case DataStoreMigration.MIGRATION_RUNNING:
                migrationJson.put("statusMessage", "Running");
                break;

            case DataStoreMigration.MIGRATION_STOPPED:
                migrationJson.put("statusMessage", "Not Running");
                break;

            default:
                migrationJson.put("statusMessage", "UNKNOWN");
        }

        if(targetStore!=null){
            migrationJson.put("properties", targetStore.toJson());
        } else {
            migrationJson.put("properties", new JSONObject());
        }

        return migrationJson;
    }

    private void startMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            EJBLocator.lookupDataStoreMigrationBean().startMigration(ticket, migrationId);
            APIUtils.setSuccess(dataObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(dataObject, e);
        }
    }

    private void stopMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            EJBLocator.lookupDataStoreMigrationBean().stopMigration(ticket, migrationId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listMigrations(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List migrations = EJBLocator.lookupDataStoreMigrationBean().listMigrations(ticket);
            JSONArray migrationsJson = new JSONArray();
            for(int i=0;i<migrations.size();i++){
                migrationsJson.put(createMigrationJson((DataStoreMigration)migrations.get(i)));
            }
            resultObject.put("migrations", migrationsJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationID = dataObject.getLong("migrationId");
            DataStoreMigration migration = EJBLocator.lookupDataStoreMigrationBean().getMigration(ticket, migrationID);
            DataStore targetStore = migration.getTargetDataStore();
            resultObject.put("migration", createMigrationJson(migration));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void setMigrationDataStoreProperties(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            JSONObject storeProperties = dataObject.getJSONObject("properties");
            DataStoreMigration migration = EJBLocator.lookupDataStoreMigrationBean().getMigration(ticket, migrationId);
            DataStore targetStore = migration.getTargetDataStore();
            JSONObject properties = dataObject.getJSONObject("properties");
            targetStore.readJson(properties);
            migration.setTargetDataStore(targetStore);
            migration = EJBLocator.lookupDataStoreMigrationBean().saveMigration(ticket, migration);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void resetMigrationDataStore(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            String storeType = dataObject.getString("storeType");
            DataStoreMigration migration = EJBLocator.lookupDataStoreMigrationBean().getMigration(ticket, migrationId);

            DataStore store = null;
            if(storeType.equalsIgnoreCase("file")){
                // File data store
                store = new FileDataStore();

            } else if(storeType.equalsIgnoreCase("s3")){
                // Amazon store
                store = new S3DataStore();

            } else if(storeType.equalsIgnoreCase("azure")){
                // Windows Azure store
                store = new AzureBlobStore();

            } else {
                // Default to file
                store = new FileDataStore();

            }

            // Stop the migration if it was running
            boolean running;
            if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){
                running = true;
                EJBLocator.lookupDataStoreMigrationBean().stopMigration(ticket, migrationId);
            } else {
                running = false;
            }

            // Delete all of the migration documents
            EJBLocator.lookupDataStoreMigrationBean().deleteDocumentsForMigration(ticket, migrationId);
            store.setWriteEnabled(true);
            migration.setTargetDataStore(store);
            EJBLocator.lookupDataStoreMigrationBean().saveMigration(ticket, migration);

            // Recreate the message for the migration
            EJBLocator.lookupDataStoreMigrationBean().addNewMigrationDocuments(ticket, migrationId);

            // Restart if it was running
            if(running){
                EJBLocator.lookupDataStoreMigrationBean().startMigration(ticket, migrationId);
            }

            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void deleteMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            EJBLocator.lookupDataStoreMigrationBean().deleteMigration(ticket, migrationId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void retryFailedDocumentsForMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            EJBLocator.lookupDataStoreMigrationBean().retryFailedDocumentsForMigration(ticket, migrationId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getMigrationState(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            JSONObject state = new JSONObject();

            state.put("migrationId", migrationId);
            state.put("total", EJBLocator.lookupDataStoreMigrationBean().getNumberOfDocumentsInMigration(ticket, migrationId));
            state.put("complete", EJBLocator.lookupDataStoreMigrationBean().getNumberOfDocumentsInMigrationWithStatus(ticket, migrationId, DocumentVersionMigration.COPY_DONE));
            state.put("waiting", EJBLocator.lookupDataStoreMigrationBean().getNumberOfDocumentsInMigrationWithStatus(ticket, migrationId, DocumentVersionMigration.WAITING_FOR_COPY));
            state.put("failed", EJBLocator.lookupDataStoreMigrationBean().getNumberOfDocumentsInMigrationWithStatus(ticket, migrationId, DocumentVersionMigration.COPY_FAILED));

            resultObject.put("state", state);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void finishMigration(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long migrationId = dataObject.getLong("migrationId");
            EJBLocator.lookupDataStoreMigrationBean().finishMigration(ticket, migrationId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void createUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                User u = new User();
                String firstname = dataObject.getString("firstname");
                String lastname = dataObject.getString("lastname");
                String email = dataObject.getString("email");
                String password = dataObject.getString("password");
                String noDash = dataObject.getString("noDash");

                u.setName(firstname + " " + lastname);
                u.setFirstName(firstname);
                u.setSurname(lastname);

                u = EJBLocator.lookupUserDirectoryBean().createAccount(u, email, password);

                if(noDash != null && noDash.equals("true")){
                    EJBLocator.lookupPropertiesBean().setProperty(ticket, u.getId(), "query", "main", "[]");
                }

                resultObject.put("id", u.getId());
                resultObject.put("name", u.getName());

            } else {
                APIUtils.setError(resultObject, "User is not an administrator");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private JSONObject createScannerJson(Ticket ticket, RemoteFilesystemScanner scanner) throws Exception {
        JSONObject scannerJson = new JSONObject();
        scannerJson.put("id", scanner.getId());
        scannerJson.put("userId", scanner.getUserId());
        scannerJson.put("folderId", scanner.getTargetFolderId());
        JSONObject wfJson = new JSONObject();

        // Put in correct workflow data
        wfJson.put("_type", "Workflow");
        if(scanner.getWorkflowId()!=null && ! scanner.getWorkflowId().isEmpty()){
            WorkflowDocument wf = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, scanner.getWorkflowId());
            if(wf!=null){
                wfJson.put("value", wf.getName());
                wfJson.put("id", wf.getId());
            } else {
                wfJson.put("value", "Missing workflow");
                wfJson.put("id", scanner.getWorkflowId());
            }
        } else {
            wfJson.put("value", "NONE");
            wfJson.put("id", "");
        }

        scannerJson.put("workflowId", scanner.getWorkflowId());

        JSONObject props = scanner.toJson();

        if(props.has("Credentials")){
            String credentialsName = "Credentials";

            if(props.getJSONObject("Credentials").has("id")){

                String credentialsId = props.getJSONObject("Credentials").getString("id");

                if(credentialsId!=null){
                    try {
                        credentialsName = EJBLocator.lookupCredentialsDirectoryBean().getCredentials(ticket, credentialsId).getName();
                    } catch (Exception e){}
                }
            }
            props.getJSONObject("Credentials").put("name", credentialsName);
            props.getJSONObject("Credentials").put("value", credentialsName);
        }

        props.put("Workflow", wfJson);

        // Remove WorkflowID from the properties because it hsa been replaced with a JSON object
        if(props.has("WorkflowID")){
            props.remove("WorkflowID");
        }
        scannerJson.put("properties", props);

        scannerJson.put("typeName", scanner.getTypeName());
        return scannerJson;
    }

    private JSONObject createRemoteFilesystemJsonObject(RemoteFilesystemObject fsObject) throws Exception {
        JSONObject fsJson = new JSONObject();
        fsJson.put("id", fsObject.getId());
        fsJson.put("size", fsObject.getCurrentSize());
        fsJson.put("name", fsObject.getName());
        fsJson.put("status", fsObject.getStatus());
        fsJson.put("message", fsObject.getStatusMessage());
        return fsJson;
    }

    private void createScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String scannerType = dataObject.getString("scannerType");
            RemoteFilesystemScanner scanner = null;
            String typeName;

            if(scannerType.toLowerCase().contains("filesystem")){
                scanner = new DiskScanner();

            } else if(scannerType.toLowerCase().contains("azure")){
                scanner = new AzureScanner();


            } else if(scannerType.toLowerCase().contains("amazon")){
                scanner = new S3Scanner();

            } else {
                throw new Exception("Unrecognised scanner type: " + scannerType);
            }

            scanner = EJBLocator.lookupScannerBean().saveScanner(ticket, scanner);

            // Attach to a study if needed
            if(dataObject.has("studyId")){
                int studyId = dataObject.getInt("studyId");
                Study s = EJBLocator.lookupStudyBean().getStudy(ticket, studyId);
                if(s!=null){
                    s.setRemoteScannerId(scanner.getId());

                    // Find a folder for the scanner
                    Folder uploadFolder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, s.getDataFolderId(), "In-Field Uploads");
                    if(uploadFolder==null){
                        uploadFolder = new Folder();
                        uploadFolder.setName("In-Field Uploads");
                        uploadFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, s.getDataFolderId(), uploadFolder);
                    }
                    scanner.setTargetFolderId(uploadFolder.getId());
                    scanner.setStudyScanner(true);
                    scanner.setStudyId(s.getId());

                    //set scanner to run each minute
                    scanner.setScanInterval(60);

                    // Setup with an unpacking workflow if needed
                    if(EJBLocator.lookupPreferencesBean().booleanValue("StudyManagement", "AutoEnableUnpackingWorkflow", false)){
                        scanner.setAutomaticWorkflowEnabled(true);
                        if(scanner.getWorkflowId()==null || scanner.getWorkflowId().isEmpty()){
                            scanner.setWorkflowId(EJBLocator.lookupPreferencesBean().stringValue("StudyManagement", "DefaultUnpackingWorkflowID", "workflows-study-defaultunpack"));
                        }
                    }

                    // Add the study ID to the bucket / container name
                    if(scanner instanceof AzureScanner){
                        ((AzureScanner)scanner).setContainerName(((AzureScanner)scanner).getContainerName() + "-" + s.getId());

                        // Get the first visible Azure credentials
                        List credentialsList = EJBLocator.lookupCredentialsDirectoryBean().listCredentials(ticket);
                        AzureCredentials ac = null;
                        for(Object o : credentialsList){
                            if(o instanceof AzureCredentials){
                                ac = (AzureCredentials)o;
                                break;
                            }
                        }
                        if(ac!=null){
                            ((AzureScanner)scanner).setCredentialsId(ac.getId());
                            scanner.setEnabled(true);

                        }

                    } else if(scanner instanceof S3Scanner){
                        ((S3Scanner)scanner).setBucketName(((S3Scanner)scanner).getBucketName() + "-" + s.getId());

                        // Get the first visible Amazon credentials
                        List credentialsList = EJBLocator.lookupCredentialsDirectoryBean().listCredentials(ticket);
                        AmazonCredentials ac = null;
                        for(Object o : credentialsList){
                            if(o instanceof AmazonCredentials){
                                ac = (AmazonCredentials)o;
                                break;
                            }
                        }
                        if(ac!=null){
                            ((S3Scanner)scanner).setCredentialsId(ac.getId());
                            scanner.setEnabled(true);

                        }

                    }
                    EJBLocator.lookupStudyBean().saveStudy(ticket, s);
                    scanner = EJBLocator.lookupScannerBean().saveScanner(ticket, scanner);
                }
            }

            resultObject.put("scanner", createScannerJson(ticket, scanner));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void deleteScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            EJBLocator.lookupScannerBean().removeScanner(ticket, id);

            if(dataObject.has("studyId")){
                int studyId = dataObject.getInt("studyId");
                Study s = EJBLocator.lookupStudyBean().getStudy(ticket, studyId);
                if(s!=null){
                    s.setRemoteScannerId(null);
                    EJBLocator.lookupStudyBean().saveStudy(ticket, s);
                }
            }
            APIUtils.setSuccess(resultObject);

        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void executeScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                scanner.forceUpload(ticket);
                APIUtils.setSuccess(resultObject);
            } else{
                APIUtils.setError(resultObject, "No such scanner: " + id);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }


    private void resetScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                EJBLocator.lookupScannerBean().resetScanner(ticket, id);
                APIUtils.setSuccess(resultObject);
            } else{
                APIUtils.setError(resultObject, "No such scanner: " + id);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listUserScanners(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List scanners = EJBLocator.lookupScannerBean().listScannersForTicket(ticket);
            JSONArray scannersJson = new JSONArray();
            for(int i=0;i<scanners.size();i++){
                scannersJson.put(createScannerJson(ticket, (RemoteFilesystemScanner)scanners.get(i)));
            }
            resultObject.put("scanners", scannersJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listAllScanners(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List scanners = EJBLocator.lookupScannerBean().listScanners(ticket);
            JSONArray scannersJson = new JSONArray();
            for(int i=0;i<scanners.size();i++){
                scannersJson.put(createScannerJson(ticket, (RemoteFilesystemScanner)scanners.get(i)));
            }
            resultObject.put("scanners", scannersJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                resultObject.put("scanner", createScannerJson(ticket, scanner));
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such scanner id: " + id);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void setScannerFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            String folderId = dataObject.getString("folderId");

            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            scanner.setTargetFolderId(folderId);
            scanner = EJBLocator.lookupScannerBean().saveScanner(ticket, scanner);

            resultObject.put("scanner", createScannerJson(ticket, scanner));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void setScannerProperties(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            JSONObject properties = dataObject.getJSONObject("properties");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                scanner.readJson(properties);

                if(properties.has("Workflow")){
                    // Pull out the workflow data
                    JSONObject wfJson = properties.getJSONObject("Workflow");
                    if(wfJson.has("id")){
                        scanner.setWorkflowId(wfJson.getString("id"));
                    }
                }
                scanner = EJBLocator.lookupScannerBean().saveScanner(ticket, scanner);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such scanner: " + id);
            }


        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void enableScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                scanner.setEnabled(true);
                scanner = EJBLocator.lookupScannerBean().saveScanner(ticket, scanner);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such scanner: " + id);
            }

        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void disableScanner(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                scanner.setEnabled(false);
                scanner = EJBLocator.lookupScannerBean().saveScanner(ticket, scanner);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such scanner: " + id);
            }

        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listScannerFiles(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, id);
            if(scanner!=null){
                List fsObjects = EJBLocator.lookupScannerBean().listFilesystemObjects(ticket, id);
                JSONArray filesArray = new JSONArray();
                for(Object o : fsObjects){
                    filesArray.put(createRemoteFilesystemJsonObject((RemoteFilesystemObject)o));
                }
                resultObject.put("files", filesArray);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such scanner: " + id);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void deleteUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String userId = dataObject.getString("id");
            EJBLocator.lookupUserDirectoryBean().deleteAccount(ticket, userId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listCredentialTypes(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            ArrayList<String> types = StoredCredentials.listTypes();
            JSONArray typesJson = new JSONArray();
            for(String s : types){
                typesJson.put(s);
            }
            resultObject.put("types", typesJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void createCredentials(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String credentialsType = dataObject.getString("type");
            StoredCredentials c = StoredCredentials.createCredentials(credentialsType);
            if(c!=null){
                c.setCreatorId(ticket.getUserId());
                c.setOrganisationId(ticket.getOrganisationId());
                c.setName("New Credentials");
                c.setDescription(credentialsType + " credentials");
                c = EJBLocator.lookupCredentialsDirectoryBean().saveCredentials(ticket, c);
                resultObject.put("credentials", createCredentialsJson(c));
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Unknown credentials type: " + credentialsType);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void deleteCredentials(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            EJBLocator.lookupCredentialsDirectoryBean().removeCredentials(ticket, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getCredentials(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            StoredCredentials c = EJBLocator.lookupCredentialsDirectoryBean().getCredentials(ticket, dataObject.getString("id"));
            if(c!=null){
                resultObject.put("credentials", createCredentialsJson(c));
            } else {
                APIUtils.setError(resultObject, "No such credentials");
            }

        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    private void listCredentials(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String credentialsType = null;
            if(dataObject.has("credentialsType")){
                credentialsType = dataObject.getString("credentialsType");
            }
            List results = EJBLocator.lookupCredentialsDirectoryBean().listCredentials(ticket);
            JSONArray credentialsList = new JSONArray();
            StoredCredentials c;
            for(Object o : results){
                c = (StoredCredentials)o;
                if(credentialsType!=null){
                    if(c.getCredentialType().equals(credentialsType)){
                        credentialsList.put(createCredentialsJson(c));
                    }
                } else {
                    credentialsList.put(createCredentialsJson(c));
                }
            }
            resultObject.put("credentials", credentialsList);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void saveCredentials(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONObject credentialsJson = dataObject.getJSONObject("credentials");
            if(credentialsJson.has("id")){
                StoredCredentials c = EJBLocator.lookupCredentialsDirectoryBean().getCredentials(ticket, credentialsJson.getString("id"));
                if(c!=null){
                    if(credentialsJson.has("name")){
                        c.setName(credentialsJson.getString("name").trim());
                    }

                    if(credentialsJson.has("description")){
                        c.setDescription(credentialsJson.getString("description").trim());
                    }

                    if(credentialsJson.has("properties")){
                        c.readJson(credentialsJson.getJSONObject("properties"));
                    }

                    c = EJBLocator.lookupCredentialsDirectoryBean().saveCredentials(ticket, c);
                    resultObject.put("credentials", createCredentialsJson(c));
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "No such credentials");
                }

            } else {
                APIUtils.setError(resultObject, "No ID present in JSON");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void resetAccessKey(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                EJBLocator.lookupOrganisationDirectoryBean().resetAccessKeyToLocalMacAddress();
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Cannot reset MAC access key: User is not an administrator");
            }

        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void listPropertyGroups(Ticket ticket, JSONObject dataObject, JSONObject resultObject){

        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                List<String> names = EJBLocator.lookupPreferencesBean().listPropertyGroupNames();
                JSONArray namesJson = new JSONArray();
                for(String n : names){
                    namesJson.put(n);
                }
                resultObject.put("names", namesJson);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Cannot list property groups: User is not an administrator");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private void getPropertyGroup(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                if(dataObject.has("groupName")){
                    XmlDataStore store = EJBLocator.lookupPreferencesBean().getPropertyGroup(dataObject.getString("groupName"));
                    if(store!=null){
                        JSONXmlDataStoreConverter converter = new JSONXmlDataStoreConverter(store);
                        resultObject.put("properties", converter.toJson());
                        resultObject.put("groupName", dataObject.getString("groupName"));
                        APIUtils.setSuccess(resultObject);
                    } else {
                        APIUtils.setError(resultObject, "Property group: " + dataObject.getString("groupName") + " does not exist");
                    }
                } else {
                    APIUtils.setError(resultObject, "No property groupName specified");
                }
            } else {
                APIUtils.setError(resultObject, "Cannot fetch property group: User is not an administrator");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    private synchronized void savePropertyGroup(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
                if(dataObject.has("groupName") && dataObject.has("properties")){
                    String groupName = dataObject.getString("groupName");
                    XmlDataStore store = EJBLocator.lookupPreferencesBean().getPropertyGroup(groupName);
                    JSONObject propertiesJson = dataObject.getJSONObject("properties");
                    if(store!=null){
                        JSONXmlDataStoreConverter converter = new JSONXmlDataStoreConverter(store);
                        converter.readJson(propertiesJson);
                        if(converter.isDataChanged()){
                            EJBLocator.lookupPreferencesBean().savePropertyGroup(groupName, store);
                            WorkflowEJBLocator.lookupWorkflowManagementBean().refetchServerProperties(ticket);
                            logger.info("Saved properties: " + groupName);
                        }
                        APIUtils.setSuccess(resultObject);

                    } else {
                        APIUtils.setError(resultObject, "Property group: " + dataObject.getString("groupName") + " does not exist");
                    }
                } else {
                    APIUtils.setError(resultObject, "No property groupName specified");
                }
            } else {
                APIUtils.setError(resultObject, "Cannot fetch property group: User is not an administrator");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Lock out an account
     */
    private void lockAccount(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String userId = dataObject.getString("userId");
            String reason = dataObject.getString("reason");

            if(reason.equalsIgnoreCase("Expiry Lock")){
                EJBLocator.lookupTicketBean().lockAccount(ticket, userId, LogonDetails.LOCKED_DUE_TO_PASSWORD_EXPIRY);

            } else if(reason.equalsIgnoreCase("Inactivity Lock")){
                EJBLocator.lookupTicketBean().lockAccount(ticket, userId, LogonDetails.LOCKED_DUE_TO_INACTIVITY);

            } else {
                EJBLocator.lookupTicketBean().lockAccount(ticket, userId, LogonDetails.LOCKED_BY_ADMINISTRATOR);
            }

            User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
            resultObject.put("user", createUserJson(u));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Reinstante an account
     */
    private void unlockAccount(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String userId = dataObject.getString("userId");
            EJBLocator.lookupTicketBean().unlockAccount(ticket, userId);
            User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
            resultObject.put("user", createUserJson(u));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}
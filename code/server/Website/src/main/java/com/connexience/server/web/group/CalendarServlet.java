/*
 * CalendarServlet.java
 */

package com.connexience.server.web.group;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.event.GroupEvent;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * This servlet provides a data source for javascript calendar
 * @author hugo
 */
public class CalendarServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String start = request.getParameter("start");
        String end = request.getParameter("end");


        WebTicket ticket = SessionUtils.getTicket(request);

        try {
            String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
            String groupId = sections[0];
            List events;
            if(start!=null && end!=null){
                long startTime = Long.parseLong(start) * 1000;
                long endTime = Long.parseLong(end) * 1000;
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(startTime);
                Date startDate = c.getTime();
                c.setTimeInMillis(endTime);
                Date endDate = c.getTime();
                events = EJBLocator.lookupEventBean().listGroupEvents(ticket, groupId, startDate, endDate);
            } else {
                events = EJBLocator.lookupEventBean().listGroupEvents(ticket, groupId);
            }
            JSONArray eventsArray = new JSONArray();
            GroupEvent event;
            JSONObject eventJson;
            for(int i=0;i<events.size();i++){
                event = (GroupEvent)events.get(i);
                eventJson = new JSONObject();
                eventJson.put("id", event.getId());
                eventJson.put("start",event.getStartDate().getTime() / 1000);
                eventJson.put("end", event.getEndDate().getTime() / 1000);
                eventJson.put("editable", false);
                eventJson.put("allDay", false);
                eventJson.put("title", event.getName());
                eventsArray.put(eventJson);
            }

            eventsArray.write(out);
        } catch (Exception ex) {
            response.setStatus(500);
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

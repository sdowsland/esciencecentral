/*
 * ProfileServlet.java
 */
package com.connexience.server.web.profile;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.model.social.requests.FriendRequest;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This class provides profile management functionality
 * @author hugo
 */
public class ProfileServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }

                    if (method.equalsIgnoreCase("saveProfile")) {
                        // Update profile
                        saveProfile(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("setProfileImage")){
                        // Set the profile image
                        setProfileImage(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("requestConnection")){
                        // Request a connection to a user
                        requestConnection(ticket, dataObject, resultObject);
                    }  else {
                        APIUtils.setError(resultObject, "Unknwon method: "+ method);
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void requestConnection(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String targetUserId = dataObject.getString("userId");
            FriendRequest req = new FriendRequest();
            req.setName("Friend Request");
            req.setOrganisationId(ticket.getOrganisationId());
            req.setSenderId(ticket.getUserId());
            req.setRecipientId(targetUserId);
            EJBLocator.lookupRequestBean().postFriendRequest(ticket, req);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    private void saveProfile(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String profileId = dataObject.getString("profileId");
            String userId = dataObject.getString("userId");
            String text = dataObject.getString("profileText");
            String firstname = dataObject.getString("firstname");
            String surname = dataObject.getString("surname");
            String email = dataObject.getString("email");
            String website = dataObject.getString("website");

            if(userId.equals(ticket.getUserId())){
                User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
                UserProfile profile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, userId);

                if(profile!=null && profile.getId().equals(profileId)){

                    user.setFirstName(firstname);
                    user.setSurname(surname);
                    profile.setText(text);
                    profile.setEmailAddress(email);
                    profile.setWebsite(website);

                    EJBLocator.lookupUserDirectoryBean().saveUser(ticket, user);
                    EJBLocator.lookupUserDirectoryBean().saveUserProfile(ticket, userId, profile);

                    APIUtils.setSuccess(resultObject);
                    
                } else {
                    APIUtils.setError(resultObject, "Cannot locate profile");
                }
            } else {
                APIUtils.setError(resultObject, "Cannot edit another users profile");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void setProfileImage(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            //get the user
            UserProfile userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, ticket.getUserId());

            String documentId = dataObject.getString("documentId");
            int x = dataObject.getInt("x");
            int y = dataObject.getInt("y");
            int w = dataObject.getInt("w");
            int h = dataObject.getInt("h");

            // Get the document and version
            DocumentRecord document = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
            DocumentVersion version;

            // Get the latest version
            version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, documentId);

            // Locate the correct data store
            DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());

            //get the image out of the store
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            store.writeToStream(document, version, baos);

            //crop the image
            Image image = new ImageIcon(baos.toByteArray()).getImage();
            BufferedImage outImage = toBufferedImage(image);
            BufferedImage img = outImage.getSubimage(x, y, w, h);

            //resize the image
            Image largeImg = img.getScaledInstance(250, 250, Image.SCALE_SMOOTH);
            Image smallImg = img.getScaledInstance(50, 50, Image.SCALE_SMOOTH);

            //create the Large Image
            BufferedImage largeBufferedImg = new BufferedImage(largeImg.getWidth(null), largeImg.getHeight(null), BufferedImage.TYPE_INT_RGB);
            // Copy image to buffered image
            Graphics largeG = largeBufferedImg.createGraphics();
            // Paint the image onto the buffered image
            largeG.drawImage(largeImg, 0, 0, null);
            largeG.dispose();

            //create the small image
            BufferedImage smallBufferedImg = new BufferedImage(smallImg.getWidth(null), smallImg.getHeight(null), BufferedImage.TYPE_INT_RGB);
            // Copy image to buffered image
            Graphics smallG = smallBufferedImg.createGraphics();
            // Paint the image onto the buffered image
            smallG.drawImage(smallImg, 0, 0, null);
            smallG.dispose();

            ByteArrayOutputStream largebaos = new ByteArrayOutputStream();
            ByteArrayOutputStream smallbaos = new ByteArrayOutputStream();

            //save the new image in the database
            ImageIO.write(largeBufferedImg, "jpg", largebaos);
            ImageIO.write(smallBufferedImg, "jpg", smallbaos);

            ImageData largeImageData = EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, userProfile.getId(), largebaos.toByteArray(), ImageData.LARGE_PROFILE);
            ImageData smallImageData = EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, userProfile.getId(), smallbaos.toByteArray(), ImageData.SMALL_PROFILE);

            ImageData largeObjectImageData = EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, ticket.getUserId(), largebaos.toByteArray(), ImageData.LARGE_PROFILE);
            ImageData smallObjectImageData = EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, ticket.getUserId(), smallbaos.toByteArray(), ImageData.SMALL_PROFILE);
            //return the id of the nwew image
            JSONObject j = new JSONObject();
            j.put("largeId", largeImageData.getId());
            j.put("smallId", smallImageData.getId());
            j.put("largeObjectId", largeObjectImageData.getId());
            j.put("smallObjectId", smallObjectImageData.getClass());
            
            resultObject.put("Result", j);

        } catch (Exception e) {
            try {
                JSONObject j = new JSONObject();
                j.put("largeId", "");
                j.put("smallId", "");
                j.put("error", true);
                resultObject.put("Result", j);
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    public static BufferedImage toBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }

        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();

        // Determine if the image has transparent pixels; for this method's
        // implementation, see e661 Determining If an Image Has Transparent Pixels
        boolean hasAlpha = hasAlpha(image);

        //boolean hasAlpha = false;

        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;
            if (hasAlpha) {
                transparency = Transparency.BITMASK;
            }

            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gs.getDefaultConfiguration();
            bimage = gc.createCompatibleImage(
                    image.getWidth(null), image.getHeight(null), transparency);
        } catch (HeadlessException e) {
            // The system does not have a screen
        }

        if (bimage == null) {
            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;
            if (hasAlpha) {
                type = BufferedImage.TYPE_INT_ARGB;
            }
            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
        }

        // Copy image to buffered image
        Graphics g = bimage.createGraphics();

        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);
        g.dispose();

        return bimage;
    }
    
    public static boolean hasAlpha(Image image) {
        // If buffered image, the color model is readily available
        if (image instanceof BufferedImage) {
            BufferedImage bimage = (BufferedImage) image;
            return bimage.getColorModel().hasAlpha();
        }

        // Use a pixel grabber to retrieve the image's color model;
        // grabbing a single pixel is usually sufficient
        PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
        }

        // Get the image's color model
        ColorModel cm = pg.getColorModel();
        return cm.hasAlpha();
    }
    
}

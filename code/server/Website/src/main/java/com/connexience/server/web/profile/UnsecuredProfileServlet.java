/*
 * ProfileServlet.java
 */
package com.connexience.server.web.profile;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.LogonDetails;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.SignatureUtils;
import com.connexience.server.web.APIUtils;
import com.connexience.server.util.IPasswordChecker;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This class provides profile management functionality
 * @author hugo
 */
public class UnsecuredProfileServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }

                    if(method.equalsIgnoreCase("resetPassword")){
                        // Reset a user password
                        resetPassword(dataObject, resultObject);
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception ignored) {
        }
        out.flush();
    }

    private void resetPassword(JSONObject dataObject, JSONObject resultObject){
        try {
            String currentPassword = dataObject.getString("currentPassword");
            String newPassword = dataObject.getString("newPassword");
            String userId = dataObject.getString("userId");
            User u;
            try {

                u = EJBLocator.lookupUserDirectoryBean().authenticateUserFromId(userId, currentPassword);

                //Use basic password checking but allow for overriding
                String passwordCheckerClass = EJBLocator.lookupPreferencesBean().stringValue("Security", "PasswordCheckerClass", "com.connexience.server.util.BasicPasswordChecker");

                Class clazz = Class.forName(passwordCheckerClass);
                IPasswordChecker checker = (IPasswordChecker) clazz.newInstance();
                checker.setUserId(u.getId());
                checker.setProposedPassword(newPassword);

                if(checker.isAcceptable()){
                    Ticket tempTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(u.getId());
                    LogonDetails logon = EJBLocator.lookupTicketBean().getLogonByUserId(u.getId());
                    EJBLocator.lookupUserDirectoryBean().setPassword(tempTicket, u.getId(), logon.getLogonName(), newPassword);

                    // Add to history if needed
                    if(checker.isHistoryCheckSupported()){
                        EJBLocator.lookupUserDirectoryBean().updatePasswordHistory(u.getId(), SignatureUtils.getHashedPassword(newPassword), EJBLocator.lookupPreferencesBean().intValue("Security", "PasswordHistoryLength", 5));
                    }
                    APIUtils.setSuccess(resultObject);
                }
                else{
                    APIUtils.setError(resultObject, "New password rejected: " + checker.getRejectionMessage());
                }
            } catch (Exception e){
                e.printStackTrace();
                APIUtils.setError(resultObject, "Old password is incorrect");
            }
        } catch(Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

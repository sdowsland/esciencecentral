package com.connexience.server.web.login;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.rest.JsonWebToken;
import com.connexience.server.rest.SignatureAlgorithm;
import com.connexience.server.rest.TokenSigner;
import com.connexience.server.rest.util.AuthUtils;
import com.connexience.server.util.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * This servlet is a fixup for the Remember Me + API problem (accessing the API is your session in the website was
 * re-established using Remember Me).
 * <p/>
 * Down the line the website SHOULD be refactored to be entirely API based and responsible for storing its own JWT token
 * from the API in LocalStorage or whatever.
 * <p/>
 * For now, this allows you to acquire a valid token for the website-api if you have a valid stateful session on the
 * Website.
 */
public class TokenServlet extends HttpServlet {
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try {
			Ticket ticket = SessionUtils.getTicket(request);
			User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

			response.setContentType("application/json");
			response.getWriter().println(generateJwtResponse(user, 86400L, request));
		} catch (ConnexienceException e) {
			throw new ServletException(e);
		}
	}

	private String generateJwtResponse(User userId, Long expires, final HttpServletRequest request) throws ConnexienceException {
		final JsonWebToken token = generatePlainToken(userId.getId(), expires, request);
		final String response = generateSignedToken(token);

		return "{'jwt':'" + response + "'}";
	}

	private JsonWebToken generatePlainToken(final String userId, final Long expires, final HttpServletRequest request) {
		JsonWebToken token = new JsonWebToken(SignatureAlgorithm.HS256);

		token.setIssuer("esc");
		token.setAudience(request.getServerName());
		token.setSubject(userId);

		// TODO: Potential to invalidate token by ID
		token.setJwtId(UUID.randomUUID().toString());

		token.setExpiration((System.currentTimeMillis() / 1000 + expires));
		token.setNotBefore(System.currentTimeMillis() / 1000);

		return token;
	}

	private String generateSignedToken(JsonWebToken token) throws ConnexienceException {
		final byte[] key = AuthUtils.getOrganisationKey();

		return new TokenSigner(SignatureAlgorithm.HS256, key).sign(token).toString();
	}
}

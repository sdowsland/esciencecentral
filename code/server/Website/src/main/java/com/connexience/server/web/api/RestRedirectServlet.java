package com.connexience.server.web.api;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Class description here.
 *
 * @author ndjm8
 */
@WebServlet("/rest/*")
public class RestRedirectServlet extends HttpServlet
{
	private void logRequest(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		final String redirectURI = request.getRequestURI().replaceFirst("/beta/rest", "/website-api/rest");

		System.out.println("(!!!) REST [" + request.getMethod() + " '" + request.getRequestURI() + "' -> '" + redirectURI + "']");

		response.sendRedirect(redirectURI);
	}

	@Override
	protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}

	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}

	@Override
	protected void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}

	@Override
	protected void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}

	@Override
	protected void doTrace(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}

	@Override
	protected void doOptions(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logRequest(request, response);
	}
}

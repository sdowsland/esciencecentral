/**
 * Created on 2007-4-14 上午12:54:50
 */
package com.connexience.server.web.login;

import com.connexience.server.web.util.WebUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openid4java.OpenIDException;
import org.openid4java.consumer.*;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.AuthSuccess;
import org.openid4java.message.MessageExtension;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.AxMessage;
import org.openid4java.message.ax.FetchRequest;
import org.openid4java.message.ax.FetchResponse;
import org.openid4java.message.sreg.SRegMessage;
import org.openid4java.message.sreg.SRegResponse;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * @author Sutra Zhou
 */
public class OpenIDServlet extends javax.servlet.http.HttpServlet {

  /**
   *
   */
  private static final long serialVersionUID = -5998885243419513055L;
  private final Log log = LogFactory.getLog(this.getClass());
  private ServletContext context;
  private ConsumerManager manager;
  private String yahooEndpoint = "https://me.yahoo.com";
  private String googleEndpoint = "https://www.google.com/accounts/o8/id";
  private String cedaEndpoint = "https://ceda.ac.uk";

  public void init(ServletConfig config) throws ServletException {
    super.init(config);

    context = config.getServletContext();

    log.debug("context: " + context);

    try {
      // --- Forward proxy setup (only if needed) ---
      // ProxyProperties proxyProps = new ProxyProperties();
      // proxyProps.setProxyName("proxy.example.com");
      // proxyProps.setProxyPort(8080);
      // HttpClientFactory.setProxyProperties(proxyProps);
      this.manager = new ConsumerManager();
      manager.setAssociations(new InMemoryConsumerAssociationStore());
      manager.setNonceVerifier(new InMemoryNonceVerifier(5000));
    } catch (ConsumerException e) {
      throw new ServletException(e);
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
   *      javax.servlet.http.HttpServletResponse)
   */
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    doPost(req, resp);
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
   *      javax.servlet.http.HttpServletResponse)
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    System.out.println("doPost");
    String contextPath = req.getContextPath();

    if ("true".equals(req.getParameter("is_return"))) {
      processReturn(req, resp);
    } else {
      String identifier = req.getParameter("openid_identifier"); //if the user has entered an openid
      String provider = req.getParameter("provider");  //if the user has clicked on one of the buttons
      if (identifier != null) {
        this.authRequest(identifier, req, resp);
      } else if (provider != null) {
        if (provider.equals("google")) {
          this.authRequest(googleEndpoint, req, resp);
        } else if (provider.equals("yahoo")) {
          this.authRequest(yahooEndpoint, req, resp);
        } else {
          this.getServletContext().getRequestDispatcher(contextPath + "/jsp/index.jsp").forward(req, resp);
        }
      } else {
        this.getServletContext().getRequestDispatcher(contextPath + "/jsp/index.jsp").forward(req, resp);
      }
    }
  }

  private void processReturn(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("processReturn");
    Identifier identifier = this.verifyResponse(req);
    log.debug("identifier: " + identifier);
    if (identifier == null) {
      //not logged in
      resp.sendRedirect("../jsp/index.jsp");
    } else {
      //logged in
      req.setAttribute("identifier", identifier.getIdentifier());
      this.getServletContext().getRequestDispatcher("/servlets/login").forward(req, resp);
    }
  }

  // --- placing the authentication request ---
  public String authRequest(String userSuppliedString, HttpServletRequest httpReq, HttpServletResponse httpResp) throws IOException, ServletException {
    try {
      // configure the return_to URL where your application will receive
      // the authentication responses from the OpenID provider
      String returnToUrl = WebUtil.getHostname(httpReq);

//removed for production
//      if (httpReq.getServerPort() != 80) {
//        returnToUrl += ":" + httpReq.getServerPort();
//      }

      returnToUrl += httpReq.getContextPath() + "/servlets/openid?is_return=true";
      System.err.println(returnToUrl);

      // perform discovery on the user-supplied identifier
      List discoveries = manager.discover(userSuppliedString);

      // attempt to associate with the OpenID provider
      // and retrieve one service endpoint for authentication
      DiscoveryInformation discovered = manager.associate(discoveries);

      // store the discovery information in the user's session
      httpReq.getSession().setAttribute("openid-disc", discovered);

      // obtain a AuthRequest message to be sent to the OpenID provider
      AuthRequest authReq = manager.authenticate(discovered, returnToUrl);

      //set the realm to be global
      String address = WebUtil.getHostname(httpReq);

//removed for production
//   if (httpReq.getServerPort() != 80) {
//        address += ":" + httpReq.getServerPort();
//      }
      address += httpReq.getContextPath();
      authReq.setRealm(address);

      FetchRequest fetch = FetchRequest.createFetchRequest();
      if (userSuppliedString.equals(googleEndpoint)) {
        fetch.addAttribute("email", "http://axschema.org/contact/email", true);
        fetch.addAttribute("firstName", "http://axschema.org/namePerson/first", true);
        fetch.addAttribute("lastName", "http://axschema.org/namePerson/last", true);
      } else if (userSuppliedString.equals(yahooEndpoint)) {
        fetch.addAttribute("email", "http://axschema.org/contact/email", true);
        fetch.addAttribute("fullname", "http://axschema.org/namePerson", true);
      } else if (userSuppliedString.toLowerCase().startsWith(cedaEndpoint.toLowerCase())) {
        fetch.addAttribute("firstname", "http://openid.net/schema/namePerson/first", true);
        fetch.addAttribute("lastname", "http://openid.net/schema/namePerson/last", true);
        fetch.addAttribute("email", "http://openid.net/schema/contact/internet/email", true);
      } else {
        fetch.addAttribute("fullname", "http://schema.openid.net/namePerson", true);
        fetch.addAttribute("email", "http://schema.openid.net/contact/email", true);
      }


      // attach the extension to the authentication request
      if (!fetch.getAttributes().isEmpty()) {
        authReq.addExtension(fetch);
      }

      httpReq.getSession().setAttribute("foo", "bar");

      if (!discovered.isVersion2()) {
        // Option 1: GET HTTP-redirect to the OpenID Provider endpoint
        // The only method supported in OpenID 1.x
        // redirect-URL usually limited ~2048 bytes
        httpResp.sendRedirect(authReq.getDestinationUrl(true));
        return null;
      } else {
        // Option 2: HTML FORM Redirection (Allows payloads >2048 bytes)
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/openIdRedirection.jsp");
        httpReq.setAttribute("prameterMap", httpReq.getParameterMap());
        httpReq.setAttribute("message", authReq);
        dispatcher.forward(httpReq, httpResp);
      }
    } catch (OpenIDException e) {
      // present error to the user
      e.printStackTrace();
      httpResp.sendRedirect("../jsp/error.jsp");
    }

    return null;
  }

  // --- processing the authentication response ---
  public Identifier verifyResponse(HttpServletRequest httpReq) {
    try {
      // extract the parameters from the authentication response
      // (which comes in as a HTTP request from the OpenID provider)
      ParameterList response = new ParameterList(httpReq.getParameterMap());

      // retrieve the previously stored discovery information
      DiscoveryInformation discovered = (DiscoveryInformation) httpReq.getSession().getAttribute("openid-disc");

      // extract the receiving URL from the HTTP request
      String hostname = WebUtil.getHostname(httpReq);
//removed for production server
//      if (httpReq.getServerPort() != 80) {
//        hostname += ":" + httpReq.getServerPort();
//      }

      StringBuffer receivingURL = new StringBuffer(hostname
              + httpReq.getContextPath() + httpReq.getServletPath());

      String queryString = httpReq.getQueryString();
      if (queryString != null && queryString.length() > 0) {
        receivingURL.append("?").append(httpReq.getQueryString());
      }

      // verify the response; ConsumerManager needs to be the same
      // (static) instance used to place the authentication request
      VerificationResult verification = manager.verify(receivingURL.toString(), response, discovered);

      // examine the verification result and extract the verified
      // identifier
      Identifier verified = verification.getVerifiedId();
      if (verified != null) {
        AuthSuccess authSuccess = (AuthSuccess) verification.getAuthResponse();

        if (authSuccess.hasExtension(SRegMessage.OPENID_NS_SREG)) {
          MessageExtension ext = authSuccess.getExtension(SRegMessage.OPENID_NS_SREG);
          if (ext instanceof SRegResponse) {
            SRegResponse sregResp = (SRegResponse) ext;
            for (Iterator iter = sregResp.getAttributeNames().iterator(); iter.hasNext();) {
              String name = (String) iter.next();
              String value = sregResp.getParameterValue(name);
              httpReq.setAttribute(name, value);
            }
          }
        }
        if (authSuccess.hasExtension(AxMessage.OPENID_NS_AX)) {
          FetchResponse fetchResp = (FetchResponse) authSuccess.getExtension(AxMessage.OPENID_NS_AX);

          List aliases = fetchResp.getAttributeAliases();
          for (Iterator iter = aliases.iterator(); iter.hasNext();) {
            String alias = (String) iter.next();
            List values = fetchResp.getAttributeValues(alias);
            if (values.size() > 0) {
              log.debug(alias + " : " + values.get(0));
              httpReq.setAttribute(alias, values.get(0));
            }
          }
        }

        return verified; // success
      }
    } catch (OpenIDException e) {
      // present error to the user
      e.printStackTrace();
    }

    return null;
  }
}
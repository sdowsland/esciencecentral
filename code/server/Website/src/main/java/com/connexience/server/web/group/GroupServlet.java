/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.server.web.group;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.security.*;
import com.connexience.server.model.social.event.GroupEvent;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import com.connexience.server.web.util.FlashUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

/** @author hugo */
public class GroupServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }

                    if (method.equalsIgnoreCase("listGroupEvents")) {
                        // List the events for a group
                        listGroupEvents(ticket, dataObject, resultObject, session);

                    } else if (method.equalsIgnoreCase("saveGroupEvent")) {
                        // Save a group event
                        saveGroupEvent(ticket, dataObject, resultObject, session);

                    }  else if (method.equalsIgnoreCase("getEvent")) {
                        // Get an event by ID
                        getEvent(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("deleteEvent")) {
                        // Delete an event
                        deleteEvent(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("shareFile")) {
                        // Share a file with a group
                        shareFile(ticket, dataObject, resultObject);
                    } else if (method.equalsIgnoreCase("changeDefaultProject")) {
                        // Share a file with a group
                        changeDefaultProject(ticket, dataObject, resultObject, request);
                    }
                    else {
                        APIUtils.setError(resultObject, "Unkown method: " + method);
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        out.flush();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** Get an event */
    private void getEvent(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String id = dataObject.getString("id");
            GroupEvent evt = EJBLocator.lookupEventBean().getGroupEvent(ticket, id);
            if (evt != null) {
                resultObject.put("event", createGroupEventJson(evt));
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such event");
            }

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    ;

    /** List all of the events that a group has */
    private void listGroupEvents(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session) {
        try {
            String groupId = dataObject.getString("groupId");
            List events = EJBLocator.lookupEventBean().listGroupEvents(ticket, groupId);
            JSONArray eventsArray = new JSONArray();
            JSONObject eventJson;
            GroupEvent event;
            for (int i = 0; i < events.size(); i++) {
                event = (GroupEvent) events.get(i);
                eventJson = createGroupEventJson(event);
                eventsArray.put(eventJson);
            }
            resultObject.put("events", eventsArray);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Save or update an event */
    private void saveGroupEvent(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session) {
        try {
            JSONObject eventJson = dataObject.getJSONObject("event");

            String id;
            if (eventJson.has("id")) {
                id = eventJson.getString("id");
            } else {
                id = null;
            }
            String groupId = eventJson.getString("groupId");

            GroupEvent event;
            if (id != null && !id.trim().equals("")) {
                // Existing event
                event = EJBLocator.lookupEventBean().getGroupEvent(ticket, id);
                if (event == null) {
                    event = new GroupEvent();
                    event.setGroupId(groupId);
                }
            } else {
                // New event
                event = new GroupEvent();
                event.setGroupId(groupId);
            }

            event.setName(eventJson.getString("name"));
            event.setDescription(eventJson.getString("description"));
            event.setStartDate(new Date(eventJson.getLong("startDateMillis")));
            event.setEndDate(new Date(eventJson.getLong("endDateMillis")));
            event = EJBLocator.lookupEventBean().saveGroupEvent(ticket, event);
            resultObject.put("event", createGroupEventJson(event));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
            e.printStackTrace();
        }
    }

    /** Share a file */
    private void shareFile(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String id = dataObject.getString("id");
            String groupId = dataObject.getString("groupId");
            EJBLocator.lookupAccessControlBean().grantAccess(ticket, groupId, id, Permission.READ_PERMISSION);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Delete an event */
    private void deleteEvent(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String id = dataObject.getString("id");
            GroupEvent evt = EJBLocator.lookupEventBean().getGroupEvent(ticket, id);
            if (evt != null) {
                // Get all of the documents that are contained by the group
                List links = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, evt.getId());
                Group grp = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, evt.getGroupId());
                if (grp != null) {
                    String groupDataFolder = grp.getDataFolder();
                    DocumentRecord doc;
                    for (int i = 0; i < links.size(); i++) {
                        doc = (DocumentRecord) links.get(i);
                        doc.setContainerId(groupDataFolder);
                        EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
                    }
                }

                EJBLocator.lookupEventBean().deleteEvent(ticket, evt);
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Create a JSON object for a group event */
    private JSONObject createGroupEventJson(GroupEvent event) throws JSONException {
        JSONObject eventJson = new JSONObject();
        eventJson.put("id", event.getId());
        eventJson.put("groupId", event.getGroupId());
        eventJson.put("name", event.getName());
        eventJson.put("startDateMillis", event.getStartDate().getTime());
        eventJson.put("endDateMillis", event.getEndDate().getTime());
        eventJson.put("description", event.getDescription());
        eventJson.put("creatorId", event.getCreatorId());
        return eventJson;
    }


    /** Share a file */
    private void changeDefaultProject(WebTicket ticket, JSONObject dataObject, JSONObject resultObject, HttpServletRequest request) {
        try {

            String studyId = dataObject.getString("studyId");
            if(studyId.equals("null")){
                studyId = null;
            }
            String folderId = null;
            Integer projectId = null;
            String projectIdString = null;

            Study study = null;

            //If changing to the user's home folder then set the value to be null
            if (studyId != null) {

                study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.valueOf(studyId));

                if(study != null && study.getDataFolderId() != null && !study.getDataFolderId().equals(""))
                {
                    projectId = study.getId();
                    folderId = study.getDataFolderId();
                }
                else{
                    APIUtils.setError(resultObject, "Project " + studyId + " does not exist or has no data folder");
                }
            }

            //Change via an EJB
            boolean success = EJBLocator.lookupGroupDirectoryBean().changeDefaultStorageFolder(ticket, folderId);
            resultObject.put("success", success);

            //set in the ticket as this isn't serialised back from the bean
            if (success) {
                //make sure we don't set the null string
                if(projectId != null){
                    projectIdString = String.valueOf(projectId);
                }
                ticket.setDefaultStorageFolderId(folderId);
                ticket.setDefaultProjectId(projectIdString);
            }

            if (study != null) {
                //Get the containing object - will be null for home folders or a OldProject for projects
                FlashUtils.setSuccess(request, "Default Project changed to: " + study.getName());
            } else {
                FlashUtils.setSuccess(request, "Default Project changed to Homespace");
            }

            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}

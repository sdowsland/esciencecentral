/*
 * DownloadServlet.java
 */
package com.connexience.server.web.download;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class provides a redirect from /data/id to the data properties page
 * @author simon
 */
public class PermalinkServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=3600,no-cache,post-check=0,pre-check=0");

        String url = request.getRequestURI();
        String documentId = url.substring( url.lastIndexOf('/')+1, url.length() );

        response.sendRedirect("../pages/data/properties.jsp?id=" + documentId);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Permalink servlet";
    }
}

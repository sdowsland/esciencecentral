/*
 * QuickViewServlet.java
 */

package com.connexience.server.web.viewer;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.SearchFolder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.properties.PropertyGroup;
import com.connexience.server.model.properties.PropertyItem;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.OldProject;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.util.JSONDate;
import com.connexience.server.util.JSONProject;
import com.connexience.server.util.JSONServerObject;
import com.connexience.server.util.NameCache;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.codehaus.jackson.map.util.JSONPObject;


/**
 * Provides support for the quick view dialog
 * @author hugo
 */
public class ViewerServlet extends HttpServlet {
    DateFormat format = DateFormat.getDateTimeInstance();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
        
        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }
                    
                    if (method.equalsIgnoreCase("getFile")) {
                        // Remove a workflow
                        getFile(ticket, dataObject, resultObject, session);

                    } else if(method.equalsIgnoreCase("listMimeTypePreferences")){
                        // Get the mime type preferences for the user
                        listMimeTypePreferences(ticket, dataObject, resultObject, session);
                        
                    } else if(method.equalsIgnoreCase("getFolderAndContents")){
                        // Get all of the documents in a folder */
                        getFolderAndContents(ticket, dataObject, resultObject, session);

                    } else if(method.equalsIgnoreCase("getVersions")){
                        // Get the versions of a file
                        getVersions(ticket, dataObject, resultObject);
 
                    } else if(method.equalsIgnoreCase("saveMimeType")){
                        // Update a mime type
                        saveMimeType(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("deleteMimeType")){
                        // Delete a mime type
                        deleteMimeType(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getRootFolders")){
                        // List the available root folders
                        getRootFolders(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getProjectDetails")){
                        // Get any project details for a document
                        getProjectDetails(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listServerObjects")){
                        // List server objects that a user can see
                        listServerObjects(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listProjects")){
                        // List projects visible to ticket 
                        listProjects(ticket, dataObject, resultObject);
                        
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    }
 
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Quick viewer support servlet";
    }

    /** List server objects that a user has access to */
    private void listServerObjects(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String className = null;
            if(dataObject.has("className")){
                className = dataObject.getString("className");
            } else {
                className = ServerObject.class.getName();
            }
            
            Class objectClass = Class.forName(className);
            List results = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, ticket.getUserId(), objectClass, 0, 0);
            JSONArray objects = new JSONArray();
            for(int i=0;i<results.size();i++){
                objects.put(new JSONServerObject((ServerObject)results.get(i)));
            }
            resultObject.put("objects", objects);
            resultObject.put("objectCount", results.size());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** List projects */
    private void listProjects(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            Integer visible = EJBLocator.lookupProjectsBean().getVisibleProjectCount(ticket);
            List<Project> results = EJBLocator.lookupProjectsBean().getVisibleProjects(ticket, 0, visible);
            JSONArray projects = new JSONArray();
            for(Project p : results){
                projects.put(new JSONProject(p));
            }
            resultObject.put("projects", projects);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Create basic JSON properties for a ServerObject */
    private JSONObject createServerObjectJson(ServerObject object) throws Exception {
        JSONObject objectJson = new JSONObject();
        objectJson.put("id", object.getId());
        objectJson.put("name", object.getName());
        objectJson.put("description", object.getDescription());
        objectJson.put("className", object.getClass().getName());
        return objectJson;
    }
    
    /** Delete a mime type */
    private void deleteMimeType(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            EJBLocator.lookupStorageBean().removeDocumentType(ticket, dataObject.getString("id"));
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Save a mime type */
    private void saveMimeType(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONObject mimeType = dataObject.getJSONObject("mimeType");
            if(mimeType.has("id") && !mimeType.getString("id").isEmpty()){
                // Existing mime
                DocumentType docType = EJBLocator.lookupStorageBean().getDocumentType(ticket, mimeType.getString("id"));
                if(docType!=null){
                    docType.setExtension(mimeType.getString("extension"));
                    docType.setMimeType(mimeType.getString("mimeType"));
                    docType.setDescription(mimeType.getString("description"));
                    EJBLocator.lookupStorageBean().saveDocumentType(ticket, docType);
                } else {
                    APIUtils.setError(resultObject, "No such mime type");
                }
            } else {
                // New mime
                DocumentType docType = new DocumentType();
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
                docType.setCreatorId(ticket.getUserId());
                docType.setOrganisationId(ticket.getOrganisationId());
                docType.setContainerId(org.getDocumentTypesFolderId());
                docType.setExtension(mimeType.getString("extension"));
                docType.setMimeType(mimeType.getString("mimeType"));
                docType.setDescription(mimeType.getString("description"));
                EJBLocator.lookupStorageBean().saveDocumentType(ticket, docType);
            }
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get a file from the filesystem */
    private void getFile(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            String documentId = dataObject.getString("documentId");
            DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
            DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, documentId);
            if(doc!=null){
                JSONObject docJson = new JSONObject();
                docJson.put("containerId", doc.getContainerId());
                docJson.put("creationDate", doc.getCreationDate());
                docJson.put("creatorId", doc.getCreatorId());
                docJson.put("currentVersionNumber", doc.getCurrentVersionNumber());
                docJson.put("description", doc.getDescription());
                docJson.put("versioned", doc.isVersioned());
                docJson.put("name", doc.getName());
                docJson.put("id", doc.getId());
                docJson.put("latestVersionId", version.getId());

                // Try and work out the mime type
                if(doc.getDocumentTypeId()!=null && !doc.getDocumentTypeId().equalsIgnoreCase("")){
                    DocumentType docType = EJBLocator.lookupStorageBean().getDocumentType(ticket, doc.getDocumentTypeId());
                    if(docType!=null){
                        docJson.put("mimeKnown", true);
                        docJson.put("mimeType", docType.getMimeType());
                    } else {
                        docJson.put("mimeKnown", false);
                    }
                } else {
                    docJson.put("mimeKnown", false);
                }

                // Does the document have a mime override set
                PropertyGroup docViewerPreferences = EJBLocator.lookupPropertiesBean().getPropertyGroup(ticket, documentId, "MimePreferences");
                if(docViewerPreferences!=null){
                    String overrideAppId = docViewerPreferences.getValue("ApplicationID", "");
                    resultObject.put("overrideApplicationId", overrideAppId);
                    if(overrideAppId.equals("")){
                        resultObject.put("overrideIsInternal", true);
                    } else {
                        resultObject.put("overrideIsInternal", false);
                    }
                    resultObject.put("mimeOverride", true);
                } else {
                    resultObject.put("mimeOverride", false);
                }

                resultObject.put("mimePreferences", createMimePreferences(ticket, session));
                resultObject.put("mimeTypes", createMimeTypeList(ticket, session));
                resultObject.put("document", docJson);
                resultObject.put("documentExists", true);
            } else {
                resultObject.put("documentExists", false);
            }
            
            // Add in quota details
            addQuotaData(resultObject, ticket);
            
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Add quota information to the result object */
    private void addQuotaData(JSONObject resultObject, Ticket ticket) throws Exception {
        if(EJBLocator.lookupQuotaBean().userHasStorageQuota(ticket, ticket.getUserId())){
            if(EJBLocator.lookupQuotaBean().getAvailableStorageQuota(ticket, ticket.getUserId()) > 0){
                resultObject.put("quotaOk", true);
            } else {
                resultObject.put("quotaOk", false);
            }
        } else {
            resultObject.put("quotaOk", true);
        }        
    }

    /** List all the server recognised mime types */
    private JSONArray createMimeTypeList(Ticket ticket, HttpSession session) throws Exception {
            JSONObject mimeType;
            JSONArray mimeTypes = new JSONArray();
            List docTypes = EJBLocator.lookupStorageBean().listDocumentTypes(ticket);
            DocumentType docType;

            for(int i=0;i<docTypes.size();i++){
                docType = (DocumentType)docTypes.get(i);
                mimeType = new JSONObject();
                mimeType.put("extension", docType.getExtension());
                mimeType.put("mimeType", docType.getMimeType());
                mimeType.put("description", docType.getDescription());
                mimeType.put("id", docType.getId());
                mimeTypes.put(mimeType);
            }
            return mimeTypes;

    }

    /** Create a mime type JSONArray */
    private JSONArray createMimePreferences(Ticket ticket, HttpSession session) throws Exception {
        // Check to see if there is a mime type map in the session
        HashMap<String, DocumentType> mimeMap = getMimeMap(ticket, session);

        PropertyGroup mimeProperties = EJBLocator.lookupPropertiesBean().getPropertyGroup(ticket, ticket.getUserId(), "MimePreferences");
        if(mimeProperties==null){
            // Need to create a new group
            mimeProperties = new PropertyGroup();
            mimeProperties.setDescription("User editor type preferences");
            mimeProperties.setName("MimePreferences");
            mimeProperties.setObjectId(ticket.getUserId());
            mimeProperties.setObjectProperty(true);
            mimeProperties.setOrganisationId(ticket.getOrganisationId());
            mimeProperties = EJBLocator.lookupPropertiesBean().savePropertyGroup(ticket, mimeProperties);
        }

        JSONObject mimePreference;
        JSONArray mimePreferences = new JSONArray();
        PropertyItem property;
        String extension;
        DocumentType docType;

        // Only include external applications
        int size = mimeProperties.getSize();
        int count = 0;
        for(int i=0;i<size;i++){
            property = mimeProperties.getProperty(i);
            mimePreference = new JSONObject();
            // Search for file extension
            extension = property.getName().trim().toLowerCase();
            if(mimeMap.containsKey(extension)){
                docType = mimeMap.get(extension);
                mimePreference.put("extension", extension);
                mimePreference.put("mimeType", docType.getMimeType());
                mimePreference.put("internalViewer", false);
                mimePreference.put("applicationId", property.getValue());
                mimePreferences.put(mimePreference);
                count++;
            }
        }
        return mimePreferences;

    }

    /** List the additional mime type preferences */
    private void listMimeTypePreferences(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            resultObject.put("mimeTypes", createMimeTypeList(ticket, session));
            resultObject.put("preferences", createMimePreferences(ticket, session));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Check to see if the mime map is present and create one if it isn't */
    private HashMap<String,DocumentType> getMimeMap(Ticket ticket, HttpSession session) throws Exception {
        // Check to see if there is a mime type map in the session
        HashMap<String, DocumentType> mimeMap;
        if(session.getAttribute("MIME_MAP")==null){
            mimeMap = createMimeMap(ticket);
            session.setAttribute("MIME_MAP", mimeMap);
        } else {
            mimeMap = (HashMap<String,DocumentType>)session.getAttribute("MIME_MAP");
        }
        return mimeMap;
    }

    /** Create a Mime map */
    private HashMap<String,DocumentType> createMimeMap(Ticket ticket) throws Exception {
        List docTypes = EJBLocator.lookupStorageBean().listDocumentTypes(ticket);
        HashMap<String,DocumentType> mimeMap = new HashMap<>();
        DocumentType docType;
        for(int i=0;i<docTypes.size();i++){
            docType = (DocumentType)docTypes.get(i);
            mimeMap.put(docType.getExtension(), docType);
        }
        return mimeMap;
    }


    /** Get the project / study details for a file */
    private void getProjectDetails(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            if(dataObject.has("id")){
                String id = dataObject.getString("id");
                DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, id);
                if(doc!=null){
                    String projectId = doc.getProjectId();
                    if(projectId!=null && !projectId.isEmpty()){
                        Project project = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(projectId));
                        if(project!=null){
                            JSONObject projectJson = new JSONObject();
                            projectJson.put("name", project.getName());
                            projectJson.put("owner", EJBLocator.lookupObjectInfoBean().getObjectName(ticket, project.getOwnerId()));
                            projectJson.put("description", project.getDescription());
                            
                            if(project instanceof Study){
                                projectJson.put("startDate", format.format(((Study)project).getStartDate()));
                                projectJson.put("endDate", format.format(((Study)project).getEndDate()));
                            }
                            
                            JSONArray propertyNames = new JSONArray();
                            JSONArray propertyValues = new JSONArray();
                            
                            Map<String,String> props = project.getAdditionalProperties();
                            for(String key : props.keySet()){
                                propertyNames.put(key);
                                propertyValues.put(props.get(key));
                            }
                            
                            JSONObject propertiesJson = new JSONObject();
                            propertiesJson.put("names", propertyNames);
                            propertiesJson.put("values", propertyValues);
                            projectJson.put("properties", propertiesJson);
                            resultObject.put("hasProject", true);
                            resultObject.put("project", projectJson);
                            
                        } else {
                            resultObject.put("hasProject", false);
                        }
                    } else {
                        resultObject.put("hasProject", false);
                    }
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "No such document: " + id);
                }
            } else {
                APIUtils.setError(resultObject, "No document id specified");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get a folder and its contents */
    private void getFolderAndContents(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            String folderId = dataObject.getString("folderId");
            Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);

            List contents = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, folderId);
            DocumentRecord doc;
            JSONObject fileJson;
            JSONArray files = new JSONArray();

            JSONObject folderJson = new JSONObject();
            folderJson.put("id", folder.getId());
            folderJson.put("name", folder.getName());
            folderJson.put("containerId", folder.getContainerId());
            folderJson.put("creatorId", folder.getCreatorId());
            folderJson.put("description", folder.getDescription());

            for(int i=0;i<contents.size();i++){
                doc = (DocumentRecord)contents.get(i);
                fileJson = new JSONObject();
                fileJson.put("id", doc.getId());
                fileJson.put("name", doc.getName());
                fileJson.put("creatorId", doc.getCreatorId());
                fileJson.put("description", doc.getDescription());
                fileJson.put("containerId", doc.getContainerId());
                files.put(fileJson);
            }

            resultObject.put("folder", folderJson);
            resultObject.put("files", files);

            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** List the versions of a file */
    private void getVersions(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String documentId = dataObject.getString("documentId");
            NameCache cache = new NameCache(1000);
            
            DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
            List versions = EJBLocator.lookupStorageBean().listVersions(ticket, documentId);
            JSONArray versionsArray = new JSONArray();
            JSONObject versionObject;
            DocumentVersion version = new DocumentVersion();
            for(int i=0;i<versions.size();i++){
                version = (DocumentVersion)versions.get(i);
                versionObject = new JSONObject();

                versionObject.put("documentId", documentId);
                versionObject.put("id", version.getId());
                versionObject.put("timestamp", format.format(version.getTimestampDate()));
                versionObject.put("size", version.getSize());
                versionObject.put("userId", version.getUserId());
                versionObject.put("comments", version.getComments());
                versionObject.put("versionNumber", version.getVersionNumber());
                versionObject.put("userName", cache.getObjectName(ticket, version.getUserId()));
                versionsArray.put(versionObject);
            }

            JSONObject fileJson = new JSONObject();
            fileJson.put("id", doc.getId());
            fileJson.put("name", doc.getName());
            fileJson.put("creatorId", doc.getCreatorId());
            fileJson.put("description", doc.getDescription());
            fileJson.put("containerId", doc.getContainerId());
            fileJson.put("latestVersionId", version.getId());
            // Add the project ID if there is one
            if(doc.getProjectId()!=null &&! doc.getProjectId().isEmpty()){
                fileJson.put("projectId", doc.getProjectId());
            }
            
            resultObject.put("document", fileJson);

            resultObject.put("versions", versionsArray);
            
            // Add in quota details
            addQuotaData(resultObject, ticket);
            
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }


    /** Define the types of events we are interesed in for differen object types */
    public Vector<String> defineEventTypesForObjectType(ServerObject so) {
        Vector<String> eventTypes = new Vector<>();

        if (so instanceof WorkflowDocument) {
            eventTypes.add("READ");
            eventTypes.add("WRITE");
            eventTypes.add("WORKFLOW_START");
            eventTypes.add("WORKFLOW_COMPLETE");
            eventTypes.add("WRITE_BY_WORKFLOW");
            eventTypes.add("READ_BY_WORKFLOW");
            eventTypes.add("SHARE");
        } else if (so instanceof DocumentRecord) {
            eventTypes.add("READ");
            eventTypes.add("WRITE");
            eventTypes.add("WORKFLOW_START");
            eventTypes.add("WORKFLOW_COMPLETE");
            eventTypes.add("WRITE_BY_WORKFLOW");
            eventTypes.add("READ_BY_WORKFLOW");
            eventTypes.add("SHARE");
        } else if (so instanceof Folder) {
            eventTypes.add("READ");
            eventTypes.add("WRITE");
            eventTypes.add("SHARE");
        } else if (so instanceof User) {
            eventTypes.add("REGISTER");
            eventTypes.add("LOGIN");
            eventTypes.add("WORKFLOW_START");
            eventTypes.add("WORKFLOW_COMPLETE");
            eventTypes.add("WRITE");
            eventTypes.add("READ");
            eventTypes.add("SHARE");
            eventTypes.add("MAKE_FRIEND");
            eventTypes.add("MAKE_GROUP");
            eventTypes.add("JOIN_GROUP");
        } else if (so instanceof Group) {
            eventTypes.add("SHARE");
            eventTypes.add("MAKE_GROUP");
            eventTypes.add("JOIN_GROUP");
        }
        return eventTypes;
    }

    private String prettyPrintStat(String operation) {
        if (operation.equals("REGISTER")) {
            return "Register: ";
        } else if (operation.equals("LOGIN")) {
            return "Logged in: ";
        } else if (operation.equals("WORKFLOW_START")) {
            return "Workflow started: ";
        } else if (operation.equals("WORKFLOW_COMPLETE")) {
            return "Workflow completed: ";
        } else if (operation.equals("SHARE")) {
            return "Shared: ";
        } else if (operation.equals("MAKE_FRIEND")) {
            return "Became Friends: ";
        } else if (operation.equals("MAKE_GROUP")) {
            return "Created Group: ";
        } else if (operation.equals("JOIN_GROUP")) {
            return "Joined Group: ";
        } else if (operation.equals("WRITE")) {
            return "Written: ";
        } else if (operation.equals("WRITE_BY_WORKFLOW")) {
            return "Written by workflow: ";
        } else if (operation.equals("READ")) {
            return "Read: ";
        } else if (operation.equals("READ_BY_WORKFLOW")) {
            return "Read by workflow: ";
        } else {
            return "Unknown event type";
        }
    }
    
    private void getRootFolders(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONArray roots = new JSONArray();
            JSONObject obj;
            
            // Add the home folder
            roots.put(APIUtils.createServerObjectJson(EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId())));
            
            // Add projects
            //add the other projects
            List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, Integer.MAX_VALUE);
            for (Project p : projects ) {
                obj = APIUtils.createProjectJson(p);
                obj.put("storageFolderId", p.getDataFolderId());
                roots.put(obj);
            }        


            // Add search folders
            List searchFolders = EJBLocator.lookupObjectDirectoryBean().getOwnedObjects(ticket, ticket.getUserId(), SearchFolder.class, 0, 0);
            for(int i=0;i<searchFolders.size();i++){
                roots.put(APIUtils.createServerObjectJson((SearchFolder)searchFolders.get(i)));
            }
            
            resultObject.put("folders", roots);
            
            // Add the project ID if there is one
            if(ticket.getDefaultProjectId()!=null){
                resultObject.put("projectId", ticket.getDefaultProjectId());
            }

            //Add the default which will either be the project data folder or the user's home folder
            if(ticket.getDefaultStorageFolderId() != null){

            resultObject.put("defaultLocation", ticket.getDefaultStorageFolderId());
            }
            else{
                resultObject.put("defaultLocation", EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getHomeFolderId());
            }
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}
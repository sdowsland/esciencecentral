/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.image;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.util.DigestBuilder;
import com.connexience.server.util.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * This servlet will get images from the image table of the database
 * <p/>
 * User: nsjw7
 * Date: Jul 31, 2009
 * Time: 1:18:06 PM
 */
public class ImageServlet extends HttpServlet
{

  /**
   * Get an image from the database.
   * Paramters: imageId - the id of the image in the database
   * or soid and type where soid is the serverObjectId the image is attached to and type is ImageData.Large_Profile etc.
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    String imageId = request.getParameter("imageId");

    //header to stop IE caching GET requests as we're not doing 100% pure REST
    response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
    response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");


    String soid = request.getParameter("soid");
    String type = request.getParameter("type");
    WebTicket ticket = SessionUtils.getTicket(request);

    try
    {
      ImageData image = null;
      if (imageId != null)
      {
        image = EJBLocator.lookupObjectDirectoryBean().getImageFromId(ticket, imageId);
      }
      else if (soid != null && type == null)
      {
        image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, soid);
      }
      else if (soid != null && type != null)
      {
        image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, soid, type);
      }

      if (image != null)
      {
        response.setContentType("image/jpeg");
        // Start streaming
        OutputStream stream = response.getOutputStream();
        stream.write(image.getData());
        stream.flush();
        stream.close();
      }
      else //write the default image
      {
          if(type==null){
                response.sendRedirect("../styles/common/images/profile.png");
          } else {

              String currentPath = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

              if(type.equals(ImageData.LARGE_PROFILE)){

                  if(EJBLocator.lookupPreferencesBean().booleanValue("Website", "UseGravatar", true)){
                      try {
                          UserProfile userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, soid);

                          String baseUrl = "http://www.gravatar.com/avatar/";
                          String referer = request.getHeader("referer");
                          if(request.isSecure() || (referer != null && referer.startsWith("https"))){
                              baseUrl = "https://www.gravatar.com/avatar/";
                          }

                          String hash = DigestBuilder.calculateMD5(userProfile.getEmailAddress());
                          String size = "300";

                          if(request.getServerName().equals("localhost")){
                              response.sendRedirect(baseUrl + hash + "?s=" + size);
                          }
                          else {
                              String defaultImage = currentPath + "/styles/common/images/profile.100x100.png";
                              response.sendRedirect(baseUrl + hash + "?s=" + size + "&d=" + defaultImage);
                          }
                      }
                      catch(Exception e) {
                          response.sendRedirect("../styles/common/images/profile.300x300.png");
                      }
                  }
                  else {
                      response.sendRedirect("../styles/common/images/profile.300x300.png");
                  }

              } else if(type.equals(ImageData.SMALL_PROFILE)){

                  if(EJBLocator.lookupPreferencesBean().booleanValue("Website", "UseGravatar", true)){
                      try {
                          UserProfile userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, soid);

                          String baseUrl = "http://www.gravatar.com/avatar/";
                          String referer = request.getHeader("referer");
                          if(request.isSecure() || (referer != null && referer.startsWith("https"))){
                              baseUrl = "https://www.gravatar.com/avatar/";
                          }

                          String hash = DigestBuilder.calculateMD5(userProfile.getEmailAddress());
                          String size = "100";

                          if(request.getServerName().equals("localhost")){
                              response.sendRedirect(baseUrl + hash + "?s=" + size);
                          }
                          else {
                              String defaultImage = currentPath + "/styles/common/images/profile.100x100.png";
                              response.sendRedirect(baseUrl + hash + "?s=" + size + "&d=" + defaultImage);
                          }
                      }
                      catch(Exception e) {
                          response.sendRedirect("../styles/common/images/profile.100x100.png");
                      }
                  }
                  else {
                      response.sendRedirect("../styles/common/images/profile.100x100.png");
                  }

              } else if(type.equals(ImageData.WORKFLOW_PREVIEW)){
                  response.sendRedirect("../images/workflow-example.png");
              } else if(type.equals(ImageData.WORKFLOW_BLOCK_ICON)){
                  response.sendRedirect("../styles/common/images/blockicon.png");
              }
          }
      }
    }
    catch (ConnexienceException e)
    {
      e.printStackTrace();
      response.sendRedirect("../styles/common/images/profile.png");
    }

  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request  servlet request
   * @param response servlet response
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    throw new ServletException("Post not supported");
  }

}

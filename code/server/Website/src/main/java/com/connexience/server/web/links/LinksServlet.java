/*
 * LinksServlet.java
 */

package com.connexience.server.web.links;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.Link;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

/**
 * This servlet provides the API for managing links between objects on the server
 * @author hugo
 */
public class LinksServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }

                    if (method.equalsIgnoreCase("getLinks")) {
                        // Get all of the linked objects
                        getLinks(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("linkObjects")){
                        // Link two objects together
                        linkObjects(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("deleteLink")){
                        // Delete a link
                        deleteLink(ticket, dataObject, resultObject);
                        
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** Get the links to an object */
    private void getLinks(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, id, ServerObject.class);
            Iterator links = EJBLocator.lookupLinkBean().getLinkedSourceObjects(ticket, obj).iterator();

            ServerObject linkedObject;
            JSONArray linksArray = new JSONArray();
            JSONObject linkJson;

            while(links.hasNext()){
                linkedObject = (ServerObject)links.next();
                linkJson = new JSONObject();

                linkJson.put("id", linkedObject.getId());
                linkJson.put("name", linkedObject.getName());
                linkJson.put("creatorId", linkedObject.getCreatorId());
                linkJson.put("description", linkedObject.getDescription());
                linkJson.put("type", linkedObject.getClass().getSimpleName());
                
                EJBLocator.lookupObjectInfoBean().getObjectName(ticket, id);

                linksArray.put(linkJson);
            }

            resultObject.put("links", linksArray);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Link two objects together */
    private void linkObjects(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String sourceId = dataObject.getString("sourceId");
            String sinkId = dataObject.getString("sinkId");

            ServerObject sourceObject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, sourceId, ServerObject.class);
            ServerObject sinkObject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, sinkId, ServerObject.class);

            if(sourceObject!=null && sinkObject!=null){
                Link link = EJBLocator.lookupLinkBean().createLink(ticket, sourceObject, sinkObject);
                JSONObject linkJson = new JSONObject();
                linkJson.put("id", link.getId());
                resultObject.put("link", linkJson);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Cannot find object to create link");
            }

        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Delete a link */
    private void deleteLink(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String sourceId = dataObject.getString("sourceId");
            String sinkId = dataObject.getString("sinkId");
            ServerObject sourceObject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, sourceId, ServerObject.class);
            ServerObject sinkObject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, sinkId, ServerObject.class);
            if(sourceObject!=null && sinkObject!=null){
                Link link = EJBLocator.lookupLinkBean().getLink(ticket, sourceObject, sinkObject);
                if(link!=null){
                    EJBLocator.lookupLinkBean().removeLink(ticket, link.getId());
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "Objects are not linked");
                }
            } else {
                APIUtils.setError(resultObject, "Cannot find objects to remove link from");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}

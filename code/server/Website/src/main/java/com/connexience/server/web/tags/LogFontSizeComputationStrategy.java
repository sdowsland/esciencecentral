package com.connexience.server.web.tags;

public class LogFontSizeComputationStrategy extends FontSizeComputationStrategyImpl
    implements FontSizeComputationStrategy
{

  public LogFontSizeComputationStrategy(int numSizes, String prefix)
  {
    super(numSizes, prefix);
  }

  protected double scaleCount(double count)
  {
    return Math.log10(count);
  }
}

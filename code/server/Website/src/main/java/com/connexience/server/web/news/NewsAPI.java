/*
 * FileAPI.java
 */

package com.connexience.server.web.news;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This servlet provides file storage and access methods via JSON for the web pages
 *
 * @author hugo
 */
public class NewsAPI extends HttpServlet
{


  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   *
   * @param request  servlet request
   * @param response servlet response
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {

    //header to stop IE caching GET requests as we're not doing 100% pure REST
    response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
    response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

    PrintWriter out = response.getWriter();
    WebTicket ticket = SessionUtils.getTicket(request);
    JSONObject resultObject = new JSONObject();
    JSONObject dataObject = null;

    try
    {
      if (ticket != null)
      {
        String data = APIUtils.extractString(request.getInputStream());
        String method = request.getParameter("method");
        String soid = request.getParameter("id");
        String themeFolder = request.getParameter("themeFolder");

        if (method != null)
        {
          //get any post data that might be there (needs to be in JSON format)
          if (data != null && !data.equals(""))
          {
            dataObject = new JSONObject(data);
          }
          else if (method.equalsIgnoreCase("getHistory"))
          {
            getHistory(resultObject, ticket, soid);
          }
          else if (method.equalsIgnoreCase("getNews"))
          {
            getUserNews(resultObject, ticket, themeFolder);
          }
          else if (method.equalsIgnoreCase("getGroupNews"))
          {
            getGroupNews(resultObject, ticket, soid, themeFolder);
          }
        }
      }
      else
      {
        APIUtils.setError(resultObject, "No user logged in");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
    finally
    {
      try
      {
        resultObject.write(out);
      }
      catch (Exception e)
      {
        System.err.println(e.getMessage());
      }
      out.close();
    }
  }


  /*
 * Get the History as JSON
 * */
  private void getHistory(JSONObject resultObject, WebTicket ticket, String soid)
  {
    try
    {
      ServerObject so = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, soid, ServerObject.class);
      if (so != null)
      {
        if (so.getCreatorId().equals(ticket.getUserId()))
        {

          resultObject.put("dateTimeFormat", "Gregorian");
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }

 /*
 * Get the History as JSON
 * */
  private void getUserNews(JSONObject resultObject, WebTicket ticket, String themeFolder)
  {
    try
    {

    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }


/*
 * Get the History as JSON
 * */
private void getGroupNews(JSONObject resultObject, WebTicket ticket, String groupId, String themeFolder)
{
  try
  {
    resultObject.put("dateTimeFormat", "Gregorian");
  }
  catch (Exception e)
  {
    e.printStackTrace();
    APIUtils.populateExceptionData(resultObject, e);
  }
}


  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request  servlet request
   * @param response servlet response
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request  servlet request
   * @param response servlet response
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   */
  public String getServletInfo()
  {
    return "Social Networking News API";
  }
}
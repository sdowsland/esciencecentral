/*
 * FileAPI.java
 */

package com.connexience.server.web.messages;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.model.social.requests.FriendRequest;
import com.connexience.server.model.social.requests.JoinGroupRequest;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import com.connexience.server.web.util.WebUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;

/**
 * This servlet provides inbox functions via JSON
 * <p/>
 * It expects URLs of the form:
 * <p/>
 *
 * @author simon
 */
public class MessagesAPI extends HttpServlet
{


  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {
    response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
    response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

    PrintWriter writer = response.getWriter();
    WebTicket ticket = SessionUtils.getTicket(request);

    JSONObject error = new JSONObject();
    JSONObject result = new JSONObject();
    JSONArray resultArray = new JSONArray();
    JSONObject dataObject;

    try
    {
      if (ticket != null)
      {
        String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
        String userId = sections[0];
        String data = APIUtils.extractString(request.getInputStream());
        //        get any post data that might be there (needs to be in JSON format)
        if (data != null && !data.equals(""))
        {
          dataObject = new JSONObject(data);
        }
        else
        {
          dataObject = new JSONObject();
        }


        if (sections.length == 3)
        {
          if(sections[1].equalsIgnoreCase("thread"))
          {
            String threadId = sections[2];
            getThread(ticket, threadId, result);
          }
          else
          {
            int start = Integer.parseInt(sections[1]);
            int pageSize = Integer.parseInt(sections[2]);
            getMessages(ticket, result, start, pageSize);
          }
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(error, e);
    }
    finally
    {
      try
      {
        if (error.length() > 0)
        {
          error.write(writer);
        }
        else if (resultArray.length() > 0)
        {
          resultArray.write(writer);
        }
        else if (result.length() > 0)
        {
          result.write(writer);
        }
      }
      catch (Exception e)
      {
        System.err.println(e.getMessage());
      }
      writer.close();
    }
  }


  private void getMessages(Ticket ticket, JSONObject resultObject, int start, int pageSize)
  {
    try
    {
      DateFormat df = new SimpleDateFormat("MMM d yyyy 'at' h:mm a");

      User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

      Collection<Message> messages = EJBLocator.lookupMessageBean().getMessages(ticket, ticket.getUserId(), user.getInboxFolderId(), start, pageSize);
      JSONArray messagesJSON = new JSONArray();
      for (Message message : messages)
      {
        User sender = EJBLocator.lookupUserDirectoryBean().getUser(ticket, message.getSenderId());
        JSONObject mj = new JSONObject();
        if (message instanceof TextMessage)
        {
          TextMessage tm = (TextMessage) message;
          
          mj.put("type", "textMessage");
          mj.put("threadId", tm.getThreadId());
          mj.put("title", tm.getTitle());
          mj.put("message", tm.getMessage());
          mj.put("senderId", tm.getSenderId());
          mj.put("senderProfileId", sender.getProfileId());
          mj.put("senderName", EJBLocator.lookupUserDirectoryBean().getUser(ticket, tm.getSenderId()).getDisplayName());
          mj.put("date", df.format(tm.getTimestamp()));
          mj.put("isRead", tm.isRead());

          //strip the html so that we don't get multiple lines
          String noHTMLString = tm.getMessage().replaceAll("\\<.*?>", "");
          if (noHTMLString.length() > 160)
          {
            noHTMLString = noHTMLString.substring(0, 159);
          }

          mj.put("summary", noHTMLString);

          messagesJSON.put(mj);
        }
        else if (message instanceof FriendRequest)
        {
          FriendRequest f = (FriendRequest) message;
          mj.put("type", "friendRequest");
          mj.put("senderId", f.getSenderId());
          mj.put("senderName", EJBLocator.lookupUserDirectoryBean().getUser(ticket, f.getSenderId()).getDisplayName());
          mj.put("senderProfileId", sender.getProfileId());
          mj.put("date", df.format(f.getTimestamp()));
          mj.put("title", "Friend Request");
          mj.put("reqId", f.getId());
          messagesJSON.put(mj);
        }
        else if (message instanceof JoinGroupRequest)
        {
          JoinGroupRequest j = (JoinGroupRequest) message;
          Group g = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, j.getGroupId());

          mj.put("type", "joinGroupRequest");
          mj.put("senderId", j.getSenderId());
          mj.put("senderName", EJBLocator.lookupUserDirectoryBean().getUser(ticket, j.getSenderId()).getDisplayName());
          mj.put("senderProfileId", sender.getProfileId());
          mj.put("date", df.format(j.getTimestamp()));
          mj.put("title", "Request to join " + g.getName());
          mj.put("reqId", j.getId());
          mj.put("groupName", g.getName());
          messagesJSON.put(mj);
        }
        //        TODO: Notifications aren't working at present and need fixing.  This code and the display code in
        //        messages.js needs checking
        //        else if (message instanceof NotificationMessage)
        //        {
        //          NotificationMessage n = (NotificationMessage) message;
        //
        //          mj.put("type", "notification");
        //          mj.put("text", n.getMessage());
        //          mj.put("senderId", n.getSenderId());
        //          mj.put("senderName", EJBLocator.lookupUserDirectoryBean().getUser(ticket, n.getSenderId()).getDisplayName());
        //          mj.put("date", df.format(n.getTimestamp()));
        //          mj.put("title", "Notification");
        //          mj.put("notId", n.getId());
        //          messagesJSON.put(mj);
        //        }


      }

      resultObject.put("messages", messagesJSON);
      APIUtils.setSuccess(resultObject);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }

  private void getThread(Ticket ticket, String threadId, JSONObject resultObject)
  {
    try
    {
      DateFormat df = new SimpleDateFormat("MMM d yyyy 'at' h:mm a");

      User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
      int start = 0;
      int pageSize = 100;

      String messageTitle = "";
      Collection<Message> messages = EJBLocator.lookupMessageBean().getMessageThread(ticket, threadId, start, pageSize);
      JSONArray messagesJSON = new JSONArray();
      for (Message message : messages)
      {
        if (message instanceof TextMessage)
        {
          TextMessage tm = (TextMessage) message;
          if (messageTitle.equals(""))
          {
            messageTitle = ((TextMessage) message).getTitle();
          }

          User sender = EJBLocator.lookupUserDirectoryBean().getUser(ticket, message.getSenderId());
          JSONObject mj = new JSONObject();
          mj.put("senderId", message.getSenderId());
          mj.put("text", message.getMessage());
          mj.put("title", ((TextMessage) message).getTitle());
          mj.put("date", df.format(message.getTimestamp()));
          mj.put("senderName", EJBLocator.lookupUserDirectoryBean().getUser(ticket, message.getSenderId()).getDisplayName());
          mj.put("senderProfileId", sender.getProfileId());
          mj.put("isRead", tm.isRead());

          messagesJSON.put(mj);
        }
      }

      EJBLocator.lookupMessageBean().markThreadAsRead(ticket, threadId);
      System.out.println("xxx");
      resultObject.put("messageTitle", messageTitle);
      resultObject.put("messages", messagesJSON);
      APIUtils.setSuccess(resultObject);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }


  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
  {

    response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
    response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

    PrintWriter writer = response.getWriter();
    WebTicket ticket = SessionUtils.getTicket(request);

    JSONObject dataObject;

    JSONObject error = new JSONObject();
    JSONObject result = new JSONObject();
    JSONArray resultArray = new JSONArray();

    try
    {
      if (ticket != null)
      {
        String data = APIUtils.extractString(request.getInputStream());
        String[] sections = APIUtils.splitRequestPath(request.getPathInfo());

        //get any post data that might be there (needs to be in JSON format)
        if (data != null && !data.equals(""))
        {
          dataObject = new JSONObject(data);
        }
        else
        {
          dataObject = new JSONObject();
        }

        if (sections.length == 2)
        {
          if (sections[1].equalsIgnoreCase("accept"))
          {
            acceptConnection(ticket, dataObject, result);
          }
          else if (sections[1].equalsIgnoreCase("reject"))
          {
            rejectConnection(ticket, dataObject, result);
          }
          else if (sections[1].equalsIgnoreCase("delete"))
          {
            deleteMessage(ticket, dataObject, result);
          }
          else if (sections[1].equalsIgnoreCase("send"))
          {
            sendMessage(ticket, dataObject, result);
          }
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(error, e);
    }
    finally
    {
      try
      {
        if (error.length() > 0)
        {
          error.write(writer);
        }
        else if (resultArray.length() > 0)
        {
          resultArray.write(writer);
        }
        else if (result.length() > 0)
        {
          result.write(writer);
        }
      }
      catch (Exception e)
      {
        System.err.println(e.getMessage());
      }
      writer.close();
    }
  }


  private void acceptConnection(Ticket ticket, JSONObject dataObject, JSONObject resultObject)
  {
    try
    {
      String requestId = dataObject.getString("reqId");
      EJBLocator.lookupRequestBean().acceptRequest(ticket, requestId);

      APIUtils.setSuccess(resultObject);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }

  private void rejectConnection(Ticket ticket, JSONObject dataObject, JSONObject resultObject)
  {
    try
    {
      String requestId = dataObject.getString("reqId");
      EJBLocator.lookupRequestBean().rejectRequest(ticket, requestId);

      APIUtils.setSuccess(resultObject);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }


  private void deleteMessage(Ticket ticket, JSONObject dataObject, JSONObject resultObject)
  {
    try
    {
      User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

      String messageId = dataObject.getString("messageId");
      Message m = (Message) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, messageId, Message.class);

      if (m.getContainerId().equals(user.getInboxFolderId()))
      {
        EJBLocator.lookupObjectRemovalBean().remove(m);
      }

      APIUtils.setSuccess(resultObject);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }


  private void sendMessage(Ticket ticket, JSONObject dataObject, JSONObject resultObject)
  {
   try
    {

      String title = dataObject.getString("title");
      String body = dataObject.getString("body");
      String threadId = dataObject.getString("threadId");
      JSONArray recipientIds = dataObject.getJSONArray("recipients");

      User sender = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
      UserProfile senderProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, ticket.getUserId());

      //if the threadId doesn't exist, add the sender to the list of recipients as this is a new message
      if(threadId.equals(""))
      {
        recipientIds.put(ticket.getUserId());
      }

      String recipientIdsString = "";

      for (int i = 0; i < recipientIds.length(); i++)
      {
        String recipientId = recipientIds.getString(i);
        //update the recipientIdsString that will be used to save the sent message
        recipientIdsString += recipientId + " ";
      }

      for (int i = 0; i < recipientIds.length(); i++)
      {
        User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, recipientIds.getString(i));
        if (u != null && !ticket.getUserId().equals(u.getId()))
        {
          TextMessage m = (TextMessage) EJBLocator.lookupMessageBean().createTextMessage(ticket, recipientIds.getString(i), recipientIdsString, threadId, title, body);
          threadId = m.getThreadId();
        }
      }

      TextMessage sentMessage = EJBLocator.lookupMessageBean().addSentTextMessageToFolder(ticket, recipientIdsString, threadId, title, body);

      //get the details to send back
      DateFormat df = new SimpleDateFormat("MMM d 'at' h:mm a");
      String photoURL = "../servlets/image?soid=" + senderProfile.getId() + "&type=" + ImageData.SMALL_PROFILE;


      //construct the response
      JSONObject j = new JSONObject();
      j.put("message", sentMessage.getMessage());
      j.put("threadId", sentMessage.getThreadId());
      j.put("photoURL", photoURL);
      j.put("senderName", sender.getDisplayName());
      j.put("senderLink", WebUtil.constructLink(sender));
      j.put("timestamp", df.format(sentMessage.getTimestamp()));
      resultObject.put("Result", j);

      APIUtils.setSuccess(resultObject);

    }
    catch (Exception e)
    {
      e.printStackTrace();
      APIUtils.populateExceptionData(resultObject, e);
    }
  }



  /**
   * Returns a short description of the servlet.
   */
  public String getServletInfo()
  {
    return "Messages API";
  }
}
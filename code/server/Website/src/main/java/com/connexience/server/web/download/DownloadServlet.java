/*
 * DownloadServlet.java
 */
package com.connexience.server.web.download;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.provenance.model.logging.events.UserReadOperation;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.util.SessionUtils;
import com.connexience.provenance.client.ProvenanceLoggerClient;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

/**
 * This class provides a download link for accessing files from the data store.
 * @author hugo
 */
public class DownloadServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=3600,no-cache,post-check=0,pre-check=0");
        
        // Get the document and version ids from the request
        String documentId = request.getParameter("documentid");
        String versionId = request.getParameter("versionid");
        String forcedMime = request.getParameter("forcemime");
        String limit = request.getParameter("limit");
        
        DocumentRecord document;
        DocumentVersion version;
        WebTicket ticket = null;
        boolean quotaError = false;

        if (documentId != null) {
            try {
                // Get the current ticket
                ticket = SessionUtils.getTicket(request);
                if (ticket != null) {
                    // Get the document and version
                    document = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);

                    if (versionId != null) {
                        // Get a specific version
                        version = EJBLocator.lookupStorageBean().getVersion(ticket, document.getId(), versionId);
                    } else {
                        // Get the latest version
                        version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, document.getId());
                    }

                    /* // Do something else for workflows
                    if (document instanceof WorkflowDocument) {
                        response.sendRedirect("../../pages/workflow/wfeditor.jsp?id=" + document.getId() + "&versionid=" + version.getId());
                    }else if(document instanceof DynamicWorkflowService){
                        DynamicWorkflowService dwfs = (DynamicWorkflowService)document;
                        if(dwfs.getProjectFileId()!=null && !dwfs.getProjectFileId().trim().isEmpty()){
                            response.sendRedirect("../../pages/blockbuilder/ide.jsp?id=" + dwfs.getProjectFileId());
                        }
                    }*/

                    // Check quota
                    if(EJBLocator.lookupQuotaBean().userHasStorageQuota(ticket, ticket.getUserId()) && EJBLocator.lookupQuotaBean().getAvailableStorageQuota(ticket, ticket.getUserId()) < EJBLocator.lookupQuotaBean().getStorageQuotaUsed(ticket, ticket.getUserId())){
                        quotaError = true;
                        throw new Exception("Storage quota exceeded");
                    }
                    
                    // Locate the correct data store
                    DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());

                    // If a store has been identified, start the download stream
                    if (store != null) {
                        // Set the mime type if a document type has been specified, otherwise
                        // set it to be plain binary data
                        if(forcedMime==null){
                            if (document.getDocumentTypeId() != null) {
                                DocumentType type = null;
                                try {
                                    type = EJBLocator.lookupStorageBean().getDocumentType(ticket, document.getDocumentTypeId());
                                } catch (ConnexienceException ce) {
                                    //do nothing - fixes a bug in the server where you might not have privileges to the document type
                                }

                                if (type != null) {
                                    response.setContentType(type.getMimeType());
                                } else {
                                    response.setContentType("application/octet-stream");
                                }
                            } else {
                                int extensionIndex = document.getName().lastIndexOf(".");
                                if (extensionIndex != -1) {
                                    String extension = document.getName().substring(extensionIndex + 1);
                                    if (extension != null) {
                                        DocumentType type = EJBLocator.lookupStorageBean().getDocumentTypeByExtension(ticket, extension);
                                        if (type != null) {
                                            response.setContentType(type.getMimeType());
                                        }
                                    } else {
                                        response.setContentType("application/octet-stream");
                                    }
                                } else {
                                    response.setContentType("application/octet-stream");
                                }
                            }
                        } else {
                            // Force a MIME type
                            response.setContentType(forcedMime);
                        }

                        String userName = EJBLocator.lookupUserDirectoryBean().getUserName(ticket, ticket.getUserId());
                        Long timestamp = System.currentTimeMillis();

                        UserReadOperation read = new UserReadOperation(document.getId(), version.getId(), document.getName(), String.valueOf(version.getVersionNumber()), ticket.getUserId(), new Date(timestamp), userName);
                        ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                        read.setProjectId(ticket.getDefaultProjectId());
                        provClient.log(read);

                        // Start streaming
                        OutputStream stream = response.getOutputStream();
                        if(limit!=null){
                            long byteLimit;
                            try {
                                byteLimit = Long.parseLong(limit);
                            } catch (Exception e){
                                byteLimit = -1;
                            }
                            
                            if(byteLimit!=-1){
                                if(byteLimit>version.getSize()){
                                    response.setHeader("Content-Length", Long.toString(version.getSize()));
                                } else {
                                    response.setHeader("Content-Length", Long.toString(byteLimit));
                                }
                                store.writeToStream(document, version, stream, byteLimit);
                            } else {
                                response.setHeader("Content-Length", Long.toString(version.getSize()));
                                store.writeToStream(document, version, stream);
                            }
                        } else {
                            response.setHeader("Content-Length", Long.toString(version.getSize()));
                            store.writeToStream(document, version, stream);
                            
                        }
                        stream.flush();
                    }

                } else {
                    throw new ConnexienceException("Cannot acquire security ticket");
                }


            } catch (Exception e) {
                //if something has gone wrong, send them to the error page.

                if (!response.isCommitted()) {
                    if(quotaError){
                        response.sendRedirect("../pages/error.jsp?type=quota");
                    } else {
                        response.sendRedirect("../pages/error.jsp");
                    }
                }


            }

        } else {
            throw new ServletException("No document specified");
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Data download servlet";
    }
}

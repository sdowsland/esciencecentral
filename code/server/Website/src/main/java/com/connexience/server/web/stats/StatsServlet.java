/*
 * AdminServlet.java
 */
package com.connexience.server.web.stats;


import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;


/**
 * This servlet deals with Admin requests for managing services and hosts
 *
 * @author hugo
 */
public class StatsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    }

                    if (method.equalsIgnoreCase("workflow")) {
                        // Remove a workflow
                        getWorkflowStats(ticket, resultObject);
                    } else if (method.equalsIgnoreCase("quota")) {
                        // Remove a workflow
                        getQuotaStats(ticket, resultObject);
                    } else if (method.equalsIgnoreCase("fileTypes")) {
                        // Remove a workflow
                        getFileTypes(ticket, resultObject);
                    }

                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        }
        catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        }
        catch (Exception e) {
        }
        out.flush();
    }


    /** List the host machines in the database */
    private void getWorkflowStats(Ticket ticket, JSONObject resultObject) {
        try {
            HashMap<String, Long> stats = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowStats(ticket);

            JSONArray statsJSON = new JSONArray();

            for (String key : stats.keySet()) {
                JSONObject stat = new JSONObject();
                stat.put("label", key);
                stat.put("data", stats.get(key));
                if (key.equalsIgnoreCase("success")) {
                    stat.put("color", "#4DA74D");
                } else if (key.equalsIgnoreCase("Failed")) {
                    stat.put("color", "#A63D3D");
                }
                statsJSON.put(stat);
            }

            if(stats.keySet().size() == 0)
            {
                JSONObject stat = new JSONObject();
                stat.put("label", "No Workflow Runs");
                stat.put("data", 0);
                statsJSON.put(stat);
            }

            resultObject.put("stats", statsJSON);
            APIUtils.setSuccess(resultObject);
        }
        catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** List the host machines in the database */
    private void getQuotaStats(Ticket ticket, JSONObject resultObject) {
        try {
            JSONArray statsJSON = new JSONArray();

            long dataAvailable = EJBLocator.lookupQuotaBean().getAvailableStorageQuota(ticket, ticket.getUserId()) / 1048576;
            if (dataAvailable < 0) dataAvailable = 0;

            long dataUsed = EJBLocator.lookupQuotaBean().getStorageQuotaUsed(ticket, ticket.getUserId()) / 1048576;

            JSONObject free = new JSONObject();
            free.put("label", "Free (MB)");
            free.put("data", dataAvailable);
            statsJSON.put(free);

            JSONObject used = new JSONObject();
            used.put("label", "Used (MB)");
            used.put("data", dataUsed);

            statsJSON.put(used);


            resultObject.put("stats", statsJSON);
            APIUtils.setSuccess(resultObject);
        }
        catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** List the host machines in the database */
    private void getFileTypes(Ticket ticket, JSONObject resultObject) {
       try {
            HashMap<String, Long> fileTypes = EJBLocator.lookupStorageBean().getFileTypesForUser(ticket);

            JSONArray fileTypesJSON = new JSONArray();

            for (String key : fileTypes.keySet()) {
                JSONObject fileType = new JSONObject();
                fileType.put("label", key);
                fileType.put("data", fileTypes.get(key));

                fileTypesJSON.put(fileType);
            }

            resultObject.put("stats", fileTypesJSON);
            APIUtils.setSuccess(resultObject);
        }
        catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}

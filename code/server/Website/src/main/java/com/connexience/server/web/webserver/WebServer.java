

package com.connexience.server.web.webserver;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.web.APIUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
/**
 * This class serves web pages from a users home directory
 * @author hugo
 */
public class WebServer extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String forcedId = request.getParameter("forceid");
        WebTicket ticket = SessionUtils.getTicket(request);

        // Check for public user
        Organisation org = SessionUtils.getOrganisation(request);
        if(org==null){
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } else {
            if(ticket.getUserId().equals(org.getDefaultUserId())){
                // Public User

                Map redirectParamMap = request.getParameterMap();
                String params = "";

                //forward the parameters
                for (Object o : redirectParamMap.keySet()) {
                    String pubRedirKey = o.toString();
                    if (!pubRedirKey.equals("needToLogin") && !pubRedirKey.equals("nextPage") && !pubRedirKey.equals("connect") && !pubRedirKey.equals("remove") && !pubRedirKey.equals("fav") && !pubRedirKey.equals("favId") && !pubRedirKey.equals("action") && !pubRedirKey.equals("logonFail")) {
                        String[] values = (String[]) redirectParamMap.get(pubRedirKey);
                        for (String value : values) {
                            params += "&" + pubRedirKey + "=" + value;
                        }
                    }
                }

                if (params.startsWith("&")) {
                    params = params.substring(1, params.length());
                }
   
                // Redirect to login screen
                if(!params.isEmpty()){
                    request.getSession(true).setAttribute("nextPage", request.getRequestURI());
                } else {
                    request.getSession(true).setAttribute("nextPage", request.getRequestURI() + "?" + params);
                }
                response.sendRedirect(request.getContextPath() + "/pages/front/welcome.jsp");
            } else {
                // Normal user
                String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
                String siteId = sections[0];

                // Construct a URI without the base siteID
                StringBuilder uriBuffer = new StringBuilder("/");
                for(int i=1;i<sections.length;i++){
                    if(i>1){
                        uriBuffer.append("/");
                    }
                    uriBuffer.append(sections[i]);
                }

                OutputStream out = null;
                try {
                    ServerObject resource = null;
                    if(forcedId!=null && !forcedId.trim().isEmpty()){
                        resource = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, forcedId);
                    } else {
                        resource = EJBLocator.lookupStorageBean().getResourceByURI(ticket, siteId, uriBuffer.toString(), Permission.READ_PERMISSION);
                    }

                    if(resource instanceof DocumentRecord){
                        DocumentRecord doc = (DocumentRecord)resource;

                        DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, resource.getId());


                        // Sort out the mime type
                        if(doc.getName().endsWith(".htm") || doc.getName().endsWith(".html")){
                            // Send html as the correct type
                            response.setContentType("text/html");
                        } else {
                            if(doc.getDocumentTypeId()!=null){
                                DocumentType docType = EJBLocator.lookupStorageBean().getDocumentType(ticket, doc.getDocumentTypeId());
                                if(docType!=null){
                                    response.setContentType(docType.getMimeType());
                                } else {
                                    response.setContentType("application/data");
                                }
                            } else {
                                response.setContentType("application/data");
                            }
                        }
                        out = response.getOutputStream();
                        StorageUtils.downloadFileToOutputStream(ticket, doc, version, out);

                    } else {
                        if(!response.isCommitted()){
                            response.sendError(HttpServletResponse.SC_NOT_FOUND);
                        }
                    }
                } catch (Exception e){
                    if(!response.isCommitted()){
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    }

                } finally {
                    if(out!=null){
                        out.flush();
                        out.close();
                    }
                }
            }
        }

    } 

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Web server servlet";
    }
}
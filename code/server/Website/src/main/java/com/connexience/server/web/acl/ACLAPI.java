/*
 * FileAPI.java
 */

package com.connexience.server.web.acl;

import com.connexience.server.ejb.acl.AccessControlRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.*;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

/**
 * This servlet provides access control functions via JSON
 * <p/>
 * It expects URLs of the form:
 * <p/>
 * GET /acl/[id]
 * POST /acl/[id]
 * GET /acl/[id]/public
 *
 * @author hugo
 */
public class ACLAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    protected void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter writer = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);

        JSONObject error = new JSONObject();
        JSONObject result = new JSONObject();
        JSONArray resultArray = new JSONArray();

        try {
            if (ticket != null) {
                String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
                String soid = sections[0];


                if (sections.length == 1) {
                    getACL(resultArray, soid, ticket);
                } else if (sections.length == 2 && sections[1].equals("public")) {
                    getPublicStatus(result, soid, ticket);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(error, e);
        } finally {
            try {
                if (error.length() > 0) {
                    error.write(writer);
                } else if (resultArray.length() > 0) {
                    resultArray.write(writer);
                } else if (result.length() > 0) {
                    result.write(writer);
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
            writer.close();
        }
    }



    private void getPublicStatus(JSONObject resultObject, String soid, WebTicket ticket) {
        try {
            if (soid != null) {
                PermissionList currentPerms = EJBLocator.lookupAccessControlBean().getObjectPermissions(ticket, soid);
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
                String publicUserId = org.getDefaultUserId();
                if (publicUserId != null) {
                    String accessLevel = "private";

                    String[] principalIds = currentPerms.getAllPrincipalIds();
                    for (String principal : principalIds) {
                        if (!principal.equals(publicUserId)  && !principal.equals(ticket.getUserId())) {
                            accessLevel = "custom";
                            break;
                        } else {
                            accessLevel = "public";
                        }
                    }
                    resultObject.put("accessLevel", accessLevel);
                }
            }
        } catch (Exception ce) {
            ce.printStackTrace();
            APIUtils.populateExceptionData(resultObject, ce);
        }

    }

    private void togglePublic(JSONObject resultObject, JSONObject dataObject, WebTicket ticket) {
        try {
            String soid = dataObject.getString("id");
            if (soid != null) {
                ServerObject so = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, soid, ServerObject.class);
                PermissionList currentPerms = EJBLocator.lookupAccessControlBean().getObjectPermissions(ticket, soid);
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
                String publicUserId = org.getDefaultUserId();

                //only the creator can make objects public/private
                if (so.getCreatorId().equals(ticket.getUserId())) {
                    if (publicUserId != null) {
                        boolean publicAccess = currentPerms.permissionExists(Permission.READ_PERMISSION, publicUserId);
                        if (publicAccess) {
                            // Revoke permission
                            EJBLocator.lookupAccessControlBean().revokeAllPermissions(ticket, publicUserId, soid);
                            resultObject.put("public", "false");
                        } else {
                            // Grant permission
                            EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserId, soid, Permission.READ_PERMISSION);
                            resultObject.put("public", "true");
                        }
                    }
                } else {
                    resultObject.put("error", "not_authorised");
                }
            }

        } catch (Exception
                ce) {
            ce.printStackTrace();
            APIUtils.populateExceptionData(resultObject, ce);
        }

    }

    private void getUsers(JSONObject resultObject, String soid, WebTicket ticket) {
        try {
            Collection users = EJBLocator.lookupAccessControlBean().getUsersWithoutPermission(ticket, soid);

            Collection jsonUsers = new Vector();
            for (Object o : users) {
                User up = (User) o;
                JSONObject j = new JSONObject();
                j.put("Name", up.getDisplayName());
                j.put("Id", up.getId());
                jsonUsers.add(j);
            }

            Collection groups = EJBLocator.lookupAccessControlBean().getGroupsWithoutPermission(ticket, soid);
            for (Object o : groups) {
                Group gp = (Group) o;
                JSONObject j = new JSONObject();
                j.put("Name", gp.getName());
                j.put("Id", gp.getId());
                jsonUsers.add(j);
            }


            resultObject.put("Result", jsonUsers);
        } catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(resultObject, e);
        }
    }


    private void saveACL(HttpServletRequest request, WebTicket ticket, JSONObject resultObject, JSONArray dataObject, String soid) {
        Collection<String> newsProfilesAdded = new Vector<>();
        try {
            AccessControlRemote acRemote = EJBLocator.lookupAccessControlBean();

            if (!soid.equals("")) {
                ServerObject so = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, soid, ServerObject.class);

                if (so != null) {
                    //get the current permissions
                    PermissionList origPerms = acRemote.getObjectPermissionsForTicket(ticket, soid);

                    //revoke all the permissions
                    acRemote.revokeAllPermissions(ticket, soid);
                    String defaultUser = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId()).getDefaultUserId();

                    //add the permissions back in
                    for (int i = 0; i < dataObject.length(); i++) {
                        JSONObject perm = (JSONObject) dataObject.get(i);
                        String uid = (String) perm.get("id");
                        String permissionType = (String) perm.get("permission");

                        //sanitise the permission type
                        String sanitisedPermission = "";
                        if (permissionType.equals("read")) {
                            sanitisedPermission = Permission.READ_PERMISSION;
                        } else if (permissionType.equals("write")) {
                            sanitisedPermission = Permission.WRITE_PERMISSION;
                        }

                        //do not allow the default user to be given write access via this API
                        if (sanitisedPermission.equals(Permission.WRITE_PERMISSION) && uid.equals(defaultUser)) {
                            break;
                        }

                        //grant the permission
                        acRemote.grantAccess(ticket, uid, soid, sanitisedPermission);

                    }

                    //don't delete the owner permission
                    if (origPerms.permissionExists(Permission.OWNER_PERMISSION)) {
                        acRemote.grantAccess(ticket, ticket.getUserId(), soid, Permission.OWNER_PERMISSION);
                    }

                    //If we don't allow making things public then don't add the public user
                    if ("false".equals(request.getSession().getServletContext().getInitParameter("public_sharing_enabled"))) {
                        acRemote.revokePermission(ticket, defaultUser, soid, Permission.READ_PERMISSION);
                        acRemote.revokePermission(ticket, defaultUser, soid, Permission.WRITE_PERMISSION);
                    }

                    resultObject.put("result", "ok");
                } else {
                    APIUtils.setError(resultObject, "identifier is invalid");
                }
            } else {
                APIUtils.setError(resultObject, "identifier is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    // get the ACL for a particular object
    private void getACL(JSONArray resultObject, String soid, WebTicket ticket) throws Exception {
        try {
            AccessControlRemote acRemote = EJBLocator.lookupAccessControlBean();

            //check the soid is valid
            if (!soid.equals("")) {
                ServerObject so = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, soid, ServerObject.class);
                if (so != null) {
                    //get the permissions
                    //if the user doesn't have permission to get the permissions, return an error message
                    PermissionList permissions = new PermissionList();
                    if (ticket.getUserId().equals(so.getCreatorId())) {
                        permissions = acRemote.getObjectPermissions(ticket, so.getId());
                    } else {

                        resultObject.put(new JSONObject().put("error", "Not authorised"));
                        return;
                    }


                    //we need them in the format of Name: name, Id: id, Access: read, write, execute.  The are returned as one
                    // permission per record.  So add them to a hashmap of vectors and then pull them out as one.
                    HashMap<String, Vector<String>> hp = new HashMap<>();

                    ArrayList al = permissions.getPermissionList();
                    for (Object anAl : al) {
                        Permission p = (Permission) anAl;

                        //if the userId isn't in the keyset, add it and a new vector
                        if (!hp.containsKey(p.getPrincipalId())) {
                            Vector<String> v = new Vector<>();
                            v.add(p.getType().toLowerCase());
                            hp.put(p.getPrincipalId(), v);
                        } else //otherwise update the vector
                        {
                            hp.get(p.getPrincipalId()).add(p.getType().toLowerCase());
                        }
                    }

                    for (Object o : hp.keySet()) {
                        String userId = o.toString();
                        ServerObject profile = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, userId, ServerObject.class);
                        String name;
                        String type;

                        if (profile instanceof User) {
                            User up = (User) profile;
                            name = up.getDisplayName();
                            type = "person";
                        } else if (profile instanceof Group) {
                            Group gp = (Group) profile;
                            name = gp.getName();
                            type = "group";
                        } else {
                            continue;
                        }

                        Vector perms = hp.get(o);
                        JSONObject j = new JSONObject();
                        j.put("name", name);
                        j.put("id", userId);
                        j.put("access", perms);
                        j.put("type", type);
                        resultObject.put(j);
                    }

                } else {
                    resultObject.put(new Vector());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Could not get ACL for " + soid, e);
        }
    }


    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processGet(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processPost(request, response);
    }

    private void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter writer = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);

        JSONArray dataObject = null;

        JSONObject error = new JSONObject();
        JSONObject result = new JSONObject();
        JSONArray resultArray = new JSONArray();

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
                String soid = sections[0];

                System.out.println(request.getPathInfo());

                if (sections.length == 1) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONArray(data);
                        saveACL(request, ticket, result, dataObject, soid);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            APIUtils.populateExceptionData(error, e);
        } finally {
            try {
                if (error.length() > 0) {
                    error.write(writer);
                } else if (resultArray.length() > 0) {
                    resultArray.write(writer);
                } else if (result.length() > 0) {
                    result.write(writer);
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
            writer.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Social Networking Visibility API";
    }
}
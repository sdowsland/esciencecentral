/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.server.web.filechooser;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.social.OldProject;
import com.connexience.server.util.SessionUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author hugo
 */
public class FileChooserServlet extends HttpServlet {
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Ticket ticket = SessionUtils.getTicket(request);
        PrintWriter writer = response.getWriter();
        try {
            String id = request.getParameter("root");

            List contents;
            Folder folder;
            ServerObject obj;
            DocumentRecord doc;
            JSONObject childJson;
            JSONArray childrenJson;
            boolean homeFolder;

            // Get the correct folder
            if(id.equalsIgnoreCase("source")){
                if(ticket.getDefaultProjectId()!=null){
                    folder = EJBLocator.lookupStorageBean().getFolder(ticket, ticket.getDefaultStorageFolderId());
                }
                else{
                    folder = EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId());
                }
                homeFolder = false;
            } else {
                obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, id, ServerObject.class);
                if(obj instanceof Folder){
                    folder = (Folder)obj;
                    homeFolder = false;
                } else if (obj instanceof OldProject){
                    OldProject proj = (OldProject)obj;
                    folder = EJBLocator.lookupStorageBean().getFolder(ticket, proj.getDataFolder());
                    homeFolder = false;
                } else {
                    throw new Exception("Invalid object for file browser");
                }
                
            }

            String parentFolderId = folder.getId();

            if(folder!=null){
                childrenJson = new JSONArray();

                if(homeFolder){
                    // Just put in top folder
                    childJson = new JSONObject();
                    childJson.put("text", folder.getName());
                    childJson.put("id", folder.getId());
                    childJson.put("hasChildren", true);
                    childJson.put("classes", "folder");
                    childJson.put("isFile", false);
                    childJson.put("className", folder.getClass().getName());
                    childrenJson.put(childJson);

                } else {
                    // Otherwise list contents
                    contents = EJBLocator.lookupStorageBean().getChildFolders(ticket, folder.getId());
                    contents.addAll(EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, folder.getId()));
                    for(int i=0;i<contents.size();i++){
                        obj = (ServerObject)contents.get(i);
                        childJson = new JSONObject();

                        if(obj instanceof Folder){
                            // Child folder
                            folder = (Folder)obj;
                            childJson.put("text", folder.getName());
                            childJson.put("id", folder.getId());
                            childJson.put("hasChildren", true);
                            childJson.put("classes", "folder");
                            childJson.put("isFile", false);
                            childJson.put("className", folder.getClass().getName());
                            childrenJson.put(childJson);

                        } else {
                            // Document
                            doc = (DocumentRecord)obj;
                            childJson.put("text", doc.getName());
                            childJson.put("id", doc.getId());
                            childJson.put("className", doc.getClass().getName());
                            childrenJson.put(childJson);
                            childJson.put("classes", "file");
                            childJson.put("isFile", true);
                        }
                    }
                }

                childrenJson.write(writer);
            } else {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e){
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        writer.flush();
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet supporting the file and folder chooser";
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.monitoring;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import com.connexience.server.model.workflow.control.WorkflowEngineRecord;
import com.connexience.server.util.SignatureUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.util.ZipUtils;
import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author Hugo
 */
public class MonitorServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        File tempFile = File.createTempFile("upload", null);
        try {
            String fileName = req.getHeader("filename");
            String hash = req.getHeader("hash");
            
            
     
            String hashedRootPassword = EJBLocator.lookupTicketBean().getHashedRootPassword();
            InputStream inStream = req.getInputStream();
            FileOutputStream outStream = new FileOutputStream(tempFile);
            ZipUtils.copyInputStream(inStream, outStream);
            outStream.flush();
            outStream.close();
            inStream.close();
                
            if(SignatureUtils.validateFileHash(tempFile, hash, hashedRootPassword)){
                String deployDir = System.getProperty("jboss.server.base.dir") + File.separator + "deployments";
                File deployFile = new File(deployDir, fileName);
                ZipUtils.copyFile(tempFile, deployFile);
            }
            
        } catch (Exception e){
            
        } finally {
            // Cleanup temporary file
            if(tempFile.exists()){
                if(!tempFile.delete()){
                    tempFile.deleteOnExit();
                }            
            }
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        Ticket ticket = new Ticket();
        ticket.setSuperTicket(true);
        JSONObject resultObject = null;
        
        try {
            String method = request.getParameter("method");
            if(method.equals("report")){
                response.setContentType("text/html");
                prepareReport(ticket, out);
                
            } else if(method.equals("status")){
                response.setContentType("text/plain");
                basicCheck(ticket, out);           
                
            } else if(method.equals("alive")){
                response.setContentType("text/plain");
                out.print("1");
                
            } else if(method.equalsIgnoreCase("countengines")){
                response.setContentType("text/plain");
                out.print(countEngines(ticket));
                
            } else {
                resultObject = new JSONObject();
                response.setContentType("application/json");
                if(method.equals("summary")){
                    // Return general status information
                    getStatusInfo(ticket, resultObject);

                } else if(method.equals("reidentify")){
                    reIdentifyEngines(ticket, resultObject);

                } else if(method.equals("engines")){
                    // List the engines
                    listEngines(ticket, resultObject);

                } else if(method.equals("storage")){
                    // Test the storage
                    storageTest(ticket, resultObject);
                    
                }
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
        
        if(resultObject!=null){
            try {
                resultObject.write(out);
            } catch (Exception e) {
            }
        }
        out.flush();        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Monitoring";
    }// </editor-fold>

    private boolean getStatusInfo(Ticket ticket, JSONObject resultObject){
        try {
            long userCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("USER");
            long workflowCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("WORKFLOWDOCUMENTRECORD");
            long invocationCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("WORKFLOWINVOCATION");
            long documentCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("DOCUMENTRECORD");

            resultObject.put("users", userCount);
            resultObject.put("workflows", workflowCount);
            resultObject.put("invocations", invocationCount);
            resultObject.put("documents", documentCount);
            
            APIUtils.setSuccess(resultObject);
            return true;
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
            return false;
        }
    }
    
    private boolean reIdentifyEngines(Ticket ticket, JSONObject resultObject){
        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().reRegisterWorkflowEngines(ticket);
            APIUtils.setSuccess(resultObject);
            return true;
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
            return false;
        }
    }
    
    private boolean listEngines(Ticket ticket, JSONObject resultObject){
        try {
            List engines = WorkflowEJBLocator.lookupWorkflowManagementBean().listDynamicWorkflowEngines(ticket);
            JSONArray enginesJson = new JSONArray();
            JSONObject engineJson;
            
            WorkflowEngineRecord engine;
            for(Object o : engines){
                engine = (WorkflowEngineRecord)o;
                engineJson = new JSONObject();
                engineJson.put("ip", engine.getIPAddress());
                enginesJson.put(engineJson);
            }
            resultObject.put("engines", enginesJson);
            APIUtils.setSuccess(resultObject);
            return true;
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
            return false;
        }
    }
    
    private boolean storageTest(Ticket ticket, JSONObject resultObject) {
        DocumentRecord testDoc = null;
        boolean enabled = false;
        try {
            enabled = EJBLocator.lookupPreferencesBean().isStorageEnabled();
        } catch (Exception e){}
        
        if(enabled){
            try {
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);
                ticket.setOrganisationId(org.getId());
                Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, org.getDataFolderId());
                testDoc = new DocumentRecord();
                testDoc.setName("StatusCheckDocument" + System.currentTimeMillis());
                testDoc.setContainerId(dataFolder.getId());
                testDoc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, testDoc);

                resultObject.put("testDocumentID", testDoc.getId());
                String testDocContents = "Test data for status check";
                DocumentVersion v = StorageUtils.upload(ticket, testDocContents.getBytes(), testDoc, "Status check document");
                resultObject.put("versionId", v.getId());
                resultObject.put("md5", v.getMd5());
                resultObject.put("size", v.getSize());


                APIUtils.setSuccess(resultObject);
                return true;
            } catch(Exception e){
                APIUtils.populateExceptionData(resultObject, e);
                return false;
            } finally {
                if(testDoc!=null){
                    try {
                        EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, testDoc.getId());
                    } catch (Exception e){}
                }

            }
        } else {
            APIUtils.setError(resultObject, "Storage is read-only");
            return false;
        }
    }
    
    private void prepareReport(Ticket ticket, PrintWriter out){
        JSONObject reidentifyResult = new JSONObject();
        JSONObject enginesResult = new JSONObject();
        JSONObject summaryResult = new JSONObject();
        JSONObject storageResult = new JSONObject();
                
        boolean reidentOk = reIdentifyEngines(ticket, reidentifyResult);
        boolean statusOk = getStatusInfo(ticket, summaryResult);
        boolean storageOk = storageTest(ticket, storageResult);
        
        // Wait a bit
        try {
            Thread.sleep(2000);
        } catch (Exception e){}
        boolean listOk = listEngines(ticket, enginesResult);
        
        try {
            out.println("<html><body>");
            
            if(statusOk){
                out.println("<h1>Basic Summary</h1>");
                out.println("<ul>");
                out.println("<li>Users: " + summaryResult.getString("users") + "</li>");
                out.println("<li>Workflows: " + summaryResult.getString("workflows") + "</li>");
                out.println("<li>Documents: " + summaryResult.getString("documents") + "</li>");
                out.println("<li>Invocations: " + summaryResult.getString("invocations") + "</li>");
                out.println("</ul>");
            } else {
                out.println("<h1>Summary test FAILED</h1>");
            }
            
            if(storageOk){
                out.println("<h1>Storage test</h1>");
                out.println("<ul>");
                out.println("<li>Test document ID: " + storageResult.getString("testDocumentID") + "</li>");
                out.println("<li>Test version ID: " + storageResult.getString("versionId") + "</li>");
                out.println("<li>Test document MD5: " + storageResult.getString("md5") + "</li>");
                out.println("<li>Test document Size: " + storageResult.getString("size") + "</li>");
                out.println("</ul>");
                
            } else {
                out.println("<h1>Storage test FAILED</h1>");
                out.println(storageResult.getString("message"));
            }
            
            if(reidentOk){
                out.println("<h1>Engine re-ident message OK</h1>");
            } else {
                out.println("<h1>Engine re-ident message FAILED</h1>");
            }
            
            if(listOk){
                out.println("<h1>Active engine list</h1>");
                JSONArray engines = enginesResult.getJSONArray("engines");
                JSONObject engineJson;
                
                out.println("<ul>");
                for(int i=0;i<engines.length();i++){
                    engineJson = engines.getJSONObject(i);
                    out.println("<li>" + engineJson.getString("ip") + "</li>");
                }
                out.println("</ul>");
            }
            out.println("</body></html>");
        } catch (Exception e){
            out.println("<html><body><h1>ERROR: " + e.getMessage() + "</h1></body></html>");
        }
    }
    
    private int countEngines(Ticket ticket){
        try {
            List engines = WorkflowEJBLocator.lookupWorkflowManagementBean().listDynamicWorkflowEngines(ticket);
            return engines.size();
        } catch(Exception e){
            return 0;
        }
    }
    
    private void basicCheck(Ticket ticket, PrintWriter out){
        boolean statusOk = getStatusInfo(ticket, new JSONObject());
        boolean storageOk = storageTest(ticket, new JSONObject());        
        if(statusOk && storageOk){
            out.print("OK");
        } else {
            out.print("FAIL");
        }
    }
}

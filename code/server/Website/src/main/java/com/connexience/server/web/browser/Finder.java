/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.browser;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.LinksFolder;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.util.FolderHolder;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.util.Zipper;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author hugo
 */
public class Finder extends HttpServlet {

    DateFormat format = DateFormat.getDateTimeInstance();

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Finder</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Finder at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");
        Ticket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        Hashtable<String, DocumentType> typeMap = null;
        Zipper zipper = null;
        String rootFolderId = request.getParameter("rootid");
        response.setContentType("text/html");
        ServerObject obj = null;
        Folder folder = null;
        try {
            // Check the type map
            if (session.getAttribute("TYPE_MAP") == null) {
                typeMap = createTypeMap(ticket);
                session.setAttribute("TYPE_MAP", typeMap);

            } else {
                typeMap = (Hashtable<String, DocumentType>) session.getAttribute("TYPE_MAP");
            }

            if (cmd != null) {
                JSONObject result = new JSONObject();
                if (cmd.equals("open")) {
                    String initParam = request.getParameter("init");
                    String treeParam = request.getParameter("tree");
                    String targetParam = request.getParameter("target");


                    if (initParam != null && (initParam.equals("true") || initParam.equals("1"))) {
                        // Re-create type map
                        typeMap = createTypeMap(ticket);
                        session.setAttribute("TYPE_MAP", typeMap);

                        // Initialisation
                        JSONObject home = null;
                        if (rootFolderId != null) {
                            home = createHomeFolderJson(ticket, rootFolderId);
                        } else {
                            // Create for current users home
                            home = createHomeFolderJson(ticket);
                        }

                        result.put("cwd", home);

                        Folder hf = null;
                        if (rootFolderId == null) {
                            hf = EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId());
                            JSONArray cdc = createFolderContentsArray(hf.getId(), ticket, typeMap);
                            result.put("cdc", cdc);
                        } else {
                            obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, rootFolderId, ServerObject.class);
                            if (obj instanceof Folder) {
                                // Get folder content
                                hf = (Folder) obj;
                                JSONArray cdc = createFolderContentsArray(hf.getId(), ticket, typeMap);
                                result.put("cdc", cdc);
                            } else if (obj instanceof Group) {
                                // Get shared files from a group
                                Group g = (Group) obj;
                                JSONArray cdc = createGroupFolderContentsArray(g.getId(), ticket, typeMap);
                                result.put("cdc", cdc);
                            } else if(obj instanceof User){
                                // Get shared files from a user
                                JSONArray cdc = createUserSharedFolderContentsArray(obj.getId(), ticket, typeMap);
                                result.put("cdc", cdc);
                                folder = new Folder();
                                folder.setName("Shared from " + ((User)obj).getDisplayName());
                                folder.setId(obj.getId());
                                folder.setCreationTime(obj.getCreationDate());                                
                            }
                        }

                        result.put("disabled", new JSONArray());

                        JSONObject params = new JSONObject();
                        JSONArray extract = new JSONArray();
                        JSONArray archives = new JSONArray();
                        archives.put("application/zip");
                        extract.put("application/zip");
                        params.put("extract", extract);

                        params.put("archives", archives);
                        result.put("params", params);

                    } else {
                        // General folder
                        
                        obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, targetParam, ServerObject.class);
                        if (obj instanceof Folder) {
                            folder = (Folder) obj;
                            JSONObject cwd = createFolderJson(folder);
                            result.put("cwd", cwd);
                            JSONArray cdc = createFolderContentsArray(folder.getId(), ticket, typeMap);
                            result.put("cdc", cdc);

                        } else if (obj instanceof Group) {
                            Group g = (Group) obj;
                            folder = new Folder();
                            folder.setName(g.getName());
                            folder.setId(g.getId());

                            JSONObject cwd = createFolderJson(folder);
                            result.put("cwd", cwd);
                            JSONArray cdc = createGroupFolderContentsArray(folder.getId(), ticket, typeMap);
                            result.put("cdc", cdc);
                        } else if(obj instanceof User){
                            folder = new Folder();
                            folder.setName("Shared from " + ((User)obj).getDisplayName());
                            folder.setId(obj.getId());
                            folder.setCreationTime(obj.getCreationDate());
                            JSONObject cwd = createFolderJson(folder);
                            result.put("cwd", cwd);
                            JSONArray cdc = createUserSharedFolderContentsArray(obj.getId(), ticket, typeMap);
                            result.put("cdc", cdc);
                        }
                    }

                    // Send the tree if asked
                    if (treeParam != null && (treeParam.equals("true") || treeParam.equals("1"))) {
                        if(obj instanceof Folder || obj instanceof Group || obj==null){
                            JSONObject tree = createFolderTree(ticket, rootFolderId);
                            result.put("tree", tree);
                        } else if(obj instanceof User){
                            JSONObject tree = new JSONObject();
                            tree.put("name", folder.getName());
                            tree.put("hash", folder.getId());
                            tree.put("read", true);
                            tree.put("write", true);

                            JSONArray dirs = new JSONArray();

                            tree.put("dirs", dirs);
                            result.put("tree", tree);
                        }
                    }
                    result.write(response.getWriter());

                } else if (cmd.equals("mkdir")) {
                    String id = request.getParameter("current");
                    String name = request.getParameter("name");
                    Folder newFolder;

                    //create a links only folder for groups
                    Folder containingFolder = EJBLocator.lookupStorageBean().getFolder(ticket, id);
                    if (containingFolder instanceof LinksFolder) {
                        LinksFolder lf = (LinksFolder) containingFolder;
                        newFolder = new LinksFolder(lf.getGroupId());
                        newFolder.setName(name);
                        newFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, id, newFolder);
                    } else {
                        newFolder = new Folder();
                        newFolder.setName(name);
                        newFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, id, newFolder);
                    }


                    result.put("cwd", createFolderJson(newFolder));
                    result.put("cdc", new JSONArray());
                    result.put("tree", createFolderTree(ticket, rootFolderId));
                    JSONArray selectArray = new JSONArray();
                    selectArray.put(newFolder.getId());
                    result.put("select", selectArray);
                    result.write(response.getWriter());
                    
                } else if(cmd.equals("mkfile")){
                    // Create a new file
                    String id = request.getParameter("current");
                    String name = request.getParameter("name");
                    Folder container = EJBLocator.lookupStorageBean().getFolder(ticket, id);
                    if(container!=null){
                        DocumentRecord doc = new DocumentRecord();
                        doc.setName(name);
                        doc.setDescription("Created from finder");
                        doc.setContainerId(id);
                        DocumentType t = EJBLocator.lookupStorageBean().getDocumentTypeByMime(ticket, "text/plain");
                        if(t!=null){
                            doc.setDocumentTypeId(t.getId());
                        }
                        
                        doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
                        DocumentVersion v = StorageUtils.upload(ticket, new byte[0], doc, "New document created in finder");
                        JSONArray cdc = createFolderContentsArray(container.getId(), ticket, typeMap);
                        result.put("cwd", createFolderJson(container));
                        result.put("cdc", cdc);                        
                        JSONArray selectArray = new JSONArray();
                        selectArray.put(doc.getId());
                        result.put("select", selectArray);
                        result.write(response.getWriter());
                        
                    } else {
                        throw new Exception("No container folder found for new file");
                    }
                    
                } else if (cmd.equals("rm")) {
                    // Delete file or folder
                    String id = request.getParameter("current");
                    String[] targets = request.getParameterValues("targets[]");

                    for (int i = 0; i < targets.length; i++) {
                        try {
                            obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, targets[i], ServerObject.class);
                            if (obj instanceof DocumentRecord) {
                                EJBLocator.lookupObjectRemovalBean().remove((DocumentRecord) obj);
                            } else if (obj instanceof Folder) {
                                User u = SessionUtils.getUser(request);
                                if (u != null) {
                                    if (!((Folder) obj).getId().equals(u.getWorkflowFolderId())) {
                                        EJBLocator.lookupObjectRemovalBean().remove(ticket, (Folder) obj);
                                    } else {
                                        throw new Exception("Cannot delete Workflows folder");
                                    }
                                } else {
                                    EJBLocator.lookupObjectRemovalBean().remove(ticket, (Folder) obj);
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                    result.put("tree", createFolderTree(ticket, rootFolderId));
                    folder = EJBLocator.lookupStorageBean().getFolder(ticket, id);
                    result.put("cwd", createFolderJson(folder));
                    result.put("cdc", createFolderContentsArray(id, ticket, typeMap));
                    result.write(response.getWriter());

                } else if (cmd.equals("paste")) {
                    // Move or copy data
                    String sourceFolderId = request.getParameter("src");
                    String targetFolderId = request.getParameter("dst");
                    String[] targets = request.getParameterValues("targets[]");
                    String cut = request.getParameter("cut");
                    ServerObject targetObject;

                    if (cut.equals("1")) {
                        // Move
                        for (int i = 0; i < targets.length; i++) {
                            targetObject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, targets[i], ServerObject.class);
                            if (targetObject instanceof Folder) {
                                // Move a folder
                                ((Folder) targetObject).setContainerId(targetFolderId);
                                EJBLocator.lookupStorageBean().updateFolder(ticket, (Folder) targetObject);

                            } else if (targetObject instanceof DocumentRecord) {
                                // Move a document
                                ((DocumentRecord) targetObject).setContainerId(targetFolderId);
                                EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, (DocumentRecord) targetObject);
                            }
                        }
                        result.put("tree", createFolderTree(ticket, rootFolderId));
                        folder = EJBLocator.lookupStorageBean().getFolder(ticket, sourceFolderId);
                        result.put("cwd", createFolderJson(folder));
                        result.put("cdc", createFolderContentsArray(sourceFolderId, ticket, typeMap));
                        result.write(response.getWriter());

                    } else {
                        // Copy-Paste
                        ArrayList<DocumentRecord> documents = new ArrayList<>();
                        DocumentRecord doc;
                        DocumentRecord newDoc;
                        DocumentVersion version;

                        for (int i = 0; i < targets.length; i++) {
                            // Only copy documents
                            try {
                                doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, targets[i]);

                                if (doc != null) {
                                    version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, doc.getId());
                                    newDoc = new DocumentRecord();
                                    newDoc.setDescription(doc.getDescription());
                                    newDoc.setDocumentTypeId(doc.getDocumentTypeId());
                                    newDoc.setLimitVersions(doc.isLimitVersions());
                                    newDoc.setVersioned(doc.isVersioned());
                                    newDoc.setMaxVersions(doc.getMaxVersions());
                                    newDoc.setName(doc.getName());
                                    newDoc.setObjectType(doc.getObjectType());
                                    newDoc.setContainerId(targetFolderId);
                                    newDoc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, newDoc);
                                    StorageUtils.copyDocumentData(ticket, doc, version, newDoc);
                                }
                            } catch (Exception e) {
                            }
                        }
                        result.put("tree", createFolderTree(ticket, rootFolderId));
                        folder = EJBLocator.lookupStorageBean().getFolder(ticket, targetFolderId);
                        result.put("cwd", createFolderJson(folder));
                        result.put("cdc", createFolderContentsArray(targetFolderId, ticket, typeMap));
                        result.write(response.getWriter());
                    }


                } else if (cmd.equals("rename")) {
                    String current = request.getParameter("current");
                    String target = request.getParameter("target");
                    String name = request.getParameter("name");
                    folder = EJBLocator.lookupStorageBean().getFolder(ticket, current);
                    obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, target, ServerObject.class);
                    JSONArray selectArray = new JSONArray();
                    if (obj instanceof Folder) {
                        obj.setName(name);
                        Folder f = EJBLocator.lookupStorageBean().updateFolder(ticket, (Folder) obj);
                        result.put("tree", createFolderTree(ticket, rootFolderId));
                        result.put("cwd", createFolderJson(folder));
                        result.put("cdc", createFolderContentsArray(folder.getId(), ticket, typeMap));
                        selectArray.put(f.getId());
                        result.put("select", selectArray);

                    } else if (obj instanceof DocumentRecord) {
                        obj.setName(name);
                        DocumentRecord d = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, (DocumentRecord) obj);

                        result.put("cwd", createFolderJson(folder));
                        result.put("cdc", createFolderContentsArray(folder.getId(), ticket, typeMap));
                        selectArray.put(d.getId());
                        result.put("select", selectArray);
                    }
                    result.write(response.getWriter());

                } else if (cmd.equals("duplicate")) {
                    // Make a duplicate of a file
                    String folderId = request.getParameter("current");
                    String documentId = request.getParameter("target");
                    folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                    ServerObject targetObj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, documentId, ServerObject.class);
                    if (targetObj instanceof DocumentRecord) {
                        DocumentRecord doc = (DocumentRecord) targetObj;
                        DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, documentId);
                        if (version != null) {

                            DocumentRecord duplicate = new DocumentRecord();
                            duplicate.setContainerId(doc.getContainerId());
                            duplicate.setCreationTime(new Date());
                            duplicate.setCreatorId(doc.getCreatorId());
                            duplicate.setDescription(doc.getDescription());
                            duplicate.setDocumentTypeId(doc.getDocumentTypeId());
                            duplicate.setLimitVersions(doc.isLimitVersions());
                            duplicate.setMaxVersions(doc.getMaxVersions());
                            duplicate.setObjectType(doc.getObjectType());
                            duplicate.setOrganisationId(doc.getOrganisationId());
                            duplicate.setVersioned(doc.isVersioned());

                            boolean found = false;
                            ServerObject temp;
                            int index = 1;
                            while (found == false) {
                                temp = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, folderId, "Copy " + index + " of " + doc.getName());
                                if (temp == null) {
                                    found = true;
                                } else {
                                    index++;
                                }
                            }
                            duplicate.setName("Copy " + index + " of " + doc.getName());
                            duplicate = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, duplicate);
                            StorageUtils.copyDocumentData(ticket, doc, version, duplicate);
                            result.put("cwd", createFolderJson(folder));
                            result.put("cdc", createFolderContentsArray(folder.getId(), ticket, typeMap));
                            JSONArray selectArray = new JSONArray();
                            selectArray.put(duplicate.getId());
                            result.put("select", selectArray);
                        }

                    } else if(targetObj instanceof Folder) {

                      if (!targetObj.getId().equals(EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getHomeFolderId()))
                      {
                        Folder sourceFolder = (Folder) targetObj;

                        //Generate a new filename - Copy X of Y
                        boolean found = false;
                        ServerObject temp;
                        int index = 1;
                        while (!found)
                        {
                          temp = EJBLocator.lookupStorageBean().getNamedFolder(ticket, sourceFolder.getContainerId(), "Copy " + index + " of " + sourceFolder.getName());
                          if (temp == null)
                          {
                            found = true;
                          }
                          else
                          {
                            index++;
                          }
                        }

                        //Copy the folder and it's contents
                        String targetFolderName = "Copy " + index + " of " + sourceFolder.getName();
                        Folder targetFolder = StorageUtils.copyFolderContents(ticket, sourceFolder, folder, targetFolderName);

                        //Set the response
                        result.put("cwd", createFolderJson(folder));
                        result.put("cdc", createFolderContentsArray(folder.getId(), ticket, typeMap));
                        JSONArray selectArray = new JSONArray();
                        selectArray.put(targetFolder.getId());
                        result.put("select", selectArray);
                      }
                      else
                      {
                        result.put("error", "Cannot duplicate Home Folder");
                      }
                    }
                  result.write(response.getWriter());

                } else if (cmd.equals("read")) {
                    // Load the contents of a text file
                    String documentId = request.getParameter("target");
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    if (doc != null) {
                        DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, documentId);
                        if (version.getSize() < 256000) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            StorageUtils.downloadFileToOutputStream(ticket, doc, version, stream);
                            stream.flush();
                            stream.close();
                            result.put("content", new String(stream.toByteArray()));
                            result.write(response.getWriter());

                        } else {
                            result.put("error", "File too big to view");
                            result.put("content", "");
                            result.write(response.getWriter());
                        }
                    }

                } else if (cmd.equals("archive")) {
                    // Create a zip file
                    String folderId = request.getParameter("current");
                    String[] targets = request.getParameterValues("targets[]");
                    FolderHolder tree = FolderHolder.createFullFolderTree(ticket);
                    FolderHolder holder;
                    folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                    DocumentRecord zipDoc = new DocumentRecord();
                    zipDoc.setName("text.zip");
                    zipDoc.setContainerId(folderId);
                    zipDoc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, zipDoc);
                    zipper = new Zipper(ticket, zipDoc, "Test ZIP File");
                    zipper.setupStreams();
                    if (zipper.hasError()) {
                        throw new Exception("Zipper Error");
                    }

                    for (int i = 0; i < targets.length; i++) {
                        obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, targets[i], ServerObject.class);

                        DocumentRecord doc;
                        Folder f;

                        if (obj instanceof DocumentRecord) {
                            doc = (DocumentRecord) obj;
                            zipper.appendDocumentRecord(doc);
                        } else {
                            f = (Folder) obj;
                            holder = tree.findHolder(f.getId());
                            holder.appendToZipper(ticket, zipper, holder);
                        }
                    }
                    zipper.closeStreams();
                    result.put("cwd", createFolderJson(folder));
                    result.put("cdc", createFolderContentsArray(folderId, ticket, typeMap));
                    JSONArray selectArray = new JSONArray();
                    selectArray.put(zipDoc.getId());
                    result.put("select", selectArray);
                    result.write(response.getWriter());
                }


            } else {
                processRequest(request, response);
            }
        } catch (Exception e) {
            try {
              e.printStackTrace();
                JSONObject result = new JSONObject();
                result.put("error", "Server Error: " + e.getMessage());
                result.put("errorData", e.toString());
                result.write(response.getWriter());
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }

            if (zipper != null) {
                try {
                    zipper.closeStreams();
                } catch (Exception ze) {
                }
            }
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Ticket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        Hashtable<String, DocumentType> typeMap = null;
        String rootFolderId = request.getParameter("rootid");
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(Long.MAX_VALUE);
        String targetFolderId = null;
        String cmd = null;
        String target = null;
        String content = null;
        List items = null;
        FileItem item;
        response.setContentType("text/html");

        ArrayList<FileItem> files = new ArrayList<>();

        try {
            // Check the type map
            if (session.getAttribute("TYPE_MAP") == null) {
                typeMap = createTypeMap(ticket);
                session.setAttribute("TYPE_MAP", typeMap);

            } else {
                typeMap = (Hashtable<String, DocumentType>) session.getAttribute("TYPE_MAP");
            }

            if (ServletFileUpload.isMultipartContent(request)) {
                // Multipart data post
                items = upload.parseRequest(request);
                for (int i = 0; i < items.size(); i++) {
                    item = (FileItem) items.get(i);
                    if (item.isFormField()) {
                        if (item.getFieldName().equals("current")) {
                            targetFolderId = item.getString();
                        } else if (item.getFieldName().equals("cmd")) {
                            cmd = item.getString();
                        } else if (item.getFieldName().equals("target")) {
                            target = item.getString();
                        } else if (item.getFieldName().equals("content")) {
                            content = item.getString();
                        }
                    } else {
                        files.add(item);
                    }
                }
            } else {
                // Simple form post
                cmd = request.getParameter("cmd");
                target = request.getParameter("target");
                targetFolderId = request.getParameter("current");
                content = request.getParameter("content");
            }

            JSONObject result = new JSONObject();

            if (cmd.equals("upload")) {
                if (targetFolderId != null) {
                    Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, targetFolderId);
                    DocumentRecord document;
                    DocumentVersion version;
                    JSONArray selectArray = new JSONArray();
                    boolean error = false;
                    JSONArray errorArray = new JSONArray();

                    if (folder != null) {
                        InputStream stream;
                        for (int i = 0; i < files.size(); i++) {
                            item = files.get(i);

                            // Check quota
                            if (EJBLocator.lookupQuotaBean().userHasStorageQuota(ticket, ticket.getUserId())==false || EJBLocator.lookupQuotaBean().getAvailableStorageQuota(ticket, ticket.getUserId()) >= item.getSize()) {
                                String name = item.getName();
                                long size = item.getSize();
                                if (name != null && !name.isEmpty() && size > 0) {
                                    // Save file
                                    try {

                                        //Create the document record, document version and file on disk in one Tx
                                        UserTransaction tx = null;
                                        try {
                                           tx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                                            tx.begin();

                                            document = StorageUtils.getOrCreateDocumentRecord(ticket, folder.getId(), name);
                                            version = StorageUtils.upload(ticket, item.getInputStream(), item.getSize(), document, "Uploaded from Web");

                                            tx.commit();
                                            }
                                            catch (Exception e) {
                                                tx.rollback();
                                                throw e;
                                            }
                                           selectArray.put(document.getId());

                                        // Run any triggers
                                        WorkflowEJBLocator.lookupWorkflowManagementBean().runTriggersForDocument(ticket, document);                                        
                                        
                                    } catch (Exception upe) {
                                        upe.printStackTrace();
                                        error = true;
                                        errorArray.put(item.getName());
                                    }
                                }
                            } else {
                                throw new Exception("Insufficient storage quota to upload file");
                            }
                        }
                    }


                    JSONObject cwd = createFolderJson(folder);
                    result.put("cwd", cwd);
                    JSONArray cdc = createFolderContentsArray(folder.getId(), ticket, typeMap);
                    result.put("cdc", cdc);
                    result.put("tree", createFolderTree(ticket, rootFolderId));
                    result.put("select", selectArray);
                    if (error) {
                        result.put("error", "Some files were not uploaded");
                        result.put("errorData", "Some files had errors");
                    }
                    result.write(response.getWriter());
                }

            } else if (cmd.equals("edit")) {
                // Save contents of a text file
                if (target != null && content != null) {
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, target);
                    if (doc != null) {
                        StorageUtils.upload(ticket, content.getBytes(), doc, "Edited from data viewer");
                        result.put("file", createDocumentRecordJson(doc, typeMap));
                        
                    } else {
                        result.put("error", "Document does not exist on server");
                    }
                } else {
                    result.put("error", "No data supplied");
                }
                result.write(response.getWriter());
            }

            EJBLocator.lookupStorageBean();
        } catch (Exception e) {
            try {
                JSONObject result = new JSONObject();
                result.put("error", "Server Error: " + e.getMessage());
                result.put("errorData", e.toString());
                result.write(response.getWriter());
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }

        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /** Create a tree for a users folders */
    private JSONObject createFolderTree(Ticket ticket, String rootFolderId) throws Exception {
        if (rootFolderId == null) {
            FolderHolder top = FolderHolder.createFullFolderTree(ticket);
            return top.createJson();
        } else {
            ServerObject root = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, rootFolderId, ServerObject.class);
            if (root instanceof Group) {
                Group group = (Group) root;
                FolderHolder top = FolderHolder.createGroupFolderTree(ticket, group.getId());
                return top.createJson();
            } else {
                FolderHolder top = FolderHolder.createFullFolderTree(ticket);
                FolderHolder subtree = top.findHolder(rootFolderId);
                if (subtree != null) {
                    return subtree.createJson();
                } else {
                    throw new Exception("Could not open folder");
                }
            }

        }
    }

    /** Create a JSON object for a folder */
    private JSONObject createFolderJson(Folder folder) throws Exception {
        JSONObject json = new JSONObject();

        if (folder.getName() != null) {
            json.put("name", folder.getName());
        } else {
            json.put("name", "Unnamed");
        }
        json.put("hash", folder.getId());
        json.put("rel", "Home");
        json.put("date", format.format(folder.getCreationDate()));
        json.put("mime", "directory");
        json.put("size", 0);
        json.put("read", true);
        json.put("write", true);
        json.put("rm", true);
        return json;
    }

    /** Create a JSON object for a file */
    private JSONObject createDocumentRecordJson(DocumentRecord doc, Hashtable<String, DocumentType> typeMap) throws Exception {
        JSONObject json = new JSONObject();
        json.put("name", doc.getName());
        json.put("hash", doc.getId());
        json.put("url", "../../servlets/download/" + doc.getName() + "?documentid=" + doc.getId() + "&forcemime=text/plain");
        json.put("date", format.format(doc.getCreationDate()));

        // Set mime type if there is a recognised type
        if (doc.getDocumentTypeId() != null) {
            if (typeMap.containsKey(doc.getDocumentTypeId())) {
                json.put("mime", typeMap.get(doc.getDocumentTypeId()).getMimeType());
            } else {
                json.put("mime", "application/data");
            }

        } else {
            json.put("mime", "application/data");
        }

        json.put("size", doc.getCurrentVersionSize());
        json.put("read", true);
        json.put("write", true);
        json.put("rm", true);
        json.put("objecttype", doc.getObjectType());
        return json;
    }

    /** Create the type mapping */
    private Hashtable<String, DocumentType> createTypeMap(Ticket ticket) throws Exception {
        List types = EJBLocator.lookupStorageBean().listDocumentTypes(ticket);
        Hashtable<String, DocumentType> typeMap = new Hashtable<>();
        DocumentType docType;
        for (int i = 0; i < types.size(); i++) {
            docType = (DocumentType) types.get(i);
            typeMap.put(docType.getId(), docType);
        }
        return typeMap;
    }

    /** Create a list of folder contents */
    private JSONArray createFolderContentsArray(String folderId, Ticket ticket, Hashtable<String, DocumentType> typeMap) throws Exception {
        JSONArray contents = new JSONArray();
        List objects = EJBLocator.lookupStorageBean().getAllFolderContents(ticket, folderId);
        ServerObject obj;

        // Folders first
        for (int i = 0; i < objects.size(); i++) {
            obj = (ServerObject) objects.get(i);
            if (obj instanceof Folder) {
                contents.put(createFolderJson((Folder) obj));
            }
        }

        // Then documents
        for (int i = 0; i < objects.size(); i++) {
            obj = (ServerObject) objects.get(i);
            if (obj instanceof DocumentRecord) {
                contents.put(createDocumentRecordJson((DocumentRecord) obj, typeMap));
            }
        }



        /*
        // Child folders
        List folders = EJBLocator.lookupStorageBean().getChildFolders(ticket, folderId);
        JSONObject folderJson;
        for(int i=0;i<folders.size();i++){
        folderJson = createFolderJson((Folder)folders.get(i));
        contents.put(folderJson);
        }
        
        // Documents
        
        List documents = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, folderId);
        JSONObject docJson;
        for(int i=0;i<documents.size();i++){
        docJson = createDocumentRecordJson((DocumentRecord)documents.get(i), typeMap);
        contents.put(docJson);
        }
         */

        return contents;
    }
    
    /** Create a list of files from a user */
    private JSONArray createUserSharedFolderContentsArray(String userId, Ticket ticket, Hashtable<String, DocumentType> typeMap) throws Exception {
        List documents = EJBLocator.lookupObjectDirectoryBean().getExplicitlySharedObjectsUserHasAccessTo(ticket, userId, DocumentRecord.class, 0, 0);
        JSONArray contents = new JSONArray();
        for (int i = 0; i < documents.size(); i++) {
            contents.put(createDocumentRecordJson((DocumentRecord) documents.get(i), typeMap));
        }
        return contents;
    }

    /** Create a list of folder contents */
    private JSONArray createGroupFolderContentsArray(String groupId, Ticket ticket, Hashtable<String, DocumentType> typeMap) throws Exception {
        JSONArray contents = new JSONArray();
        ServerObject obj;

        Group g = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, groupId);
        Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, g.getDataFolder());
        Folder eventsFolder = EJBLocator.lookupStorageBean().getFolder(ticket, g.getEventsFolder());

        contents.put(createFolderJson(dataFolder));
        contents.put(createFolderJson(eventsFolder));

        return contents;
    }

    /** Create a JSON object for the home folder */
    private JSONObject createHomeFolderJson(Ticket ticket) throws Exception {
        Folder folder = EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId());
        JSONObject json = new JSONObject();
        json.put("name", "Home");
        json.put("hash", folder.getId());
        json.put("rel", "Home");
        json.put("date", format.format(folder.getCreationDate()));
        json.put("mime", "directory");
        json.put("size", 0);
        json.put("read", true);
        json.put("write", true);
        json.put("rm", false);
        return json;
    }

    /** Create a JSON object for an arbitrary home folder */
    private JSONObject createHomeFolderJson(Ticket ticket, String rootFolderId) throws Exception {

        Folder folder;
        ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, rootFolderId, ServerObject.class);
        if (obj instanceof Folder) {
            folder = (Folder) obj;
        } else if (obj instanceof Group) {
            Group group = (Group) obj;
            folder = new Folder();
            folder.setName(group.getName());
            folder.setId(group.getId());
            folder.setCreationTime(group.getCreationDate());
        } else if(obj instanceof User){
            folder = new Folder();
            folder.setName("Shared from " + ((User)obj).getDisplayName());
            folder.setId(obj.getId());
            folder.setCreationTime(obj.getCreationDate());
        } else {
            throw new ConnexienceException("Cannot find folder or group with id: " + rootFolderId);
        }

        JSONObject json = new JSONObject();
        json.put("name", folder.getName());
        json.put("hash", folder.getId());
        json.put("rel", "Home");
        json.put("date", format.format(folder.getCreationDate()));
        json.put("mime", "directory");
        json.put("size", 0);
        json.put("read", true);
        json.put("write", true);
        json.put("rm", false);
        return json;
    }
}

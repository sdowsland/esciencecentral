/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.blockbuilder;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.provenance.model.logging.events.LibrarySaveOperation;
import com.connexience.provenance.model.logging.events.ServiceSaveOperation;
import com.connexience.server.model.properties.PropertyGroup;
import com.connexience.server.model.properties.PropertyItem;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.util.FolderHolder;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.util.Zipper;
import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.server.model.project.Project;
import com.connexience.server.util.ZipUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.util.*;

/**
 *
 * @author hugo
 */
public class BlockBuilderServlet extends HttpServlet {
    private static DateFormat fullFormat = DateFormat.getDateTimeInstance();
    private static DateFormat dayFormat = DateFormat.getDateInstance(DateFormat.SHORT);
    private static DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();
        try {
            WebTicket ticket = SessionUtils.getTicket(request);
            
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            String method = request.getParameter("method");
            if (method.equals("getFolder")) {
                // Get a folder
                String id = request.getParameter("id");
                Folder folder;

                if (id == null || id.equals("home")) {
                    // Get the home folder
                    if(ticket.getDefaultProjectId()==null || ticket.getDefaultProjectId().isEmpty()){
                        // Actual home folder
                        JSONObject folderJson = createFolderJson(EJBLocator.lookupStorageBean().getFolder(ticket, user.getHomeFolderId()));
                        folderJson.write(writer);                        
                        
                    } else {
                        // Project home folder
                        Project p = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(ticket.getDefaultProjectId()));
                        JSONObject folderJson = createFolderJson(EJBLocator.lookupStorageBean().getFolder(ticket, p.getDataFolderId()));
                        folderJson.write(writer);                           
                    }

                } else {
                    // Get a specific folder
                    Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, id);
                    String childName = request.getParameter("folder");
                    String type = request.getParameter("type");
                    JSONObject folderJson = null;
                    if (type != null) {
                        if (type.equals("child")) {
                            // Get a named child
                            if (childName != null) {
                                List children = EJBLocator.lookupStorageBean().getChildFolders(ticket, f.getId());//api.getFolderContents(f);

                                for (int i = 0; i < children.size(); i++) {
                                    if (children.get(i) instanceof Folder) {
                                        folder = (Folder) children.get(i);
                                        if (folder.getName().equals(childName) && !folder.getId().equals(f.getId())) {
                                            folderJson = createFolderJson(folder);
                                        }
                                    }
                                }
                                if (folderJson != null) {
                                    folderJson.write(writer);
                                } else {
                                    throw new Exception("No such folder: " + childName);
                                }
                            } else {
                                folderJson = createFolderJson(EJBLocator.lookupStorageBean().getFolder(ticket, id));
                                folderJson.write(writer);
                            }

                        } else if (type.equals("parent")) {
                            // Get the parent of this folder
                            String parentId = f.getContainerId();
                            if (!f.getId().equals(user.getHomeFolderId())) {
                                // Change up if we are not in the home directory
                                folder = EJBLocator.lookupStorageBean().getFolder(ticket, parentId);
                                folderJson = createFolderJson(folder);
                                folderJson.write(writer);
                            } else {
                                // Cannot change up from home
                                throw new Exception("Cannot change up from home folder");
                            }
                            
                        } else if(type.equals("folder")){
                            // Get the actual folder
                            folderJson = createFolderJson(f);
                            folderJson.write(writer);
                        }

                    } else {
                        throw new Exception("Missing information");
                    }

                }

            } else if (method.equals("getContents")) {
                // Get a folder contents
                String id = request.getParameter("id");
                if (id != null) {
                    
                    List contents = EJBLocator.lookupStorageBean().getAllFolderContents(ticket, id);
                    Object obj;
                    JSONArray contentsArray = new JSONArray();
                    for (int i = 0; i < contents.size(); i++) {
                        obj = contents.get(i);
                        if (obj instanceof DocumentRecord) {
                            contentsArray.put(createDocumentJson((DocumentRecord) obj));
                        } else if (obj instanceof Folder) {
                            if (!((Folder) obj).getId().equals(id)) {
                                // Exclude current folder
                                contentsArray.put(createFolderJson((Folder) obj));
                            }
                        }
                    }
                    contentsArray.write(writer);

                } else {
                    throw new Exception("No folder id specified");
                }

            } else if(method.equals("getVersions")){
                // Get the versions of a file
                String id = request.getParameter("id");
                if(id!=null){
                    List versions = EJBLocator.lookupStorageBean().listVersions(ticket, id);
                    Collections.reverse(versions);
                    JSONArray versionsJson = new JSONArray();
                    for(int i=0;i<versions.size();i++){
                        versionsJson.put(createVersionJson((DocumentVersion)versions.get(i)));
                    }
                    versionsJson.write(writer);
                } else {
                    throw new Exception("No document id specified");
                }
                
            } else if(method.equals("getProperty")){
                // Get a single property
                String id = request.getParameter("id");
                String groupName = request.getParameter("group");
                String propertyName = request.getParameter("property");
                PropertyItem item = EJBLocator.lookupPropertiesBean().getProperty(ticket, id, groupName, propertyName);
                if(item!=null){
                    JSONObject result = createPropertyItemJson(item);
                    result.write(writer);
                    
                } else {
                    JSONObject result = new JSONObject();
                    result.put("type", "notpresent");
                    result.write(writer);
                }
            } else if (method.equals("getProperties")){
                // Get object properties
                String id = request.getParameter("id");
                String groupName = request.getParameter("group");
                PropertyGroup group = EJBLocator.lookupPropertiesBean().getPropertyGroup(ticket, id, groupName);
                JSONObject propertiesJson;
                
                if(group!=null){
                    propertiesJson = createPropertyGroupJson(group);
                } else {
                    group = new PropertyGroup();
                    group.setObjectId(id);
                    group.setName(groupName);
                    propertiesJson = createPropertyGroupJson(group);
                }
                propertiesJson.write(writer);
                
            } else if (method.equals("getFile")) {
                // Get the contents of a file
                String id = request.getParameter("id");
                String versionId = request.getParameter("versionid");
                String containerId = request.getParameter("container");
                String name = request.getParameter("name");
                boolean fetchContents = true;
                if(request.getParameter("getcontents")!=null){
                    if(request.getParameter("getcontents").equals("true")){
                        fetchContents = true;
                    } else {
                        fetchContents = false;
                    }
                } 
                long maxBytes = 1000000;
                if (request.getParameter("maxbytes") != null) {
                    maxBytes = Long.parseLong(request.getParameter("maxbytes"));
                }

                InputStream stream = null;
                JSONObject fileJson = new JSONObject();
                DocumentRecord doc = null;

                if (id != null) {
                    // Plain file ID
                    doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, id);

                } else if (containerId != null && name != null) {
                    // Get a named document
                    doc = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, containerId, name);

                } else {
                    throw new Exception("No document id specified");
                }

                if (doc != null) {
                    fileJson = createDocumentJson(doc);

                    if(fetchContents){
                        DocumentVersion version;
                        if (versionId == null) {
                            version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, doc.getId());
                            stream = StorageUtils.getInputStream(ticket, doc, version);
                        } else {
                            version = EJBLocator.lookupStorageBean().getVersion(ticket, doc.getId(), versionId);
                            stream = StorageUtils.getInputStream(ticket, doc, version);
                        }

                        if (stream != null) {
                            long totalBytesRead = 0;
                            byte[] buffer = new byte[4096];
                            int bytesRead;
                            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                            while (totalBytesRead < maxBytes && (bytesRead = stream.read(buffer)) != -1) {
                                outStream.write(buffer, 0, bytesRead);
                                totalBytesRead += bytesRead;
                            }
                            stream.close();
                            outStream.flush();
                            outStream.close();

                            fileJson.put("content", new String(outStream.toByteArray()));
                            fileJson.put("version", createVersionJson(version));
                            
                        } else {
                            throw new Exception("Could not open document");
                        }
                    } else {
                        fileJson.put("content", "");
                    }
                    fileJson.write(writer);
                } else {
                    JSONObject errJson = createErrorJson("No such document", "notfound");
                    errJson.write(writer);
                }
            }

        } catch (Exception e) {
            try {
                JSONObject exJson = createExceptionJson(e);
                exJson.write(writer);
            } catch (Exception ex2) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
        writer.flush();
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
        File tempFile = null;
        try {

            WebTicket ticket = SessionUtils.getTicket(request);
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            String method = request.getParameter("method");
            String folderId = request.getParameter("container");
            String data = extractString(request.getInputStream());

            JSONObject dataObject;
            if(data!=null && !data.isEmpty()){
                dataObject = new JSONObject(data);
            } else {
                dataObject = new JSONObject();
            }

            if (method.equals("putFile")) {
                // Upload a new file
                DocumentRecord doc = null;
                Folder parent;
                if(folderId!=null && !folderId.isEmpty()){
                    parent = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                } else {
                    parent = null;
                }
                
                if (parent != null && dataObject.has("content")) {

                    if (!dataObject.has("id") || dataObject.getString("id").equals("")) {
                        // No id - new file
                        doc = new DocumentRecord();
                        doc.setName(dataObject.getString("name"));
                        doc.setDescription(dataObject.getString("description"));
                        doc.setContainerId(parent.getId());
                        doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
                    } else {
                        // Existing document
                        doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, dataObject.getString("id").trim());
                        doc.setName(dataObject.getString("name"));
                        doc.setDescription(dataObject.getString("description"));
                        doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
                    }

                    if (doc != null) {
                        byte[] btData = dataObject.getString("content").getBytes();
                        ByteArrayInputStream stream = new ByteArrayInputStream(btData);
                        StorageUtils.upload(ticket, stream, btData.length, doc, "");
                        stream.close();
                        JSONObject docJson = createDocumentJson(doc);
                        docJson.write(writer);
                    } else {
                        throw new Exception("Could not locate document");
                    }
                    
                } else if(parent==null && dataObject.has("content") && dataObject.has("id") && !dataObject.getString("id").equals("")){
                    // File already present in system
                    doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, dataObject.getString("id").trim());
                    if (doc != null) {
                        byte[] btData = dataObject.getString("content").getBytes();
                        ByteArrayInputStream stream = new ByteArrayInputStream(btData);
                        StorageUtils.upload(ticket, stream, btData.length, doc, "");
                        stream.close();
                        JSONObject docJson = createDocumentJson(doc);
                        docJson.write(writer);
                    } else {
                        throw new Exception("Could not locate document");
                    }                    
                    
                } else {
                    throw new Exception("Could not locate parent folder");
                }
                
            } else if(method.equals("copyBlockIcon")){
                // Copy a block icon file
                String targetFolderId = dataObject.getString("targetFolderId");
                String fileName = dataObject.getString("fileName");
                
                InputStream inStream = getClass().getResourceAsStream("/blockicons/" + fileName);
                
                // Do an in-memory copy so we can get the content length
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                ZipUtils.copyInputStream(inStream, buffer);
                inStream.close();
                buffer.flush();
                buffer.close();
                byte[] btData = buffer.toByteArray();
                ByteArrayInputStream byteStream = new ByteArrayInputStream(btData);
                DocumentRecord iconDoc = StorageUtils.getOrCreateDocumentRecord(ticket, targetFolderId, "icon.jpg");
                StorageUtils.upload(ticket, byteStream, btData.length, iconDoc, "Block Icon");
                

            } else if(method.equals("copyFile")){
                // Copy a file to a target folder
                String fileId = dataObject.getString("fileId");
                folderId = dataObject.getString("folderId");
                
                DocumentRecord file = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, fileId);
                Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                DocumentRecord copyFile = new DocumentRecord();
                copyFile.setName(file.getName());
                copyFile.setDescription(file.getDescription());
                copyFile.setContainerId(folder.getId());
                copyFile = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, copyFile);
                tempFile = File.createTempFile("file", "dat");
                DocumentVersion v = StorageUtils.copyDocumentData(ticket, file, null, copyFile);
                createDocumentJson(copyFile).write(writer);
                
            } else if(method.equals("removeFile")){
                // Delete a file
                String id = request.getParameter("id");
                if(id!=null && !id.isEmpty()){
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, id);
                    if(doc!=null){
                        JSONObject docJson = createDocumentJson(doc);
                        EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, id);
                        docJson.write(writer);
                    } else {
                        throw new Exception("Could not locate document");
                    }
                } else {
                    throw new Exception("No document id specified");
                }
                
            } else if (method.equals("putFolder")) {
                // Save a folder
                Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                Folder f;
                if (parent != null) {
                    if (!dataObject.has("id") || dataObject.getString("id").equals("")) {
                        // New folder
                        f = new Folder();
                        f.setName(dataObject.getString("name"));
                        f.setDescription(dataObject.getString("description"));
                        f.setContainerId(folderId);
                        f = EJBLocator.lookupStorageBean().addChildFolder(ticket, parent.getId(), f);

                    } else {
                        // Existing
                        f = EJBLocator.lookupStorageBean().getFolder(ticket, dataObject.getString("id").trim());
                        f.setName(dataObject.getString("name"));
                        f.setDescription(dataObject.getString("description"));
                        f = EJBLocator.lookupStorageBean().updateFolder(ticket, f);

                    }

                    if (f != null) {
                        JSONObject folderJson = createFolderJson(f);
                        folderJson.write(writer);
                    } else {
                        throw new Exception("Error creating folder");
                    }
                }
            } else if(method.equals("propagatePermissions")){
                // Apply the security settings of the project file to everything in the project folder
                String projectFolderId = dataObject.getString("id");
                if(projectFolderId!=null){
                    Folder projectFolder = EJBLocator.lookupStorageBean().getFolder(ticket, projectFolderId);
                    if(projectFolder!=null){
                        EJBLocator.lookupAccessControlBean().propagatePermissions(ticket, projectFolderId);
                        JSONObject folderJson = createFolderJson(projectFolder);
                        folderJson.write(writer);
                    } else {
                        throw new Exception("Project folder not found");
                    }
                } else {
                    throw new Exception("No project folder ID specified");
                }
            
            } else if(method.equals("setProperty")){
                // Set property of an object
                String objectId = dataObject.getString("objectId");
                String groupName = dataObject.getString("groupName");
                String propertyName = dataObject.getString("propertyName");
                String propertyValue = dataObject.getString("propertyValue");
                String propertyType;
                if(dataObject.has("propertyType")){
                    propertyType = dataObject.getString("propertyType");
                } else {
                    propertyType = "String";
                }
                EJBLocator.lookupPropertiesBean().setProperty(ticket, objectId, groupName, propertyName, propertyValue);
                PropertyGroup group = EJBLocator.lookupPropertiesBean().getPropertyGroup(ticket, objectId, groupName);
                if(group!=null){
                    createPropertyGroupJson(group).write(writer);
                } else {
                    throw new Exception("Property was not saved");
                }
                
            } else if(method.equals("buildService")){
                // Construct a service document
                String topFolderId = dataObject.getString("folderId");
                String srcFolderId = dataObject.getString("srcFolderId");
                String blockFileName = dataObject.getString("blockFileName");
                String projectFileId = dataObject.getString("projectFileId");
                boolean isService = dataObject.getBoolean("hasServiceXml");
                
                // Find the block file
                Folder topFolder = EJBLocator.lookupStorageBean().getFolder(ticket, topFolderId);
                if(topFolder!=null){
                    Folder srcFolder = EJBLocator.lookupStorageBean().getFolder(ticket, srcFolderId);
                    if(srcFolder!=null){
                        DocumentRecord serviceDoc = null;
                        DynamicWorkflowService dynamicServiceDoc = null;
                        DynamicWorkflowLibrary dynamicLibraryDoc = null;
                        
                        serviceDoc = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, topFolderId, blockFileName);
                        
                        if(serviceDoc!=null){
                            // Pre-existing document
                            if(!(serviceDoc instanceof DynamicWorkflowService) && isService){
                                // Wrong kind of document - needs to be a dynamic service
                                EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, serviceDoc.getId());
                                serviceDoc = new DynamicWorkflowService();
                                serviceDoc.setContainerId(topFolderId);
                                serviceDoc.setName(blockFileName);
                                
                                dynamicServiceDoc = (DynamicWorkflowService)serviceDoc;
                                dynamicServiceDoc.setProjectFileId(projectFileId);
                                serviceDoc = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(ticket, dynamicServiceDoc);
                                
                            } else if(!(serviceDoc instanceof DynamicWorkflowLibrary) && isService==false){
                                // Wrong kind of document - needs to be a dynamic library
                                EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, serviceDoc.getId());
                                serviceDoc = new DynamicWorkflowLibrary();
                                serviceDoc.setContainerId(topFolderId);
                                serviceDoc.setName(blockFileName);
                                
                                dynamicLibraryDoc = (DynamicWorkflowLibrary)serviceDoc;
                                serviceDoc = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(ticket, dynamicLibraryDoc);
                            }
                            
                        } else {
                            // Need to create a new document
                            if(isService){
                                serviceDoc = new DynamicWorkflowService();
                                serviceDoc.setContainerId(topFolderId);
                                serviceDoc.setName(blockFileName);
                                dynamicServiceDoc = (DynamicWorkflowService)serviceDoc;
                                dynamicServiceDoc.setProjectFileId(projectFileId);
                                serviceDoc = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(ticket, dynamicServiceDoc);
                            } else {
                                serviceDoc = new DynamicWorkflowLibrary();
                                serviceDoc.setContainerId(topFolderId);
                                serviceDoc.setName(blockFileName);
                                dynamicLibraryDoc = (DynamicWorkflowLibrary)serviceDoc;
                                serviceDoc = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(ticket, dynamicLibraryDoc);
                            }
                        }
                        
                        // Zip the folder
                        if(!srcFolder.getId().equals(user.getHomeFolderId())){
                            Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, srcFolder.getContainerId());
                            if(EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, parent, Permission.WRITE_PERMISSION)){
                                // Build a folder tree for the user
                                DocumentRecord zipDoc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, serviceDoc.getId());
                                
                                FolderHolder tree = null;
                                if(ticket.getDefaultProjectId()!=null && !ticket.getDefaultProjectId().isEmpty()){
                                    // Build project tree
                                    Project p = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(ticket.getDefaultProjectId()));
                                    if(p!=null){
                                        Folder projectFolder = EJBLocator.lookupStorageBean().getFolder(ticket, p.getDataFolderId());
                                        if(projectFolder!=null){
                                            tree = FolderHolder.createProjectFolderTree(ticket, p.getId());
                                        } else {
                                            tree = FolderHolder.createFullFolderTree(ticket);
                                        }
                                    } else {
                                        tree = FolderHolder.createFullFolderTree(ticket);
                                    }
                                    
                                } else {
                                    // Create user directory tree
                                    tree = FolderHolder.createFullFolderTree(ticket);
                                }
                                
                                FolderHolder holder = tree.findHolder(srcFolder.getId());
                                if(holder!=null){
                                    Zipper zipper = new Zipper(ticket, zipDoc, "Compressed version of: " + srcFolder.getName());
                                    zipper.setupStreams();
                                    if(zipper.hasError()){
                                        throw new Exception("Zip error");
                                    }

                                    holder.appendToZipper(ticket, zipper, holder);
                                    DocumentVersion version = zipper.closeStreams();
                                    if(version==null){
                                        throw new Exception("Error zipping data");
                                    }

                                    // Process service document if this is a workflow service
                                    if(zipDoc instanceof DynamicWorkflowService){
                                        WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceXml(ticket, zipDoc.getId(), version.getId());

                                        //log that the library has been saved in the graphDb
                                        DynamicWorkflowService service = (DynamicWorkflowService) zipDoc;
                                        DocumentVersion serviceVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, service.getId());
                                        ServiceSaveOperation saveOp = new ServiceSaveOperation(service.getId(), serviceVersion.getId(), service.getCurrentVersionNumber(), service.getName(), ticket.getUserId(), new Date(System.currentTimeMillis()));
                                        saveOp.setProjectId(ticket.getDefaultProjectId());
                                        ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                                        provClient.log(saveOp);
                                    } else if(zipDoc instanceof DynamicWorkflowLibrary){
                                        WorkflowEJBLocator.lookupWorkflowManagementBean().updateLibraryXml(ticket, zipDoc.getId(), version.getId());
                                        
                                        // Log the save of the libarary
                                        DynamicWorkflowLibrary library = (DynamicWorkflowLibrary)zipDoc;
                                        DocumentVersion libraryVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, library.getId());
                                        LibrarySaveOperation saveOp = new LibrarySaveOperation(library.getId(), libraryVersion.getId(), library.getLibraryName(),libraryVersion.getVersionNumber(), ticket.getUserId(), new Date());
                                        saveOp.setProjectId(ticket.getDefaultProjectId());
                                        ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                                        provClient.log(saveOp);                                              
                                    }

                                    // Make sure everything has the correct permissions
                                    EJBLocator.lookupAccessControlBean().propagatePermissions(ticket, topFolderId);
                                    
                                    JSONObject documentJson = createDocumentJson(zipDoc);
                                    documentJson.write(writer);                                                    

                                } else {
                                    throw new Exception("Cannot find target folder in tree");
                                }

                            } else {
                                throw new Exception("User cannot access parent folder: " + parent.getName());
                            }
                        } else {
                            throw new Exception("Cannot zip entire home folder");
                        }                        
                      
                    } else {
                        throw new Exception("No source folder found in top level block folder");
                    }
                } else {
                    throw new Exception("No top level service folder");
                }
                
            } else {
                throw new Exception("Unknown method");
            }

        } catch (Exception e) {
            try {
                JSONObject exJson = createExceptionJson(e);
                exJson.write(writer);
            } catch (Exception ex2) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        } finally {
            if(tempFile!=null){
                try {
                    if(!tempFile.delete()){
                        tempFile.deleteOnExit();
                    }
                } catch (Exception e){
                    tempFile.deleteOnExit();
                }
            }
        }
        writer.flush();
    }
    
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** Create a JSON Object to represent an Exception */
    private JSONObject createExceptionJson(Exception e) throws Exception {
        JSONObject exceptionJson = new JSONObject();
        exceptionJson.put("class", e.getClass().getSimpleName());
        exceptionJson.put("message", e.getMessage());
        exceptionJson.put("type", "error");
        return exceptionJson;
    }

    /** Create an error JSON object */
    private JSONObject createErrorJson(String message, String errorCategory) throws Exception {
        JSONObject errorJson = new JSONObject();
        errorJson.put("message", message);
        errorJson.put("class", "GeneratedError");
        errorJson.put("type", "error");
        errorJson.put("errorCategory", errorCategory);
        return errorJson;
    }

    /** Create a folder json */
    private JSONObject createFolderJson(Folder folder) throws Exception {
        JSONObject folderJson = new JSONObject();
        if (folder != null) {
            folderJson.put("id", folder.getId());
            folderJson.put("name", folder.getName());
            folderJson.put("description", folder.getDescription());
            folderJson.put("type", "folder");
        } else {
            throw new Exception("No such folder");
        }
        return folderJson;
    }

    /** Create JSON for a property item */
    private JSONObject createPropertyItemJson(PropertyItem property) throws Exception {
        JSONObject propertyJson = new JSONObject();            
        propertyJson.put("name", property.getName());
        propertyJson.put("type", property.getType());
        propertyJson.put("value", property.getValue());
        return propertyJson;       
    }
    
    /** Create JSON for a property group */
    private JSONObject createPropertyGroupJson(PropertyGroup group) throws Exception {
        JSONObject groupJson = new JSONObject();
        groupJson.put("id", group.getId());
        groupJson.put("objectId", group.getObjectId());
        groupJson.put("groupName", group.getName());
        groupJson.put("type", "properties");
        Enumeration<PropertyItem> i = group.getProperties().elements();
        JSONArray propertiesJson = new JSONArray();
        while(i.hasMoreElements()){
            propertiesJson.put(createPropertyItemJson(i.nextElement()));
        }
        groupJson.put("properties", propertiesJson);
        return groupJson;
    }
    
    /** Create a document json */
    private JSONObject createDocumentJson(DocumentRecord doc) throws Exception {
        if (doc != null) {
            JSONObject documentJson = new JSONObject();
            documentJson.put("id", doc.getId());
            documentJson.put("name", doc.getName());
            documentJson.put("description", doc.getDescription());
            documentJson.put("type", "document");
            documentJson.put("containerId", doc.getContainerId());
            return documentJson;
        } else {
            throw new Exception("No such document");
        }
    }
    
    /** Create a document version json */
    private JSONObject createVersionJson(DocumentVersion version) throws Exception {
        if(version!=null){
            JSONObject versionJson = new JSONObject();
            versionJson.put("id", version.getId());
            versionJson.put("documentId", version.getDocumentRecordId());
            versionJson.put("size", version.getSize());
            versionJson.put("timestamp", fullFormat.format(version.getTimestampDate()));
            versionJson.put("day", dayFormat.format(version.getTimestampDate()));
            versionJson.put("time", timeFormat.format(version.getTimestampDate()));
            versionJson.put("number", version.getVersionNumber());
            versionJson.put("comments", version.getComments());
            return versionJson;
        } else {
            throw new Exception("No such version");
        }
    }

    /**
     * Extract a String from an InputStream
     */
    public static String extractString(InputStream stream) throws Exception {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = stream.read(buffer)) != -1) {
            array.write(buffer, 0, len);
        }
        array.flush();
        array.close();
        return new String(array.toByteArray());
    }
}

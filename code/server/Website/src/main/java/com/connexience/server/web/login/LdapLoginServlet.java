package com.connexience.server.web.login;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.LogonEvent;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.ExternalLogonDetails;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.util.LdapUtils;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.util.FlashUtils;
import com.connexience.server.web.util.WebUtil;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LdapLoginServlet extends HttpServlet {
    private static final String LDAP_PROVIDER = "LDAP";

    private boolean enabled = false;
    private String host = "";
    private String defaultDomain = "";

    @Override
    public void init() throws ServletException {
        final ServletContext context = getServletContext();

        enabled = Boolean.valueOf(context.getInitParameter("ldap.enabled"));

        if (enabled) {
            host = "ldap://" + context.getInitParameter("ldap.host") + "/";
            defaultDomain = context.getInitParameter("ldap.domain");

            if (StringUtils.isBlank(host) || StringUtils.isBlank(defaultDomain)) {
                throw new ServletException("LDAP Host and Domain must be specified in web.xml");
            }
        }
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try {
            // Login may include domain and username
            final String login = request.getParameter("username");
            final String password = request.getParameter("password");

            final boolean rememberMe = Boolean.valueOf(request.getParameter("rememberMe")); // true only for "true"

            if (!enabled) {
                FlashUtils.setError(request, "LDAP Authentication is not enabled.");
                response.sendRedirect("../pages/front/welcome.jsp");
            }

            if (StringUtils.isBlank(login) || StringUtils.isBlank(password)) {
                fail(request, response, "You must specify a username and password.");
                return;
            }

            final String username = LdapUtils.extractUsername(login);
            final String domain = LdapUtils.extractDomain(login, defaultDomain);

            if (!LdapUtils.validateCredentials(username, password, domain, host)) {
                fail(request, response, "Provided LDAP credentials could not be authenticated.");
                return;
            }

            // combine DOMAIN with USERNAME for some kind of collision resistance
            final String id = LdapUtils.generateExternalLogonId(username, domain);

            ExternalLogonDetails external = EJBLocator.lookupTicketBean().getExternalLogon(id, LDAP_PROVIDER);

            if (external != null) {
                SessionUtils.login(request, external.getUserId());
                LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, ((User) request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
                ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                client.log(logonEvent);
            } else {
                // Get original URL for creating ELD
                String originalURL = WebUtil.getHostname(request) + request.getContextPath();

                // Build the user
                User user = new User();
                user.setName(username);

                // ConnexienceException thrown if user cannot be registered
                user = EJBLocator.lookupUserDirectoryBean().createAccountForExternalLogon(user, id, originalURL, LDAP_PROVIDER);

                // Login
                SessionUtils.login(request, user.getId());
                LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, ((User) request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
                ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                client.log(logonEvent);

                //get the ticket so that we can set the email address in the profile
                Ticket ticket = (Ticket) request.getSession().getAttribute("TICKET");

                // Set names and save
                user.setFirstName(username);
                user.setSurname("(" + domain + ")");

                user = EJBLocator.lookupUserDirectoryBean().saveUser(ticket, user);
            }

            /* REMEMBER ME*/
            Ticket ticket = (Ticket) request.getSession().getAttribute("TICKET");

            if (rememberMe && ticket != null) {
                String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket).getDefaultUserId();
                if (!ticket.getUserId().equals(publicUserId)) {
                    String uuid = EJBLocator.lookupTicketBean().addRememberMe(ticket);
                    Cookie c = new Cookie("rememberMe", uuid);
                    c.setMaxAge(60 * 60 * 24 * 365); // 1 year
                    c.setPath(request.getContextPath()); //add the cookie for all directories, not just login
                    response.addCookie(c);
                }
            }

            response.sendRedirect("../pages/front/index.jsp");
        } catch (final Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    // Set fail message and redirect to LDAP login
    private void fail(HttpServletRequest request, HttpServletResponse response, String error) throws IOException {
        FlashUtils.setError(request, error);
        response.sendRedirect("../pages/front/ldap.jsp");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.finder;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.SearchFolder;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.OldProject;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.StorageUtils;
import static com.connexience.server.util.StorageUtils.getDocumentTypeByExtention;
import com.connexience.server.util.ZipFileCreator;
import com.connexience.server.util.Zipper;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author hugo
 */
public class ElFinder extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ElFinder</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ElFinder at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");

        Ticket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        Hashtable<String, DocumentType> typeMap;
        ZipFileCreator zipper = null;

        response.setContentType("text/html");
        ServerObject obj;
        Folder folder;
        try {
            // Check the type map
            if (session.getAttribute("TYPE_MAP") == null) {
                typeMap = createTypeMap(ticket);
                session.setAttribute("TYPE_MAP", typeMap);

            } else {
                typeMap = (Hashtable<String, DocumentType>) session.getAttribute("TYPE_MAP");
            }

            // Split the target param into a volumeID and folder ID
            String targetParam = request.getParameter("target");
            String folderId = null;
            String volumeId = null;

            if (targetParam != null) {
                int location = targetParam.indexOf('_');
                if (location != -1) {
                    volumeId = targetParam.substring(0, location);
                    folderId = targetParam.substring(location + 1);
                } else {
                    volumeId = "";
                    folderId = targetParam;
                }
            }

            if (cmd != null) {
                JSONObject result = new JSONObject();

                if (cmd.equals("open")) {
                    String initParam = request.getParameter("init");

                    if (initParam != null && (initParam.equals("true") || initParam.equals("1"))) {
                        // Re-create type map
                        typeMap = createTypeMap(ticket);
                        session.setAttribute("TYPE_MAP", typeMap);

                        // Build a list of volumes. If none, use the home folder
                        Enumeration<String> pNamesEnum = request.getParameterNames();

                        //Convert the enum into an Array so that we can sort it to control the order
                        //that things are returned.  The list is used in the LHS of the elfinder2 for the
                        //tree, in the order returned by this servlet.
                        ArrayList<String> pNamesArray = new ArrayList<>();
                        while (pNamesEnum.hasMoreElements()) {
                            pNamesArray.add(pNamesEnum.nextElement());
                        }
                        Collections.sort(pNamesArray);

                        ArrayList<String> volumeIds = new ArrayList<>();
                        for (String paramName : pNamesArray) {
                            if (paramName.startsWith("vol")) {
                                volumeIds.add(request.getParameter(paramName));
                            }
                        }

                        Folder[] volumes;

                        if (volumeIds.size() > 0) {
                            volumes = new Folder[volumeIds.size()];
                            for (int i = 0; i < volumeIds.size(); i++) {
                                ServerObject so = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, volumeIds.get(i), ServerObject.class);

                                if (so instanceof Folder) {
                                    volumes[i] = EJBLocator.lookupStorageBean().getFolder(ticket, volumeIds.get(i));
                                } else if (so instanceof User) {
                                    //object is not a folder; is it a user?
                                    User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, volumeIds.get(i));
                                    volumes[i] = EJBLocator.lookupStorageBean().getFolder(ticket, u.getHomeFolderId());
                                }
                            }
                        } else {
                            volumes = new Folder[1];
                            Folder home = EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId());
                            volumes[0] = home;
                        }

                        // Send the first volume as the current working directory
                        Folder cwdFolder = volumes[0];
                        JSONObject cwdJson = createFolderJson(cwdFolder.getId(), cwdFolder);
                        result.put("cwd", cwdJson);

                        // Files object
                        JSONArray files = new JSONArray();

                        Folder homeFolder = EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId());

                        // Add volumes
                        JSONObject volumeJson;
                        for (Folder volume : volumes) {
                            if (volume.getId().equals(homeFolder.getId())) {
                                volumeJson = createHomeFolderJson(homeFolder);
                            } else {
                                volumeJson = createVolumeJson(volume, volume.getId());
                            }
                            files.put(volumeJson);
                        }

                        // Add files for each volumne
                        for (Folder volume : volumes) {
                            String shared = request.getParameter("shared");
                            if (shared != null && shared.equals("true")) {
                                addSharedFolderContents(ticket, files, volume.getCreatorId(), volume.getId(), typeMap);
                                //reset all of the container ids as otherwise they won't show in elfinder
                                for (int i = 0; i < files.length(); i++) {
                                    JSONObject file = files.getJSONObject(i);
                                    if (file.has("phash")) {
                                        file.put("phash", volume.getId() + "_" + volume.getId());
                                    }
                                }
                            } else {
                                addFolderContents(ticket, files, volume.getId(), volume.getId(), typeMap);
                            }
                        }

                        // TODO: ADD VOLUMES AND FILES FOR PROJECTS
                        result.put("files", files);

                        // Send API version
                        result.put("api", "2.0");

                    } else {
                        // Plain open without an init parameter
                        folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);

                        JSONObject folderJson = createFolderJson(volumeId, folder);
                        result.put("cwd", folderJson);

                        JSONArray files = new JSONArray();
                        addFolderContents(ticket, files, folderId, volumeId, typeMap);
                        result.put("files", files);

                    }

                    result.put("options", createOptions());

                } else if (cmd.equals("tree")) {
                    // Return child directories
                    result.put("tree", createTree(ticket, folderId, volumeId));

                } else if (cmd.equals("ls")) {
                    // List files in folder
                    List contents = EJBLocator.lookupStorageBean().getAllFolderContents(ticket, folderId);
                    JSONArray list = new JSONArray();
                    for (int i = 0; i < contents.size(); i++) {
                        if (contents.get(i) instanceof DocumentRecord) {
                            list.put(((DocumentRecord) contents.get(i)).getName());
                        }
                    }
                    result.put("list", list);

                } else if (cmd.equals("paste")) {
                    // Copy or move files
                    String targetFolderId = getObjectId(request.getParameter("dst"));
                    if (!isSearchFolder(ticket, targetFolderId)) {
                        String[] targets = request.getParameterValues("targets[]");
                        String cut = request.getParameter("cut");
                        JSONArray removed = new JSONArray();
                        JSONArray added = new JSONArray();

                        for (int i = 0; i < targets.length; i++) {
                            obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(targets[i]), ServerObject.class);

                            String newProjectId = null;
                            String destination = request.getParameter("dst");

                            if (destination != null) {
                                //will be of the form volId_docId
                                if (!getVolumeId(targets[i]).equals(getVolumeId(destination))) {
                                    String newProjectDataFolderId = getVolumeId(destination);

                                    //if the new project volume == 0 this is the user's home folder.  Leave the newProjectId as null
                                    //so that it gets unset in the save
                                    if (!newProjectDataFolderId.equals("0")) {
                                        Folder newProjectDataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, newProjectDataFolderId);
                                        newProjectId = newProjectDataFolder.getProjectId();
                                    }
                                }
                            }

                            if (cut.equals("1")) {
                                // Move files
                                if (obj instanceof DocumentRecord) {
                                    // Move document
                                    EJBLocator.lookupStorageBean().moveDocument(ticket, obj.getId(), targetFolderId, newProjectId);
                                    removed.put(targets[i]);
                                } else if (obj instanceof Folder) {
                                    // Move folder
                                    EJBLocator.lookupStorageBean().moveFolder(ticket, obj.getId(), targetFolderId, newProjectId);
                                    removed.put(targets[i]);
                                } else if(obj instanceof Dataset){
                                    Dataset dataset = (Dataset)obj;
                                    dataset.setProjectId(newProjectId);
                                    dataset.setContainerId(targetFolderId);
                                    dataset = EJBLocator.lookupDatasetsBean().saveDataset(ticket, dataset);
                                    removed.put(targets[i]);
                                }

                            } else {
                                // Copy files
                                if (obj instanceof DocumentRecord) {

                                    DocumentRecord sourceDocument = (DocumentRecord) obj;
                                    DocumentRecord targetDocument = new DocumentRecord();

                                    if (obj instanceof WorkflowDocument) {
                                        sourceDocument = (WorkflowDocument) obj;
                                        targetDocument = new WorkflowDocument();
                                    }

                                    sourceDocument.populateCopy(targetDocument);
                                    targetDocument.setContainerId(targetFolderId);
                                    targetDocument = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, targetDocument);

                                    EJBLocator.lookupStorageBean().checkProjectId(ticket, targetDocument, newProjectId);

                                    StorageUtils.copyDocumentData(ticket, sourceDocument, null, targetDocument);

                                    ImageData image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, obj.getId(), ImageData.WORKFLOW_PREVIEW);
                                    if (obj instanceof WorkflowDocument) {
                                        if (image != null) {
                                            ImageData copyImage = image.getCopy();
                                            copyImage.setServerObjectId(targetDocument.getId());
                                            EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, targetDocument.getId(), copyImage.getData(), copyImage.getType());
                                        }
                                    }

                                    added.put(createDocumentRecordJson(getVolumeId(targets[i]), targetDocument, typeMap));
                                } else {
                                    // TODO: Cannot copy folders (yet)
                                    throw new Exception("Cannot copy folders or datasets yet");
                                }
                            }
                        }

                        result.put("added", added);
                        result.put("removed", removed);
                    }

                } else if (cmd.equals("rm")) {
                    // Delete a document
                    String[] targets = request.getParameterValues("targets[]");
                    JSONArray removed = new JSONArray();
                    boolean error = false;
                    String errorMessage = "";

                    for (int i = 0; i < targets.length; i++) {
                        try {
                            obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(targets[i]), ServerObject.class);
                            if (obj instanceof DocumentRecord) {
                                EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, getObjectId(targets[i]));
                            } else if (obj instanceof Folder) {
                                EJBLocator.lookupStorageBean().removeFolderTree(ticket, getObjectId(targets[i]));
                            } else if (obj instanceof Dataset){
                                EJBLocator.lookupDatasetsBean().removeDataset(ticket, getObjectId(targets[i]));
                            }

                            removed.put(targets[i]);
                        } catch (Exception e) {
                            error = true;
                            errorMessage = e.getMessage();
                        }
                    }
                    result.put("removed", removed);
                    if (error) {
                        throw new Exception("Some folders could not be removed: " + errorMessage);
                    }

                } else if (cmd.equals("rename")) {
                    // Rename a document
                    String target = request.getParameter("target");
                    String newName = request.getParameter("name");
                    if (newName != null && !newName.trim().isEmpty()) {
                        obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(target), ServerObject.class);
                        if (obj instanceof DocumentRecord) {
                            DocumentRecord doc = (DocumentRecord) obj;
                            
                            // Re-do the MIME type
                            String newExtension = StorageUtils.getExtension(newName.trim());
                            String oldExtension = StorageUtils.getExtension(doc.getName());
                            if(newExtension==null) {
                                // Remove mime type
                                doc.setDocumentTypeId(null);
                            } else {
                                // If mimetype has changed, reset it in the database
                                if (oldExtension != null && !oldExtension.trim().equals(newExtension.trim())) {
                                    DocumentType type = getDocumentTypeByExtention(ticket, newExtension);
                                    if (type != null) {
                                        doc.setDocumentTypeId(type.getId());
                                    }
                                } else if(oldExtension==null){
                                    DocumentType type = getDocumentTypeByExtention(ticket, newExtension);
                                    if (type != null) {
                                        doc.setDocumentTypeId(type.getId());
                                    }
                                    
                                }
                            }
                            
                            doc.setName(newName.trim());
                            doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
                            JSONArray added = new JSONArray();
                            added.put(createDocumentRecordJson(getVolumeId(target), doc, typeMap));
                            JSONArray removed = new JSONArray();
                            removed.put(target);
                            result.put("added", added);
                            result.put("removed", removed);
                        } else if (obj instanceof Folder) {
                            folder = (Folder) obj;
                            folder.setName(newName.trim());
                            folder = EJBLocator.lookupStorageBean().updateFolder(ticket, folder);
                            JSONArray added = new JSONArray();
                            added.put(createFolderJson(getVolumeId(target), folder));
                            JSONArray removed = new JSONArray();
                            removed.put(target);
                            result.put("added", added);
                            result.put("removed", removed);
                        } else if(obj instanceof Dataset){
                            Dataset dataset = (Dataset)obj;
                            dataset.setName(newName.trim());
                            dataset = EJBLocator.lookupDatasetsBean().saveDataset(ticket, dataset);
                            JSONArray added = new JSONArray();
                            added.put(createDatasetRecordJson(getVolumeId(target), dataset));
                            JSONArray removed = new JSONArray();
                            removed.put(target);
                            result.put("added", added);
                            result.put("removed", removed);
                        }

                    } else {
                        throw new Exception("No name specified");
                    }
                } else if (cmd.equals("mkdir")) {
                    // Create a folder
                    String target = request.getParameter("target");
                    String name = request.getParameter("name");
                    if (name != null && !name.trim().isEmpty()) {
                        String targetFolderId = getObjectId(target);
                        Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
                        if (parent != null) {
                            folder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, folderId, name);
                            if (folder == null) {
                                // OK to create folder
                                folder = new Folder();
                                folder.setName(name);
                                folder = EJBLocator.lookupStorageBean().addChildFolder(ticket, parent.getId(), folder);

                                setProjectIdFromVolume(ticket, folder, parent, getVolumeId(target));

                                JSONArray added = new JSONArray();
                                added.put(createFolderJson(getVolumeId(target), folder));
                                result.put("added", added);
                            } else {
                                throw new Exception("Folder: " + name + " already exists");
                            }
                        } else {
                            throw new Exception("Error creating folder. Parent folder does not exist");
                        }
                    }

                } else if (cmd.equals("mkfile")) {
                    // Create a new text file 
                    String name = request.getParameter("name");
                    String target = request.getParameter("target");
                    if (name != null && !name.trim().isEmpty()) {
                        folder = EJBLocator.lookupStorageBean().getFolder(ticket, getObjectId(target));
                        if (EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, folder.getId(), name.trim()) == null) {
                            DocumentRecord doc = new DocumentRecord();
                            doc.setName(name.trim());
                            doc.setCreatorId(ticket.getUserId());
                            doc.setContainerId(folder.getId());
                            DocumentType t = EJBLocator.lookupStorageBean().getDocumentTypeByMime(ticket, "text/plain");
                            if (t != null) {
                                doc.setDocumentTypeId(t.getId());
                            }
                            doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);
                            DocumentVersion v = StorageUtils.upload(ticket, new byte[0], doc, "Created using file manager");

                            //re-get the document as it now has a new latest version number in it
                            doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, doc.getId());

                            setProjectIdFromVolume(ticket, doc, folder, getVolumeId(target));

                            JSONArray added = new JSONArray();
                            added.put(createDocumentRecordJson(getVolumeId(target), doc, typeMap));
                            result.put("added", added);
                        } else {
                            throw new Exception("File named: " + name.trim() + " already exists");
                        }
                    } else {
                        throw new Exception("Cannot create file with empty name");
                    }

                } else if (cmd.equals("escmkdataset")){
                    // Create a dataset
                    String target = request.getParameter("target");
                    String name = request.getParameter("name");
                    if (name != null && !name.trim().isEmpty()) {
                        String targetFolderId = getObjectId(target);
                        Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, targetFolderId);
                        if (parent != null) {
                            // Create a dataset here
                            Dataset ds = new Dataset();
                            ds.setName(name);
                            ds.setContainerId(targetFolderId);
                            ds.setDescription("New Dataset");
                            ds = EJBLocator.lookupDatasetsBean().saveDataset(ticket, ds);
                            setProjectIdFromVolume(ticket, ds, parent, getVolumeId(target));
                            JSONArray added = new JSONArray();
                            added.put(createDatasetRecordJson(getVolumeId(target), ds));
                            result.put("added", added);
                        } else {
                            throw new Exception("Error creating folder. Parent folder does not exist");
                        }
                    }                    
                    
                } else if(cmd.equals("escextractdataset")){
                    // Extract a dataset
                    String target = request.getParameter("target");
                    String fileName = request.getParameter("fileName");
                    String itemName = request.getParameter("itemName");
                    obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(target), ServerObject.class);
                    if(obj instanceof Dataset){
                        Dataset ds = (Dataset)obj;
                        Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, ds.getContainerId());
                        DocumentRecord doc = StorageUtils.getOrCreateDocumentRecord(ticket, parent.getId(), fileName);
                        setProjectIdFromVolume(ticket, doc, parent, getVolumeId(target));
                        long rows = EJBLocator.lookupDatasetsBean().extractItemDataToDocument(ticket, ds.getId(), itemName, doc, 100);
                        JSONArray added = new JSONArray();
                        added.put(createDocumentRecordJson(getVolumeId(target), doc, typeMap));
                        result.put("added", added);
                        
                    } else {
                        throw new Exception("Selected object is not a dataset");
                    }
                    
                } else if (cmd.equals("get")) {
                    // Get the content of the file
                    String target = request.getParameter("target");
                    obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(target), ServerObject.class);
                    if (obj instanceof DocumentRecord) {
                        DocumentRecord doc = (DocumentRecord) obj;
                        byte[] docData = StorageUtils.download(ticket, doc, null);
                        result.put("content", new String(docData));
                    } else {
                        throw new Exception("Selected object is not a document record");
                    }

                } else if (cmd.equals("archive")) {
                    // Create a zip archive of a folder
                    String[] targets = request.getParameterValues("targets[]");
                    if (targets != null && targets.length > 0) {
                        zipper = new ZipFileCreator(ticket);
                        for (int i = 0; i < targets.length; i++) {
                            zipper.addTopLevelItem(getObjectId(targets[i]));
                        }
                        String fileName = EJBLocator.lookupObjectInfoBean().getObjectName(ticket, getObjectId(targets[0])) + ".zip";
                        String targetFolderId = getVolumeId(targets[0]);

                        // Begin the zip process
                        DocumentRecord doc = zipper.compressData(targetFolderId, fileName);

                        JSONArray added = new JSONArray();
                        added.put(createDocumentRecordJson(getVolumeId(targets[0]), doc, typeMap));
                        result.put("added", added);

                    } else {
                        throw new Exception("No targets specified in archive");
                    }
                    
                } else if(cmd.equals("file")){
                    // Handle download
                    if("1".equals(request.getParameter("download"))){
                        // Download a file
                        String target = request.getParameter("target");
                        if(target!=null && !target.isEmpty()){

                            // Send single file
                            //<a href="../../servlets/download/StudyBlock.blk?documentid=7220&amp;versionid=7221" 
                            String documentId = getObjectId(target);
                            DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                            if(doc!=null){
                                /*
                                DocumentVersion v = EJBLocator.lookupStorageBean().getLatestVersion(ticket, doc.getId());
                                if(v!=null){
                                    response.setContentType("application/data");
                                    result = null;
                                    OutputStream stream = response.getOutputStream();
                                    try {
                                        StorageUtils.downloadFileToOutputStream(ticket, doc, v, stream);
                                    } catch (Exception e){
                                        throw e;
                                    } finally {
                                        stream.flush();
                                        stream.close();
                                    }
                                } else {
                                    throw new Exception("No data for document");
                                }
                                    */
                                response.sendRedirect("../servlets/download/" + doc.getName() + "?documentid=" + doc.getId());
                                
                            } else {
                                throw new Exception("No such document");
                            }
                        }
                    }
                    
                } else if (cmd.equals("parents")) {
                    // Parents of the folder
                    /*
                    String target = request.getParameter("target");
                    folderId = getObjectId(target);
                    volumeId = getVolumeId(target);
                    List parents = EJBLocator.lookupStorageBean().getParentHierarchy(ticket, folderId);
                    
                    JSONArray parentsJson = new JSONArray();
                    Folder f;
                    for(int i=0;i<parents.size();i++){
                        f = (Folder)parents.get(i);
                        if(i==0){
                            parentsJson.put(createVolumeJson(f, volumeId));
                                    
                        } else {
                            if(f.getId().equals(folderId)){
                                parentsJson.put(createFolderJson(volumeId, (Folder)f));
                            }
                        }
                    }
                    
                    // First folder will be the volume
                    
                    result.put("tree", parentsJson);
                    */

                } else if (cmd.equals("size")) {
                    String[] targets = request.getParameterValues("targets[]");
                    if (targets != null && targets.length > 0) {
                        long totalSize = 0;
                        for (String target : targets) {
                            totalSize += getObjectSize(ticket, getObjectId(target));
                        }
                        result.put("size", totalSize);
                    }
                    
                } else {
                    log("Unsupported ElFinder GET command: " + cmd);
                }
                
                if(result!=null){
                    result.write(response.getWriter());
                }
            } else {
                processRequest(request, response);
            }
        } catch (Exception e) {
            try {
                e.printStackTrace();
                if(!response.isCommitted()){
                    JSONObject result = new JSONObject();
                    result.put("error", "Server Error: " + e.getMessage());
                    result.put("errorData", e.toString());
                    result.write(response.getWriter());
                }
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }

        }
    }


    private long getObjectSize(Ticket ticket, String objectId) throws ConnexienceException
    {
        ServerObject o = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, objectId, ServerObject.class);

        if (o instanceof DocumentRecord) {
            return ((DocumentRecord)o).getCurrentVersionSize();
        } else if (o instanceof Folder) {
            List contents = EJBLocator.lookupStorageBean().getAllFolderContents(ticket, o.getId());
            long size = 0;
            for (Object c : contents) {
                ServerObject sObj = (ServerObject)c;
                size += getObjectSize(ticket, ((ServerObject) c).getId());
            }
            return size;
        } else {
            // TODO: Check with Hugo whether it's safe to ignore the size of Datasets and possibly other types of objects.
            return 0;
        }
    }


    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Ticket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        Hashtable<String, DocumentType> typeMap = null;
        String rootFolderId = request.getParameter("rootid");
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(Long.MAX_VALUE);
        String cmd = null;
        String target = null;
        String content = null;
        List items = null;
        FileItem item;
        response.setContentType("text/html");

        ArrayList<FileItem> files = new ArrayList<>();

        try {
            // Check the type map
            if (session.getAttribute("TYPE_MAP") == null) {
                typeMap = createTypeMap(ticket);
                session.setAttribute("TYPE_MAP", typeMap);

            } else {
                typeMap = (Hashtable<String, DocumentType>) session.getAttribute("TYPE_MAP");
            }

            if (ServletFileUpload.isMultipartContent(request)) {
                // Multipart data post
                items = upload.parseRequest(request);
                for (int i = 0; i < items.size(); i++) {
                    item = (FileItem) items.get(i);
                    if (item.isFormField()) {
                        if (item.getFieldName().equals("cmd")) {
                            cmd = item.getString();
                        } else if (item.getFieldName().equals("target")) {
                            target = item.getString();
                        } else if (item.getFieldName().equals("content")) {
                            content = item.getString();
                        }
                    } else {
                        files.add(item);
                    }
                }
            } else {
                // Simple form post
                cmd = request.getParameter("cmd");
                target = request.getParameter("target");
                content = request.getParameter("content");
            }

            // Act on command
            if (cmd != null) {
                JSONObject result = new JSONObject();

                if (cmd.equals("upload")) {
                    if (target != null) {
                        Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, getObjectId(target));
                        DocumentRecord document;
                        DocumentVersion version;
                        JSONArray added = new JSONArray();
                        boolean error = false;
                        JSONArray errorArray = new JSONArray();

                        if (folder != null) {
                            InputStream stream;
                            for (int i = 0; i < files.size(); i++) {
                                item = files.get(i);

                                // Check quota
                                if (EJBLocator.lookupQuotaBean().userHasStorageQuota(ticket, ticket.getUserId()) == false || EJBLocator.lookupQuotaBean().getAvailableStorageQuota(ticket, ticket.getUserId()) >= item.getSize()) {
                                    String name = item.getName();
                                    long size = item.getSize();
                                    if (name != null && !name.isEmpty() && size > 0) {
                                        // Save file
                                        try {

                                            //Create the document record, document version and file on disk in one Tx
                                            UserTransaction tx = null;
                                            try {
                                                tx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                                                tx.begin();

                                                document = StorageUtils.getOrCreateDocumentRecord(ticket, folder.getId(), name);
                                                version = StorageUtils.upload(ticket, item.getInputStream(), item.getSize(), document, "Uploaded from Web");

                                                //re-get the document so that we can set the project id
                                                document = (DocumentRecord) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, document.getId(), DocumentRecord.class);
                                                setProjectIdFromVolume(ticket, document, folder, getVolumeId(target));

                                                tx.commit();
                                            } catch (Exception e) {
                                                tx.rollback();
                                                throw e;
                                            }

                                            added.put(createDocumentRecordJson(getVolumeId(target), document, typeMap));

                                            // Run any triggers
                                            WorkflowEJBLocator.lookupWorkflowManagementBean().runTriggersForDocument(ticket, document);


                                        } catch (Exception upe) {
                                            log("Upload error: " + upe.getMessage());
                                            error = true;
                                            errorArray.put(item.getName());
                                        }
                                    }
                                } else {
                                    throw new Exception("Insufficient storage quota to upload file");
                                }
                            }
                            result.put("added", added);
                        } else {
                            throw new Exception("No target folder");
                        }
                    } else {
                        throw new Exception("No targetid");
                    }

                } else if (cmd.equals("put")) {
                    // Send some data for an edited document
                    ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(target), ServerObject.class);
                    if (obj instanceof DocumentRecord) {
                        DocumentVersion v = StorageUtils.upload(ticket, content.getBytes(), (DocumentRecord) obj, "Edited by file manager");
                        JSONArray changed = new JSONArray();
                        changed.put(createDocumentRecordJson(getVolumeId(target), (DocumentRecord) obj, typeMap));
                        result.put("changed", changed);
                    } else {
                        throw new Exception("Cannot put data into a file that is not a document");
                    }

                } else {
                    log("Unrecognised POST command" + cmd);
                }
                result.write(response.getWriter());

            } else {
                throw new Exception("No command");
            }
        } catch (Exception e) {
            try {
                JSONObject result = new JSONObject();
                result.put("error", "Server Error: " + e.getMessage());
                result.put("errorData", e.toString());
                result.write(response.getWriter());
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /**
     * Create the type mapping
     */
    private Hashtable<String, DocumentType> createTypeMap(Ticket ticket) throws Exception {
        List types = EJBLocator.lookupStorageBean().listDocumentTypes(ticket);
        Hashtable<String, DocumentType> typeMap = new Hashtable<>();
        DocumentType docType;
        for (int i = 0; i < types.size(); i++) {
            docType = (DocumentType) types.get(i);
            typeMap.put(docType.getId(), docType);
        }
        return typeMap;
    }

    /**
     * Create a JSON object for a folder
     */
    private JSONObject createFolderJson(String volumeId, Folder folder) throws Exception {
        JSONObject json = new JSONObject();

        if (folder.getName() != null) {
            json.put("name", folder.getName());
        } else {
            json.put("name", "Unnamed");
        }
        json.put("escid", folder.getId());
        json.put("hash", volumeId + "_" + folder.getId());
        json.put("phash", volumeId + "_" + folder.getContainerId());
        json.put("ts", folder.getCreationDate().getTime() / 1000);
        json.put("mime", "directory");
        json.put("size", 0);
        json.put("read", 1);
        json.put("write", 1);
        json.put("rm", 1);
        json.put("dirs", 1);
        return json;
    }

    /**
     * Create a JSON object for a Dataset
     */
    private JSONObject createDatasetRecordJson(String volumeId, Dataset ds) throws Exception {
        JSONObject json = new JSONObject();

        if (ds.getName() != null) {
            json.put("name", ds.getName());
        } else {
            json.put("name", "Unnamed");
        }


        json.put("mime", "esc/dataset");
            

        json.put("escid", ds.getId());
        json.put("hash", volumeId + "_" + ds.getId());
        json.put("phash", volumeId + "_" + ds.getContainerId());
        json.put("castatus", "-");
        json.put("ts", ds.getCreationDate().getTime() / 1000);
        json.put("size", "0");
        json.put("read", 1);
        json.put("write", 1);
        return json;        
    }
    
    /**
     * Create a JSON Object for a document record
     */
    private JSONObject createDocumentRecordJson(String volumeId, DocumentRecord doc, Hashtable<String, DocumentType> typeMap) throws Exception {
        JSONObject json = new JSONObject();

        if (doc.getName() != null) {
            json.put("name", doc.getName());
        } else {
            json.put("name", "Unnamed");
        }

        if (doc.getDocumentTypeId() != null) {
            if (typeMap.containsKey(doc.getDocumentTypeId())) {
                json.put("mime", typeMap.get(doc.getDocumentTypeId()).getMimeType());
            } else {
                json.put("mime", "application/data");
            }
        } else {
            json.put("mime", "application/data");
        }

        json.put("escid", doc.getId());
        json.put("hash", volumeId + "_" + doc.getId());
        json.put("phash", volumeId + "_" + doc.getContainerId());
        json.put("castatus", doc.getCurrentArchiveStatusAsString());
        if(doc.getCurrentVersionTimestamp() == 0){
            json.put("ts", doc.getCreationDate().getTime() / 1000);
        }
        else{
            json.put("ts", doc.getCurrentVersionTimestamp() / 1000);
        }
        json.put("size", doc.getCurrentVersionSize());
        json.put("read", 1);
        json.put("write", 1);
        return json;
    }

    /**
     * Create a JSON object for the home folder
     */
    private JSONObject createHomeFolderJson(Folder homeFolder) throws Exception {
        JSONObject homeJson = createFolderJson("0", homeFolder);
        homeJson.put("volumeid", "0_");
        homeJson.remove("phash");
        homeJson.put("escobjecttype", "home");
        return homeJson;
    }

    /**
     * Create a JSON object for a volume
     */
    private JSONObject createVolumeJson(Folder volumeFolder, String volumeId) throws Exception {
        JSONObject json = createFolderJson(volumeId, volumeFolder);
        json.put("volumeId", volumeId + "_");
        json.remove("phash");

        if (volumeFolder instanceof SearchFolder) {
            json.put("escobjecttype", "search");
        } else {
            json.put("escobjecttype", "project");
        }
        return json;
    }

    /**
     * Create a list of files from a user
     */
    private JSONArray createUserSharedFolderContentsArray(String userId, Ticket ticket, Hashtable<String, DocumentType> typeMap) throws Exception {
        List documents = EJBLocator.lookupObjectDirectoryBean().getExplicitlySharedObjectsUserHasAccessTo(ticket, userId, DocumentRecord.class, 0, 0);
        JSONArray contents = new JSONArray();
        for (int i = 0; i < documents.size(); i++) {
            contents.put(createDocumentRecordJson("share", (DocumentRecord) documents.get(i), typeMap));
        }
        return contents;
    }

    /**
     * Add the contents of a folder to a JSON array
     */
    private void addFolderContents(Ticket ticket, JSONArray files, String folderId, String volumeId, Hashtable<String, DocumentType> mimeMap) throws Exception {
        Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
        List results = EJBLocator.lookupStorageBean().getAllFolderContents(ticket, folderId);
        ServerObject obj;
        JSONObject json;

        for (int i = 0; i < results.size(); i++) {
            obj = (ServerObject) results.get(i);
            if (obj instanceof Folder) {
                // Create folder
                json = createFolderJson(volumeId, (Folder) obj);
                files.put(json);

            } else if (obj instanceof DocumentRecord) {
                // Create document
                json = createDocumentRecordJson(volumeId, (DocumentRecord) obj, mimeMap);
                files.put(json);
            } else if(obj instanceof Dataset){
                // Create dataset
                json = createDatasetRecordJson(volumeId, (Dataset)obj);
                files.put(json);
            }
        }
    }

    private void addSharedFolderContents(Ticket ticket, JSONArray files, String userId, String volumeId, Hashtable<String, DocumentType> mimeMap) throws Exception {
        List results = EJBLocator.lookupObjectDirectoryBean().getExplicitlySharedObjectsUserHasAccessTo(ticket, userId, DocumentRecord.class, 0, Integer.MAX_VALUE);
        ServerObject obj;
        JSONObject json;

        for (int i = 0; i < results.size(); i++) {
            obj = (ServerObject) results.get(i);
            if (obj instanceof Folder) {
                // Create folder
                json = createFolderJson(volumeId, (Folder) obj);
                files.put(json);

            } else if (obj instanceof DocumentRecord) {
                // Create document
                json = createDocumentRecordJson(volumeId, (DocumentRecord) obj, mimeMap);
                files.put(json);
            } else if(obj instanceof Dataset){
                json = createDatasetRecordJson(volumeId, (Dataset)obj);
                files.put(json);
            }
        }
    }

    /**
     * Create the options object
     */
    private JSONObject createOptions() throws Exception {
        JSONObject options = new JSONObject();
        // Options array

        options.put("path", "");
        options.put("disabled", new JSONArray());

        JSONObject archivers = new JSONObject();
        JSONArray creators = new JSONArray();
        creators.put("application/zip");
        archivers.put("create", creators);
        archivers.put("extract", new JSONArray());

        options.put("archivers", archivers);
        options.put("copyOverwrite", 1);

        return options;
    }

    /**
     * Get an object ID from a target string
     */
    private String getObjectId(String targetParam) {
        int location = targetParam.indexOf("_");
        if (location != -1) {
            return targetParam.substring(location + 1, targetParam.length());
        } else {
            return targetParam;
        }
    }

    /**
     * Get a volume ID from a target string
     */
    private String getVolumeId(String targetParam) {
        int location = targetParam.indexOf("_");
        if (location != -1) {
            return targetParam.substring(0, location);
        } else {
            return targetParam;
        }
    }

    /**
     * Create a tree response
     */
    private JSONArray createTree(Ticket ticket, String folderId, String volumeId) throws Exception {
        Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);

        JSONArray tree = new JSONArray();
        tree.put(createFolderJson(volumeId, folder));
        List children = EJBLocator.lookupStorageBean().getChildFolders(ticket, folderId);
        for (int i = 0; i < children.size(); i++) {
            tree.put(createFolderJson(volumeId, (Folder) children.get(i)));
        }
        return tree;
    }

    /**
     * Is a folder a search folder
     */
    private boolean isSearchFolder(Ticket ticket, String folderId) throws Exception {
        Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
        if (f instanceof SearchFolder) {
            return true;
        } else {
            return false;
        }
    }

    private void setProjectIdFromVolume(Ticket ticket, ServerObject object, Folder container, String volumeId) throws Exception {
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        //If a project id is set on the container, set it on the document
        if (container.getProjectId() != null && !container.getProjectId().isEmpty()) {
            EJBLocator.lookupStorageBean().checkProjectId(ticket, object, container.getProjectId());
        }

        //The user's home folder may come through as 0 or the homefolderId
        if (!volumeId.equals("0") && !volumeId.equals(user.getHomeFolderId())) {

            //If it isn't the home folder, try to get the project
            // container = project data folder
            // container.container = project
            Folder containingFolder = EJBLocator.lookupStorageBean().getFolder(ticket, volumeId);
            if (containingFolder != null && containingFolder.getContainerId() != null) {
                ServerObject isThisAProject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, containingFolder.getContainerId(), ServerObject.class);
                if (isThisAProject instanceof OldProject) {
                    OldProject p = (OldProject) isThisAProject;
                    EJBLocator.lookupStorageBean().checkProjectId(ticket, object, p.getId());
                }
            }

        } else  //make sure that the project id isn't set for the user's home folder
        {
            EJBLocator.lookupStorageBean().checkProjectId(ticket, object, null);
        }
    }
}

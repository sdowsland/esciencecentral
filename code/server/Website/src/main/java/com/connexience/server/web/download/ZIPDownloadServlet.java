/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.web.download;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.ZipFileCreator;
import com.connexience.server.util.ZipUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.ejb.EJBLocalHome;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;

/**
 * This servlet provides a mechanism for downloading data from a folder into a .zip file
 * @author hugo
 */
public class ZIPDownloadServlet extends HttpServlet {
    Logger logger = Logger.getLogger(ZIPDownloadServlet.class);


    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Ticket ticket = SessionUtils.getTicket(request);
        String folderId = request.getParameter("id");
        FileInputStream inStream = null;
        OutputStream outStream = null;
        
        try {
            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
            if(f!=null){
                ZipFileCreator zipper = new ZipFileCreator(ticket);
                zipper.addTopLevelItem(f.getId());
                zipper.prepareTemporaryFile();
                File tempFile = zipper.getTempFile();
                if(tempFile.exists()){
                    response.setContentType("application/zip");
                    response.setContentLength((int)tempFile.length());
                    inStream = new FileInputStream(tempFile);
                    outStream = response.getOutputStream();
                    ZipUtils.copyInputStream(inStream, outStream);
                } else {
                    throw new Exception("Could not create temporary zip file");
                }
                
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (Exception e){
            if(!response.isCommitted()){
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot access data: " + e.getMessage());
            } else {
                logger.error("Error zipping folder: " + e.getMessage());
            }
        } finally {
            if(inStream!=null){try {inStream.close();} catch (Exception ex){}}
            if(outStream!=null){try {outStream.flush();outStream.close();} catch (Exception ex){}}            
        }
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Packages up folders and presents them as a .zip file";
    }
}
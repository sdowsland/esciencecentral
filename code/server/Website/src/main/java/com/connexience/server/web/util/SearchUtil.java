package com.connexience.server.web.util;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Tag;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.model.workflow.WorkflowDocument;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Simon Date: Jun 11, 2009
 */
public class SearchUtil {

    public static List<SearchResult> formatSearchResults(Ticket ticket, List results) throws ConnexienceException {
        List<SearchResult> formattedResults = new ArrayList<>();


        for (Object o : results) {
            try {
                if (o instanceof ServerObject) {
                    ServerObject so = (ServerObject) o;

                    if (so instanceof User) {
                        User u = (User) so;
                        UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, u.getId());

                        //remove any HTML and shorten the description
                        String noHTMLString = up.getText();
                        if (noHTMLString == null || noHTMLString.equals("")) {
                            noHTMLString = u.getDisplayName() + " has no profile information yet.";
                        } else {
                            noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                            if (noHTMLString.length() > 220) {
                                noHTMLString = noHTMLString.substring(0, 219) + "...";
                            }
                        }
                        //Link below has an extra ../ as all other images are relative to styles/theme/images/ and so need to go back up one level
                        formattedResults.add(new SearchResult(u.getDisplayName(), "../../pages/profile/profile.jsp?id=" + so.getId(), noHTMLString, "../../../servlets/image?soid=" + u.getProfileId() + "&type=" + ImageData.SMALL_PROFILE, u.getDisplayName()));
                    } else if (so instanceof Group) {
                        Group g = (Group) so;

                        //remove any HTML and shorten the description
                        String noHTMLString = g.getDescription();
                        if (noHTMLString == null || noHTMLString.equals("")) {
                            noHTMLString = g.getName() + " has no description yet.";
                        } else {
                            noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                            if (noHTMLString.length() > 220) {
                                noHTMLString = noHTMLString.substring(0, 219) + "...";
                            }
                        }

                        formattedResults.add(new SearchResult(g.getName(), "../../pages/groups/groups.jsp?groupId=" + g.getId(), noHTMLString, "search/group.png", g.getName()));
                    } else if (so instanceof WorkflowDocument) {
                        WorkflowDocument w = (WorkflowDocument) so;

                        //remove any HTML and shorten the description
                        String noHTMLString = w.getDescription();

                        if (noHTMLString == null || noHTMLString.equals("")) {
                            noHTMLString = "";
                        } else {
                            noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                            if (noHTMLString.length() > 220) {
                                noHTMLString = noHTMLString.substring(0, 219) + "...";
                            }
                        }

                        formattedResults.add(new SearchResult(w.getName(), "../../pages/workflow/wfeditor.jsp?id=" + w.getId(), noHTMLString, "search/workflow.png", w.getName()));
                    } else if (so instanceof DocumentRecord) {
                        //remove any HTML and shorten the description
                        String noHTMLString = so.getDescription();

                        if (noHTMLString == null || noHTMLString.equals("")) {
                            noHTMLString = "";
                        } else {
                            noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                            if (noHTMLString.length() > 220) {
                                noHTMLString = noHTMLString.substring(0, 219) + "...";
                            }
                        }
                        //todo: quickview
                        formattedResults.add(new SearchResult(so.getName(), "", noHTMLString, "search/doc.png", so.getName(), "qv.showFile('" + so.getId() + "', '" + so.getName() + "');"));
                    } else if (so instanceof Folder) {
                        //remove any HTML and shorten the description
                        String noHTMLString = so.getDescription();

                        if (noHTMLString == null || noHTMLString.equals("")) {
                            noHTMLString = "";
                        } else {
                            noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                            if (noHTMLString.length() > 220) {
                                noHTMLString = noHTMLString.substring(0, 219) + "...";
                            }
                        }

                        formattedResults.add(new SearchResult(so.getName(), "../../pages/front/data.jsp?rootid=" + so.getId(), noHTMLString, "search/folder.png", so.getName()));
                    } else if (so instanceof TextMessage) {
                        TextMessage tm = (TextMessage) so;
                        //remove any HTML and shorten the description
                        String noHTMLString = tm.getDescription();

                        if (noHTMLString == null || noHTMLString.equals("")) {
                            noHTMLString = "";
                        } else {
                            noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                            if (noHTMLString.length() > 220) {
                                noHTMLString = noHTMLString.substring(0, 219) + "...";
                            }
                        }
                        String title = "No Title";
                        if (!tm.getTitle().equals("")) {
                            title = tm.getTitle();
                        }
                        formattedResults.add(new SearchResult(title, "../../pages/messages/viewmessage.jsp?id=" + tm.getThreadId(), noHTMLString, "search/message.png", title));
                    }
                } else if (o instanceof Tag) {
                    Tag t = (Tag) o;

                    //todo: number of items tagged with this

                    int numResults = EJBLocator.lookupSearchBean().countTagSearch(ticket, t.getTagText());
                    String noHTMLString = numResults + " items tagged with " + t.getTagText();

                    formattedResults.add(new SearchResult(t.getTagText(), "../../pages/search/tagsearch.jsp?t=" + t.getTagText(), noHTMLString, "search/tag.png", t.getTagText()));

                } else if (o instanceof UserProfile) {


                    UserProfile up = (UserProfile) o;
                    User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, up.getId());


                    //remove any HTML and shorten the description
                    String noHTMLString = up.getText();
                    if (noHTMLString == null || noHTMLString.equals("")) {
                        noHTMLString = u.getDisplayName() + " has no profile information yet.";
                    } else {
                        noHTMLString = noHTMLString.replaceAll("\\<.*?>", "");
                        if (noHTMLString.length() > 220) {
                            noHTMLString = noHTMLString.substring(0, 219) + "...";
                        }
                    }
                    //Link below has an extra ../ as all other images are relative to styles/theme/images/ and so need to go back up one level
                    formattedResults.add(new SearchResult(u.getDisplayName(), "../../pages/profile/profile.jsp?id=" + up.getId(), noHTMLString, "../../../servlets/image?soid=" + u.getProfileId() + "&type=" + ImageData.SMALL_PROFILE, u.getDisplayName()));
                }
            } catch (ConnexienceException e) {
                e.printStackTrace();
            }
        }
        return formattedResults;
    }

}
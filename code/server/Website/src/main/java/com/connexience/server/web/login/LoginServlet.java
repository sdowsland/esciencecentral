package com.connexience.server.web.login;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.LogonEvent;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.ExternalLogonDetails;
import com.connexience.server.model.security.LogonDetails;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;

/**
 * Author: Simon Date: Mar 9, 2010
 */
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        try {
            //todo: change paths in this file
            String contextPath = request.getContextPath();
            String openIdUserName = request.getParameter("openid_identifier");  //when submitted from login form comes as parameter
            String username = request.getParameter("j_username");
            String password = request.getParameter("j_password");
            String rememberMeParam = request.getParameter("rememberMe");
            String isReturnFromOpenId = request.getParameter("is_return");
            String isReturnFromLinkAccounts = request.getParameter("linkAccounts");
            String isReturnFromCreateAccount = request.getParameter("isReturnFromCreateAccount");

            //openId returns parameters as request attributes
            String openId = (String) request.getAttribute("identifier");
            String email = (String) request.getAttribute("email");
            String fullname = (String) request.getAttribute("fullname");

            if (fullname == null) {
                String firstName = (String) request.getAttribute("firstName");
                String lastName = (String) request.getAttribute("lastName");
                if (firstName != null && lastName != null) {
                    fullname = firstName + " " + lastName;
                }

        //if the name is still null
                //loop over all of the attributes looking for firstname or lastname
                if (fullname == null) {
                    Enumeration attrNames = request.getAttributeNames();
                    while (attrNames.hasMoreElements()) {
                        String attrName = (String) attrNames.nextElement();
                        if (attrName.toLowerCase().contains("openid.ax.value.firstname.1")) {
                            firstName = (String) request.getAttribute(attrName);
                        } else if (attrName.toLowerCase().contains("openid.ax.value.lastname.1")) {
                            lastName = (String) request.getAttribute(attrName);
                        }
                    }
                    if (firstName != null && lastName != null) {
                        fullname = firstName + " " + lastName;
                    }
                }
            }

            //return from OpenId Servlet following a successful OpenId login
            if ("true".equals(isReturnFromOpenId)) {
                if (openId != null && !openId.equals("")) {
                    //Do we have an external login for this user?
                    ExternalLogonDetails extLogon = EJBLocator.lookupTicketBean().getExternalLogon(openId);
                    if (extLogon != null) {
                        SessionUtils.login(request, extLogon.getUserId());
                    } else if (email != null && !email.equals("")) //do we have a user with a username or email with this address on the system?
                    {
                        User existingUser = EJBLocator.lookupUserDirectoryBean().getUserFromEmailAddress(email);
                        if (existingUser != null) {
                            session.setAttribute("openId", openId);
                            session.setAttribute("email", email);
                            session.setAttribute("linkUserId", existingUser.getId());
                            session.setAttribute("fullname", fullname);
                            response.sendRedirect(contextPath + "/signup/linkAccounts.jsp");
                            return;
                        } else {
                            if (fullname != null && !fullname.equals("")) //try to create account
                            {
                                createAccount(request, fullname, email, openId);
                            } else {
                                //email but no name
                                session.setAttribute("openId", openId);
                                response.sendRedirect(contextPath + "/signup/createFromOpenID.jsp?email=" + email);
                                return;
                            }
                        }
                    } else if (fullname != null && !fullname.equals("")) //try to create account
                    {
                        createAccount(request, fullname, email, openId);
                    } else {
                        session.setAttribute("openId", openId);
                        //no email or name
                        response.sendRedirect(contextPath + "/signup/createFromOpenID.jsp");
                        return;
                    }
                } else {
                    response.sendRedirect("../jsp/index.jsp");
                    throw new ConnexienceException("Invalid return from OpenID Provider");
                }
            } else if (isReturnFromLinkAccounts != null && !isReturnFromLinkAccounts.equals("")) {
                if (isReturnFromLinkAccounts.equals("true")) {
                    String linkOpenId = request.getParameter("openId");
                    String linkUserId = request.getParameter("linkUserId");

                    try {
                        EJBLocator.lookupUserDirectoryBean().authenticateUserFromId(linkUserId, password);
            //no exception indicates success

                        //link accounts - create new External Logon
                        if (linkOpenId != null && !linkOpenId.equals("") && linkUserId != null && !linkUserId.equals("")) {
                            ExternalLogonDetails extDetails = EJBLocator.lookupTicketBean().addExternalLogon(linkUserId, linkOpenId);
                            SessionUtils.login(request, extDetails.getUserId());
                            cleanSession(session);
                        }
                    } catch (ConnexienceException e) {
                        e.printStackTrace();
                        session.setAttribute("error", "wrongPass");
                        response.sendRedirect(contextPath + "/signup/linkAccounts.jsp");
                        return;
                    }
                } else {
                    //create new account
                    if (fullname == null) {
                        fullname = request.getParameter("fullname");
                    }
                    if (email == null) {
                        email = request.getParameter("email");
                    }
                    if (openId == null) {
                        openId = request.getParameter("openId");
                    }

                    createAccount(request, fullname, email, openId);
                    cleanSession(session);
                }
            } else if (isReturnFromCreateAccount != null && isReturnFromCreateAccount.equals("true")) {
                String firstname = request.getParameter("firstname");
                String lastname = request.getParameter("lastname");
                email = request.getParameter("email");
                if (firstname != null && !firstname.equals("") && lastname != null && !lastname.equals("")) {
                    createAccount(request, firstname, lastname, email, (String) session.getAttribute("openId"));
                    cleanSession(session);
                } else {
                    //no names
                    response.sendRedirect(contextPath + "/signup/createFromOpenID.jsp");
                    return;
                }
            } else {
                String provider = request.getParameter("provider");
                //try to login with username and password
                if (username != null && password != null && !username.equals("") && !password.equals("")) {
                    // Login with e-SC login engine. Need to do the password expiry / account lock logic here
                    LogonDetails details = EJBLocator.lookupTicketBean().getLogonByLogonName(username);
                    if(details!=null){
                        if(details.isEnabled()){
                            boolean loggedIn = SessionUtils.login(request, username, password); //will throw an exception if it can't login

                            if(!loggedIn){
                                // Not logged in - need to increment failed counter
                                details.setFailureCount(details.getFailureCount() + 1);
                                details.setLastLogonAttempt(new Date());
                                details.setLastFailedLogon(new Date());

                                details = EJBLocator.lookupTicketBean().saveLogon(details);

                                // Should we apply lockout logic
                                if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "LockoutAfterNFailedAttempts", false)){
                                    if(details.getFailureCount()>=EJBLocator.lookupPreferencesBean().intValue("Security", "LockoutThreshold", 5)){
                                        details.setEnabled(false);
                                        details.setLockReason(LogonDetails.LOCKED_DUE_TO_FAILED_LOGONS);
                                        details.setLockTime(new Date());
                                        details = EJBLocator.lookupTicketBean().saveLogon(details);
                                        session.setAttribute("logonFailReason", "Account is locked - too many failed logon attempts.");
                                    }
                                }
                            } else {
                                details.setFailureCount(0);
                                details.setLastLogonAttempt(new Date());
                                details.setLastSucessfulLogon(new Date());
                                session.removeAttribute("logonExpired");
                                session.removeAttribute("logonExpiredUserId");
                                session.removeAttribute("logonFailReason");

                                details = EJBLocator.lookupTicketBean().saveLogon(details);
                                LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, username, WebUtil.getClientIP(request));
                                ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                                client.log(logonEvent);
                            }
                        } else {

                            // Set reasons for lockout
                            if(details.getLockReason().equals(LogonDetails.LOCKED_DUE_TO_PASSWORD_EXPIRY)){
                                // Can the old password be validated?
                                try {
                                    User tempUser = EJBLocator.lookupUserDirectoryBean().authenticateUser(username, password);
                                    if(tempUser!=null){
                                        // The password was correct, so need to trigger the reset procedure
                                        session.setAttribute("logonFailReason", "Account is locked");
                                        session.setAttribute("logonExpired", true);
                                        session.setAttribute("logonExpiredUserId", tempUser.getId());
                                    } else {
                                        session.removeAttribute("logonFailReason");
                                        session.removeAttribute("logonExpired");
                                        session.removeAttribute("logonExpiredUserId");
                                    }

                                } catch (Exception e){
                                    session.removeAttribute("logonFailReason");
                                    session.removeAttribute("logonExpired");
                                    session.removeAttribute("logonExpiredUserId");

                                }
                            } else if(details.getLockReason().equals(LogonDetails.LOCKED_DUE_TO_INACTIVITY)){
                                // Inactivity
                                try {
                                    User tempUser = EJBLocator.lookupUserDirectoryBean().authenticateUser(username, password);
                                    if(tempUser!=null){
                                        // Password was correct, can send the actual reason
                                        session.setAttribute("logonFailReason", "Account is locked due to inactivity");
                                    }
                                } catch (Exception e){
                                    // Exception authenticating
                                    session.removeAttribute("logonExpired");
                                    session.removeAttribute("logonExpiredUserId");
                                }
                            }
                        }
                    } else {
                        // Standard logon login
                        SessionUtils.login(request, username, password); //will throw an exception if it can't login
                    }
                    
                } //try to login with OpenId
                else if ((openIdUserName != null && !openIdUserName.equals("")) || provider != null && (provider.equals("google") || provider.equals("yahoo"))) {
                    try {
                        this.getServletContext().getRequestDispatcher("/servlets/openid").forward(request, response);
                        return;
                    } catch (Exception e) {
                        response.sendRedirect("../jsp/index.jsp?error=openIdError");
                    }
                }
            }

            Ticket ticket = (Ticket) session.getAttribute("TICKET");
            //successful login if the user isn't public
            Boolean loggedIn = false;
            User u = (User) session.getAttribute("USER");
            if (!u.getId().equals(EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket).getDefaultUserId())) {
                loggedIn = true;
            }


            /* REMEMBER ME*/
            if (ticket != null) {
                String publicUserId = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket).getDefaultUserId();
                if (!ticket.getUserId().equals(publicUserId)) {
                    if (rememberMeParam != null && rememberMeParam.equals("true")) {
                        String uuid = EJBLocator.lookupTicketBean().addRememberMe(ticket);
                        Cookie c = new Cookie("rememberMe", uuid);
                        c.setMaxAge(60 * 60 * 24 * 365); // 1 year
                        c.setPath(request.getContextPath()); //add the cookie for all directories, not just login
                        response.addCookie(c);
                    }
                }
            }

            if (!loggedIn) {
                session.setAttribute("logonFail", "true");
                session.setAttribute("username", username);
            }

            String referrerPage = contextPath + "/pages/front/index.jsp";

            /* forward the user to the correct page */
            if (session.getAttribute("nextPage") != null) {
                referrerPage = session.getAttribute("nextPage").toString();
            }

            response.sendRedirect(referrerPage);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void cleanSession(HttpSession session) {
        session.removeAttribute("openId");
        session.removeAttribute("email");
        session.removeAttribute("fullname");
        session.removeAttribute("linkUserId");
        session.removeAttribute("error");
    }

    private User createAccount(HttpServletRequest request, String firstname, String lastname, String email, String openId) throws ConnexienceException {
        User user = new User();
        user.setFirstName(firstname);
        user.setSurname(lastname);

        //create the user
        String defaultDomain = "http://" + request.getServerName() + request.getContextPath() + "/";
        user = EJBLocator.lookupUserDirectoryBean().createAccountForExternalLogon(user, openId, defaultDomain);

        //log the user in
        SessionUtils.login(request, user.getId());
        LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, user.getDisplayName(), WebUtil.getClientIP(request));
        ProvenanceLoggerClient client = new ProvenanceLoggerClient();
        client.log(logonEvent);

        if (email == null) {
            email = "";
        }

        //get the ticket so that we can set the email address in the profile
        Ticket ticket = (Ticket) request.getSession().getAttribute("TICKET");
        UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, user.getId());
        if (up != null) {
            up.setEmailAddress(email);
            EJBLocator.lookupUserDirectoryBean().saveUserProfile(ticket, user.getId(), up);
        }

        return user;
    }

    private User createAccount(HttpServletRequest request, String fullname, String email, String openId) throws ConnexienceException {
        String firstName = "";
        String surname = "";

        String[] fullNameSplit = fullname.split(" ");
        if (fullNameSplit.length == 1) {
            //the name is all the surname
            surname = fullNameSplit[0];
        } else if (fullNameSplit.length == 2) {
            //the name is two parts
            firstName = fullNameSplit[0];
            surname = fullNameSplit[1];
        } else {
            //the first name is up to the first space
            firstName = fullNameSplit[0];
            surname = fullname.substring(fullname.indexOf(" "));
        }

        return createAccount(request, firstName, surname, email, openId);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

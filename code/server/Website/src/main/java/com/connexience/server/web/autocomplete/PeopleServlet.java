package com.connexience.server.web.autocomplete;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * Author: Simon
 * Date: Oct 5, 2010
 * <p/>
 * This servlet returns a list of items in JSON format, for instance the friends of a person.
 * Id and Name of the object are returned.
 * <p/>
 * It expects URLs of the form /people/[id]/friends
 * or /people/[id]/groups
 */
public class PeopleServlet extends HttpServlet
{

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    Ticket ticket = SessionUtils.getTicket(request);
    PrintWriter writer = response.getWriter();
    try
    {
      String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
      String userId = sections[0];
      if (ticket != null)
      {

        if (sections.length > 1)
        {
          String type = sections[1];

          if (type.equals("friends"))
          {
            JSONArray results = new JSONArray();

            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
            @SuppressWarnings("unchecked")
            Collection<User> friends = EJBLocator.lookupLinkBean().getLinkedSourceObjects(ticket, user, User.class);
            for (User friend : friends)
            {
              JSONObject result = new JSONObject();
              result.put("label", friend.getDisplayName());
              result.put("id", friend.getId());
              results.put(result);
            }
            results.write(writer);
          }
          else if (type.equals("groups"))
          {
            JSONArray results = new JSONArray();

            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId);
            @SuppressWarnings("unchecked")
            Collection<Group> groups = EJBLocator.lookupUserDirectoryBean().listUserGroups(ticket, user.getId());
            for (Group group : groups)
            {
              JSONObject result = new JSONObject();
              result.put("label", group.getDisplayName());
              result.put("id", group.getId());
              results.put(result);
            }
            results.write(writer);
          }
          else
          {
            return;
          }
        }
        else
        {
          return;
        }
      }
      else
      {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        System.out.println("Ticket is null in call to PeopleServlet");
      }
    }
    catch (Exception e)
    {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
      e.printStackTrace();
    }
    writer.flush();

  }
}

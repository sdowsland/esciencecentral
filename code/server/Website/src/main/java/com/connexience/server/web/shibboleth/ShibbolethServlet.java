/*
 * ShibbolethServlet.java
 */
package com.connexience.server.web.shibboleth;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.LogonEvent;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.ExternalLogonDetails;
import com.connexience.server.model.security.User;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.util.FlashUtils;
import com.connexience.server.web.util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * This servlet deals with Shibboleth authentication (and implicit registration
 * if the authenticated shibboleth user is here for the first time).
 * 
 * @author ndjm
 */
public class ShibbolethServlet extends HttpServlet
{
	// Provider for ExternalLoginDetails
	private final String SHIB_PROVIDER = "SHIBBOLETH";
	
	private final String SHIB_EMAIL = "HTTP_SHIB_EMAILADDRESS";
	private final String SHIB_GIVEN_NAME = "HTTP_SHIB_PN_GIVENNAME";
	private final String SHIB_SURNAME = "HTTP_SHIB_PN_SN";
	
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		final List<String> headerNames = Collections.list(request.getHeaderNames());
		
		// Sanitise header names (_.toUpperCase())
		for (int i = 0; i < headerNames.size(); i++)
			headerNames.set(i, headerNames.get(i).toUpperCase());
		
		// Required HTTP headers (redirect with information if any are missing)
		final String[] scanHeaders = new String[] {
		SHIB_EMAIL, SHIB_GIVEN_NAME, SHIB_SURNAME
		};
		
		// Scan for the headers
		for (final String headerName : scanHeaders)
		{
			if (!headerNames.contains(headerName))
			{
				FlashUtils.setInfo(request, String.format("Shibboleth Exception, '%s' HTTP header was not present.", headerName));
				response.sendRedirect("../pages/front/index.jsp?needToLogin=true");
				return;
			}
		}
		
		// Check for existence of eSC accounts using email addresses
		final String email = request.getHeader(SHIB_EMAIL).replaceFirst("@newcastle\\.ac\\.uk$", "@ncl.ac.uk");
		final String altEmail = email.replaceFirst("@ncl\\.ac\\.uk$", "@newcastle.ac.uk");
		
		try
		{
			// Try with ncl.ac.uk
			ExternalLogonDetails external = EJBLocator.lookupTicketBean().getExternalLogon(email, SHIB_PROVIDER);
			
			// Try with newcastle.ac.uk (if null)
			if (external == null)
			{
				external = EJBLocator.lookupTicketBean().getExternalLogon(altEmail, SHIB_PROVIDER);
			}
			
			// Found ELD, proceed to create ticket
			if (external != null)
			{
				SessionUtils.login(request, external.getUserId());
                LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, ((User)request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
                ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                client.log(logonEvent);
				response.sendRedirect("../pages/front/index.jsp");
				return;
			}
			else
			{
				// Get original URL for creating ELD
				String originalURL = WebUtil.getHostname(request) + request.getContextPath();
				
				// Check for user with ncl.ac.uk email
				User user = EJBLocator.lookupUserDirectoryBean().getUserFromEmailAddress(email);
				
				// Check for newcastle.ac.uk email if no user found
				if (user == null)
				{
					user = EJBLocator.lookupUserDirectoryBean().getUserFromEmailAddress(altEmail);
				}
				
				// If user still null, register and create ELD.
				if (user == null)
				{
					user = new User();
					// Create the user
					user.setFirstName(request.getHeader(SHIB_GIVEN_NAME));
					user.setSurname(request.getHeader(SHIB_SURNAME));
					user.setName(request.getHeader(SHIB_EMAIL));
					
					// ConnexienceException thrown if user cannot be registered
					user = EJBLocator.lookupUserDirectoryBean().createAccountForExternalLogon(user, request.getHeader(SHIB_EMAIL), originalURL, SHIB_PROVIDER);
					
					// Login
					SessionUtils.login(request, user.getId());
                    LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, ((User)request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
                    ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                    client.log(logonEvent);
				}
				else
				{
					// Create ELD for existing User
					external = EJBLocator.lookupTicketBean().addExternalLogon(user.getId(), user.getName(), SHIB_PROVIDER);
					
					// Login
					SessionUtils.login(request, external.getUserId());
                    LogonEvent logonEvent = new LogonEvent(LogonEvent.WEBSITE_LOGON, ((User)request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
                    ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                    client.log(logonEvent);
				}
				
				response.sendRedirect("../pages/front/index.jsp");
				return;
			}
		}
		catch (ConnexienceException e)
		{
			// TODO: Proper way to report this?
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo()
	{
		return "Shibboleth Authentication Servlet";
	}
}

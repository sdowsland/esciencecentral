/*
 * APIUtils.java
 */

package com.connexience.server.web;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.NameCache;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class provides some basic JSON utility methods for the API servlets.
 *
 * @author hugo
 */
public class APIUtils {
    public APIUtils() {
    }


    /**
     * Parse the URL into blocks
     *
     * @param path The request path for the servlet
     * @return The path split on '/' and returned as a String[]
     */
    public static String[] splitRequestPath(String path) {
        if (path.startsWith("/")) {
            // Split leading /
            path = path.substring(1);
        }
        return path.split("/");
    }


    /**
     * Set an OK flag in the result object
     */
    public static void setSuccess(JSONObject resultsObject) {
        try {
            resultsObject.put("error", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set an error in the result object
     */
    public static void setError(JSONObject resultObject, String errorMessage) {
        try {
            resultObject.put("error", true);
            resultObject.put("message", errorMessage);
        } catch (Exception e) {
        }
    }

    /**
     * Populate a JSON Object with exception data
     */
    public static void populateExceptionData(JSONObject resultObject, Exception e) {
        try {
            resultObject.put("error", true);
            resultObject.put("message", e.getMessage());
        } catch (Exception ex) {
        }
    }

    /**
     * Create a correctly formatted JSON result object with the proper field
     */
    public static JSONObject createResultsObject() {
        return null;
    }

    /*
    * Create a JSON object representing a file within the system.
    * */
    public static JSONObject createJSONFileObject(String name, String id, boolean readOnly, boolean editable, boolean getInfo) {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
            json.put("id", id);
            json.put("readOnly", readOnly);
            json.put("editable", editable);
            json.put("info", getInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    /*
    * Create a JSON object representing a file with a hyperlink within the system.
    * */
    public static JSONObject createJSONFileObject(String name, String id, String link, boolean readOnly, boolean editable, boolean getInfo) {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
            json.put("id", id);
            json.put("readOnly", readOnly);
            json.put("link", link);
            json.put("editable", editable);
            json.put("info", getInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }


    /**
     * Extract a String from an InputStream
     */
    public static String extractString(InputStream stream) throws Exception {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        boolean finished = false;
        int value;

        while (finished == false) {
            value = stream.read();
            if (value == -1) {
                finished = true;
            } else {
                array.write(value);
            }
        }
        return new String(array.toByteArray());
    }


    public static Date parseDate(String strDate) {
        //we're expecting "Monday, 27th October 2008"
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        try {
            return sdf.parse(strDate);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static String formatDate(Date date) {
        //we're expecting "Monday, 27th October 2008"
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        return sdf.format(date);
    }

    public static String getObjectNameFromCache(Ticket ticket, String objectId, HttpSession session) throws ConnexienceException {
        NameCache cache;
        if (session.getAttribute("NAMECACHE") == null) {
            cache = new NameCache(1000);
            session.setAttribute("NAMECACHE", cache);
        } else {
            cache = (NameCache) session.getAttribute("NAMECACHE");
        }
        return cache.getObjectName(ticket, objectId);
    }

    public static void evictObjectFromNameCache(String objectId, HttpSession session) {
        NameCache cache;
        if (session.getAttribute("NAMECACHE") == null) {
            cache = new NameCache(1000);
            session.setAttribute("NAMECACHE", cache);
        } else {
            cache = (NameCache) session.getAttribute("NAMECACHE");
        }
        cache.evictId(objectId);
    }

    /**
     * Create bare minimum JSON for a ServerObject
     */
    public static JSONObject createServerObjectJson(ServerObject obj) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", obj.getId());
        json.put("name", obj.getName());
        json.put("description", obj.getDescription());
        json.put("creatorId", obj.getCreatorId());
        if (obj.getProjectId() != null && !obj.getProjectId().isEmpty()) {
            json.put("projectId", obj.getProjectId());
        }
        return json;
    }

    public static JSONObject createProjectJson(Project p) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", p.getId());
        json.put("name", p.getName());
        json.put("description", p.getDescription());
        json.put("creatorId", p.getOwnerId());
        json.put("projectId", p.getId());

        return json;
    }


}






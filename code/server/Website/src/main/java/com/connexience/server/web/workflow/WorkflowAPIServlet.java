/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.workflow;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.image.ImageData;
import com.connexience.provenance.model.logging.events.WorkflowSaveOperation;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.workflow.BlockHelpHtml;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowFolderTrigger;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.model.workflow.WorkflowServiceLog;
import com.connexience.server.model.workflow.WorkflowTerminationItem;
import com.connexience.server.model.workflow.WorkflowTerminationReport;
import com.connexience.server.model.workflow.control.IWorkflowDebugClient;
import com.connexience.server.model.workflow.control.IWorkflowEngineControl;
import com.connexience.server.model.workflow.control.WorkflowDebugClientList;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.model.workflow.notification.WorkflowLockMember;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.util.WorkflowUtils;
import com.connexience.server.web.APIUtils;
import com.connexience.server.workflow.json.JSONBlockCreator;
import com.connexience.server.workflow.json.JSONDrawingExporter;
import com.connexience.server.workflow.json.JSONDrawingImporter;
import com.connexience.server.workflow.json.JSONPaletteCreator;
import com.connexience.server.workflow.util.InvocationReportBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.drawing.BlockExecutionReport;
import org.pipeline.core.drawing.DrawingModel;
import org.pipeline.core.drawing.gui.DefaultDrawingRenderer;
import org.pipeline.core.drawing.model.DefaultDrawingModel;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.io.XmlDataStoreByteArrayIO;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamReader;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * This class provides an API for executing and managing workflows
 * @author hugo
 */
public class WorkflowAPIServlet extends HttpServlet {
    private static final WorkflowDebugClientList debugClients = new WorkflowDebugClientList();
    private static Logger logger = Logger.getLogger(WorkflowAPIServlet.class);
    
    DateFormat format = DateFormat.getDateTimeInstance();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //header to stop IE caching GET requests as we're not doing 100% pure REST
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    }

                    //Get the contents of a directory
                    if (method.equalsIgnoreCase("deleteWorkflow")) {
                        // Remove a workflow
                        deleteWorkflow(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("deleteInvocation")) {
                        // Remove an invocation folder
                        deleteInvocation(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("listWorkflows")) {
                        // List the workflows
                    } else if (method.equalsIgnoreCase("getWorkflowJson")) {
                        getWorkflowJson(ticket, dataObject, resultObject);
                    } else if (method.equalsIgnoreCase("putWorkflowJson")) {
                        putWorkflowJson(ticket, dataObject, resultObject);
                    } else if (method.equalsIgnoreCase("getPalette")) {
                        // Get the entire palette
                        getPaletteJson(ticket, dataObject, resultObject);
                    } else if (method.equalsIgnoreCase("getHierarchicalPalette")) {
						// Get the entire hierarchical palette of blocks
						getHierarchicalPaletteJson(ticket, dataObject, resultObject);
					} else if (method.equalsIgnoreCase("getServiceJson")) {
                        // Get data for a service
                        getServiceJson(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("fetchInvocationWorkflowData")) {
                        // Get the drawing data for an invocation
                        fetchInvocationWorkflowData(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("fetchReports")) {
                        // Get invocation reports
                        fetchReports(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("listInvocations")) {
                        // Get the workflow runs
                        listInvocations(ticket, dataObject, resultObject, session);

                    } else if (method.equalsIgnoreCase("listInvocationFiles")) {
                        // Get the contents of an invocation directory
                        listInvocationFiles(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("executeWorkflow")) {
                        // Run a workflow
                        executeWorkflow(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("fetchLockSummary")){
                        // Get details of a lock
                        fetchLockSummary(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("rerunFailedInLock")){
                        // Resubmit the failed invocations
                        rerunFailedInLock(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("proceedWithLock")){
                        // Continue running a lock
                        proceedWithLock(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("terminateLock")){
                        // Kill a lock
                        terminateLock(ticket, dataObject, resultObject);
                        
                    } else if (method.equalsIgnoreCase("fetchBlockExecutionOutput")) {
                        // Fetch execution system.out
                        fetchBlockExecutionOutput(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("listWorkflowVersions")) {
                        // List the versions of a workflow
                        listWorkflowVersions(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("getWorkflowServiceHelp")) {
                        // Get the help data for a service
                        getWorkflowServiceHelp(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("listServiceVersions")) {
                        // List all of the versions of a service
                        listServiceVersions(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("getWorkflowInvocation")) {
                        // Get a workflow invocation
                        getWorkflowInvocation(ticket, dataObject, resultObject, session);

                    } else if (method.equalsIgnoreCase("terminateInvocation")) {
                        // Terminate a running invocation
                        terminateInvocation(ticket, dataObject, resultObject);

                    } else if (method.equalsIgnoreCase("terminateAllInvocationsOfWorkflow")){
                        // Terminate every instance of a workflow
                        terminateAllInvocationsOfWorkflow(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getInvocationTable")){
                        // Get a table of all of the invocations
                        getInvocationTable(ticket, dataObject, resultObject, session);

                    } else if(method.equalsIgnoreCase("copyWorkflow")){
                        // Copy a workflow document
                        copyWorkflow(ticket, dataObject, resultObject);
                        
                    } else if (method.equalsIgnoreCase("getCategories")) {
                        // get the categories
                        
                    } else if(method.equalsIgnoreCase("sendDebugCommand")){
                        // Send a command to a debugger
                        sendDebugCommand(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getLastDebugBuffer")){
                        // Get the last debug buffer content
                        getLastDebugBuffer(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("closeDebugger")){
                        // Close a debug connection
                        closeDebugger(ticket, dataObject, resultObject);
                    } else if(method.equalsIgnoreCase("getInvocationCounts")){
                        // Get the number of running invocations for a set of workflows
                        getInvocationCounts(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("createTrigger")){
                        // Create a folder trigger
                        createTrigger(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("deleteTrigger")){
                        // Delete a folder trigger
                        deleteTrigger(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listServerObjects")){
                        // List the server objects of a certain class that the user has access to
                        listServerObjects(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listProjectObjects")){
                        // List all of the projects the user owns or is a member of
                        listProjectObjects(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getWorkflowTrigger")){
                        // Get a trigger object
                        getWorkflowTrigger(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("saveWorkflowTrigger")){
                        // Save a trigger object
                        saveWorkflowTrigger(ticket, dataObject, resultObject);
                    } else if(method.equalsIgnoreCase("reRun")){
                        reExecuteWorkflow(ticket, dataObject, resultObject);
                        
                    } else if (method.equalsIgnoreCase("getServices")) {
                        // get the categories
                        String responseText = "<ul>\n"
                                + "        <li class=\"draggable\" id=\"s1\">Service 1</li>\n"
                                + "        <li class=\"draggable\" id=\"s2\">Service 2</li>\n"
                                + "        <li class=\"draggable\" id=\"s3\">Service 3</li>\n"
                                + "      </ul>";

                        out.write(responseText);
                        
                    } else if(method.equalsIgnoreCase("fetchLiveBlockOutput")){
                        // Fetch the stdout from a running block
                        fetchLiveBlockOutput(ticket, dataObject, resultObject);
                    }

                }
            } else {
                APIUtils.setError(resultObject, "No user logged in");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);

        } finally {
            try {
                resultObject.write(out);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            out.close();
        }
    }


    private void fetchLiveBlockOutput(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String invocationId = dataObject.getString("invocationId");
            String contextId = dataObject.getString("contextId");
            
            WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);

            if(invocation!=null){
                if(invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_RUNNING || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER){
                    // Potentially need live output
                    if(invocation.getCurrentBlockId().equals(contextId)){
                        // Block is still running 
                        IWorkflowEngineControl control = WorkflowUtils.connectToWorkflowEngine(ticket, invocation);
                        String results = new String(control.fetchStdOut(invocationId, contextId));
                        addServiceLogDataJsonToResult("Output", results, invocationId, contextId, resultObject);
                        resultObject.put("liveOutput", true);
                        APIUtils.setSuccess(resultObject);
                    
                    } else {
                        // Block possibly finished - check database
                        WorkflowServiceLog log = WorkflowEJBLocator.lookupWorkflowManagementBean().getServiceLog(ticket, invocationId, contextId);
                        if(log!=null){
                            addServiceLogJsonToResult(log, resultObject);                        
                            resultObject.put("liveOutput", false);
                        } else {
                            addServiceLogDataJsonToResult("Error", "No console output available for block", invocationId, contextId, resultObject);
                            resultObject.put("liveOutput", false);
                        }
                    }
                    
                } else {
                    // Return stored output
                    WorkflowServiceLog log = WorkflowEJBLocator.lookupWorkflowManagementBean().getServiceLog(ticket, invocationId, contextId);
                    if(log!=null){
                        addServiceLogJsonToResult(log, resultObject);      
                        resultObject.put("liveOutput", false);
                    } else {
                        addServiceLogDataJsonToResult("Error", "No console output available for block", invocationId, contextId, resultObject);
                        resultObject.put("liveOutput", false);
                    }
                }
                APIUtils.setSuccess(resultObject);
                
            } else {
                APIUtils.setError(resultObject, "No such invocation");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void getInvocationCounts(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONArray workflowIds = dataObject.getJSONArray("workflowIds");
            JSONArray results = new JSONArray();
            JSONObject result;
            int count;
            
            String id;
            for(int i=0;i<workflowIds.length();i++){
                id = workflowIds.getString(i);
                try {
                    count = WorkflowEJBLocator.lookupWorkflowManagementBean().getNumberOfInvocations(ticket, id);
                    result = new JSONObject();
                    result.put("workflowId", id);
                    result.put("count", count);
                    results.put(result);
                } catch (Exception e){
                    
                }
            }
            resultObject.put("results", results);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Close a debug connection */
    private void closeDebugger(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String invocationId = dataObject.getString("invocationId");
            String contextId = dataObject.getString("contextId");
            debugClients.removeDebugConnection(ticket, invocationId, contextId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Send a command to a debugger */
    private void sendDebugCommand(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String invocationId = dataObject.getString("invocationId");
            String contextId = dataObject.getString("contextId");
            String command = dataObject.getString("command");
            IWorkflowDebugClient client = debugClients.getDebugConnection(ticket, invocationId, contextId);
            if(client!=null){
                client.sendCommand(command);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Cannot get debug connection");
            }            
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get the last debug buffer contents */
    private void getLastDebugBuffer(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String invocationId = dataObject.getString("invocationId");
            String contextId = dataObject.getString("contextId");
            IWorkflowDebugClient client = debugClients.getDebugConnection(ticket, invocationId, contextId);
            if(client!=null){
                String bufferText = new String(client.getLastResponseBuffer());
                resultObject.put("buffer", bufferText);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Cannot get debug connection");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
            
    /**
     * Take a copy of a workflow
     */
    private void copyWorkflow(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String versionId = null;
            if(dataObject.has("versionId")){
                versionId = dataObject.getString("versionId");
            }
            
            String newName = null;
            if(dataObject.has("newName")){
                newName = dataObject.getString("newName");
            }
            
            
            WorkflowDocument source = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, id);
            if(source!=null){
                // Make a copy of the document
                DocumentVersion version;
                if(versionId!=null){
                    version = EJBLocator.lookupStorageBean().getVersion(ticket, id, versionId);
                } else {
                    version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, id);
                }
                
                WorkflowDocument target = new WorkflowDocument();
                source.populateCopy(target);
                
                // Use new name if present
                if(newName!=null && !newName.isEmpty()){
                    target.setName(newName);
                }
                
                User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
                target.setContainerId(u.getWorkflowFolderId());
                target.setCreatorId(u.getId());
                target.setCreationTime(new Date());
                target = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(ticket, target);
                
                // Now copy the data
                StorageUtils.copyDocumentData(ticket, source, version, target);
                
                // Copy image if there is any
                ImageData image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, id, ImageData.WORKFLOW_PREVIEW);
                if(image!=null){
                    ImageData copyImage = image.getCopy();
                    copyImage.setServerObjectId(target.getId());
                    EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, target.getId(), copyImage.getData(), copyImage.getType());
                }
                resultObject.put("id", target.getId());
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such worklow");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /**
     * Delete a workflow
     */
    private void deleteWorkflow(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String workflowId = dataObject.getString("id");
            WorkflowEJBLocator.lookupWorkflowManagementBean().deleteWorkflowDocument(ticket, workflowId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /**
     * Delete a workflow invocation folder
     */
    private void deleteInvocation(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("id");
            if (invocationId != null) {
                Folder folder = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);

                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, folder, Permission.WRITE_PERMISSION)) {
                    EJBLocator.lookupObjectRemovalBean().remove(ticket, folder);
                } else {
                    APIUtils.setError(resultObject, "Permission denied");
                }
            } else {
                APIUtils.setError(resultObject, "No invocation specified");
            }
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get a JSON representation of a workflow */
    private void getWorkflowJson(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String workflowId = dataObject.getString("workflowId");
            String versionId = null;
            if (dataObject.has("versionId")) {
                versionId = dataObject.getString("versionId");
            }

            WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, workflowId);
            DocumentVersion version;
            if (versionId != null) {
                version = EJBLocator.lookupStorageBean().getVersion(ticket, workflowId, versionId);
            } else {
                version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, workflowId);
            }

            InputStream inStream = StorageUtils.getInputStream(ticket, workflow, version);
            XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(inStream);
            XmlDataStore workflowData = reader.read();
            inStream.close();

            DrawingModel drawing = new DefaultDrawingModel();
            ((DefaultDrawingModel) drawing).recreateObject(workflowData);
            JSONDrawingExporter exporter = new JSONDrawingExporter(drawing);
            exporter.setDescription(workflow.getDescription());
            exporter.setDocumentId(workflow.getId());
            exporter.setExternalBlockName(workflow.getExternalDataBlockName());
            exporter.setExternalDataSupported(workflow.isExternalDataSupported());
            exporter.setExternalService(workflow.isExternalService());
            exporter.setName(workflow.getName());
            exporter.setVersionId(version.getId());
            exporter.setVersionNumber(version.getVersionNumber());
            exporter.setDeletedOnSuccess(workflow.isDeletedOnSuccess());
            exporter.setOnlyFailedOutputsUploaded(workflow.isOnlyFailedOutputsUploaded());
            exporter.setSingleVmMode(workflow.isSingleVmMode());
            
            resultObject.put("drawing", exporter.saveToJson());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Save a JSON drawing representation back to the database */
    private void putWorkflowJson(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        WorkflowDocument document = null;
        try {
            if (dataObject.has("documentId") && !dataObject.getString("documentId").equals("")) {
                document = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, dataObject.getString("documentId"));
            }

            if (document == null) {
                document = new WorkflowDocument();
              String folderId = null;
              if (ticket.getDefaultProjectId() != null)
              {
                  if(StringUtils.isNotEmpty(ticket.getDefaultProjectId()))
                  {
                      Study study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.parseInt(ticket.getDefaultProjectId()));
                      folderId = study.getWorkflowFolderId();
                  }
              }
              else
              {
                folderId = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getWorkflowFolderId();
              }
              if (folderId != null)
              {
                document.setContainerId(folderId);
              }
            }

            // Existing document
            if (dataObject.has("name")) {
                document.setName(dataObject.getString("name"));
            }
            if (dataObject.has("description")) {
                document.setDescription(dataObject.getString("description"));
            }
            if (dataObject.has("externalDataSupported")) {
                document.setExternalDataSupported(dataObject.getBoolean("externalDataSupported"));
            }
            if (dataObject.has("externalBlockName")) {
                document.setExternalDataBlockName(dataObject.getString("externalBlockName"));
            }
            if (dataObject.has("dynamicEngine")) {
                if (dataObject.getBoolean("dynamicEngine") == true) {
                    document.setEngineType(WorkflowDocument.DYNAMIC_ENGINE);
                } else {
                    document.setEngineType(WorkflowDocument.STATIC_ENGINE);
                }
            }
            if(dataObject.has("singleVMMode")){
                document.setSingleVmMode(dataObject.getBoolean("singleVMMode"));
            } else {
                document.setSingleVmMode(true);
            }
            if(dataObject.has("deletedOnSuccess")){
                if(dataObject.getBoolean("deletedOnSuccess")==true){
                    document.setDeletedOnSuccess(true);
                } else {
                    document.setDeletedOnSuccess(false);
                }
            } else {
                document.setDeletedOnSuccess(false);
            }
            
            if(dataObject.has("onlyFailedOutputsUploaded")){
                if(dataObject.getBoolean("onlyFailedOutputsUploaded")==true){
                    document.setOnlyFailedOutputsUploaded(true);
                } else {
                    document.setOnlyFailedOutputsUploaded(false);
                }
            } else {
                document.setOnlyFailedOutputsUploaded(false);
            }
            
            if(dataObject.has("externalService")){
                document.setExternalService(dataObject.getBoolean("externalService"));
            } else {
                document.setExternalService(false);
            }
            
            // Save the document
            document = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(ticket, document);

            // Try and read the drawing JSON data
            JSONDrawingImporter importer = new JSONDrawingImporter(dataObject);
            DrawingModel drawing = importer.loadDrawing();

            // Try and render this drawing onto an Image
            int width;
            int height;
            if (dataObject.has("width")) {
                width = dataObject.getInt("width");
            } else {
                width = 640;
            }

            if (dataObject.has("height")) {
                height = dataObject.getInt("height");
            } else {
                height = 480;
            }

            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = img.createGraphics();
            DefaultDrawingRenderer renderer = new DefaultDrawingRenderer(drawing);
            renderer.renderDrawing(g, width, height);

            ByteArrayOutputStream imgStream = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", imgStream);
            imgStream.flush();
            imgStream.close();
            EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, document.getId(), imgStream.toByteArray(), ImageData.WORKFLOW_PREVIEW);

            XmlDataStoreByteArrayIO writer = new XmlDataStoreByteArrayIO(((XmlStorable) drawing).storeObject());
            writer.setDescriptionIncluded(true);
            byte[] drawingData = writer.toByteArray();
            DocumentVersion version = WorkflowEJBLocator.lookupWorkflowManagementBean().uploadWorkflowDocumentData(ticket, document.getId(), drawingData);
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            WorkflowSaveOperation op = new WorkflowSaveOperation(version.getDocumentRecordId(), version.getId(), version.getVersionNumber(), document.getName(), user.getDisplayName(), new Date());
            ProvenanceLoggerClient loggerClient = new ProvenanceLoggerClient();
            loggerClient.log(op);

            resultObject.put("documentId", document.getId());
            resultObject.put("versionId", version.getId());
            resultObject.put("versionNumber", version.getVersionNumber());
            APIUtils.setSuccess(resultObject);

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

	@Deprecated
    public void getPaletteJson(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            JSONPaletteCreator paletteCreator = new JSONPaletteCreator(ticket);
            JSONObject paletteJson = paletteCreator.getPalette();
            resultObject.put("palette", paletteJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

	public void getHierarchicalPaletteJson(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
		try {
			JSONPaletteCreator paletteCreator = new JSONPaletteCreator(ticket);
			JSONArray paletteJson = paletteCreator.getHierarchicalPalette();

			resultObject.put("palette", paletteJson);
			APIUtils.setSuccess(resultObject);
		} catch (Exception e) {
			APIUtils.populateExceptionData(resultObject, e);
		}
	}

    public void getServiceJson(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String serviceId = dataObject.getString("serviceId");
            String versionId = null;
            if (dataObject.has("versionId")) {
                versionId = dataObject.getString("versionId");
            }
            JSONBlockCreator creator = new JSONBlockCreator(ticket, serviceId, versionId);
            resultObject.put("block", creator.createBlockJson());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    public void listInvocations(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session) {
        try {
            String workflowId = dataObject.getString("workflowId");

            List results = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(ticket, workflowId);
            JSONObject invocationsJson = new JSONObject();
            JSONArray invocationArray = new JSONArray();
            JSONObject invocationJson;
            WorkflowInvocationFolder invocation;
            for (int i = 0; i < results.size(); i++) {
                invocation = (WorkflowInvocationFolder) results.get(i);
                invocationJson = createInvocationJson(invocation, ticket, session);
                invocationArray.put(invocationJson);
            }

            invocationsJson.put("invocationArray", invocationArray);
            resultObject.put("invocations", invocationsJson);

            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    public void executeWorkflow(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String workflowId = dataObject.getString("workflowId");
            if (workflowId != null) {
                WorkflowInvocationMessage message = new WorkflowInvocationMessage(ticket, workflowId);
                if(dataObject.has("documentId")){
                    message.setTargetFileId(dataObject.getString("documentId"));
                }
                String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, message);
                resultObject.put("invocationId", invocationId);
            } else {
                throw new Exception("No workflow specified");
            }

            APIUtils.setSuccess(resultObject);

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get the workflow drawing data for an invocation */
    public void fetchInvocationWorkflowData(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("invocationId");
            WorkflowInvocationFolder folder = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
            if (folder != null) {
                resultObject.put("folderExists", true);
                resultObject.put("status", folder.getInvocationStatus());
                DocumentRecord workflowDoc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, folder.getWorkflowId());
                
                DocumentVersion version;
                if(folder.getInvocationId()!=null){
                    version = EJBLocator.lookupStorageBean().getVersion(ticket, folder.getWorkflowId(), folder.getVersionId()); 
                } else {
                    version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, folder.getWorkflowId());
                }

                if (workflowDoc != null && version != null) {
                    resultObject.put("documentExists", true);
                    InputStream inStream = StorageUtils.getInputStream(ticket, workflowDoc, version);
                    XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(inStream);
                    XmlDataStore workflowData = reader.read();
                    inStream.close();
                    DrawingModel drawing = new DefaultDrawingModel();
                    ((DefaultDrawingModel) drawing).recreateObject(workflowData);
                    JSONDrawingExporter exporter = new JSONDrawingExporter(drawing);
                    exporter.setDescription("Executed workflow");
                    exporter.setDocumentId(workflowDoc.getId());
                    exporter.setExternalBlockName("");
                    exporter.setExternalDataSupported(false);
                    exporter.setName(workflowDoc.getName());
                    exporter.setVersionId(version.getId());
                    exporter.setVersionNumber(version.getVersionNumber());

                    resultObject.put("drawing", exporter.saveToJson());
                    resultObject.put("invocation", createBasicInvocationJson(folder));
                    APIUtils.setSuccess(resultObject);

                } else {
                    resultObject.put("documentExists", false);
                    throw new Exception("Invocation has no workflow data");
                }

            } else {
                resultObject.put("folderExists", false);
                resultObject.put("status", WorkflowInvocationFolder.INVOCATION_WAITING);
                throw new Exception("Workflow invocation does not exist");
            }

        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

    }

    /** Fetch the execution reports for a given invocation */
    public void fetchReports(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("invocationId");
            if (invocationId != null) {
                InvocationReportBuilder builder = new InvocationReportBuilder(invocationId, ticket);
                XmlDataStore reports = builder.buildReport();
                WorkflowInvocationFolder folder = builder.getFolder();

                BlockExecutionReport report;
                int size = reports.intValue("ReportCount", 0);
                JSONObject reportJson;
                JSONArray reportsArray = new JSONArray();
                for (int i = 0; i < size; i++) {
                    reportJson = new JSONObject();
                    report = (BlockExecutionReport) reports.xmlStorableValue("Report" + i);

                    if (report.getExecutionStatus() == BlockExecutionReport.NO_ERRORS) {
                        reportJson.put("executedOk", true);
                    } else {
                        reportJson.put("executedOk", false);
                    }

                    reportJson.put("executionStatus", report.getExecutionStatus());
                    reportJson.put("message", report.getMessage());
                    reportJson.put("additionalMessage", report.getAdditionalMessage());
                    reportJson.put("commandOutput", report.getCommandOutput());
                    reportJson.put("containsCommandOutput", report.containsCommandOutput());
                    reportJson.put("blockGuid", report.getBlockGuid());
                    reportsArray.put(reportJson);

                }
                resultObject.put("invocationTime", folder.getInvocationDate());
                resultObject.put("invocationId", invocationId);
                resultObject.put("folderId", folder.getId());
                resultObject.put("status", folder.getInvocationStatus());
                resultObject.put("reportCount", size);
                resultObject.put("reports", reportsArray);
                resultObject.put("invocation", createBasicInvocationJson(folder));
            } else {
                throw new Exception("InvocationID not specified");
            }

            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

    }

    /** List all of the files produced by an invocation */
    public void listInvocationFiles(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("invocationId");
            if (invocationId != null) {
                Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, invocationId);
                if (folder != null) {

                    JSONArray contentsArray = new JSONArray();
                    JSONObject docJson;
                    DocumentRecord doc;

                    List<Folder> folders = EJBLocator.lookupStorageBean().getChildFolders(ticket, folder.getId());
                    for (Folder subFolder : folders)
                    {
                        JSONObject folderJSON = new JSONObject();
                        folderJSON.put("type", "folder");
                        folderJSON.put("name", subFolder.getName());
                        folderJSON.put("id", subFolder.getId());
                        folderJSON.put("description", subFolder.getDescription());
                        if(subFolder instanceof WorkflowInvocationFolder)
                        {
                            folderJSON.put("workflow", true);
                        }
                        contentsArray.put(folderJSON);
                    }

                    List contents = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, folder.getId());
                    for (int i = 0; i < contents.size(); i++) {
                        doc = (DocumentRecord) contents.get(i);
                        docJson = new JSONObject();
                        docJson.put("type", "document");
                        docJson.put("name", doc.getName());
                        docJson.put("id", doc.getId());
                        docJson.put("description", doc.getDescription());
                        contentsArray.put(docJson);
                    }

                    resultObject.put("folder", invocationId);
                    resultObject.put("container", folder.getContainerId());
                    resultObject.put("documentCount", contentsArray.length());
                    resultObject.put("documents", contentsArray);
                    APIUtils.setSuccess(resultObject);
                } else {
                    throw new Exception("Specified Invocation does not exist");
                }

            } else {
                throw new Exception("InvocationID not specified");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Fetch the system.out data for an execution */
    public void fetchBlockExecutionOutput(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("invocationId");
            String contextId = dataObject.getString("contextId");
            WorkflowServiceLog log = WorkflowEJBLocator.lookupWorkflowManagementBean().getServiceLog(ticket, invocationId, contextId);
            addServiceLogJsonToResult(log, resultObject);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** List the versions of a workflow */
    public void listWorkflowVersions(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String id = dataObject.getString("id");
            DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, id);
            DateFormat format = DateFormat.getDateTimeInstance();
            if (doc != null) {
                List versionList = EJBLocator.lookupStorageBean().listVersions(ticket, id);
                JSONArray versions = new JSONArray();
                JSONObject versionJson;
                DocumentVersion version;
                for (int i = 0; i < versionList.size(); i++) {
                    version = (DocumentVersion) versionList.get(i);
                    versionJson = new JSONObject();
                    versionJson.put("timestamp", format.format(version.getTimestamp()));
                    versionJson.put("versionNumber", version.getVersionNumber());
                    versionJson.put("userId", version.getUserId());
                    versionJson.put("versionId", version.getId());
                    versions.put(versionJson);
                }
                resultObject.put("versions", versions);
                resultObject.put("versionCount", versionList.size());
            } else {
                throw new Exception("Workflow does not exist");
            }
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** List the versions of a specific service */
    public void listServiceVersions(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String serviceId = dataObject.getString("id");
            DateFormat format = DateFormat.getDateTimeInstance();
            if (serviceId != null) {
                List versions = EJBLocator.lookupStorageBean().listVersions(ticket, serviceId);
                JSONObject versionJson;
                JSONArray versionsJson = new JSONArray();
                DocumentVersion version;
                int count = versions.size();
                for (int i = 0; i < count; i++) {
                    versionJson = new JSONObject();
                    version = (DocumentVersion) versions.get(i);
                    versionJson.put("id", version.getId());
                    versionJson.put("comments", version.getComments());
                    versionJson.put("timestamp", format.format(version.getTimestampDate()));
                    versionJson.put("userId", version.getUserId());
                    versionJson.put("versionNumber", version.getVersionNumber());
                    versionsJson.put(versionJson);
                }

                resultObject.put("versions", versionsJson);
                resultObject.put("versionCount", count);
            } else {
                throw new Exception("Cannot find specified service");
            }

            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get the help text for a service */
    public void getWorkflowServiceHelp(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String serviceId = dataObject.getString("serviceId");
            String versionId;
            if (dataObject.has("versionId")) {
                versionId = dataObject.getString("versionId");
            } else {
                versionId = null;
            }

            BlockHelpHtml htmlHelp = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicServiceHelp(ticket, serviceId, versionId);
            resultObject.put("html", htmlHelp.getHtmlData());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    public void getWorkflowInvocation(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session) {
        try {
            String invocationId = dataObject.getString("invocationId");
            WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
            JSONObject invocationJson = createInvocationJson(invocation, ticket, session);
            
            // Look for any invocation locks
            List locks = WorkflowEJBLocator.lookupWorkflowLockBean().listInvocationLocks(ticket, invocationId);
            if(locks.size()>0){
                int remaining = 0;
                for(int i=0;i<locks.size();i++){
                    try {
                        remaining = remaining + WorkflowEJBLocator.lookupWorkflowLockBean().getNumberOfRemainingInvocationsInLock(ticket, ((WorkflowLock)locks.get(i)).getId());
                    } catch (Exception e){
                        
                    }
                }
                invocationJson.put("hasLocks", true);
                invocationJson.put("workflowsRemaining", remaining);
            }           
            resultObject.put("invocation", invocationJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet to manage workflow editing";
    }
    
    private JSONObject createTerminationItem(WorkflowTerminationItem item) throws Exception {
        JSONObject terminationItem = new JSONObject();
        terminationItem.put("message", item.getMessage());
        terminationItem.put("id", item.getId());
        terminationItem.put("name", item.getName());
        terminationItem.put("subworkflowMessage", item.getSubWorkflowProcessingMessage());
        terminationItem.put("terminatedOk", item.isTerminatedOk());
        terminationItem.put("subworkflowsTerminationError", item.isSubWorkflowsProcessingError());
        return terminationItem;
    }

    /** Create basic JSON properties for a ServerObject */
    private JSONObject createServerObjectJson(ServerObject object) throws Exception {
        JSONObject objectJson = new JSONObject();
        objectJson.put("id", object.getId());
        objectJson.put("name", object.getName());
        objectJson.put("description", object.getDescription());
        objectJson.put("className", object.getClass().getName());
        return objectJson;
    }
    
    /** Create a basic JSON representation of a project */
    private JSONObject createProjectObjectJson(Project project) throws Exception {
        JSONObject projectJson = new JSONObject();
        projectJson.put("id", project.getId());
        projectJson.put("name", project.getName());
        projectJson.put("description", project.getDescription());
        projectJson.put("className", project.getClass().getName());
        return projectJson;
    }
    
    private JSONObject createInvocationJson(WorkflowInvocationFolder invocation, Ticket ticket, HttpSession session) throws Exception {
        JSONObject invocationJson = new JSONObject();
        invocationJson.put("invocationName", invocation.getName());
        invocationJson.put("invocationId", invocation.getInvocationId());
        invocationJson.put("versionId", invocation.getVersionId());
        invocationJson.put("folderId", invocation.getId());
        if(invocation.getInvocationDate()!=null){
            invocationJson.put("submitTime", format.format(invocation.getInvocationDate()));
        } else {
            invocationJson.put("submitTime", "Unknown");
        }

        if(invocation.getQueuedTime()!=null){
            invocationJson.put("queuedTime", format.format(invocation.getQueuedTime()));
        } else {
            invocationJson.put("queuedTime", "Unknown");
        }

        if(invocation.getDequeuedTime() != null) {
            invocationJson.put("dequeuedTime", format.format(invocation.getDequeuedTime()));
        } else {
            invocationJson.put("dequeuedTime", "Unknown");
        }

        if(invocation.getExecutionStartTime()!=null){
            invocationJson.put("startTime", format.format(invocation.getExecutionStartTime()));
        } else {
            invocationJson.put("startTime", "Unknown");
        }

        if(invocation.getExecutionEndTime()!=null){
            invocationJson.put("endTime", format.format(invocation.getExecutionEndTime()));
        } else {
            invocationJson.put("endTime", "Unknown");
        }

        try {
            DocumentVersion workflowVersion = EJBLocator.lookupStorageBean().getVersion(ticket, invocation.getWorkflowId(), invocation.getVersionId());
            invocationJson.put("versionNum", workflowVersion.getVersionNumber());
            invocationJson.put("currentBlockId", invocation.getCurrentBlockId());
            invocationJson.put("status", invocation.getInvocationStatus());
            invocationJson.put("workflowId", invocation.getWorkflowId());
            invocationJson.put("workflowName", APIUtils.getObjectNameFromCache(ticket, invocation.getWorkflowId(), session));
            invocationJson.put("statusText", getInvocationStatusText(invocation));
            invocationJson.put("hasLocks", false);
            invocationJson.put("workflowsRemaining", 0);
            invocationJson.put("message", invocation.getMessage());
            invocationJson.put("bytesStreamed", invocation.getBytesStreamed());
            invocationJson.put("totalBytesToStream", invocation.getTotalBytesToStream());
            invocationJson.put("engineId", invocation.getEngineId());
        } catch (Exception x) {
            logger.warn("Cannot retrieve workflow: " + invocation.getWorkflowId() + " in version: " + invocation.getVersionId());
        }

        return invocationJson;
    }

    /** Create a service log JSON object */
    private void addServiceLogJsonToResult(WorkflowServiceLog log, JSONObject resultObject) throws Exception {
        resultObject.put("invocationId", log.getInvocationId());
        resultObject.put("contextId", log.getContextId());
        resultObject.put("outputText", log.getOutputText());
        resultObject.put("statusMessage", log.getStatusMessage());        
    }
    
    /** Add an error message for block output */
    private void addServiceLogDataJsonToResult(String message, String outputText, String invocationId, String contextId, JSONObject resultObject) throws Exception {
        resultObject.put("invocationId", invocationId);
        resultObject.put("contextId", contextId);
        resultObject.put("outputText", outputText);
        resultObject.put("statusMessage", message);          
    }
    
    /** Create a basic invocation object */
    private JSONObject createBasicInvocationJson(WorkflowInvocationFolder invocation) throws Exception {
        JSONObject invocationJson = new JSONObject();
        invocationJson.put("invocationId", invocation.getInvocationId());
        invocationJson.put("folderId", invocation.getId());
        invocationJson.put("versionId", invocation.getVersionId());
        invocationJson.put("currentBlockId", invocation.getCurrentBlockId());
        invocationJson.put("status", invocation.getInvocationStatus());
        invocationJson.put("workflowId", invocation.getWorkflowId());
        invocationJson.put("statusText", getInvocationStatusText(invocation));
        invocationJson.put("message", invocation.getMessage());
        invocationJson.put("bytesStreamed", invocation.getBytesStreamed());
        invocationJson.put("totalBytesToStream", invocation.getTotalBytesToStream());        
        return invocationJson;
    }
    
    /** Get the status text for an invocation */
    private String getInvocationStatusText(WorkflowInvocationFolder invocation){
        switch (invocation.getInvocationStatus()) {
            case WorkflowInvocationFolder.INVOCATION_FINISHED_OK:
                return "Finished OK";

            case WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS:
                return "Finished with Errors";

            case WorkflowInvocationFolder.INVOCATION_RUNNING:
                return "Running";

            case WorkflowInvocationFolder.INVOCATION_WAITING:
                return "Waiting in Queue";

            default:
                return "Unknown";
        }
    }

    /** Kill all invocations of a workflow owned by a user */
    private void terminateAllInvocationsOfWorkflow(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String workflowId = dataObject.getString("workflowId");
            List invocations = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(ticket, workflowId);
            
            // Kill them all
            WorkflowInvocationFolder invocation;
            for(Object o : invocations){
                invocation = (WorkflowInvocationFolder)o;
                if(invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_RUNNING || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER || invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_WAITING){
                    try {
                        WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, invocation.getId());
                    } catch (Exception e){
                        logger.error("Error terminating workflow: " + e.getMessage());
                    }
                }
            }
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Kill a workflow invocation */
    private void terminateInvocation(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String invocationId = dataObject.getString("invocationId");
            WorkflowTerminationReport report = WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, invocationId);
            
            JSONArray reports = new JSONArray();
            List<WorkflowTerminationItem> terminationItems = report.getFlatList();
            for(WorkflowTerminationItem item : terminationItems){
                reports.put(createTerminationItem(item));
            }
            resultObject.put("reports", reports);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Get a datatables formatted array of data for all of the user workflows */
    private void getInvocationTable(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            int filterStatus;
            if(dataObject.has("filter")){
                filterStatus = dataObject.getInt("filter");
            } else {
                filterStatus = -1;
            }

            boolean filterSucceeded;
            if(dataObject.has("filterSucceeded")){
                filterSucceeded = dataObject.getBoolean("filterSucceeded");
            } else {
                filterSucceeded = true;
            }
            
            List invocations = WorkflowEJBLocator.lookupWorkflowManagementBean().getAllInvocationFoldersForUser(ticket, filterStatus, filterSucceeded);
            JSONArray dataRows = new JSONArray();
            WorkflowInvocationFolder invocation;
            JSONArray invocationData;
            for(int i=0;i<invocations.size();i++){
                invocation = (WorkflowInvocationFolder)invocations.get(i);
                invocationData = new JSONArray();

                // Status
                invocationData.put(invocation.getInvocationStatus());

                // Name
                invocationData.put(APIUtils.getObjectNameFromCache(ticket, invocation.getWorkflowId(), session));

                // Start time
                invocationData.put(APIUtils.formatDate(invocation.getInvocationDate()));

                // Invocation ID for access to files
                invocationData.put(invocation.getId());

                invocationData.put(invocation.getId());

                // Workflow ID
                invocationData.put(invocation.getWorkflowId());

                dataRows.put(invocationData);
            }

            resultObject.put("invocations", dataRows);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Create a folder trigger */
    private void createTrigger(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String workflowId = dataObject.getString("workflowId");
            String folderId = dataObject.getString("folderId");
            WorkflowFolderTrigger trigger = new WorkflowFolderTrigger();
            trigger.setComments("Workflow trigger");
            trigger.setFolderId(folderId);
            trigger.setWorkflowId(workflowId);
            trigger = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowFolderTrigger(ticket, trigger);
            resultObject.put("triggerId", trigger.getId());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Delete a folder trigger */
    private void deleteTrigger(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long triggerId = dataObject.getLong("triggerId");
            WorkflowEJBLocator.lookupWorkflowManagementBean().deleteWorkflowFolderTrigger(ticket, triggerId);;
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }    
    
    private void listProjectObjects(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List results = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, 0, Integer.MAX_VALUE);
            JSONArray projects = new JSONArray();
            for(int i=0;i<results.size();i++){
                projects.put(createProjectObjectJson((Project)results.get(i)));
            }
            resultObject.put("objects", projects);
            resultObject.put("objectCount", results.size());
            APIUtils.setSuccess(resultObject);
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** List server objects that a user has access to */
    private void listServerObjects(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String className = null;
            if(dataObject.has("className")){
                className = dataObject.getString("className");
            } else {
                className = ServerObject.class.getName();
            }
            
            Class objectClass = Class.forName(className);
            List results = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, ticket.getUserId(), objectClass, 0, 0);
            JSONArray objects = new JSONArray();
            for(int i=0;i<results.size();i++){
                objects.put(createServerObjectJson((ServerObject)results.get(i)));
            }
            resultObject.put("objects", objects);
            resultObject.put("objectCount", results.size());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get a folder trigger as a JSON object */
    private void getWorkflowTrigger(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            
            WorkflowFolderTrigger trigger = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowFolderTrigger(ticket, id);
            if(trigger!=null){
                JSONObject triggerJson = new JSONObject();
                triggerJson.put("id", trigger.getId());
                triggerJson.put("properties", trigger.toJson());
                resultObject.put("trigger", triggerJson);
                APIUtils.setSuccess(resultObject);                
            } else {
                APIUtils.setError(resultObject, "No such workflow trigger");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Save the properties from a workflow trigger */
    private void saveWorkflowTrigger(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            WorkflowFolderTrigger trigger = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowFolderTrigger(ticket, id);
            if(trigger!=null){
                JSONObject properties = dataObject.getJSONObject("properties");
                trigger.readJson(properties);
                trigger = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowFolderTrigger(ticket, trigger);
                APIUtils.setSuccess(resultObject);                
            } else {
                APIUtils.setError(resultObject, "No such workflow trigger");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Save the properties from a workflow trigger */
    private void reExecuteWorkflow(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String newInvocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().resubmitWorkflow(ticket, id);
            if(newInvocationId != null && !newInvocationId.equals(""))
            {
                resultObject.put("invocationId", newInvocationId);
                APIUtils.setSuccess(resultObject);
            }
            else{
                APIUtils.setError(resultObject, "Unable to re-run worklfow");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get summary details for a workflow lock */
    private void fetchLockSummary(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String invocationId = dataObject.getString("invocationId");
            String contextId = dataObject.getString("contextId");
            WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
            if(invocation!=null){
                if(invocation.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_RUNNING){
                    if(invocation.getCurrentBlockId().equals(contextId)){
                        List locks = WorkflowEJBLocator.lookupWorkflowLockBean().listInvocationLocks(ticket, invocationId);
                        WorkflowLock lock = null;
                        for(Object i : locks){
                            if(((WorkflowLock)i).getContextId().equals(contextId)){
                                lock = (WorkflowLock)i;
                            }
                        }
                        
                        if(lock!=null){
                            List members = WorkflowEJBLocator.lookupWorkflowLockBean().getLockMembers(ticket, lock.getId());
                            int waitingCount = 0;
                            int runningCount = 0;
                            int succeededCount = 0;
                            int failedCount = 0;

                            WorkflowLockMember member;
                            for(Object i : members){
                                member = (WorkflowLockMember)i;
                                
                                switch(member.getInvocationStatus()){
                                    case WorkflowInvocationFolder.INVOCATION_FINISHED_OK :
                                        succeededCount++;
                                        break;
                                        
                                    case WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS:
                                        failedCount++;
                                        break;
                                        
                                    case WorkflowInvocationFolder.INVOCATION_RUNNING:
                                        runningCount++;
                                        break;
                                        
                                    case WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER:
                                        runningCount++;
                                        break;
                                        
                                    case WorkflowInvocationFolder.INVOCATION_WAITING:
                                        waitingCount++;
                                        break;
                                }
                            }
                            
                            // Send back results
                            JSONObject lockDetails = new JSONObject();
                            
                            lockDetails.put("lockId", lock.getId());
                            lockDetails.put("status", lock.getStatus());
                            lockDetails.put("running", runningCount);
                            lockDetails.put("queued", waitingCount);
                            lockDetails.put("succeded", succeededCount);
                            lockDetails.put("failed", failedCount);
                            lockDetails.put("total", members.size());
                            
                            resultObject.put("lockDetails", lockDetails);
                        } else {
                            APIUtils.setError(resultObject, "Block is not holding a lock");
                        }
                    } else {
                        APIUtils.setError(resultObject, "Selected block is not executing");
                    }
                } else {
                    APIUtils.setError(resultObject, "Workflow is not executing");
                }
            } else {
                APIUtils.setError(resultObject, "No such invocation");
            }
                    
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Proceed with a lock */
    private void proceedWithLock(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long lockId = dataObject.getLong("lockId");
            WorkflowEJBLocator.lookupWorkflowLockBean().notifyLockHolderIfComplete(ticket, lockId, true);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Re-run failed invocations */
    private void rerunFailedInLock(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long lockId = dataObject.getLong("lockId");
            List invocations = WorkflowEJBLocator.lookupWorkflowLockBean().getLockMembers(ticket, lockId);
            WorkflowLockMember member;
            for(Object i : invocations){
                try {
                    member = (WorkflowLockMember)i;
                    if(member.getInvocationStatus()==WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS){
                        WorkflowEJBLocator.lookupWorkflowEnactmentBean().resubmitWorkflow(ticket, member.getInvocationId());
                    }
                } catch (Exception e){
                }
                APIUtils.setSuccess(resultObject);
            }
            APIUtils.setSuccess(resultObject);
        } catch(Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Terminate a lock and all of its invocations */
    private void terminateLock(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long lockId = dataObject.getLong("lockId");
            WorkflowLock lock = WorkflowEJBLocator.lookupWorkflowLockBean().getLock(ticket, lockId);
            WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, lock.getInvocationId());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}

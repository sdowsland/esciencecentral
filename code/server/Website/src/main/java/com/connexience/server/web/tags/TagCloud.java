package com.connexience.server.web.tags;

import com.connexience.server.model.social.TagCloudElement;

import java.util.List;

public interface TagCloud {
    public List<TagCloudElement> getTagCloudElements();
}

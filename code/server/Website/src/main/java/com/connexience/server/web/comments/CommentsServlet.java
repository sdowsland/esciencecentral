/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.server.web.comments;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.Comment;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author hugo
 */
public class CommentsServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }

                    if (method.equalsIgnoreCase("fetchComments")) {
                        // List the events for a group
                        fetchComments(ticket, dataObject, resultObject, session);

                    } else if(method.equalsIgnoreCase("addComment")){
                        // Add a comment to an object
                        addComment(ticket, dataObject, resultObject, session);

                    } else if(method.equalsIgnoreCase("updateComment")){
                        // Save a comment
                        updateComment(ticket, dataObject, resultObject, session);

                    } else if(method.equalsIgnoreCase("deleteComment")){
                        // Delete a comment
                        deleteComment(ticket, dataObject, resultObject, session);
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** Fetch all of the comments for an object */
    private void fetchComments(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            String objectId = dataObject.getString("objectId");
            List comments = EJBLocator.lookupCommentBean().getComments(ticket, objectId);
            Comment comment;
            JSONArray commentsArray = new JSONArray();
            for(int i=0;i<comments.size();i++){
                comment = (Comment)comments.get(i);
                commentsArray.put(createCommentJson(comment));
            }
            resultObject.put("comments", commentsArray);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Add a comment to an object */
    private void addComment(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            String objectId = dataObject.getString("objectId");
            String text = dataObject.getString("text");

            Comment comment = EJBLocator.lookupCommentBean().createComment(ticket, objectId, text);
            resultObject.put("comment", createCommentJson(comment));
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Change a comment */
    private void updateComment(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            String commentId = dataObject.getString("commentId");
            String text = dataObject.getString("text");
            if(commentId.equals("unsaved")){
                String objectId = dataObject.getString("objectId");
                Comment comment = EJBLocator.lookupCommentBean().createComment(ticket, objectId, text);
                resultObject.put("comment", createCommentJson(comment));
            } else {
                Comment comment = EJBLocator.lookupCommentBean().updateComment(ticket, commentId, text);
                resultObject.put("comment", createCommentJson(comment));
            }
            APIUtils.setSuccess(resultObject);
        } catch(Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Delete a comment */
    private void deleteComment(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpSession session){
        try {
            String commentId = dataObject.getString("commentId");
            EJBLocator.lookupCommentBean().deleteComment(ticket, commentId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Create a JSON representation of a comment */
    private JSONObject createCommentJson(Comment comment) throws Exception {
        JSONObject commentJson = new JSONObject();
        commentJson.put("id", comment.getId());
        commentJson.put("name", comment.getName());
        commentJson.put("authorName", comment.getAuthorName());
        commentJson.put("description", comment.getDescription());
        commentJson.put("text", comment.getText());
        commentJson.put("creatorId", comment.getCreatorId());
        commentJson.put("timestampMillis", comment.getTimeInMillis());
        return commentJson;
    }

}

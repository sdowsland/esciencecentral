/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.metadata;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.SearchFolder;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.MetadataQuery;
import com.connexience.server.model.metadata.MetadataQueryItem;
import com.connexience.server.model.metadata.queries.*;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author hugo
 */
public class MetadataServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
        
        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }
                    
                    if (method.equalsIgnoreCase("getMetadata")) {
                        // Remove a workflow
                        getMetadata(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("addMetadata")){
                        // Add some metadata
                        addMetadata(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("removeMetadata")){
                        // Remove some metadata
                        removeMetadata(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getSearchFolder")){
                        // Get data for a smart folder
                        getSearchFolder(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listSearchFolders")){
                        // List all of the search folders
                        listSearchFolders(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("createSearchFolder")){
                        // Create a new search folder
                        createSearchFolder(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("removeSearchFolder")){
                        // Remove a search folder
                        removeSearchFolder(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("createQueryItem")){
                        // Create a new query item
                        createQueryItem(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("saveSearchFolder")){
                        // Save the search folder
                        saveSearchFolder(ticket, dataObject, resultObject);
                        
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
    /** Create a JSON object for a search folder */
    private JSONObject createSearchFolderJson(SearchFolder f) throws Exception {
        JSONObject folderJson = new JSONObject();
        folderJson.put("id", f.getId());
        folderJson.put("description", f.getDescription());
        folderJson.put("name", f.getName());
        folderJson.put("creatorId", f.getCreatorId());
        folderJson.put("creationDate", f.getCreationDate());
        return folderJson;
    }
    
    /** Create a JSON Object for a piece of metadata */
    private JSONObject createMetadataJson(MetadataItem md) throws Exception {
        JSONObject metadataJson = new JSONObject();
        metadataJson.put("category", md.getCategory());
        metadataJson.put("id", md.getId());
        metadataJson.put("name", md.getName());
        metadataJson.put("objectId", md.getObjectId());
        metadataJson.put("userId", md.getUserId());
        
        if(md instanceof TextMetadata){
            metadataJson.put("type", "text");
            metadataJson.put("value", ((TextMetadata)md).getTextValue());
                    
        } else if(md instanceof NumericalMetadata){
            metadataJson.put("type", "numerical");
            metadataJson.put("value", ((NumericalMetadata)md).getDoubleValue());
            
        } else if(md instanceof BooleanMetadata){
            metadataJson.put("type", "boolean");
            metadataJson.put("value", ((BooleanMetadata)md).isBooleanValue());
            
        } else if(md instanceof DateMetadata){
            metadataJson.put("type", "date");
            metadataJson.put("value", ((DateMetadata)md).getDateValue());
            
        } else {
            throw new Exception("Invalid metadata type");
            
        }
        
        return metadataJson;
    }
            
    /** Get metadata for an object */
    private void getMetadata(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("documentId");
            List metadata = EJBLocator.lookupMetaDataBean().getObjectMetadata(ticket, id);
            JSONArray metadataArray = new JSONArray();
            for(int i=0;i<metadata.size();i++){
                metadataArray.put(createMetadataJson((MetadataItem)metadata.get(i)));
            }
            
            resultObject.put("metadata", metadataArray);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Add some metadat to an object */
    private void addMetadata(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("documentId");
            
            String type = dataObject.getString("type");
            MetadataItem item;
            
            if(type.equals("text")){
                item = new TextMetadata();
                ((TextMetadata)item).setTextValue(dataObject.getString("value"));
                
            } else if (type.equals("numerical")){
                item = new NumericalMetadata();
                ((NumericalMetadata)item).setDoubleValue(dataObject.getDouble("value"));
                
            } else if (type.equals("date")){
                item = new DateMetadata();
                ((DateMetadata)item).setDateValue(new Date(dataObject.getLong("value")));
                
            } else if (type.equals("boolean")){
                item = new BooleanMetadata();
                ((BooleanMetadata)item).setBooleanValue(dataObject.getBoolean("value"));
            } else {
                throw new Exception("Unknown metadata type");
            }
            
            if(item!=null){
                if(dataObject.has("category") && !dataObject.getString("category").isEmpty()){
                    item.setCategory(dataObject.getString("category").trim());
                } else {
                    item.setCategory(null);
                }
                
                item.setObjectId(id);
                item.setUserId(ticket.getUserId());
                if(dataObject.has("name") && !dataObject.getString("name").isEmpty()){
                    item.setName(dataObject.getString("name").trim());
                } else {
                    item.setName(null);
                }
                
                EJBLocator.lookupMetaDataBean().addMetadata(ticket, id, item);
                APIUtils.setSuccess(resultObject);
            } else {
                throw new Exception("No metadata created");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Delete a piece of metadata */
    private void removeMetadata(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            String documentId = dataObject.getString("documentId");
            EJBLocator.lookupMetaDataBean().removeMetadata(ticket, documentId, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** List all of the search folders for a user */
    private void listSearchFolders(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List results = EJBLocator.lookupObjectDirectoryBean().getOwnedObjectsOrderByTimeDesc(ticket, ticket.getUserId(), SearchFolder.class, 0, 0);
            JSONArray searchFolders = new JSONArray();
            for(int i=0;i<results.size();i++){
                searchFolders.put(createSearchFolderJson((SearchFolder)results.get(i)));
            }
                    
            resultObject.put("searchFolders", searchFolders);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get a specific search folder including JSON for the metadata query items */
    private void getSearchFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, id);
                    
            if(f instanceof SearchFolder){
                SearchFolder sf = (SearchFolder)f;
                
                JSONObject folderJson = createSearchFolderJson(sf);
                
                String searchQuery = sf.getQueryJson();
                MetadataQuery q = null;
                try {
                     JSONObject query = new JSONObject(searchQuery);
                     q = new MetadataQuery();
                     q.readJson(query);
                     
                } catch (Exception e){
                     q = new MetadataQuery();
                }
                
                folderJson.put("query", q.toJson());
                resultObject.put("folder", folderJson);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Object is not a search folder");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Create a new search folder */
    private void createSearchFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String name = dataObject.getString("name");
            SearchFolder f = new SearchFolder();
            f.setCreatorId(ticket.getUserId());
            f.setName(name);
            f.setDescription("Newly created search folder");
            
            MetadataQuery q = new MetadataQuery();
            f.setQueryJson(q.toJson().toString());
                    
            f = (SearchFolder)EJBLocator.lookupStorageBean().updateFolder(ticket, f);
            JSONObject searchJson = createSearchFolderJson(f);
            searchJson.put("query", q.toJson());
            resultObject.put("folder", searchJson);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Remove a search folder */
    private void removeSearchFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String id = dataObject.getString("id");
            EJBLocator.lookupStorageBean().removeFolderTree(ticket, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Create a new query item */
    private void createQueryItem(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String itemId= dataObject.getString("itemId");
            MetadataQueryItem item = null;
            String label = "";
            if(itemId.equals("boolean-item")){
                item = new BooleanMetadataQueryItem();
                
            } else if(itemId.equals("date-range-item")){
                item = new DateRangeMetadataQueryItem();
                
            } else if(itemId.equals("numerical-value-item")){
                item = new ExactNumericalMetadataQueryItem();
                
            } else if(itemId.equals("numerical-range-item")){
                item = new NumericalRangeMetadataQueryItem();
                
            } else if(itemId.equals("text-value-item")){
                item = new TextMetadataQueryItem();
            }
                    
            if(item!=null){
                JSONObject itemJson = item.toJson();
                resultObject.put("item", itemJson);
                APIUtils.setSuccess(resultObject);
            } else{
                APIUtils.setError(resultObject, "Cannot create metadata query item: " + itemId);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Save the query data */
    private void saveSearchFolder(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONObject folderJson = dataObject.getJSONObject("folder");
            
            String id = folderJson.getString("id");
            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, id);
            if(f instanceof SearchFolder){
                SearchFolder folder = (SearchFolder)f;
                
                if(folderJson.has("name") && !folderJson.getString("name").trim().isEmpty()){
                    folder.setName(folderJson.getString("name").trim());
                }
                
                if(folderJson.has("description")){
                    folder.setDescription(folderJson.getString("description"));
                }
                
                if(folderJson.has("query")){
                    JSONObject queryJson = folderJson.getJSONObject("query");
                    MetadataQuery q = new MetadataQuery();
                    q.readJson(queryJson);
                    folder.setQueryJson(q.toJson().toString());
                }
                
                folder = (SearchFolder)EJBLocator.lookupStorageBean().updateFolder(ticket, folder);
                
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Folder is not a search folder");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}

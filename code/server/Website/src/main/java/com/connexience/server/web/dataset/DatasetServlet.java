/*
 * DatasetServlet.java
 */
package com.connexience.server.web.dataset;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.dashboard.Dashboard;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetCatalog;
import com.connexience.server.model.datasets.DatasetCatalogItem;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.datasets.DatasetQueryFactory;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.model.datasets.items.SingleValueItem;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.JSONContainer;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import org.pipeline.core.data.Data;

import org.pipeline.core.data.io.DelimitedTextDataStreamImporter;
import org.pipeline.core.data.io.JsonRowArrayExporter;

/**
 * This servlet provides access to dataset data for the website
 * @author hugo
 */
public class DatasetServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }

                    if (method.equalsIgnoreCase("listDatasets")) {
                        listDatasets(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listAllDatasets")){
                        listAllDatasets(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("resetDataset")){
                        resetDataset(ticket, dataObject, resultObject, request);
                        
                    } else if(method.equalsIgnoreCase("deleteDataset")){
                        deleteDataset(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getDatasetDefinition")){
                        getDatasetDefinition(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getCatalog")){
                        getCatalog(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("addItemToDataset")){
                        addItemToDataset(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("removeItemFromDataset")){
                        removeItemFromDataset(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("updateDataset")){
                        updateDataset(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("performQuery")){
                        performQuery(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("queryAll")){
                        queryAll(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("createDataset")){
                        createDataset(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("saveDataSetItemProperties")){
                        saveDataSetItemProperties(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getItemInfo")){
                        getItemInfo(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("queryMultiRowItem")){
                        queryMultiRowItem(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("querySingleRowItem")){
                        querySingleRowItem(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("addCSVFileToMultiRowItem")){
                        addCSVFileToMultiRowItem(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("resetDatasetItem")){
                        resetDatasetItem(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listQueriesForItem")){
                        listQueriesForItem(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getDashboardLayout")){
                        getDashboardLayout(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("saveDashboardLayout")){
                        saveDashboardLayout(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("createNewDashboard")){
                        createNewDashboard(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("deleteDashboard")){
                        deleteDashboard(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("getDashboardDetails")){
                        getDashboardDetails(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("setDashboardDetails")){
                        setDashboardDetails(ticket, dataObject, resultObject);
                        
                    }
                }
                
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /** List the datasets available to the user */
    private void listDatasets(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONArray datasets = new JSONArray();
            List results = EJBLocator.lookupDatasetsBean().listDatasets(ticket, false);
            for(int i=0;i<results.size();i++){
                datasets.put(createDatasetJson((Dataset)results.get(i)));
            }
            
            resultObject.put("datasets", datasets);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** List all of the datasets including system datasets */
    private void listAllDatasets(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONArray datasets = new JSONArray();
            List results = EJBLocator.lookupDatasetsBean().listDatasets(ticket, true);
            for(int i=0;i<results.size();i++){
                datasets.put(createDatasetJson((Dataset)results.get(i)));
            }
            
            resultObject.put("datasets", datasets);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }    
    
    /** Reset a data set item */
    private void resetDatasetItem(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String datasetId = dataObject.getString("datasetId");
            String itemName = dataObject.getString("itemName");
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
            if(item!=null){
                EJBLocator.lookupDatasetsBean().resetDatasetItem(ticket, item.getId());
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such item: " + itemName);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Reset a dataset */
    private void resetDataset(Ticket ticket, JSONObject dataObject, JSONObject resultObject, HttpServletRequest request){
        try {
            String id = dataObject.getString("id");
            EJBLocator.lookupDatasetsBean().resetDataset(ticket, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Delete a dataset */
    private void deleteDataset(Ticket tickt, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            EJBLocator.lookupDatasetsBean().removeDataset(tickt, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get information for a DatasetItem */
    private void getItemInfo(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String itemName = dataObject.getString("itemName");
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, id, itemName);
            if(item!=null){
                int size = EJBLocator.lookupDatasetsBean().getMultipleValueItemSize(ticket, id, itemName);
                resultObject.put("itemLength", size);
                resultObject.put("name", item.getName());
                if(size>0){
                    JSONContainer container = EJBLocator.lookupDatasetsBean().queryMultipleValueItem(ticket, id, itemName, 0, 1);
                    resultObject.put("firstRow", container.getJSONObject());
                    resultObject.put("containsData", true);
                } else {
                    resultObject.put("containsData", false);
                }
            } else {
                APIUtils.setError(resultObject, "No such item: " + itemName);
            }
                
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get a dataset definition */
    private void getDatasetDefinition(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            Dataset db = EJBLocator.lookupDatasetsBean().getDataset(ticket, id);
            JSONObject datasetJson = createDatasetJson(db);
            
            // Create the individual items
            List items = EJBLocator.lookupDatasetsBean().getDatasetItems(ticket, id);
            JSONArray itemArray = new JSONArray();
            for(int i=0;i<items.size();i++){
                itemArray.put(createDatasetItemJson((DatasetItem)items.get(i)));
            }
            datasetJson.put("items", itemArray);
            resultObject.put("dataset", datasetJson);
            
            // Add in the catalog as well
            Iterator<DatasetCatalogItem> catalogiItems = DatasetCatalog.listNonSystemItems().iterator();
            JSONArray catalog = new JSONArray();
            while(catalogiItems.hasNext()){
                catalog.put(createDatasetCatalogItemJson(catalogiItems.next()));
            }
            resultObject.put("catalog", catalog);
            
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Save the properties for an item */
    private void saveDataSetItemProperties(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long itemId = dataObject.getLong("itemId");
            JSONObject properties = dataObject.getJSONObject("properties");
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, itemId);
            if(item!=null){
                item.readJson(properties);
                item = EJBLocator.lookupDatasetsBean().saveDatasetItem(ticket, item);
                resultObject.put("item", createDatasetItemJson(item));
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such dataset item");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get the dataset item catalog */
    private void getCatalog(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            Iterator<DatasetCatalogItem> items = DatasetCatalog.listNonSystemItems().iterator();
            JSONArray catalog = new JSONArray();
            while(items.hasNext()){
                catalog.put(createDatasetCatalogItemJson(items.next()));
            }
            resultObject.put("catalog", catalog);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
   
    /** Add an item to a dataset */
    private void addItemToDataset(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String itemId = dataObject.getString("itemId");
            String name = dataObject.getString("itemName");
            
            Dataset db = EJBLocator.lookupDatasetsBean().getDataset(ticket, id);
            
            DatasetCatalogItem catalogItem = DatasetCatalog.getItem(itemId);
            DatasetItem item = catalogItem.createItem(name, db.getId());
            item = EJBLocator.lookupDatasetsBean().saveDatasetItem(ticket, item);
            
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Remove and item from a dataset */
    private void removeItemFromDataset(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            long id = dataObject.getLong("id");
            EJBLocator.lookupDatasetsBean().removeDatasetItem(ticket, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Update a dataset details */
    private void updateDataset(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String name = dataObject.getString("name");
            String description = dataObject.getString("description");
            
            Dataset db = EJBLocator.lookupDatasetsBean().getDataset(ticket, id);
            if(name!=null && !name.trim().isEmpty()){
                db.setName(name.trim());
            }
            
            if(description!=null && !description.trim().isEmpty()){
                db.setDescription(description.trim());
            }
            db = EJBLocator.lookupDatasetsBean().saveDataset(ticket, db);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Perform a query by posting the Query JSON structure */
    private void performQuery(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            JSONObject queryJson = dataObject.getJSONObject("query");
            if(queryJson.has("_className")){
                String className = queryJson.getString("_className");
                DatasetQuery q = DatasetQueryFactory.createQueryByClassname(className);
                if(q!=null){
                    q.readJson(queryJson);
                    JSONContainer results = EJBLocator.lookupDatasetsBean().performQuery(ticket, q);
                    resultObject.put("results", results.getJSONObject());
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "Error creating query");
                }
                
            } else {
                APIUtils.setError(resultObject, "Query does not have a className attribute");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Query for a page of a multi-row item */
    private void queryMultiRowItem(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String itemName = dataObject.getString("itemName");
            int rowStart = dataObject.getInt("rowStart");
            int pageSize = dataObject.getInt("pageSize");
            JSONContainer result = EJBLocator.lookupDatasetsBean().queryMultipleValueItem(ticket, id, itemName, rowStart, pageSize);
            resultObject.put("dataPage", result.getJSONObject());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get the data in a single row item */
    private void querySingleRowItem(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String itemName = dataObject.getString("itemName");
            Object value = EJBLocator.lookupDatasetsBean().getDatasetItemValue(ticket, id, itemName);
            
            // Put the correct data into the response
            if(value instanceof JSONContainer){
                if(!((JSONContainer)value).getStringData().isEmpty()){
                    resultObject.put("jsonData", ((JSONContainer)value).getJSONObject());
                } else {
                    resultObject.put("textData", "EMPTY");
                }
            } else {
                resultObject.put("textData", value.toString());
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Add a CSV file to a multi-row item */
    private void addCSVFileToMultiRowItem(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        InputStream stream = null;
        try {
            String datasetId = dataObject.getString("datasetId");
            String documentId = dataObject.getString("documentId");
            String itemName = dataObject.getString("itemName");
            Dataset ds = EJBLocator.lookupDatasetsBean().getDataset(ticket, datasetId);
            if(ds!=null){
                if(!EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, ds, Permission.WRITE_PERMISSION)){
                    throw new Exception("Access denied for dataset write");
                }
                
                DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, datasetId, itemName);
                if(item instanceof MultipleValueItem){
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    if(doc!=null){
                        DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();
                        DocumentVersion latest = EJBLocator.lookupStorageBean().getLatestVersion(ticket, doc.getId());
                        stream = StorageUtils.getInputStream(ticket, doc, latest);
                        importer.resetWithInputStream(stream);
                        importer.setChunkSize(100);

                        // Import as chunks
                        Data chunk;
                        while(!importer.isFinished()){
                            chunk = importer.importNextChunk();
                            String[] rows = new JsonRowArrayExporter(chunk).toJsonRows();
                            for(String s : rows){
                                EJBLocator.lookupDatasetsBean().quickUpdateDatasetItemWithValue(ticket, item, s);
                            }
                        }

                    } else {
                        APIUtils.setError(resultObject, "No such document: " + documentId);
                    }
                } else {
                    APIUtils.setError(resultObject, "Object: " + itemName + " is not a multi-row item");
                }
            } else {
                APIUtils.setError(resultObject, "No such dataset: " + datasetId);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        } finally {
            try {
                stream.close();
            } catch (Exception ex){}
        }
    }
            
    
    /** Query all of the data in a dataset */
    private void queryAll(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            if(id!=null && !id.trim().isEmpty()){
                List items = EJBLocator.lookupDatasetsBean().getDatasetItems(ticket, id);
                JSONArray data = new JSONArray();
                JSONObject dataJson;
                DatasetItem item;
                Object multiValue;
                Object singleValue;
                
                for(int i=0;i<items.size();i++){
                    item = (DatasetItem)items.get(i);
                    dataJson = new JSONObject();
                    dataJson.put("name", item.getName());
                    dataJson.put("id", item.getId());
                    dataJson.put("type", item.getTypeLabel());
                    
                    if(item instanceof SingleValueItem){
                        try {
                            singleValue = EJBLocator.lookupDatasetsBean().getDatasetItemValue(ticket, item);
                            dataJson.put("singleValue", singleValue);
                            dataJson.put("queryOk", true);
                            if(singleValue instanceof JSONContainer){
                                dataJson.put("isJson", true);
                            } else {
                                dataJson.put("isJson", false);
                            }
                        } catch (Exception e){
                            dataJson.put("queryOk", false);
                            dataJson.put("errorMessage", e.getMessage());
                        }
                    } else {
                        try {
                            multiValue = EJBLocator.lookupDatasetsBean().getDatasetItemValue(ticket, item);
                            if(multiValue instanceof JSONContainer){
                                dataJson.put("multiValue", ((JSONContainer)multiValue).getJSONObject());
                                dataJson.put("queryOk", true);
                            } else {
                                dataJson.put("queryOk", false);
                                dataJson.put("errorMessage", "Only JSON data is currently supported");
                            }
                        } catch (Exception e){
                            dataJson.put("queryOk", false);
                            dataJson.put("errorMessage", e.getMessage());
                        }
                    }
                    data.put(dataJson);
                }
                
                resultObject.put("data", data);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No dataset specified");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Create a new dataset */
    private void createDataset(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String name = dataObject.getString("name");
            if(!name.trim().isEmpty()){
                Dataset dataset = new Dataset();
                dataset.setName(name);
                dataset.setDescription("Newly created dataset");
                dataset = EJBLocator.lookupDatasetsBean().saveDataset(ticket, dataset);
                resultObject.put("dataset", createDatasetJson(dataset));
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No dataset name specified");
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** List the valid queries for an item */
    private void listQueriesForItem(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            String itemName = dataObject.getString("itemName");
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(ticket, id, itemName);
            if(item!=null){
                ArrayList<DatasetQuery> results = DatasetQueryFactory.createQueryTemplatesForItem(item);
                JSONArray queries = new JSONArray();
                for(DatasetQuery q : results){
                    queries.put(q.toJson());
                }
                resultObject.put("queries", queries);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such item: " + itemName);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }

    /** Create a JSON representation of a catalog item */
    private JSONObject createDatasetCatalogItemJson(DatasetCatalogItem item) throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", item.getId());
        json.put("label", item.getLabel());
        if(item.getDescription()!=null && !item.getDescription().isEmpty()){
            json.put("description", item.getDescription());
        } else {
            json.put("description", "");
        }
        return json;
    }
    
    /** Create a JSON representation of a dataset */
    private JSONObject createDatasetJson(Dataset db) throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", db.getId());
        json.put("creatorId", db.getCreatorId());
        json.put("name", db.getName());
        if(db.getDescription()!=null && !db.getDescription().isEmpty()){
            json.put("description", db.getDescription());
        } else {
            json.put("description", "");
        }
        return json;
    }
    
    /** Create a JSON representation of a dataset item */
    private JSONObject createDatasetItemJson(DatasetItem item) throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", item.getId());
        json.put("datasetId", item.getDatasetId());
        json.put("name", item.getName());
        json.put("multipleItem", item.isMultipleItem());
        json.put("type", item.getTypeLabel());
        
        // Add the json properties to the object
        json.put("properties", item.toJson());
        
        return json;
    }
    
    /** Get dashboard details */
    private void getDashboardDetails(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            Dashboard db = EJBLocator.lookupDashboardBean().getDashboard(ticket, id);
            if(db!=null){
                resultObject.put("name", db.getName());
                if(db.getDescription()!=null){
                    resultObject.put("description", db.getDescription());
                } else {
                    resultObject.put("description", "");
                }
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such dashboard");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Save dashboard name / description */
    private void setDashboardDetails(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            Dashboard db = EJBLocator.lookupDashboardBean().getDashboard(ticket, id);
            
            if(db!=null){
                if(dataObject.has("name") && !dataObject.getString("name").trim().isEmpty()){
                    db.setName(dataObject.getString("name").trim());
                }
                
                if(dataObject.has("description") && !dataObject.getString("description").trim().isEmpty()){
                    db.setDescription(dataObject.getString("description").trim());
                }
                EJBLocator.lookupDashboardBean().saveDashboard(ticket, db);
                APIUtils.setSuccess(resultObject);
                
            } else {
                APIUtils.setError(resultObject, "No such dashboard: " + id);
            }
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    /** Get a dashboard object */
    private void getDashboardLayout(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            Dashboard db = EJBLocator.lookupDashboardBean().getDashboard(ticket, id);
            
            if(db!=null){
                JSONArray dashboardJson = new JSONArray(db.getLayoutData());
                resultObject.put("dashboard", dashboardJson);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such dashboard: " + id);
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void saveDashboardLayout(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            JSONArray layout = dataObject.getJSONArray("dashboard");
            Dashboard db = EJBLocator.lookupDashboardBean().getDashboard(ticket, id);
            db.setLayoutData(layout.toString());
            db = EJBLocator.lookupDashboardBean().saveDashboard(ticket, db);
            resultObject.put("id", db.getId());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void createNewDashboard(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String name;
            if(dataObject.has("name")){
                name = dataObject.getString("name");
            } else {
                name = "New Dashboard";
            }
            
            JSONArray layout;
            if(dataObject.has("defaultLayout")){
                layout = dataObject.getJSONArray("defaultLayout");
            } else {
                layout = new JSONArray();
            }
            
            Dashboard db = new Dashboard();
            db.setName(name);
            db.setDescription("");
            db.setLayoutData(layout.toString());
            db = EJBLocator.lookupDashboardBean().saveDashboard(ticket, db);
            resultObject.put("dashboard", layout);
            resultObject.put("id", db.getId());
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void deleteDashboard(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String id = dataObject.getString("id");
            EJBLocator.lookupDashboardBean().removeDashboard(ticket, id);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}
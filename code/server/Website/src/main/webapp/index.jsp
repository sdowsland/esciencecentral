<%-- 
    Document   : index
    Created on : Sep 24, 2010, 11:15:32 AM
    Author     : hugo
--%>

<%@page contentType="text/html" pageEncoding="MacRoman"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%

    if(com.connexience.server.ejb.util.EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(null) == null) {
        response.sendRedirect("unsecured/admin/createorganisation.jsp");
    
    } else {
        response.sendRedirect("pages/front/index.jsp");
    }
%>


/* 
 * This script provides dataset management functions
 */
function DatasetManager(){
    this.divName = "";
    this.dataset = null;
    this.datasetId = null;
    this.catalog = null;
    this.inputDialog = new InputDialog();
    this.propertyEditor = new PropertyEditor();
    this.chooser = new FileChooser();
    this.confirmDialog = new ConfirmDialog();
    
    this.tableView = new MultiRowTableView();
    this.singleItemView = new SingleRowJsonView();
    
    // Add some selections for standard properties
    var strategySelections = ["replace", "minimum", "maximum", "average", "sum"];
    this.propertyEditor.selections["UpdateStrategy"] = strategySelections;
}

/** Initialise a manager dialog box */
DatasetManager.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(divName);
    if(div){    
        var manager = this;
        var dialog = $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 600,
            width: 800,
            title: "Edit dataset",
            modal: true,
            viewer: this,
            buttons: {
                'Close': function()
                {
                    $(this).dialog('close');
                }

            }
        });        
        
        var tableViewDiv = document.createElement("div");
        tableViewDiv.setAttribute("class", "dialog");
        tableViewDiv.setAttribute("id", this.divName + "_table_view");
        div.appendChild(tableViewDiv);
        this.tableView.init(this.divName + "_table_view");
        
        var singleValueDiv = document.createElement("div");
        singleValueDiv.setAttribute("class", "dialog");
        singleValueDiv.setAttribute("id", this.divName + "_single_value_view");
        div.appendChild(singleValueDiv);
        this.singleItemView.init(this.divName + "_single_value_view");
        
        var inputDialogDiv = document.createElement("div");
        inputDialogDiv.setAttribute("class", "dialog");
        inputDialogDiv.setAttribute("id", this.divName + "_input_dialog");
        div.appendChild(inputDialogDiv);
        this.inputDialog.init(this.divName + "_input_dialog");
        this.inputDialog.okCallback = function(value){
            manager.createDataset(value);
        };
        
        var confirmDiv = document.createElement("div");
        confirmDiv.setAttribute("class", "dialog");
        confirmDiv.setAttribute("id", this.divName + "_confirm_dialog");
        div.appendChild(confirmDiv);
        this.confirmDialog.init(this.divName + "_confirm_dialog");
        
        var chooserDiv = document.createElement("div");
        chooserDiv.setAttribute("class", "dialog");
        chooserDiv.setAttribute("id", this.divName + "_chooser_dialog");
        div.appendChild(chooserDiv);
        this.chooser.init(this.divName + "_chooser_dialog");
        this.chooser.okCallback = function(chooser){
            if(chooser.item){
                manager.importCsv(this.datasetId, this.itemName, chooser.selectedFileId);
                chooser.item = undefined;
                chooser.datasetId = undefined;
                chooser.itemName = undefined;
            }
        };
        
        var propertiesDialogDiv = document.createElement("div");
        propertiesDialogDiv.setAttribute("class", "dialog");
        propertiesDialogDiv.setAttribute("id", this.divName + "_properties_dialog");
        div.appendChild(propertiesDialogDiv);
        this.propertyEditor.init(this.divName + "_properties_dialog");
        this.propertyEditor.okCallback = function(properties){
            if(manager.dataset){
                var cb = function(){
                    manager.fetchDatasetDefinition(manager.dataset.id);
                };
                jsonCall({itemId: this.item.id, properties: properties}, "../../servlets/dataset?method=saveDataSetItemProperties", cb);
            }            
            this.itemId = undefined;
            this.item = null;
        };
    }
};

DatasetManager.prototype.editDataset = function(id){
    this.datasetId = id;
    this.dataset = null;
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = "Fetching...";
        if(id!==undefined && id!==null){
            $(div).dialog({title: "Edit dataset [" + id + "]"});
        } else {
            $(div).dialog({title: "Edit dataset"});
        }
        
        $(div).dialog('open');
        this.fetchDatasetDefinition(id);
    }
};

DatasetManager.prototype.fetchDatasetDefinition = function(id, callback){
    var dialog = this;
    var cb = function(o){
        if(!o.error){
            if(callback){
                // Use external callback
                callback(o.dataset);
            } else {
                // Display in dialog
                if(o.catalog){
                    dialog.catalog = o.catalog;
                }
                dialog.displayDatasetDefinition(o.dataset);
            }
        }
    };
    jsonCall({id: id}, "../../servlets/dataset?method=getDatasetDefinition", cb);    
};

/** Reset and remove all data from a dataset */
DatasetManager.prototype.resetDataset = function(id, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({id: id}, "../../servlets/dataset?method=resetDataset", cb);
};

/** Delete a dataset */
DatasetManager.prototype.deleteDataset = function(id, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({id: id}, "../../servlets/dataset?method=deleteDataset", cb);    
};

/** Display the editor window for a dataset */
DatasetManager.prototype.displayDatasetDefinition = function(dataset){
    this.dataset = dataset;
    var div = document.getElementById(this.divName);
    if(div){
        var i;
        var item;
        var html='<div class="dialogheading">Dataset details:</div>';
        html+='<form onsubmit="return false;">';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="dbname">Dataset name:</label>';
        html+='<input name="dbname" type="text" id="' + this.divName + '_dbname" size="20" value="' + dataset.name + '"/>';
        html+='</div>';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="dbdescription">Dataset description:</label>';
        html+='<input name="dbdescription" type="text" id="' + this.divName + '_dbdescription" size="20" value="' + dataset.description + '"/>';
        html+='<input type="button" class="btn higherButton" id="' + this.divName + '_update_db_button" value="Save"/>';
        html+='</div>';        
        html+='</form><hr class="smallHr">';
        html+='<div class="dialogheading">Add dataset data items:</div>';
        html+='<form onsubmit="return false;">';
        html+='<div class="formInput">';

        html+='<label for="itemname">Name:</label>';
        html+='<input name="itemname" type="text" size="20" value="" id="' + this.divName + '_name_field"/>';
        html+='<select name="addmetadata" id="' + this.divName + '_item_type_select">';   
        
        // Add catalog items
        if(this.catalog){
            for(i=0;i<this.catalog.length;i++){
                html+='<option id="' + this.catalog[i].id + '">' + this.catalog[i].description + '</option>';
            }
        }
        
        html+='<input type="button" class="btn higherButton" name="additem" value="Add" id="' + this.divName + '_add_item_button"/>';
        html+='</div>';
        html+='</form>';
        html+='<hr class="smallHr"/>';        
        
        // Table of the actual items
        html+='<table id="' + this.divName + '_item_table" style="width: 100%;" class="display"><thead>';
        html+='<tr><th>Multiple</th><th>Type</th><th>Name</th><th>Action</th></tr></thead><tbody>';
        for(i=0;i<this.dataset.items.length;i++){
            item = this.dataset.items[i];
            html+='<tr>';
            if(item.multipleItem){
                html+='<td>Yes</td>';
            } else {
                html+='<td>No</td>';
            }
            html+='<td>' + item.type + '</td>';
            html+='<td>' + item.name + '</td>';
            html+='<td>';
            html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/datasets/images/cross.png") + '" id="' + this.divName + '_remove_' + item.id + '"/>';
            html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/datasets/images/database_delete.png") + '" id="' + this.divName + '_reset_' + item.id + '"/>';
            html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/datasets/images/pencil.png") + '" id="' + this.divName + '_properties_' + item.id + '"/>';    
            html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/datasets/images/folder.png") + '" id="' + this.divName + '_inspect_' + item.id + '"/>';
            if(item.multipleItem){
                html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/datasets/images/csv.png") + '" id="' + this.divName + '_import_' + item.id + '"/>';
            }
            html+='</td>';
            html+='</tr>';
        }
        html+='</tbody></table>';
        
        div.innerHTML = html;
        

        
        // Add handlers
        var dialog = this;
        var addButton = document.getElementById(this.divName + "_add_item_button");
        addButton.onclick = function(){
            var selector = document.getElementById(dialog.divName + "_item_type_select");
            var index = selector.selectedIndex;
            var typeId = selector.options[index].id;
            var name = document.getElementById(dialog.divName + "_name_field").value;
            dialog.addDatasetItem(typeId, name);
        };
        
        var updateButton = document.getElementById(this.divName + "_update_db_button");
        updateButton.onclick = function(){
            var name = document.getElementById(dialog.divName + "_dbname").value;
            var description = document.getElementById(dialog.divName + "_dbdescription").value;
            dialog.updateDataset(name, description);
        };
        
        // Add the click handlers for the delete buttons
        var img;
        for(i=0;i<this.dataset.items.length;i++){
            item = this.dataset.items[i];
            img = document.getElementById(this.divName + '_remove_' + item.id);
            if(img){
                img.itemId = item.id;
                img.onclick = function(){
                    dialog.removeDatasetItem(this.itemId);
                };
            }
            
            img = document.getElementById(this.divName + '_properties_' + item.id);
            if(img){
                img.itemId = item.id;
                img.item = this.dataset.items[i];
                img.onclick = function(){
                    dialog.editDatasetItemProperties(this.itemId, this.item);
                };
            }
            
            img = document.getElementById(this.divName + '_inspect_' + item.id);
            if(img){
                img.itemId = item.id;
                img.datasetId = dialog.datasetId;
                img.itemName = item.name;
                img.item = item;
                img.onclick = function(){
                    if(this.item){
                        if(this.item.multipleItem){
                            dialog.tableView.openDatasetItem(this.datasetId, this.itemName);
                        } else {
                            dialog.singleItemView.openDatasetItem(this.datasetId, this.itemName);
                        }
                    }
                };
            }
            
            img = document.getElementById(this.divName + '_import_' + item.id);
            if(img){
                img.itemId = item.id;
                img.datasetId = dialog.datasetId;
                img.itemName = item.name;
                img.item = item;
                img.onclick = function(){
                    dialog.chooser.item = this.item;
                    dialog.chooser.itemName = this.itemName;
                    dialog.chooser.datasetId = this.datasetId;
                    dialog.chooser.showHomeFolder();
                };
            }
            
            img = document.getElementById(this.divName + "_reset_" + item.id);
            if(img){
                img.itemId = item.id;
                img.datasetId = dialog.datasetId;
                img.itemName = item.name;
                img.item = item;
                img.onclick = function(){
                    dialog.resetItem(this.datasetId, this.itemName);
                };
            }
        }
        
        // Setup table
        $('#' + this.divName + "_item_table").dataTable({"bJQueryUI":true});        
    }
};

/** Edit the properties of a data set */
DatasetManager.prototype.editDatasetItemProperties = function(id, item){
    this.propertyEditor.itemId = id;
    this.propertyEditor.item = item;
    this.propertyEditor.editProperties(item.properties);
};

/** Set dataset name and description */
DatasetManager.prototype.updateDataset = function(name, description){
    if(this.dataset){
        var updateButton = document.getElementById(this.divName + "_update_db_button");
        var cb = function(){
            $.jGrowl("Dataset details changed");
            updateButton.disabled = false;
        };
        updateButton.disabled = true;
        jsonCall({id: this.dataset.id, name: name, description: description}, "../../servlets/dataset?method=updateDataset", cb);
    }
};

/** Remove an item from this dataset */
DatasetManager.prototype.removeDatasetItem = function(id){
    if(this.dataset){
        var dialog = this;
        var cb = function(){
            dialog.fetchDatasetDefinition(dialog.dataset.id);
        };
        jsonCall({id: id}, "../../servlets/dataset?method=removeItemFromDataset", cb);
    }    
};

/** Add an item to this dataset */
DatasetManager.prototype.addDatasetItem = function(id, name){
    if(this.dataset){
        var dialog = this;
        var cb = function(){
            dialog.fetchDatasetDefinition(dialog.dataset.id);
        };
        jsonCall({id: this.dataset.id, itemId: id, itemName: name}, "../../servlets/dataset?method=addItemToDataset", cb);
    }
};

/** Create a dataset with using a dialog box */
DatasetManager.prototype.createDatasetWithDialog = function(){
    this.inputDialog.show("NewDataset");
};

/** Add a dataset */
DatasetManager.prototype.createDataset = function(name){
    var cb = function(){
        location.reload();
    };
    jsonCall({name: name}, "../../servlets/dataset?method=createDataset", cb, cb);
};

DatasetManager.prototype.importCsv = function(datasetId, itemName, documentId, callback){
    var cb = function(o){
        if(callback){
            callback(o);
        } else {
            if(!o.error){
                $.jGrowl("Data imported");
            } else {
                $.jGrowl("Import error: " + o.message);
            }
        }
    };
    jsonCall({datasetId: datasetId, itemName: itemName, documentId: documentId}, rewriteAjaxUrl("../../servlets/dataset?method=addCSVFileToMultiRowItem") ,cb);
};

DatasetManager.prototype.resetItem = function(datasetId, itemName, callback) {
    var cb = function(o){
        if(callback){
            callback(o);
        } else {
            if(!o.error){
                $.jGrowl("Item reset");
            } else {
                $.jGrowl("Error resetting item: " + o.message);
            }
        }
    };
    jsonCall({datasetId: datasetId, itemName: itemName}, rewriteAjaxUrl("../../servlets/dataset?method=resetDatasetItem"), cb);
};
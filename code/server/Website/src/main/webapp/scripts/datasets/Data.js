/** This object represents a column in a set of data */
function Column(){
    this.columnData = new Array();
    this.name = "";
}

Column.prototype.size = function(){
    return this.columnData.length;
};

Column.prototype.appendValue = function(value){
    this.columnData.push(value);
};

Column.prototype.getValue = function(index){
    return this.columnData[index];
};

Column.prototype.getNumberValue = function(index){
    return parseFloat(this.columnData[index]);
};

Column.prototype.isValueNumerical = function(index){
    var n = this.columnData[index];
    return !isNaN(parseFloat(n)) && isFinite(n);  
};

Column.prototype.setValue = function(index, value){
    this.columnData[index] = value;
};

Column.prototype.padToLength = function(length, value){
    if(length>this.columnData.length){
        for(var i=this.columnData.length;i<length;i++){
            if(value!=undefined){
                this.columnData.push(0);
            } else {
                this.columnData.push(null);
            }
        }
    }
};

Column.prototype.minimum = function(){
    var validValue = false;
    var min = Number.POSITIVE_INFINITY;
    var value;
    for(var i=0;i<this.columnData.length;i++){
        if(this.isValueNumerical(i)){
            value = this.getNumberValue(i);
            if(value < min){
                min = value;
                validValue = true;
            }
        }
    }
    
    if(validValue){
        return parseFloat(min);
    } else {
        return Number.NaN;
    }
};

Column.prototype.maximum = function(){
    var validValue = false;
    var max = Number.NEGATIVE_INFINITY;
    var value;
    for(var i=0;i<this.columnData.length;i++){
        if(this.isValueNumerical(i)){
            value = this.getNumberValue(i);
            if(value > max){
                max = value;
                validValue = true;
            }
        }
    }
    
    if(validValue){
        return parseFloat(max);
    } else {
        return Number.NaN;
    }
};

Column.prototype.mean = function(){
    return NaN;
};

Column.prototype.std = function(){
    return NaN;
};

/* 
 * This script provides a container for some CSV data that can be loaded from 
 * user storage
 */
function Data(){
    this.columns = new Array();
    this.dataLoadedOk = false;
    this.dataLoadError = "";
}

Data.prototype.clearData = function(){
    this.columns = new Array();
};

Data.prototype.column = function(index){
    return this.columns[index];
};

Data.prototype.getColumnByName = function(name){
    for(var i=0;i<this.columns.length;i++){
        if(name===this.columns[i].name){
            return this.columns[i];
        }
    }
    return null;
};

Data.prototype.appendColumn = function(column){
    this.columns.push(column);
};

Data.prototype.getColumns = function(){
    return this.columns.length;
};

Data.prototype.getNamesArray = function(){
    var names = new Array();
    for(var i=0;i<this.columns.length;i++){
        names[i] = this.columns[i].name;
    }
    return names;
};

Data.prototype.getRows = function(){
    var rows = 0;
    for(var i=0;i<this.columns.length;i++){
        if(this.columns[i].size()>rows){
            rows = this.columns[i].size();
        }
    }
    return rows;
};


/* 
 * This script provides methods to query a dashboard for data
 */
function DatasetQueryManager(){
    this.dataset = null;
    this.datasetId = null;
    this.data = null;
    this.catalog = null;
    this.eventManager = new EventManager(this);
}

/** Set the ID of the dataset and load the definition */
DatasetQueryManager.prototype.openDataset = function(id){
    var manager = this;
    this.datasetId = id;
    var cb = function(o){
        manager.dataset = o.dataset;
        if(o.catalog){
            manager.catalog = o.catalog;
        }
        manager.eventManager.trigger("openOk");
    };
    
    var errCb = function(){
        manager.eventManager.trigger("openError");
    };
    
    this.eventManager.trigger("openStart");
    jsonCall({id: id}, "../../servlets/dataset?method=getDatasetDefinition", cb, errCb);
};

/** Get all of the data from the dataset database */
DatasetQueryManager.prototype.queryData = function(){
    this.eventManager.trigger("dataStart");
    if(this.dataset){
        var manager = this;
        var cb = function(o){
            manager.data = o.data;
            manager.eventManager.trigger("dataOk");
        };
        
        var errCb = function(){
            manager.data = null;
            manager.eventManager.trigger("dataError");
        };
        jsonCall({id: this.datasetId}, "../../servlets/dataset?method=queryAll", cb, errCb);
    } else {
        this.eventManager.trigger("dataError");
    }
};

/** Get item data by name */
DatasetQueryManager.prototype.getQueryDataByName = function(name){
    if(this.data){
        for(var i=0;i<this.data.length;i++){
            if(this.data[i].name==name){
                return this.data[i];
            }
        };
        return null;
    } else {
        return null;
    }
};

/** Get item by name formatted into an array */
DatasetQueryManager.prototype.getArrayQueryDataByName = function(name){
    var data = this.getQueryDataByName(name);
    var arrayData = null;
    if(data){
        if(data.multiValue){
            arrayData = data.multiValue.data;
        } else {
            if(data.isJson){
                arrayData = new Array();
                try {
                    arrayData[0] = JSON.parse(data.singleValue);
                } catch (err){
                    arrayData[0] = {Error: err};
                }
            } else {
                arrayData = new Array();
                arrayData[0] = {Value: data.singleValue};
            }
        }           
        return arrayData;
    } else {
        return null;
    }
};

/** Get a list of the sub-fields in an item */
DatasetQueryManager.prototype.getItemFields = function (name){
    var arrayData = this.getArrayQueryDataByName(name);
    var row;
    var names;
    var namesArray = new Array();
    var counter;
    var i;
    var j;
    var columnCount = 0;
    
    if(arrayData){
        // See how many table columns there should be
        for(i=0;i<arrayData.length;i++){
            row = arrayData[i];
            names = Object.getOwnPropertyNames(row).sort();
            if(names.length > columnCount){
                counter=0;
                for(j=0;j<names.length;j++){
                   if(!names[j].beginsWith("_")){
                       namesArray[counter] = names[j];
                       counter++;
                   }
                }
                columnCount = counter;
            }            
        }    
    }
    namesArray.sort();
    return namesArray;
};

String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
};

DatasetQueryManager.prototype.performQuery = function(query, callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o.results);
        }
    };
    
    var errCb = function(){
        
    };
    jsonCall({query: query}, rewriteAjaxUrl("../../servlets/dataset?method=performQuery"), cb, errCb);
};
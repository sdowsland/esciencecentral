/* 
 * This class provides a dialog that can be used to edit a DatasetQuery
 * object.
 */
function DatasetQueryEditor(){
    this.divName = "";
    this.okCallback = function(){};
    this.propertyEditor = new PropertyEditor();
    this.queryChooser = new MultiValueSelectDialog();
    this.multiRowViewer = new MultiRowTableView();
    this.singleRowViewer = new SingleRowJsonView();
    this.datasets = new Array();
    this.queries = new Array();
    this.datasetQuery = {};
}

DatasetQueryEditor.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var editor = this;
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 600,
            width: 800,
            title: "Edit Dataset Query",
            modal: true,
            buttons: {
                'Ok': function() {
                    if(editor.okCallback){
                        // Parse the key list
                        var keyListBox = document.getElementById(editor.divName + "_key_list");
                        if(keyListBox){
                            var keyListText = $(keyListBox).val();
                            var keyArray = new Array();
                            if(keyListText!==null && keyListText!==""){
                                keyArray = keyListText.split(",");
                            }
                            
                            if(editor.datasetQuery._hidden){
                                editor.datasetQuery._hidden.keys = keyArray;
                            } else {
                                editor.datasetQuery._hidden = {
                                    keys: keyArray,
                                };
                            }
                            
                            if(editor.datasetQuery && editor.datasetQuery._itemName){
                                if(editor.datasetQuery._hidden){
                                    editor.datasetQuery._hidden.itemName = editor.datasetQuery._itemName;
                                } else {
                                    editor.datasetQuery._hidden = {
                                        itemName: editor.datasetQuery._itemName
                                    }
                                }
                            }
                        }
                        editor.okCallback(editor.datasetQuery);
                        editor.datasetQuery = {};
                    }
                    
                    $(this).dialog('close');
                },
                'Cancel': function(){
                    $(this).dialog('close');
                }
            }            
        });
        
        var chooserDiv = document.createElement("div");
        chooserDiv.setAttribute("class", "dialog");
        chooserDiv.setAttribute("id", this.divName + "_chooser");
        div.appendChild(chooserDiv);
        this.queryChooser.init(this.divName + "_chooser");
        
        var singleRowDiv = document.createElement("div");
        singleRowDiv.setAttribute("class", "dialog");
        singleRowDiv.setAttribute("id", this.divName + "_single_row_div");
        div.appendChild(singleRowDiv);
        this.singleRowViewer.init(this.divName + "_single_row_div");
        
        var multipleRowDiv = document.createElement("div");
        multipleRowDiv.setAttribute("class", "dialog");
        multipleRowDiv.setAttribute("id", this.divName + "_multiple_row_div");
        div.appendChild(multipleRowDiv);
        this.multiRowViewer.init(this.divName + "_multiple_row_div");        
                
    }
};

DatasetQueryEditor.prototype.editQuery = function(datasetQuery){
    this.datasetQuery = this.cloneQuery(datasetQuery);
    this.propertyEditor.editProperties({}, true);
    this.clearQueryTypes();
    var div = document.getElementById(this.divName);
    if(div){
        var editor = this;
        var html='<div class="dialogheading">Dataset details:</div>';
        html+='<form onsubmit="return false;">';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="datasetid">Dataset name:</label>';
        html+='<select name="datasetid" id="' + this.divName + '_datasetid"></select>';
        html+='</div>';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="itemname">Item name:</label>';
        html+='<select name="itemname" id="' + this.divName + '_item_names" ></select>';
        html+='<input type="button" class="btn higherButton" style="width:80px;" value="View" id="' + this.divName + '_viewdata"></input>';
        html+='</div>'; 
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="querytype">Query type:</label>';
        html+='<input type="text" style="width:210px;" name="querytype" id="' + this.divName + '_query_type" ></input>';
        html+='<input type="button" class="btn higherButton" style="width:80px;" value="Change" id="' + this.divName + '_button"></input>';
        html+='</div>';               

        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="keylist">Filter keys:</label>';
        html+='<input type="text" style="width:210px;" name="keylist" id="' + this.divName + '_key_list" ></input>';
        html+='<input type="button" class="btn higherButton" style="width:80px;" value="Clear" id="' + this.divName + '_clear_keys"></input>';
        html+='</div>';  
        
        html+='</form><div id="' + this.divName + '_properties"></div>';
    
        div.innerHTML = html;
        this.propertyEditor.init(this.divName + "_properties", true);
        this.propertyEditor.editProperties(this.datasetQuery, true);
        
        var datasetSelector = document.getElementById(this.divName + "_datasetid");
        if(datasetSelector){
            datasetSelector.onchange = function(){
                var option = this.options[this.selectedIndex];
                if(option){
                    editor.datasetQuery._datasetId = option.datasetId;
                    editor.datasetQuery._itemName = "";
                    // Fetch the items for the dataset
                    editor.fetchDatasetItems(option.datasetId);
                    editor.clearKeyList();
                }
            };
        }
        
        var itemSelector = document.getElementById(this.divName + "_item_names");
        if(itemSelector){
            itemSelector.onchange = function(){
                var option = this.options[this.selectedIndex];
                if(option){
                    editor.datasetQuery._itemName = option.itemName;
                    
                    // Fetch the valid queries
                    if(editor.datasetQuery && editor.datasetQuery._datasetId && editor.datasetQuery._itemName){
                        editor.fetchQueryTypes(editor.datasetQuery._datasetId, editor.datasetQuery._itemName);
                    } else {
                        editor.clearQueryTypes();
                    }
                    
                    editor.clearKeyList();

                }
            };
        }
        
        var clearKeysButton = document.getElementById(this.divName + "_clear_keys");
        if(clearKeysButton){
            clearKeysButton.onclick = function(){
                editor.clearKeyList();
            };
        }
        
        var changeButton = document.getElementById(this.divName + "_button");
        if(changeButton){
            changeButton.onclick = function(){
                editor.changeQuery();
            };
        }
        
        var viewButton = document.getElementById(this.divName + "_viewdata");
        if(viewButton){
            viewButton.onclick = function(){
                var selector = document.getElementById(editor.divName + "_item_names");
                if(selector){
                    var option = selector.options[selector.selectedIndex];
                    if(option){
                        if(option.multipleItem){
                            editor.multiRowViewer.openDatasetItem(option.datasetId, option.itemName);
                        } else {
                            editor.singleRowViewer.openDatasetItem(option.datasetId, option.itemName);
                        }
                    }
                }                
            };
        }
        
        var queryLabel = document.getElementById(this.divName + "_query_type");
        if(queryLabel){
            queryLabel.setAttribute("value", this.datasetQuery._label);
        }
        
        var keyList = document.getElementById(this.divName + "_key_list");
        if(keyList){
            if(this.datasetQuery && this.datasetQuery._hidden && this.datasetQuery._hidden.keys){
                $(keyList).val(this.datasetQuery._hidden.keys.join());
            } else {
                $(keyList).val("");
            }            
        }
        
        $(div).dialog('open');
        this.listDatasets();
    }       
};


DatasetQueryEditor.prototype.clearQueryTypes = function(){
    this.datasets = new Array();
    this.queries = new Array();    
};

DatasetQueryEditor.prototype.clearKeyList = function(){
    var keyList = document.getElementById(this.divName + "_key_list");
    if(keyList){
        $(keyList).val("");
    }    
};


DatasetQueryEditor.prototype.listDatasets = function(){
    var editor = this;
    var cb = function(o){
        if(o.datasets){
            editor.datasets = o.datasets;
            var datasetSelector = document.getElementById(editor.divName + "_datasetid");
            while(datasetSelector.options.length>0){
                datasetSelector.remove(0);
            }
            
            var option;
            for(var i=0;i<o.datasets.length;i++){
                option = document.createElement("option");
                option.datasetId = o.datasets[i].id;
                option.appendChild(document.createTextNode(o.datasets[i].name));
                datasetSelector.add(option);
            }
            
            if(editor.datasetQuery && editor.datasetQuery._datasetId){
                var index = -1;
                for(var i=0;i<datasetSelector.options.length;i++){
                    if(editor.datasetQuery._datasetId===datasetSelector.options[i].datasetId){
                        index = i;
                    }
                }
                if(index!=-1) {
                    datasetSelector.selectedIndex = index;
                }
                editor.fetchDatasetItems(editor.datasetQuery._datasetId);
                
            } else if(editor.datasetQuery && o.datasets.length>0){
                // Add item if there is one
                editor.datasetQuery._datasetId = o.datasets[0].id;
                editor.fetchDatasetItems(editor.datasetQuery._datasetId);
            }
            
            
        }
    };
    jsonCall({}, rewriteAjaxUrl("../../servlets/dataset?method=listAllDatasets"), cb);
};

DatasetQueryEditor.prototype.fetchDatasetItems = function(id){
    var editor = this;
    var cb = function(o){
        if(!o.error && o.dataset){
            var itemSelector = document.getElementById(editor.divName + "_item_names");
            if(itemSelector){
                while(itemSelector.options.length>0){
                    itemSelector.remove(0);
                }              
                
                var option;
                for(var i=0;i<o.dataset.items.length;i++){
                    option = document.createElement("option");
                    option.itemId = o.dataset.items[i].id;
                    option.itemName = o.dataset.items[i].name;
                    option.datasetId = o.dataset.id;
                    option.multipleItem = o.dataset.items[i].multipleItem;
                    option.appendChild(document.createTextNode(o.dataset.items[i].name));
                    itemSelector.add(option);
                }                
                
                if(editor.datasetQuery && editor.datasetQuery._itemName){
                    var index = -1;
                    for(var i=0;i<itemSelector.options.length;i++){
                        if(editor.datasetQuery._itemName===itemSelector.options[i].itemName){
                            index = i;
                        }
                    }
                    if(index!=-1) {
                        itemSelector.selectedIndex = index;
                    } else {
                        itemSelector.selectedIndex = 0;
                        if(o.dataset.items.length>0){
                            editor.datasetQuery._itemName = o.dataset.items[i].name;
                        }
                    }
                    editor.fetchQueryTypes(editor.datasetQuery._datasetId, editor.datasetQuery._itemName);
                    
                } else if(editor.datasetQuery && o.dataset.items.length>0){
                    // Add item name if there is one
                    editor.datasetQuery._itemName = o.dataset.items[0].name;
                    editor.fetchQueryTypes(editor.datasetQuery._datasetId, editor.datasetQuery._itemName);
                }

            }
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/dataset?method=getDatasetDefinition"), cb);
};

DatasetQueryEditor.prototype.fetchQueryTypes = function(datasetId, itemName){
    var editor = this;
    var cb = function(o){
        if(!o.error){
            editor.queries = o.queries;
            editor.resetQueryIfNotSupported();
        } else {
            editor.clearQueryTypes();
        }
    };
    jsonCall({id: datasetId, itemName: itemName}, rewriteAjaxUrl("../../servlets/dataset?method=listQueriesForItem"), cb);
};

DatasetQueryEditor.prototype.resetQueryIfNotSupported = function(){
    if(this.datasetQuery){
        var className = this.datasetQuery._className;
        var supported = false;
        for(var i=0;i<this.queries.length;i++){
            if(this.queries[i]._className===className){
                supported = true;
            }
        }
        
        if(!supported && this.queries.length>0){
            this.switchQuery(this.queries[0]);
        }
    }
};

DatasetQueryEditor.prototype.switchQuery = function(newQuery){
    if(this.datasetQuery){
        var datasetId = this.datasetQuery._datasetId;
        var itemName = this.datasetQuery._itemName;
        this.datasetQuery = {};
        this.datasetQuery = this.cloneQuery(newQuery);
        this.datasetQuery._datasetId = datasetId;
        this.datasetQuery._itemName = itemName;

    } else {
        // New query
        this.datasetQuery = this.cloneQuery(newQuery);

    }
    var queryLabel = document.getElementById(this.divName + "_query_type");
    if(queryLabel){
        queryLabel.setAttribute("value", this.datasetQuery._label);
    }

    this.propertyEditor.editProperties({}, false);
    this.propertyEditor.editProperties(this.datasetQuery, true);    
};

DatasetQueryEditor.prototype.cloneQuery = function(query){
    return $.extend({}, query);
};

DatasetQueryEditor.prototype.changeQuery = function(){
    var editor = this;
    this.queryChooser.okCallback = function(value, index){
        var newQuery = editor.queries[index];
        editor.switchQuery(newQuery);
    };
    
    var labels = new Array();
    for(var i=0;i<this.queries.length;i++){
        labels[i] = this.queries[i]._label;
    }
    this.queryChooser.show(labels, this.datasetQuery._label);
};
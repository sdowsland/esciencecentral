/** This function provides a CSV loader which can read data into a Data object */
function CSVLoader(){
    if(EventManager){
        this.eventManager = new EventManager();
    }
    this.reset();
}

CSVLoader.prototype.reset = function(){
    this.data = new Data();
    this.documentId = null;
    this.settings = {
        ReadHeaders: true,
        HeaderRow: 1,
        StartRow: 1,
        LimitRows: false,
        EndRow: 1,
        SubsampleInterval: 1
    };
};

CSVLoader.prototype.getProperties = function(){
    return this.settings;
};

CSVLoader.prototype.setProperties = function(properties){
    this.settings = properties;
};

CSVLoader.prototype.readFile = function(documentId){
    if(documentId){
        this.documentId = documentId;
    }

    if(this.documentId){
        var url = "../../servlets/download/data?documentid=" + this.documentId;
        var csvData = textGet(url);
        this.readCsvString(csvData /*+ "\n"*/);
        if(this.eventManager){
            this.eventManager.trigger("dataOk");
        }

    } else {
        if(this.eventManager){
            this.eventMangaer.trigger("dataError");
        }
        throw "No document ID set";
    }
};

CSVLoader.prototype.readCsvString = function(csvString){
    var arrays;
    this.data.clearData();
    
    try {
        arrays = $.csv.toArrays(csvString);
    } catch (err){
        this.data.dataLoadedOk = false;
        this.data.dataLoadError = err;
        //throw "Error parsing CSV data: " + err;
    }    
    
    if(arrays){
        // Load headers?
        var namesLoaded = false;
        var namesArray = new Array();
        var rowPosition = 0;
        var rowCount = 0;
        var dataStartRow;
        var dataEndRow;
        var column;
        var name;
        var row;
        var i;
        var j;
        
        // Load column names if required
        if(this.settings.ReadHeaders){
            var actualRow = this.settings.HeaderRow - 1;
            if(arrays.length>=actualRow){
                // Row present
                row = arrays[actualRow];
                for(i=0;i<row.length;i++){
                    namesArray.push(row[i]);
                }
                namesLoaded = true;
                dataStartRow = actualRow + this.settings.StartRow;
            } else {
                dataStartRow = this.settings.StartRow - 1;
            }
        } else {
            dataStartRow = this.settings.StartRow - 1;
        }
        
        // Load the data
        if(arrays.length>dataStartRow){
            // Work out the end row
            if(this.settings.LimitRows){
                dataEndRow = this.settings.EndRow - 1;
                if(dataEndRow>=arrays.length){
                    dataEndRow = arrays.length;
                }
            } else {
                dataEndRow = arrays.length;
            }
            
            for(rowPosition=dataStartRow;rowPosition<dataEndRow;rowPosition=rowPosition+this.settings.SubsampleInterval){
                row = arrays[rowPosition];
                for(i=0;i<row.length;i++){
                    if(i>=this.data.getColumns()){
                        // Create a new column
                        column = new Column();
                        if(namesLoaded){
                            if(i<namesArray.length){
                                name = namesArray[i];
                            } else {
                                name = "C" + i;
                            }
                            
                        } else {
                            name = "C" + i;
                        }
                        column.name = name;
                        
                        // Pad to length if needed
                        if(rowCount>0){
                            column.padToLength(rowCount - 1);
                        }
                        column.appendValue(row[i]);
                        this.data.appendColumn(column);
                    } else {
                        this.data.column(i).appendValue(row[i]);
                    }
                }
                rowCount++;
            }
        }
        this.data.dataLoadedOk = true;
        this.data.dataLoadError = "";
    } else {
        this.data.dataLoadedOk = false;
        this.data.dataLoadError = "No valid CSV data found";
    }
};

/* 
 * This script provides a dialog that can pick a dataset item
 */
function DatasetItemPicker(){
    this.divName = "";
    this.okCallback = function(datasetItemJson){};
    this.datasetId = "";
    this.itemName = "";
}

DatasetItemPicker.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var editor = this;
        var html='<form onsubmit="return false;">';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="datasetid">Dataset name:</label>';
        html+='<select name="datasetid" id="' + this.divName + '_datasetid"></select>';
        html+='</div>';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="itemname">Item name:</label>';
        html+='<select name="itemname" id="' + this.divName + '_item_names" ></select>';
        html+='</div>'; 
        html+='</form>';
        div.innerHTML = html;
        
        var datasetSelector = document.getElementById(this.divName + "_datasetid");
        if(datasetSelector){
            datasetSelector.onchange = function(){
                var option = this.options[this.selectedIndex];
                if(option){
                    editor.datasetId = option.datasetId;
                    editor.itemName = "";
                    // Fetch the items for the dataset
                    editor.fetchDatasetItems(option.datasetId);
                }
            };
        }
        
        var itemSelector = document.getElementById(this.divName + "_item_names");
        if(itemSelector){
            itemSelector.onchange = function(){
                var option = this.options[this.selectedIndex];
                if(option){
                    editor.itemName = option.itemName;
                }
            };
        }
        
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 280,
            width: 500,
            title: "Select Dataset Item",
            modal: true,
            buttons: {
                'Ok': function() {
                    if(editor.okCallback){
                        var result = {
                            datasetId: editor.datasetId,
                            itemName: editor.itemName
                        };
                        editor.okCallback(result);
                    }
                    
                    $(this).dialog('close');
                },
                'Cancel': function(){
                    $(this).dialog('close');
                }
            
            }
        });
    }
};

DatasetItemPicker.prototype.editDatasetItem = function(itemJson){
    if(itemJson.datasetId){
        this.datasetId = itemJson.datasetId;
    } else {
        this.datasetId = "";
    }
    
    if(itemJson.itemName){
        this.itemName = itemJson.itemName;
    } else {
        this.itemName = "";
    }
    
    $("#" + this.divName).dialog('open');
    this.listDatasets();
};

DatasetItemPicker.prototype.listDatasets = function(){
    var editor = this;
    var cb = function(o){
        if(o.datasets){
            editor.datasets = o.datasets;
            var datasetSelector = document.getElementById(editor.divName + "_datasetid");
            while(datasetSelector.options.length>0){
                datasetSelector.remove(0);
            }
            
            var option;
            for(var i=0;i<o.datasets.length;i++){
                option = document.createElement("option");
                option.datasetId = o.datasets[i].id;
                option.appendChild(document.createTextNode(o.datasets[i].name));
                datasetSelector.add(option);
            }
            
            
            if(editor.datasetId){
                var index = -1;
                for(var i=0;i<datasetSelector.options.length;i++){
                    if(editor.datasetId===datasetSelector.options[i].datasetId){
                        index = i;
                    }
                }
                if(index!=-1) {
                    datasetSelector.selectedIndex = index;
                }
                editor.fetchDatasetItems(editor.datasetId);
                
            } else if(o.datasets.length>0){
                // Add item if there is one
                editor.datasetId = o.datasets[0].id;
                editor.fetchDatasetItems(editor.datasetId);
            }
            
          
            
        }
    };
    jsonCall({}, rewriteAjaxUrl("../../servlets/dataset?method=listAllDatasets"), cb);
};

DatasetItemPicker.prototype.fetchDatasetItems = function(id){
    var editor = this;
    var cb = function(o){
        if(!o.error && o.dataset){
            var itemSelector = document.getElementById(editor.divName + "_item_names");
            if(itemSelector){
                while(itemSelector.options.length>0){
                    itemSelector.remove(0);
                }              
                
                var option;
                for(var i=0;i<o.dataset.items.length;i++){
                    option = document.createElement("option");
                    option.itemId = o.dataset.items[i].id;
                    option.itemName = o.dataset.items[i].name;
                    option.datasetId = o.dataset.id;
                    option.multipleItem = o.dataset.items[i].multipleItem;
                    option.appendChild(document.createTextNode(o.dataset.items[i].name));
                    itemSelector.add(option);
                }                
                
                if(editor.itemName){
                    var index = -1;
                    for(var i=0;i<itemSelector.options.length;i++){
                        if(editor.itemName===itemSelector.options[i].itemName){
                            index = i;
                        }
                    }
                    
                    if(index!=-1) {
                        itemSelector.selectedIndex = index;
                    } else {
                        itemSelector.selectedIndex = 0;
                        
                        
                        if(o.dataset.items.length>0){
                            editor.itemName = o.dataset.items[i].name;
                        }
                        
                    }

                    
                } else if(o.dataset.items.length>0){
                    // Add item name if there is one
                    editor.itemName = o.dataset.items[0].name;
                }
            }
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/dataset?method=getDatasetDefinition"), cb);
};
/** This script provides a view for a multi-row item */
if (typeof String.prototype.startsWith != 'function') {
  // see below for better implementation!
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

function MultiRowTableView(){
    this.divName = "";
    this.datasetId = "";
    this.itemName = "";
    this.itemLength = 0;
    this.rowStart = 0;
    this.pageSize = 100;
    this.rowHeight = 20;
    this.containsData = false;
    this.columnNames = new Array();
    this.dataPage = new Array();
}

MultiRowTableView.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(divName);
    if(div){
        var dialog = this;
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 600,
            width: 800,
            title: "Item contents",
            modal: true,
            resizeStop: function(event, ui){
                dialog.computePageSize();
                dialog.fetchDataPage();
            },
                    
            buttons: {
                'First' : {
                    class: "btn-secondary",
                    text: 'First',
                    click: function(){
                        dialog.rowStart = 0;
                        dialog.fetchDataPage();
                    }
                },
                'Back' : {
                    class: "btn-secondary",
                    text: '<',
                    click: function(){
                        if(dialog.rowStart>=dialog.pageSize){
                            dialog.rowStart = dialog.rowStart - dialog.pageSize;
                        } else {
                            dialog.rowStart = 0;
                        }
                        if(dialog.rowStart<0){
                            dialog.rowStart = 0;
                        }                        
                        dialog.fetchDataPage();
                    }
                },
                'Fowards' : {
                    class: "btn-secondary",
                    text: '>',
                    click: function(){
                        if(dialog.rowStart + dialog.pageSize<dialog.itemLength){
                            dialog.rowStart = dialog.rowStart + dialog.pageSize;
                        } else {
                            dialog.rowStart = dialog.itemLength - dialog.pageSize;
                        }
                        if(dialog.rowStart<0){
                            dialog.rowStart = 0;
                        }
                        dialog.fetchDataPage();
                    }
                },
                'Last' : {
                    class: "btn-secondary",
                    text: 'Last',
                    click: function(){
                        dialog.rowStart = dialog.itemLength - dialog.pageSize;
                        if(dialog.rowStart<0){
                            dialog.rowStart = 0;
                        }
                        
                        dialog.fetchDataPage();
                    }
                },
                
                'Close':  {
                    class: "btn-secondary",
                    text: 'Close',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
                
            }
        });     
    }
};

MultiRowTableView.prototype.openDatasetItem = function(datasetId, itemName){
    this.datasetId = datasetId;
    this.itemName = itemName;
    this.rowStart = 0;
    this.containsData = false;
    this.columnNames = new Array();
    this.itemLength = 0;
    this.containsData = false;
    
    // Clear the table
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = "<h4>Fetching...</h4>";
    }
    
    $(div).dialog('open');
    
    var view = this;
    var okCallback = function(o){
        if(!o.error){
            
            if(o.containsData){
                view.itemLength = o.itemLength;
                var d = o.firstRow.data[0];
                view.columnNames = new Array();
                if(d){
                    var keys = Object.keys(d);
                    for(i=0;i<keys.length;i++){
                        if(!keys[i].startsWith('_')){
                            view.columnNames.push(keys[i]);
                        }
                    }
                }
                view.columnNames.sort();
                view.containsData = true;
                
                view.computePageSize();
                view.displayTable();
                view.fetchDataPage();
            } else {
                view.containsData = false;
                view.columnNames = new Array();
                view.itemLength = 0;
            }
            
        } else {
            view.displayError(o.message);
        }
    };
    
    
    
    jsonCall({id: this.datasetId, itemName: this.itemName}, rewriteAjaxUrl("../../servlets/dataset?method=getItemInfo"), okCallback);
};

MultiRowTableView.prototype.displayError = function(errorText){
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = "<h1>Error: " + errorText + "</h1>";
    }
};

MultiRowTableView.prototype.displayTable = function(){
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = "";
        if(this.containsData){
            var table = document.createElement("table");
            table.setAttribute("width", "100%");
            var thead = this.createThead();
            table.appendChild(thead);
            
            var tbody = document.createElement("tbody");
            
            // Create the rows
            for(var i=0;i<this.dataPage.length;i++){
                tbody.appendChild(this.createTRow(this.dataPage[i], this.rowStart + i));
            }
            
            table.appendChild(tbody);
            div.appendChild(table);
        }
    }
};

MultiRowTableView.prototype.createTRow = function(rowData, rowIndex){
    var tr = document.createElement("tr");
    tr.setAttribute("style", "line-height: " + (this.rowHeight - 2) + "px;");
    var td;
    var value;
    
    // Add index column
    td = document.createElement("td");
    td.appendChild(document.createTextNode(rowIndex));
    tr.appendChild(td);
    
    for(var i=0;i<this.columnNames.length;i++){
        td = document.createElement("td");
        value = rowData[this.columnNames[i]];
        if(value!==undefined){
            td.appendChild(document.createTextNode(value));
        } else {
            td.appendChild(document.createTextNode("--"));
        }
        tr.appendChild(td);
    }
    
    // Put the database ID into the row
    if(rowData._id){
        tr.setAttribute("databaseId", rowData._id);
    }
    
    return tr;
};

MultiRowTableView.prototype.createThead = function(){
    if(this.containsData){
        var thead = document.createElement("thead");
        var tr = document.createElement("tr");
        tr.setAttribute("style", "border-bottom: solid;border-bottom-width: 1px;border-top: solid;border-top-width: 1px; text-align: left; line-height: " + (this.rowHeight - 2) + "px;");
        var th;
        th = document.createElement("th");
        th.appendChild(document.createTextNode("#"));
        tr.appendChild(th);
        
        for(var i=0;i<this.columnNames.length;i++){
            th = document.createElement("th");
            th.appendChild(document.createTextNode(this.columnNames[i]));
            tr.appendChild(th);
        }
        
        thead.appendChild(tr);
        return thead;
    } else {
        return document.createElement("thead");
    }
};

MultiRowTableView.prototype.computePageSize = function(){
    var div = document.getElementById(this.divName);
    var height = div.clientHeight - (2 * this.rowHeight);
    if(height>this.rowHeight){
        this.pageSize = Math.floor(height / this.rowHeight);
    } else {
        this.pageSize = 1;
    }
};

MultiRowTableView.prototype.fetchDataPage = function(){
    if(this.containsData){
        var div = document.getElementById(this.divName);
        if(div){
            div.innerHTML = "<h4>Fetching...</h4>";
        }
    
        var view = this;
        var cb = function(o){
            if(!o.error && o.dataPage){
                view.dataPage = o.dataPage.data;
                view.rebuildNames();
                view.displayTable();
            } else {
                view.displayError(o.message);
            }
        };
        
        jsonCall({id: this.datasetId, itemName: this.itemName, rowStart: this.rowStart, pageSize: this.pageSize}, rewriteAjaxUrl("../../servlets/dataset?method=queryMultiRowItem"), cb);
    }
};


MultiRowTableView.prototype.rebuildNames = function(){
    
    if(this.dataPage){
        var updatedNames = new Array();
        var row;
        var keys;
        for(var i=0;i<this.dataPage.length;i++){
            row = this.dataPage[i];
            if(row){
                var keys = Object.keys(row);
                for(j=0;j<keys.length;j++){
                    if(!keys[j].startsWith('_')){
                        if(updatedNames.indexOf(keys[j])===-1){
                            updatedNames.push(keys[j]);
                        }
                    }
                }                    
                
            }
        }
        this.columnNames = updatedNames.sort();
        
    }
};
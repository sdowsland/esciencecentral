/* 
 * This script displays a data item as a single row of JSON data
 */
function SingleRowJsonView(){
    this.divName = "";
    this.datasetId = "";
    this.itemName = "";
}

SingleRowJsonView.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var dialog = this;
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 600,
            width: 800,
            title: "Item contents",
            modal: true,
                    
            buttons: {
                'Close':  {
                    class: "btn-secondary",
                    text: 'Close',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
                
            }
        });             
    }
};

SingleRowJsonView.prototype.openDatasetItem = function(datasetId, itemName){
    this.datasetId = datasetId;
    this.itemName = itemName;
    
    // Clear the display
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = "<h4>Fetching...</h4>";
        $(div).dialog('open');
    }
    
    var view = this;
    var cb = function(o){
        if(!o.error){
            if(o.jsonData){
                div.innerHTML = "<pre>" + FormatJSON(o.jsonData) + "</pre>";
            } else if(o.textData){
                div.innerHTML = "<pre>" + o.textData + "</pre>";
            }
        }
    };
    
    jsonCall({id: this.datasetId, itemName: this.itemName}, rewriteAjaxUrl("../../servlets/dataset?method=querySingleRowItem"), cb);
    
};

function FormatJSON(oData, sIndent) {
    if (arguments.length < 2) {
        var sIndent = "";
    }
    var sIndentStyle = "    ";
    var sDataType = RealTypeOf(oData);

    // open object
    if (sDataType == "array") {
        if (oData.length == 0) {
            return "[]";
        }
        var sHTML = "[";
    } else {
        var iCount = 0;
        $.each(oData, function() {
            iCount++;
            return;
        });
        if (iCount == 0) { // object is empty
            return "{}";
        }
        var sHTML = "{";
    }

    // loop through items
    var iCount = 0;
    $.each(oData, function(sKey, vValue) {
        if (iCount > 0) {
            sHTML += ",";
        }
        if (sDataType == "array") {
            sHTML += ("\n" + sIndent + sIndentStyle);
        } else {
            sHTML += ("\n" + sIndent + sIndentStyle + "\"" + sKey + "\"" + ": ");
        }

        // display relevant data type
        switch (RealTypeOf(vValue)) {
            case "array":
            case "object":
                sHTML += FormatJSON(vValue, (sIndent + sIndentStyle));
                break;
            case "boolean":
            case "number":
                sHTML += vValue.toString();
                break;
            case "null":
                sHTML += "null";
                break;
            case "string":
                sHTML += ("\"" + vValue + "\"");
                break;
            default:
                sHTML += ("TYPEOF: " + typeof(vValue));
        }

        // loop
        iCount++;
    });

    // close object
    if (sDataType == "array") {
        sHTML += ("\n" + sIndent + "]");
    } else {
        sHTML += ("\n" + sIndent + "}");
    }

    // return
    return sHTML;
};

function RealTypeOf(v) {
  if (typeof(v) == "object") {
    if (v === null) return "null";
    if (v.constructor == (new Array).constructor) return "array";
    if (v.constructor == (new Date).constructor) return "date";
    if (v.constructor == (new RegExp).constructor) return "regex";
    return "object";
  }
  return typeof(v);
}
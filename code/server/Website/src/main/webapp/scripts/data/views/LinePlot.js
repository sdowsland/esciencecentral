/* 
 * This script provides a line plot for the visualiser
 */
function LinePlot(){
    this.data = null;
    this.divName = "";  
    this.allData = null;
    this.properties = {
        Variable1: "",
        Variable2: "",
        Variable3: "",
        Variable4: "",
        Variable5: ""
    }
}

/** Create default properties for the current data */
LinePlot.prototype.createDefaults = function(){
    if(this.data!=null){
        if(this.data.getColumns()>0){
            this.properties["Variable1"] = this.data.column(0).name;
        } else {
            this.properties["Variable1"] = "";
        }
    } else {
        this.properties["Variable1"] = "";
    }
        
    this.properties["Variable2"] = "";
    this.properties["Variable3"] = "";
    this.properties["Variable4"] = "";
    this.properties["Variable5"] = "";
};

/** Update this view with data */
LinePlot.prototype.update = function(divName, data, createDefaults){
    if(divName){
        this.divName = divName;
    }
    if(data){
        this.data = data;
    }
    if(createDefaults){
        this.createDefaults();
    }
    
    // Show the data
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = '<div id="' + this.divName + 'holder" style="width:100px; height:100px;"></div>';
        
        var yColumn;


        this.allData = new Array();
        var seriesData;
        var cols = this.data.getColumns();
        var rows;
        var series;
        var j;
        var variableName;
        for(var i=0;i<5;i++){
            if(i<cols){
                seriesData = new Array();
                variableName = this.properties["Variable" + (i + 1)];
                    
                if(variableName && variableName!=""){
                    yColumn = this.data.getColumnByName(variableName);
                    if(yColumn){
                        rows = yColumn.size();
                        for(j=0;j<rows;j++){
                            if(yColumn.isValueNumerical(j)){
                                seriesData.push([j, yColumn.getNumberValue(j)]);
                            }
                        }
                        
                        series = {
                            label: yColumn.name,
                            data: seriesData
                        }
                        this.allData.push(series);                        
                    }
                }
            }
        }
        this.resize();
    }
};

LinePlot.prototype.resize = function(){
    var div = document.getElementById(this.divName);
    var holder = document.getElementById(this.divName + "holder");
    if(div && holder){
        if(div.clientHeight>0){
            holder.style.height = div.style.height;
        } else {
            holder.style.height = "25px";
        }
        
        if(div.clientWidth>0){
            holder.style.width = div.style.width;
        } else {
            holder.style.width = "25px";
        }
            
        try {
            $.plot($("#" + this.divName + "holder"), this.allData, {
                series: {
                    lines: { show: true }
                }, 
                xaxis: {
                    label: "Sample Count",
                    labelPos: "high"
                }
            });
        } catch (err){}
    }
};

visualiserViewManager.registerView("Line Plot", "Chart", function(){return new LinePlot();});
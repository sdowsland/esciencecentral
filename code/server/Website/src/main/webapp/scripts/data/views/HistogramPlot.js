/* 
 * This function provides a Histogram chart for a variable 
 */
function HistogramPlot(){
    this.divName = "";
    this.data = null;
    this.addData = null;
    this.properties = {
        Variable: "",
        BinCount: 10
    }
    
}

HistogramPlot.prototype.createDefaults = function(){
    if(this.data && this.data.getColumns()>0){
        this.properties.Variable = this.data.column(0).name;
    } else {
        this.properties.Variable = "";
    }
};

HistogramPlot.prototype.update = function(divName, data, createDefaults){
    if(divName){
        this.divName = divName;
    }
    if(data){
        this.data = data;
    }
    if(createDefaults){
        this.createDefaults();
    }    
    
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = '<div id="' + this.divName + 'holder" style="width:100px; height:100px;"></div>';
        if(this.data){
            var column = this.data.getColumnByName(this.properties.Variable);
            if(column){
                var bins = this.calculateBins(column);
                this.allData = new Array();
                var seriesData = new Array();
                for(var i=0;i<bins.length;i++){
                    seriesData.push([bins[i].midPoint, bins[i].count]);
                }
                var series = {
                    label: column.name,
                    data: seriesData,
                    bars: {
                        show: true,
                        barWidth: bins[0].width,
                        fill: true,
                        lineWidth: 1
                    }
                }
                this.allData.push(series);              
            }
        }
    }
};

HistogramPlot.prototype.resize = function(){
    var div = document.getElementById(this.divName);
    var holder = document.getElementById(this.divName + "holder");
    if(div && holder){
        if(div.clientHeight>0){
            holder.style.height = div.style.height;
        } else {
            holder.style.height = "25px";
        }
        
        if(div.clientWidth>0){
            holder.style.width = div.style.width;
        } else {
            holder.style.width = "25px";
        }
            
        try {
            $.plot($("#" + this.divName + "holder"), this.allData, {
                xaxis: {
                    label: "Value",
                    labelPos: "high"
                },
                yaxis: {
                    label: "Frequency",
                    labelPos: "high"
                }
            });
        } catch (err){}
    }
};

HistogramPlot.prototype.calculateBins = function(column){
    var bins = new Array();
    var min = column.minimum();
    var max = column.maximum();
    var binCount = this.properties.BinCount;
    var bin;
    if(min!=Number.NaN && max!=Number.NaN){
        var binEdge = min;
        var binDelta = (max - min) / binCount;
        for(var i=0;i<binCount;i++){
            bin = {
                lowerBound: binEdge + (i * binDelta),
                upperBound: binEdge + (i * binDelta) + binDelta,
                midPoint: binEdge + (i * binDelta) + (binDelta / 2.0),
                width: binDelta,
                count: 0
            }
            bins.push(bin);
        }
        
        // Assign data to the bins
        var rows = column.size();
        for(i=0;i<rows;i++){
            if(column.isValueNumerical(i)){
                this.assignValueToBin(bins, column.getNumberValue(i));
            }
        }
    }
    return bins;
};

HistogramPlot.prototype.assignValueToBin = function(bins, value){
    var bin = null;
    for(var i=0;i<bins.length;i++){
        bin = bins[i];
        if(bin.lowerBound<=value && bin.upperBound>value){
            bin.count++;
        }
    }
};

visualiserViewManager.registerView("Histogram", "Chart", function(){return new HistogramPlot();});
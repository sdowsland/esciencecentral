/* 
 * This view provides an X-Y chart for a set of data
 */
function XYPlot(){
    this.data = null;
    this.divName = "";  
    this.allData = null;
    this.properties = {
        XVariable: "",
        YVariable: ""
    }
}

/** Create default properties for the current data */
XYPlot.prototype.createDefaults = function(){
    if(this.data!=null){
        if(this.data.getColumns()==1){
            this.properties.XVariable = this.data.column(0).name;
            this.properties.YVariable = this.data.column(0).name;
            
        } else if(this.data.getColumns()>1){
            this.properties.XVariable = this.data.column(0).name;
            this.properties.YVariable = this.data.column(1).name;
            
        } else {
            this.properties.XVariable = "";
            this.properties.YVariable = "";
        }
    }
};

/** Update this view with data */
XYPlot.prototype.update = function(divName, data, createDefaults){
    if(divName){
        this.divName = divName;
    }
    if(data){
        this.data = data;
    }
    if(createDefaults){
        this.createDefaults();
    }
    
    // Show the data
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = '<div id="' + this.divName + 'holder" style="width:100px; height:100px;"></div>';
        
        var xColumn = this.data.getColumnByName(this.properties.XVariable);
        var yColumn = this.data.getColumnByName(this.properties.YVariable);
        if(xColumn!=null && yColumn!=null){
            this.allData = new Array();
            var seriesData = new Array();
            var rows = xColumn.size();
            if(yColumn.size()<rows){
                rows = yColumn.size();
            }
            
            for(var i=0;i<rows;i++){
                if(xColumn.isValueNumerical(i) && yColumn.isValueNumerical(i)){
                    seriesData.push([xColumn.getNumberValue(i), yColumn.getNumberValue(i)]);
                }
            }
            
            var series = {
                label: xColumn.name + "-v-" + yColumn.name,
                data: seriesData
            }
            
            this.allData.push(series);
            this.resize();
        } else {
            div.innerHTML = "<h1>Cannot access data columns</h1>";
        }
    }
};

XYPlot.prototype.resize = function(){
    var div = document.getElementById(this.divName);
    var holder = document.getElementById(this.divName + "holder");
    if(div && holder){
        if(div.clientHeight>0){
            holder.style.height = div.style.height;
        } else {
            holder.style.height = "25px";
        }
        
        if(div.clientWidth>0){
            holder.style.width = div.style.width;
        } else {
            holder.style.width = "25px";
        }
            
        try {
            $.plot($("#" + this.divName + "holder"), this.allData, {
                series: {
                    lines: { show: false },
                    points: { show: true },
                    color: "black"
                }, 
                xaxis: {
                    label: this.properties.XVariable,
                    labelPos: "high"
                },
                yaxis: {
                    label: this.properties.YVariable,
                    labelPos: "high"
                }
            });
        } catch (err){}
    }
};

visualiserViewManager.registerView("XY Plot", "Chart", function(){return new XYPlot();});
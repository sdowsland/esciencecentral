/* 
 * This script provides a dialog box that can be used to crop an image
 */

//Requires
//<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
//<script type="text/javascript" src="../../scripts/image/jquery.Jcrop.min.js"></script>
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/image/jquery.Jcrop.css">


function ImageCropperDialog(){
    this.divName = "";
    this.documentId = "";
    this.fileChooser = new FileChooser();
    this.x = 0;
    this.y = 0;
    this.w = 0;
    this.h = 0;
    this.okCallback = null;
}

ImageCropperDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(divName);
    
    // Toolbar
    this.createToolbar();
    
    // File chooser div
    var fileChooserDiv = document.createElement("div");
    fileChooserDiv.setAttribute("id", this.divName + "_chooser");
    div.appendChild(fileChooserDiv);
    this.fileChooser.init(this.divName + "_chooser");
    var dialog = this;
    this.fileChooser.okCallback = function(){
        dialog.fileSelected();
    }
    
    // Image panel div
    var imageDiv = document.createElement("div");
    imageDiv.setAttribute("id", this.divName + "_image_div");
    imageDiv.setAttribute("style", "position: absolute; top: 35px; width: 95%; height: 90%;");
    
    var img = document.createElement("img");
    img.setAttribute("id", this.divName + "_image");
    
    imageDiv.appendChild(img);
    div.appendChild(imageDiv);

    // Create the dialog
    $("#" + divName).dialog({
    autoOpen: false,
    height: 420,
    width: 600,
    title: "Manage profile image",
    resizable: true,
    buttons: {
      "OK": function() {
          dialog.cropImage();
      }, 
      Cancel: function(){
          $(this).dialog("close");
      }
    }
    });    
};

ImageCropperDialog.prototype.show = function(documentId){
    $("#" + this.divName).dialog('open');  
    if(documentId){
        this.documentId = documentId;
        
        // Fetch the image
    } else {
        // Nothing
    }
}

ImageCropperDialog.prototype.createToolbar = function(){
    var div = document.getElementById(this.divName);
    
    var toolbarDiv = document.createElement("div");
    toolbarDiv.setAttribute("style", "position: absolute; top:2px; left:2px; width:99% ; height: 30px");

    var addButton = document.createElement("div");
    addButton.appendChild(document.createTextNode("Select Image"));
    $(addButton).button({icons:{primary: "ui-icon-plusthick"}});
    toolbarDiv.appendChild(addButton);
    var dialog = this;
    addButton.onclick = function(){
        dialog.showChooser();
    };


    div.appendChild(toolbarDiv);    
};

ImageCropperDialog.prototype.showChooser = function(){
    this.fileChooser.showHomeFolder();
}

ImageCropperDialog.prototype.fileSelected = function(){
    this.documentId = this.fileChooser.selectedFileId;
    var img = document.getElementById(this.divName + "_image");
    img.setAttribute("src", rewriteAjaxUrl("../../servlets/download?documentid=" + this.documentId));
    
    var dialog = this;
    var onchange = function(c){
        dialog.x = c.x;
        dialog.y = c.y;
        dialog.w = c.w;
        dialog.h = c.h;
    }
    jQuery('#' + this.divName + "_image").Jcrop({
        onChange: onchange,
        onSelect: onchange
    });    
};


ImageCropperDialog.prototype.cropImage = function(){
    if(this.documentId){
        var callData = {
            documentId: this.documentId,
            x: this.x,
            y: this.y,
            h: this.h,
            w: this.w
        };
        
        var dialog = this;
        var cb = function(){
            $("#" + dialog.divName).dialog('close');
            if(dialog.okCallback){
                dialog.okCallback();
            }
        }
        jsonCall(callData, "../../servlets/profile?method=setProfileImage", cb);
    }
}
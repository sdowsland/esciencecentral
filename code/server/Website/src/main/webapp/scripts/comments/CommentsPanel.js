/* 
 * This javascript file provides a panel that contains a list of comments
 */

function CommentsPanel(){
    this.divName = "";
    this.manager = new CommentsManager();
    this.objectId = null;
    this.commentsArray = null;
    this.editingComment = null;
    tinyMCE.init({
            // General options
            mode : "none",
            theme : "advanced",
            plugins: "save",
            theme_advanced_buttons1 : "save,mylistbox,mysplitbutton,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,undo,redo,link,unlink",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom"
    });
}

CommentsPanel.prototype.scrollToLastComment = function(){
    var div = document.getElementById(this.divName);
    div.scrollTop = div.scrollHeight;
}

CommentsPanel.prototype.setObjectId = function(objectId){
    this.objectId = objectId;
}

CommentsPanel.prototype.init = function(divName){
    this.divName = divName;
};

CommentsPanel.prototype.fetchObjectComments = function(){
    var panel = this;
    this.clearComments();
    var callback = function(comments){
        this.editingComment = null;
        panel.commentsArray = comments;
        panel.displayComments();
    }
    this.manager.fetchComments(this.objectId, callback);
};

CommentsPanel.prototype.clearComments = function(){
    var i;
    var comment;
    if(this.commentsArray){
        for(i=0;i<this.commentsArray.length;i++){
            comment = this.commentsArray[i];
            tinyMCE.execCommand("mceRemoveControl", true, "editor_" + comment.id);
        }
    }
    this.editingComment = null;
    this.commentsArray = new Array();
    var div = document.getElementById(this.divName);
    div.innerHTML = "";
};

CommentsPanel.prototype.displayComments = function(){
    if(this.commentsArray){
        var mainDiv = document.getElementById(this.divName);
        mainDiv.innerHTML = "";
        var comment;
        var i;
        var commentDiv;
        var titleDiv;
        var mainTextDiv;
        var bottomTextDiv;
        var img;
        var edit;
        var del;
        var button;
        var type = "small profile";
        for(i=0;i<this.commentsArray.length;i++){
            comment = this.commentsArray[i];
            commentDiv = document.createElement("div");
            commentDiv.setAttribute("id", this.divName + "_comment_" + comment.id);
            commentDiv.setAttribute("class", "listElement");
            commentDiv.comment = comment;
            comment.commentDiv = commentDiv;

            img = document.createElement("img");
            // <img src="../../pages/demo/sjw.jpg" alt="profile picture" class="listItemImg"/>
            img.setAttribute("src", "../../servlets/image?soid=" + comment.creatorId + "&type=" + type);
            img.setAttribute("alt", "Profile picture");
            img.setAttribute("class", "listItemImg");

            titleDiv = document.createElement("div");
            titleDiv.setAttribute("class", "listTopText");
            titleDiv.appendChild(document.createTextNode(comment.authorName));

            mainTextDiv = document.createElement("div");
            mainTextDiv.setAttribute("class", "listMainText");
            mainTextDiv.innerHTML = comment.text;
            
            bottomTextDiv = document.createElement("div");
            bottomTextDiv.setAttribute("class", "listBottomText");

            edit = document.createElement("a");
            edit.appendChild(document.createTextNode("Edit"));
            edit.comment = comment;
            edit.panel = this;

            edit.onclick = function(){
                this.panel.editComment(this.comment);
            }
            bottomTextDiv.appendChild(edit);

            del = document.createElement("a");
            del.appendChild(document.createTextNode("Delete"));
            del.comment = comment;
            del.panel = this;
            del.onclick = function(){
                this.panel.deleteComment(this.comment);
            }
            bottomTextDiv.appendChild(del);

            commentDiv.appendChild(img);
            commentDiv.appendChild(titleDiv);
            commentDiv.appendChild(mainTextDiv);
            commentDiv.appendChild(bottomTextDiv);

            mainDiv.appendChild(commentDiv);
        }
    }
};

/** Create an empty unsaved comment */
CommentsPanel.prototype.createEmptyComment = function(){
    var comment = {
        id: "unsaved",
        objectId: this.objectId,
        text: ""
    }
    this.commentsArray[this.commentsArray.length] = comment;
    this.displayComments();
    this.editComment(comment);
};

/** Save a comment for the object */
CommentsPanel.prototype.addComment = function(text){
    var panel = this;
    var callback = function(comment){
        $.jGrowl("Saved comment");
        if(panel.commentsArray){
            panel.commentsArray[panel.commentsArray.length] = comment;
        }
        panel.displayComments();
    };
    this.manager.addComment(this.objectId, text, callback);
};

CommentsPanel.prototype.addAndEditComment = function(cb){
    if(this.editingComment==null){
        var panel = this;
        var callback = function(comment){
            $.jGrowl("Saved comment");
            if(panel.commentsArray){
                panel.commentsArray[panel.commentsArray.length] = comment;
            }
            panel.displayComments();
            panel.editComment(comment);
            if(cb){
                cb();
            }
        };
        this.manager.addComment(this.objectId, "", callback);
    }
};

/** Set one of the comments div up for editing */
CommentsPanel.prototype.editComment = function(comment){
    var commentDiv = comment.commentDiv;
    if(commentDiv && this.editingComment==null){
        this.editingComment = comment;
        commentDiv.innerHTML = "";
        var form = document.createElement("form");
        form.setAttribute("id", this.divName + "form_" + comment.id);
        form.setAttribute("style", "width: 100%; padding: 0px;");
        var textArea = document.createElement("TextArea");
        textArea.setAttribute("style", "width:100%;height:150px;float: right;");
        textArea.setAttribute("id", this.divName + "editor_" + comment.id);
        textArea.appendChild(document.createTextNode(comment.text));
        form.appendChild(textArea);
        form.comment = comment;
        var panel = this;
        form.submit = function(){
            var text = this.elements[0].value;
            var cb = function(newComment){
                comment.text=newComment.text;
                comment.id=newComment.id;
                comment.authorName=newComment.authorName;
                comment.timestamp = new Date(comment.timestampMillis);
                panel.displayComments();
            }
            tinyMCE.execCommand("mceRemoveControl", true, panel.divName + "editor_" + comment.id);
            panel.editingComment = null;
            panel.manager.updateComment(comment.id, text, cb, panel.objectId);
        }
        commentDiv.appendChild(form);
        tinyMCE.execCommand("mceAddControl", true, this.divName + "editor_" + comment.id);
    }
};

/** Save the current comment */
CommentsPanel.prototype.saveCurrentComment = function(){
    if(this.editingComment){
        var form = document.getElementById(this.divName + "form_" + this.editingComment.id);
        if(form){
            var panel = this;
            var comment = this.editingComment;
            form.submit = function(){
                var text = this.elements[0].value;
                var cb = function(newComment){
                    comment.text=newComment.text;
                    comment.id=newComment.id;
                    comment.authorName=newComment.authorName;
                    comment.timestamp = new Date(comment.timestampMillis);
                    panel.displayComments();
                }
                tinyMCE.execCommand("mceRemoveControl", true, panel.divName + "editor_" + comment.id);
                panel.editingComment = null;
                panel.manager.updateComment(comment.id, text, cb, panel.objectId);
            }
        }
    }
}

/** Delete a comment */
CommentsPanel.prototype.deleteComment = function(comment){
    var panel = this;
    var callback = function(){
        var index = panel.commentsArray.indexOf(comment);
        var copyComments = new Array();
        var i;
        var count = 0;
        for(i=0;i<panel.commentsArray.length;i++){
            if(i!=index){
                copyComments[count] = panel.commentsArray[i];
                count++;
            }
        }
        panel.commentsArray = copyComments;
        panel.displayComments();
    }
    if(this.editingComment && !this.editingComment.id==comment.id){
        this.editingComment = null;
    }
    
    this.manager.deleteComment(comment.id, callback);
};

/** Remove all of the comment editors */
CommentsPanel.prototype.removeAllCommentEditors = function(){
    var size = this.commentsArray.length;
    var i;
    var comment;
    for(i=0;i<size;i++){
        comment = this.commentsArray[i];
        tinyMCE.execCommand("mceRemoveControl", true, this.divName + "editor_" + comment.id);
    }
};
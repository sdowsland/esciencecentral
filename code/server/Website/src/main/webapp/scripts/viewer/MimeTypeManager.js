/* 
 * This script provides an object that manages mime type for documents
 */
//Requires
//<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>


function MimeTypeManager(){
    this.mimeMap = new List();              // Mime types and associated viewers
    this.builtinMimeMap = new List();       // All mime types with a built in viewer
    this.allMimeTypes = new List();         // All server mime types
    this.initMimeMap();
}

/** Initialise the mime maps */
MimeTypeManager.prototype.initMimeMap = function(){
    this.builtinMimeMap.clear();
    this.builtinMimeMap.add(new MimeType({extension: "txt", mimeType: "text/plain", internalViewerName: "text"}));
    this.builtinMimeMap.add(new MimeType({extension: "m", mimeType: "text/plain", internalViewerName: "text"}));
    this.builtinMimeMap.add(new MimeType({extension: "log", mimeType: "text/plain", internalViewerName: "text"}));
    this.builtinMimeMap.add(new MimeType({extension: "sh", mimeType: "text/plain", internalViewerName: "text"}));
    this.builtinMimeMap.add(new MimeType({extension: "bat", mimeType: "text/plain", internalViewerName: "text"}));
    this.builtinMimeMap.add(new MimeType({extension: "arff", mimeType: "text/arff", internalViewerName: "text"}));
    this.builtinMimeMap.add(new MimeType({extension: "csv", mimeType: "text/csv", internalViewerName: "csv"}));
    this.builtinMimeMap.add(new MimeType({extension: "xml", mimeType: "text/xml", internalViewerName: "xml"}));
    this.builtinMimeMap.add(new MimeType({extension: "jpg", mimeType: "image/jpeg", internalViewerName: "image"}));
    this.builtinMimeMap.add(new MimeType({extension: "jpeg", mimeType: "image/jpeg", internalViewerName: "image"}));
    this.builtinMimeMap.add(new MimeType({extension: "png", mimeType: "image/png", internalViewerName: "image"}));
    this.builtinMimeMap.add(new MimeType({extension: "gif", mimeType: "image/gif", internalViewerName: "image"}));
    this.builtinMimeMap.add(new MimeType({extension: "svg", mimeType: "image/svg", internalViewerName: "svg"}));
    this.builtinMimeMap.add(new MimeType({extension: "htm", mimeType: "text/html", internalViewerName: "html"}));
    this.builtinMimeMap.add(new MimeType({extension: "html", mimeType: "text/html", internalViewerName: "html"}));

    this.mimeMap.clear();
    var i;
    for(i=0;i<this.builtinMimeMap.size();i++){
        this.mimeMap.add(this.builtinMimeMap.get(i));
    }
};

/** Copy in server supplied mime preferences */
MimeTypeManager.prototype.mergeMimePreferences = function(mimePreferences){
    var size = mimePreferences.length;
    var existingMime;
    var serverMime;
    var i;
    var index;
    var typesToRemove = new List();
    var typesToAdd = new List();
    for(i=0;i<size;i++){
        serverMime = new MimeType(mimePreferences[i]);
        serverMime.internalViewer = false;
        typesToAdd.add(serverMime);
        existingMime = this.getMimeObjectByMimeType(serverMime.mimeType);
        if(existingMime){
            typesToRemove.add(existingMime);
        }
    }

    for(i=0;i<typesToRemove.size();i++){
        index = this.mimeMap.indexOf(typesToRemove.get(i));
        this.mimeMap.remove(index);
    }

    for(i=0;i<typesToAdd.size();i++){
        this.mimeMap.add(typesToAdd.get(i));
    }
};

/** Get a mime object by file extension */
MimeTypeManager.prototype.getMimeObjectByExtension = function(extension){
    var i;
    for(i=0;i<this.mimeMap.size();i++){
        if(this.mimeMap.get(i).extension==extension){
            return this.mimeMap.get(i);
        }
    }
    return null;
};

/** Get a mime object by mime type */
MimeTypeManager.prototype.getMimeObjectByMimeType = function(mimeType){
    var i;
    for(i=0;i<this.mimeMap.size();i++){
        if(this.mimeMap.get(i).mimeType==mimeType){
            return this.mimeMap.get(i);
        }
    }
    return null;
};

/** Get a basic server recognised mime type by extension */
MimeTypeManager.prototype.getServerMimeTypeByExtension = function(extension){
    var i;
    for(i=0;i<this.allMimeTypes.size();i++){
        if(this.allMimeTypes.get(i).extension==extension){
            return this.allMimeTypes.get(i);
        }
    }
    return null;
};

/** Get a built in mime type by extension */
MimeTypeManager.prototype.getBuiltinMimeTypeByExtension = function(extension){
    var i;
    for(i=0;i<this.builtinMimeMap.size();i++){
        if(this.builtinMimeMap.get(i).extension==extension){
            return this.builtinMimeMap.get(i);
        }
    }
    return null;
};

/** Work out a file extension */
MimeTypeManager.prototype.getFileExtension = function(filename)
{
  var ext = /^.+\.([^.]+)$/.exec(filename);
  //return ext == null ? "" : ext[1];
  return ext == null ? null : ext[1];
};

MimeTypeManager.prototype.parseServerMimeTypes = function(mimeTypes){
    var size = mimeTypes.length;
    var i;
    this.allMimeTypes.clear();
    for(i=0;i<size;i++){
        this.allMimeTypes.add(new MimeType(mimeTypes[i]));
    }
};

/** Save a mime type back to the server */
MimeTypeManager.prototype.saveMimeType = function(mimeType, externalCallback){
    var url = "../../servlets/viewer?method=saveMimeType";
    jsonCall({mimeType: mimeType}, url, externalCallback);
};

/** Delete a mime type */
MimeTypeManager.prototype.deleteMimeType = function(mimeTypeId, externalCallback){
    var url = "../../servlets/viewer?method=deleteMimeType";
    jsonCall({id: mimeTypeId}, url, externalCallback);
    
};

/** Build a JSON Table of the server mime types */
MimeTypeManager.prototype.getServerMimeTypeTable = function(){
    var mimeArray = new Array();
    var mimeType;
    var mimeRow;
    for(var i=0;i<this.allMimeTypes.size();i++){
        mimeType = this.allMimeTypes.get(i);
        mimeRow = new Array();
        mimeRow[0] = mimeType.mimeType;
        mimeRow[1] = mimeType.extension;
        mimeRow[2] = mimeType.description;
        mimeRow[3] = mimeType.id;
        mimeArray[i] = mimeRow;
    }
    return mimeArray;
};

/** Fetch all of the mime type preferences from the server */
MimeTypeManager.prototype.fetchMimeTypeData = function(externalCallback){
    var url = rewriteAjaxUrl("../../servlets/viewer?method=listMimeTypePreferences");
    var callback = function(o){
        if(!o.error){
            // Store list of all mime types
            if(o.mimeTypes){
                this.manager.parseServerMimeTypes(o.mimeTypes);
            }

            // Merge in the user preferences
            if(o.preferences){
                this.manager.mergeMimePreferences(o.preferences);
            }

            // Execute the callback if defined
            if(externalCallback){
                externalCallback();
            }

        } else {
            $.jGrowl("Error fetching Mime Type data: " + o.message);
        }
    };

    $.ajax({
      type: 'GET',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      success: callback,
      dataType: "json",
      manager: this
    });
};

/** Assign an application to a specific document */
MimeTypeManager.prototype.assignApplicationToDocument = function(documentId, applicationId, callback){

    // Only add mime types that the server knows about
    var url = rewriteAjaxUrl("../../servlets/viewer?method=addMimeTypePreferenceForDocument");
    var callData = {
        documentId: documentId,
        applicationId: applicationId
    };
    var callString = JSON.stringify(callData);

    var cb;
    if(callback){
        cb = function(o){
            if(!o.error){
                callback(o);
            } else {
                $.jGrowl("Error saving viewer preference: " + o.message);
            }
        };

    } else {
        cb = function(o){
            if(!o.error){
                $.jGrowl("Saved viewer preference");
            } else {
                $.jGrowl("Error saving viewer preference: " + o.message);
            }
        };
    }

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: cb,
      dataType: "json",
      manager: this
    });
};

/** Assign an applicataion to view a mime type. This works globally for all documents */
MimeTypeManager.prototype.assignApplicationToMimeType = function(extension, applicationId, callback){
    var mimeType = this.getServerMimeTypeByExtension(extension);
    if(mimeType){
        // Only add mime types that the server knows about
        var url = rewriteAjaxUrl("../../servlets/viewer?method=addMimeTypePreference");
        var callData = {
            extension: extension,
            applicationId: applicationId
        };
        var callString = JSON.stringify(callData);

        var cb;
        if(callback){
            cb = function(o){
                if(!o.error){
                    callback(o);
                } else {
                    $.jGrowl("Error saving viewer preference: " + o.message);
                }
            };

        } else {
            cb = function(o){
                if(!o.error){
                    $.jGrowl("Saved viewer preference");
                } else {
                    $.jGrowl("Error saving viewer preference: " + o.message);
                }
            };
            
        }

        $.ajax({
          type: 'POST',
          url: url,
          xhrFields: {
              withCredentials: true
          },           
          data: callString,
          success: cb,
          dataType: "json",
          manager: this
        });
    }
};

/** List applications that can view a file with an extension */
MimeTypeManager.prototype.listApplicationsForExtension = function(extension, externalCallback){
    var mimeType = this.getServerMimeTypeByExtension(extension);
    if(mimeType){
        // Only add mime types that the server knows about
        var url = rewriteAjaxUrl("../../servlets/viewer?method=listApplicationsSupportingMimeType");
        var callData = {
            mimeType: mimeType.mimeType
        };
        var callString = JSON.stringify(callData);

        var callback = function(o){
            if(!o.error){
                if(externalCallback){
                    externalCallback(o.applications);
                }
            } else {
                $.jGrowl("Error listing applications: " + o.message);
            }
        };

        $.ajax({
          type: 'POST',
          url: url,
          xhrFields: {
              withCredentials: true
          },           
          data: callString,
          success: callback,
          dataType: "json",
          manager: this
        });
    }
};
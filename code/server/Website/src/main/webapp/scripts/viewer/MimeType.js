/* 
 * This script represents a mime type
 */
function MimeType(initData){
    if(initData){
        if(initData.extension){
            this.extension = initData.extension;
        } else {
            this.extension = "";
        }

        if(initData.mimeType){
            this.mimeType = initData.mimeType;
        } else {
            this.mimeType = "";
        }

        if(initData.internalViewer){
            this.internalViewer = initData.internalViewer;
        } else {
            this.internalViewer = true;
        }

        if(initData.internalViewerName){
            this.internalViewerName = initData.internalViewerName;
        } else {
            this.internalViewerName = "text";
        }

        if(initData.applicationId){
            this.applicationId = initData.applicationId;
        } else {
            this.applicationId = null;
        }

        if(initData.description){
            this.description = initData.description;
        } else {
            this.description = "";
        }

        if(initData.id){
            this.id = initData.id;
        } else {
            this.id = null;
        }
    } else {
        this.extension = "";
        this.mimeType = "";
        this.internalViewer = true;
        this.internalViewerName = "text";
        this.applicationId = null;
        this.description = "";
        this.id = null;
        
    }
}
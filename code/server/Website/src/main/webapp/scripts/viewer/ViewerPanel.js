/* 
 * This script provides a panel that can be used to view the content of
 * files.
 */
//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
//<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>


function ViewerPanel() {
    this.divName = "";                          // Name of the div containing the popup window
    this.div = null;
    this.contentsDiv = null;                    // Div containing the actual document
    this.documentId = null;                     // ID of the document to display
    this.documentJson = null;                   // Current document details
    this.mimeManager = null;   // Manages viewer mime types
    this.iFrame = null;
    this.mimeObject = null;                     // Mime object of selected document
    this.extension = null;                      // Extension of file
    this.quotaOk = true;                        // Is the storage within quota
    this.provGraph = null;
    this.dateTimeDialog = null;
    this.viewerChooser = null;
    this.documentName = "Viewer";
    this.visualiser = null;
    this.viewFileDetailsCallback = function (o) {
        this.panel.quotaOk = o.quotaOk;
        this.panel.removeVisualiser();
        if (o.quotaOk == false) {
            // Display quota problem
            this.panel.displayQuotaErrorMessage();

        } else if (!o.error && o.quotaOk == true) {
            // Merge in the mime type preferences if there are any
            this.panel.mimeManager.initMimeMap();
            if (o.mimePreferences) {
                this.panel.mimeManager.mergeMimePreferences(o.mimePreferences);
            }

            if (o.mimeTypes) {
                this.panel.mimeManager.parseServerMimeTypes(o.mimeTypes);
            }

            if (o.documentExists) {
                this.panel.documentJson = o.document;
                if (this.forcedMime) {
                    // Document specified mime object
                    this.panel.view(this.forcedMime);
                } else {
                    // Default mime search
                    if (o.mimeOverride == true) {
                        // Document specific mime
                        if (o.overrideIsInternal == true) {
                            // View with an internal mime
                            var extension = this.panel.mimeManager.getFileExtension(o.document.name);
                            var internalMime = this.panel.mimeManager.getMimeObjectByExtension(extension);
                            if (internalMime != null) {
                                // Force internal mime
                                this.panel.view(internalMime);
                            } else {
                                // Default view
                                this.panel.view();
                            }
                        } else {
                            // View with an application
                            var appMime = {
                                internalViewer:false,
                                applicationId:o.overrideApplicationId
                            };
                            this.panel.view(appMime);
                        }

                    } else {
                        // Default search
                        this.panel.view();
                    }
                }


            } else {
                this.panel.documentJson = null;
                $.jGrowl("File cannot be found");
            }
        } else {
            $.jGrowl("Error fetching file details: " + o.message);
        }
    };
}

ViewerPanel.prototype.init = function (divName) {
    //init members
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    this.mimeManager = new MimeTypeManager();
    this.dateTimeDialog = new DateTimeDialog();
    this.viewerChooser = new ViewerChooser();


    this.divName = divName;
    this.div = document.getElementById(divName);

    var chooserDiv = document.createElement("div");
    chooserDiv.setAttribute("id", this.divName + "_chooser");
    this.div.appendChild(chooserDiv);
    this.viewerChooser.init(this.divName + "_chooser");
    var panel = this;
    this.viewerChooser.okButtonCallback = function () {
        if (panel.documentId) {
            if (panel.viewerChooser.viewerForExtension) {
                panel.showFile(panel.documentId, panel.viewerChooser.viewerForExtension);
            } else {
                panel.showFile(panel.documentId);
            }
        }
    }

    var timePickerDiv = document.createElement("div");
    timePickerDiv.setAttribute("id", this.divName + "_timepicker");
    this.div.appendChild(timePickerDiv);
    this.dateTimeDialog.init(this.divName + "_timepicker");

    this.dateTimeDialog.okCallback = function (date) {
        var valueField = document.getElementById(panel.divName + '_metadata_value_field');
        if (valueField != null) {
            valueField.value = date;
            valueField.dateValue = date;
        }
    };

    this.contentsDiv = document.createElement("div");
    this.contentsDiv.setAttribute("id", this.divName + "_contents");
    this.div.appendChild(this.contentsDiv);
};

/** Show the chooser box and refresh the view when Ok is clicked */
ViewerPanel.prototype.showViewerChooser = function () {
    if (this.mimeObject && this.mimeObject.extension) {
        this.viewerChooser.documentId = this.documentId;
        this.viewerChooser.show(this.mimeObject.extension);
    } else if (this.extension) {
        this.viewerChooser.show(this.extension);
    } else if (this.documentJson && this.documentJson.name != null) {
        var extension = this.mimeManager.getFileExtension(this.documentJson.name);
        if (extension != null) {
            this.viewerChooser.show(extension);
        } else {
            this.viewerChooser.show("");
        }

    } else {
        $.jGrowl("Unknown extension");
    }
};

/** Fetch the file details */
ViewerPanel.prototype.showFile = function (documentId, forcedMime) {
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    this.removeVisualiser();
    this.documentId = documentId;
    this.clear();
    var callData = {
        documentId:documentId
    };

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/viewer?method=getFile");

    $.ajax({
        type:'POST',
        url:url,
        xhrFields: {
            withCredentials: true
        },         
        data:callString,
        success:this.viewFileDetailsCallback,
        dataType:"json",
        panel:this,
        forcedMime:forcedMime
    });
};

/** Fetch the versions of a file */
ViewerPanel.prototype.showVersions = function (documentId) {
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    this.removeVisualiser();
    this.documentId = documentId;
    this.clear();
    var callData = {
        documentId:documentId
    };
    var panel = this;

    var callback = function (o) {
        panel.quotaOk = o.quotaOk;
        if (!o.error) {
            panel.documentJson = o.document;
            panel.viewVersions(o.versions);
        } else {
            $.jGrowl("Error fetching document versions: " + o.message);
        }
    };
    jsonCall(callData, rewriteAjaxUrl("../../servlets/viewer?method=getVersions"), callback);

}

/** Fetch the tags for a file */
ViewerPanel.prototype.showTags = function (documentId) {
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    this.removeVisualiser();
    this.documentId = documentId;
    this.clear();

    var url = rewriteAjaxUrl("../../servlets/tags?method=listTags&id=" + this.documentId);
    var dialog = this;
    var cb = function (o) {
        if (!o.error) {
            dialog.viewTags(o.tags);
        }
    };

    $.ajax({
        type:'GET',
        url:url,
        xhrFields: {
            withCredentials: true
        },         
        success:cb,
        dataType:"json"
    });
};

/** Display an XML document */
ViewerPanel.prototype.displayXml = function (mimeObject) {
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    if (this.documentJson) {
        this.iFrame = null;
        this.contentsDiv.innerHTML = "";
        var xmlDiv = document.createElement("div");
        xmlDiv.setAttribute("id", this.divName + "_xml");
        this.contentsDiv.appendChild(xmlDiv);
        var url = rewriteAjaxUrl("../../servlets/download/" + encodeURI(this.documentJson.name) + "?documentid=" + this.documentJson.id + "&forcemime=" + mimeObject.mimeType);
        LoadXML(this.divName + "_xml", url);
    }
};

/** Display an SVG */
ViewerPanel.prototype.displaySVG = function(mimeObject){
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    if (this.documentJson) {        
        this.iFrame = null;
        var url = rewriteAjaxUrl("../../servlets/download/" + encodeURI(this.documentJson.name) + "?documentid=" + this.documentJson.id);
       
        this.contentsDiv.innerHTML="";
        var obj = document.createElement("object");
        obj.setAttribute("data", url);
        obj.setAttribute("type", "image/svg+xml");
        this.contentsDiv.appendChild(obj);
    }    
};

/** Display an image */
ViewerPanel.prototype.displayImage = function (mimeObject) {
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    if (this.documentJson) {
        this.contentsDiv.innerHTML = "";
        this.iFrame = null;
        var img = document.createElement("img");
        img.setAttribute("stye", "overflow: auto");
        var url = rewriteAjaxUrl("../../servlets/download/" + encodeURI(this.documentJson.name) + "?documentid=" + this.documentJson.id + "&forcemime=" + mimeObject.mimeType);
        img.setAttribute("src", url);
        this.contentsDiv.appendChild(img);
    }
};

ViewerPanel.prototype.displayCSV = function(mimeObject){
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    if (typeof(Visualiser) == 'function') {
        if(this.documentJson){
            this.visualiser = new Visualiser();
            this.visualiser.init(this.divName + "_contents");
            this.visualiser.autoCreateView = "TableView";
            this.visualiser.readFile(this.documentJson.id);
        }
        this.resizeIFrame();
        
    } else {
        if (this.documentJson) {
            this.contentsDiv.innerHTML = "";
            this.iFrame = null;
            var url = rewriteAjaxUrl("../../servlets/download/" + encodeURI(this.documentJson.name) + "?documentid=" + this.documentJson.id + "&forcemime=" + mimeObject.mimeType);
            var tableGen = new CSVTable();
            tableGen.display(this.contentsDiv, url);
        }    
    }
};

/** Display a text file */
ViewerPanel.prototype.displayText = function (mimeObject) {
    tinyMCE.execCommand("mceRemoveControl", true, this.divName + "_editor"); 
    if (this.documentJson) {
        this.contentsDiv.innerHTML = "";
        this.iFrame = null;
        var paragraph = document.createElement("pre");
        paragraph.setAttribute("stye", "overflow: auto");
        this.contentsDiv.appendChild(paragraph);
        var url = rewriteAjaxUrl("../../servlets/download/" + encodeURI(this.documentJson.name) + "?documentid=" + this.documentJson.id + "&forcemime=" + mimeObject.mimeType);
        $(paragraph).load(url);
    }
};

/** Display and html file */
ViewerPanel.prototype.displayHtml = function (mimeObject) {
    if (this.documentJson) {
        this.contentsDiv.innerHTML = "Fetching...";

        var dialog = this;

        var url;
        if (this.documentJson.containerId) {
            url = rewriteAjaxUrl("../../servlets/web/" + this.documentJson.containerId + "/" + this.documentJson.name + "?forceid=" + this.documentJson.id);
        } else {
            url = rewriteAjaxUrl("../../servlets/download/" + encodeURI(this.documentJson.name) + "?documentid=" + this.documentJson.id + "&forcemime=" + mimeObject.mimeType);
        }

        $.ajax({
            type: 'GET',
            url: rewriteAjaxUrl(url),
            dataType: 'text',
            xhrFields: {
                withCredentials: true
            },
            success: function(result) {
                dialog.contentsDiv.innerHTML = '<div id="' + dialog.divName + '_editor">' + result + '</div>';
                tinyMCE.execCommand("mceAddControl", true, dialog.divName + "_editor"); 

            },
            data: {},
            async: true
        });

    }
};

/** Display the quota error message */
ViewerPanel.prototype.displayQuotaErrorMessage = function () {
    this.contentsDiv.innerHTML = "";
    var div = document.createElement("div");
    div.setAttribute("class", "appchooserdialog");

    var span = document.createElement("span");
    span.setAttribute("style", "color: red;");
    span.appendChild(document.createTextNode("Storage Quota has been exceeded. All uploads / downloads have been disabled"));
    div.appendChild(span);
    this.contentsDiv.appendChild(div);
};

/** Reset the iFrame height to match the container */
ViewerPanel.prototype.resizeIFrame = function () {
    if(this.contentsDiv){
        
    }
};

ViewerPanel.prototype.removeVisualiser = function(){
    if(this.visualiser){
        this.visualiser.cleanUp();
    }
    this.visualiser = null;
};

/** Clear the panel contents */
ViewerPanel.prototype.clear = function () {
    this.contentsDiv.innerHTML = "";
};

/** View a document */
ViewerPanel.prototype.view = function (forcedMimeObject) {
    var mimeObject;

    if (forcedMimeObject) {
        // Forcing in a mime object
        mimeObject = forcedMimeObject;
    } else {
        // Looking up a mime object
        mimeObject = this.mimeObject;
        this.extension = this.mimeManager.getFileExtension(this.documentJson.name);

        if (this.documentJson.mimeTypeKnown == true) {
            // Server supplied a mime type
            mimeObject = this.mimeManager.getMimeObjectByMimeType(this.documentJson.mimeType);

        } else {
            // Try and guess a mime type
            if (this.extension != null) {
                mimeObject = this.mimeManager.getMimeObjectByExtension(this.extension);
            } else {
                mimeObject = null;
            }
        }
    }


    if (mimeObject != null) {
        this.mimeObject = mimeObject;
        if (mimeObject.internalViewer) {
            // Handled internally
            if (mimeObject.internalViewerName == "text") {
                // Text document
                this.displayText(mimeObject);
                
            } else if (mimeObject.internalViewerName == "csv"){
                // CSV data
                this.displayCSV(mimeObject);
                
            } else if (mimeObject.internalViewerName == "image") {
                // Image viewer
                this.displayImage(mimeObject);
                
            } else if (mimeObject.internalViewerName == "svg") {
                // SVG viewer
                this.displaySVG(mimeObject);

            } else if (mimeObject.internalViewerName == "xml") {
                // Display XML viewer
                this.displayXml(mimeObject);

            } else if (mimeObject.internalViewerName == "html") {
                // Display an html file
                this.displayHtml(mimeObject);

            } else {
                $.jGrowl("Unknown internal viewer");
            }
        }

    }
};

ViewerPanel.prototype.viewVersions = function (versions) {
    this.contentsDiv.innerHTML = "";
    var i;
    var version;
    var div;
    var versionDiv;
    var dateDiv;
    var userDiv;
    var sizeDiv;
    var listDiv = document.createElement("div");
    var img;
    var a;
    var url;
    var dlt;
    var span;

    listDiv.setAttribute("class", "list");
    //display versions in reverse chronological order
    for (i = versions.length - 1; i >= 0; i--) {
        version = versions[i];
        div = document.createElement("div");
        div.setAttribute("class", "versionListElement");

        dlt = document.createElement("div");
        if (this.quotaOk) {
            dlt.appendChild(document.createTextNode("Download"));
        } else {
            dlt.appendChild(document.createTextNode("No Quota"));
        }

        dlt.setAttribute("class", "versionListDownloadText");

        if (this.quotaOk) {
            url = rewriteAjaxUrl("../../servlets/download/" + this.documentJson.name + "?documentid=" + version.documentId + "&versionid=" + version.id);
        } else {
            url = "#";
        }
        a = document.createElement("a");
        a.setAttribute("href", url);
        a.setAttribute("target", "_blank");
        a.setAttribute("style", "border-bottom: none");

        img = document.createElement("img");
        img.setAttribute("alt", "Download");
        if (this.quotaOk) {
            img.setAttribute("src", rewriteAjaxUrl("../../scripts/viewer/images/downarrow.png"));
        } else {
            img.setAttribute("src", rewriteAjaxUrl("../../scripts/viewer/images/quotaerror.png"));
            img.setAttribute("style", "margin-top: 5px;");
        }

        span = document.createElement("span");
        span.setAttribute("class", "versionListDownload");
        span.appendChild(dlt);
        a.appendChild(img);
        span.appendChild(a);
        div.appendChild(span);

        versionDiv = document.createElement("div");
        versionDiv.setAttribute("class", "versionListNumber");
        versionDiv.appendChild(document.createTextNode(version.versionNumber));


        div.appendChild(versionDiv);

        dateDiv = document.createElement("div");
        dateDiv.setAttribute("class", "versionListDate");
        dateDiv.appendChild(document.createTextNode(version.timestamp + " (versionId: " + version.id + ")"));

        div.appendChild(dateDiv);

        userDiv = document.createElement("div");
        userDiv.setAttribute("class", "versionListUser");
        userDiv.appendChild(document.createTextNode(version.userName));
        div.appendChild(userDiv);

        sizeDiv = document.createElement("div");
        sizeDiv.setAttribute("class", "versionListSize");
        sizeDiv.appendChild(document.createTextNode(this.formatSize(version.size)));
        div.appendChild(sizeDiv);


        listDiv.appendChild(div);


    }
    this.contentsDiv.appendChild(listDiv);
};

ViewerPanel.prototype.viewTags = function (tags) {
    var dialog = this;
    var i;
    var html = '<form onsubmit="return false;">';
    html += '<div class="formInput">';
    html += '<label for="addtag">Add a new tag:</label>';
    html += '<input name="addtag" type="text" size="15" value="" id="' + this.divName + '_tag_field"/>';
    html += '<input class="btn higherButton inlineButton" type="button" name="addtag" value="Add" id="' + this.divName + '_add_tag_button"/>';
    html += '</div>';
    html += '</form>';
    html += "<hr/>";
    if (tags.length > 0) {
        html += '<table><thead><th>Tag</th><th>Action</th><tbody>';
        for (i = 0; i < tags.length; i++) {
            html += '<tr class="tagTableRow">';
            html += '<td class="tagTableTagColumn">' + tags[i].text + '</td>';
            html += '<td><div class="tagDeleteImg" id="' + this.divName + '_remove_tag_' + i + '"></div></td>';
            html += '</tr>';

        }
        html += '</tbody></table>';
    } else {
        html += '<p>No tags defined</p>';
    }
    html += '</form>';
    this.contentsDiv.innerHTML = html;

    document.getElementById(this.divName + "_add_tag_button").onclick = function () {
        dialog.addTag();
    };

    var img;
    for (i = 0; i < tags.length; i++) {
        img = document.getElementById(this.divName + "_remove_tag_" + i);
        img.tagId = tags[i].id;
        img.onclick = function () {
            dialog.removeTag(this.tagId);
        };
    }
};

ViewerPanel.prototype.addTag = function () {
    var tag = $("#" + this.divName + "_tag_field").val();
    $("#" + this.divName + "_tag_field").val("");
    var dialog = this;
    var callback = function () {
        dialog.showTags(dialog.documentId);
    };
    jsonCall({id:this.documentId, text:tag}, "../../servlets/tags?method=addTag", callback);
};

ViewerPanel.prototype.removeTag = function (id) {
    var dialog = this;
    var callback = function () {
        dialog.showTags(dialog.documentId);
    };
    jsonCall({tagid:id, id:this.documentId}, "../../servlets/tags?method=removeTag", callback);
};


ViewerPanel.prototype.formatSize = function (filesize) {

    if (filesize >= 1073741824) {
        filesize = this.number_format(filesize / 1073741824, 2, '.', '') + ' GB';
    } else {
        if (filesize >= 1048576) {
            filesize = this.number_format(filesize / 1048576, 2, '.', '') + ' MB';
        } else {
            if (filesize >= 1024) {
                filesize = this.number_format(filesize / 1024, 0) + ' KB';
            } else {
                filesize = this.number_format(filesize, 0) + ' bytes';
            }
        }
    }
    return filesize;
};

ViewerPanel.prototype.number_format = function (number, decimals, dec_point, thousands_sep) {
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

/** Load metadata for the current document */
ViewerPanel.prototype.showMetadata = function (documentId) {
    this.removeVisualiser();
    this.documentId = documentId;
    this.clear();
    var callData = {
        documentId:documentId
    };
    var panel = this;

    var callback = function (o) {
        if (!o.error) {
            panel.viewMetadata(o.metadata);
        } else {
            $.jGrowl("Error fetching document versions: " + o.message);
        }
    };
    jsonCall(callData, "../../servlets/metadata?method=getMetadata", callback);
};

/** Display the metadata table */
ViewerPanel.prototype.viewMetadata = function (metadata) {
    var dialog = this;
    var i;
    var html = '<form style="margin-top: 10px;" onsubmit="return false;">';
    html += '<div class="formInput">';

    html += '<label style="display:inline-block; margin-right: 5px;" for="mdcategory">Category:</label>';
    html += '<input style="width:100px;" name="mdcategory" type="text" size="10" value="" id="' + this.divName + '_metadata_category_field"/>';
    html += '<label style="display:inline-block;margin-right: 5px;" for="name">Name:</label>';
    html += '<input style="width:100px;" name="name" type="text" size="10" value="" id="' + this.divName + '_metadata_name_field"/>';
    html += '<label style="display:inline-block;margin-right: 5px;" for="mdvalue">Value:</label>';
    html += '<input style="width:100px;" name="mdvalue" type="text" size="10" value="" id="' + this.divName + '_metadata_value_field"/>';
    html += '<label style="display:inline-block;margin-right: 5px;" for="addmetadta">Type:</label>';
    html += '<select style="width: 100px;" name="addmetadata" id="' + this.divName + '_metadata_type_select">';
    html += '<option>Text</option><option>Numerical</option><option>Timestamp</option><option>true/false</option></select>';
    html += '<input class="btn higherButton inlineButton" type="button" name="addmetadata" value="Add" id="' + this.divName + '_add_metadata_button"/>';
    html += '</div>';
    html += '</form>';
    html += '<hr class="smallHr"/>';
    //if(metadata.length>0){
    html += '<table class="display" style="width: 100%;" id="' + this.divName + '_metadata_table"><thead><tr><th>Type</th><th>Category</th><th>Name</th><th>Value</th><th>Del</th></tr></thead><tbody>';
    for (i = 0; i < metadata.length; i++) {
        html += '<tr>';
        html += '<td style="width: 20px;">' + metadata[i].type + '</td>';
        html += '<td>' + metadata[i].category + '</td>';
        html += '<td>' + metadata[i].name + '</td>';
        html += '<td>' + metadata[i].value + '</td>';
        html += '<td><img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/viewer/images/cross.png") + '" id="' + this.divName + '_metadata_remove_' + metadata[i].id + '"/></td>';
        html += '</tr>';

    }
    html += '</tbody></table>';
    //} else {
    //    html+='<p>No metadata defined</p>';
    //}
    html += '</form>';
    this.contentsDiv.innerHTML = html;

    // Assign callbacks
    var img;
    for (i = 0; i < metadata.length; i++) {
        img = document.getElementById(this.divName + "_metadata_remove_" + metadata[i].id);
        img.metadataId = metadata[i].id;
        if (img) {
            img.onclick = function () {
                dialog.removeMetadata(this.metadataId);
            };
        }
    }

    $('#' + this.divName + '_metadata_table').dataTable({"bJQueryUI":true});

    // Add a listener to the select box
    var selectBox = document.getElementById(this.divName + "_metadata_type_select");


    // Set up click event of the metadata value box
    selectBox.onchange = function () {
        if (selectBox.selectedIndex == 2) {
            // Initialise date picker
            document.getElementById(dialog.divName + "_metadata_value_field").onclick = function () {
                dialog.dateTimeDialog.show(this.dateValue);
            };

        } else {
            // Reset date picker
            document.getElementById(dialog.divName + "_metadata_value_field").value = "";
            document.getElementById(dialog.divName + "_metadata_value_field").dateValue = null;
            document.getElementById(dialog.divName + "_metadata_value_field").onclick = function () {
            };
        }
    };

    // Add the click handler to the add button
    document.getElementById(this.divName + "_add_metadata_button").onclick = function () {
        var category = document.getElementById(dialog.divName + "_metadata_category_field").value;
        var name = document.getElementById(dialog.divName + "_metadata_name_field").value;

        var metadata = {
            name:name,
            category:category,
            documentId:dialog.documentId
        };

        if (selectBox.selectedIndex == 0) {
            // Text metadata
            metadata.type = "text";
            metadata.value = document.getElementById(dialog.divName + "_metadata_value_field").value;

        } else if (selectBox.selectedIndex == 1) {
            metadata.type = "numerical";
            metadata.value = document.getElementById(dialog.divName + "_metadata_value_field").value;

        } else if (selectBox.selectedIndex == 2) {
            // Add some date metadata
            metadata.type = "date";
            metadata.value = document.getElementById(dialog.divName + "_metadata_value_field").dateValue.getTime();

        } else if (selectBox.selectedIndex == 3) {
            // Add true / false

        }
        dialog.addMetadata(metadata);
    };

    /*
     document.getElementById(this.divName + "_add_tag_button").onclick = function(){
     dialog.addTag();
     }

     var img;
     for(i=0;i<tags.length;i++){
     img = document.getElementById(this.divName + "_remove_tag_" + i);
     img.tagId = tags[i].id;
     img.onclick = function(){
     dialog.removeTag(this.tagId);
     }
     }*/
};

/** Upload a piece of metadata to the server */
ViewerPanel.prototype.addMetadata = function (metadata) {
    var dialog = this;
    var callback = function () {
        dialog.showMetadata(dialog.documentId);
    };
    jsonCall(metadata, "../../servlets/metadata?method=addMetadata", callback);
};

ViewerPanel.prototype.removeMetadata = function (id) {
    var dialog = this;
    var callback = function () {
        dialog.showMetadata(dialog.documentId);
    };
    jsonCall({id:id, documentId:this.documentId}, "../../servlets/metadata?method=removeMetadata", callback);
};


/** Load provenance for the current document */
ViewerPanel.prototype.showProvenance = function (documentId) {
    this.removeVisualiser();
    this.documentId = documentId;
    this.clear();
    var dialog = this;

    this.contentsDiv.innerHTML = '<div id="' + this.divName + '_prov_container" >' +
            '                <button style="margin:10px" id="' + this.divName + '_prov_DownloadPDF">Download PDF</button>' +
            '          </div>';

    $("#" + dialog.divName + "_prov_DownloadPDF").button().click(function () {

        var versionId = dialog.documentJson.latestVersionId;
        var documentId = dialog.documentJson.id;
        var documentName = dialog.documentJson.name;

        //URL of the provenance and monitoring server
        window.open("/monitor/servlets/provenance/report/Report.pdf?documentId=" + documentId + "&versionId=" + versionId +"&documentName=" + documentName, "_blank");
    });

};

ViewerPanel.prototype.showProjectDetails = function(documentId){
    this.removeVisualiser();
    this.documentId = documentId;
    this.clear();

    var url = rewriteAjaxUrl("../../servlets/viewer?method=getProjectDetails");
    var dialog = this;
    var cb = function (o) {
        if (!o.error) {
            if(o.hasProject){
                dialog.viewProjectDetails(o.project);
            } else {
                dialog.viewProjectDetails(null);
            }
        }
    };
    
    jsonCall({id: documentId}, url, cb, null);
};

ViewerPanel.prototype.viewProjectDetails = function(project){
    if(project==null){
        this.contentsDiv.innerHTML = "<p>No Project data present for document</p>";
    } else {
        var html = "<div>";
        html+="<h4>Project Details</h4><h5>Name: " + project.name + "</h5>";
        html+="<h5>Owner: " + project.owner + "</h5>";
        if(project.startDate){
            html+="<h5>Start: " + project.startDate + "</h5>";
        }   
        
        if(project.endDate){
            html+="<h5>End: " + project.startDate + "</h5>";
        }
        html+="</div><h4>Additional Attributes</h4>";
        
        html += '<table class="display" style="width: 100%;" id="' + this.divName + '_project_properties_table"><thead><tr><th>Name</th><th>Value</th></tr></thead><tbody>';
        for (i = 0; i < project.properties.names.length; i++) {
            html += '<tr>';
            html += '<td>' + project.properties.names[i] + '</td>';
            html += '<td>' + project.properties.values[i] + '</td>';
            html += '</tr>';

        }
        html += '</tbody></table>';
        this.contentsDiv.innerHTML = html;
        $('#' + this.divName + '_project_properties_table').dataTable({"bJQueryUI":true});
    }
};

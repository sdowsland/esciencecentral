/* 
 * This file provides a viewer that can be used to quickly view / preview
 * the content of a variety of files stored in the system. It is used, for
 * example, on the workflow runs tab to display graphs, text files, data
 * etc.
 */
function QuickView(){
    this.panel = new ViewerPanel();
    this.panelDiv = null;
    this.divName = null;
    this.div = null;
    this.documentId = null;
}

/** Set up with a div name */
QuickView.prototype.init = function(divName){
    this.divName = divName;
    this.div = document.getElementById(divName);
    this.div.viewer = this;
    this.createToolbar();

    this.panelDiv = document.createElement("div");
    this.panelDiv.setAttribute("id", this.divName + "_panel");
    this.panelDiv.setAttribute("style", "overflow: auto; position: absolute; top: 45px; width:97%;");
    this.div.appendChild(this.panelDiv);

    this.panel.init(this.divName + "_panel");
    var panel = this;


    var dialog = $(this.div).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 600,
        width: 800,
        title: "Viewer",
        modal: true,
        viewer: this,    
        buttons: {
            'Close': function()
            {
                $(this).dialog('close');
            }

        },
        close: function()
        {

        }
    });


    dialog.bind("dialogresize", function(){
        
        if(this.viewer){
            /*
            if(this.viewer.resizePanel){
                this.viewer.resizePanel();
            }
            */
            if(this.viewer.panel && this.viewer.panel.resizeIFrame){
                this.viewer.panel.resizeIFrame();
            }
        }
        
    });
    
};

QuickView.prototype.showFile = function(documentId, documentName){
    this.panel.clear();
    $(this.div).dialog('open');

    //try to change the title to the name of the document being viewed
    if (documentName) {
        var titleDiv = $("#ui-dialog-title-" + this.divName);
        if (titleDiv.length !== 0) //we've got the title div
        {
            titleDiv.html(documentName);
        }
    }

    this.documentId = documentId;
    this.displayVersions();
    $('a[href=#versionsContent]').tab('show');
};


/** Display the file contents */
QuickView.prototype.displayContents = function(){
    this.panel.showFile(this.documentId);
};

/** Display the file versions */
QuickView.prototype.displayVersions = function(){
    this.panel.showVersions(this.documentId);
};

/** Display the project details */
QuickView.prototype.displayProjectDetails = function(){
    this.panel.showProjectDetails(this.documentId);
};

/** Display the history of a file */
QuickView.prototype.displayHistory = function(){
    this.panel.showHistory(this.documentId);
};

QuickView.prototype.displayTags = function(){
    this.panel.showTags(this.documentId);
};

QuickView.prototype.displayMetadata = function(){
    this.panel.showMetadata(this.documentId);
};

QuickView.prototype.displayProvenance = function(){
    this.panel.showProvenance(this.documentId);
};

/** Create the toolbar */
QuickView.prototype.createToolbar = function(){

    var tabHolder = document.createElement("ul");
    tabHolder.setAttribute("id", this.divName + "_toolbar");
    tabHolder.setAttribute("class", "nav nav-tabs");

    var versions = document.createElement("li");
    var versionsLink = document.createElement("a");
    versionsLink.setAttribute("href", "#versionsContent");
    versionsLink.setAttribute("data-toggle", "tab");
    versionsLink.appendChild(document.createTextNode("Versions"));
    versions.appendChild(versionsLink);
    tabHolder.appendChild(versions);

    var tag = document.createElement("li");
    var tagLink = document.createElement("a");
    tagLink.setAttribute("href", "#tagContent");
    tagLink.setAttribute("data-toggle", "tab");
    tagLink.appendChild(document.createTextNode("Tag"));
    tag.appendChild(tagLink);
    tabHolder.appendChild(tag);

    var metadata = document.createElement("li");
    var metadataLink = document.createElement("a");
    metadataLink.setAttribute("href", "#metadataContent");
    metadataLink.setAttribute("data-toggle", "tab");
    metadataLink.appendChild(document.createTextNode("Metadata"));
    metadata.appendChild(metadataLink);
    tabHolder.appendChild(metadata);

    var viewer = document.createElement("li");
    var viewerLink = document.createElement("a");
    viewerLink.setAttribute("href", "#viewerContent");
    viewerLink.setAttribute("data-toggle", "tab");
    viewerLink.appendChild(document.createTextNode("Viewer"));
    viewer.appendChild(viewerLink);
    tabHolder.appendChild(viewer);

    var provenance = document.createElement("li");
    var provenanceLink = document.createElement("a");
    provenanceLink.setAttribute("href", "#provenanceContent");
    provenanceLink.setAttribute("data-toggle", "tab");
    provenanceLink.appendChild(document.createTextNode("Provenance"));
    provenance.appendChild(provenanceLink);
    tabHolder.appendChild(provenance);

    var project = document.createElement("li");
    var projectLink = document.createElement("a");
    projectLink.setAttribute("href", "#projectContent");
    projectLink.setAttribute("data-toggle", "tab");
    projectLink.appendChild(document.createTextNode("Project Details"));
    project.appendChild(projectLink);
    tabHolder.appendChild(project);
    this.div.appendChild(tabHolder);

    var viewer = this;

    versionsLink.onclick = function(){viewer.displayVersions();};
    tagLink.onclick = function(){viewer.displayTags(); };
    viewerLink.onclick = function(){viewer.displayContents();};
    metadataLink.onclick = function (){viewer.displayMetadata();};
    provenanceLink.onclick = function (){viewer.displayProvenance();};
    projectLink.onclick = function(){viewer.displayProjectDetails();};
};
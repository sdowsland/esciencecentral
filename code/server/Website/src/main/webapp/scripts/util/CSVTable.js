/* 
 * This script creates a table to display some CSV data
 */
function CSVTable(){
    this.div = null;
    this.csvData = "";
    this.url = "";
    this.sizeLimit = 0;
}

/** Display data */
CSVTable.prototype.display = function(div, url){
    var dataLimit;
    if(this.sizeLimit==0){
        dataLimit = 100000;
    } else if(this.sizeLimit==1){
        dataLimit = 500000;
    } else if(this.sizeLimit==2){
        dataLimit = 1000000;
    }
    
    if(div){
        this.div = div;
    }
    
    if(url){
        this.url = url;
    }
    this.csvData = textGet(this.url, dataLimit);
    this.displayAsCSV();
};

/** Display CSV data in a table */
CSVTable.prototype.displayAsCSV = function(){
    if(this.div){
        try {
            var arrays = $.csv.toArrays(this.csvData + "\n");
        } catch (err){
            $.jGrowl("Error parsing CSV data. Displaying as text");
            this.displayAsText();
            return;
        }
        var html;
        if(arrays){
            html = this.createSelector();
            html+='<table class="display" style="width: 100%;" id="' + this.div.id + '_csvtbl">';
            var row;
            var i;
            var j;
            
            if(arrays.length==1){
                // Single line of data
                html+="<tr><tbody>";
                row = arrays[0];
                for(j=0;j<row.length;j++){
                    html+="<td>" + row[j] + "</td>";
                }
                html+="<tr></tbody>";
            } else if(arrays.length>1) {
                // Multiple rows
                
                // Header
                row = arrays[0];
                html+="<thead><tr>";
                for(j=0;j<row.length;j++){
                    html+="<th>(" + j + ")</th>";
                }
                html+="</tr><tr>";
                for(j=0;j<row.length;j++){
                    html+="<th>" + row[j] + "</th>";
                }
                html+="</tr></thead><tbody>";
                
                // Body
                for(i=1;i<arrays.length;i++){
                    row = arrays[i];
                    html+="<tr>";
                    for(j=0;j<row.length;j++){
                        html+="<td>" + row[j] + "</td>";
                    }
                    html+="</tr>";
                }
                html+="</tbody>";
                                
            } else {
                // No data
                html+="<tbody><tr><td>NO DATA</td></tr></tbody>";
            }
            
            html+="</table>";
            this.div.innerHTML = html;
            this.setupSelector(0);


        
        } else {
            html = this.createSelector();
            html+="<h1>Error parsing CSV data</h1>";
            this.div.innerHTML = html;
        }
    }
};

/** Display as plain text if CSV doesn't work or user has selected text from the drop down */
CSVTable.prototype.displayAsText = function(){
    var html = this.createSelector();
    html+="<pre>" + this.csvData + "</pre>";
    this.div.innerHTML = html;
    this.setupSelector(1);
};

CSVTable.prototype.setupSelector = function(selectedIndex){
    var selector = document.getElementById(this.div.id + "_selector");
    var dialog = this;
    if(selector){
        selector.selectedIndex = selectedIndex;
        $("#" + this.div.id + "_selector").selectBox();
        selector.onchange = function(){
            var index = this.selectedIndex;
            if(index==0){
                dialog.displayAsCSV();
            } else {
                dialog.displayAsText();
            }
        }
        
    }    
    
    selector = document.getElementById(this.div.id + "_size_limit");
    if(selector){
        selector.selectedIndex = this.sizeLimit;
        $("#" + this.div.id + "_size_limit").selectBox();
        selector.onchange = function(){
            dialog.sizeLimit = this.selectedIndex;
            dialog.display();
        }
    }
};

CSVTable.prototype.createSelector = function(){
    var html='Display Mode: <select id="' + this.div.id + '_selector">';
    html+='<option id="' + this.div.id + '_select_csv">View as CSV</option>';
    html+='<option id="' + this.div.id + '_select_text">View as Text</option>';
    html+="</select>";
    
    html+='  Size Limit: <select id="' + this.div.id + '_size_limit">';
    html+='<option id="' + this.div.id + '_100">100 KB</option>';
    html+='<option id="' + this.div.id + '_500">500 KB</option>';
    html+='<option id="' + this.div.id + '_1000">1 MB</option>';
    html+="</select>";
    
    html+="<hr>";
    return html;
};
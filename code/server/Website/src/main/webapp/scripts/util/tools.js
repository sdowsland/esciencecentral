
function clearTabs()
{
  //remove all of the classes from the tab_nav elements
  $('#navigation').children(['a']).removeClass();
}

//Function to set the selected tab in the top navigation
//sets all other tabs to be unselected
function setTab(tabname, state)
{
  //remove all of the classes from the tab_nav elements
  $('#navigation').children(['a']).removeClass();

  //if the tab isn't already inactive add the desired state to it
  if (!$('#' + tabname).hasClass(state))
  {
    $('#' + tabname).addClass(state);
  }
}


$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

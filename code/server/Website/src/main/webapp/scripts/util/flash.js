/*
   ALLOWED TYPES: error, success, info, warning
 */
function flashMessage(message, type, target, cssClass) {

    var html;

    if(type == 'error')
    {
        html =  '<div class="alert alert-error ' + cssClass + '">' +
                '<strong>Error!</strong> ' + message +
                '<a class="close" data-dismiss="alert" href="#">&times;</a></div>';
    }
    else if(type == 'success')
    {
        html =  '<div class="alert alert-success ' + cssClass + '">' +
                '<strong>Success!</strong> ' + message +
                '<a class="close" data-dismiss="alert" href="#">&times;</a></div>';
    }
    else if(type == 'warning')
    {
        html =  '<div class="alert alert-info ' + cssClass + '">' +
                '<strong>Info!</strong> ' + message +
                '<a class="close" data-dismiss="alert" href="#">&times;</a></div>';
    }
    else
    {
        html =  '<div class="alert ' + cssClass + '">' +
                '<strong>Warning!</strong> ' + message +
                '<a class="close" data-dismiss="alert" href="#">&times;</a></div>';
    }

    console.log(html);

    if(target == 'undefined')
    {
        $('#header-divider').after(html);
    }
    else
    {
        target.after(html);
    }
}

function flashError(message) {
    if ($("#flash_container").length == 0) {
        var flash_container = $("<div id='flash_container'></div>");
        flash_container.addClass("grid_12");
        $('.content:first').before(flash_container);

        var clearfix = $("<div>&nbsp;</div>");
        clearfix.addClass("grid_12 clearfix");
        $('.content:first').before(clearfix);
    }

    var flash = $("<div></div>");
    flash.addClass("ui-widget flash");

    var ui = $("<div></div>");
    ui.addClass("ui-state-error ui-corner-all");

    var p = $("<p><strong>Alert:</strong> " + message + "</p>");

    var close = $('<div style="float:right; cursor:pointer; color:#393939" onclick="$(this).parent().parent().parent().remove()">&times;</div>');
    var span = $("<span></span>");
    span.addClass("ui-icon ui-icon-alert");

    p.prepend(span);
    p.prepend(close);
    ui.append(p);
    flash.append(ui);

    $("#flash_container").append(flash);
}

function flashInfo(message) {
    if ($("#flash_container").length == 0) {
        var flash_container = $("<div id='flash_container'></div>");
        flash_container.addClass("grid_12");
        $('.content:first').before(flash_container);


        var clearfix = $("<div></div>");
        clearfix.addClass("grid_12 clearfix");
        $('.content:first').before(clearfix);
    }

    var flash = $("<div></div>");
    flash.addClass("ui-widget flash");

    var ui = $("<div></div>");
    ui.addClass("ui-state-highlight ui-corner-all");

    var p = $("<p><strong>Info:</strong> " + message + "</p>");
    var close = $('<div style="float:right; cursor:pointer; color:#393939" onclick="$(this).parent().parent().parent().remove()">&times;</div>');
    var span = $("<span></span>");
    span.addClass("ui-icon ui-icon-info");

    p.prepend(span);
    p.prepend(close);
    ui.append(p);
    flash.append(ui);

    $("#flash_container").append(flash);
}

function flashFrontPageError(message) {
    var flash = $("<div></div>");
    flash.addClass("alert alert-error");

    var span = $("<span style='font-weight: bold'>Error: </span>");

    var span2 = $("<span>" + message + "</span>");
    flash.append(span);
    flash.append(span2);

   $("#flash-wrapper").append(flash);
}

function flashFrontPageInfo(message) {
    var flash = $("<div></div>");
    flash.addClass("alert alert-info");

    var span = $("<span style='font-weight: bold'>Info: </span>");

    var span2 = $("<span>" + message + "</span>");
    flash.append(span);
    flash.append(span2);

   $("#flash-wrapper").append(flash);
}

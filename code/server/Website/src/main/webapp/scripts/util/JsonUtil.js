/* 
 * This script provides utilities for sending JSON messages to the website API
 * servlets.
 */
if(typeof globalAjaxBaseUrl==="undefined"){
    var globalAjaxBaseUrl = "../../";
}

function jsonCall(callData, url, callback, errorCallback){
    var callString = JSON.stringify(callData);
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o);
            }
        } else {
            if(errorCallback){
                errorCallback(o.message, o);
            } else {
                $.jGrowl("Error executing request: " + o.message);
            }
        }
    };

    var errCb = function(jqXHR, textStatus, errorThrown){
        if(errorCallback){
            errorCallback(textStatus);
        }
    };
    
    
    $.ajax({
      type: 'POST',
      url: rewriteAjaxUrl(url),
      data: callString,
      success: cb,
      error: errCb,
      xhrFields: {
          withCredentials: true
      },              
      dataType: "json"
    });
};

function jsonGet(url){
    return $.ajax({
        type: 'GET',
        url: rewriteAjaxUrl(url),
        dataType: 'json',
        /*
        xhrFields: {
            withCredentials: true
        },
        */
        success: function() { },
        data: {},
        async: false
    });    
};

function jsonPost(url, callData){
    var callString = JSON.stringify(callData);
    return $.ajax({
        type: 'POST',
        url: rewriteAjaxUrl(url),
        dataType: "json",
        /*
        xhrFields: {
            withCredentials: true
        },
        */
        contentType: "application/json",
        success: function() { },
        data: callString,
        async: false
    });    
};

function textGet(url, maxSize){
    if(maxSize){
        url+="&limit=" + maxSize;
    }
    
    var result = $.ajax({
        type: 'GET',
        url: rewriteAjaxUrl(url),
        dataType: 'text',
        /*
        xhrFields: {
            withCredentials: true
        },
        */
        success: function() { },
        data: {},
        async: false
    });    
    return result.responseText;
};

function asyncTextGet(url, maxSize){
    if(maxSize){
        url+="&limit=" + maxSize;
    }
    
    var result = $.ajax({
        type: 'GET',
        url: rewriteAjaxUrl(url),
        dataType: 'text',
        xhrFields: {
            withCredentials: true
        },         
        success: function() { },
        data: {},
        async: true
    });    
    return result.responseText;
};

function asyncJsonGet(url, callback){
    var cb = function(o){
        if(!o.error){
            callback(o);
        } else {
            $.jGrowl("Error executing request: " + o.message);
        }
    };
    
    return $.ajax({
        type: 'GET',
        url: rewriteAjaxUrl(url),
        dataType: 'json',
        /*
        xhrFields: {
            withCredentials: true
        },
        */
        success: cb,
        data: {},
        async: false
    });       
};

function jsonLogin(username, password){
    var callData = {
        username: username,
        password: password
    };
    return jsonPost(rewriteAjaxUrl("../../servlets/externallogin?method=loginToSession"), callData);
};

function rewriteAjaxUrl(url){
    var newUrl;
    if(typeof globalAjaxRewrite!="undefined" && globalAjaxRewrite){
        if(globalAjaxBaseUrl===url.substring(0, globalAjaxBaseUrl.length)){
            // Matches the start of the replacement string
            newUrl = globalAjaxURL + url.substring(globalAjaxBaseUrl.length);
        } else {
            // No match, return the original url
            newUrl = url;
        }
    } else {
        newUrl = url;
    }
    
    if(typeof globalAjaxLog!="undefined" && globalAjaxLog){
        console.log(url + " -> " + newUrl);
    }
    
    return newUrl;
};

/* 
 * This scripts initialises the API plugin
 */

function DocumentRecord(){
    this.name = "";
    this.description = "";
    this.id = null;
    this.containerId = null;
    this.content = null;
    this.version = null;
}

DocumentRecord.prototype.parseJson = function(fileJson){
    this.name = fileJson.name;
    this.id = fileJson.id;
    this.description = fileJson.description;
    this.containerId = fileJson.containerId;
    if(fileJson.content){
        this.content = fileJson.content;
    } else {
        this.content = null;
    }
    if(fileJson.version){
        this.version = new DocumentVersion();
        this.version.parseJson(fileJson.version);
    } else {
        this.version = null;
    }
};

DocumentRecord.prototype.toString = function(){
    return this.name;
};

function DocumentVersion(){
    this.id = "";
    this.documentId = "";
    this.timestamp = "";
    this.day = "";
    this.time = "";
    this.comments = "";
    this.number = 0;
    this.size = 0;
};

DocumentVersion.prototype.parseJson = function(versionJson){
    this.id = versionJson.id;
    this.documentId = versionJson.documentId;
    this.size = versionJson.size;
    this.number = versionJson.number;
    this.comments = versionJson.comments;
    this.timestamp = versionJson.timestamp;
    this.day = versionJson.day;
    this.time = versionJson.time;
};

function Folder(){
    this.name = "";
    this.description = "";
    this.id = "";
}

Folder.prototype.parseJson = function(folderJson){
    this.name = folderJson.name;
    this.id = folderJson.id;
    this.description = folderJson.description;
};

Folder.prototype.toJson = function(){
    var json = {
        name: this.name,
        description: this.description,
        id: this.id
    };
    return json;
};

Folder.prototype.toString = function(){
    return this.name;
};

function PropertyItem(json){
    if(json){
        if(json.name){
            this.name = json.name;
        } else {
            this.name = "";
        }
        
        if(json.value){
            this.value = json.value;
        } else {
            this.value = "";
        }
        
        if(json.type){
            this.type = json.type;
        } else {
            this.type = "String";
        }
    } else {
        this.name = "";
        this.value = "";
        this.type = "String";
    }
}

PropertyItem.prototype.toJson = function(){
    var json = {
        name: this.name,
        value: this.value,
        type: this.type
    };
    return json;
};

function PropertyList(){
    this.objectId = "";
    this.groupName = "";
    this.properties = new List();
};

PropertyList.prototype.size = function(){
    return this.properties.size();
};

PropertyList.prototype.get = function(index){
    return this.properties.get(index);
};

PropertyList.prototype.find = function(name){
    for(var i=0;i<this.properties.size();i++){
        if(this.properties.get(i).name==name){
            return this.properties.get(i);
        }
    }
    return null;
};

PropertyList.prototype.add = function(property){
    this.properties.add(property);
};

PropertyList.prototype.set = function(name, value){
    var property = this.findProperty(name);
    if(property){
        property.value = value.toString();
        property.type = "String";
    } else {
        property = new PropertyItem();
        property.name = name;
        property.value = value.toString();
        property.type = "String";
        this.properties.add(property);
    }
};

PropertyList.prototype.findProperty = function(name){
    for(var i=0;i<this.properties.size();i++){
        if(this.properties.get(i).name==name){
            return this.properties.get(i);
        }
    }
    return null;
};

PropertyList.prototype.toJson = function(){
    var propertyArray = new Array(this.properties.size());
    for(var i=0;i<this.properties.size();i++){
        propertyArray[i] = this.properties.get(i).toJson();
    }
    
    var json = {
        objectId: this.objectId,
        groupName: this.groupName,
        properties: propertyArray
    };
    return json;
};

PropertyList.prototype.parseJson = function(json){
    this.properties.clear();
    this.objectId = json.objectId;
    this.groupName = json.groupName;
    for(var i=0;i<json.properties.length;i++){
        this.properties.add(new PropertyItem(json.properties[i]));
    }
};

PropertyList.prototype.showDetails = function(){
    
    var html = "<ul>";
    html+="<li>Property group: " + this.groupName + "<li>";
    for(var i=0;i<this.properties.size();i++){
        html+="<li>" + this.properties.get(i).name + " = " + this.properties.get(i).value + "</li>";
    }
    html+="</ul>";
    return html;
};

PropertyList.prototype.toString = function(){
    return "Properties group: " + this.groupName + " for object: " + this.objectId;
};

function APIClient(){
}

APIClient.prototype.getFolderById = function(id){
    var url = rewriteAjaxUrl("../../servlets/blockbuilder?method=getFolder&id=" + id + "&type=folder");
    /*
    var result = $.ajax({
        type: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        dataType: 'json',
        success: function() { },
        data: {},
        async: false
    });
    */
    var result = jsonGet(url);
    var folder = new Folder();
    var j = JSON.parse(result.responseText);
    if(j.type=="folder"){
        folder.parseJson(j);
        return folder;
    } else if(j.type=="error"){
        throw "Error: " + j.message;
    } else {
        throw "Wrong kind of object returned: " + j.type;
    }
};

APIClient.prototype.getFolder = function(parent, type, name){
    if(parent){
        var url;
        if(name){
            url = rewriteAjaxUrl("../../servlets/blockbuilder?method=getFolder&id=" + parent.id + "&type=" + type + "&folder=" + name);
        } else {
            url = rewriteAjaxUrl("../../servlets/blockbuilder?method=getFolder&id=" + parent.id + "&type=" + type);
        }
        
        /*
        var result = $.ajax({
            type: 'GET',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: 'json',
            success: function() { },
            data: {},
            async: false
        });
        */
        var result = jsonGet(url);
        var folder = new Folder();
        var j = JSON.parse(result.responseText);
        if(j.type=="folder"){
            folder.parseJson(j);
            return folder;
        } else if(j.type=="error"){
            throw "Error: " + j.message;
        } else {
            throw "Wrong kind of object returned: " + j.type;
        }
    } else {
        throw "No working directory";
    }
};

APIClient.prototype.getHomeFolder = function(){
    var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=getFolder&id=home');
    /*
    var result = $.ajax({
        type: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        dataType: 'json',
        success: function() { },
        data: {},
        async: false
    });
    */
    var result = jsonGet(url);
    var folder = new Folder();
    var j = JSON.parse(result.responseText);
    folder.parseJson(j);
    return folder;
};

APIClient.prototype.getFolderContents = function(folder){
    var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=getContents&id=' + folder.id);
    /*
    var result = $.ajax({
        type: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        dataType: 'json',
        success: function() { },
        data: {},
        async: false
    });
    */
    var result = jsonGet(url);
    var j = JSON.parse(result.responseText);
    var contents = new Array();
    var i;
    var obj;
    var f;
    var doc;
    var count = 0;

    for(i=0;i<j.length;i++){
        obj = j[i];
        if(obj.type=="document"){
            doc = new DocumentRecord();
            doc.parseJson(obj);
            contents[count] = doc;
            count++;

        } else if(obj.type=="folder"){
            f = new Folder();
            f.parseJson(obj);
            contents[count] = f;
            count++;
        }
    }
    return contents;
};

APIClient.prototype.copyFile = function(file, targetFolder){
    if(file && targetFolder){
        var fileId;
        if(file instanceof DocumentRecord){
            fileId = file.id;
        } else {
            fileId = file;
        }
        
        var folderId;
        if(targetFolder instanceof Folder){
            folderId = targetFolder.id;
        } else {
            folderId = targetFolder;
        }
        
        var data = {
            fileId: fileId,
            folderId: folderId
        };
        //var callString = JSON.stringify(data);
        var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=copyFile');
        var result = jsonPost(url, data);
        /*
        var result = $.ajax({
            type: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: "json",
            contentType: "application/json",
            success: function() { },
            data: callString,
            async: false
        });
        */

        var j = JSON.parse(result.responseText);
        var doc = new DocumentRecord();
        if(j.type=="error"){
            throw "Error: " + j.message;
        } else {
            doc.parseJson(j);
            return doc;
        }        
    } else {
        return null;
    }
};

APIClient.prototype.getFile = function(folder, name){
    if(folder!=undefined){

        var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=getFile&container=' + folder.id + "&name=" + name + "&getcontents=false");
        /*
        var result = $.ajax({
            type: 'GET',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: 'json',
            success: function() { },
            data: {},
            async: false
        });
        */
        var result = jsonGet(url);
        var j = JSON.parse(result.responseText);
        var doc = new DocumentRecord();
        if(j.type=="error"){
            return null;
        } else {
            doc.parseJson(j);
            return doc;
        }

    } else {
        throw "No working directory available";
    }    
};

APIClient.prototype.deleteFile = function(doc){
    var id;
    if(doc instanceof DocumentRecord){
        id = doc.id;
    } else {
        id = doc;
    }
    if(id){
        var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=removeFile&id=' + id);
        /*
        var result = $.ajax({
            type: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: 'json',
            success: function() { },
            data: {},
            async: false
        });
        */
        var result = jsonPost(url, {});
        var j = JSON.parse(result.responseText);
        var doc = new DocumentRecord();
        if(j.type=="error"){
            throw "Error: " + j.message;
        } else {
            doc.parseJson(j);
            return doc;
        }
    }            
};

/** Get a file and data */
APIClient.prototype.getFileContentsById = function(id, versionId){
    var url;
    if(versionId){
        url = '../../servlets/blockbuilder?method=getFile&id=' + id + "&getcontents=true&versionid=" + versionId;
    } else {
        url = '../../servlets/blockbuilder?method=getFile&id=' + id + "&getcontents=true";
    }
    
    var result = jsonGet(url);

    var j = JSON.parse(result.responseText);
    var doc = new DocumentRecord();
    if(j.type=="error"){
        return j;
    } else {
        doc.parseJson(j);
        return doc;
    }
};

/** Get a file and data */
APIClient.prototype.getFileById = function(id){
    var url = '../../servlets/blockbuilder?method=getFile&id=' + id + "&getcontents=false";
    var result = jsonGet(url);

    var j = JSON.parse(result.responseText);
    var doc = new DocumentRecord();
    if(j.type=="error"){
        return j;
    } else {
        doc.parseJson(j);
        return doc;
    }
};

/** Get the versions of a file */
APIClient.prototype.getFileVersions = function(id){
    var url = '../../servlets/blockbuilder?method=getVersions&id=' + id;
    var result = jsonGet(url);

    var j = JSON.parse(result.responseText);
    
    if(j.type=="error"){
        return j;
    } else {
        var versions = new Array();
        var v;
        for(var i=0;i<j.length;i++){
            v = new DocumentVersion();
            v.parseJson(j[i]);
            versions[i] = v;
        }
        return versions;
    }
};

/** Get a file and data */
APIClient.prototype.getFileContents = function(folder, name){
    if(folder!=undefined){

        var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=getFile&container=' + folder.id + "&name=" + name);
        /*
        var result = $.ajax({
            type: 'GET',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: 'json',
            success: function() { },
            data: {},
            async: false
        });
        */
        var result = jsonGet(url);
        var j = JSON.parse(result.responseText);
        var doc = new DocumentRecord();
        if(j.type=="error"){
            return j;
        } else {
            doc.parseJson(j);
            return doc;
        }

    } else {
        throw "No working directory available";
    }
};

/** Set the contents of a file. This will update a file or create a new record */
APIClient.prototype.setFileContents = function(folder, saveDoc){
    var url;
    if(folder!=null && folder!=undefined){
        url = '../../servlets/blockbuilder?method=putFile&container=' + folder.id;
    } else {
        url = '../../servlets/blockbuilder?method=putFile';
    }

    var result = jsonPost(url, saveDoc);
    var j = JSON.parse(result.responseText);
    var doc = new DocumentRecord();
    if(j.type=="error"){
        return j;
    } else {
        doc.parseJson(j);
        return doc;
    }
};

/** Copy the icon file from the resources directory into the block src file */
APIClient.prototype.copyBlockIconFile = function(targetFolder, fileName){
    var callData = {
        targetFolderId: targetFolder.id,
        fileName: fileName
    };
    //var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/blockbuilder?method=copyBlockIcon");
    jsonPost(url, callData);
    
    /*
    $.ajax({
        type: 'POST',
        url: rewriteAjaxUrl("../../servlets/blockbuilder?method=copyBlockIcon"),
        dataType: "json",
        xhrFields: {
            withCredentials: true
        },         
        contentType: "application/json",
        success: function() { },
        data: callString,
        async: false
    });    
    */

};

APIClient.prototype.createFile = function(parent, fileName){
    if(parent){
        var f = new DocumentRecord();
        f.name = fileName;
        f.content = "";
        f.containerId = parent.id;
        f.id = "";
        f.description = "File uploaded from API";
        return APIClient.prototype.setFileContents(parent, f);
    } else {
        throw "No parent folder specified";
    }
};

APIClient.prototype.saveFolder = function(parent, folder){
    if(parent!=undefined && folder!=undefined){
       var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=putFolder&container=' + parent.id);
       //var callString = JSON.stringify(folder);
       var result = jsonPost(url, folder);
       /*
        var result = $.ajax({
            type: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: "json",
            contentType: "application/json",
            success: function() { },
            data: callString,
            async: false
        });
        */
        var j = JSON.parse(result.responseText);
        var f = new Folder();
        if(j.type=="error"){
            return j;
        } else {
            f.parseJson(j);
            return f;
        }
        
    }
};

APIClient.prototype.propagateFolderPermissions = function(folder){
    if(folder!=undefined){
       var url;
       var callData;
       if(folder instanceof Folder){
           url = rewriteAjaxUrl('../../servlets/blockbuilder?method=propagatePermissions&id=' + folder.id);
           callData = {id: folder.id};
       } else {
           url = rewriteAjaxUrl('../../servlets/blockbuilder?method=propagatePermissions&id=' + folder);
           callData = {id: folder};
       }
           
       //var callString = JSON.stringify(callData);
       jsonPost(url, callData);
       /*
        var result = $.ajax({
            type: 'POST',
            url: url,
            xhrFields: {
                withCredentials: true
            },             
            dataType: "json",
            contentType: "application/json",
            success: function() { },
            data: callString,
            async: false
        });
        */
        var j = JSON.parse(result.responseText);
        var f = new Folder();
        if(j.type=="error"){
            return j;
        } else {
            f.parseJson(j);
            return f;
        }
        
    }
};

APIClient.prototype.getUrlContentAsString = function(url){
    var strReturn = "";
    $.ajax({
        url:url,        
        success:function(html){
            strReturn = html;
        }, 
        async:false,
        dataType: "text"
    });
    return strReturn;
};

APIClient.prototype.endsWith = function (str, suffix) {
    return str.match(suffix+"$")==suffix;
};

APIClient.prototype.getObjectPropertyGroup = function(objectId, groupName){
    var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=getProperties&id=' + objectId + "&group=" + groupName);
    /*
    var result = $.ajax({
        type: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        dataType: 'json',
        success: function() { },
        data: {},
        async: false
    });
    */
    var result = jsonGet(url);
    var j = JSON.parse(result.responseText);
    var properties = new PropertyList();
    if(j.type=="error"){
        return j;
    } else {
        properties.parseJson(j);
        return properties;
    }
    
};

APIClient.prototype.getObjectProperty = function(objectId, groupName, propertyName){
    var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=getProperty&id=' + objectId + "&group=" + groupName + "&property=" + propertyName);
    /*
    var result = $.ajax({
        type: 'GET',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        dataType: 'json',
        success: function() { },
        data: {},
        async: false
    });
    */
    var result = jsonGet(url);
    var j = JSON.parse(result.responseText);
    
    if(j.type=="error"){
        return j;
    } else if(j.type=="notpresent"){
        return null;
    } else {
        return JSON.parse(j.value);
    }    
};

APIClient.prototype.setObjectProperty = function(objectId, groupName, propertyName, propertyValue){
    var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=setProperty');
    var callData = {
        objectId: objectId,
        groupName: groupName, 
        propertyName: propertyName,
        propertyValue: propertyValue
    };
    
    //var callString = JSON.stringify(callData);
    var result = jsonPost(url, callData);
    /*
    var result = $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        dataType: "json",
        contentType: "application/json",
        success: function() {
        },
        data: callString,
        async: false
    });
    */
    var j = JSON.parse(result.responseText);
    var properties = new PropertyList();
    if (j.type == "error") {
        return j;
    } else {
        properties.parseJson(j);
        return properties;
    }
};
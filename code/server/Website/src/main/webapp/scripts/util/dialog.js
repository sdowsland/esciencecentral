/** 
 * This script provides a basic message dialog 
 */
if(typeof _globalDialogHeight==="undefined"){
    var _globalDialogHeight = 200;
}

function MessageDialog(){
    this.divName = "";
    this.textDiv = null;
}

MessageDialog.prototype.cleanUp = function(){
$("#" + this.divName).parents(".ui-dialog").remove();  
};

/** Initialise the confirm dialog on a div */
MessageDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    this.textDiv = document.createElement("div");
    div.appendChild(this.textDiv);
    
    var d = this;
    $("#" + this.divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: _globalDialogHeight,
        width: 350,
        dialogClass: "alert",
        modal: true,
        title: "Message",
        buttons: {
                'Ok': function() {
                    $(this).dialog('close');
                }
        },
        close: function() {

        }
    });    
    
};

/** Show the confirm dialog with a message */
MessageDialog.prototype.show = function(message){
    this.textDiv.innerHTML = message;
    $("#" + this.divName).dialog("open");
};

/* 
 * This script provides a basic Yes/No confirm dialog
 */
function ConfirmDialog(){
    this.divName = "";
    this.yesCallback = null;
    this.noCallback = null;
    this.textDiv = null;
}

ConfirmDialog.prototype.cleanUp = function(){
$("#" + this.divName).parents(".ui-dialog").remove();  
};

/** Initialise the confirm dialog on a div */
ConfirmDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    this.textDiv = document.createElement("div");
    div.appendChild(this.textDiv);
    
    var d = this;
    $("#" + this.divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: _globalDialogHeight,
        width: 350,
        dialogClass: "alert",
        modal: true,
        title: "Question",
        buttons: {
                'Yes': function() {
                    if(d.yesCallback){
                        d.yesCallback();
                    }
                    $(this).dialog('close');
                },

                'No': function() {
                    if(d.noCallback){
                        d.noCallback();
                    }
                    $(this).dialog('close');
                }

        },
        close: function() {

        }
    });    
    
};

/** Show the confirm dialog with a message */
ConfirmDialog.prototype.show = function(message){
    this.textDiv.innerHTML = message;
    $("#" + this.divName).dialog("open");
};

/* 
 * This script provides a model busy dialog that is shown and then performs
 * its "execute" function. When this finishes, the dialog is closed
 */
function BusyDialog(){
    this.divName = "";
    this.captionText = null;
}

BusyDialog.prototype.cleanUp = function(){
$("#" + this.divName).parents(".ui-dialog").remove();  
};

BusyDialog.prototype.init = function(divName){
    this.divName = divName;
    
    var img = document.createElement("img");
    img.setAttribute("src", rewriteAjaxUrl("../../scripts/util/system-run-3.png"));
    img.setAttribute("style", "float: left");
    
    this.captionText = document.createElement("p");
    this.captionText.setAttribute("style", "font-size: 14pt; margin-left: 80px;margin-top:20px;")
    this.captionText.appendChild(document.createTextNode("Doing something"));
    document.getElementById(this.divName).setAttribute("style", "overflow: hidden;");
    document.getElementById(this.divName).appendChild(img);
    document.getElementById(this.divName).appendChild(this.captionText);
    $('#' + this.divName).dialog({
        height: 75,
        width: 350,
        dialogClass: "alert",
        autoOpen: false,
        resizable: false,
        closeOnEscape: false,
        position: "center",
        modal: true        
    });
    $("#" + this.divName).prev('.ui-dialog-titlebar').hide();
};

BusyDialog.prototype.show = function(executeFunction){
    if(!executeFunction){
        executeFunction = {
            caption: "Working",
            func: null
        };
    }
    var dn = this.divName;
    if(executeFunction.caption){
        this.captionText.innerHTML = "";
        this.captionText.appendChild(document.createTextNode(executeFunction.caption));
    } else {
        this.captionText.innerHTML = "";
        this.captionText.appendChild(document.createTextNode("Busy..."));
    }
    
    $('#' + this.divName).dialog('open');
    if(executeFunction.func){
        var scopeFunction = executeFunction;

        var f = function(){
            scopeFunction.func();
            $('#' + dn).dialog('close');
        };
        setTimeout(f, 10);
    }
};

BusyDialog.prototype.close = function(){
    $('#' + this.divName).dialog('close');
};

/* 
 * This dialog provides a dialog with OK and cancel buttons with an input text
 * field
 */
function InputDialog(){
    this.divName = "";
    this.okCallback = null;
    this.cancelCallback = null;
    this.title = "Please enter a value";
}

/** Clean up this dialog */
InputDialog.prototype.cleanUp = function(){
    $("#" + this.divName).parents(".ui-dialog").remove();    
};

/** Initialise the confirm dialog on a div */
InputDialog.prototype.init = function(divName){
    this.divName = divName;
    
    var d = this;
    $("#" + this.divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: _globalDialogHeight,
        width: 350,
        dialogClass: "alert",
        modal: true,
        title: this.title,
        buttons: {
                'Ok': function() {
                    if(d.okCallback){
                        var input = document.getElementById(d.divName + "_input");
                        d.okCallback(input.value);
                    }
                    $(this).dialog('close');
                },

                'Cancel': function() {
                    if(d.cancelCallback){
                        d.cancelCallback();
                    }
                    $(this).dialog('close');
                }

        },
        close: function() {

        }
    });    
    
};

/** Show the confirm dialog with a message */
InputDialog.prototype.show = function(initialValue){
    var div = document.getElementById(this.divName);
    div.innerHTML = "";
    var input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("style", "width: 95%; margin-top: 10px;");   
    input.setAttribute("id", this.divName + "_input");
    div.appendChild(input);        
    
    if(initialValue!=undefined){
        input.setAttribute("value", initialValue);
    } else {
        input.setAttribute("value", "");
    }
    $("#" + this.divName).dialog("open");
    input.select();
};

/** Date-time picker dialog */
function DateTimeDialog(){
    this.divName = "";
    this.okCallback = null;
    this.cancelCallback = null;
    this.title = "Please select a date and time";
    this.selectHrs = null;
    this.selectMins = null;
    this.selectSeconds = null;
}

DateTimeDialog.prototype.cleanUp = function(){
$("#" + this.divName).parents(".ui-dialog").remove();  
};

DateTimeDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
       
        var d = this;
        $("#" + this.divName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 450,
            width: 300,
            dialogClass: "alert",
            modal: true,
            title: this.title,
            buttons: {
                    'Ok': function() {
                        var date = $("#" + d.divName + "_picker").datepicker('getDate');
                        if(date!=null){
                            var hrs = d.selectHrs.selectedIndex;
                            var mins = d.selectMins.selectedIndex;
                            var secs = d.selectSeconds.selectedIndex;
                            date.setHours(hrs, mins, secs);
                            if(d.okCallback){
                                d.okCallback(date);
                            }
                        } else {
                            if(d.cancelCallback){
                                d.cancelCallback();
                            }                            
                        }
                        $(this).dialog('close');
                    },

                    'Cancel': function() {
                        if(d.cancelCallback){
                            d.cancelCallback();
                        }
                        $(this).dialog('close');
                    }

            },
            close: function() {

            }
        });        
        
        var labelDiv1 = document.createElement("div");
        labelDiv1.appendChild(document.createTextNode("Date:"));
        div.appendChild(labelDiv1);
        
        var pickerDiv = document.createElement("div");
        pickerDiv.setAttribute("id", this.divName + "_picker");
        div.appendChild(pickerDiv);
        $(pickerDiv).datepicker();
        
        // Fields for time
        var labelDiv2 = document.createElement("div");
        labelDiv2.appendChild(document.createTextNode("Time:"));
        div.appendChild(labelDiv2);
        
        var timeDiv = document.createElement("div");
        var i;
        this.selectHrs = document.createElement("select");
        this.selectHrs.setAttribute("style", "width: 60px;");
        var opt;
        
        for(i=0;i<24;i++){
            opt = document.createElement("option");
            opt.appendChild(document.createTextNode(i));
            this.selectHrs.appendChild(opt);
        }
        
        this.selectMins = document.createElement("select");
        this.selectMins.setAttribute("style", "width: 60px;");
        this.selectSeconds = document.createElement("select");
        this.selectSeconds.setAttribute("style", "width: 60px;");
        for(i=0;i<60;i++){
            opt = document.createElement("option");
            opt.appendChild(document.createTextNode(i));
            this.selectMins.appendChild(opt);
            
            opt = document.createElement("option");
            opt.appendChild(document.createTextNode(i));
            this.selectSeconds.appendChild(opt);
        }
        
        timeDiv.appendChild(this.selectHrs);
        timeDiv.appendChild(document.createTextNode(":"));        
        timeDiv.appendChild(this.selectMins);
        timeDiv.appendChild(document.createTextNode(":"));
        timeDiv.appendChild(this.selectSeconds);
       
        div.appendChild(timeDiv);
    }
    $("#ui-datepicker-div").remove();
   
};
    
/** Show the dialog */
DateTimeDialog.prototype.show = function(dateValue){
    if(dateValue){

        $("#" + this.divName + "_picker").datepicker('setDate', new Date(dateValue));
        this.selectHrs.selectedIndex = dateValue.getHours();
        this.selectMins.selectedIndex = dateValue.getMinutes();
        this.selectSeconds.selectedIndex = dateValue.getSeconds();

    }
    $('#' + this.divName).dialog('open');
};

/** Dialog to pick a true / false value */
function TrueFalseDialog(){
    this.divName = "";
    this.okCallback = null;
    this.cancelCallback = null;
    this.title = "Please select a value";    
}

TrueFalseDialog.prototype.cleanUp = function(){
$("#" + this.divName).parents(".ui-dialog").remove();  
};

TrueFalseDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var d = this;
        $("#" + this.divName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: _globalDialogHeight + 20,
            width: 300,
            modal: true,
            title: "Select True/False value",
            buttons: {
                'Ok': function() {
                    if(d.okCallback){
                        d.okCallback(d.getValue());
                    }
                    $(this).dialog('close');
                },

                'Cancel': function() {
                    if(d.cancelCallback){
                        d.cancelCallback();
                    }
                    $(this).dialog('close');
                }

            },
            close: function() {

            }
        });           
        var form = document.createElement("form");
        form.setAttribute("action", "");
        var trueOption = document.createElement("input");
        trueOption.setAttribute("type", "radio");
        trueOption.setAttribute("name", this.divName + "_radio");
        trueOption.setAttribute("id", this.divName + "_true");
        trueOption.setAttribute("value", "True");
        
        var trueLabel = document.createElement("label");
        trueLabel.setAttribute("style", "position: relative; top: 4px; display: inline; line-height: 20px; margin-left: 8px;");
        trueLabel.appendChild(document.createTextNode("True"));
        
        var falseOption = document.createElement("input");
        falseOption.setAttribute("type", "radio");
        falseOption.setAttribute("name", this.divName + "_radio");
        falseOption.setAttribute("id", this.divName + "_false");
        falseOption.setAttribute("value", "False");
        
        var falseLabel = document.createElement("label");
        falseLabel.setAttribute("style", "position: relative; top: 4px; display: inline; line-height: 20px; margin-left: 8px;");
        falseLabel.appendChild(document.createTextNode("False"));
        
        var d1 = document.createElement("div");
        d1.setAttribute("class", "formInput");
        d1.setAttribute("style", "line-height:30px;");
        d1.appendChild(trueOption);
        d1.appendChild(trueLabel);
        
        var d2 = document.createElement("div");
        d2.setAttribute("class", "formInput");
        d2.setAttribute("style", "line-height:30px;");
        d2.appendChild(falseOption);
        d2.appendChild(falseLabel);

        form.appendChild(d1);
        form.appendChild(d2);
        div.appendChild(form);
    }
};

TrueFalseDialog.prototype.show = function(value){
    $('#' + this.divName).dialog('open');
    if(value==true){
        var trueOption = document.getElementById(this.divName + "_true");
        trueOption.checked = true;
    } else {
        var falseOption = document.getElementById(this.divName + "_false");
        falseOption.checked = true;        
    }
};

TrueFalseDialog.prototype.getValue = function(){
    var trueOption = document.getElementById(this.divName + "_true");
    if(trueOption.checked){
        return true;
    } else {
        return false;
    }
};

/** This dialog picks a value from a predifined list */
function MultiValueSelectDialog(){
    this.okCallback = null;
    this.cancelCallback = null;
    this.divName = "";
    this.title = "Select a value";
}

MultiValueSelectDialog.prototype.cleanUp = function(){
    $("#" + this.divName).parents(".ui-dialog").remove();  
};

MultiValueSelectDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var d = this;
        $("#" + this.divName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: _globalDialogHeight,
            width: 350,
            dialogClass: "alert",
            modal: true,
            title: this.title,
            buttons: {
                'Ok': function() {
                    var selector = document.getElementById(d.divName + "_selector");
                    var v = selector.options[selector.selectedIndex].value;
                    if(d.okCallback){
                        d.okCallback(v, selector.selectedIndex);
                    }
                    $(this).dialog('close');
                },

                'Cancel': function() {
                    if(d.cancelCallback){
                        d.cancelCallback();
                    }
                    $(this).dialog('close');
                }

            },
            close: function() {

            }
        });        
    }
};

MultiValueSelectDialog.prototype.show = function(valueList, value){
    var div = document.getElementById(this.divName);
    var html='<select style="width: 100%; margin-top: 10px; name="options" id="' + this.divName + '_selector">';
    for(var i=0;i<valueList.length;i++){
        html+='<option id="' + this.divName + '_' + i + '">' + valueList[i] + '</option>';
    }
    html+='</select>';
    div.innerHTML = html;
    
    var index = valueList.indexOf(value, 0);
    if(index!=-1){
        var selector = document.getElementById(this.divName + "_selector");
        if(selector){
            selector.selectedIndex = index;
        }
    }
    $('#' + this.divName).dialog('open');
};

/** This dialog allows an object name / description to be edited */
function NameDescriptionDialog(){
    this.okCallback = null;
    this.cancelCallback = null;
    this.divName = "";
    this.title = "Edit object details";    
}

NameDescriptionDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var d = this;
        $("#" + this.divName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: _globalDialogHeight + 100,
            width: 420,
            modal: true,
            title: this.title,
            buttons: {
                'Ok': function() {
                    if(d.okCallback){
                        d.okCallback($("#" + d.divName + "_name").val(), $("#" + d.divName + "_description").val());
                    }
                    $(this).dialog('close');
                },

                'Cancel': function() {
                    if(d.cancelCallback){
                        
                    }
                    $(this).dialog('close');
                }

            },
            close: function() {

            }
        });                 
        
        html='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="objectname">Object name:</label>';
        html+='<input type="text" id="' + this.divName + '_name"></input>';
        html+='</div>';
        
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="objectdescription">Object description:</label>';
        html+='<textarea id="' + this.divName + '_description"></textarea>';
        html+='</div>';
        html+='</form>';
        div.innerHTML=html;
    }
};

NameDescriptionDialog.prototype.show = function(name, description){    
    $("#" + this.divName + "_name").val(name);
    $("#" + this.divName + "_description").val(description);
    $("#" + this.divName).dialog('open');
};
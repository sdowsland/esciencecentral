elFinder.prototype._options.commands.push('escarchive');
elFinder.prototype._options.contextmenu.files.push('escarchive');
elFinder.prototype.i18.en.messages['cmdescarchive'] = 'Archive';
	
elFinder.prototype._options.commands.push('escrestore');
elFinder.prototype._options.contextmenu.files.push('escrestore');
elFinder.prototype.i18.en.messages['cmdescrestore'] = 'Restore';

elFinder.prototype._options.commands.push('escinfo');
elFinder.prototype._options.contextmenu.files.push('escinfo');
elFinder.prototype.i18.en.messages['cmdescinfo'] = 'Info';

elFinder.prototype._options.commands.push('acl');
elFinder.prototype._options.contextmenu.files.push('acl');
elFinder.prototype.i18.en.messages['cmdacl'] = 'Access Control';

elFinder.prototype._options.commands.push('escprovenance');
elFinder.prototype._options.contextmenu.files.push('escprovenance');
elFinder.prototype.i18.en.messages['cmdescprovenance'] = 'Provenance';

elFinder.prototype._options.commands.push('escquickview');
elFinder.prototype._options.contextmenu.files.push('escquickview');
elFinder.prototype.i18.en.messages['cmdescquickview'] = 'Quick View';

elFinder.prototype._options.commands.push('escupload');
elFinder.prototype._options.contextmenu.files.push('escupload');
elFinder.prototype.i18.en.messages['cmdescupload'] = 'Upload';

elFinder.prototype.resources.tpl.arched = '<span class="elfinder-escarched"/>';
elFinder.prototype.resources.tpl.unarching = '<span class="elfinder-escunarching"/>';
elFinder.prototype.resources.tpl.arching = '<span class="elfinder-escarching"/>';
elFinder.prototype.resources.tpl.archederr = '<span class="elfinder-escarchederr"/>';
elFinder.prototype.resources.tpl.unarchingerr = '<span class="elfinder-escunarchingerr"/>';
elFinder.prototype.resources.tpl.archingerr = '<span class="elfinder-escarchingerr"/>';

elFinder.prototype.i18.en.messages['cmdescarchive'] = 'Archive to long term storage';
elFinder.prototype.i18.en.messages['cmdescrestore'] = 'Restore from long term storage';
elFinder.prototype.i18.en.messages['cmdescprovenance'] = 'Provenance Report';

elFinder.prototype._options.commands.push('escmkdataset');
elFinder.prototype._options.contextmenu.cwd.push('escmkdataset');
elFinder.prototype.i18.en.messages['cmdescmkdataset'] = 'Create a new Dataset';

elFinder.prototype._options.commands.push('escextractdataset');
elFinder.prototype._options.contextmenu.files.push('escextractdataset');
elFinder.prototype.i18.en.messages['cmdescextractdataset'] = 'Extract Dataset data';

elFinder.prototype.uniqueName = function(prefix, phash) {
        var i = 0, ext = '', p, name;

        prefix = this.i18n(prefix); 
        phash = phash || this.cwd().hash;

        if ((p = prefix.indexOf('.txt')) !== -1) {
                ext    = '.txt';
                prefix = prefix.substr(0, p);
        } else if ((p = prefix.indexOf('.csv')) !== -1) {
                ext    = '.csv';
                prefix = prefix.substr(0, p);
        }

        name   = prefix+ext;

        if (!this.fileByName(name, phash)) {
                return name;
        }
        while (i < 10000) {
                name = prefix + ' ' + (++i) + ext;
                if (!this.fileByName(name, phash)) {
                        return name;
                }
        }
        return prefix + Math.random() + ext;
};
elFinder.prototype.commands.escupload = function() {
  	var hover = this.fm.res('class', 'hover');

  	this.disableOnSearch = true;
  	this.updateOnSelect  = false;

  	// Shortcut opens dialog
  	this.shortcuts = [{
  		pattern     : 'ctrl+u'
  	}];

  	/**
  	 * Return command state
  	 *
  	 * @return Number
  	 **/
  	this.getstate = function() {
  		return !this._disabled && this.fm.cwd().write ? 0 : -1;
  	}


  	this.exec = function(data) {
  		var fm = this.fm,
  			upload = function(data) {
  				dialog.elfinderdialog('close');
  				fm.upload(data)
  					.fail(function(error) {
  						dfrd.reject(error);
  					})
  					.done(function(data) {
  						dfrd.resolve(data);
  					});
  			},
  			dfrd, dialog, input, button, dropbox;

  		if (this.disabled()) {
  			return $.Deferred().reject();
  		}

  		if (data && (data.input || data.files)) {
  			return fm.upload(data);
  		}

  		dfrd = $.Deferred();


  		input = $('<input type="file" multiple="true"/>')
  			.change(function() {
  				upload({input : input[0]});
  			});

  		button = $('<div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text">'+fm.i18n('selectForUpload')+'</span></div>')
  			.append($('<form/>').append(input))
  			.hover(function() {
  				button.toggleClass(hover)
  			})

  		dialog = $('<div class="elfinder-upload-dialog-wrapper"/>')
  			.append(button);

  		if (fm.dragUpload) {
  			dropbox = $('<div class="ui-corner-all elfinder-upload-dropbox">'+fm.i18n('dropFiles')+'</div>')
  				.prependTo(dialog)
  				.after('<div class="elfinder-upload-dialog-or">'+fm.i18n('or')+'</div>')[0];

  			dropbox.addEventListener('dragenter', function(e) {
  				e.stopPropagation();
  			  	e.preventDefault();
  				$(dropbox).addClass(hover);
  			}, false);

  			dropbox.addEventListener('dragleave', function(e) {
  				e.stopPropagation();
  			  	e.preventDefault();
  				$(dropbox).removeClass(hover);
  			}, false);

  			dropbox.addEventListener('dragover', function(e) {
  				e.stopPropagation();
  			  	e.preventDefault();
  			}, false);

  			dropbox.addEventListener('drop', function(e) {
  				e.stopPropagation();
  			  	e.preventDefault();

  				upload({files : e.dataTransfer.files});
  			}, false);

  		}

  		fm.dialog(dialog, {
  			title          : this.title,
  			modal          : true,
  			resizable      : false,
  			destroyOnClose : true
  		});

  		return dfrd;
  	}

  }

elFinder.prototype.commands.escmkdataset = function(){
	this.disableOnSearch = true;
	this.updateOnSelect  = false;
	this.mime            = 'esc/dataset';
	this.prefix          = 'Untitled Dataset';
	this.exec            = $.proxy(this.fm.res('mixin', 'make'), this);
	
	this.getstate = function() {
            return !this._disabled && this.fm.cwd().write ? 0 : -1;
	};
};

elFinder.prototype.commands.escextractdataset = function(){
    var self = this;
    this.getstate = function(){
        var sel = this.files(sel);
        var mimeOk = false;
        if(sel.length===1){
            if(sel[0].mime==="esc/dataset"){
                mimeOk = true;
            }
        }        
        return !this._disabled && mimeOk && this.fm.cwd().write ? 0 : -1;
    };
    
    this.exec = function(hashes){        
        var files = this.files(hashes);
        if(files.length===1){
            if(files[0].mime==="esc/dataset"){
                var manager = new DatasetManager();
                var dialog;
                if(!self.fm.pickItemDialog){
                    self.fm.pickItemDialog = new MultiValueSelectDialog();
                    var divName = self.fm.id + "-datasetItemListDiv";
                    var div = document.createElement("div");
                    div.setAttribute("id", divName);
                    div.setAttribute("class", "dialog");
                    var containerDiv = document.getElementById(self.fm.id);
                    containerDiv.appendChild(div); 
                    self.fm.pickItemDialog.init(divName);
                    dialog = self.fm.pickItemDialog;
                } else {
                    dialog = self.fm.pickItemDialog;
                }
                
                if(manager){
                    var targetHash = files[0].hash;
                    manager.fetchDatasetDefinition(files[0].escid, function(dataset){
                        if(dataset.items.length>0){
                            var namesArray = new Array();
                            for(var i=0;i<dataset.items.length;i++){
                                namesArray.push(dataset.items[i].name);
                            }
                            
                            self.fm.pickItemDialog.okCallback = function(selectedName){
                                var uniqueName = self.fm.uniqueName(dataset.name + "_" + selectedName + ".csv",files[0].phash);
                                return self.fm.request({
                                        data       : {cmd : 'escextractdataset', itemName: selectedName, target : targetHash, fileName: uniqueName},
                                        notify     : {type : 'upload', cnt : 1, msg: "Data extracted"},
                                        syncOnFail : true
                                });        
                            };
                            self.fm.pickItemDialog.title = "Select item from: " + dataset.name;
                            self.fm.pickItemDialog.show(namesArray, dataset.items[0].name);

                        } else {
                            self.fm.confirm({
                                title: "Error", 
                                text: "Selected Dataset has no items in it",
                                accept : {
                                    label: "Ok",
                                    callback: function(){}
                                },
                                cancel : { 
                                    label: "Cancel",
                                    callback: function(){}
                                }
                            });
                        }
                    });
                }                
            }
        }
    };
};
/**
 * SJW + SMW Additions - start archive
 */

elFinder.prototype.commands.escarchive = function() {

  //Used to determine if the command is allowed - this code probably checks that there is only one object selected, needs changing
  	this.getstate = function(sel) {
            var sel = this.files(sel);
            var mimeOk = true;
            if(sel.length===1){
                if(sel[0].mime==="esc/dataset"){
                    mimeOk = false;
                }
            }
            cnt = sel.length;
            return !this._disabled && mimeOk && sel.length == 1 && sel[0].phash && !sel[0].locked  ? 0 : -1;
  	}

  	this.exec = function(hashes) {

      var fm     = this.fm,
  			files  = this.files(hashes),
  			cnt    = files.length,
  			dfrd   = $.Deferred()
  				.fail(function(error) {
  					error && fm.error(error);
  				}),
  			args = [];

  		if (!cnt || this._disabled) {
  			return dfrd.reject();
  		}

      //get the docId from the document
      var docId = files[0].escid;

      var cb = function(o)
          {
              if (!o.error)
              {
                  alert('Archive succeedfully started');
              }
              else
              {
                  alert("Problem starting archiving data")
              }
          }

      var callData = {id: docId};
      var url = "../../servlets/archive?method=startarchive";
      jsonCall(callData, url, cb);
  	}
  };

/**
 * SJW + SMW Additions - start unarchive
 */

elFinder.prototype.commands.escrestore = function() {

    //Used to determine if the command is allowed - this code probably checks that there is only one object selected, needs changing
    	this.getstate = function(sel) {
            var sel = this.files(sel),cnt = sel.length;
            var mimeOk = true;
            if(sel.length===1){
                if(sel[0].mime==="esc/dataset"){
                    mimeOk = false;
                }
            }                
            return !this._disabled && mimeOk && sel.length == 1 && sel[0].phash && !sel[0].locked  ? 0 : -1;
    	}

    	this.exec = function(hashes) {

        var fm     = this.fm,
    			files  = this.files(hashes),
    			cnt    = files.length,
    			dfrd   = $.Deferred()
    				.fail(function(error) {
    					error && fm.error(error);
    				}),
    			args = [];

    		if (!cnt || this._disabled) {
    			return dfrd.reject();
    		}

        //get the docId from the document
        var docId = files[0].escid;

      var cb = function(o)
          {
              if (!o.error)
              {
                  alert('Unarchive succeedfully started');
              }
              else
              {
                  alert("Problem starting unarchiving data")
              }
          }

      var callData = {id: docId};
      var url = "../../servlets/archive?method=startunarchive";
      jsonCall(callData, url, cb);
    	}
  };

/**
 * SMW Additions - download provenance report
 */

elFinder.prototype.commands.escprovenance = function() {

    //Used to determine if the command is allowed - this code probably checks that there is only one object selected, needs changing
    	this.getstate = function(sel) {
    		var sel = this.files(sel),
    			cnt = sel.length;
          return !this._disabled && sel.length == 1 && sel[0].phash && !sel[0].locked  ? 0 : -1;
    	}

    	this.exec = function(hashes) {

        var fm     = this.fm,
    			files  = this.files(hashes),
    			cnt    = files.length,
    			dfrd   = $.Deferred()
    				.fail(function(error) {
    					error && fm.error(error);
    				}),
    			args = [];

    		if (!cnt || this._disabled) {
    			return dfrd.reject();
    		}

        //get the userid and public userid from the document
        var docId = files[0].escid;
        var filename = files[0].name;
        window.location = "../../servlets/provenance/report/Report_" + filename + ".pdf?escId=" + docId;

//         window.location = "../../servlets/provenance/report?escId=" + docId;
    	}
  };

elFinder.prototype.commands.acl = function() {
	var fm = this.fm;

	this.getstate = function(sel) {
		var sel = this.files(sel),
			cnt = sel.length;
      return !this._disabled && sel.length == 1 && sel[0].phash && !sel[0].locked  ? 0 : -1;
	}

	this.exec = function(hashes) {

    var fm     = this.fm,
			files  = this.files(hashes),
			cnt    = files.length,
			dfrd   = $.Deferred()
				.fail(function(error) {
					error && fm.error(error);
				}),
			args = [];

		if (!cnt || this._disabled) {
			return dfrd.reject();
		}

    //get the userid and public userid from the document
    var docId = files[0].escid;
    var publicUserId = $('#publicUserId').val();
    var currentUserId = $('#currentUserId').val();
    var enablePublic= $('#enablePublic').val();
    var usersGroupId = $('#usersGroupId').val();

        //remove any current ACL
    $('#elfinderAcl').remove();

    //add a new ACL
    $('body').append('<div id="elfinderAcl" class="acl"></div>');
    var acl = new AccessControl();
    acl.init("elfinderAcl", currentUserId, docId, publicUserId, enablePublic, usersGroupId);
    acl.open();

	}
};


elFinder.prototype.commands.escquickview = function ()
{

  var self = this;
  this.name = 'Preview with Quick Look';


    this.exec = function (hashes)
    {
        var files = this.files(hashes);
        if(files.length===1){
            if(files[0].mime==="esc/dataset"){
                if(window.DatasetManager){
                    var datasetViewer = this.initInkspotDatasetView();
                    if(datasetViewer){
                        datasetViewer.editDataset(files[0].escid);
                    } else {
                        alert("Sorry, Viewer not available");
                    }
                    
                } else {
                    alert("Sorry, Viewer not available");
                    
                }
            } else {
                // Normal file for quickviewer
                if (window.QuickView){
                    var viewer = this.initInkspotQuickView();
                    if (viewer) {
                        // Use the Inkspot / esc file viewer
                        if (files.length == 1) {
                            // Standard QuickView
                            var docId = files[0].escid;
                            var docName = files[0].name;
                            viewer.showFile(docId, docName);
                        }
                    } else {
                        // Fallback to built-in quick view
                        alert("Sorry, Viewer not available");
                    }
                } else {
                    // Fallback to built-in quick view
                    alert("Sorry, Viewer not available");
                }            
            }
        }
        

    };

  this.getstate = function (sel)
  {
    var sel = this.files(sel) ;
    return !this._disabled && sel.length == 1 && sel[0].phash && !sel[0].locked ? 0 : -1;
  };

  // Create a dataset viewer if possible
  this.initInkspotDatasetView = function(){
      if(window.DatasetManager){
          if(self.fm.datasetViewer){
              return self.fm.datasetViewer;
          } else {
            // Need to init
            self.fm.datasetViewer = new DatasetManager();
            if(self.fm.datasetViewerDivName){
                self.fm.datasetViewer.init(self.fm.datasetViewerDivName);
                return self.fm.datasetViewer;
            } else {
                var divName = self.fm.id + "-datasetviewerDiv";
                self.fm.datasetViewerDivName = divName;
                var div = document.createElement("div");
                div.setAttribute("id", divName);
                div.setAttribute("class", "dialog");
                var containerDiv = document.getElementById(self.fm.id);
                containerDiv.appendChild(div);
                self.fm.datasetViewer.init(divName);
                return self.fm.datasetViewer;
            }
          }
          
      } else {
          return null;  // Can't instantiate viewer
      }
  };
  
  // Create an inkspot / esc quickview window if possible
  this.initInkspotQuickView = function ()
  {
    if (window.QuickView)
    {
      // Has a viewer been created Ok
      if (self.viewer)
      {
        return self.viewer;
      } else
      {
        self.viewer = new QuickView();
        if (self.viewerDivName)
        {
          self.viewer.init(self.viewerDivName);
          return self.viewer;
        } else
        {
          var divName = self.fm.id + "-viewerDiv";
          self.viewerDivName = divName;
          var div = document.createElement("div");
          div.setAttribute("id", divName);
          var containerDiv = document.getElementById(self.fm.id);
          containerDiv.appendChild(div);
          self.viewer.init(divName);
          return self.viewer;
        }
      }
    } else
    {
      // Cannot create a quick view panel
      return null;
    }
  }
};

elFinder.prototype.commands.escinfo = function() {
	var m   = 'msg',
		fm  = this.fm,
		spclass = 'elfinder-info-spinner',
		msg = {
			calc     : fm.i18n('calc'),
			size     : fm.i18n('size'),
			unknown  : fm.i18n('unknown'),
			path     : fm.i18n('path'),
			aliasfor : fm.i18n('aliasfor'),
			modify   : fm.i18n('modify'),
			perms    : fm.i18n('perms'),
			locked   : fm.i18n('locked'),
			dim      : fm.i18n('dim'),
			kind     : fm.i18n('kind'),
			files    : fm.i18n('files'),
			folders  : fm.i18n('folders'),
			items    : fm.i18n('items'),
			yes      : fm.i18n('yes'),
			no       : fm.i18n('no'),
			link     : fm.i18n('link'),
      hash     : fm.hash
		};
		
	this.tpl = {
		main       : '<div class="ui-helper-clearfix elfinder-info-title"><span class="elfinder-cwd-icon {class} ui-corner-all"/>{title}</div><table class="elfinder-info-tb">{content}</table>',
		itemTitle  : '<strong>{name}</strong><span class="elfinder-info-kind">{kind}</span>',
		groupTitle : '<strong>{items}: {num}</strong>',
		row        : '<tr><td>{label} : </td><td>{value}</td></tr>',
		spinner    : '<span>{text}</span> <span class="'+spclass+'"/>'
	}
	
	this.alwaysEnabled = true;
	this.updateOnSelect = false;
	this.shortcuts = [{
		pattern     : 'ctrl+i'
	}];
	
	this.init = function() {
		$.each(msg, function(k, v) {
			msg[k] = fm.i18n(v);
		});
	}
	
	this.getstate = function() {
		return 0;
	}
	
	this.exec = function(hashes) {
		var self    = this,
			fm      = this.fm,
			tpl     = this.tpl,
			row     = tpl.row,
			files   = this.files(hashes),
			cnt     = files.length,
			content = [],
			view    = tpl.main,
			l       = '{label}',
			v       = '{value}',
			opts    = {
				title : this.title,
				width : 'auto',
				close : function() { $(this).elfinderdialog('destroy'); }
			},
			count = [],
			replSpinner = function(msg) { dialog.find('.'+spclass).parent().text(msg); },
			id = fm.namespace+'-info-'+$.map(files, function(f) { return f.hash }).join('-'),
			dialog = fm.getUI().find('#'+id), 
			size, tmb, file, title, dcnt;
			
		if (!cnt) {
			return $.Deferred().reject();
		}
			
		if (dialog.length) {
			dialog.elfinderdialog('toTop');
			return $.Deferred().resolve();
		}
		
			
		if (cnt == 1) {
			file  = files[0];
			
			view  = view.replace('{class}', fm.mime2class(file.mime));
			title = tpl.itemTitle.replace('{name}', fm.escape(file.i18 || file.name)).replace('{kind}', fm.mime2kind(file));

			if (file.tmb) {
				tmb = fm.option('tmbUrl')+file.tmb;
			}
			
			if (!file.read) {
				size = msg.unknown;
			} else if (file.mime != 'directory' || file.alias) {
				size = fm.formatSize(file.size);
			} else {
				size = tpl.spinner.replace('{text}', msg.calc);
				count.push(file.hash);
			}

			//SJW + SMW
			content.push(row.replace(l, "Id").replace(v, file.escid));

			content.push(row.replace(l, "Archival Status").replace(v, file.castatus));
  		        //End SJW + SMW

			content.push(row.replace(l, msg.size).replace(v, size));
			file.alias && content.push(row.replace(l, msg.aliasfor).replace(v, file.alias));
			content.push(row.replace(l, msg.path).replace(v, fm.escape(fm.path(file.hash, true))));
			file.read && content.push(row.replace(l, msg.link).replace(v,  '<a href="'+fm.url(file.hash)+'" target="_blank">'+file.name+'</a>'));
			
			if (file.dim) { // old api
				content.push(row.replace(l, msg.dim).replace(v, file.dim));
			} else if (file.mime.indexOf('image') !== -1) {
				if (file.width && file.height) {
					content.push(row.replace(l, msg.dim).replace(v, file.width+'x'+file.height));
				} else {
					content.push(row.replace(l, msg.dim).replace(v, tpl.spinner.replace('{text}', msg.calc)));
					fm.request({
						data : {cmd : 'dim', target : file.hash},
						preventDefault : true
					})
					.fail(function() {
						replSpinner(msg.unknown);
					})
					.done(function(data) {
						replSpinner(data.dim || msg.unknown);
					});
				}
			}
			
			
			content.push(row.replace(l, msg.modify).replace(v, fm.formatDate(file)));
			content.push(row.replace(l, msg.perms).replace(v, fm.formatPermissions(file)));
			content.push(row.replace(l, msg.locked).replace(v, file.locked ? msg.yes : msg.no));
		} else {
			view  = view.replace('{class}', 'elfinder-cwd-icon-group');
			title = tpl.groupTitle.replace('{items}', msg.items).replace('{num}', cnt);
			dcnt  = $.map(files, function(f) { return f.mime == 'directory' ? 1 : null }).length;
			if (!dcnt) {
				size = 0;
				$.each(files, function(h, f) { 
					var s = parseInt(f.size);
					
					if (s >= 0 && size >= 0) {
						size += s;
					} else {
						size = 'unknown';
					}
				});
				content.push(row.replace(l, msg.kind).replace(v, msg.files));
				content.push(row.replace(l, msg.size).replace(v, fm.formatSize(size)));
			} else {
				content.push(row.replace(l, msg.kind).replace(v, dcnt == cnt ? msg.folders : msg.folders+' '+dcnt+', '+msg.files+' '+(cnt-dcnt)))
				content.push(row.replace(l, msg.size).replace(v, tpl.spinner.replace('{text}', msg.calc)));
				count = $.map(files, function(f) { return f.hash });
				
			}
		}
		
		view = view.replace('{title}', title).replace('{content}', content.join(''));
		
		dialog = fm.dialog(view, opts);
		dialog.attr('id', id)

		// load thumbnail
		if (tmb) {
			$('<img/>')
				.load(function() { dialog.find('.elfinder-cwd-icon').css('background', 'url("'+tmb+'") center center no-repeat'); })
				.attr('src', tmb);
		}
		
		// send request to count total size
		if (count.length) {
			fm.request({
					data : {cmd : 'size', targets : count},
					preventDefault : true
				})
				.fail(function() {
					replSpinner(msg.unknown);
				})
				.done(function(data) {
					var size = parseInt(data.size);
					replSpinner(size >= 0 ? fm.formatSize(size) : msg.unknown);
				});
		}
		
	}
}

//END SJW
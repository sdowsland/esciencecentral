/* 
 * This class provides a simple table for editing a JSON object as a group of 
 * properties.
 */
function PropertyEditor(){
    this.divName = "";
    this.properties = null;
    this.inputEditor = new InputDialog();
    this.dateEditor = new DateTimeDialog();
    this.booleanEditor = new TrueFalseDialog();
    this.selectionEditor = new MultiValueSelectDialog();
    this.withoutDialog = false;
    this.withoutDataTables = false;
    
    // Add filechooser if it exists
    if(typeof FileChooser!=="undefined"){
        this.fileChooser = new FileChooser();
    }
    
    this.okCallback = null;
    this.cancelCallback = null;
    this.selections = {};
    this.eventManager = new EventManager();
    this.wildcardSelections = new Array();
    
    // Callback for the nput editor
    var editor = this;
    this.inputEditor.okCallback = function(value){
        editor.setProperty(this.propertyIndex, this.propertyName, value);
        this.propertyName = undefined;
    };
    
    // Callback for the date editor
    this.dateEditor.okCallback = function(value){
        editor.setProperty(this.propertyIndex, this.propertyName, value);
        this.propertyName = undefined;
    };
    
    // True / false editor
    this.booleanEditor.okCallback = function(value){
        editor.setProperty(this.propertyIndex, this.propertyName, value);
        this.propertyName = undefined;
    };
    
    // Selection editor
    this.selectionEditor.okCallback = function(value, index){
        this.selectedIndex = index;
        editor.setProperty(this.propertyIndex, this.propertyName, value);
        this.propertyName = undefined;
        this.objectList = undefined;
    };
    
    this.selectionEditor.cancelCallback = function(){
        this.propertyName = undefined;
        this.objectList = undefined;
    };
    
    if(this.fileChooser){
        this.fileChooser.okCallback = function(chooser){
            
            editor.setProperty(chooser.propertyIndex, chooser.propertyName, {id: chooser.selectedFileId, name: chooser.selectedFileName});
            chooser.propertyName = undefined;
        };
    }
}

PropertyEditor.prototype.init = function(divName, withoutDialog){
    this.withoutDialog = withoutDialog;
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var dialog = this;
        if(!withoutDialog){
            $(div).dialog({
                bgiframe: true,
                autoOpen: false,
                height: 500,
                width: 550,
                title: "Edit Properties",
                modal: true,
                viewer: this,
                buttons: {
                    'Ok' : function(){
                        if(dialog.okCallback){
                            dialog.okCallback(dialog.properties);
                        }
                        $(this).dialog('close');
                    },

                    'Cancel': function() {
                        if(dialog.cancelCallback){
                            dialog.cancelCallback();
                        }
                        $(this).dialog('close');
                    }

                }
            });        
        }
        
        // Create some dialog divs
        var inputDiv = document.createElement("div");
        inputDiv.setAttribute("id", this.divName + "_input_dialog");
        inputDiv.setAttribute("class", "dialog");
        div.appendChild(inputDiv);
        this.inputEditor.init(this.divName + "_input_dialog");
        
        var dateDiv = document.createElement("div");
        dateDiv.setAttribute("id", this.divName + "_date_dialog");
        dateDiv.setAttribute("class", "dialog");
        div.appendChild(dateDiv);
        this.dateEditor.init(this.divName + "_date_dialog");
        
        var trueFalseDiv = document.createElement("div");
        trueFalseDiv.setAttribute("id", this.divName + "_true_false_dialog");
        trueFalseDiv.setAttribute("class", "dialog");
        div.appendChild(trueFalseDiv);
        this.booleanEditor.init(this.divName + "_true_false_dialog");
        
        var selectionDiv = document.createElement("div");
        selectionDiv.setAttribute("id", this.divName + "_selection_dialog");
        selectionDiv.setAttribute("class", "dialog");
        div.appendChild(selectionDiv);
        this.selectionEditor.init(this.divName + "_selection_dialog");
        
        if(this.fileChooser){
            var chooserDiv = document.createElement("div");
            chooserDiv.setAttribute("id", this.divName + "_chooser_dialog");
            chooserDiv.setAttribute("class", "dialog");
            div.appendChild(chooserDiv);
            this.fileChooser.init(this.divName + "_chooser_dialog");
        }
    }
};

PropertyEditor.prototype.cleanUp = function(){
    this.inputEditor.cleanUp();
    this.booleanEditor.cleanUp();
    this.dateEditor.cleanUp();
    this.selectionEditor.cleanUp();
    if(!this.withoutDialog){
        $("#" + this.divName).parents(".ui-dialog").remove();
    }
};

PropertyEditor.prototype.editProperties = function(properties, noClone){
    // Copy the properties into this editor
    if(properties){
        if(noClone){
            this.properties = properties;
        } else {
            this.properties = $.extend(true, {}, properties);
        }
    }
    
    // Do the properties have an _options field. If so, set these up as selections
    if(this.properties._options){
        this.selections = this.properties._options;
    }
    
    var div = document.getElementById(this.divName);
    if(div){
        var i;
        var propertyIndex;
        var allNames = Object.getOwnPropertyNames(this.properties).sort();
        var names = new Array();
        for(i = 0;i<allNames.length;i++){
            if(!allNames[i].beginsWith("_")){
                names.push(allNames[i]);
            }
        }
        
        var html='<table id="' + this.divName + '_table" style="width: 100%;">';
        html+='<thead><tr><th>Name</th><th>Value</th><th>Edit</th></tr></thead><tbody>';
        
        for(i=0;i<names.length;i++){
            html+='<tr>';
            html+='<td>' + names[i] + '</td>';
            
            propertyIndex = allNames.indexOf(names[i]);
            
            html+='<td id="' + this.divName + '_' + propertyIndex + '_value">' + this.getDisplayValue(this.properties[names[i]]) + '</td>';
            html+='<td><img style="cursor:pointer; margin-bottom:0px;" src="' + rewriteAjaxUrl("../../scripts/properties/images/pencil.png") + '" id="' + this.divName + '_' + propertyIndex + '"/></td>';
            html+='</tr>';
        }
        html+='</tbody></table>';
        div.innerHTML = html;
        
        if(!this.withoutDataTables){
            $('#' + this.divName + "_table").dataTable({"bJQueryUI":true});
        }
        
        // Add the action listeners
        var valueDiv;
        var dialog = this;
        for(i=0;i<names.length;i++){
            propertyIndex = allNames.indexOf(names[i]);
            valueDiv = document.getElementById(this.divName + "_" + propertyIndex);
            if(valueDiv){
                valueDiv.propertyJson = this.properties[names[i]];
                valueDiv.propertyName = names[i];
                valueDiv.propertyIndex = propertyIndex;
                valueDiv.onclick = function(){
                    dialog.editProperty(this.propertyIndex, this.propertyName, this.propertyJson);
                }
            }
        }
        
        if(!this.withoutDialog){
            $(div).dialog('open');
        }
    }
};

PropertyEditor.prototype.editProperty = function(index, name, jsonValue){
    var s;
    
    if(typeof jsonValue=="string"){
        s = this.getSelectionsForValue(name);
        if(s){
            this.selectionEditor.propertyName = name;
            this.selectionEditor.propertyIndex = index;
            this.selectionEditor.show(s, jsonValue);
        } else {
            this.inputEditor.propertyName = name;
            this.inputEditor.propertyIndex = index;
            this.inputEditor.show(jsonValue);
            
        }
        
    } else if(typeof jsonValue=="number"){
        this.inputEditor.propertyName = name;
        this.inputEditor.propertyIndex = index;
        this.inputEditor.show(jsonValue);
        
    } else if(typeof jsonValue=="boolean"){
        this.booleanEditor.propertyName = name;
        this.booleanEditor.propertyIndex = index;
        this.booleanEditor.show(jsonValue);
        
    } else if(typeof jsonValue=="object"){
        if(jsonValue instanceof Date){
            this.dateEditor.propertyName = name;
            this.dateEditor.propertyIndex = index;
            this.dateEditor.show(jsonValue);
            
        } else if(jsonValue._type && jsonValue._type=="java.util.Date"){
            // Custom Date object
            this.dateEditor.propertyName = name;
            this.dateEditor.propertyIndex = index;
            var dt = new Date(jsonValue._milliseconds);
            this.dateEditor.show(dt);
            
        } else if(jsonValue._type && jsonValue._type=="Workflow"){
            // Pick a workflow
            this.fileChooser.propertyName = name;
            this.fileChooser.propertyIndex = index;
            this.fileChooser.showHomeFolder(false);
            
        } else if(jsonValue._type && jsonValue._type=="Project"){
            // Choose a project
            if(jsonValue.className){
                // Fetch a list of all Projects of the specified class that
                // the user can see
                var results = JSON.parse(jsonPost(rewriteAjaxUrl("../../servlets/viewer?method=listProjects"), {className: jsonValue.className}).responseText);
                if(!results.error){
                    var projectList = results.projects;
                    this.selectionEditor.objectList = projectList;
                    this.selectionEditor.propertyName = name;
                    this.selectionEditor.propertyIndex = index;                    
                    var names = new Array();
                    for(var i=0;i<projectList.length;i++){
                        names[i] = projectList[i].name;
                    }
                    this.selectionEditor.show(names);
                }
            }
            
        } else if(jsonValue._type && jsonValue._type==="ServerObject"){
            if(jsonValue.className){
                var results = JSON.parse(jsonPost(rewriteAjaxUrl("../../servlets/viewer?method=listServerObjects"), {className: jsonValue.className}).responseText);
                if(!results.error){
                    var credentialList = results.objects;
                    this.selectionEditor.objectList = credentialList;
                    this.selectionEditor.propertyName = name;
                    this.selectionEditor.propertyIndex = index;                    
                    var names = new Array();
                    for(var i=0;i<credentialList.length;i++){
                        names[i] = credentialList[i].name;
                    }
                    this.selectionEditor.show(names);
                }            
            }
            
        } else if(jsonValue instanceof Array){
            this.inputEditor.propertyName = name;
            this.inputEditor.propertyIndex = index;
            this.inputEditor.show(this.arrayToString(jsonValue));            
        }
        else if(jsonValue._type && jsonValue._type==="Credentials"){
                var results = JSON.parse(jsonPost(rewriteAjaxUrl("../../servlets/admin?method=listCredentials"), {className: jsonValue.className}).responseText);
                if(!results.error){
                    var credentialList = results.credentials;
                    this.selectionEditor.objectList = credentialList;
                    this.selectionEditor.propertyName = name;
                    this.selectionEditor.propertyIndex = index;
                    var names = new Array();
                    for(var i=0;i<credentialList.length;i++){
                        names[i] = credentialList[i].name;
                    }
                    this.selectionEditor.show(names, jsonValue.name);
                }
        }
    
    } else {
        alert("Editing property: " + index + ": " + name + "=" + jsonValue + "-" + typeof jsonValue);
    }
};


PropertyEditor.prototype.setProperty = function(index, name, value){
    if(this.properties){
        var oldProperty = this.properties[name];
        if(oldProperty!=undefined){
            if(typeof oldProperty == "string"){
                // Set string
                this.properties[name] = value.toString();
                document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.properties[name];
                document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];
                
            } else if(typeof oldProperty == "number"){
                // Set number
                if(!isNaN(parseFloat(value))){
                    this.properties[name] = parseFloat(value);
                    document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.properties[name];
                    document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];
                }
                
            } else if(typeof oldProperty == "boolean"){
                // Set a boolean
                if(typeof value=="boolean"){
                    this.properties[name] = value;
                    document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.properties[name];
                    document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];
                }
                
            } else if(typeof oldProperty == "object"){
                // Object value
                if(oldProperty instanceof Date && value instanceof Date){
                    // Setting a date
                    this.properties[name] = value;
                    document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.properties[name];
                    document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];
                    
                } else if(oldProperty._type && oldProperty._type=="java.util.Date"){
                    // Custom date obejct
                    this.properties[name] = {
                        _milliseconds: value.getTime(),
                        _type: "java.util.Date",
                        value: value.toUTCString()
                    }
                    document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.getDisplayValue(this.properties[name]);
                    document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];
                    
                } else if(oldProperty._type && oldProperty._type=="Workflow"){
                    // Workflow file
                    this.properties[name] = {
                        _type: "Workflow",
                        value: value.name,
                        id: value.id
                    }
                    document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.getDisplayValue(this.properties[name]);
                    document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];                    
                    
                } else if(oldProperty._type && oldProperty._type=="Project"){
                    // Set a project
                    if(this.selectionEditor.objectList){
                        this.properties[name] = this.selectionEditor.objectList[this.selectionEditor.selectedIndex];
                        document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.getDisplayValue(this.properties[name]);
                        document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];                           
                    }
                    
                } else if(oldProperty._type && oldProperty._type=="ServerObject"){
                    // Set some windows Azure credentials
                    if(this.selectionEditor.objectList){
                        this.properties[name] = this.selectionEditor.objectList[this.selectionEditor.selectedIndex];
                        document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.getDisplayValue(this.properties[name]);
                        document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];                                
                    }
                    
                } else if(oldProperty instanceof Array){
                    // Array
                    this.properties[name] = this.stringToArray(value);
                    document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.arrayToString(this.properties[name]);
                    document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];                    
                }
                else if(oldProperty._type && oldProperty._type=="Credentials"){
                    // Set some windows Azure credentials
                    if(this.selectionEditor.objectList){
                        this.properties[name] = this.selectionEditor.objectList[this.selectionEditor.selectedIndex];
                        document.getElementById(this.divName + "_" + index + "_value").innerHTML = this.getDisplayValue(this.properties[name]);
                        document.getElementById(this.divName + "_" + index).propertyJson = this.properties[name];
                    }

                }
            }
        }
        
        // Raise an Event
        this.eventManager.trigger("propertyChanged");
    }
};

/** Get any set of options for a specified property name */
PropertyEditor.prototype.getSelectionsForValue = function(name){
    if(this.selections[name]){
        return this.selections[name];
    } else {
        var wildcardName = this.matchSelectionWithWildcard(name);
        if(wildcardName){
            if(this.selections[wildcardName]){
                return this.selections[wildcardName];
            } else {
                return  null;
            }
        }
    }
};

/** Find a wildcard matching the name if there is one */
PropertyEditor.prototype.matchSelectionWithWildcard = function(name){
    var expressionData;
    if(name.findPattern){
        for(var i=0;i<this.wildcardSelections.length;i++){
            expressionData = this.wildcardSelections[i];
            if(name.findPattern(expressionData.wildcard)){
                return expressionData.selectionName;
            }
        }
        return null;
    } else {
        return null;
    }
};

/** Add a wildcard to match a selection */
PropertyEditor.prototype.addWildcardForSelections = function(wildcard, selectionName){
    var expressionData = {
        wildcard: wildcard,
        selectionName: selectionName
    }
    this.removeWildcardForSelections(wildcard);
    this.wildcardSelections.push(expressionData);
};

/** Remove a wildcard for a selection */
PropertyEditor.prototype.removeWildcardForSelections = function(wildcard){
    var index = -1;
    for(var i=0;i<this.wildcardSelections.length;i++){
        if(this.wildcardSelections[i].wildcard==wildcard){
            index = i;
        }
    }
    if(index!=-1){
        this.wildcardSelections.splice(index, 1);
    }
};

/** Get the display text for a JSON property value */
PropertyEditor.prototype.getDisplayValue = function(value){
    var displayValue;
    // Get the correct text display value
    if(typeof value=="object"){
        if(value._type){
            displayValue = value.value;
        } else {
            if(value instanceof Array){
                displayValue = this.arrayToString(value);
            } else {
                displayValue = "[ObjectValue]";
            }
        }
    } else {
        displayValue = value
    }    
    return displayValue;
};

PropertyEditor.prototype.arrayToString = function(value){
    var displayValue='';
    for(var i=0;i<value.length;i++){
        if(i==0){
            displayValue+=value[i];
        } else {
            displayValue+=','+value[i];
        }
    }  
    return displayValue;
};

PropertyEditor.prototype.stringToArray = function(value){
    return value.split(',');
};


String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
};

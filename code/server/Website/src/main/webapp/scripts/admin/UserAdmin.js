/*
 * This script provides user administration controls.
 */

function UserAdmin(){
    this.userList = new Array();
    this.selectedUser = null;
    this.passwordDialogDivName = "";
    this.passwordUserId = "";
    this.passwordLogon = "";
    this.quotaDialogDivName = "";
    this.createUserDialogDivName = "";
    this.quotaUserId = "";
    this.groupDivName = "";
    this.groupUserId = "";
    this.deleteUserConfirmDialog = new ConfirmDialog();
    this.lockReasonDialog = new MultiValueSelectDialog();

}

UserAdmin.prototype.searchUsers = function(searchText, callback){
    var url = "../../servlets/admin?method=searchForUsers";
    var callData = {searchText: searchText};
    var admin = this;
    var cb = function(o){
        if(!o.error){
            admin.userList = o.users;
            callback();
        }
    };
    jsonCall(callData, url, cb);
};

UserAdmin.prototype.getUserById = function(id, callback){
    var callData = {
        userId: id
    };
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };

    jsonCall(callData, "../../servlets/admin?method=getUserAndQuota", callback);
};

UserAdmin.prototype.getUserByLogon = function(logon, callback){
    var url = "../../servlets/admin?method=getUserByLogon";
    var callData = {logon: logon};
    var admin = this;
    var cb = function(o){
        if(!o.error){
            admin.selectedUser = o.user;
            callback();
        }
    };
    jsonCall(callData, url, cb);
};

UserAdmin.prototype.initDeleteUserConfirmDialog = function(divName){
    this.deleteUserConfirmDialog.init(divName);
};

UserAdmin.prototype.initLockReasonDialog = function(divName) {
    this.lockReasonDialog.init(divName);
};

UserAdmin.prototype.initPasswordDialog = function(divName){
    this.passwordDialogDivName = divName;

    var div = document.getElementById(this.passwordDialogDivName);
    var htmlString = '<div id="' + this.passwordDialogDivName + '_error"></div>'+
        ' <form>' +
        '        <div class="formInput">' +
        '          <label style="display: inline-block; width: 170px;" for="logon">Logon</label>' +
        '          <input type="text" name="logon" id="' + this.passwordDialogDivName + '_logon"/>'+
        '        </div>' +
        '        <div class="formInput">' +
        '          <label style="display: inline-block; width: 170px;" for="password1">New Password</label>' +
        '          <input type="password" name="password1" id="' + this.passwordDialogDivName + '_password1"/>' +
        '        </div>' +
        '        <div class="formInput">' +
        '          <label style="display: inline-block; width: 170px;" for="password2">Confirm Password</label>' +
        '          <input type="password" name="password2" id="' + this.passwordDialogDivName + '_password2"/>' +
        '        </div>' +
        '  </form>';
    var manager = this;
    $('#' + this.passwordDialogDivName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 300,
        width: 500,
        title: "Change Password",
        modal: true,
        buttons: {
            'Ok': function() {
                var password1 = document.getElementById(manager.passwordDialogDivName + "_password1").value;
                var password2 = document.getElementById(manager.passwordDialogDivName + "_password2").value;
                var logon = document.getElementById(manager.passwordDialogDivName + "_logon").value;
                if(password1==password2){
                    manager.setUserPassword(manager.passwordUserId, logon, password1);
                    $(this).dialog('close');
                } else {
                    $("#" + manager.passwordDialogDivName + "_error").html("<span class='red'>Passwords do not match</span>");
                }
            },
            'Cancel' : function(){
                $(this).dialog('close');
            }
        }
    });
    div.innerHTML = htmlString;
};

UserAdmin.prototype.showPasswordDialog = function(userId, logon){
    this.passwordUserId = userId;
    this.passwordLogon = logon;
    $('#' + this.passwordDialogDivName + "_error").html("");
    $('#' + this.passwordDialogDivName + "_password1").val("");
    $('#' + this.passwordDialogDivName + "_password2").val("");
    $('#' + this.passwordDialogDivName + "_logon").val(logon);
    $('#' + this.passwordDialogDivName).dialog('open');
};

UserAdmin.prototype.setUserPassword = function(userId, newLogon, newPassword){
    var url = "../../servlets/admin?method=setUserPassword";
    var callData = {
        userId: userId,
        logon: newLogon,
        password: newPassword
    };

    var admin = this;
    var cb = function(o){
        $('#' + admin.passwordDialogDivName).dialog('close');
    };
    jsonCall(callData, url, cb);
};

/** Display a list of users in a div */
UserAdmin.prototype.displayUserList = function(divName, urlRoot){
    var div = document.getElementById(divName);
    if(div){
        div.innerHTML = "";
        var user;
        for(var i=0;i<this.userList.length;i++){
            user = this.userList[i];
            div.appendChild(this.createUserDiv(user));
        }
    }
};

/** Create a div for a user */
UserAdmin.prototype.createUserDiv = function(user){
    var manager = this;
    var userDiv;
    var titleDiv;
    var mainTextDiv;
    var bottomTextDiv;
    var img;
    var impersonate;
    var quota;
    var groups;

    var password;
    var deleteUser;
    var lockUnlock;

    var type = "small profile";
    userDiv = document.createElement("div");
    userDiv.setAttribute("id", "user_" + user.id);
    if(user.enabled){
        userDiv.setAttribute("class", "user-profile clearfix");
    } else {
        userDiv.setAttribute("class", "user-profile clearfix account-locked");
    }

    img = document.createElement("img");
    img.setAttribute("src", rewriteAjaxUrl("../../servlets/image?soid=" + user.id + "&type=" + type));
    img.setAttribute("alt", "Profile picture");
    img.setAttribute("class", "img-polaroid img-circle");

    titleDiv = document.createElement("h4");
    titleDiv.setAttribute("class", "user-name");
    titleDiv.setAttribute("id", "title_" + user.id);

    if(user.enabled){
        titleDiv.appendChild(document.createTextNode(user.displayName));
    } else {
        titleDiv.appendChild(document.createTextNode(user.displayName + " [" + user.lockReason + "]"));
    }

    mainTextDiv = document.createElement("h6");
    mainTextDiv.setAttribute("class", "user-email");
    mainTextDiv.innerHTML = user.logon;

    bottomTextDiv = document.createElement("ul");
    bottomTextDiv.setAttribute("class", "inline user-menu");

    impersonate = document.createElement("li");
    impersonate.appendChild(document.createTextNode("Impersonate"));
    impersonate.userId = user.id;
    impersonate.onclick = function(){
        manager.switchUser(this.userId);
    };
    bottomTextDiv.appendChild(impersonate);

    groups = document.createElement("li");
    groups.appendChild(document.createTextNode("Groups"));
    groups.userId = user.id;
    groups.onclick = function(){
        manager.showGroupDialog(this.userId);
    };
    bottomTextDiv.appendChild(groups);

    quota = document.createElement("li");
    quota.appendChild(document.createTextNode("Quota"));
    quota.userId = user.id;
    quota.onclick = function(){
        manager.showQuotaDialog(this.userId);
    };
    bottomTextDiv.appendChild(quota);

    password = document.createElement("li");
    password.appendChild(document.createTextNode("Change Password"));
    password.userId = user.id;
    password.logonName = user.logon;

    password.onclick = function(){
        manager.showPasswordDialog(this.userId, this.logonName);
    };
    bottomTextDiv.appendChild(password);

    deleteUser = document.createElement("li");
    deleteUser.setAttribute("id", "delete_" + user.id);
    deleteUser.appendChild(document.createTextNode("Delete"));
    deleteUser.userId = user.id;
    deleteUser.onclick = function(){
        manager.showDeleteUserDialog(this.userId);
    };
    bottomTextDiv.appendChild(deleteUser);

    lockUnlock = document.createElement("li");
    lockUnlock.setAttribute("id", "lockunlock_" + user.id);
    if(user.enabled){
        lockUnlock.appendChild(document.createTextNode("Lock"));
        lockUnlock.onclick = function(){
            manager.showLockDialog(this.userId);
        };

    } else {
        lockUnlock.appendChild(document.createTextNode("Unlock"));
        lockUnlock.onclick = function(){
            manager.unlockAccount(this.userId);
        }
    }
    lockUnlock.userId = user.id;
    bottomTextDiv.appendChild(lockUnlock);

    userDiv.appendChild(img);
    userDiv.appendChild(titleDiv);
    userDiv.appendChild(mainTextDiv);
    userDiv.appendChild(bottomTextDiv);

    return userDiv;
};

UserAdmin.prototype.lockAccount = function(userId, reason){
    var manager = this;
    var cb = function(o){
        if(!o.error){
            var lockUnlock = document.getElementById("lockunlock_" + o.user.id);
            var title = document.getElementById("title_" + o.user.id);
            if(o.user.enabled){
                $("#user_" + o.user.id).removeClass("account-locked");
                lockUnlock.innerHTML = "";
                lockUnlock.appendChild(document.createTextNode("Lock"));
                lockUnlock.onclick = function(){
                    manager.showLockDialog(this.userId);
                };

                title.innerHTML = "";
                title.appendChild(document.createTextNode(o.user.displayName));

            } else {
                $("#user_" + o.user.id).addClass("account-locked");
                lockUnlock.innerHTML = "";
                lockUnlock.appendChild(document.createTextNode("Unlock"));
                lockUnlock.onclick = function(){
                    manager.unlockAccount(this.userId);
                };

                title.innerHTML = "";
                title.appendChild(document.createTextNode(o.user.displayName + " [" + o.user.lockReason + "]"));
            }

            $.jGrowl("User account locked");
        }
    };
    jsonCall({userId: userId, reason: reason}, "../../servlets/admin?method=lockAccount", cb);
};

UserAdmin.prototype.showLockDialog = function(userId){
    var lockUserId = userId;
    var admin = this;
    this.lockReasonDialog.okCallback = function(value, index){
        admin.lockAccount(lockUserId, value);
    };

    var options = new Array();
    options.push("Expiry Lock");
    options.push("Inactivity Lock");
    options.push("Administrator Lock");

    this.lockReasonDialog.show(options, "Administrator Lock");

};

UserAdmin.prototype.unlockAccount = function(userId){
    var manager = this;
    var cb = function(o){
        if(!o.error){
            if(o.user && o.user.id){
                var lockUnlock = document.getElementById("lockunlock_" + o.user.id);
                var title = document.getElementById("title_" + o.user.id);

                if(o.user.enabled){
                    $("#user_" + o.user.id).removeClass("account-locked");
                    lockUnlock.innerHTML = "";
                    lockUnlock.appendChild(document.createTextNode("Lock"));
                    lockUnlock.onclick = function(){
                        manager.showLockDialog(this.userId);
                    };

                    title.innerHTML = "";
                    title.appendChild(document.createTextNode(o.user.displayName));

                } else {
                    $("#user_" + o.user.id).addClass("account-locked");
                    lockUnlock.innerHTML = "";
                    lockUnlock.appendChild(document.createTextNode("Unlock"));
                    lockUnlock.onclick = function(){
                        manager.unlockAccount(this.userId);
                    };

                    title.innerHTML = "";
                    title.appendChild(document.createTextNode(o.user.displayName + " [" + o.user.lockReason + "]"));
                }
            }
            $.jGrowl("User account unlocked");
        }
    };
    jsonCall({userId: userId}, "../../servlets/admin?method=unlockAccount", cb);
};

/** Switch the current user in the session */
UserAdmin.prototype.switchUser = function(userId, callback){
    var url = "../../servlets/admin?method=switchUser";
    var callData = {userId: userId};
    var admin = this;
    var cb = function(o){
        if(!o.error){
            document.location = "../front/index.jsp";
        }
    };
    jsonCall(callData, url, cb);
    jsonGet('/website-api/rest/account/switchsession/' + userId);
};

UserAdmin.prototype.initQuotaDialog = function(divName){
    this.quotaDialogDivName = divName;
    var div = document.getElementById(this.quotaDialogDivName);

    var htmlString = ' <form>' +
        '        <div class="formInput">' +
        '          <label style="width:170px; display:inline-block;" for="quotaenabled">Enabled</label>' +
        '          <input class="higherButton" type="checkbox" name="quotaenabled" id="' + this.quotaDialogDivName + '_enabled"/>'+
        '        </div>' +
        '        <div class="formInput">' +
        '          <label style="width:170px; display:inline-block;" for="quotasize">Quota size (MB)</label>' +
        '          <input type="text" name="quotasize" id="' + this.quotaDialogDivName + '_size"/>' +
        '        </div>' +
        '  </form>';
    var manager = this;
    $('#' + this.quotaDialogDivName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 250,
        width: 500,
        title: "Set User Quota",
        modal: true,
        buttons: {
            'Ok': function()
            {
                var quota = document.getElementById(manager.quotaDialogDivName + "_size").value;
                var enabled = document.getElementById(manager.quotaDialogDivName + "_enabled").checked;
                manager.setQuota(manager.quotaUserId, enabled, quota, function(){
                    $("#" + manager.quotaDialogDivName).dialog('close');
                    manager.quotaUserId = undefined;
                });
            },
            'Cancel' : function(){
                $(this).dialog('close');
            }
        }
    });
    div.innerHTML = htmlString;
};

UserAdmin.prototype.showQuotaDialog = function(userId){
    var div = document.getElementById(this.quotaDialogDivName);
    if(div){
        var manager = this;
        var callData = {
            userId: userId
        };
        this.quotaUserId = userId;
        var callback = function(o){
            if(!o.error){
                // Set up the dialog
                var quotaSizeBox = document.getElementById(manager.quotaDialogDivName + "_size");
                if(quotaSizeBox){
                    quotaSizeBox.value = o.user.storageQuota;
                }

                var quotaEnabledBox = document.getElementById(manager.quotaDialogDivName + "_enabled");
                if(quotaEnabledBox){
                    quotaEnabledBox.checked = o.user.quotaEnforced;
                }

                $(div).dialog('open');

            }
        };
        jsonCall(callData, "../../servlets/admin?method=getUserAndQuota", callback);
    }

};

UserAdmin.prototype.setQuota = function(userId, enabled, size, callback){
    var callData = {
        userId: userId,
        enabled: enabled,
        size: size
    };
    jsonCall(callData, "../../servlets/admin?method=setUserQuota", callback);
};

UserAdmin.prototype.initGroupDialog = function(groupDivName){
    this.groupDivName = groupDivName;
    var div = document.getElementById(this.groupDivName);
    if(div){
        var htmlString = '<h1>Groups</h1>';
        var manager = this;
        $('#' + this.groupDivName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 620,
            width: 600,
            title: "Set User Group Membership",
            modal: true,
            buttons: {
                'Ok': function() {


                    if(div.groups && div.groupMembership){

                        //The pages of the dataTable which aren't currently displayed are not present in the DOM
                        //This is the way to get the selected  items
                        var selected =  $('input', groupsDialogDatatable.fnGetNodes());

                        var newMembership = [];
                        $(selected).each(function(){
                            if(this.checked === true)
                            {
                                var parts = this.name.split("_");
                                var groupId = parts[parts.length - 1];
                                console.log(groupId);
                                newMembership.push(groupId);
                            }
                        });

                        manager.setGroupMembership(manager.groupUserId, div.groupMembership, newMembership, function(){
                            $("#" + manager.groupDivName).dialog('close');
                            manager.groupUserId = undefined;
                            div.groups = undefined;
                            div.groupMembership = undefined;
                        });

                    }
                    div.groups = undefined;
                    div.groupMembership = undefined;
                    $(this).dialog('close');
                },
                'Cancel' : function(){
                    $(this).dialog('close');
                }
            }
        });
        div.innerHTML = htmlString;

    }
};

UserAdmin.prototype.showGroupDialog = function(userId){
    var div = document.getElementById(this.groupDivName);
    if(div){
        this.groupUserId = userId;
        var manager = this;
        var callData = {
            userId: userId
        };

        var callback = function(o){
            if(!o.error){
                var group;
                var htmlString = '<form id="setGroupsForm"><table class="table table-striped table-bordered" id="' + manager.groupDivName + '_table"><thead><tr>';
                htmlString+='<th>Type</th><th>Name</th><th>Member</th>';
                htmlString+='</tr></thead><tbody>';
                for(var i=0;i<o.groups.length;i++){
                    group = o.groups[i];
                    htmlString+='<tr>';
                    if(group.isProject){
                        htmlString+='<td>Project</td>';
                    } else {
                        htmlString+='<td>Group</td>';
                    }
                    htmlString+='<td>' + group.name + '</td>';

                    if(o.membership.indexOf(group.id)!=-1){
                        htmlString+='<td><input id="' + manager.groupDivName + '_member_' + group.id + '" type="checkbox" name="' + manager.groupDivName + '_member_' + group.id + '" checked /></td>';
                    } else {
                        htmlString+='<td><input id="' + manager.groupDivName + '_member_' + group.id + '" type="checkbox" name="' + manager.groupDivName + '_member_' + group.id + '"/></td>';
                    }

                    htmlString+='</tr>';
                }
                htmlString+='</tbody></table></form>';
                div.innerHTML = htmlString;

                div.groups = o.groups;
                div.groupMembership = o.membership;

                groupsDialogDatatable = $("#" + manager.groupDivName + "_table").dataTable({
                    "bJQueryUI":true,
                    "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>"
                });
                $(div).dialog('open');
            }
        };

        jsonCall(callData, "../../servlets/admin?method=getGroupMembershipForUser", callback);
    }
};

UserAdmin.prototype.setGroupMembership = function(userId, oldMembership, newMembership, callback) {
    var callData = {
        userId: userId,
        newMembership: newMembership,
        oldMembership: oldMembership
    };
    jsonCall(callData, "../../servlets/admin?method=saveGroupMembershipForUser", callback);
};

UserAdmin.prototype.initCreateUserDialog = function(divName){
    this.createUserDialogDivName = divName;
    var div = document.getElementById(this.createUserDialogDivName);
    if(div){
        var htmlString = ' <form>' +
            '        <div class="formInput">' +
            '          <label style="display: inline-block; width: 170px;" for="firstname">First name</label>' +
            '          <input type="text" size="40" name="firstname" id="' + this.createUserDialogDivName + '_firstname"/>'+
            '        </div>' +
            '        <div class="formInput">' +
            '          <label style="display: inline-block; width: 170px;" for="lastname">Last name</label>' +
            '          <input type="text" size="40" name="lastname" id="' + this.createUserDialogDivName + '_lastname"/>'+
            '        </div>' +
            '        <div class="formInput">' +
            '          <label style="display: inline-block; width: 170px;" for="email">E-Mail address</label>' +
            '          <input type="text" size="40" name="email" id="' + this.createUserDialogDivName + '_email"/>'+
            '        </div>' +
            '        <div class="formInput">' +
            '          <label style="display: inline-block; width: 170px;" for="password1">Password</label>' +
            '          <input type="password" size="40" name="password1" id="' + this.createUserDialogDivName + '_password1"/>'+
            '        </div>' +
            '        <div class="formInput">' +
            '          <label style="display: inline-block; width: 170px;" for="password2">Password (again)</label>' +
            '          <input type="password" size="40" name="password2" id="' + this.createUserDialogDivName + '_password2"/>'+
            '        </div>' +
            '        <div class="formInput">' +
            '                     <label style="display: inline-block; width: 170px;" for="noDash">Disable Dashboards</label>' +
            '                       <input type="radio"  name="noDash" id="' + this.createUserDialogDivName + '_noDash"/>'+
            '                     </div>' +
            '' +
            '' +
            '  </form>';
        var manager = this;
        $('#' + this.createUserDialogDivName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 400,
            width: 600,
            title: "Create User",
            modal: true,
            buttons: {
                'Cancel' : function(){
                    $(this).dialog('close');
                },
                'Ok': function()
                {

                    var firstname = document.getElementById(manager.createUserDialogDivName + "_firstname").value;
                    var lastname = document.getElementById(manager.createUserDialogDivName + "_lastname").value;
                    var email = document.getElementById(manager.createUserDialogDivName + "_email").value;
                    var password1 = document.getElementById(manager.createUserDialogDivName + "_password1").value;
                    var password2 = document.getElementById(manager.createUserDialogDivName + "_password2").value;
                    var noDash = document.getElementById(manager.createUserDialogDivName + "_noDash").checked;
                    if(password1===password2){
                        manager.createUser(email, firstname, lastname, password1, noDash, function(o){
                            $("#" + manager.createUserDivName).dialog("close");
                            document.location = "../../pages/admin/useradmin.jsp?userid=" + o.id;
                        });
                    } else {
                        $.jGrowl("Passwords do not match");
                    }

                    /*
                     var enabled = document.getElementById(manager.quotaDialogDivName + "_enabled").checked;
                     manager.setQuota(manager.quotaUserId, enabled, quota, function(){
                     $("#" + manager.quotaDialogDivName).dialog('close');
                     manager.quotaUserId = undefined;
                     });
                     */
                }
            }
        });
        div.innerHTML = htmlString;
    }
};

UserAdmin.prototype.showDeleteUserDialog = function(userId){
    this.deleteUserConfirmDialog.yesCallback = function(){
        var cb = function(){
            location.reload();
        };
        jsonCall({id: userId}, rewriteAjaxUrl("../../servlets/admin?method=deleteUser"), cb);
    };
    this.deleteUserConfirmDialog.show("Are you sure you want to delete the user: " + userId);
};

UserAdmin.prototype.showCreateUserDialog = function(){
    $("#" + this.createUserDialogDivName).dialog('open');
};

UserAdmin.prototype.createUser = function(email, firstname, lastname, password, noDash, callback){
    var callData = {
        email: email,
        firstname: firstname,
        lastname: lastname,
        password: password,
        noDash: noDash
    };

    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };

    jsonCall(callData, "../../servlets/admin?method=createUser", cb);
};
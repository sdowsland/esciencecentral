/* 
 * This script provides code to manage workflow locks.
 */
function WorkflowLockAdmin(){
    this.divName = "";
    this.locks = new Array();
    this.displayLockMembers = function(admin, o){
        var div = document.getElementById(admin.divName + "_members_dialog");
        if(div){
            var html='<table cellpadding="0" cellspacing="0" border="0" width="100% " class="display" id="' + admin.divName + '_members_table"><thead><tr>';
            html+='<th>Start Time</th><th>Engine ID</th><th>Status</th>';
            html+='</tr></thead><tbody>';

            var i;
            var member;
            for(i=0;i<o.members.length;i++){
                member = o.members[i];

                html+='<tr>';
                html+='<td>' + member.startTime + '</td>';
                html+='<td>' + member.engineId + '</td>';
                html+='<td>' + member.statusMessage + '</td>';
                html+='</tr>';
            }

            html+='</tbody';
            html+="</table>";    
            div.innerHTML = html;
            $("#" + admin.divName + "_members_table").dataTable({"bJQueryUI": true});
        }
    }
}

WorkflowLockAdmin.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var statusDialog = document.createElement("div");
        statusDialog.setAttribute("id", this.divName + "_members_dialog");
        statusDialog.setAttribute("class", "dialog");
        div.appendChild(statusDialog);
        $(statusDialog).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 500,
            width: 700,
            title: "Lock Members",
            modal: true,
            buttons: {
                'Attempt Redelivery' : function(){
                    this.admin.attemptLockRedelivery(this.lockId);
                },
                
                'Delete Lock' : function(){
                    this.admin.deleteLock(this.lockId);
                },
                
                'Refresh' : function(){
                    this.admin.viewLock(this.lockId);
                },                
                
                'Ok': function() {
                    $(this).dialog('close');
                }
            },    
            close : function(){
                this.lockId = undefined;
                this.admin.fetchLocks();
            }
        });    
        statusDialog.admin = this;
    }
};

/** Fetch the list of workflow locks from the server */
WorkflowLockAdmin.prototype.fetchLocks = function(callback, errorCallback){
    var mgr = this;
    var cb = function(o){
        if(!o.error){
            mgr.locks = o.locks;
            mgr.displayLocks();
        }
        if(callback){
            callback();
        }
    };
    
    var errCb = function(){
        if(errorCallback){
            errorCallback();
        }
    };
   
   jsonCall({}, "../../servlets/admin?method=listWorkflowLocks", cb, errCb);
};

/** Display the workflow locks in the table */
WorkflowLockAdmin.prototype.displayLocks = function(){
    var html='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="' + this.divName + '_table"><thead><tr>';
    html+='<th>Last Change</th><th>Workflow</th><th>User</th><th>Status</th><th>View</th>';
    html+='</tr></thead><tbody>';
    
    var i;
    var lock;
    for(i=0;i<this.locks.length;i++){
        lock = this.locks[i];
        
        html+='<tr>';
        html+='<td>' + lock.lastChange + '</td>';
        html+='<td>' + lock.workflowName + '</td>';
        html+='<td>' + lock.user + '</td>';
        html+='<td>' + lock.statusMessage + '</td>';
        html+='<td><a id="' + this.divName + '_viewlock_' + i + '">View</a></td>';
        html+='</tr>';
    }
    
    html+='</tbody';
    html+="</table>";
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = html;
        
        // Add the callbacks
        var viewLock;
        var admin = this;
        for(i=0;i<this.locks.length;i++){
            lock = this.locks[i];
            viewLock = document.getElementById(this.divName + "_viewlock_" + i);
            if(viewLock){
                viewLock.lockId = lock.id;
                viewLock.onclick = function(){
                    admin.viewLock(this.lockId);
                }
            }
        }
        $("#" + this.divName + "_table").dataTable({"bJQueryUI": true});
    }    
};

/** Display the status of an individual lock */
WorkflowLockAdmin.prototype.viewLock = function(lockId){
    var div = document.getElementById(this.divName + "_members_dialog");
    div.innerHTML = "Fetching...";
    document.getElementById(this.divName + "_members_dialog").lockId = lockId;
    $("#" + this.divName + "_members_dialog").dialog('open');
    
    var admin = this;
    var cb = function(o){
        if(!o.error){
            admin.displayLockMembers(admin, o);
        }
    }
    jsonCall({lockId: lockId}, "../../servlets/admin?method=getWorkflowLockMembers", cb);
};

/** Attempt to re-deliver a completed lock message */
WorkflowLockAdmin.prototype.attemptLockRedelivery = function(lockId){
    var admin = this;
    var cb = function(o){
        if(!o.error){
            $.jGrowl("Lock complete message re-sent");
        }
        admin.fetchLocks();
    }
    jsonCall({lockId: lockId}, "../../servlets/admin?method=attemptLockRedelivery", cb);
};

WorkflowLockAdmin.prototype.deleteLock = function(lockId){
    var admin = this;
    var cb = function(o){
        if(!o.error){
            $.jGrowl("Workflow lock deleted");
            $("#" + admin.divName).dialog('close');
            admin.fetchLocks();
        }
    }
    jsonCall({lockId: lockId}, "../../servlets/admin?method=deleteWorkflowLock", cb);
};
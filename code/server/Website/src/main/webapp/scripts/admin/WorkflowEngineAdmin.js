/* 
 * This script contains admin code for managing workflow engines
 */
function WorkflowEngineAdmin(){
    this.workflowEngines = new Array();
    this.selectedEngineId = null;
    this.divName = "";
    
    this.statusFunction = function(admin, o){
        var div = document.getElementById(admin.divName + "_status_dialog");
        if(div){
            var html='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="' + admin.divName + '_st_table"><thead><tr>';
            html+='<th>Property</th><th>Value</th>';
            html+='</tr></thead><tbody>';

            html+='<tr><td>Disk free space</td><td>' + o.engineStatus.diskFree + '</td></tr>';
            html+='<tr><td>Disk size</td><td>' + o.engineStatus.diskSize + '</td></tr>';
            html+='<tr><td>Disk used</td><td>' + o.engineStatus.diskUsed + '</td></tr>';
            html+='<tr><td>Disk percent</td><td>' + o.engineStatus.diskPercent + '%</td></tr>';
            html+='<tr><td>Engine start time</td><td>' + o.engineStatus.engineStartTime + '</td></tr>';
            html+='<tr><td>Workflow capacity</td><td>' + o.engineStatus.workflowCapacity + '</td></tr>';
            html+='<tr><td>Running workflow count</td><td>' + o.engineStatus.workflowCount + '</td></tr>';  
            html+='<tr><td>Total workflow count</td><td>' + o.engineStatus.workflowsStarted + '</td></tr>';
            html+='<tr><td>Failed workflow count</td><td>' + o.engineStatus.workflowsFailed + '</td></tr>';
            html+='<tr><td>Succeeded workflow count</td><td>' + o.engineStatus.workflowsSucceeded + '</td></tr>';
            html+='<tr><td>Current workflow slot usage</td><td>' + o.engineStatus.workflowPercent + '%</td></tr>';
            html+='</tbody';
            html+="</table>";     
            div.innerHTML = html;
        }
    };
    
    this.displayFunction = function(admin, o){
        var div = document.getElementById(admin.divName + "_workflows_dialog");
        if(!o.error){
            var invocations = o.engineStatus.invocations;
            if(invocations.length>0){
                var html='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="' + admin.divName + '_wf_table"><thead><tr>';
                html+='<th>Name</th><th>Start Time</th><th>Running</th><th>Action</th>';
                html+='</tr></thead><tbody>';

                var i;

                for(i=0;i<invocations.length;i++){

                    html+='<tr>';
                    html+='<td>' + invocations[i].name + '</td>';
                    html+='<td>' + invocations[i].startTime + '</td>';
                    if(invocations[i].running){
                        html+='<td>Running</td>';
                    } else {
                        html+='<td>Waiting</td>';
                    }
                    html+='<td><a id="' + admin.divName + '_kill_' + invocations[i].invocationId + '">Kill</a></td>';
                    html+='</tr>';
                }

                html+='</tbody';
                html+="</table>";       
                div.innerHTML = html;
                
                // Add the kill callbacks
                var killa;
                var cb = function(admin, o){
                    admin.fetchEngineStatus(admin.selectedEngineId, admin.displayFunction);
                };
                
                for(i=0;i<invocations.length;i++){
                    killa = document.getElementById(admin.divName + "_kill_" + invocations[i].invocationId);
                    
                    
                    if(killa){
                        
                        killa.invocationId = invocations[i].invocationId;
                        killa.onclick = function(){
                            admin.terminateInvocation(this.invocationId, cb);
                        };
                    }        
                }
                
                $("#" + admin.divName + "_wf_table").dataTable({"bJQueryUI": true});
            } else {
                div.innerHTML = "No Invocations";
            }
            
        } else {
            div.innerHTML = "Error";
        }
    };
    this.displayFunction.admin = this;
}

WorkflowEngineAdmin.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(divName);
    if(div){
        var tableHolder = document.createElement("div");
        tableHolder.setAttribute("id", this.divName + "_table_holder");
        div.appendChild(tableHolder);
        
        var workflowsDialog = document.createElement("div");
        workflowsDialog.setAttribute("id", this.divName + "_workflows_dialog");
        div.appendChild(workflowsDialog);
        var admin = this;
        $(workflowsDialog).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 400,
            width: 620,
            title: "Running workflows",
            modal: true,
            buttons: {
                'Refresh' : function(){
                    admin.fetchWorkflows(admin.selectedEngineId, admin.displayFunction);
                },
                'Ok': function() {
                    $(this).dialog('close');
                }
            }
        });
        
        var statusDialog = document.createElement("div");
        statusDialog.setAttribute("id", this.divName + "_status_dialog");
        div.appendChild(statusDialog);
        $(statusDialog).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 400,
            width: 500,
            title: "Engine Status",
            modal: true,
            buttons: {
                'Refresh' : function(){
                    admin.showEngineStatus(admin.selectedEngineId, admin.statusFunction);
                },                
                'Ok': function() {
                    $(this).dialog('close');
                }
            }          
        });
    }
};

WorkflowEngineAdmin.prototype.showEngineStatus = function(engineId){
    $("#" + this.divName + "_status_dialog").html("Fetching...");
    $("#" + this.divName + "_status_dialog").dialog('open');
    this.selectedEngineId = engineId;
    this.fetchEngineStatus(engineId, this.statusFunction);    
};

WorkflowEngineAdmin.prototype.fetchWorkflows = function(engineId){
    $("#" + this.divName + "_workflows_dialog").html("Fetching...");
    $("#" + this.divName + "_workflows_dialog").dialog('open');
    this.selectedEngineId = engineId;
    this.fetchEngineStatus(engineId, this.displayFunction);
};


/** Fetch the status of the current engine */
WorkflowEngineAdmin.prototype.fetchEngineStatus = function(engineId, callback){
    var url = "../../servlets/admin?method=fetchEngineStatus";
    var callData = {
        databaseId: engineId
    };
    var admin = this;
    var cb = function(o){
        if(!o.error && callback){
            callback(admin, o);
        }
    }
    jsonCall(callData, url, cb);
    
};

/** Get a list of workflow engines */
WorkflowEngineAdmin.prototype.fetchEngineList = function(callback){
    var admin = this;
    var cb = function(o){
        if(!o.error){
            admin.workflowEngines = o.engines;
            if(callback){
                callback(o);
            }
        }
    };
    var url = "../../servlets/admin?method=listWorkflowEngines";
    var callData = {};
    jsonCall(callData, url, cb);
};

/** Kill an invocation */
WorkflowEngineAdmin.prototype.terminateInvocation = function(invocationId, callback){
    var url = "../../servlets/admin?method=terminateInvocation";
    var data = {invocationId: invocationId};
    var admin = this;
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(admin, o);
            }
        }
    };
    jsonCall(data, url, cb);
};

/** Attach JMS */
WorkflowEngineAdmin.prototype.attachJms = function(engineId, callback){
    if(this.selectedEngine){
        var url = "../../servlets/admin?method=attachWorkflowEngineJMS";
        var callData = {
            databaseId: engineId
        };
        
        var cb = function(o){
            if(!o.error && callback){
                callback(o);
            }
        };
        jsonCall(callData, url, cb);        
    }
};

/** Re-register all workflow engines */
WorkflowEngineAdmin.prototype.reRegisterEngines = function(callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({}, "../../servlets/admin?method=reRegisterEngines", cb, null);
};

/** Exit all of the workflow engines with an exit code */
WorkflowEngineAdmin.prototype.exitAllEngines = function(exitCode, waitForWorkflows, callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({exitCode: exitCode, waitForWorkflows: waitForWorkflows}, "../../servlets/admin?method=exitAllEngines", cb, null);    
};

/** Kill all running workflows */
WorkflowEngineAdmin.prototype.killAllWorkflows = function(callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({}, "../../servlets/admin?method=killAllWorkflows", cb, null);
};

/** Flush the information caches on the workflow engines */
WorkflowEngineAdmin.prototype.flushEngineInformationCaches = function(callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({}, "../../servlets/admin?method=flushEngineInformationCaches", cb, null);
};

/** Reset the engines to use RMI */
WorkflowEngineAdmin.prototype.resetEngineRmiCommunications = function(callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({}, "../../servlets/admin?method=resetEngineRmiCommunications", cb, null);    
};

/** Rebuild the core library */
WorkflowEngineAdmin.prototype.rebuildCoreLibrary = function(callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({}, "../../servlets/admin?method=rebuildCoreLibrary", cb, null);      
};

/** Notify all finished lock holders */
WorkflowEngineAdmin.prototype.notifyAllFinishedLockHolders = function(callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o);
        }
    };
    jsonCall({}, "../../servlets/admin?method=notifyAllFinishedLockHolders", cb, null);       
};

/** Attach JMS */
WorkflowEngineAdmin.prototype.detachJms = function(engineId, callback){
    if(this.selectedEngine){
        var url = "../../servlets/admin?method=detachWorkflowEngineJMS";
        var callData = {
            databaseId: engineId
        };
        
        var cb = function(o){
            if(!o.error && callback){
                callback(o);
            }
        };
        jsonCall(callData, url, cb);        
    }
};

/** Display a table of the workflow engines into a div */
WorkflowEngineAdmin.prototype.displayTable = function(){
    var html='<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="' + this.divName + '_table"><thead><tr>';
    html+='<th>IP Address</th><th>Workflows</th><th>Status</th>';
    html+='</tr></thead><tbody>';
    
    var i;
    var engine;
    for(i=0;i<this.workflowEngines.length;i++){
        engine = this.workflowEngines[i];
        
        html+='<tr>';
        html+='<td>' + engine.hostIPAddress + '</td>';
        html+='<td><a id="' + this.divName + '_wf_' + engine.databaseId + '">List</a></td>';
        html+='<td><a id="' + this.divName + '_st_' + engine.databaseId + '">View</a></td>';
        html+='</tr>';
    }
    
    html+='</tbody';
    html+="</table>";
    var div = document.getElementById(this.divName + "_table_holder");
    if(div){
        div.innerHTML = html;
        
        // Add the callbacks
        var wfa;
        var sta;
        var admin = this;
        for(i=0;i<this.workflowEngines.length;i++){
            engine = this.workflowEngines[i];
            wfa = document.getElementById(this.divName + "_wf_" + engine.databaseId);
            if(wfa){
                wfa.databaseId = engine.databaseId;
                wfa.onclick = function(){
                    admin.fetchWorkflows(this.databaseId);
                };
            }
            
            sta = document.getElementById(this.divName + "_st_" + engine.databaseId);
            if(sta){
                sta.databaseId = engine.databaseId;
                sta.onclick = function(){
                    admin.showEngineStatus(this.databaseId);
                };
            }
        }
        $("#" + this.divName + "_table").dataTable({"bJQueryUI": true});
    }
};
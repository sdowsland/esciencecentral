/* 
 * This script provides a basic container for a file tree that can be used
 * without being put into a dialog. It is mainly used in the workflow
 * editor so that a browser can be popped up in the block properties
 * box.
 * "../../servlets/filechooser"
 */

//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
//<script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
//<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>

function FileTree(){
    this.divName = "";                      // Name of the div that will hold the dialog
    this.fileSelectCallback = null;         // Callback when a file is clicked
    this.folderSelectCallback = null;       // Callback when a folder is clicked
    this.fileDoubleClickCallback = null;    // Callback when a file is double clicked
    this.selectedFileId = null;             // ID of the last clicked file
    this.selectedFileName = null;
    this.selectedFolderId = null;           // ID of the last clicked folder
    this.selectedFolderName = null;
    this.selectedClassName = null;          // Classname of selected object
    this.fileList = new List();             // All of the file nodes in the tree
}

/** Setup with a div */
FileTree.prototype.init = function(divName){
    this.divName = divName;
}

FileTree.prototype.resetFileTree = function(){
    var html = '<ul id="_filetreediv_' + this.divName + '" class="filetree"></ul>';
    this.fileList.clear();
    var div = document.getElementById(this.divName);
    if(div!=null){
        div.innerHTML = html;
    }
};

FileTree.prototype.showHomeFolder = function(){
    this.resetFileTree();
    this.fileList.clear();
    $("#_filetreediv_" + this.divName).treeview({
            url: rewriteAjaxUrl("../../servlets/filechooser"),
            chooser: this
    });
};

FileTree.prototype.showFolder = function(folderId){
    this.resetFileTree();
    this.fileList.clear();
    $("#_filetreediv_" + this.divName).treeview({
            url: rewriteAjaxUrl("../../servlets/filechooser"),
            chooser: this,
            root: folderId
    });
};

FileTree.prototype.setSelectedFileId = function(fileId, fileName){
    this.selectedFileId = fileId;
    this.selectedFileName = fileName;
    if(this.fileSelectCallback){
        this.fileSelectCallback(fileId, fileName);
    }
};

FileTree.prototype.setSelectedFolderId = function(folderId, folderName){
    this.selectedFolderId = folderId;
    this.selectedFolderName = folderName;
    if(this.folderSelectCallback){
        this.folderSelectCallback(folderId, folderName);
    }
};

FileTree.prototype.doubleClickFileId = function(fileId, fileName){
    this.selectedFileId = fileId;
    this.selectedFileName = fileName;
    if(this.fileDoubleClickCallback){
        this.fileDoubleClickCallback(fileId, fileName);
    }
};

FileTree.prototype.addNodeToList = function(node){
    if(!this.fileList.contains(node)){
        this.fileList.add(node);
    }
};

FileTree.prototype.highlightFileDiv = function(div){
    var i;
    var d;
    for(i=0;i<this.fileList.size();i++){
        d = this.fileList.get(i);
        if(d===div){
            // Highlight this one
            //d.style.backgroundColor = "#D64B1F";
            $(d).addClass("selected");
            
        } else {
            // Unhighlight this div
            //d.style.backgroundColor = "#FFFFFF";
            $(d).removeClass("selected");
            d.fileSelected = false;
        }
    }
};
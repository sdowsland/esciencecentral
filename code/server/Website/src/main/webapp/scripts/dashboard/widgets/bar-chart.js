function buildBarChart(widget, container){
    
    var floatValue;
    
    if(widget.data && widget.data.length>0){
        var i;
        var row = widget.data[0];
        var names = Object.getOwnPropertyNames(row).sort();
        var name;
        var chartData = [];
        var chartSeries;
        
        var series;
        for(i=0;i<names.length;i++){
            name = names[i];
            if(!name.beginsWith("_")){
                try {
                    floatValue = parseFloat(row[name]);
                    chartSeries = {
                        name: name,
                        data: new Array()
                    };
                    
                    chartSeries.data.push({y: floatValue});
                    
                    chartData.push(chartSeries);
                } catch (err){
                    
                }
            }
        }

        var chart = new Highcharts.Chart({
            chart: {
                type: 'bar',
                backgroundColor: '#FAFAFA',
                height:200,
                spacingTop: 5,
                spacingBottom: 5,
                spacingLeft: 10,
                spacingRight: 10,
                renderTo: container
            },
            colors: ['#AAAAAA', '#3869A5'],
            title: {
                text: '',
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                categories: ['Storage'],
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                min: 0,
                max: 100,
                title: null,
                labels: {
                    enabled: true
                }
            },
            legend: {
                backgroundColor: null,
                borderWidth: 0,
                reversed: true,
                itemStyle: {
                    cursor: 'pointer',
                    color: '#AAAAAA',
                    fontSize: '12px'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size: 10px;">{point.y}% {series.name}</span>',
                pointFormat: ''
            },
            series: chartData
        });    
        
    }
}

String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
};

function buildTable(widget)
{
    var html = '<table class="table table-hover table-condensed widget-table">';

    html += "<thead><tr>";

    $.each(widget.data[0], function(key, value){

        html += "<th>" + key + "</th>";

    });

    html += "</tr></thead>";

    html += buildTableRow(widget);

    html += '</table>';

    return html;
}

function buildTableRow(widget)
{
    var html = "";

    $.each(widget.data, function(index, item)
    {
        console.log(item);

        html += "<tr>";

        $.each(item, function(key, value)
        {
            html += "<td>" + value + "</td>";
        });

        html += "</tr>";
    });

    return html;
}
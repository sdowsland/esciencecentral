defaultDashboard = new Array();

defaultDashboard.push(
    {
        "query": {
            "_datasetId": "core-datasets-userstats",
            "_itemName": "FileQuota",
            "_hidden": {
                "itemName": "FileQuota",
                "keys": [],
                "datasetId": "core-datasets-userstats"
            },
            "_className": "com.connexience.server.model.datasets.queries.system.SystemDatasetItemQuery",
            "_label": "Current Value"
        },
        "datasetId": "core-datasets-userstats",
        "type": "pie-chart"
    });
defaultDashboard.push(
    {
        "query": {
            "_datasetId": "core-datasets-userstats",
            "_itemName": "GeneralData",
            "_hidden": {
                "itemName": "GeneralData",
                "keys": [],
                "datasetId": "core-datasets-userstats"
            },
            "_className": "com.connexience.server.model.datasets.queries.system.SystemDatasetItemQuery",
            "_label": "Current Value"
        },
        "datasetId": "core-datasets-userstats",
        "type": "list"
    });
   
defaultDashboard.push({
        "query": {
            "_datasetId": "core-datasets-userstats",
            "_itemName": "WorkflowStatus",
            "_hidden": {
                "itemName": "WorkflowStatus",
                "keys": [],
                "datasetId": "core-datasets-userstats"
            },
            "_className": "com.connexience.server.model.datasets.queries.system.SystemDatasetItemQuery",
            "_label": "Current Value"
        },
        "datasetId": "core-datasets-userstats",
        "type": "pie-chart"
    });

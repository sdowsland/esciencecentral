/* 
 * This script provides a manager for controlling a view onto a dashboard. It contains
 * methods for retrieving and saving dashboard layouts
 */
function DashboardViewManager(){
    this.saveAsMainDashboard = true;
    this.dashboardId = null;
    this.userId = null;
    this.newDashboard = false;
}

/** Saves a dashboard using whatever method has been set */
DashboardViewManager.prototype.saveDashboard = function(dashboardJson){
    if(this.saveAsMainDashboard){
        this.saveUserMainDashboard(this.userId, dashboardJson);
    } else {
        this.saveGeneralDashboard(this.dashboardId, dashboardJson);
    }
};

/** Retrieve a dashboard using whatever method has been set */
DashboardViewManager.prototype.loadDashboard = function(callback){
    if(this.newDashboard){
        // Create a new dashboard
        if(!this.saveAsMainDashboard){
            this.createGeneralDashboard(callback);
        }
        
    } else {
        // Load an existing dashboard
        if(this.saveAsMainDashboard){
            this.getMainUserDashboard(this.userId, callback);
        } else {
            this.loadGeneralDashboard(this.dashboardId, callback);
        }
    }
};

/** Saves a dashboard layout as a property of the currently logged in user */
DashboardViewManager.prototype.saveUserMainDashboard = function(userId, dashboardJson){
    APIClient.prototype.setObjectProperty(userId, "query", "main", dashboardJson);
};

/** Retrieve the dashboard set as a property of the currently logged in user */
DashboardViewManager.prototype.getMainUserDashboard = function(userId, callback){
    var value = APIClient.prototype.getObjectProperty(userId, "query", "main");
    callback(value);
};

/** Load a dashboard object by ID */
DashboardViewManager.prototype.loadGeneralDashboard = function(dashboardId, callback){
    var cb = function(o){
        if(!o.error){
            callback(o.dashboard);
        }
    };
    jsonCall({id: dashboardId}, rewriteAjaxUrl("../../servlets/dataset?method=getDashboardLayout"), cb);
};

DashboardViewManager.prototype.saveGeneralDashboard = function(dashboardId, layout, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o);
            }
        }
    };
    jsonCall({id: dashboardId, dashboard: layout}, rewriteAjaxUrl("../../servlets/dataset?method=saveDashboardLayout"), cb);
};

DashboardViewManager.prototype.createGeneralDashboard = function(callback){
    var manager = this;
    var cb = function(o){
        if(!o.error){
            manager.newDashboard = false;
            manager.dashboardId = o.id;
            if(callback){
                callback(o.dashboard);
            }
        }
    };
    jsonCall({}, rewriteAjaxUrl("../../servlets/dataset?method=createNewDashboard"), cb);
};

DashboardViewManager.prototype.getDashboardDetails = function(id, callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o.name, o.description);
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/dataset?method=getDashboardDetails"), cb);
};

DashboardViewManager.prototype.setDashboardDetails = function(id, name, description, callback){
    jsonCall({id: id, name: name, description: description}, rewriteAjaxUrl("../../servlets/dataset?method=setDashboardDetails"), callback);
};


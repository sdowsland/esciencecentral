function buildList(widget)
{
    var html = '<ul class="unstyled widget-list">';

    html += buildListItem(widget);

    html += '</ul>';

    return html;
}

function buildListItem(widget)
{
    var html = "";

    $.each(widget.data, function(index, item)
    {
        

        $.each(item, function(key, value)
        {
            var iconClass="";
            
            if(widget.metadata && widget.metadata.descriptions && widget.metadata.descriptions[key]){
                var description = widget.metadata.descriptions[key];
                if(widget.metadata && widget.metadata.icons && widget.metadata.icons[key]){
                    iconClass = widget.metadata.icons[key];
                }
                html += "<li><i class='" + iconClass + "'></i>" + description.replace('$', value) + "</li>";                
            } else {
                html += "<li>" + key + ": " + value + "</li>";
            }
        });
    });

    return html;
}
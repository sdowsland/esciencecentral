function buildLineChart(widget, container) {
    if(widget.data){
        var rows = widget.data.length;
        var cols = 0;
        var names;
        var name;
        var row;
        var i,j;
        var timestamp;
        var count;
        
        if(rows>0){
            // Work out series details using first row of data
            row = widget.data[0];
            names = Object.getOwnPropertyNames(row).sort();
            
            var seriesData = new Array();
            var series;
            for(i=0;i<names.length;i++){
                name = names[i];
                if(!name.beginsWith("_")){
                    series = {
                        name: name,
                        data: new Array()
                    };
                    seriesData.push(series);                    
                }
            }
            
            // Now add all of the data
            for(i=0;i<rows;i++){
                row = widget.data[i];
                names = Object.getOwnPropertyNames(row).sort();
                count = 0;
                if(row["_timeInMillis"]){
                    timestamp = new Date(row["_timeInMillis"]);
                    for(j=0;j<names.length;j++){
                        if(!names[j].beginsWith("_")){
                            series = seriesData[count];
                            if(series.name===names[j]){
                                series.data.push([i, parseFloat(row[names[j]])]);
                            }
                            count++;
                        }
                    }
                }
            }
            
            var chart = new Highcharts.Chart({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    renderTo: container
                },
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                tooltip: {
                    headerFormat: '<span style="font-size: 10px;">{point.y} {point.key}</span>',
                    pointFormat: ''
                },
                xAxis: {
                    title: {
                        text: "Sample Number"
                    }
                },
                yAxis: {
                    title: {
                        text: 'Data Value'
                    }
                },
                series: seriesData
            });
            
        }
    }
}

String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
};
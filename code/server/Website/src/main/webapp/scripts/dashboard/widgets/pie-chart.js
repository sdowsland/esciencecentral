function buildPieChart(widget, container) {

    var chartData = [];
    var floatValue;
    
    $.each(widget.data, function(index, item){
        $.each(item, function(key, value)
        {
            if(!key.beginsWith("_")){
                if(typeof value==="number") {
                    chartData.push([key,value]);
                } else {
                    // Try and parse as a float anyway
                    try {
                        floatValue = parseFloat(value);
                        chartData.push([key, floatValue]);
                    } catch (err){

                    }
                }
            }
        });

    });

    //console.log(chartData);

    var chart = new Highcharts.Chart({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            renderTo: container
        },
        title: {
            text: '',
            style: {
                display: 'none'
            }
        },
        legend: {

        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size: 10px;">{point.y} {point.key}</span>',
            pointFormat: ''
        },
        series: [{
            type: 'pie',
            data: chartData
        }]
    });
}

String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
};
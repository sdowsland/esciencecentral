/* 
 * This object represents a basic block type that can be created using the
 * block builder
 */
function BlockType(label, description, helper, isBlock){
    this.description = description;
    this.label = label;
    this.helper = helper;
    this.isBlock = isBlock;
}


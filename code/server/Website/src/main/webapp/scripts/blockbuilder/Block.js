/* 
 * This script represents an instance of a block on the server. It contains
 * a link to the block source code, the block type and the properties of the
 * folder that contain it.
 */

function Block(){
    this.topFolder = null;
    this.srcFolder = null;
    this.buildFolder = null;
    this.packagedFile = null;
    this.helper = null;
    this.hasServiceXml = true;
    this.serviceXml = new ServiceXML(this);
    this.blockName = "NewBlock";
    this.projectFile = null;
    this.blockTypeLabel = "";
    this.isDeployable = true;
}

/** Create a new block in a folder. This creates the block property file, the
 * src and build folders and also the block helper object */
Block.prototype.createInFolder = function(folder, blockTypeLabel){
    this.topFolder = folder;
    this.blockTypeLabel = blockTypeLabel;
    this.saveProjectXml();
    this.checkFolders();
    this.initHelper();
    this.validateHelper();
    this.parseServiceXml();
};

/** Creates a block using values gathered from the block wizard */
Block.prototype.createFromWizard = function(wizardData){
    var folderId = wizardData.folderId;
    var folder = APIClient.prototype.getFolderById(folderId);
    if(folder){
        // Create the top folder
        var f = new Folder();
        f.name = wizardData.blockName;
        this.topFolder = APIClient.prototype.saveFolder(folder, f);
        
        this.blockName = wizardData.blockName;
        this.blockTypeLabel = wizardData.blockType;
        this.saveProjectXml();
        this.checkFolders();
        this.initHelper();
        this.validateHelper(wizardData);
        this.parseServiceXml();
    } else {
        throw "No target folder available";
    }
};

/** Initialise the block with a top level folder. This will check to see if
 * the correct block metadata is present as folder properties. If not, it
 * will create the data and save it back to the server */
Block.prototype.initWithProjectFile = function(projectFile){
    this.projectFile = projectFile;
    this.topFolder = APIClient.prototype.getFolderById(projectFile.containerId);
    if(this.topFolder){
        this.loadProjectXml();
        this.checkFolders();
        this.initHelper();
        this.validateHelper();
        this.parseServiceXml();
    } else {
        throw "Cannot access parent directory for: " + projectFile.name;
    }
};

/** Save the project xml file for the block */
Block.prototype.saveProjectXml = function(){
    if(this.topFolder){
        var xml = "<BlockProject>";
        xml+="  <BlockType>" + this.blockTypeLabel + "</BlockType>";
        xml+="  <BlockName>" + this.blockName + "</BlockName>";
        xml+="</BlockProject>"
        
        // Is there an existing project file
        if(this.projectFile){
            // Save updated project data to this file
            this.projectFile.name = this.blockName + ".blk"; 
            this.projectFile.contents = xml;
            this.projectFile = APIClient.prototype.setFileContents(this.topFolder, this.projectFile);
        } else {
            // Need to create a new project file
            this.projectFile = new DocumentRecord();
            this.projectFile.name = this.blockName + ".blk";  
            this.projectFile.id = "";
            this.projectFile.content = xml;
            this.projectFile = APIClient.prototype.setFileContents(this.topFolder, this.projectFile);
        }
    }
};

/** Load the project.xml file into the block */
Block.prototype.loadProjectXml = function(){
    if(this.projectFile){
        var loadedFile = APIClient.prototype.getFileContentsById(this.projectFile.id);
        if(loadedFile){
            var xmlDoc = loadXMLString(loadedFile.content, "text/xml");
            var name;
            var node = xmlDoc.childNodes[0];     
            
            for(var i=0;i<node.childNodes.length;i++){
                name = node.childNodes[i].nodeName.toLowerCase();

                if(name=="blocktype"){
                    // Type label
                    this.blockTypeLabel = node.childNodes[i].textContent;
                    
                } else if(name=="blockname"){
                    this.blockName = node.childNodes[i].textContent;
                    
                }
            }                    
        } else {
            throw "Cannot load project file";
        }
    } else {
        throw "No project file set in block";
    }
};

Block.prototype.forceParseServiceXml = function(){
    if(this.hasServiceXml){
        this.serviceXml.fetch();
        this.serviceXml.parse();
    }
};

Block.prototype.parseServiceXml = function(){
    if(this.hasServiceXml){
        if(this.serviceXml.fetchIfNeeded()){
            this.serviceXml.parse();
        } else {
            globalBuilder.log("Error retrieving service XML data");
        }
    }
};

Block.prototype.setBlockType = function(blockTypeLabel){
    this.properties.set("BlockType", blockTypeLabel);
    this.saveProperties();
};
 
/** Check the folder structure is OK in the top level folder */
Block.prototype.checkFolders = function(){
    if(this.topFolder){
        globalBuilder.log("Checking directory structure in folder: " + this.topFolder);
        this.srcFolder = globalBuilder.createFolderIfNotPresent(this.topFolder, "src");
        
        if(this.isDeployable){
            this.buildFolder = globalBuilder.createFolderIfNotPresent(this.topFolder, "build");
        }
        
        if(this.srcFolder==null || (this.isDeployable==true && this.buildFolder==null)){
            throw "Could not create directory structure for block";
        }
    } else {
        throw "No top folder present";
    }
};

/** Set up the helper if possible */
Block.prototype.initHelper = function(){
    if(this.topFolder && this.blockTypeLabel){
        var blockType = globalBuilder.findBlockType(this.blockTypeLabel);
        if(blockType){
            this.helper = blockType.helper.prototype.createInstance(this);
        }
    }
};
 
/** Setup the helper object */
Block.prototype.validateHelper = function(wizardData){
    if(wizardData){
        this.helper.validate(wizardData);
    } else {
        this.helper.validate();
    }
};

Block.prototype.toString = function(){
    if(this.topFolder){
        return "Block in folder " + this.topFolder;
    } else {
        return "Block without folder";
    }
};

Block.prototype.uploadBlock = function(){
    if(this.topFolder){
        // Upload the service.xml
        if(this.hasServiceXml){
            var xml = this.serviceXml.save();
        }
        
        
        // Let the helper do any preparation
        if(this.helper && this.helper.prepareZip){
            this.helper.prepareZip();
        }
        
        // Do the service upload
        var data = {
            folderId: this.buildFolder.id,
            srcFolderId: this.srcFolder.id,
            blockFileName: this.blockName,
            projectFileId: this.projectFile.id,
            hasServiceXml: this.hasServiceXml
        };
        
        var url = rewriteAjaxUrl('../../servlets/blockbuilder?method=buildService');

        var callString = JSON.stringify(data);
        var result = $.ajax({
            type: 'POST',
            url: url,
            dataType: "json",
            contentType: "application/json",
            success: function() { },
            data: callString,
            async: false
        });        
        
        var j = JSON.parse(result.responseText);
        var doc = new DocumentRecord();
        if(j.type=="error"){
            throw "Error saving service: " + j.message;
        } else {
            doc.parseJson(j);
            this.packagedFile = doc;
        }        
        return doc;
        
        
    }
};
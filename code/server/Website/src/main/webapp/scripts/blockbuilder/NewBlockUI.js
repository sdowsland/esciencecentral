/* 
 * This script creates a window that creates a new block by showing
 * a list of block types and a folder chooser to select the block 
 * location
 */
//Requires
// $.rloader({type: 'css', src: '../../scripts/blockbuilder/servicexmlui.css'});
//$.rloader({type: 'js', src: '../../scripts/jquery-selectbox/jquery.selectBox.js'});
//$.rloader({type: 'css', src: '../../scripts/jquery-selectbox/jquery.selectBox.css'});
function NewBlockUI(){
    this.divName = "";
    this.createWindow();
    this.selectedFolder = null;
    this.divId = "";
}

NewBlockUI.prototype.createWindow = function(){
    var dialog = this;
    var d = _globalShellManager.newWindow({title: "Create New Block", width: 500, height:200, buttons: {'Ok': function(){dialog.createBlock();$(this).dialog('close');}}});
    this.tabbedUI = d.tabbedUI;
    this.tabContainer = d.tabDiv;
    this.editorWindow = d[0];
    this.editorWindow.ownerObject = this;
    this.divId = this.editorWindow.id;    
    
    this.mainDiv = document.createElement("div");
    this.mainDiv.setAttribute("id", this.divId + "_main_div");
    this.divName = this.divId + "_main_div";
    this.editorWindow.appendChild(this.mainDiv);
    
    // Create the editor form
    var form = document.createElement("form");
    var typeSelect;
    var typeLabel;
    var typeDiv;
    var op;
    
    var nameField;
    var nameLabel;
    var nameDiv;    
    
    var folderDiv;
    var folderLabel;
    var folderField;
    var folderButton;
    
    typeDiv = document.createElement("div");
    typeDiv.setAttribute("class", "formInput");
    typeLabel = document.createElement("label");
    typeLabel.setAttribute("for", "blocktype");
    typeLabel.appendChild(document.createTextNode("Type:"));
    typeSelect = document.createElement("select");
    typeSelect.setAttribute("class", "streammodeSelect");
    typeSelect.setAttribute("name", "blocktype");
    typeSelect.setAttribute("id", this.divName + "_block_type");    

    for(var i=0;i<_blockBuilderPlugin.getBlockTypeCount();i++){
        op = document.createElement("option");
        op.setAttribute("value", i + 1);
        op.appendChild(document.createTextNode(_blockBuilderPlugin.getBlockType(i).label));    
        typeSelect.appendChild(op);        
    }
    
    typeDiv.appendChild(typeLabel);
    typeDiv.appendChild(typeSelect);
    form.appendChild(typeDiv);
    
    // Block name
    nameDiv = document.createElement("div");
    nameDiv.setAttribute("class", "formInput");
    nameLabel = document.createElement("label");
    nameLabel.setAttribute("for", "name");
    nameLabel.appendChild(document.createTextNode("Name:"));
    nameField = document.createElement("input");
    nameField.setAttribute("type", "text");
    nameField.setAttribute("size", "40");
    nameField.setAttribute("name", "name");
    nameField.setAttribute("id", this.divName + "_block_name");
    nameField.value = "NewBlock";
    nameDiv.appendChild(nameLabel);
    nameDiv.appendChild(nameField);    
    form.appendChild(nameDiv);
    
    // Storage folder
    folderDiv = document.createElement("div");
    folderDiv.setAttribute("class", "formInput");
    folderLabel = document.createElement("label");
    folderLabel.setAttribute("for", "folder");
    folderLabel.appendChild(document.createTextNode("Storage folder:"));
    folderField = document.createElement("input");
    folderField.setAttribute("type", "text");
    folderField.setAttribute("size", 32);
    folderField.setAttribute("name", "folder");
    folderField.setAttribute("id", this.divName + "_block_folder");
    folderField.value = "";
    folderButton = document.createElement("input");
    folderButton.setAttribute("type", "button");
    folderDiv.appendChild(folderLabel);
    folderDiv.appendChild(folderField);
    folderDiv.appendChild(folderButton);
    form.appendChild(folderDiv);
    $(folderButton).button({
            text: true,
            label: "..."
    });
        
    folderButton.onclick = function(){
        dialog.selectFolder();
    };
    
    this.mainDiv.appendChild(form);
    $('#' + this.divName + "_block_type").selectBox(); 
};

// Make the new block
NewBlockUI.prototype.createBlock = function(){
    var nameField = document.getElementById(this.divName + "_block_name");
    var typeSelect = document.getElementById(this.divName + "_block_type");
    var blockType = _blockBuilderPlugin.getBlockType(typeSelect.value - 1);
    
    if(blockType){
        var block = new Block();
        if(this.selectedFolder==undefined || this.selectedFolder==null){
            // Use the home folder
            this.selectedFolder = APIClient.prototype.getHomeFolder();
        }
        block.blockName = nameField.value.trim();
        block.createInFolder(this.selectedFolder, blockType.label);
        return new SimpleBlockUI(block);        
        $("#" + this.divId).dialog("close");
    }
};

// Select the target folder for the block
NewBlockUI.prototype.selectFolder = function(){
    var dialog = this;
    var cb = function(chooser){
        // Get the actual folder
        if(chooser.selectedFolderId){
            var folder = APIClient.prototype.getFolderById(chooser.selectedFolderId);
            if(folder){
                var folderField = document.getElementById(dialog.divName + "_block_folder");
                folderField.value = folder.name;
                dialog.selectedFolder = folder;
            }
        } else {
            throw "No folder specified";
        }
    }
    shellShowFolderChooser(cb);
};
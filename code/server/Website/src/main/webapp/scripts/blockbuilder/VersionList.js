/* 
 * This script provides a version list for the block builder
 */
function VersionList(){
    this.divName = "";
    this.doc = null;
    this.documentId = "";
    this.versions = new Array();
    this.builder = null;
    this.timeoutVar = undefined;
    this.enabled = true;
    this.selectedVersionId = null;
}

VersionList.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    div.setAttribute("style", "overflow: hidden;");
    var toolbar = document.createElement("div");
    toolbar.setAttribute("id", this.divName + "_inner_toolbar");
    toolbar.setAttribute("class", "navbar clearfix");

    var toolbarContainer = document.createElement("div");
    toolbarContainer.setAttribute("class", "navbar-inner");

    var toolbarMenu = document.createElement("ul");
    toolbarMenu.setAttribute("class", "nav clearfix");
    
    /*
    var html = '<button id="' + this.divName + 'refresh">Refresh versions</button>';
    html+='<button id="' + this.divName + 'latest">Use latest version</button>';
    toolbar.innerHTML = html;
    */
   
    // Refresh button
    var refresh = document.createElement("li");
    var refreshButton = document.createElement("a");
    refreshButton.setAttribute("id", this.divName + "refresh");
    refreshButton.innerHTML = "<i class='icomoon-loop-2'></i>";
    refresh.appendChild(refreshButton);
    toolbarMenu.appendChild(refresh);

    // Latest button
    var latest = document.createElement("li");
    var latestButton = document.createElement("a");
    latestButton.setAttribute("id", this.divName + "latest");
    latestButton.innerHTML = "<i class='icomoon-star-3'></i>Use Latest";
    latest.appendChild(latestButton);
    toolbarMenu.appendChild(latest);

    
    toolbar.appendChild(toolbarContainer);
    toolbarContainer.appendChild(toolbarMenu);
    
    var mainDiv = document.createElement("div");
    mainDiv.setAttribute("id", this.divName + "list");
    mainDiv.setAttribute("style", "overflow: auto; padding-top: 5px;");
    
    div.appendChild(toolbar);
    div.appendChild(mainDiv);
   
    var list = this;
    document.getElementById(this.divName + "refresh").onclick = function(){
        list.refreshVersions();
    }
    
    document.getElementById(this.divName + "latest").onclick = function(){
        if(list.versions.length>0){
            list.selectedVersionId = list.versions[0].id;
            list.builder.switchDocumentVersion(list.versions[0]);
            list.displayVersions();
        }
    }    
};

VersionList.prototype.resize = function(){
    var div = document.getElementById(this.divName);
    var h = div.clientHeight;
    var mainDiv = document.getElementById(this.divName + "list");
    mainDiv.style.height = (h - 37) + "px";
};

VersionList.prototype.enable = function(){
    this.enabled = true;
    this.clearDisplay();
    this.triggerFetch();
};

VersionList.prototype.disable = function(){
    this.enabled = false;
    this.clearDisplay();
};

VersionList.prototype.setDocument = function(doc){
    this.doc = doc;
    this.documentId = doc.id;
    if(this.doc.version){
        this.selectedVersionId = doc.version.id;
    } else {
        this.selectedVersionId = null;
    }
    if(this.enabled){
        this.triggerFetch();
    } else {
        this.clearDisplay();
    }
};

VersionList.prototype.refreshVersions = function(){
    if(this.doc && this.enabled){
        this.triggerFetch(1);
    }
};

VersionList.prototype.triggerFetch = function(timeout){
    var div = document.getElementById(this.divName + "list");
    div.innerHTML = "Fetching...";
    var list = this;
    var fetcher = function(){
        list.fetchVersions();
    };
    
    if(this.timeoutVar!=undefined){
        clearTimeout(this.timeoutVar);
        this.timeoutVar = undefined;
    }
    
    if(timeout){
        this.timeoutVar = setTimeout(fetcher, timeout);    
    } else {
        this.timeoutVar = setTimeout(fetcher, 100);    
    }
};

VersionList.prototype.clearDisplay = function(){
    var div = document.getElementById(this.divName + "list");
    div.innerHTML = "";    
};

VersionList.prototype.clearDocument = function(){
    if(this.timeoutVar!=undefined){
        clearTimeout(this.timeoutVar);
        this.timeoutVar = undefined;
    }
    this.doc = null;
    this.documentId = null;
    this.selectedVersionId = null;
    this.clearDisplay();
};

VersionList.prototype.fetchVersions = function(){
    var div = document.getElementById(this.divName + "list");
    div.innerHTML = "Fetching...";    
    this.versions = APIClient.prototype.getFileVersions(this.documentId);
    this.resize();
    this.displayVersions();
};

VersionList.prototype.selectVersion = function(version){
    if(version.documentId==this.doc.id){
        this.selectedVersionId = version.id;
        this.displayVersions();
        this.builder.switchDocumentVersion(version);
    }
};

VersionList.prototype.displayVersions = function(){
    var div = document.getElementById(this.divName + "list");
    div.innerHTML = "";

    var numberDiv;
    var dateDiv;
    var versionDiv;
    
    for(var i=0;i<this.versions.length;i++){
        versionDiv = document.createElement("div");
        versionDiv.setAttribute("class", "versionElement");
        
        numberDiv = document.createElement("div");
        numberDiv.setAttribute("class", "versionNumber");
        numberDiv.appendChild(document.createTextNode(this.versions[i].number));
        
        dateDiv = document.createElement("div");
        dateDiv.setAttribute("class", "versionDate");
        dateDiv.appendChild(document.createTextNode(this.versions[i].timestamp));
        
        if(this.selectedVersionId && this.versions[i].id==this.selectedVersionId){
            $(dateDiv).addClass("selected");
            $(numberDiv).addClass("selected");
            $(versionDiv).addClass("selected");
        } else {
            $(dateDiv).removeClass("selected");
            $(numberDiv).removeClass("selected");
            $(versionDiv).removeClass("selected");
            
        }
        dateDiv.version = this.versions[i];
        dateDiv.dialog = this;
        dateDiv.onclick = function(){
            this.dialog.selectVersion(this.version);
        }
        versionDiv.appendChild(numberDiv);
        versionDiv.appendChild(dateDiv);
        
        div.appendChild(versionDiv);
    }
    
};
/* 
 * This object represents a block input or output port
 */
function BlockIO(){
    this.name = "input";
    this.type = "data-wrapper";
    this.streaming = false;
    this.isInput = true;
}

BlockIO.prototype.createXmlTag = function(){
    if(this.isInput){
        if(this.streaming){
            return '<Input name="' + this.name + '" type="' + this.type + '" streaming="true"/>';
        } else {
            return '<Input name="' + this.name + '" type="' + this.type + '" streaming="false"/>';
        }
    } else {
        return '<Output name="' + this.name + '" type="' + this.type + '"/>';
    }
};
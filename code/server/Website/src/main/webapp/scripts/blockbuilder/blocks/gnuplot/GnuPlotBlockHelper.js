/* 
 * This script provides a helper that can create gnuplot workflow blocks
 */
function GnuPlotBlockHelper(){
    this.block = null;
    this.supportScriptFolder = null;
    this.blockTypeLabel = "Gnuplot"; 
    this.isBlock = true;
    this.isDeployable = true;
}

GnuPlotBlockHelper.prototype.createInstance = function(block){
    var helper = new GnuPlotBlockHelper();
    if(block){
        helper.block = block;
        block.helper = helper;
        block.hasServiceXml = helper.isBlock;
    }
    return helper;
};

GnuPlotBlockHelper.prototype.validate = function(wizardData){
    // Build a list of parameter replacements
    var replacements;
    if(wizardData && wizardData.replacements){
        replacements = wizardData.replacements;
    } else {
        replacements = {
            names: ["name", "description","category"],
            values: ["NewBlock","Gnuplot Block","My Services"]
        }
    }
    
    // Standard block files
    var contents = APIClient.prototype.getFolderContents(this.block.srcFolder);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "service.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/gnuplot/templates/service.xml"), contents, replacements);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "library.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/gnuplot/templates/library.xml"), contents);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "dependencies.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/gnuplot/templates/dependencies.xml"), contents);
    globalBuilder.copyBlockIcon(this.block.srcFolder, "gnuplot.jpg");
    
    // Use the wizard data code if present
    if(wizardData && wizardData.blockCode){
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "initplot.txt", wizardData.blockCode, contents);
    } else {
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "initplot.txt", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/gnuplot/templates/initplot.txt"), contents);
    }    
    
};


GnuPlotBlockHelper.prototype.prepareZip = function(){
    
};

GnuPlotBlockHelper.prototype.getBlockMainCode = function(){
    if(this.block.srcFolder){
        var doc = APIClient.prototype.getFileContents(this.block.srcFolder, "initplot.txt");
        return doc;
    } else {
        throw "No source folder present";
    }
};

GnuPlotBlockHelper.prototype.getBlockMainCodeTemplateAsDocument = function(){
    var code = APIClient.prototype.getUrlContentAsString(rewriteAjaxUrl("../../scripts/blockbuilder/blocks/gnuplot/templates/initplot.txt"));
    var doc = new DocumentRecord();
    doc.name = "initplot.txt";
    doc.content = code;
    return doc;
};

globalBuilder.registerBlockType(new BlockType("Gnuplot", "Gnuplot is a portable command-line driven graphing utility for linux, OS/2, MS Windows, OSX, VMS, and many other platforms. The source code is copyrighted but freely distributed (i.e., you don't have to pay for it). It was originally created to allow scientists and students to visualize mathematical functions and data interactively, but has grown to support many non-interactive uses such as web scripting. It is also used as a plotting engine by third-party applications like Octave. Gnuplot has been supported and under active development since 1986.", GnuPlotBlockHelper, true));


/* 
 * This script provides paging data for lists that can be used in a reasonably
 * generic way
 */
function PagingData(){
    this.objectClassName = "";
    this.objectCount = 0;
    this.pageSize = 10;
    this.startIndex = 0;
}

PagingData.prototype.updateSize = function(){
    var pagingJson = jsonGet("../../servlets/paging?className=" + this.objectClassName);
};

PagingData.prototype.toJson = function(){
    return {
        startIndex: this.startIndex,
        pageSize: this.pageSize
    };
};
// This Javascript object defines an input / output port contained
// on a DrawingNode.
function DrawingPort(portName, portLocation, portOffset, parentBlock) {
    this.location = portLocation;
    this.offset = portOffset;
    this.name = portName;
    this.optional = false;
    this.type = "input";
    this.block = parentBlock;
    this.connections = new List();
    this.streamable = false;
    this.supportedTypes = new List();
}

$(document).ready(function () {
    DrawingPort.prototype.fileImage = new Image();
    DrawingPort.prototype.fileImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/file-wrapper.gif");

    DrawingPort.prototype.dataImage = new Image();
    DrawingPort.prototype.dataImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/data-wrapper.gif");

    DrawingPort.prototype.linkImage = new Image();
    DrawingPort.prototype.linkImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/link-wrapper.gif");

    DrawingPort.prototype.objectImage = new Image();
    DrawingPort.prototype.objectImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/object-wrapper.gif");

    DrawingPort.prototype.propertiesImage = new Image();
    DrawingPort.prototype.propertiesImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/properties-wrapper.gif");

    DrawingPort.prototype.libraryImage = new Image();
    DrawingPort.prototype.libraryImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/library-wrapper.gif");

    DrawingPort.prototype.controlDepImage = new Image();
    DrawingPort.prototype.controlDepImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/control-dependency.gif");

    DrawingPort.prototype.unknownImage = new Image();
    DrawingPort.prototype.unknownImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/warning.gif");
});

/** Add a supported type */
DrawingPort.prototype.addSupportedType = function (typeName) {
    this.supportedTypes.add(typeName);
};

/** Can another port be connected to this one */
DrawingPort.prototype.canConnectToPort = function (port) {
    var i;
    var typeName;

    for (i = 0; i < port.supportedTypes.size(); i++) {
        typeName = port.supportedTypes.get(i);
        if (this.supportedTypes.contains(typeName)) {
            return true;
        }
    }
    return false;
};

/** Get the first supported data type */
DrawingPort.prototype.getDefaultType = function () {
    return this.supportedTypes.get(0);
};

/** Get the number of connections present in this port */
DrawingPort.prototype.getConnectionCount = function () {
    return this.connections.size();
};

/** Get a specific connection */
DrawingPort.prototype.getConnection = function (index) {
    return this.connections.get(index);
};

/** Add a connection */
DrawingPort.prototype.addConnection = function (connection) {
    this.connections.add(connection);
};

/** Remove a connection */
DrawingPort.prototype.removeConnection = function (connection) {
    var index = this.connections.indexOf(connection);
    if (index !== -1) {
        this.connections.remove(index);
    }
};

/** Can this port accept more connections */
DrawingPort.prototype.canAcceptConnections = function () {
    if (this.type === "output") {
        return true;
    } else {
        if (this.connections.size() === 0) {
            return true;
        } else {
            return false;
        }
    }
};

/** Make this into an input port */
DrawingPort.prototype.makeInputPort = function () {
    this.type = "input";
};

/** Make this into an output port */
DrawingPort.prototype.makeOutputPort = function () {
    this.type = "output";
};

/** Render this port onto a drawing context at a specified location */
DrawingPort.prototype.renderPort = function (ctx, p) {
    var sameBlock = false;

    var inpSize;
    var outSize;
    var forceDataTypeDisplay = false;
    var imageOffset = 0;

    if (this.block.parentDrawing.linkStartPort !== null) {
        if (this.block.parentDrawing.linkStartPort.block === this.block) {
            sameBlock = true;
        }
    }


    if (this.type === "input") {

        if (this.canAcceptConnections() && sameBlock === false && this.supportedTypes.contains(this.block.parentDrawing.highlightPortDataType) && this.block.parentDrawing.highlightPortType === "input") {
            ctx.fillStyle = INPUT_PORT_HIGHLIGHT_COLOR;
            inpSize = INPUT_PORT_HIGHLIGHT_SIZE;
            imageOffset = 2;
            forceDataTypeDisplay = true;
        } else {
            ctx.fillStyle = INPUT_PORT_COLOR;
            inpSize = INPUT_PORT_SIZE;
            forceDataTypeDisplay = false;
        }

        if (this.location === LEFT_OF_BLOCK) {
            ctx.beginPath();
            ctx.moveTo(p.x - inpSize, p.y - inpSize);
            ctx.lineTo(p.x, p.y);
            ctx.lineTo(p.x - inpSize, p.y + inpSize);
            ctx.lineTo(p.x - inpSize, p.y - inpSize);
            ctx.closePath();
            if(!this.optional){ctx.fill();} else {ctx.stroke();}
            if (this.streamable) {
                ctx.beginPath();
                ctx.moveTo(p.x, p.y - inpSize);
                ctx.lineTo(p.x + inpSize, p.y);
                ctx.lineTo(p.x, p.y + inpSize);
                ctx.lineTo(p.x, p.y - inpSize);
                ctx.closePath();
                if(!this.optional){ctx.fill();} else {ctx.stroke();}
            }

            if (forceDataTypeDisplay || this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x - inpSize - 15 - imageOffset, p.y - 6);
                ctx.globalAlpha = 1.0;
            }

        } else if (this.location === RIGHT_OF_BLOCK) {
            ctx.beginPath();
            ctx.moveTo(p.x, p.y - inpSize);
            ctx.lineTo(p.x + inpSize, p.y);
            ctx.lineTo(p.x, p.y + inpSize);
            ctx.lineTo(p.x, p.y - inpSize);
            ctx.closePath();
            if(!this.optional){ctx.fill();} else {ctx.stroke();}
            if (this.streamable) {
                ctx.beginPath();
                ctx.moveTo(p.x - inpSize, p.y - inpSize);
                ctx.lineTo(p.x, p.y);
                ctx.lineTo(p.x - inpSize, p.y + inpSize);
                ctx.lineTo(p.x - inpSize, p.y - inpSize);
                ctx.closePath();
                if(!this.optional){ctx.fill();} else {ctx.stroke();}
            }

            if (forceDataTypeDisplay || this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x + inpSize + 5 + imageOffset, p.y - 6);
                ctx.globalAlpha = 1.0;
            }


        } else if (this.location === TOP_OF_BLOCK) {
            ctx.beginPath();
            ctx.moveTo(p.x, p.y);
            ctx.lineTo(p.x - inpSize, p.y - inpSize);
            ctx.lineTo(p.x + inpSize, p.y - inpSize);
            ctx.lineTo(p.x, p.y);
            ctx.closePath();
            if(!this.optional){ctx.fill();} else {ctx.stroke();}
            if (this.streamable) {
                ctx.beginPath();
                ctx.moveTo(p.x, p.y);
                ctx.lineTo(p.x - inpSize, p.y + inpSize);
                ctx.lineTo(p.x + inpSize, p.y + inpSize);
                ctx.lineTo(p.x, p.y);
                ctx.closePath();
                if(!this.optional){ctx.fill();} else {ctx.stroke();}
            }

            if (forceDataTypeDisplay || this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x, p.y - inpSize - 15 - imageOffset);
                ctx.globalAlpha = 1.0;
            }

        } else {
            ctx.beginPath();
            ctx.moveTo(p.x - inpSize, p.y);
            ctx.lineTo(p.x, p.y + inpSize);
            ctx.lineTo(p.x + inpSize, p.y);
            ctx.lineTo(p.x - inpSize, p.y);
            ctx.closePath();
            if(!this.optional){ctx.fill();} else {ctx.stroke();}
            if (this.streamable) {
                ctx.beginPath();
                ctx.moveTo(p.x - inpSize, p.y - inpSize);
                ctx.lineTo(p.x, p.y);
                ctx.lineTo(p.x + inpSize, p.y - inpSize);
                ctx.lineTo(p.x - inpSize, p.y - inpSize);
                ctx.closePath();
                if(!this.optional){ctx.fill();} else {ctx.stroke();}
            }

            if (this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x, p.y + inpSize + 4 + imageOffset);
                ctx.globalAlpha = 1.0;
            }


        }


    } else {
        if (sameBlock === false && this.supportedTypes.contains(this.block.parentDrawing.highlightPortDataType) && this.block.parentDrawing.highlightPortType === "output") {
            ctx.fillStyle = OUTPUT_PORT_HIGHLIGHT_COLOR;
            outSize = OUTPUT_PORT_HIGHLIGHT_SIZE;
            imageOffset = 4;
            forceDataTypeDisplay = true;
        } else {
            ctx.fillStyle = OUTPUT_PORT_COLOR;
            outSize = OUTPUT_PORT_SIZE;
            forceDataTypeDisplay = false;
        }

        if (this.location === LEFT_OF_BLOCK) {
            ctx.beginPath();
            ctx.arc(p.x - outSize, p.y, outSize, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();

            if (forceDataTypeDisplay || this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x - outSize - 15 - imageOffset, p.y - 6);
                ctx.globalAlpha = 1.0;
            }

        } else if (this.location === RIGHT_OF_BLOCK) {
            ctx.beginPath();
            ctx.arc(p.x + outSize, p.y, outSize, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();

            if (forceDataTypeDisplay || this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x + outSize + 5 + imageOffset, p.y - 6);
                ctx.globalAlpha = 1.0;
            }

        } else if (forceDataTypeDisplay || this.location === BOTTOM_OF_BLOCK) {
            ctx.beginPath();
            ctx.arc(p.x, p.y + outSize, outSize, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();

            if (this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x, p.y + outSize + 4 + imageOffset);
                ctx.globalAlpha = 1.0;
            }

        } else {
            ctx.beginPath();
            ctx.arc(p.x, p.y - outSize, outSize, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();

            if (forceDataTypeDisplay || this.block.parentDrawing.isObjectSelected(this.block)) {
                ctx.globalAlpha = DRAWING_PORT_ICON_ALPHA;
                ctx.drawImage(this.getImageForDataType(), p.x, p.y - outSize - 15 - imageOffset);
                ctx.globalAlpha = 1.0;
            }

        }


    }
};

DrawingPort.prototype.getImageForDataType = function () {
    var dt = this.getDefaultType();
    if (dt==="file-wrapper") {
        return DrawingPort.prototype.fileImage;

    } else if (dt==="link-wrapper") {
        return DrawingPort.prototype.linkImage;

    } else if (dt==="data-wrapper") {
        return DrawingPort.prototype.dataImage;

    } else if (dt==="properties-wrapper") {
        return DrawingPort.prototype.propertiesImage;

    } else if (dt==="object-wrapper") {
        return DrawingPort.prototype.objectImage;

    } else if (dt==="library-wrapper") {
        return DrawingPort.prototype.libraryImage;

    } else if (dt==="control-dependency") {
        return DrawingPort.prototype.controlDepImage;

    } else {
        return DrawingPort.prototype.unknownImage;

    }
};
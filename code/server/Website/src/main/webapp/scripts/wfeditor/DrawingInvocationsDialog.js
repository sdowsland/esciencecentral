/*
 * This code provides a dialog box that can be used to browse through invocations
 * of a workflow.
 */
function DrawingInvocationsDialog(){
    this.drawing = new DrawingObject();
    this.messageDialog = null;
    this.executionManager = new DrawingExecutionManager();
    this.locksDialog = null;
    this.drawingEditor = new DrawingEditor();
    this.fetcher = new DrawingFetcher();
    this.selectedInvocation = null;
    this.invocationFiles = new List();
    this.dialogDivName = "invocationsDialog";
    this.documentId = "";
    this.invocationId = "";
    this.toolbarDivName = "";
    this.autoNavigateId = null;
    this.createdDateSpan = null;
    this.queuedDateSpan = null;
    this.dequeuedDateSpan = null;
    this.startDateSpan = null;
    this.endDateSpan = null;
    this.statusSpan = null;
    this.viewer = null;
    this.debugWindowDivName = "";
    this.debugWindow = null;
    this.outputViewerWindow = null;
    this.terminationReportDivName = "";
    this.terminationReportWindow = null;
    this.timeoutVar = null;
}

DrawingInvocationsDialog.prototype.showBlockOutput = function(block){
    if(this.setSelectedInvocation && block){
        this.outputViewerWindow.showWithOutput(this.selectedInvocation.invocationId, block.guid);
    }
};

DrawingInvocationsDialog.prototype.showBlockDebugger = function(block){
    if(this.selectedInvocation){
        this.debugWindow.showDialogWithDebugging(block, this.selectedInvocation.invocationId);
    }
};

DrawingInvocationsDialog.prototype.showBlockLocks = function(block){
    if(this.setSelectedInvocation){
        this.locksDialog.viewLockForBlock(this.selectedInvocation.invocationId, block.guid);
    }
};

DrawingInvocationsDialog.prototype.init = function(dialogDivName){
    this.dialogDivName = dialogDivName;
    var dialogDiv = document.getElementById(this.dialogDivName);
    dialogDiv.innerHTML = "";

    if(dialogDiv!=null){
        // Create a toolbar
        this.createToolbar();
        
        var drawingDiv = document.createElement("div");
        drawingDiv.setAttribute("id", this.dialogDivName + "_drawing");
        drawingDiv.setAttribute("style", "z-index:0; border:1px solid gray; float:right; margin-top: 10px; width: 560px; height: 315px; overflow:auto;");
        drawingDiv.setAttribute("class", "ui-corner-all");

        dialogDiv.appendChild(drawingDiv);
        var dialog = this;

        $("#" + this.dialogDivName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 610,
            width: 810,
            title: "Workflow Invocations",
            modal: true,
            buttons: {
                'Close': function()
                {
                    $(this).dialog('close');
                }

            },
            close: function()
            {

            }, 
            open: function(){
                dialog.resizeUI();
            }
        });
        
        $('#' + this.dialogDivName).bind("dialogresize", function(){
            dialog.resizeUI();
        });
            
        // Create a quickview
        if(this.viewer==null){
            this.viewer = new QuickView();
            var quickViewDiv = document.createElement("div");
            quickViewDiv.setAttribute("id", this.dialogDivName + "_viewer");
            dialogDiv.appendChild(quickViewDiv);            
            this.viewer.init(this.dialogDivName + "_viewer");
        }
        
        // Create a debug window
        if(this.debugWindow==null){
            this.debugWindow = new DrawingBlockDebugger();
            var debugWindowDiv = document.createElement("div");
            debugWindowDiv.setAttribute("class", "dialog");
            debugWindowDiv.setAttribute("id", this.dialogDivName + "_debugger");
            dialogDiv.appendChild(debugWindowDiv);
            this.debugWindow.init(this.dialogDivName + "_debugger");
        }
        
        // Create a block output viewer window
        if(this.outputViewerWindow==null){
            this.outputViewerWindow = new DrawingBlockOutputViewer();
            var outputWindowDiv = document.createElement("div");
            outputWindowDiv.setAttribute("class", "dialog");
            outputWindowDiv.setAttribute("id", this.dialogDivName + "_output");
            dialogDiv.appendChild(outputWindowDiv);
            this.outputViewerWindow.init(this.dialogDivName + "_output");
        }
        
        // Create a termination report window
        if(this.terminationReportWindow==null){
            this.terminationReportWindow = new DrawingTerminationReportDialog();
            var terminationReportDiv = document.createElement("div");
            terminationReportDiv.setAttribute("class", "dialog");
            terminationReportDiv.setAttribute("id", this.dialogDivName + "_terminationReport");
            dialogDiv.appendChild(terminationReportDiv);
            this.terminationReportWindow.init(this.dialogDivName + "_terminationReport");
        }
        
        // Create a confirm window
        if(this.messageDialog==null){
            this.messageDialog = new MessageDialog();
            var confirmDialogDiv = document.createElement("div");
            confirmDialogDiv.setAttribute("class", "dialog");
            confirmDialogDiv.setAttribute("id", this.dialogDivName + "_message");
            dialogDiv.appendChild(confirmDialogDiv);
            this.messageDialog.init(this.dialogDivName + "_message");
        }
        
        // Create a lock viewer window
        if(this.locksDialog==null){
            this.locksDialog = new DrawingLockManager();
            var lockDialogDiv = document.createElement("div");
            lockDialogDiv.setAttribute("class", "dialog");
            lockDialogDiv.setAttribute("id", this.dialogDivName + "_locks");
            dialogDiv.appendChild(lockDialogDiv);
            this.locksDialog.init(this.dialogDivName + "_locks");
        }
        
        // Create the drawing canvas
        this.canvasDiv = document.createElement("canvas");
        this.canvasDiv.invocationsDialog = this;
        this.canvasDiv.setAttribute("id", this.dialogDivName + "_canvas");
        this.canvasDiv.setAttribute("width", "560px");
        this.canvasDiv.setAttribute("height", "315px");
        this.drawing.minimumWidth = 560;
        this.drawing.minimumHeight = 315;
        drawingDiv.appendChild(this.canvasDiv);
        
        var canvas = this.initCanvas(this.canvasDiv);
        var isIE = this.isIE();
        this.drawing.isIE = isIE;
        this.drawing.canvas = canvas;
        this.drawing.showName = false;
        this.drawingEditor.drawing = this.drawing;
        this.drawingEditor.wfeditor = this;
        this.drawingEditor.canvas = canvas;
        this.drawing.editor = this.drawingEditor;
        this.drawingEditor.setupMouse();
        

        
        // Create the invocations list
        var invocationsDiv = document.createElement("div");
        invocationsDiv.setAttribute("id", this.dialogDivName + "_invocations");
        invocationsDiv.setAttribute("class", "ui-corner-all");
        invocationsDiv.setAttribute("style", "z-index:0; border:1px solid gray; margin-top: 10px; width: 190px; height: 310px; overflow:auto;");
        dialogDiv.appendChild(invocationsDiv);

        this.fetcher.fetchComplete = function(){
            
        };
        this.fetcher.editor = this.drawingEditor;
        this.fetcher.drawing = this.drawing;


        // Create the file list
        this.createInvocationFilesBox();
        
        // Create invocation details
        this.createInvocationDetailsBox();


        // Setup the execution manager
        var invocationsFetched = function(){
            if(this.invocations.size()>0){
                if(this.wfeditor.autoNavigateId!=null){
                    var invocation = this.getInvocationById(this.wfeditor.autoNavigateId);
                    this.wfeditor.autoNavigateId = null;
                    if(invocation!=null){
                        this.wfeditor.selectedInvocation = invocation;
                    } else {
                        this.wfeditor.selectedInvocation = this.invocations.get(0);
                    }                    
                } else {
                    if(this.wfeditor.selectedInvocation==null){
                        this.wfeditor.selectedInvocation = this.invocations.get(0);
                    }
                }
                this.wfeditor.drawing.displayLabel = null;
                this.wfeditor.displayInvocations();
                this.wfeditor.hightlightSelectedInvocation();
                this.wfeditor.fetchSelectedInvocation();

            } else {
                this.wfeditor.selectedInvocation = null;
                this.wfeditor.invocationFiles.clear();
                this.wfeditor.displayInvocations();
                this.wfeditor.hightlightSelectedInvocation();
                this.wfeditor.drawing.clearDrawing();
                this.wfeditor.drawing.clearExecutionReports();
                this.wfeditor.drawing.dirty = false;
                this.wfeditor.drawing.displayLabel = "No Invocations Available";
                this.wfeditor.drawing.repaint();
                this.wfeditor.displayInvocationFiles();
            }
        }
        
        this.executionManager.wfeditor = this;
        this.executionManager.drawing = this.drawing;
        this.executionManager.invocationsFetched = invocationsFetched;
        this.executionManager.invocationWorkflowJsonFetched = function(result){
            // Paint the workflow invocation list
            var invocationJson = result.invocation;
            var drawingJson = result.drawing;
            var invocation;
            if(invocationJson && drawingJson){
                invocation = new DrawingInvocation();
                invocation.parseJson(invocationJson);                
            
                // Paint the invocation state
                // Repaint the element in the invocations list
                var reportDiv = document.getElementById(this.wfeditor.dialogDivName + "_report_" + invocation.invocationId);
                if(reportDiv){
                    if(invocation.status==invocation.INVOCATION_WAITING){
                        reportDiv.setAttribute("class", "invocation-waiting");

                    } else if(invocation.status==invocation.INVOCATION_RUNNING){
                        reportDiv.setAttribute("class", "invocation-running");

                    } else if(invocation.status==invocation.INVOCATION_FINISHED_OK){
                        reportDiv.setAttribute("class", "invocation-ok");

                    } else if(invocation.status==invocation.INVOCATION_FINISHED_WITH_ERRORS){
                        reportDiv.setAttribute("class", "invocation-error");

                    } else if(invocation.status==invocation.INVOCATION_WAITING_FOR_DEBUGGER){
                        reportDiv.setAttribute("class", "invocation-debugging");

                    } else {
                        reportDiv.setAttribute("class", "invocation-unknown");
                    }
                }
                this.wfeditor.hightlightSelectedInvocation();

                if(invocation.status==invocation.INVOCATION_WAITING || invocation.status==invocation.INVOCATION_RUNNING || invocation.status==invocation.INVOCATION_WAITING_FOR_DEBUGGER){
                    // Paint the drawing
                    this.wfeditor.drawing.clearExecutionReports();
                    this.wfeditor.fetcher.parseDrawing(drawingJson);
                    this.wfeditor.setCorrectDrawingSize();
                    this.wfeditor.drawing.setExecutingBlock(invocationJson.currentBlockId, invocationJson.totalBytesToStream, invocationJson.bytesStreamed);
                    
                    if(invocation.hasLocks){ 
                        this.wfeditor.drawing.displayLabel = "Locked. Waiting for: " + invocation.workflowsRemaining + " sub-workflows";
                    } else {
                        this.wfeditor.drawing.displayLabel = "Running...";
                    }
                    this.wfeditor.drawing.invocationMessage = invocation.message;
                    this.wfeditor.drawing.repaint();
                    
                    // Get the files
                    this.fetchInvocationFiles(invocation.invocationId);
                    
                    // Still not finished
                    this.wfeditor.requestRefresh(invocation.invocationId, 5000);

                } else {
                    // Finished
                    this.wfeditor.fetcher.parseDrawing(drawingJson);
                    this.wfeditor.setCorrectDrawingSize();
                    this.wfeditor.drawing.setExecutingBlock(null);
                    this.wfeditor.drawing.invocationMessage = invocation.message;
                    this.wfeditor.drawing.repaint();
                    this.wfeditor.fetchCurrentInvocationDetails();
                }
            }
        };

        this.executionManager.invocationWorkflowJsonNotPresent = function(status){
            // Need to send another query
            if(status!=3){
                // Only send if it hasn't crashed
                var invocationId = this.wfeditor.selectedInvocation.invocationId;
                this.wfeditor.requestRefresh(invocationId, 2000);
            }
        }

        this.executionManager.invocationFilesFetched = function(filesJson){
            var i;
            this.wfeditor.invocationFiles.clear();
            for(i=0;i<filesJson.documentCount;i++){
                this.wfeditor.invocationFiles.add(filesJson.documents[i]);
            }
            this.wfeditor.displayInvocationFiles(filesJson.folder, filesJson.container);
        };

        this.executionManager.invocationReportsFetched = function(reportsJson){
            this.wfeditor.drawing.setExecutionReports(reportsJson);
            
            // Display invocation status property
            if(reportsJson.invocation){
                // Delegate to the invocation display function declared below
                this.wfeditor.executionManager.invocationFetched(reportsJson.invocation, true);
            }
        };

        this.executionManager.invocationFetched = function(invocationJson, noRefetch){
            // Start a timer running if the selected invocation hasn't finished
            var invocation = new DrawingInvocation();
            invocation.parseJson(invocationJson);
            //this.wfeditor.setCorrectDrawingSize();
            
            // Repaint the element in the invocations list
            var reportDiv = document.getElementById(this.wfeditor.dialogDivName + "_report_" + invocation.invocationId);
            if(reportDiv){
                if(invocation.status==invocation.INVOCATION_WAITING){
                    reportDiv.setAttribute("class", "invocation-waiting");

                } else if(invocation.status==invocation.INVOCATION_RUNNING){
                    reportDiv.setAttribute("class", "invocation-running");

                } else if(invocation.status==invocation.INVOCATION_FINISHED_OK){
                    reportDiv.setAttribute("class", "invocation-ok");

                } else if(invocation.status==invocation.INVOCATION_FINISHED_WITH_ERRORS){
                    reportDiv.setAttribute("class", "invocation-error");

                } else if(invocation.status==invocation.INVOCATION_WAITING_FOR_DEBUGGER){
                    reportDiv.setAttribute("class", "invocation-debugging");

                } else {
                    reportDiv.setAttribute("class", "invocation-unknown");
                }
            }
            
            // Update the selected invocation 
            if(this.wfeditor.selectedInvocation && this.wfeditor.selectedInvocation.invocationId==invocation.invocationId){
                this.wfeditor.selectedInvocation.status=invocation.status;
                this.wfeditor.selectedInvocation.statusText = invocation.statusText;
                this.wfeditor.selectedInvocation.currentBlockId = invocation.currentBlockId;
            }
            
            if(invocation.status==invocation.INVOCATION_WAITING || invocation.status==invocation.INVOCATION_RUNNING || invocation.status==invocation.INVOCATION_WAITING_FOR_DEBUGGER){
                // Paint the drawing
                this.wfeditor.drawing.clearExecutionReports();
                this.wfeditor.drawing.setExecutingBlock(invocationJson.currentBlockId, invocationJson.totalBytesToStream, invocationJson.bytesStreamed);
                this.wfeditor.drawing.invocationMessage = invocation.message;
                if(invocation.hasLocks){ 
                    this.wfeditor.drawing.displayLabel = "Locked. Waiting for: " + invocation.workflowsRemaining + " sub-workflows";
                } else {
                    this.wfeditor.drawing.displayLabel = "Running...";
                }
                this.wfeditor.drawing.repaint();

                // Still not finished
                if(!noRefetch){
                    this.wfeditor.requestRefresh(invocation.invocationId, 5000);
                }
                
            } else {
                // Finished
                this.wfeditor.drawing.setExecutingBlock(null);
                this.wfeditor.drawing.repaint();
                this.wfeditor.drawing.invocationMessage = invocation.message;
                if(!noRefetch){
                    this.wfeditor.executionManager.fetchInvocationsForDocumentId(this.wfeditor.documentId);
                }
            }
        };

        this.executionManager.invocationDeleted = function(){
            this.wfeditor.selectedInvocation = null;
            this.wfeditor.executionManager.fetchInvocationsForDocumentId(this.wfeditor.documentId);
        };
        
        this.executionManager.invocationTerminated = function(results){
            this.wfeditor.terminationReportWindow.show(results);
        }
        
        this.executionManager.drawing = this.drawing;
        
        // Setup the block menu      
        if(this.drawing.menu.commands.size()==0){
            var outputCmd = new DrawingContextMenuCommand("output", function(block)
            {
                this.dialog.showBlockOutput(block);
            }, rewriteAjaxUrl("../../scripts/wfeditor/images/application_osx_terminal.png"));
            outputCmd.dialog = this;
            this.drawing.menu.addCommand(outputCmd);


            var editCmd = new DrawingContextMenuCommand("edit", function(block)
            {
                this.dialog.showBlockDebugger(block);
            }, rewriteAjaxUrl("../../scripts/wfeditor/images/server_connect.png"));
            editCmd.dialog = this;
 
            this.drawing.menu.addCommand(editCmd);            
            
            var messageCmd = new DrawingContextMenuCommand("message", function(block){
                if(this.dialog.messageDialog && block.reportJson){
                    if(block.reportJson.additionalMessage && !block.reportJson.additionalMessage==""){
                        this.dialog.messageDialog.show(block.reportJson.additionalMessage);
                    } else {
                        this.dialog.messageDialog.show(block.reportJson.message);
                    }
                }
            }, rewriteAjaxUrl("../../scripts/wfeditor/images/information.png"));
            messageCmd.dialog = this;
            this.drawing.menu.addCommand(messageCmd);
            
            var lockCmd = new DrawingContextMenuCommand("lock", function(block){
                this.dialog.showBlockLocks(block);
            }, rewriteAjaxUrl("../../scripts/wfeditor/images/lock.png"));
            lockCmd.dialog = this;
            this.drawing.menu.addCommand(lockCmd);
        }
    }
    this.resizeUI();
};

DrawingInvocationsDialog.prototype.debugSelectedInvocation = function(){
    // Open the debugger if it is not already open and we are able to
    if(this.selectedInvocation && this.selectedInvocation.status==this.selectedInvocation.INVOCATION_WAITING_FOR_DEBUGGER ){
        var blk = this.drawing.getBlockByGuid(this.selectedInvocation.currentBlockId);
        if(blk){
            this.showBlockDebugger(blk);
        }
    }     
};

DrawingInvocationsDialog.prototype.setCorrectDrawingSize = function(){
    var drawingDiv = document.getElementById(this.dialogDivName + "_drawing");
    this.drawing.minimumWidth = drawingDiv.clientWidth;
    this.drawing.minimumHeight = drawingDiv.clientHeight;
    this.drawing.repaint();
}

DrawingInvocationsDialog.prototype.resizeUI = function(){
    var dialogDiv = document.getElementById(this.dialogDivName);
    var divHeight = dialogDiv.clientHeight;
    var divWidth = dialogDiv.clientWidth;
    
    var drawingDiv = document.getElementById(this.dialogDivName + "_drawing");
    
    var newHeight = divHeight - 200;
    var newWidth = divWidth - 240;
    drawingDiv.style.width = (newWidth) + "px";    
    drawingDiv.style.height = (newHeight) + "px";
    
    var filesDiv = document.getElementById(this.dialogDivName + "_invocationFiles");
    filesDiv.style.width = (divWidth - 240) + "px";
    
    var invocationsDiv = document.getElementById(this.dialogDivName + "_invocations");
    invocationsDiv.style.height = (divHeight - 200) + "px";
    
    this.drawing.minimumWidth = newWidth;
    this.drawing.minimumHeight = newHeight;  
    this.drawing.repaint();
};

DrawingInvocationsDialog.prototype.showDialog = function(documentId){
    $("#" + this.dialogDivName).dialog('open');
    this.documentId = documentId;
    this.drawing.clearDrawing();
    this.selectedInvocation = null;
    this.invocationFiles.clear();
    this.drawing.repaint();
    this.displayInvocationFiles();
    this.drawing.documentId = documentId;
    this.executionManager.fetchInvocations();
};

DrawingInvocationsDialog.prototype.initCanvas = function(canvas){
  if (window.G_vmlCanvasManager && window.attachEvent && !window.opera)
  {
    canvas = window.G_vmlCanvasManager.initElement(canvas);
  }
  return canvas;
};

DrawingInvocationsDialog.prototype.isIE = function(){
  if (window.G_vmlCanvasManager && window.attachEvent && !window.opera) {
    return true;
  } else {
      return false;
  }
};

DrawingInvocationsDialog.prototype.fetchSelectedInvocation = function(){
    if(this.selectedInvocation!=null){
        this.drawing.clearExecutionReports();
        this.drawing.clearDrawing();
        this.drawing.repaint();
        this.drawing.editor.setupMouse();
        this.executionManager.fetchInvocationWorkflowJson(this.selectedInvocation.invocationId);
    }
};

DrawingInvocationsDialog.prototype.fetchSelectedInvocationById = function(invocationId){
    this.drawing.clearExecutionReports();
    this.drawing.clearDrawing();
    this.drawing.repaint();
    this.drawing.editor.setupMouse();
    this.executionManager.fetchInvocationWorkflowJson(invocationId);
};

DrawingInvocationsDialog.prototype.setSelectedInvocation = function(selectedInvocation){
    this.selectedInvocation = selectedInvocation;
    this.hightlightSelectedInvocation();
    this.fetchSelectedInvocation();
    this.invocationFiles.clear();
    this.displayInvocationFiles();
    
    var dialog = this;

    // Start a timer running if the selected invocation hasn't finished
    if(this.selectedInvocation.status==this.selectedInvocation.INVOCATION_WAITING || this.selectedInvocation.status==this.selectedInvocation.INVOCATION_RUNNING || this.selectedInvocation.status==this.selectedInvocation.INVOCATION_WAITING_FOR_DEBUGGER){
        this.requestRefresh(selectedInvocation.invocationId, 1000);
    } else if(this.selectedInvocation.status==this.selectedInvocation.INVOCATION_FINISHED_OK || this.selectedInvocation.status==this.selectedInvocation.INVOCATION_FINISHED_WITH_ERRORS){
        // Fetch the reports
        
    }
};

DrawingInvocationsDialog.prototype.hightlightSelectedInvocation = function(){
    var invocationsList = document.getElementById(this.dialogDivName + "_invocations_list");
    var nodes = invocationsList.childNodes;
    var i;
    var node;
    for(i=0;i<nodes.length;i++){
        node = nodes.item(i);
        if(node.invocation){
            if(this.selectedInvocation && node.invocation.invocationId==this.selectedInvocation.invocationId){
                node.setAttribute("style", "background-color: silver;");
            } else {
                node.setAttribute("style", "background-color: white;");
            }
        }
    }

    if (this.selectedInvocation && this.createdDateSpan && this.selectedInvocation && this.selectedInvocation.submitTime) {
      this.createdDateSpan.innerHTML = this.selectedInvocation.submitTime;
    } else {
      this.createdDateSpan.innerHTML = "";
    }

    if (this.selectedInvocation && this.wfidSpan && this.selectedInvocation && this.selectedInvocation.workflowId) {
      this.wfidSpan.innerHTML = this.selectedInvocation.workflowId;
    } else {
      this.wfidSpan.innerHTML = "";
    }

    if (this.selectedInvocation && this.invidSpan && this.selectedInvocation && this.selectedInvocation.invocationId) {
        this.invidSpan.innerHTML = this.selectedInvocation.invocationId;
    } else {
        this.invidSpan.innerHTML = "";
    }


    if (this.selectedInvocation && this.versionIdSpan && this.selectedInvocation && this.selectedInvocation.versionId) {
      this.versionIdSpan.innerHTML = this.selectedInvocation.versionId;
    } else {
      this.versionIdSpan.innerHTML = "";
    }

    if (this.selectedInvocation && this.versionNumSpan && this.selectedInvocation && this.selectedInvocation.versionNum) {
      var link = rewriteAjaxUrl('../../pages/workflow/wfeditor.jsp?id=' + this.selectedInvocation.workflowId + '&versionid=' + this.selectedInvocation.versionId);
      this.versionNumSpan.innerHTML = "<a href='" + link + "'>" + this.selectedInvocation.versionNum + "&nbsp;[open]</a>";
    } else {
      this.versionNumSpan.innerHTML = "";
    }

   if (this.selectedInvocation && this.queuedDateSpan && this.selectedInvocation && this.selectedInvocation.queuedTime) {
      this.queuedDateSpan.innerHTML = this.selectedInvocation.queuedTime;
    } else {
      this.queuedDateSpan.innerHTML = "";
    }

    if (this.selectedInvocation && this.dequeuedDateSpan && this.selectedInvocation && this.selectedInvocation.dequeuedTime) {
      this.dequeuedDateSpan.innerHTML = this.selectedInvocation.dequeuedTime;
    } else {
      this.dequeuedDateSpan.innerHTML = "";
    }

    if (this.selectedInvocation && this.engineIdSpan && this.selectedInvocation.engineId) {
        this.engineIdSpan.innerHTML = this.selectedInvocation.engineId;
    } else {
        this.engineIdSpan.innerHTML = "";
    }

    if (this.selectedInvocation && this.startDateSpan && this.selectedInvocation && this.selectedInvocation.startTime) {
      this.startDateSpan.innerHTML = this.selectedInvocation.startTime;
    } else {
      this.startDateSpan.innerHTML = "";
    }

    if(this.selectedInvocation && this.endDateSpan && this.selectedInvocation && this.selectedInvocation.endTime){
        this.endDateSpan.innerHTML = this.selectedInvocation.endTime;
    } else {
        this.endDateSpan.innerHTML = "";
    }

    if(this.selectedInvocation && this.statusSpan && this.selectedInvocation && this.selectedInvocation.statusText){
        this.statusSpan.innerHTML = this.selectedInvocation.statusText;
    } else {
        this.statusSpan.innerHTML = "";
    }

};

DrawingInvocationsDialog.prototype.displayInvocations = function(){
    var invocationsDiv = document.getElementById(this.dialogDivName + "_invocations");
    if(invocationsDiv!=null){
        invocationsDiv.innerHTML = "";
        var i;
        var invocation;
        var reportDiv;

        var listDiv = document.createElement("ul");
        listDiv.setAttribute("id", this.dialogDivName + "_invocations_list");

        for(i=0;i<this.executionManager.getInvocationCount();i++){
            reportDiv = document.createElement("li");
            invocation = this.executionManager.getInvocation(i);
            reportDiv.appendChild(document.createTextNode(invocation.invocationName));
            reportDiv.invocation = invocation;
            reportDiv.dialog = this;
            reportDiv.id = this.dialogDivName + "_report_" + invocation.invocationId;
            reportDiv.onclick = function(){
                this.dialog.setSelectedInvocation(this.invocation);
            }

            if(invocation.status==invocation.INVOCATION_WAITING){
                reportDiv.setAttribute("class", "invocation-waiting");

            } else if(invocation.status==invocation.INVOCATION_RUNNING){
                reportDiv.setAttribute("class", "invocation-running");

            } else if(invocation.status==invocation.INVOCATION_FINISHED_OK){
                reportDiv.setAttribute("class", "invocation-ok");

            } else if(invocation.status==invocation.INVOCATION_FINISHED_WITH_ERRORS){
                reportDiv.setAttribute("class", "invocation-error");

            } else if(invocation.status==invocation.INVOCATION_WAITING_FOR_DEBUGGER){
                reportDiv.setAttribute("class", "invocation-debugging");

            } else {
                reportDiv.setAttribute("class", "invocation-unknown");
            }

            if(invocation===this.selectedInvocation){
                reportDiv.setAttribute("style", "background-color: silver");
            }
            listDiv.appendChild(reportDiv);
        }
        invocationsDiv.appendChild(listDiv);
    }
};

DrawingInvocationsDialog.prototype.createToolbar = function(){
    var dialogDiv = document.getElementById(this.dialogDivName);
    this.toolbarDivName = this.dialogDivName + "_toolbar";
    
    var toolbarDiv = document.createElement("div");
    toolbarDiv.setAttribute("id", this.toolbarDivName);
    toolbarDiv.setAttribute("class", "navbar clearfix");
    toolbarDiv.setAttribute("style", "margin-bottom: 0px;");
    
    var toolbarContainer = document.createElement("div");
    toolbarContainer.setAttribute("class", "navbar-inner");
    toolbarContainer.setAttribute("style", "padding-left: 0px;");

    var toolbarMenu = document.createElement("ul");
    toolbarMenu.setAttribute("class", "nav clearfix");
        
    // Create Delete button
    var deleteLi = document.createElement("li");
    var deleteButton = document.createElement("a");
    deleteButton.setAttribute("id", this.toolbarDivName + "_delete");
    deleteButton.innerHTML = "<i class='icomoon-remove'></i>Delete";
    deleteLi.appendChild(deleteButton);
    toolbarMenu.appendChild(deleteLi);

    // Create a refresh services button
    var refresh = document.createElement("li");
    var refreshButton = document.createElement("a");
    refreshButton.setAttribute("id", this.toolbarDivName + "_refresh");
    refreshButton.innerHTML = "<i class='icomoon-loop-2'></i>Refresh";
    refresh.appendChild(refreshButton);
    toolbarMenu.appendChild(refresh);
    
    // Kill button
    var kill = document.createElement("li");
    var killButton = document.createElement("a");
    killButton.setAttribute("id", this.toolbarDivName + "_kill");
    killButton.innerHTML = "<i class='icomoon-switch'></i>Kill";
    kill.appendChild(killButton);
    toolbarMenu.appendChild(kill);
    
    var reRun = document.createElement("li");
    var reRunButton = document.createElement("a");
    reRunButton.setAttribute("id", this.toolbarDivName + "_rerun");
    reRunButton.innerHTML = "<i class='icomoon-play-3'></i>Rerun";
    reRun.appendChild(reRunButton);
    toolbarMenu.appendChild(reRun);
    
    // Debug button
    var debug = document.createElement("li");
    var debugButton = document.createElement("a");
    debugButton.setAttribute("id", this.toolbarDivName + "_debug");
    debugButton.innerHTML = "<i class='icomoon-bug'></i>Debug";
    debug.appendChild(debugButton);
    toolbarMenu.appendChild(debug);
    
    // Zoom Out button
    var zoomOut = document.createElement("li");
    var zoomOutButton = document.createElement("a");
    zoomOutButton.setAttribute("id", this.toolbarDivName + "_zoomOut");
    zoomOutButton.setAttribute("class", "glyphicons zoom_out");
    zoomOutButton.innerHTML = "<i class='icomoon-zoom-out'></i>Zoom Out";
    zoomOut.appendChild(zoomOutButton);
    toolbarMenu.appendChild(zoomOut);

    // Reset zoom button
    var resetZoom = document.createElement("li");
    var resetZoomButton = document.createElement("a");
    resetZoomButton.setAttribute("id", this.toolbarDivName + "_resetZoom");
    resetZoomButton.innerHTML = "<i class='icomoon-expand'></i>Reset Zoom";
    resetZoom.appendChild(resetZoomButton);
    toolbarMenu.appendChild(resetZoom);

    // Zoom In button
    var zoomIn = document.createElement("li");
    var zoomInButton = document.createElement("a");
    zoomInButton.setAttribute("id", this.toolbarDivName + "_zoomIn");
    zoomInButton.innerHTML = "<i class='icomoon-zoom-in'></i>Zoom In";
    zoomIn.appendChild(zoomInButton);
    toolbarMenu.appendChild(zoomIn);    

    toolbarDiv.appendChild(toolbarContainer);
    toolbarContainer.appendChild(toolbarMenu);
    dialogDiv.appendChild(toolbarDiv);
    
    zoomOutButton.wfeditor = this;
    zoomOutButton.onclick = function(){
        this.wfeditor.drawing.zoomOut();
    };

    resetZoomButton.wfeditor = this;
    resetZoomButton.onclick = function(){
        this.wfeditor.drawing.resetZoom();
    };

    zoomInButton.wfeditor = this;
    zoomInButton.onclick = function(){
        this.wfeditor.drawing.zoomIn();
    };
    
    refreshButton.wfeditor = this;
    refreshButton.onclick = function(){
        this.wfeditor.executionManager.fetchInvocationsForDocumentId(this.wfeditor.documentId);
    };

    deleteButton.wfeditor = this;
    deleteButton.onclick = function(){
        var invocation = this.wfeditor.selectedInvocation;
        if(invocation!=null){
            this.wfeditor.executionManager.deleteInvocation(invocation.invocationId);
        }
    };

    killButton.wfeditor = this;
    killButton.onclick = function(){
        var invocation = this.wfeditor.selectedInvocation;
        if(invocation!=null){
            this.wfeditor.terminationReportWindow.show(null);
            this.wfeditor.executionManager.terminateInvocation(invocation);
        }
    };

    reRunButton.wfeditor = this;
    reRunButton.onclick = function(){
        var invocation = this.wfeditor.selectedInvocation;
        if(invocation!=null){
            this.wfeditor.resubmitInvocation(invocation);
        }        
    };

    debugButton.wfeditor = this;
    debugButton.onclick = function(){
        this.wfeditor.debugSelectedInvocation();
    };
};

DrawingInvocationsDialog.prototype.resubmitInvocation = function(invocation){
    if(invocation){
        var dialog = this;
        var cb = function(){
            dialog.executionManager.fetchInvocationsForDocumentId(dialog.documentId);
        };
        this.executionManager.resubmitInvocation(invocation, cb);
    }
};

DrawingInvocationsDialog.prototype.createInvocationDetailsBox = function(){
    // Create the invocation properties
    var dialogDiv = document.getElementById(this.dialogDivName);
    var invocationDetailsDiv = document.createElement("div");
    invocationDetailsDiv.setAttribute("id", this.dialogDivName + "_invocationDetails");
    invocationDetailsDiv.setAttribute("class", "ui-corner-all");
    invocationDetailsDiv.setAttribute("style", "z-index:0; border:1px solid gray; margin-top: 10px; font-size: 10pt; overflow: auto; width: 190px; height: 120px;");

//    Removed as not enough space on the div
//    var titleDiv = document.createElement("div");
//    titleDiv.appendChild(document.createTextNode("Invocation Details"));
//    titleDiv.setAttribute("style", "padding-left: 2px; padding-top: 4px; padding-bottom: 5px;");
//    invocationDetailsDiv.appendChild(titleDiv);

    var wfidDiv = document.createElement("div");
    wfidDiv.appendChild(document.createTextNode("WorkflowId:"));
    wfidDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%; overflow: hidden;");

    this.wfidSpan = document.createElement("span");
    this.wfidSpan.appendChild(document.createTextNode(""));
    this.wfidSpan.setAttribute("style", "padding-left: 2px;");
    wfidDiv.appendChild(this.wfidSpan);
    invocationDetailsDiv.appendChild(wfidDiv);


    var InvidDiv = document.createElement("div");
    InvidDiv.appendChild(document.createTextNode("InvocationId:"));
    InvidDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%; overflow: hidden;");

    this.invidSpan= document.createElement("span");
    this.invidSpan.appendChild(document.createTextNode(""));
    this.invidSpan.setAttribute("style", "padding-left: 2px;");
    InvidDiv.appendChild(this.invidSpan);
    invocationDetailsDiv.appendChild(InvidDiv);





    var versionIdDiv = document.createElement("div");
    versionIdDiv.appendChild(document.createTextNode("VersionId:"));
    versionIdDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%; overflow: hidden;");

    this.versionIdSpan = document.createElement("span");
    this.versionIdSpan.appendChild(document.createTextNode(""));
    this.versionIdSpan.setAttribute("style", "padding-left: 2px;");
    versionIdDiv.appendChild(this.versionIdSpan);
    invocationDetailsDiv.appendChild(versionIdDiv);

    var versionNumDiv = document.createElement("div");
    versionNumDiv.appendChild(document.createTextNode("Version Num:"));
    versionNumDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.versionNumSpan = document.createElement("span");
    this.versionNumSpan.appendChild(document.createTextNode(""));
    this.versionNumSpan.setAttribute("style", "padding-left: 2px;");
    versionNumDiv.appendChild(this.versionNumSpan);
    invocationDetailsDiv.appendChild(versionNumDiv);

    var createdDiv = document.createElement("div");
    createdDiv.appendChild(document.createTextNode("Created:"));
    createdDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.createdDateSpan = document.createElement("span");
    this.createdDateSpan.appendChild(document.createTextNode(""));
    this.createdDateSpan.setAttribute("style", "padding-left: 2px;");
    createdDiv.appendChild(this.createdDateSpan);
    invocationDetailsDiv.appendChild(createdDiv);

    var queuedDiv = document.createElement("div");
    queuedDiv.appendChild(document.createTextNode("Queued:"));
    queuedDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.queuedDateSpan = document.createElement("span");
    this.queuedDateSpan.appendChild(document.createTextNode(""));
    this.queuedDateSpan.setAttribute("style", "padding-left: 2px;");
    queuedDiv.appendChild(this.queuedDateSpan);
    invocationDetailsDiv.appendChild(queuedDiv);

    var dequeuedDiv = document.createElement("div");
    dequeuedDiv.appendChild(document.createTextNode("Dequeued:"));
    dequeuedDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.dequeuedDateSpan = document.createElement("span");
    this.dequeuedDateSpan.appendChild(document.createTextNode(""));
    this.dequeuedDateSpan.setAttribute("style", "padding-left: 2px;");
    dequeuedDiv.appendChild(this.dequeuedDateSpan);
    invocationDetailsDiv.appendChild(dequeuedDiv);

    var engineIdDiv = document.createElement("div");
    engineIdDiv.appendChild(document.createTextNode("Engine:"));
    engineIdDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.engineIdSpan = document.createElement("span");
    this.engineIdSpan.appendChild(document.createTextNode(""));
    this.engineIdSpan.setAttribute("style", "padding-left: 2px;");
    engineIdDiv.appendChild(this.engineIdSpan);
    invocationDetailsDiv.appendChild(engineIdDiv);

    var startDiv = document.createElement("div");
    startDiv.appendChild(document.createTextNode("Start:"));
    startDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.startDateSpan = document.createElement("span");
    this.startDateSpan.appendChild(document.createTextNode(""));
    this.startDateSpan.setAttribute("style", "padding-left: 2px;");
    startDiv.appendChild(this.startDateSpan);
    invocationDetailsDiv.appendChild(startDiv);

    var endDiv = document.createElement("div");
    endDiv.appendChild(document.createTextNode("End:"));
    endDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.endDateSpan = document.createElement("span");
    this.endDateSpan.appendChild(document.createTextNode(""));
    this.endDateSpan.setAttribute("style", "padding-left: 2px;");
    endDiv.appendChild(this.endDateSpan);
    invocationDetailsDiv.appendChild(endDiv);

    var statusDiv = document.createElement("div");
    statusDiv.appendChild(document.createTextNode("Status:"));
    statusDiv.setAttribute("style", "padding-top: 2px; padding-left: 2px; font-size: 75%;");

    this.statusSpan = document.createElement("span");
    this.statusSpan.appendChild(document.createTextNode(""));
    this.statusSpan.setAttribute("style", "padding-left: 2px;");
    statusDiv.appendChild(this.statusSpan);
    invocationDetailsDiv.appendChild(statusDiv);

    dialogDiv.appendChild(invocationDetailsDiv);
};

/** Create the file list box */
DrawingInvocationsDialog.prototype.createInvocationFilesBox = function(){
    var dialogDiv = document.getElementById(this.dialogDivName);
    var fileListDiv = document.createElement("div");
    fileListDiv.setAttribute("id", this.dialogDivName + "_invocationFiles");
    fileListDiv.setAttribute("class", "ui-corner-all");
    fileListDiv.setAttribute("style", "overflow-y: scroll; overflow-x: auto; z-index:0; border:1px solid gray; float:right; margin-top: 10px; width: 560px; height: 120px;");
    dialogDiv.appendChild(fileListDiv);
};

DrawingInvocationsDialog.prototype.fetchCurrentInvocationDetails = function(){
    if(this.selectedInvocation!=null){
        this.invocationFiles.clear();
        this.displayInvocationFiles();
        this.executionManager.fetchInvocationFiles(this.selectedInvocation.invocationId);
        this.executionManager.fetchInvocationReports(this.selectedInvocation.invocationId);
    }
};

DrawingInvocationsDialog.prototype.displayInvocationFiles = function(folder, container){
    var fileListDiv = document.getElementById(this.dialogDivName + "_invocationFiles");
    fileListDiv.innerHTML = "";
    var size = this.invocationFiles.size();
    var i;
    var li;
    var ul = document.createElement("ul");
    var p;
    var file;

    if(this.selectedInvocation !== null && folder !== this.selectedInvocation.invocationId)
    {
        li = document.createElement("li");
        li.setAttribute("class", "invocation invocation-back");
        li.dialog = this;
        li.onclick = function () {
            this.dialog.executionManager.fetchInvocationFiles(container);
        };
        li.appendChild(document.createTextNode("Back"));
        ul.appendChild(li);

    }
    for (i = 0; i < size; i++) {
        file = this.invocationFiles.get(i);

        if (file.type !== undefined && file.type === "folder") {
            li = document.createElement("li");
            if(file.workflow)
            {
                li.setAttribute("class", "invocation invocation-workflow");
            }
            else
            {
                li.setAttribute("class", "invocation invocation-folder");
            }
            li.documentId = file.id;
            li.dialog = this;
            li.onclick = function () {
                this.dialog.executionManager.fetchInvocationFiles(this.documentId);
            };
            li.appendChild(document.createTextNode(file.name));
            ul.appendChild(li);
        }
        else {
            li = document.createElement("li");
            li.setAttribute("class", "invocation invocation-file");
            li.documentId = file.id;
            li.dialog = this;
            li.onclick = function () {
                this.dialog.viewer.showFile(this.documentId, file.name);
            };
            li.appendChild(document.createTextNode(file.name));
            ul.appendChild(li);
        }
    }
    fileListDiv.appendChild(ul);
};

DrawingInvocationsDialog.prototype.timer = function(scope, invocationId){
    scope.timeoutVar = null;
    scope.executionManager.fetchInvocation(invocationId);
};

DrawingInvocationsDialog.prototype.requestRefresh = function(invocationId, delay){
    if(this.timeoutVar!=null || this.timeoutVar!=undefined){
        clearTimeout(this.timeoutVar);
        this.timeoutVar = null;
    }
    
    this.timeoutVar = setTimeout(this.timer, 5000, this, invocationId);    
};
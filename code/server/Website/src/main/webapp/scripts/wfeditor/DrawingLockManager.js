/* 
 * This script provides tools for managing a single workflow lock.
 */
function DrawingLockManager(){
    this.divName = "";
    this.lockDetails = null;
    this.confirmDialog = new ConfirmDialog();
    this.invocationId = null;
    this.contextId = null;
    this.timeoutData = null;
    this.timeoutInterval = 10000;
}

DrawingLockManager.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var contentsDiv = document.createElement("div");
        contentsDiv.setAttribute("id", this.divName + "_contents");
        
        var dialogDiv = document.createElement("div");
        dialogDiv.setAttribute("id", this.divName + "_dialog");
        dialogDiv.setAttribute("class", "dialog");
        
        
        div.appendChild(dialogDiv);
        div.appendChild(contentsDiv);
        this.confirmDialog.init(this.divName + "_dialog");
            
        var manager = this;
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 450,
            width: 600,
            resizable: false,
            title: "Workflow Lock Summary",
            modal: true,
            buttons: {
                'Refresh' : function(){
                    manager.refresh();
                },
                'Proceed' : function(){
                    manager.proceed();
                },
                'Rerun Failed' : function(){
                    manager.rerunFailed();
                },
                'Terminate' : function(){
                    manager.terminate();
                },
                'Close': function(){
                    $(this).dialog('close');
                }                        
            },
            close: function(){
                clearTimeout(manager.timeoutData);
                manager.lockDetails = null;
                manager.invocationId = null;
                manager.contextId = null;
            }
        });
    }
};

DrawingLockManager.prototype.viewLockForBlock = function(invocationId, contextId){
    if(invocationId){
        this.invocationId = invocationId;
    }
    
    if(contextId){
        this.contextId = contextId;
    }
    
    var div = document.getElementById(this.divName);
    if(div && this.invocationId && this.contextId){
        var contentsDiv = document.getElementById(this.divName + "_contents");

        $(div).dialog('open');
        var callData = {
            invocationId: this.invocationId,
            contextId: this.contextId
        };

        var dialog = this;
        var cb = function(o){
            if(!o.error){
                dialog.lockDetails = o.lockDetails;
                
                // Display
                var html='<div class="lockDialogDiv">';
                html+="<ul>";
                html+='<li class="lockLi lockTotalText">Total:' + o.lockDetails.total + '</li>';
                html+='<li class="lockLi lockWaitingText">Waiting:' + o.lockDetails.queued + '</li>';
                html+='<li class="lockLi lockRunningText">Running:' + o.lockDetails.running + '</li>';
                html+='<li class="lockLi lockSucceededText">Succeeded:' + o.lockDetails.succeded + '</li>';
                html+='<li class="lockLi lockFailedText">Failed:' + o.lockDetails.failed + '</li>';
                html+="</ul>";
                html+='<div id="' + dialog.divName + '_legend"></div>';
                html+='</div>';
                html+='<div class="lockChart" id="' + dialog.divName + '_chart"></div>';
                contentsDiv.innerHTML = html;
                
                // Graph
                var data = new Array();
                data[0] = {data: o.lockDetails.queued, label: "Waiting"};
                data[1] = {data: o.lockDetails.running, label: "Running"};
                data[2] = {data: o.lockDetails.failed,label: "Failed"};
                data[3] = {data: o.lockDetails.succeded,label: "Succeeded"};                
                var params = {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.5,
                            label: {
                                show: true,
                                radius: 3 / 4,
                                background: { opacity: 0.5
                                }

                            }
                        }
                    },
                    legend: {
                        show: true,
                        container: '#' + dialog.divName + '_legend'
                    }
                };                  
                $.plot($("#" + dialog.divName + "_chart"), data, params);

            }
            
            dialog.timeoutData = setTimeout(function(param){
               param.viewLockForBlock();
            }, dialog.timeoutInterval, dialog);
                
        };
        
        var errorCb = function(message){
            clearTimeout(dialog.timeoutData);
            contentsDiv.innerHTML = '<div class="ui-state-error ui-corner-all" style="width:95%;">' + message + '</div>';
        };
        jsonCall(callData, "../../servlets/workflow?method=fetchLockSummary", cb, errorCb);
    }
};

DrawingLockManager.prototype.proceed = function(){
    if(this.lockDetails && this.invocationId && this.contextId){
        var callData = {
            lockId: this.lockDetails.lockId
        };
        
        var dialog = this;
        var cb = function(o){
            clearTimeout(dialog.timeoutData);
            dialog.lockDetails = null;
            dialog.invocationId = null;
            dialog.contextId = null;
            $("#" + dialog.divName).dialog('close');
        };
        jsonCall(callData, "../../servlets/workflow?method=proceedWithLock", cb);
    }
};

DrawingLockManager.prototype.rerunFailed = function(){
    if(this.lockDetails && this.invocationId && this.contextId){
        var callData = {
            lockId: this.lockDetails.lockId
        };
        
        var dialog = this;
        var cb = function(o){
            dialog.viewLockForBlock(dialog.invocationId, dialog.contextId);
        };
        jsonCall(callData, "../../servlets/workflow?method=rerunFailedInLock", cb);
    }    
};

DrawingLockManager.prototype.refresh = function(){
    if(this.invocationId && this.contextId){
        this.viewLockForBlock(this.invocationId, this.contextId);
    }    
};

DrawingLockManager.prototype.terminate = function(){
    if(this.lockDetails && this.invocationId && this.contextId){
        var callData = {
            lockId: this.lockDetails.lockId
        };
        
        var dialog = this;
        var cb = function(o){
            dialog.viewLockForBlock(dialog.invocationId, dialog.contextId);
        };
        jsonCall(callData, "../../servlets/workflow?method=terminateLock", cb);
    }     
};
/* 
 * This script provides a dialog box that can display performance data for a block
 */
function PerformanceDialog(dataFetcher){
    this.divName = "";
    this.dataFetcher = dataFetcher;
    this.block = null;
    this.chartSelectDiv = null;
}

PerformanceDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var manager = this;
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 450,
            width: 600,
            title: "Block Performance Summary",
            modal: true,
            buttons: {
                /*
                'Terminate' : function(){
                    manager.terminate();
                },
                */
                'Close': function(){
                    $(this).dialog('close');
                }                        
            },
            close: function(){

            },
            resizeStop: function(event, ui){
        
            }
        });
        
        var bp = $("#" + this.divName).parent(".ui-dialog").children(".ui-dialog-buttonpane")[0];
        if(bp){
            this.chartSelectDiv = document.createElement("div");

            bp.appendChild(this.chartSelectDiv);
            
        }
    }
};

PerformanceDialog.prototype.setBlock = function(block){
    this.block = block;
    
    // Setup the selector
    if(this.chartSelectDiv){
        if(this.selector){
            $(this.selector).selectBox('destroy');
        }
        this.chartSelectDiv.innerHTML = "";
        var titleSpan = document.createElement("span");
        titleSpan.appendChild(document.createTextNode("Show: " ));

        this.selector = document.createElement("select");

        var option = document.createElement("option");
        option.setAttribute("value", "summary");
        option.serviceId = block.serviceId;
        option.appendChild(document.createTextNode("Summary"));
        this.selector.appendChild(option);
        var modelOption;
        var summaryOption;

        // Add options
        var memoryOption = document.createElement("option");
        memoryOption.setAttribute("value", "memory");
        memoryOption.serviceId = block.serviceId;
        memoryOption.appendChild(document.createTextNode("Memory Stats"));
        this.selector.appendChild(memoryOption);
        
        var durationOption = document.createElement("option");
        durationOption.setAttribute("value", "duration");
        durationOption.serviceId = block.serviceId;
        durationOption.appendChild(document.createTextNode("Duration Stats"));
        this.selector.appendChild(durationOption);

        this.chartSelectDiv.appendChild(titleSpan);
        this.chartSelectDiv.appendChild(this.selector);    
        $(this.selector).selectBox();
        
        var dialog = this;
        $(this.selector).change(function(){
            var selectedOption = this.selectedOptions[0];
            
            if(selectedOption && selectedOption.value && selectedOption.serviceId && selectedOption.serviceId!==""){
                var div = document.getElementById(dialog.divName);
                div.innerHTML = "Fetching...";                
                
                if("summary"===selectedOption.value){
                    setTimeout(dialog.showSummary, 10, dialog);
                } else if("memory"===selectedOption.value){
                    setTimeout(dialog.showMemory, 10, dialog);
                } else if("duration"===selectedOption.value){
                    setTimeout(dialog.showDuration, 10, dialog);
                }
            }
        });
        
        /*
                    try {
                        $.plot($("#" + dialog.divName), {}, {});
                        $.plot($("#" + dialog.divName)).shutdown();
                    } catch (err){}        
        */
    }

};

PerformanceDialog.prototype.showSummary = function(dialog){
    var self;
    if(dialog){
        self = dialog;
    } else {
        self = this;
    }
    
    $("#" + self.divName).dialog('open');

    try {
        $.plot($("#" + self.divName), {}, {});
        $.plot($("#" + self.divName)).shutdown();
    } catch (err){}   
                    
    if(self.block && self.block.serviceId){
        asyncJsonGet("/monitor/wfeditorcharts/summary/" + self.block.serviceId, function(results){
            var div = document.getElementById(self.divName);
            if(div){
                var html = '<table style="width:98%;"><thead><tr><th>Attribute</th><th>Value</th></tr></thead><tbody>';
                var keys = Object.keys(results);
                for(var i=0;i<keys.length;i++){
                    html+='<tr><td>' + keys[i] + '</td><td>' + results[keys[i]] + '</td></tr>';
                }
                html+="</tbody></table>";
                div.innerHTML = html;
            }
        });
    }
};

PerformanceDialog.prototype.showMemory = function(dialog){
    var self;
    if(dialog){
        self = dialog;
    } else {
        self = this;
    }
    
    $("#" + self.divName).dialog('open');

    try {
        $.plot($("#" + self.divName + "_chart"), {}, {});
        $.plot($("#" + self.divName + "_chart")).shutdown();
    } catch (err){} 
    
    if(self.block && self.block.serviceId){
        asyncJsonGet("/monitor/wfeditorcharts/memory/" + self.block.serviceId + "/20", function(results){
            self.displayHistogram(results, self.divName);
        });
    }   
};

PerformanceDialog.prototype.showDuration = function(dialog){
    var self;
    if(dialog){
        self = dialog;
    } else {
        self = this;
    }
    
    $("#" + self.divName).dialog('open');

    try {
        $.plot($("#" + self.divName + "_chart"), {}, {});
        $.plot($("#" + self.divName + "_chart")).shutdown();
    } catch (err){} 
    
    if(self.block && self.block.serviceId){
        asyncJsonGet("/monitor/wfeditorcharts/durations/" + self.block.serviceId + "/20", function(results){
            self.displayHistogram(results, self.divName);
        });
    }    
};

/*
PerformanceDialog.prototype.showDurationHistogram = function(dialog){

    if(dialog.block && dialog.block.serviceId){

        var cb = function(results){
            dialog.displayHistogram(results);
        };
        dialog.dataFetcher.fetchDurationHistogram(dialog.block.serviceId, "all", cb);
    }
};
*/

PerformanceDialog.prototype.displayHistogram = function(data, divName){
    var div = document.getElementById(this.divName);
    if(div){
        div.innerHTML = "";
        if(!data.error){
            var chartDiv = document.createElement("div");
            chartDiv.setAttribute("id", divName + "_chart");
            chartDiv.setAttribute("style", "width: 95%; height:95%;");
            div.appendChild(chartDiv);
            $.plot($("#" + divName + "_chart"), data.flotData, {
                xaxis: {
                    label: "Value",
                    labelPos: "high"
                },
                yaxis: {
                    label: "Frequency",
                    labelPos: "high"
                }
            });            
        }     
    } else {
        div.innerHTML = "Data Error";
    }
};

PerformanceDialog.prototype.displayXYChart = function(results){
    if(!results.error){
        $.plot($("#" + this.divName), results.flotData, results.flotProperties);
    }        
};
    
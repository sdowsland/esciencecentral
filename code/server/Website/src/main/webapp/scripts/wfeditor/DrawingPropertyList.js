/* 
 * This object provides a list of properties
 */
function DrawingPropertyList(parentObject){
    this.properties = new List();       // Properties
    this.parentObject = parentObject;   // Object containing these properties
    this.editorDivName = "";            // Div holding the editor
    this.filetree = new FileTree();     // File chooser tree
    this.propertiesChanged = false;     // Have any properties been altered
}

/** Get the drawing containing this list  */
DrawingPropertyList.prototype.getDrawing = function(){
    /*
    if(this.parentObject instanceof DrawingObject){
        return this.parentObject;
    } else {
        return this.parentObject.parentDrawing;
    }
    */
   if(this.parentObject){
       if(this.parentObject.parentDrawing){
           return this.parentObject.parentDrawing;
       } else {
           return this.parentObject;
       }
   } else {
       return null;
   }
};

/** Get a list of properties that have values different to their defaults */
DrawingPropertyList.prototype.listNonDefaultProperties = function(){
    var changed = new Array();
    for(var i=0;i<this.properties.size();i++){
        if(this.properties.get(i).containsNonDefaultValue){
            changed.push(this.properties.get(i));
        }
    }       
    return changed;
};

/** Validate the non-default status of the properties */
DrawingPropertyList.prototype.validateNonDefaultStatuses = function(){
    for(var i=0;i<this.properties.size();i++){
        this.properties.get(i).hasNonDefaultValue();
    }    
};

/** Add an editable property */
DrawingPropertyList.prototype.addProperty = function(property){
	this.properties.add(property);
        property.parentList = this;
};

/** Clear the properties */
DrawingPropertyList.prototype.clear = function(){
    this.properties.clear();
};

/** Get a property by name */
DrawingPropertyList.prototype.getPropertyByName = function(name){
    var i;
    var property;
    for(i=0;i<this.properties.size();i++){
        property = this.properties.get(i);
        if(property.name==name){
            return property;
        }
    }
    return null;
};

/** Get a property by index */
DrawingPropertyList.prototype.getProperty = function(index){
    return this.properties.get(index);
}

/** Get the number of properties */
DrawingPropertyList.prototype.getPropertyCount = function() {
    return this.properties.size();
};

/** Parse a set of JSON properties as returned by the server */
DrawingPropertyList.prototype.parseJson = function(propertiesJson){
    this.properties.clear();
    var propertyJson;
    var propertyCount = propertiesJson.propertyCount;
    var property;
    var i;
    for(i=0;i<propertyCount;i++){
        propertyJson = propertiesJson.propertyArray[i];
        property = new DrawingProperty(propertyJson.name, propertyJson.value, propertyJson.type, propertyJson.description);
        property.category = propertyJson.category;
        property.parentList = this;
        if(propertyJson.jsonValue){
            property.jsonValue = propertyJson.jsonValue;
            if(propertyJson.id){
                property.id = propertyJson.id;
            } else {
                property.id = null;
            }
        }
        
        if(propertyJson.options){
            property.options = propertyJson.options;
        }
        
        // Exposed details
        if(propertyJson.exposedProperty){
            property.exposedProperty = propertyJson.exposedProperty;
        } else {
            property.exposedProperty = false;
            
        }
        
        if(propertyJson.exposedName){
            property.exposedName = propertyJson.exposedName;
        } else {
            property.exposedName = propertyJson.name;
        }
        
        // Defaults
        if(propertyJson.defaultSupported){
            property.defaultSupported = propertyJson.defaultSupported;
        } else {
            property.defaultSupported = false;
        }
        
        if(propertyJson.defaultPresent){
            property.defaultPresent = propertyJson.defaultPresent;
        } else {
            property.defaultPresent = false;
        }
        
        property.defaultValue = propertyJson.defaultValue;
        this.addProperty(property);
    }
    
    // Update the status of the defaults
    this.validateNonDefaultStatuses();
};

/** Create a JSON representation of this property list */
DrawingPropertyList.prototype.createJson = function(){
    var propertyCount = this.getPropertyCount();
    var propertyArray = new Array(propertyCount);
    var property;
    var propertyJson;
    var i;

    for(i=0;i<propertyCount;i++){
        property = this.getProperty(i);
        propertyJson = {
            name: property.name,
            exposedProperty: property.exposedProperty,
            value: property.value,
            type: property.propertyType,
            description: property.description,
            defaultSupported: property.defaultSupported,
            defaultPresent: property.defaultPresent,
            defaultValue: property.defaultValue,
            category: property.category
        };

        if(property.exposedName){
            propertyJson.exposedName = property.exposedName;
        }
        
        if(property.id!=null){
            propertyJson.id = property.id;
        } else {
            propertyJson.id = null;
        }

        // Add the options if there are any
        if(property.options){
            propertyJson.options = property.options;
        }
        
        // Add the extra data if it is present
        if(property.jsonValue){
            propertyJson.jsonValue = property.jsonValue;
        }
        propertyArray[i] = propertyJson;
        
        
    }

    var propertiesJson = {
        propertyCount: propertyCount,
        propertyArray: propertyArray
    };
    return propertiesJson;

};

/** Add a string property */
DrawingPropertyList.prototype.addString = function(name, value, description){
    var property = this.getPropertyByName(name);
    if(property==null){
        property = new DrawingProperty();
        property.parentList = this;
        property.name = name;
        this.addProperty(property);
    }
    if(description){
        property.description = description;
    }
    property.propertyType = property.STRING_TYPE;
    property.value = value;
};

/** Add a boolean property */
DrawingPropertyList.prototype.addBoolean = function(name, value, description){
    var property = this.getPropertyByName(name);
    if(property==null){
        property = new DrawingProperty();
        property.parentList = this;
        property.name = name;
        this.addProperty(property);
    }
    if(description){
        property.description = description;
    }
    property.propertyType = property.BOOLEAN_TYPE;
    property.value = value;
};

/** Get a list of unique categories */
DrawingPropertyList.prototype.listCategories = function(){
    var categoryArray = new Array();
    
    // Push in standard categories that we want to come first
    categoryArray.push("Block");
    categoryArray.push("Display");
    categoryArray.push("Engine");
    categoryArray.push("Debugging");
    var property;
    for(var i=0;i<this.properties.size();i++){
        property = this.properties.get(i);
        if(property.category!=undefined && property.category!=null){
            if(categoryArray.indexOf(property.category)==-1){
                categoryArray.push(property.category);
            }
        }
    }    
    return categoryArray;
};

DrawingPropertyList.prototype.displayProperties2 = function(){
    var i;
    var property;
    var tr;
    var form = document.createElement("form");
    form.setAttribute("name", this.editorDivName + "_propertyForm");
    var table = document.createElement("table");
    table.setAttribute("cellspacing", "5");
    form.appendChild(table);


    for(i=0;i<this.getPropertyCount();i++){
        property = this.getProperty(i);
        tr = property.createHtmlTableRow();
        table.appendChild(tr);
    }
    var editorElement = document.getElementById(this.editorDivName);

    editorElement.innerHTML = "";
    editorElement.appendChild(form);
    
    
};

DrawingPropertyList.prototype.displayProperties = function(){
     // Create the tab layout
     var categories;
     if(this.parentObject instanceof DrawingObject){
         categories = new Array();
         categories.push("Drawing");
     } else {
         categories = this.listCategories();
     }
     var i;
     
     var html = '<div style="overflow: auto;" id="' + this.editorDivName + '_tabs">';
     html+='<ul>';
     for(i=0;i<categories.length;i++){
         html+='<li><a href="#' + this.editorDivName + '_tabs_' + categories[i] + '">' + categories[i] + '</a></li>';
     }
     
     html+='</ul>';
     for(i=0;i<categories.length;i++){
        html+='<div id="' + this.editorDivName + '_tabs_' + categories[i] + '" style="overflow: auto"></div>';
     }
     html+="</div>";
    
    var editorElement = document.getElementById(this.editorDivName);
    editorElement.style.overflow = "auto";
    editorElement.innerHTML = html;
    $("#" + this.editorDivName + "_tabs").tabs();
    
    // Create a table form for each category
    var form;
    var property;
    var tr;
    var table;    
    var formTab
    for(i=0;i<categories.length;i++){
        form = document.createElement("form");
        form.setAttribute("id", this.editorDivName + "_form_" + categories[i]);
        table = document.createElement("table");
        table.setAttribute("cellspacing", "5");
        table.setAttribute("id", this.editorDivName + "_table_" + categories[i]);
        if(this.getDrawing() && this.getDrawing().isExternalService() && this.parentObject instanceof DrawingBlock){
            table.innerHTML = '<thead><tr><th style="text-align: left;">Name</th><th style="text-align: left;">Value</th><th></th><th style="text-align: left;">Public</th></tr></thead>';
        }
        
        form.appendChild(table);  
        formTab = document.getElementById(this.editorDivName + "_tabs_" + categories[i]);
        formTab.appendChild(form);        
    }
    
    if(this.parentObject instanceof DrawingBlock){
        for(i=0;i<this.getPropertyCount();i++){
            property = this.getProperty(i);
            tr = property.createHtmlTableRow();
            table = document.getElementById(this.editorDivName + "_table_" + property.category);
            if(table){
                table.appendChild(tr);
            }
        }        
    } else {
        table = document.getElementById(this.editorDivName + "_table_Drawing");
        for(i=0;i<this.getPropertyCount();i++){
            property = this.getProperty(i);
            tr = property.createHtmlTableRow();
            table.appendChild(tr);            
        }               
    }
    $("#option_selector").selectBox();
    this.resize();
};

DrawingPropertyList.prototype.resize = function(){
    var editorDiv = document.getElementById(this.editorDivName);
    var tabDiv = document.getElementById(this.editorDivName + "_tabs");
    if(editorDiv && tabDiv){
        var width = editorDiv.clientWidth;
        var height = editorDiv.clientHeight;
        
        tabDiv.style.height = (height - 22) + "px";
        tabDiv.style.width = (width - 34) + "px";
    }
};
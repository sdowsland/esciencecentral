/** This class defines an x-y co-ordinate */
function DrawingPoint(xPos, yPos){
	this.x = xPos;
	this.y = yPos;
}

/** Calculate the distance from another point */
DrawingPoint.prototype.distanceFrom = function(p){
	return Math.sqrt(((this.x - p.x) * (this.x - p.x)) + ((this.y - p.y) * (this.y - p.y)));
};
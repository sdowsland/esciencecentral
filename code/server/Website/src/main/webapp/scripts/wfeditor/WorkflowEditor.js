/* 
 * This is the top level script for a workflow editor object. It manages a
 * workflow drawing on a page, all of the executions etc.
 */
//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/wfeditor/wfeditor.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
//<script type="text/javascript" src="../../scripts/canvas/canvas.text.js"></script>
//<script type="text/javascript" src="../../scripts/filebrowser/filetree.js"></script>
//<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
//<script type="text/javascript" src="../../scripts/canvas/optimer-bold-normal.js"></script>
//<script type="text/javascript" src="../../scripts/canvas/optimer-normal-normal.js"></script>
//<script type="text/javascript" src="../../scripts/jscrollpane/jquery.mousewheel.js"></script>
//<script type="text/javascript" src="../../scripts/jscrollpane/jquery.em.js"></script>
//<script type="text/javascript" src="../../scripts/jscrollpane/jScrollPane.js"></script>
//<script type="text/javascript" src="../../scripts/jgrowl/jquery.jgrowl_minimized.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingBlock.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingConnection.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingConstants.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingContextMenu.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingEditor.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingExecutionManager.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingFetcher.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingInvocation.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingObject.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingPoint.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingPort.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingProperty.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingPropertyList.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingSaver.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingUtils.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/PaletteFetcher.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingInvocationsDialog.js"></script>
//<script type="text/javascript" src="../../scripts/wfeditor/DrawingBlockDebugger.js"></script>
//
//<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>





function WorkflowEditor(){
    this.editorDivName = "";                                    // Div that holds everything
    this.editorDiv = null;
    this.canvasDivName = "";                                    // Div that holds the drawing
    this.toolbarDivName = "";
    this.canvasDiv = null;
    this.dialogDivName = "";
    this.outputDialogDivName = "";                              // Div that holds the block output text dialog
    this.blockCaptionDivName = "";                              // Div that holds the block caption
    this.paletteDivName = "";                                   // Name of the div that will hold the palette
    this.helpDialogDivName = "";                                // Name of the div that holds the block description
    this.serviceVersionDialogDivName = "";                      // Version chooser dialog
    this.drawing = new DrawingObject();                         // Drawing being edited
    this.drawingEditor = new DrawingEditor();                    // Controls the editing process
    this.fetcher = new DrawingFetcher();                         // Loads drawings from the server
    this.paletteFetcher = new PaletteFetcher();                  // Loads workflow blocks
    this.performanceFetcher = new PerformanceDataFetcher(this.drawing); // Fetched performance data
    this.performanceDialog = new PerformanceDialog(this.performanceFetcher);    // Displays performance results
    this.saver = new DrawingSaver();                             // Saves drawings back to the server
    this.executionManager = new DrawingExecutionManager();       // Deals with running the drawing
    this.temporaryWorkflowJson = null;                           // Used as a temporary store for a workflow
    this.selectedInvocation = null;                              // Workflow run that is highlighted
    this.fileChooserDivName = "";                               // Name of the div that holds the filechooser
    this.fileChooser = new FileChooser();                       // File chooser for property editing
    this.datasetQueryEditor = new DatasetQueryEditor();         // For editing dataset queries
    this.datasetItemPicker = new DatasetItemPicker();           // For choosing dataset items
    this.quickView = null;                                      // Quick view panel
    this.resizeTimeout;
    this.width = 1040;
    this.paletteWidth = 190;
    this.height = 400;
    this.drawingWidth = 840;
    this.showPalette = true;
    this.showToolbar = true;
    this.enableEditor = true;
    this.allowSave = true;
    this.invocationsDialog = new DrawingInvocationsDialog();

    this.versionFetchCallback = function(o){
        if(!o.error){
        } else {
            $.jGrowl("Error fetching workflow versions: " + o.message);
        }
    };

    this.blockHelpFetchCallback = function(o){
        if(!o.error){
            var helpTextDiv = document.getElementById(this.wfeditor.helpDialogDivName + "_div");
            helpTextDiv.innerHTML = o.html;
        } else {
            helpTextDiv.innerHTML = "Error fetching help: " + o.message;
        }
    };

    this.blockVersionsFetchCallback = function(o){
        if(!o.error){
            this.wfeditor.buildBlockVersionsDialog(o, this.block, this.wfeditor);
        } else {
            $.jGrowl("Error fetching block versions: " + o.message);
        }
    };
}

WorkflowEditor.prototype.init = function(settings){
    // Copy settings if present
    if(settings.divName){
        this.editorDivName = settings.divName;
    }

    if(settings.width){
        this.width = settings.width;
    }

    if(settings.paletteWidth){
        this.paletteWidth = settings.paletteWidth;
    }
    
    if(settings.height){
        this.height = settings.height;
    }

    if(settings.showPalette!=null){
        this.showPalette = settings.showPalette;
    }

    if(settings.showToolbar!=null){
        this.showToolbar = settings.showToolbar;
    }

    if(settings.enableEditor!=null){
        this.enableEditor = settings.enableEditor;
    }
    
    this.drawingWidth = this.width - this.paletteWidth - 40;

    this.canvasDivName = this.editorDivName + "_drawing";
    this.editorDiv = document.getElementById(this.editorDivName);

    // Create the toolbar
    if(this.showToolbar){
        this.createToolbar();
    }
    
    // Create the drawing canvas
    var holderDiv = document.createElement("div");
    holderDiv.setAttribute("id", this.editorDivName + "_drawing_holder");
    holderDiv.setAttribute("class", "ui-corner-all wfcanvas");
    holderDiv.setAttribute("style", "overflow:auto; z-index:0; float:right; margin-top: 10px; margin-right: 10px; width:" + this.drawingWidth + "px; height:" + this.height + "px;margin-bottom:10px;");
    this.canvasDiv = document.createElement("canvas");
    this.canvasDiv.wfeditor = this;
    this.canvasDiv.setAttribute("id", this.canvasDivName);

    this.canvasDiv.setAttribute("width", this.drawingWidth);
    this.canvasDiv.setAttribute("height", this.height);
    this.drawing.minimumWidth = this.drawingWidth - 20;
    this.drawing.minimumHeight = this.height - 20;
    holderDiv.appendChild(this.canvasDiv);
    this.editorDiv.appendChild(holderDiv);

    $("#" + this.canvasDivName).droppable({
        drop: function(event, ui)
        {
            this.wfeditor.drawingEditor.addBlock(ui.draggable.attr('id'), event.originalEvent);
        }
    });

    // Create the execution window div
    var invocationsDiv = document.createElement("div");
    invocationsDiv.setAttribute("id", this.canvasDivName + "_invocations");
    this.canvasDiv.appendChild(invocationsDiv);
    this.invocationsDialog.documentId = "";
    this.invocationsDialog.init(this.canvasDivName + "_invocations");
    
    // Create the properties div
    var dialogDiv = document.createElement("div");
    this.dialogDivName = this.canvasDivName + "_properties";
    dialogDiv.setAttribute("id", this.dialogDivName);
    dialogDiv.setAttribute("style", "overflow: hidden;");
    //dialogDiv.setAttribute("class", "dialog");
    this.canvasDiv.appendChild(dialogDiv);

    var editor = this;
    $("#" + this.dialogDivName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 400,
        width: 800,
        modal: true,
        title: "Properties",
        drawing: this.drawing,
        buttons: {
            'Close': function()
            {
                $(this).dialog('close');
            }

        },
        close: function()
        {
            if(this.block.updateCaption){
                this.block.updateCaption();
            }
            
            if(this.block.updateName){
                this.block.updateName();
            }
            
            if(this.block.parentDrawing){
                if(!this.block.parentDrawing.dirty){
                    // Only change the dirty status if it has not been set
                    this.block.parentDrawing.dirty = this.block.properties.propertiesChanged;
                    
                }
                
                // Change performance data
                if(this.block.properties.propertiesChanged){
                    editor.dirtyPerformanceData();
                }                
                this.block.properties.listNonDefaultProperties();
                this.block.parentDrawing.repaint();
                
            } else if(this.block instanceof DrawingObject){
                if(!this.block.dirty){
                    // Only change the dirty status if it has not been set
                    this.block.dirty = this.properties.propertiesChanged;
                }
                
                // Change performance data
                if(this.properties.propertiesChanged){
                    editor.dirtyPerformanceData();
                }                
                this.properties.listNonDefaultProperties();
                this.block.repaint();
            }
        }, 
        resize: function(event, ui) {
            this.properties.resize();
        }
    });

    // Create the performance dialog div
    var performanceDiv = document.createElement("div");
    performanceDiv.setAttribute("id", this.editorDivName + "_performance");
    performanceDiv.setAttribute("class", "dialog");
    this.editorDiv.appendChild(performanceDiv);
    this.performanceDialog.init(this.editorDivName + "_performance");
    
    // Create the help div
    var helpDiv = document.createElement("div");
    this.helpDialogDivName = this.canvasDivName + "_help";
    helpDiv.setAttribute("id", this.helpDialogDivName);

    var helpTextDiv = document.createElement("div");
    helpTextDiv.setAttribute("id", this.helpDialogDivName + "_div");
    helpTextDiv.setAttribute("class", "helppanel");
    helpDiv.appendChild(helpTextDiv);
    this.editorDiv.appendChild(helpDiv);

    $("#" + this.helpDialogDivName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 300,
        width: 400,
        title: "Block Help",
        modal: true,
        buttons: {
            'Ok': function()
            {
                $(this).dialog('close');
            }

        },
        close: function()
        {

        }
    });

    // Create the output text div
    var outputDiv = document.createElement("div");
    this.outputDialogDivName = this.canvasDivName + "_output";
    outputDiv.setAttribute("id", this.outputDialogDivName);
    this.editorDiv.appendChild(outputDiv);

    // Create the file chooser div
    var chooserDiv = document.createElement("div");
    this.fileChooserDivName = this.canvasDivName + "_chooser";
    chooserDiv.setAttribute("id", this.fileChooserDivName);
    this.editorDiv.appendChild(chooserDiv);
    this.fileChooser.init(this.fileChooserDivName);
    
    // Create the dataset query editor div
    var datasetEditorDiv = document.createElement("div");
    datasetEditorDiv.setAttribute("id", this.canvasDivName + "_dataset_editor");
    datasetEditorDiv.setAttribute("class", "dialog");
    this.editorDiv.appendChild(datasetEditorDiv);
    this.datasetQueryEditor.init(this.canvasDivName + "_dataset_editor");
    
    // Create the dataset item picker div
    var datasetPickerDiv = document.createElement("div");
    datasetPickerDiv.setAttribute("id", this.canvasDivName + "_dataset_picker");
    datasetPickerDiv.setAttribute("class", "dialog");
    this.editorDiv.appendChild(datasetPickerDiv);
    this.datasetItemPicker.init(this.canvasDivName + "_dataset_picker");
    
    // Create the service version list div
    var serviceVersionDiv = document.createElement("div");
    this.serviceVersionDialogDivName = this.canvasDivName + "_versions";

    var serviceVersionContentsDiv = document.createElement("div");
    serviceVersionContentsDiv.setAttribute("id", this.serviceVersionDialogDivName + "_div");
    serviceVersionDiv.setAttribute("id", this.serviceVersionDialogDivName);
    serviceVersionDiv.appendChild(serviceVersionContentsDiv);
    this.editorDiv.appendChild(serviceVersionDiv);

    $("#" + this.serviceVersionDialogDivName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 300,
        width: 400,
        title: "Block Code Versions",
        modal: true,
        buttons: {
            'Ok': function()
            {
                $(this).dialog('close');
            }

        },
        close: function()
        {

        }
    });


    var canvas = this.initCanvas(this.canvasDiv);
    var isIE = this.isIE();
    this.drawing.isIE = isIE;
    this.drawing.editor = this.drawingEditor;
    this.drawingEditor.drawing = this.drawing;
    this.drawingEditor.wfeditor = this;
    this.drawingEditor.canvas = canvas;
    if(this.enableEditor){
        this.drawingEditor.setupMouse();
    }

    // Setup the fetcher
    var drawingFetchComplete = function(){
        this.wfeditor.listInvocations();
        if(this.useLatest){
            this.wfeditor.fetchWorkflowVersions(this.documentId);
        } else {
            this.wfeditor.fetchWorkflowVersions(this.documentId, this.versionId);
        }
    };

    var versionsFetchComplete = function(){
        if(this.wfeditor.showToolbar){
            var i;
            var versionJson;
            var versionText;
            var drawingCombo = document.getElementById(this.wfeditor.toolbarDivName + "_versions");
            while(drawingCombo.length>0){
                drawingCombo.remove(0);
            }
            for(i=0;i<this.drawingVersionList.length;i++){
                versionJson = this.drawingVersionList[i];
                versionText = versionJson.versionNumber + ": " + versionJson.timestamp;
                drawingCombo.options[i] = new Option(versionText, versionJson.versionId);
            }
            var versionIndex = this.wfeditor.fetcher.findVersionIndex(this.wfeditor.drawing.versionId);
            if(versionIndex!=-1){
                drawingCombo.selectedIndex = versionIndex;
            }
        }
    };

    this.fetcher.wfeditor = this;
    this.fetcher.fetchComplete = drawingFetchComplete;
    this.fetcher.versionsFetchComplete = versionsFetchComplete;
    this.fetcher.drawing = this.drawing;
    this.fetcher.editor = this.drawingEditor;

    // Setup the palette and fetcher
    if(this.showPalette){
        this.createPalette();
        this.paletteFetcher.wfeditor = this;
    }

    // Setup the saver
    var drawingSaveComplete = function(){
        this.editor.fetchWorkflowVersions(this.editor.saver.documentId, this.editor.saver.versionId);
    };
    drawingSaveComplete.editor = this;
    drawingSaveComplete.saver = this.saver;
    this.saver.editor = this;
    this.saver.drawing = this.drawing;
    this.saver.saveComplete = drawingSaveComplete;

    // Setup the execution manager
    var executionCallback = function(invocationId){
        this.wfeditor.invocationsDialog.autoNavigateId = invocationId;
        this.wfeditor.invocationsDialog.init(this.wfeditor.canvasDivName + "_invocations");
        this.wfeditor.invocationsDialog.showDialog(this.wfeditor.drawing.documentId);
    };
    this.executionManager.wfeditor = this;
    this.executionManager.drawing = this.drawing;
    this.executionManager.drawingExecuted = executionCallback;
    
    // Setup the block menu
    var editCmd = new DrawingContextMenuCommand("edit", function(block)
    {
        this.wfeditor.showBlockEditor(block);
    }, rewriteAjaxUrl("../../scripts/wfeditor/images/cog_edit.png"));
    editCmd.wfeditor = this;

    var versionCmd = new DrawingContextMenuCommand("version", function(block)
    {
        this.wfeditor.showBlockVersions(block);
    }, rewriteAjaxUrl("../../scripts/wfeditor/images/clock.png"));
    versionCmd.wfeditor = this;

    var helpCmd = new DrawingContextMenuCommand("help", function(block){
        this.wfeditor.showBlockHelp(block);
    }, rewriteAjaxUrl("../../scripts/wfeditor/images/help.png"));
    
    helpCmd.wfeditor = this;

    var deleteCmd = new DrawingContextMenuCommand("delete", function(block)
    {
        var drawing = block.parentDrawing;
        drawing.removeBlock(block);
        this.wfeditor.dirtyPerformanceData();
        drawing.repaint();
    }, rewriteAjaxUrl("../../scripts/wfeditor/images/cross.png"));
    deleteCmd.wfeditor = this;
    
    var performanceCmd = new DrawingContextMenuCommand("performance", function(block){
        this.wfeditor.showPerformanceDialog(block);
    }, rewriteAjaxUrl("../../scripts/wfeditor/images/chart_line.png"));
    performanceCmd.wfeditor = this;
    
    var makeInputCmd = new DrawingContextMenuCommand("makeinput", function(block){
        this.wfeditor.makeBlockIntoWorkflowInput(block);
    }, rewriteAjaxUrl("../../scripts/wfeditor/images/drive_go.png"));
    makeInputCmd.wfeditor = this;
    
    this.drawing.menu.addCommand(editCmd);
    this.drawing.menu.addCommand(makeInputCmd);
    this.drawing.menu.addCommand(versionCmd);
    this.drawing.menu.addCommand(helpCmd);
    if(typeof performanceUrl!="undefined"){
        this.drawing.menu.addCommand(performanceCmd);
    }
    this.drawing.menu.addCommand(deleteCmd);
};

/** Make a block into the workflow input */
WorkflowEditor.prototype.makeBlockIntoWorkflowInput = function(block){
    if(block.inputList.size()===0 && (block.properties.getPropertyByName("Source")!==null || block.properties.getPropertyByName("SourceFolder")!==null)){
        var blockName = block.name;
        this.drawing.getEditableProperties().getPropertyByName("ExternalDataSupported").value = true;
        this.drawing.getEditableProperties().getPropertyByName("ExternalBlockName").value = blockName;
        this.drawing.getEditableProperties().propertiesChanged = true;
        this.drawing.repaint();
    }
};

/** Fetch the palette from the server */
WorkflowEditor.prototype.fetchPalette = function(){
    if(this.showPalette){
        this.paletteFetcher.fetchPalette();
    }
};

WorkflowEditor.prototype.createPalette = function(){
    this.paletteDivName = this.editorDivName + "_palette";
    var paletteContainer = document.createElement("div");
    paletteContainer.setAttribute("id", this.editorDivName + "_paletteContainer");

    
    //paletteContainer.setAttribute("class", "scroll-pane");
    paletteContainer.setAttribute("style", "float: left; width: " + this.paletteWidth + "px; margin-top: 10px; margin-left: 10px; overflow: auto;");

    var palette = document.createElement("div");
    palette.setAttribute("id", this.paletteDivName);
    palette.setAttribute("class", "palette");
    //$('#' + this.paletteDivName + ' h3').next().toggle();
    //$('#' + this.editorDivName + "_paletteContainer").jScrollPane({scrollbarOnLeft:true});

    
    paletteContainer.appendChild(palette);
    this.editorDiv.appendChild(paletteContainer);

};

WorkflowEditor.prototype.createToolbar = function(){

    this.toolbarDivName = this.editorDivName + "_toolbar";
    var toolbarDiv = document.createElement("div");
    toolbarDiv.setAttribute("id", this.toolbarDivName);
    toolbarDiv.setAttribute("class", "navbar clearfix");

    var toolbarContainer = document.createElement("div");
    toolbarContainer.setAttribute("class", "navbar-inner");

    var toolbarMenu = document.createElement("ul");
    toolbarMenu.setAttribute("class", "nav clearfix");

    // Create a refresh services button
    var refreshServices = document.createElement("li");
    var refreshServicesButton = document.createElement("a");
    refreshServicesButton.setAttribute("id", this.toolbarDivName + "_refresh_palette");
    refreshServicesButton.innerHTML = "<i class='icomoon-loop2'></i>Refresh";
    refreshServices.appendChild(refreshServicesButton);
    toolbarMenu.appendChild(refreshServices);

    // Create save button
    if(this.allowSave)
    {
        var save = document.createElement("li");
        var saveButton = document.createElement("a");
        saveButton.setAttribute("id", this.toolbarDivName + "_save");
        saveButton.innerHTML = "<i class='icomoon-disk'></i>Save";
        save.appendChild(saveButton);
        toolbarMenu.appendChild(save);
    }

    // Create the run button
    var run = document.createElement("li");
    var runButton = document.createElement("a");
    runButton.setAttribute("id", this.toolbarDivName + "_run");
    runButton.setAttribute("class", "glyphicons play");
    runButton.innerHTML = "<i class='icomoon-play3'></i>Run";
    run.appendChild(runButton);
    toolbarMenu.appendChild(run);

    // Create the executions button
    var invocations = document.createElement("li");
    var invocationsButton = document.createElement("a");
    invocationsButton.setAttribute("id", this.toolbarDivName + "_invocations");
    invocationsButton.innerHTML = "<i class='icomoon-folder-open'></i>Invocations";
    invocations.appendChild(invocationsButton);
    toolbarMenu.appendChild(invocations);

    // Delete selected objects
    var deleteWF = document.createElement("li");
    var deleteWFButton = document.createElement("a");
    deleteWFButton.setAttribute("id", this.toolbarDivName + "_delete");
    deleteWFButton.innerHTML = "<i class='icomoon-remove'></i>Delete";
    deleteWF.appendChild(deleteWFButton);
    toolbarMenu.appendChild(deleteWF);

    // Zoom Out button
    var zoomOut = document.createElement("li");
    var zoomOutButton = document.createElement("a");
    zoomOutButton.setAttribute("id", this.toolbarDivName + "_zoomOut");
    zoomOutButton.setAttribute("class", "glyphicons zoom_out");
    zoomOutButton.innerHTML = "<i class='icomoon-zoomout'></i>Zoom Out";
    zoomOut.appendChild(zoomOutButton);
    toolbarMenu.appendChild(zoomOut);

    // Reset zoom button
    var resetZoom = document.createElement("li");
    var resetZoomButton = document.createElement("a");
    resetZoomButton.setAttribute("id", this.toolbarDivName + "_resetZoom");
    resetZoomButton.innerHTML = "<i class='icomoon-expand'></i>Reset Zoom";
    resetZoom.appendChild(resetZoomButton);
    toolbarMenu.appendChild(resetZoom);

    // Zoom In button
    var zoomIn = document.createElement("li");
    var zoomInButton = document.createElement("a");
    zoomInButton.setAttribute("id", this.toolbarDivName + "_zoomIn");
    zoomInButton.innerHTML = "<i class='icomoon-zoomin'></i>Zoom In";
    zoomIn.appendChild(zoomInButton);
    toolbarMenu.appendChild(zoomIn);

    // Properties button
    var properties = document.createElement("li");
    var propertiesButton = document.createElement("a");
    propertiesButton.setAttribute("id", this.toolbarDivName + "_properties");
    propertiesButton.innerHTML = "<i class='icomoon-cog2'></i>Properties";
    properties.appendChild(propertiesButton);
    toolbarMenu.appendChild(properties);

    // Create the versions list
    var version = document.createElement("div");
    version.setAttribute("class", "input-prepend");
    var versionLable = document.createElement("span");
    versionLable.setAttribute("class", "add-on");
    versionLable.innerHTML = "Version"

    var versionList = document.createElement("select");
    versionList.setAttribute("class", "");
    versionList.setAttribute("id", this.toolbarDivName + "_versions");

    version.appendChild(versionLable);
    version.appendChild(versionList);
    toolbarMenu.appendChild(version);

    toolbarDiv.appendChild(toolbarContainer);
    toolbarContainer.appendChild(toolbarMenu);
    this.editorDiv.appendChild(toolbarDiv);

    if(this.allowSave){
        //$("#" + this.toolbarDivName + "_save").button({icons:{primary: "ui-icon-disk"}});
        saveButton.wfeditor = this;
        saveButton.onclick = function(){
            this.wfeditor.saveDrawing();
        }
    }

    refreshServicesButton.wfeditor = this;
    refreshServicesButton.onclick = function(){
        this.paletteDivName = this.editorDivName + "_palette";
        var paletteContainer = document.getElementById(this.wfeditor.editorDivName + "_paletteContainer");
        paletteContainer.innerHTML = "";

        var palette = document.createElement("div");
        palette.setAttribute("id", this.wfeditor.paletteDivName);
        palette.setAttribute("class", "palette");
        paletteContainer.appendChild(palette);
        this.wfeditor.fetchPalette();
    }


    runButton.wfeditor = this;
    runButton.onclick = function(){
        this.wfeditor.runDrawing();
    };

    invocationsButton.wfeditor = this;
    invocationsButton.onclick = function(){
        this.wfeditor.invocationsDialog.init(this.wfeditor.canvasDivName + "_invocations");
        this.wfeditor.invocationsDialog.autoNavigateId = null;
        this.wfeditor.invocationsDialog.showDialog(this.wfeditor.drawing.documentId);
    }

    deleteWFButton.wfeditor = this;
    deleteWFButton.onclick = function(){
        this.wfeditor.drawing.deleteSelected();
        this.wfeditor.drawing.repaint();
    }

    zoomOutButton.wfeditor = this;
    zoomOutButton.onclick = function(){
        this.wfeditor.drawing.zoomOut();
    }

    resetZoomButton.wfeditor = this;
    resetZoomButton.onclick = function(){
        this.wfeditor.drawing.resetZoom();
    }

    zoomInButton.wfeditor = this;
    zoomInButton.onclick = function(){
        this.wfeditor.drawing.zoomIn();
    }

    versionList.wfeditor = this;
    versionList.onchange = function(){
        var versionId = this.value;
        var documentId = this.wfeditor.fetcher.documentId;
        this.wfeditor.loadDrawing(documentId, versionId);
    }

    propertiesButton.wfeditor = this;
    propertiesButton.onclick = function(){
        this.wfeditor.showDrawingEditor();
    }
}

WorkflowEditor.prototype.fetchPerformance = function(){
    this.performanceFetcher.modelDrawing();
};

WorkflowEditor.prototype.disablePerformance = function(){
    this.performanceFetcher.disableModelling();
};

WorkflowEditor.prototype.initCanvas = function(canvas){
  if (window.G_vmlCanvasManager && window.attachEvent && !window.opera)
  {
    canvas = window.G_vmlCanvasManager.initElement(canvas);
  }
  return canvas;
};

WorkflowEditor.prototype.isIE = function(){
  if (window.G_vmlCanvasManager && window.attachEvent && !window.opera) {
    return true;
  } else {
      return false;
  }
};

WorkflowEditor.prototype.listInvocations = function(){
    this.executionManager.fetchInvocations();
};

WorkflowEditor.prototype.fetchWorkflowVersions = function(){
    this.fetcher.fetchDrawingVersions();
};

WorkflowEditor.prototype.showDrawingEditor = function(){
    var properties = this.drawing.getEditableProperties();
    properties.editorDivName = this.dialogDivName;
    properties.displayProperties();
    var editorElement = document.getElementById(this.dialogDivName);
    editorElement.title = "Editing Workflow properties";
    editorElement.block = this.drawing;
    editorElement.properties = properties;
    $("#" + this.dialogDivName).dialog('open');
    properties.resize();
};

WorkflowEditor.prototype.showPerformanceDialog = function(block){
    if(typeof performanceUrl!="undefined"){
        this.performanceDialog.setBlock(block);
        this.performanceDialog.showSummary();
    }
};

WorkflowEditor.prototype.showBlockVersions = function(block){
    var blockVersionDiv = document.getElementById(this.serviceVersionDialogDivName + "_div");
    blockVersionDiv.innerHTML = "Fetching...";
    $('#' + this.serviceVersionDialogDivName).dialog('open');

    // Fetch the versions
    var url = rewriteAjaxUrl("../../servlets/workflow?method=listServiceVersions");

    var callData = {
            id: block.serviceId
        };

    var callString = JSON.stringify(callData);

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.blockVersionsFetchCallback,
      dataType: "json",
      wfeditor: this,
      block: block
    });
};

WorkflowEditor.prototype.buildBlockVersionsDialog = function(o, block, wfeditor) {
    var i;
    var version;
    var blockVersionDiv = document.getElementById(this.serviceVersionDialogDivName + "_div");
    var latest = document.createElement("a");
    latest.setAttribute("style", "cursor: pointer; padding-bottom: 10px;");
    latest.onclick = function(){
        block.useLatest = true;
        block.parentDrawing.repaint();
        $('#' + wfeditor.serviceVersionDialogDivName).dialog('close');
    };

    latest.appendChild(document.createTextNode("Use latest version"));

    var table = document.createElement("table");
    table.setAttribute("style", "width: 90%;");
    var tr;
    var td1, td2, td3, a, p;


    for(i=0;i<o.versionCount;i++){
        version = o.versions[i];
        tr = document.createElement("tr");

        // Version number
        td1 = document.createElement("td");
        td1.setAttribute("style", "width: 40px;");
        td1.appendChild(document.createTextNode(version.versionNumber));

        // Timestamp
        td2 = document.createElement("td");
        td2.appendChild(document.createTextNode(version.timestamp));

        // Select
        td3 = document.createElement("td");
        td3.setAttribute("style", "width: 75px;");
        a = document.createElement("a");

        a.onclick = function(){
            block.useLatest = false;
            block.versionId = this.version.id;
            block.versionNumber = this.version.versionNumber;
            block.parentDrawing.repaint();
            $('#' + wfeditor.serviceVersionDialogDivName).dialog('close');
        };
        
        a.version = version;
        a.appendChild(document.createTextNode("Select"));
        a.setAttribute("style", "cursor: pointer;");
        td3.appendChild(a);

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        table.appendChild(tr);
    }
    blockVersionDiv.innerHTML = "";
    blockVersionDiv.appendChild(latest);
    p = document.createElement("p");
    p.setAttribute("style", "margin-bottom: 10px;");
    blockVersionDiv.appendChild(p);
    blockVersionDiv.appendChild(table);
};

WorkflowEditor.prototype.showBlockHelp = function(block){
    var helpTextDiv = document.getElementById(this.helpDialogDivName + "_div");
    helpTextDiv.innerHTML = "Fetching...";
    $('#' + this.helpDialogDivName).dialog('open');

    var url = rewriteAjaxUrl("../../servlets/workflow?method=getWorkflowServiceHelp");


    var callData;
    if(block.useLatest){
        callData = {
            serviceId: block.serviceId
        };
    } else {
         callData = {
            serviceId: block.serviceId,
            versionId: block.versionId
        };
    }

    var callString = JSON.stringify(callData);

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.blockHelpFetchCallback,
      dataType: "json",
      wfeditor: this
    });
    
};

WorkflowEditor.prototype.showBlockEditor = function(block){
    var properties = block.properties;
    properties.propertiesChanged = false;
    properties.editorDivName = this.dialogDivName;
    properties.quickView = this.quickView;
    properties.displayProperties();
    properties.fileChooser = this.fileChooser;
    properties.datasetQueryEditor = this.datasetQueryEditor;
    properties.datasetItemPicker = this.datasetItemPicker;
    var editorElement = document.getElementById(this.dialogDivName);
    editorElement.title = "Editing: " + block.name;
    editorElement.block = block;
    editorElement.properties = properties;
    $("#" + this.dialogDivName).dialog('open');
    properties.resize();
};

WorkflowEditor.prototype.loadDrawing = function(documentId, versionId){
    if(versionId==null){
        this.fetcher.useLatest = true;
        this.fetcher.versionId = null;
    } else {
        this.fetcher.useLatest = false;
        this.fetcher.versionId = versionId;
    }
    this.fetcher.documentId = documentId;
    this.invocationsDialog.documentId = documentId;
    this.fetcher.getDrawing();
};

WorkflowEditor.prototype.createDrawing = function(){
    this.drawing.repaint();
    this.showDrawingEditor();
};

WorkflowEditor.prototype.saveDrawing = function(){
    this.saver.saveDrawing();
};

WorkflowEditor.prototype.addBlock = function(serviceId, top, left){
    if(serviceId!=null && serviceId!=""){
      var db = new DrawingBlock();
      db.label = "Fetching...";

      db.left = left;
      db.top = top;

     
      db.serviceId = serviceId;
      db.versionId = null;

      this.drawing.addBlock(db);
      this.dirtyPerformanceData();
      this.drawingEditor.repaint();
      db.fetchData();
    }
};

WorkflowEditor.prototype.dirtyPerformanceData = function(){
    // Change performance data
    if(typeof performanceUrl!="undefined"){
        this.performanceFetcher.dirtyData();
    }                                  
};

WorkflowEditor.prototype.runDrawing = function(){
    if(!this.drawing.dirty){
        this.executionManager.execute(this.drawing.documentId);
    } else {

        var wfe = this;
        var sc = function(){
            wfe.executionManager.execute(wfe.drawing.documentId);
        };
        
        this.saver.saveDrawing(sc);
    }
};

WorkflowEditor.prototype.resizeUI = function(width, height){
    var toolbarDiv = document.getElementById(this.toolbarDivName);
    this.height = height - 240;
    var paletteHeight = height - toolbarDiv.clientHeight - 240;
    
    // Set the Size of the palette
    //var paletteDiv = document.getElementById(this.editorDivName + "_paletteContainer");
    var paletteDiv = document.getElementById(this.paletteDivName);
    if(paletteDiv){
        $(paletteDiv).css('height', paletteHeight);

    }
    
    // Set the size of the drawing area

    this.drawingWidth = width - paletteDiv.clientWidth - 40;
    var drawingHolderDiv = document.getElementById(this.editorDivName + "_drawing_holder");
    if(drawingHolderDiv){
        $(drawingHolderDiv).css('height', paletteHeight);
        $(drawingHolderDiv).css('width', this.drawingWidth);
    }
    
    
    // Update the minimum width of the drawing
    clearTimeout(this.resizeTimeout);
    var d = this;
    this.resizeTimeout = setTimeout(function(){
        d.canvasDiv.setAttribute("width", d.drawingWidth - 10);
        d.canvasDiv.setAttribute("height", paletteHeight - 40);
        d.drawing.minimumWidth = d.drawingWidth - 25;
        d.drawing.minimumHeight = d.height - 45;        
        d.drawing.repaint();
    }, 100);

    // Update the push div needed for absolute positioning
    $('#positionPush').css('height',($('#drawingCanvas').css('height')));

    DrawingObject.prototype.repaint();
};

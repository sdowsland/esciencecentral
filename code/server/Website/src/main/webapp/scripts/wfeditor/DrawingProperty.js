// This file defines an editable property for a drawing block
function DrawingProperty(propertyName, propertyValue, propertyType, propertyDescription){
	this.name = propertyName;                   // Property name to display
	this.value = propertyValue;                 // Plain value if it is a simple property
	this.propertyType = propertyType;           // Property type text - same as XmlDataObjectFactory
        this.description = propertyDescription;     // Descriptive text for property
        this.exposedProperty = false;               // Is this property exposed externally
        this.exposedName = "";                      // Name of the exposed property
        this.defaultSupported = false;
        this.defaultPresent = false;
        this.defaultValue = null;
        this.containsNonDefaultValue = false;
        this.jsonValue = null;                      // Data storage for a complex property type.
        this.editorDiv = null;                      // Div that is used when this property has a full dialog editor
        this.parentList = null;                     // Property list containing this property
        this.options = null;                        // List of property
        this.STRING_TYPE = "String";
        this.BOOLEAN_TYPE = "Boolean";
        this.id = null;
        this.exposedNameInput = null;
        this.editDiv = null;
}

/** Get the text representation of this property */
DrawingProperty.prototype.getTextValue = function(){
    if(this.jsonValue!=null){
        if(this.jsonValue.textValue){
            return this.jsonValue.textValue;
        } else {
            return "ComplexType";
        }
    } else {
        return this.value;
    }
};

/** Check to see if this parameter has a non-default value */
DrawingProperty.prototype.hasNonDefaultValue = function(){
    if(this.propertyType==="String" || this.propertyType==="Integer" || this.propertyType==="Double" || this.propertyType==="Long" || this.propertyType==="Boolean"){
        // Simple property
        if(this.defaultSupported && this.defaultPresent){
            // Default is supported and present
            if(this.value!==undefined && this.defaultValue!==undefined){
                // Different to default
                if(this.value.toString()!==this.defaultValue.toString()){
                    this.containsNonDefaultValue = true;
                } else {
                    this.containsNonDefaultValue = false;
                }

            } else{
                this.containsNonDefaultValue = false;
            }
        } else {
            this.containsNonDefaultValue = false;
        }

    } else {
        if(this.propertyType==="Document" || this.propertyType==="Folder" || this.propertyType==="ServerObject"){
            // Folder, Document, ServerObject
            if(this.jsonValue){
                if(this.jsonValue.id && this.jsonValue.id!==""){
                    this.containsNonDefaultValue = true;
                } else {
                    this.containsNonDefaultValue = false;
                }
            } else {
                this.containsNonDefaultValue = false;
            }
            
        } else if(this.propertyType==="Project"){
            if(this.jsonValue){
                if(this.jsonValue.id && this.jsonValue.id!==0){
                    this.containsNonDefaultValue = true;
                } else {
                    this.containsNonDefaultValue = false;
                }
            } else {
                this.containsNonDefaultValue = false;
            }
            
        } else if(this.propertyType==="DatasetItem"){
            if(this.jsonValue){
                if(this.jsonValue.datasetId && this.jsonValue.datasetId!=="" && this.jsonValue.itemName && this.jsonValue.itemName!==""){
                    this.containsNonDefaultValue = true;
                } else {
                    this.containsNonDefaultValue = false;
                }
                    
            } else {
                this.containsNonDefaultValue = false;
            }
            
        } else if(this.propertyType==="StringList"){
            if(this.jsonValue){
                if(this.jsonValue.valueCount>0){
                    this.containsNonDefaultValue = true;
                } else {
                    this.containsNonDefaultValue = false;
                }
            } else {
                this.containsNonDefaultValue = false;
            }
            
        } else if(this.propertyType==="TwoColumnList"){
            if(this.jsonValue){
                if(this.jsonValue.valueCount>0){
                    this.containsNonDefaultValue = true;
                } else {
                    this.containsNonDefaultValue = false;
                }
            } else {
                this.containsNonDefaultValue = false;
            }
            
        } else {
            // Complex property
            this.containsNonDefaultValue = false;
            
        }
    }    
    
    if(this.titleText){
        if(this.containsNonDefaultValue){
            $(this.titleText).css("font-weight", "bold");
        } else {
            $(this.titleText).css("font-weight", "normal");
        }
    }
};

/** Get the drawing containing this property */
DrawingProperty.prototype.getDrawing = function(){
    if(this.parentList){
        return this.parentList.getDrawing();
    } else {
        return null;
    }
};

/** Create a table row for this property */
DrawingProperty.prototype.createHtmlTableRow = function() {
    var html;
    var tr;
    var td1;
    var td2;
    var td3;
    var td4 = document.createElement("td");
    var select;
    var option;
    var input;
    var editText;
    var style = "";
    var disabledText;
    var helpDiv;
    var workflowDiv = null;
    var i;
    var index;
    
    if(this.getDrawing()){
        // Check reports status
        if(this.getDrawing().displayingReports){
            disabledText = "DISABLED";
        } else {
            disabledText = "";
        }
        
        // Check external service status
        if(this.getDrawing().isExternalService() && this.parentList && this.parentList.parentObject instanceof DrawingBlock){
            var publicOption = document.createElement("select");
            publicOption.setAttribute("style", "width: 120px;")
            
            var op1 = document.createElement("option");
            op1.setAttribute("name", "Exposed");
            op1.appendChild(document.createTextNode("Exposed As"));
            
            
            var op2 = document.createElement("option");
            op2.setAttribute("name", "Hidden");
            op2.appendChild(document.createTextNode("Hidden"));
            
            publicOption.appendChild(op1);
            publicOption.appendChild(op2);
            
            this.exposedNameInput = document.createElement("input");
            this.exposedNameInput.setAttribute("type", "text");
            this.exposedNameInput.setAttribute("value", this.exposedName);
            this.exposedNameInput.setAttribute("style", "width: 180px;");
            this.exposedNameInput.property = this;
            this.exposedNameInput.onchange = this.changeExposedName;

            td4.appendChild(publicOption);
            td4.appendChild(this.exposedNameInput);
            
            // Set correct settings
            if(this.exposedProperty){
                publicOption.selectedIndex = 0;
            } else {
                publicOption.selectedIndex = 1;
            }
            
            publicOption.onchange = this.changeExposed;
            publicOption.property = this;           
            
            this.changeExposed.call(publicOption, null, true);
        }
    } else {
        disabledText = "";
    }
    if(this.jsonValue===null){

        if(this.propertyType==="String" || this.propertyType==="Integer" || this.propertyType==="Double" || this.propertyType==="Long"){
           if(this.options){
               // Have some options for a dropdown list
               tr = document.createElement("tr");
               td1 = this.createTitleTd(this.name);
               td2 = document.createElement("td");
               td3 = document.createElement("td");
               td3.setAttribute("style", "width: 20px; vertical-align: top;");

               select = document.createElement("select");
               for(i=0;i<this.options.length;i++){
                   option = document.createElement("option");
                   option.setAttribute("id", "option_selector");
                   option.setAttribute("name", this.options[i]);
                   option.appendChild(document.createTextNode(this.options[i]));
                   select.appendChild(option);
               }
               select.onchange = this.changeProperty;
               select.property = this;
               index = this.options.indexOf(this.value);
               if(index!==-1){
                   select.selectedIndex = index;
               }
               helpDiv = document.createElement("div");
               helpDiv.setAttribute("class", "property-info");
               helpDiv.property = this;
               helpDiv.title = this.description;

               td2.appendChild(select);
               td3.appendChild(helpDiv);
               tr.appendChild(td1);
               tr.appendChild(td2);
               tr.appendChild(td3);
               tr.appendChild(td4);
               return tr;
               
           } else {
                // Simple editor
               tr = document.createElement("tr");
               td1 = this.createTitleTd(this.name);
               td2 = document.createElement("td");
               td3 = document.createElement("td");
               td3.setAttribute("style", "width: 20px; vertical-align: top;");

               input = document.createElement("input");
               input.setAttribute("type", "text");
               input.setAttribute("name", this.name);
               input.setAttribute("value", this.value);
               input.setAttribute("title", this.description);
               input.setAttribute("class","ui-corner-all property-input");
               input.property = this;
               input.onchange = this.changeProperty;

               helpDiv = document.createElement("div");
               helpDiv.setAttribute("class", "property-info");
               helpDiv.property = this;
               helpDiv.title = this.description;

               td2.appendChild(input);
               td3.appendChild(helpDiv);
               tr.appendChild(td1);
               tr.appendChild(td2);
               tr.appendChild(td3);
               tr.appendChild(td4);
               return tr;
           }

        } else if(this.propertyType==="Boolean"){
            // Checkbox
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "width: 20px; vertical-align: top;");
           
           input = document.createElement("input");
           input.setAttribute("type", "checkbox");
           input.setAttribute("name", this.name);
           //input.setAttribute("value", this.value);
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all higherButton");
           if(this.value===true || this.value==="true"){
               input.checked = true;
           } else {
               input.checked = false;
           }
           input.property = this;
           input.onchange = this.changeCheckbox;

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;
           td3.appendChild(helpDiv);

           td2.appendChild(input);
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;


        } else {
           tr = document.createElement("tr");
           td1 = document.createElement("td");
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "vertical-align: top;");
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;
        }

        //return html;

    } else {
        html='<tr><td>' + this.name +'</td>' ;
        if(this.propertyType==="Document"){
            // Document
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "width: 75px; vertical-align: top;");
           
           input = document.createElement("input");
           input.setAttribute("type", "text");
           input.setAttribute("name", this.name);
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all property-input");
           if(this.jsonValue.name){
               input.setAttribute("value", this.jsonValue.name);
               input.setAttribute("data-id", this.jsonValue.id);
           } else {
               input.setAttribute("value", "Empty");
           }
           input.property = this;
           input.onmousedown = this.changeFile;  //mousedown as Chrome lets this field be editable

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;

            if(this.jsonValue.name && /[^.]+$/.exec(this.jsonValue.name)[0].toLowerCase() === 'wf') {
                workflowDiv = document.createElement("div");
                workflowDiv.property = this;
                workflowDiv.setAttribute("class", "property-workflow");
                workflowDiv.setAttribute("data-workflow-id", this.jsonValue.id);
                workflowDiv.setAttribute("title", "View " + this.jsonValue.name + " workflow");
                workflowDiv.onclick = this.viewWorkflow;
            }
           
           td2.appendChild(input);
           this.editDiv = document.createElement("div");
           td3.appendChild(helpDiv);
           td3.appendChild(this.editDiv);

            if(workflowDiv) {
                td3.appendChild(workflowDiv);
            }
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           this.setupEditDiv();
           return tr;

        } else if(this.propertyType==="Folder"){
            // Create an editor for a folder
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "vertical-align: top;");

            input = document.createElement("input");
           input.setAttribute("type", "text");
           input.setAttribute("name", this.name);
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all property-input");
           if(this.jsonValue.name){
               input.setAttribute("value", this.jsonValue.name);
           } else {
               input.setAttribute("value", "Empty");
           }
           input.property = this;
           input.onmousedown = this.changeFolder;   //mousedown as Chrome lets this field be editable

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;

           td2.appendChild(input);
           td3.appendChild(helpDiv);
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;

            //style = "color: black; text-decoration: underline; cursor: pointer;";


        } else if(this.propertyType==="StringList"){
            // Create a viewer for a string list
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "vertical-align: top;");

           input = document.createElement("input");
           input.setAttribute("type", "text");
           input.setAttribute("value", "List(" + this.jsonValue.valueCount + ")");
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all property-input");
           input.onclick = this.changeStringList;
           input.property = this;

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;

           td3.appendChild(helpDiv);
           td2.appendChild(input);
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;

        } else if(this.propertyType==="TwoColumnList"){
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "vertical-align: top;");

           input = document.createElement("input");
           input.setAttribute("type", "text");
           input.setAttribute("value", "List(" + this.jsonValue.valueCount + ", 2)");
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all property-input");
           input.onmousedown = this.changeTwoColumnList;
           input.property = this;

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;

           td2.appendChild(input);
           td3.appendChild(helpDiv);
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;
        
        } else if(this.propertyType==="ServerObject"){
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "vertical-align: top;");
           
           input = document.createElement("input");
           input.setAttribute("type", "text");
           if(this.jsonValue.name){
               input.setAttribute("value", this.jsonValue.name);
           } else {
               input.setAttribute("value", "Empty");
           }
           
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all");
           input.onmousedown = this.changeServerObject;
           input.property = this;

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;

           td2.appendChild(input);
           td3.appendChild(helpDiv);
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;
       
        } else if(this.propertyType==="Project"){
           tr = document.createElement("tr");
           td1 = this.createTitleTd(this.name);
           td2 = document.createElement("td");
           td3 = document.createElement("td");
           td3.setAttribute("style", "vertical-align: top;");
           
           input = document.createElement("input");
           input.setAttribute("type", "text");
           if(this.jsonValue.name){
               input.setAttribute("value", this.jsonValue.name);
           } else {
               input.setAttribute("value", "Empty");
           }
           
           input.setAttribute("title", this.description);
           input.setAttribute("class","ui-corner-all");
           input.onmousedown = function(){
               this.property.changeServerObject("../../servlets/workflow?method=listProjectObjects");
           };
           input.property = this;

           helpDiv = document.createElement("div");
           helpDiv.setAttribute("class", "property-info");
           helpDiv.property = this;
           helpDiv.title = this.description;

           td2.appendChild(input);
           td3.appendChild(helpDiv);
           tr.appendChild(td1);
           tr.appendChild(td2);
           tr.appendChild(td3);
           tr.appendChild(td4);
           return tr;
           
        } else if(this.propertyType==="DatasetItem"){
            // Dataset item reference 
            tr = document.createElement("tr");
            td1 = this.createTitleTd(this.name);
            td2 = document.createElement("td");
            td3 = document.createElement("td");
            td3.setAttribute("style", "width: 20px; vertical-align: top;");

            input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", this.name);
            if(this.jsonValue && this.jsonValue.itemName){
                input.setAttribute("value", this.jsonValue.itemName);
            } else {
                input.setAttribute("value", "DatasetItem");
            }
            input.setAttribute("title", this.description);
            input.setAttribute("class","ui-corner-all property-input");
            input.property = this;
            input.onclick = this.changeDatasetItem;

            helpDiv = document.createElement("div");
            helpDiv.setAttribute("class", "property-info");
            helpDiv.property = this;
            helpDiv.title = this.description;

            td2.appendChild(input);
            td3.appendChild(helpDiv);
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            return tr;
            
        } else if(this.propertyType=="DatasetQuery"){
            // Dataset query
            tr = document.createElement("tr");
            td1 = this.createTitleTd(this.name);
            td2 = document.createElement("td");
            td3 = document.createElement("td");
            td3.setAttribute("style", "width: 20px; vertical-align: top;");

            input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", this.name);
            input.setAttribute("value", "DatasetQuery");
            input.setAttribute("title", this.description);
            input.setAttribute("class","ui-corner-all property-input");
            input.property = this;
            input.onclick = this.changeDatasetQuery;

            helpDiv = document.createElement("div");
            helpDiv.setAttribute("class", "property-info");
            helpDiv.property = this;
            helpDiv.title = this.description;

            td2.appendChild(input);
            td3.appendChild(helpDiv);
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            return tr;
               
        } else {
            html+='<td>Object</td><td></td>'
        }
        html+="</tr></td>"
        //return html;
           tr = document.createElement("tr");
           td1 = document.createElement("td");
           td2 = document.createElement("td");
           tr.appendChild(td1);
           tr.appendChild(td2);
           return tr;
    }
    
    
};

/** Setup the edit div to reflect what is present */
DrawingProperty.prototype.setupEditDiv = function(){
    // Special case for workflow
    if (this.jsonValue && this.jsonValue.className && this.jsonValue.className==="com.connexience.server.model.workflow.WorkflowDocument") {
        this.editDiv.property = this;
        this.editDiv.setAttribute("class", "property-popupedit");
        this.editDiv.onclick = function() {
            if (this.property && this.property.jsonValue && this.property.jsonValue.id) {
                window.open("../../pages/workflow/wfeditor.jsp?id=" + this.property.jsonValue.id);
            }
        };

    } else {
        this.editDiv.property = this;
        this.editDiv.setAttribute("class", "property-view");
        this.editDiv.onclick = this.viewFile;
    }
};

/** Override toString to produce name: value display */
DrawingProperty.prototype.toString = function(){
    if(this.propertyType==="String" || this.propertyType==="Integer" || this.propertyType==="Double" || this.propertyType==="Long"){
        return this.name + ": " + this.value;
        
    } else if(this.propertyType==="Boolean"){
        return this.name + ": " + this.value;
        
    } else if(this.propertyType==="TwoColumnList"){
        if(this.jsonValue && this.jsonValue.valueCount){
            return this.name + ": " + this.jsonValue.valueCount + "x2";
        } else {
            return this.name + ": Empty";
        }

        
    } else if(this.propertyType==="StringList"){
        if(this.jsonValue && this.jsonValue.valueCount){
            return this.name + ": " + this.jsonValue.valueCount + "x1";
        } else {
            return this.name + ": Empty";
        }
        

    } if (this.jsonValue){
        // Complex property
        if(this.jsonValue.name){
            return this.name + ": " + this.jsonValue.name;
            
        } else if(this.jsonValue.itemName){
            return this.name + ": " + this.jsonValue.itemName;
            
        } else {
            return this.name + ": Empty";
        }
        
    } else {
        return "Unknown";
    }
};

/** Create the title value for a row */
DrawingProperty.prototype.createTitleTd = function(title){
    var td = document.createElement("td");
    td.appendChild(document.createTextNode(title));
    if(this.containsNonDefaultValue){
        $(td).css("font-weight", "bold");
    } else {
        $(td).css("font-weight", "normal");
    }
    this.titleText = td;
    return td;
};

/** Insert a blank entry in the Json value. The behaviour of this method
 * varies depending on what type of property this is */
DrawingProperty.prototype.insertNewValueAt = function(index){
    var size;
    var i;
    var count = 0;

    if(this.propertyType==="StringList"){
        // Insert a new empty string at the specified location
        size = this.jsonValue.valueCount;
        if(size>0){
            var temp = new Array();

            for(i=0;i<(size + 1);i++){
                if(i!=(index+1)){
                    temp[i] = this.jsonValue.values[count];
                    count++;
                } else {
                    temp[i] = "";
                }
            }

            this.jsonValue.valueCount = size + 1;
            this.jsonValue.values = temp;
        } else {
            this.jsonValue.valueCount = 1;
            this.jsonValue.values[0] = "";
        }
    
    } else if(this.propertyType==="TwoColumnList"){
        // Insert new row
        size = this.jsonValue.valueCount;
        if(size>0){
            var tempc1 = new Array();
            var tempc2 = new Array();

            for(i=0;i<(size + 1);i++){
                if(i!=(index+1)){
                    tempc1[i] = this.jsonValue.column1[count];
                    tempc2[i] = this.jsonValue.column2[count];
                    count++;
                } else {
                    tempc1[i] = "";
                    tempc2[i] = "";
                }
            }

            this.jsonValue.valueCount = size + 1;
            this.jsonValue.column1 = tempc1;
            this.jsonValue.column2 = tempc2;
        } else {
            this.jsonValue.valueCount = 1;
            this.jsonValue.column1[0] = "";
            this.jsonValue.column2[0] = "";
        }
    }
}

/** Remove an entry from this JSON value */
DrawingProperty.prototype.removeValueAt = function(index){
    var size;
    var i;
    var count = 0;

    if(this.propertyType==="StringList"){
        // Remove string at the specified location
        size = this.jsonValue.valueCount;
        if(size>0){
            var temp = new Array();

            for(i=0;i<size;i++){
                if(i!=(index)){
                    temp[count] = this.jsonValue.values[i];
                    count++;
                }
            }

            this.jsonValue.valueCount = size - 1;
            this.jsonValue.values = temp;
        }

    } else if(this.propertyType==="TwoColumnList"){
        // Remove string at the specified location
        size = this.jsonValue.valueCount;
        if(size>0){
            var tempc1 = new Array();
            var tempc2 = new Array();

            for(i=0;i<size;i++){
                if(i!=(index)){
                    tempc1[count] = this.jsonValue.column1[i];
                    tempc2[count] = this.jsonValue.column2[i];
                    count++;
                }
            }

            this.jsonValue.valueCount = size - 1;
            this.jsonValue.column1 = tempc1;
            this.jsonValue.column2 = tempc2;
        }

    }
}

DrawingProperty.prototype.changeProperty = function(){
    var property = this.property;
    var drawing = property.getDrawing();
    property.parentList.propertiesChanged = true;
    if(drawing==null){
        property.value = this.value;
    } else if(!drawing.displayingReports){
        property.value = this.value;
    }
    this.property.hasNonDefaultValue();
};

DrawingProperty.prototype.changeCheckbox = function(){
    var property = this.property;
    property.parentList.propertiesChanged = true;
    var drawing = property.getDrawing();
    if(drawing==null){
        if(this.checked){
            property.value = true;
        } else {
            property.value = false;
        }
    } else if(!drawing.displayingReports){
        if(this.checked){
            property.value = true;
        } else {
            property.value = false;
        }
    }
    this.property.hasNonDefaultValue();
};

DrawingProperty.prototype.viewFile = function(){
    var property = this.property;
    
    if(property.parentList.quickView && property.jsonValue){
        qv.showFile(property.jsonValue.id, property.jsonValue.name);
    }
};

/** View workflow in new tab **/

DrawingProperty.prototype.viewWorkflow = function(){
    var wfid = this.getAttribute('data-workflow-id');

    if (wfid) {
        window.open(location.pathname + '?id=' + wfid);
    }

};


/** Show a folder chooser div */
DrawingProperty.prototype.changeFolder = function(){
    var property = this.property;
    property.parentList.propertiesChanged = true;
    property.hasNonDefaultValue();
    if(property.parentList.fileChooser){
        property.parentList.fileChooser.okCallback = property.folderSelected;
        property.parentList.fileChooser.property = property;
        property.parentList.fileChooser.showHomeFolder();
    } else {
        var divName = property.parentList.editorDivName;
        var filetree = property.parentList.filetree;
        var div = document.getElementById(divName);
        if(div){
            div.innerHTML = "";
            filetree.fileSelectCallback = null;
            filetree.property = property;
            filetree.folderSelectCallback = property.folderSelected;
            filetree.init(divName);
            filetree.showHomeFolder();

        }
    }
};

/** Show the dataset item picker */
DrawingProperty.prototype.changeDatasetItem = function(){
    var property = this.property;
    
    if(property.parentList.datasetItemPicker){
        property.parentList.datasetItemPicker.okCallback = function(itemJson){
            property.parentList.propertiesChanged = true;
            property.jsonValue = {
                datasetId: itemJson.datasetId,
                itemName: itemJson.itemName
            };
            property.hasNonDefaultValue();
            property.parentList.displayProperties();
        };
        
        property.parentList.datasetItemPicker.editDatasetItem(this.property.jsonValue);
    }        
};

/** Show the dataset query editor */
DrawingProperty.prototype.changeDatasetQuery = function(){
    var property = this.property;
    
    if(property.parentList.datasetQueryEditor){
        property.parentList.datasetQueryEditor.okCallback = function(query){
            property.parentList.propertiesChanged = true;
            if(query){
                property.jsonValue = $.extend({}, query);
            }
            property.hasNonDefaultValue();
        };
        
        property.parentList.datasetQueryEditor.editQuery(this.property.jsonValue);
    }    
};

/** Show a file chooser div */
DrawingProperty.prototype.changeFile = function(){
    var property = this.property;
    property.parentList.propertiesChanged = true;
    property.hasNonDefaultValue();
    if(property.parentList.fileChooser){
        property.parentList.fileChooser.okCallback = property.fileSelected;
        property.parentList.fileChooser.property = property;
        property.parentList.fileChooser.showHomeFolder();
    } else {
        var divName = property.parentList.editorDivName;
        var filetree = property.parentList.filetree;
        var div = document.getElementById(divName);
        if(div){
            div.innerHTML = "";
            div.style.overflow = "auto";
            filetree.folderSelectCallback = null;
            filetree.property = property;
            filetree.fileSelectCallback = property.fileSelected;
            filetree.init(divName);
            filetree.showHomeFolder();

        }
    }
};

/** Update a folder */
DrawingProperty.prototype.folderSelected = function(id, name){
    if(id instanceof FileChooser){
        if(this.property){
            var folderId = id.selectedFolderId;
            var folderName = id.selectedFolderName;
            var className = id.selectedClassName;
            
            if(folderId){
                var newJson = {
                    id: folderId,
                    name: folderName,
                    className: className,
                    description: ""
                };
                this.property.jsonValue = newJson;
                this.property.hasNonDefaultValue();
                this.property.parentList.displayProperties();
            }
        }             
    } else {
        if(this.property){
            var newJson = {
                id: id,
                name: name,
                description: ""
            };
            this.property.jsonValue = newJson;
            this.property.hasNonDefaultValue();
            this.property.parentList.displayProperties();
        }   
    }
};

/** Folder selected callback */
DrawingProperty.prototype.fileSelected = function(id, name){
    if(id instanceof FileChooser){
        if(this.property){
            var fileId = id.selectedFileId;
            var fileName = id.selectedFileName;   
            var className = id.selectedClassName;
            if(fileId){
                var newJson = {
                    id: fileId,
                    name: fileName,
                    className: className,
                    description: ""
                };
                this.property.jsonValue = newJson;
                this.property.hasNonDefaultValue();
                this.property.parentList.displayProperties();
                this.property.setupEditDiv();
            }
        }            
    } else {
        if(this.property){
            var newJson = {
                id: id,
                name: name,
                description: ""
            };
            this.property.jsonValue = newJson;
            this.property.hasNonDefaultValue();
            this.property.parentList.displayProperties();
        }      
    }
};


/** Change a single or double column string list */
DrawingProperty.prototype.changeStringList = function(){
    var property;
    if(this.property){
        property = this.property;
    } else {
        property = this;
    }
    property.parentList.propertiesChanged = true;
    property.hasNonDefaultValue();
    var divName = property.parentList.editorDivName;
    var div = document.getElementById(divName);
    div.innerHTML = "";

    var backLink = document.createElement("div");
    backLink.appendChild(document.createTextNode("Back"));
    backLink.setAttribute("style", "color: black; text-decoration: underline; cursor: pointer; margin-bottom: 10px;");
    backLink.property = property;
    backLink.onclick = property.backClicked;
    div.appendChild(backLink);

    var table = document.createElement("table");
    table.setAttribute("cellspacing", "5");
    var i;
    var item;
    var size = property.jsonValue.valueCount;
    var tr;
    var td1,td2,td3;
    var input;
    var addImg, delImg;

    if(size>0){
        for(i=0;i<size;i++){
            item = property.jsonValue.values[i];
            tr = document.createElement("tr");
            td1 = document.createElement("td");
            td2 = document.createElement("td");
            td2.style.verticalAlign = "middle";
            td3 = document.createElement("td");
            td3.style.verticalAlign = "middle";
            
            // Row data input
            input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("size", "20");
            input.setAttribute("value", item);
            input.rowIndex = i;
            input.property = property;
            input.onchange = function(){
                this.property.jsonValue.values[this.rowIndex] = this.value;
            };
            td1.appendChild(input);

            addImg = document.createElement("img");
            addImg.setAttribute("src", rewriteAjaxUrl("../../scripts/wfeditor/images/plus.png"));
            addImg.rowIndex = i;
            addImg.property = property;
            addImg.style.marginBottom = "0px";
            addImg.onclick = function(){
                this.property.insertNewValueAt(this.rowIndex);
                this.property.hasNonDefaultValue();
                this.property.changeStringList();
            };
            
            td2.appendChild(addImg);

            delImg = document.createElement("img");
            delImg.setAttribute("src", rewriteAjaxUrl("../../scripts/wfeditor/images/cross.png"));
            delImg.rowIndex = i;
            delImg.property = property;
            delImg.style.marginBottom = "0px";
            delImg.onclick = function(){
                this.property.removeValueAt(this.rowIndex);
                this.property.hasNonDefaultValue();
                this.property.changeStringList();
            }
            td3.appendChild(delImg);
            
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            table.appendChild(tr);
        }
    } else {
        tr = document.createElement("tr");

        td1 = document.createElement("td");
        td2 = document.createElement("td");
        td2.style.verticalAlign = "middle";
        td3 = document.createElement("td");

        td1.appendChild(document.createTextNode("Add new row"));

        addImg = document.createElement("img");
        addImg.setAttribute("src", rewriteAjaxUrl("../../scripts/wfeditor/images/plus.png"));
        addImg.rowIndex = i;
        addImg.property = property;
        addImg.style.marginBottom = "0px";
        addImg.onclick = function(){
            this.property.insertNewValueAt(0);
            this.property.changeStringList();
        };
        td2.appendChild(addImg);

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        table.appendChild(tr);
    }
    div.appendChild(table);
};

DrawingProperty.prototype.backClicked = function(){
    if(this.property){
        this.property.hasNonDefaultValue();
        this.property.parentList.displayProperties();
    } else if(this.parentList){
        if(this.parentList.hasNonDefaultValue){
            this.parentList.hasNonDefaultValue();
        }
        this.parentList.displayProperties();
    }
};

DrawingProperty.prototype.changeTwoColumnList = function(){
    var property;
    if(this.property){
        property = this.property;
    } else {
        property = this;
    }
    
    property.parentList.propertiesChanged = true;
    property.hasNonDefaultValue();
    var divName = property.parentList.editorDivName;
    var div = document.getElementById(divName);
    div.innerHTML = "";

    var backLink = document.createElement("div");
    backLink.appendChild(document.createTextNode("Back"));
    backLink.setAttribute("style", "color: black; text-decoration: underline; cursor: pointer; margin-bottom: 10px;");
    backLink.property = property;
    backLink.onclick = property.backClicked;
    div.appendChild(backLink);

    var table = document.createElement("table");
    table.setAttribute("cellspacing", "5");
    var i;
    var item1;
    var item2;
    var size = property.jsonValue.valueCount;
    var tr;
    var td1,td2,td3,td4;
    var input1, input2;
    var addImg, delImg;

    if(size>0){
        for(i=0;i<size;i++){
            item1 = property.jsonValue.column1[i];
            item2 = property.jsonValue.column2[i];
            tr = document.createElement("tr");
            td1 = document.createElement("td");
            td2 = document.createElement("td");
            td3 = document.createElement("td");
            td3.style.verticalAlign = "middle";
            td4 = document.createElement("td");
            td4.style.verticalAlign = "middle";

            // Column 1 data input
            input1 = document.createElement("input");
            input1.setAttribute("type", "text");
            input1.setAttribute("size", "10");
            input1.setAttribute("value", item1);
            input1.rowIndex = i;
            input1.property = property;
            input1.onchange = function(){
                this.property.jsonValue.column1[this.rowIndex] = this.value;
            };
            td1.appendChild(input1);

            // Column 2 data input
            input2 = document.createElement("input");
            input2.setAttribute("type", "text");
            input2.setAttribute("size", "10");
            input2.setAttribute("value", item2);
            input2.rowIndex = i;
            input2.property = property;
            input2.onchange = function(){
                this.property.jsonValue.column2[this.rowIndex] = this.value;
            };
            td2.appendChild(input2);

            addImg = document.createElement("img");
            addImg.setAttribute("src", rewriteAjaxUrl("../../scripts/wfeditor/images/plus.png"));
            addImg.rowIndex = i;
            addImg.property = property;
            addImg.style.marginBottom = "0px";
            addImg.onclick = function(){
                this.property.insertNewValueAt(this.rowIndex);
                this.property.hasNonDefaultValue();
                this.property.changeTwoColumnList();
            };

            td3.appendChild(addImg);

            delImg = document.createElement("img");
            delImg.setAttribute("src", rewriteAjaxUrl("../../scripts/wfeditor/images/cross.png"));
            delImg.rowIndex = i;
            delImg.property = property;
            delImg.style.marginBottom = "0px";
            delImg.onclick = function(){
                this.property.removeValueAt(this.rowIndex);
                this.property.hasNonDefaultValue();
                this.property.changeTwoColumnList();
            }
            td4.appendChild(delImg);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            table.appendChild(tr);
        }
    } else {
        tr = document.createElement("tr");

        td1 = document.createElement("td");
        td2 = document.createElement("td");
        td3 = document.createElement("td");
        td3.style.verticalAlign = "middle";
        td4 = document.createElement("td");

        td1.appendChild(document.createTextNode("Add new row"));

        addImg = document.createElement("img");
        addImg.setAttribute("src", rewriteAjaxUrl("../../scripts/wfeditor/images/plus.png"));
        addImg.rowIndex = i;
        addImg.property = property;
        addImg.style.marginBottom = "0px";
        addImg.onclick = function(){
            this.property.insertNewValueAt(0);
            this.property.hasNonDefaultValue();
            this.property.changeTwoColumnList();
        };
        td3.appendChild(addImg);

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);
        table.appendChild(tr);
    }
    div.appendChild(table);
};

DrawingProperty.prototype.changeServerObject = function(callUrl){
    var property;
    if(this.property){
        property = this.property;
    } else {
        property = this;
    }
    
    property.parentList.propertiesChanged = true;
    var divName = property.parentList.editorDivName;
    var div = document.getElementById(divName);
    div.innerHTML = "Fetching...";
    
    var cb = function(o){
        if(!o.error){
            // List the objects
            div.innerHTML = "";
            var backLink = document.createElement("div");
            backLink.appendChild(document.createTextNode("Back"));
            backLink.setAttribute("style", "color: black; text-decoration: underline; cursor: pointer; margin-bottom: 10px;");
            backLink.property = property;
            backLink.onclick = property.backClicked;
            div.appendChild(backLink);

            var table = document.createElement("table");
            table.setAttribute("cellspacing", "5");
            table.setAttribute("style", "width: 100%");
            table.innerHTML = '<thead style="text-align: left;"><tr><th>Name</th><th>Select</th></thead>';
            var tbody = document.createElement("tbody");
            
            var i;
            var item;
            var size = o.objects.length;
            var tr;
            var td1,td2, a;

            if(size>0){
                for(i=0;i<size;i++){
                    item = o.objects[i];
                    tr = document.createElement("tr");
                    td1 = document.createElement("td");
                    td2 = document.createElement("td");
                    td2.style.verticalAlign = "middle";

                    // Object name
                    td1.appendChild(document.createTextNode(item.name));

                    a = document.createElement("a");
                    a.appendChild(document.createTextNode("Select"));
                    a.setAttribute("style", "cursor: pointer;");
                    a.objectJson = item;
                    a.property = property;
                    a.onclick = function(){
                        this.property.jsonValue = this.objectJson;
                        this.property.hasNonDefaultValue();
                        this.property.backClicked();
                    };

                    td2.appendChild(a);


                    tr.appendChild(td1);
                    tr.appendChild(td2);
                    tbody.appendChild(tr);
                }

            }
            table.appendChild(tbody);
            div.appendChild(table);

        }
    };
    
    var callData = {};
    if(property.jsonValue && property.jsonValue.className){
        callData.className = property.jsonValue.className;
    }
    
    if(callUrl && !(callUrl instanceof MouseEvent)){
        // Use alternative call URL - this is to allow us to select other object types using the same dialog
        jsonCall(callData, rewriteAjaxUrl(callUrl), cb, null);
    } else {
        jsonCall(callData, rewriteAjaxUrl("../../servlets/workflow?method=listServerObjects"), cb, null);
    }
};

DrawingProperty.prototype.changeExposed = function(evt, ignoreDirty){
    if(this.selectedIndex===0){
        // Exposed
        if(this.property){
            this.property.exposedProperty = true;
            if(this.property.exposedNameInput){
                $(this.property.exposedNameInput).css("display", "inline-block");
            }
        }
        
    } else {
        // Not exposed
        if(this.property){
            this.property.exposedProperty = false;
            if(this.property.exposedNameInput){
                $(this.property.exposedNameInput).css("display", "none");
            }
        }
    }
    
    // This has come from a click
    if(ignoreDirty===undefined && this.property){
        this.property.parentList.propertiesChanged = true;
    }
};

DrawingProperty.prototype.changeExposedName = function(){
    var property = this.property;
    if(property){
        property.exposedName = this.value;
        property.parentList.propertiesChanged = true;
    }
};
/* 
 *.The javascript class manages the execution of the drawing loaded into the
 * drawing editor
 */

function DrawingExecutionManager(){
    this.drawing = null;                    // Drawing being managed;
    this.invocations = new List();          // List of invocations
    this.invocationWorkflowJsonFetched = null;
    this.invocationWorkflowJsonNotPresent = null;
    this.invocationsFetched = function(){};  // Callback function to call when the invocations have been fetched
    this.invocationFilesFetched = function(files){};    // Callback to call when invocation file list has been fetched
    this.invocationsFetchFailed = function(message){};   // Invocations fetching failed
    this.invocationFetched = function(invocation){};    // Single invocation has been fetched
    this.invocationReportsFetched = function(reports){};  // Reports have been fetched
    this.invocationDeleted = function(){};  // An invocation has been deleted
    this.invocationTerminated = function(message){};  // An invocation has been terminated
    this.drawingExecuted = function(){invocationId};
    this.executionSent = function(){};      // Callback when an invocation request is sent Ok
    this.executionSentFailed = function(message){};    // Invocation request could not be sent

    this.fetchCallback = function(o){
        if(!o.error){
            // Parse the invocation results and store them
            var invocationArray = o.invocations.invocationArray;
            var i;
            var invocationJson;
            var invocation;

            for(i=0;i<invocationArray.length;i++){
                invocationJson = invocationArray[i];
                invocation = new DrawingInvocation();
                invocation.parseJson(invocationJson);
                this.executionManager.invocations.add(invocation);
            }

            if(this.executionManager.invocationsFetched){
                this.executionManager.invocationsFetched();
            }

        } else {
            // Invocations fetch didn't work
            if(this.executionManager.invocationsFetchFailed){
                this.executionManager.invocationsFetchFailed(result.message);
            }
        }
    };

    this.fetchInvocationDrawingCallback = function(o){
        if(!o.error){
            if(this.executionManager.invocationWorkflowJsonFetched){
                this.executionManager.invocationWorkflowJsonFetched(o);
            }

        } else {
            if(this.executionManager.invocationWorkflowJsonNotPresent){
                this.executionManager.invocationWorkflowJsonNotPresent(o.status);
            }
        }
    };

    this.fetchInvocationsFilesCallback = function(o){
        if(!o.error){
            if(this.executionManager.invocationFilesFetched){
                this.executionManager.invocationFilesFetched(o);
            }
        } else {
            $.jGrowl("Error fetching invocation files: " + o.message);
        }
    };

    this.invocationFetchCallback = function(o){
        if(!o.error){
            if(this.executionManager.invocationFetched){
                this.executionManager.invocationFetched(o.invocation);
            }
        } else {
            $.jGrowl("Error fetching invocation data: " + o.message);
        }
    }

    this.invocationReportsCallback = function(o){
        if(!o.error){
            if(this.executionManager.invocationReportsFetched){
                this.executionManager.invocationReportsFetched(o);
            }
        } else {

        }
    }

    this.invocationDeleteCallback = function(o){
        if(!o.error){
            if(this.executionManager.invocationDeleted){
                this.executionManager.invocationDeleted();
            }
        } else {
            $.jGrowl("Error deleting invocation: " + o.message);
        }
    }

    this.executeCallback = function(o){
        if(!o.error){
            if(this.executionManager.drawingExecuted){
                this.executionManager.drawingExecuted(o.invocationId);
            }
        } else {
            $.jGrowl("Error running workflow: " + o.message);
        }
    }

    this.terminateCallback = function(o){
        if(!o.error){
            if(this.executionManager.invocationTerminated){
                this.executionManager.invocationTerminated(o.reports);
            }
        } else {
            $.jGrowl("Error killing workflow: " + o.message);
        }
    }
}

/** Clear the invocations list */
DrawingExecutionManager.prototype.clearInvocations = function(){
    this.invocations.clear();
};

/** Fetch all of the execution invocations for this drawing */
DrawingExecutionManager.prototype.fetchInvocations = function(){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=listInvocations");
    var callData = {workflowId: this.drawing.documentId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();
    
    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.fetchCallback,
      dataType: "json",
      executionManager: this
    });

};

/** Fetch all of the execution invocations of a specific drawing */
/** Fetch all of the execution invocations for this drawing */
DrawingExecutionManager.prototype.fetchInvocationsForDocumentId = function(documentId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=listInvocations");
    var callData = {workflowId: documentId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.fetchCallback,
      dataType: "json",
      executionManager: this
    });

};

/** Get the number of invocations */
DrawingExecutionManager.prototype.getInvocationCount = function(){
    return this.invocations.size();
};

/** Get an invocation by index */
DrawingExecutionManager.prototype.getInvocation = function(index){
    return this.invocations.get(index);
};

/** Get an invocation by ID */
DrawingExecutionManager.prototype.getInvocationById = function(id){
    var i;
    var invocation;
    for(i=0;i<this.invocations.size();i++){
        invocation = this.invocations.get(i);
        if(invocation.invocationId==id){
            return invocation;
        }
    }
    return null;
}

/** Get a table friendly data set */
DrawingExecutionManager.prototype.getTableData = function(){
    var dataArray = new Array(this.invocations.size());
    var i;
    for(i=0;i<this.invocations.size();i++){
        dataArray[i] = this.invocations.get(i).getTableRow(i + 1);
    }
    return dataArray;
};

/** Fetch data for a specific invocation */
DrawingExecutionManager.prototype.fetchInvocationWorkflowJson = function(invocationId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=fetchInvocationWorkflowData");
    var callData = {invocationId: invocationId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.fetchInvocationDrawingCallback,
      dataType: "json",
      executionManager: this
    });
};

/** Fetch files for a specific invocation */
DrawingExecutionManager.prototype.fetchInvocationFiles = function(invocationId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=listInvocationFiles");
    var callData = {invocationId: invocationId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.fetchInvocationsFilesCallback,
      dataType: "json",
      executionManager: this
    });
};

/** Fetch files for a specific invocation */
DrawingExecutionManager.prototype.fetchInvocationReports = function(invocationId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=fetchReports");
    var callData = {invocationId: invocationId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.invocationReportsCallback,
      dataType: "json",
      executionManager: this
    });
};

/** Fetch a specific workflow invocation */
DrawingExecutionManager.prototype.fetchInvocation = function(invocationId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=getWorkflowInvocation");
    var callData = {invocationId: invocationId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.invocationFetchCallback,
      dataType: "json",
      executionManager: this
    });
};

/** Delete an invocation */
DrawingExecutionManager.prototype.deleteInvocation = function(invocationId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=deleteInvocation");
    var callData = {id: invocationId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.invocationDeleteCallback,
      dataType: "json",
      executionManager: this
    });
};

DrawingExecutionManager.prototype.execute = function(workflowId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=executeWorkflow");
    var callData = {workflowId: workflowId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.executeCallback,
      dataType: "json",
      executionManager: this
    });
};

DrawingExecutionManager.prototype.executeWithDocumentId = function(workflowId, documentId){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=executeWorkflow");
    var callData = {workflowId: workflowId, documentId: documentId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.executeCallback,
      dataType: "json",
      executionManager: this
    });
};

/** Terminate a running invocation */
DrawingExecutionManager.prototype.terminateInvocation = function(invocation){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=terminateInvocation");
    var callData = {invocationId: invocation.invocationId};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.terminateCallback,
      dataType: "json",
      executionManager: this
    });
};

/** Terminate all invocations of a workflow */
DrawingExecutionManager.prototype.terminateAllInvocations = function(workflowId, callback){
    var callData = {
        workflowId: workflowId
    };
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback();
            }
        }
    };
    jsonCall(callData, "../../servlets/workflow?method=terminateAllInvocationsOfWorkflow", cb);
};


/** Resubmit an invocation message */
DrawingExecutionManager.prototype.resubmitInvocation = function(invocation, callback){
    var callData = {
        id: invocation.invocationId
    };

    var url = rewriteAjaxUrl("../../servlets/workflow?method=reRun");
    var callString = JSON.stringify(callData);

    var cb = function (o) {
        if (!o.error) {
            $.jGrowl("Workflow re-run with invocationId: " + o.invocationId);
            if(callback){
                callback();
            }
        } else {
            $.jGrowl("Error re-running workflow: " + o.message);
        }
    };

    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: cb,
        dataType: "json",
        manager: this
    });    
};

/** Fetch a list of running invocation counts */
DrawingExecutionManager.prototype.getInvocationCounts = function(workflowIds, callback){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=getInvocationCounts");
    var callData = {workflowIds: workflowIds};
    var callString = JSON.stringify(callData);
    this.invocations.clear();

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: callback,
      dataType: "json",
      executionManager: this
    });    
};

/** Create a trigger for a folder */
DrawingExecutionManager.prototype.createTrigger = function(workflowId, folderId, callback){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=createTrigger");
    var callData = {workflowId: workflowId, folderId: folderId};
    var callString = JSON.stringify(callData);
    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: callback,
      dataType: "json",
      executionManager: this
    });      
};

/** Create a trigger for a folder */
DrawingExecutionManager.prototype.deleteTrigger = function(triggerId, callback){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=deleteTrigger");
    var callData = {triggerId: triggerId};
    var callString = JSON.stringify(callData);
    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: callback,
      dataType: "json",
      executionManager: this
    });      
};

/** Get a trigger object */
DrawingExecutionManager.prototype.getTrigger = function(triggerId, callback){
    var cb = function(o){
        if(!o.error){
            callback(o.trigger);
        }
    }
    jsonCall({id: triggerId}, "../../servlets/workflow?method=getWorkflowTrigger", cb, null);
};

/** Save a trigger object */
DrawingExecutionManager.prototype.saveTrigger = function(triggerId, properties, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    }
    jsonCall({id: triggerId, properties: properties}, "../../servlets/workflow?method=saveWorkflowTrigger", cb, null);
};
/* 
 * This javascript object displays a table of current workflow invocations for
 * all workflows and lets the user kill running workflows etc.
 */
//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<link rel="stylesheet" type="text/css"href="../../scripts/datatables/css/datatable.css"/>
//<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
//<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/FolderViewer.js"></script>
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>

function WorkflowRuns() {
    this.tableDivName = "";
    this.folderViewer = new FolderViewer();
    this.invocationData = null;
}

/** Initialise with a table div */
WorkflowRuns.prototype.init = function (tableDivName) {
    this.tableDivName = tableDivName;
    var viewerDiv = document.createElement("div");
    viewerDiv.setAttribute("id", this.tableDivName + "_viewer");
    var table = document.getElementById(this.tableDivName);
    if (table) {
        table.appendChild(viewerDiv);
        this.folderViewer.init(this.tableDivName + "_viewer");
    }
};

/** Fetch the invocation data */
WorkflowRuns.prototype.fetchAllInvocations = function () {
    var callData = {
        filterSucceeded: false
    };

    var url = rewriteAjaxUrl("../../servlets/workflow?method=getInvocationTable");
    var callString = JSON.stringify(callData);

    var callback = function (o) {
        if (!o.error) {
            this.manager.displayList(o.invocations);
        } else {
            $.jGrowl("Error listing workflow runs: " + o.message);
        }
    };

    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: callback,
        dataType: "json",
        manager: this
    });
};

/** Kill all the invocations for the current user */
/** Kill all running workflows */
WorkflowRuns.prototype.killAllWorkflows = function(){
    var cb = function(o){
        if(!o.error){
            $.jGrowl("All workflows killed");
        };
    };
    jsonCall({}, "../../servlets/admin?method=killAllUserWorkflows", cb, null);
};

/** Fetch all of the running invocations */
WorkflowRuns.prototype.fetchRunningInvocations = function () {
    var callData = {
        filterSucceeded: true,
        filter: 1
    };

    var url = rewriteAjaxUrl("../../servlets/workflow?method=getInvocationTable");
    var callString = JSON.stringify(callData);

    var callback = function (o) {
        if (!o.error) {
            this.manager.displayList(o.invocations);
        } else {
            $.jGrowl("Error listing workflow runs: " + o.message);
        }
    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: callback,
        dataType: "json",
        manager: this
    });
};

/** Fetch queued invocations */
WorkflowRuns.prototype.fetchQueuedInvocations = function () {
    var callData = {
        filterSucceeded: true,
        filter: 0
    };

    var url = rewriteAjaxUrl("../../servlets/workflow?method=getInvocationTable");
    var callString = JSON.stringify(callData);

    var callback = function (o) {
        if (!o.error) {
            this.manager.displayList(o.invocations);
        } else {
            $.jGrowl("Error listing workflow runs: " + o.message);
        }
    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: callback,
        dataType: "json",
        manager: this
    });
};

WorkflowRuns.prototype.fetchFailedInvocations = function () {
    var callData = {
        filterSucceeded: true,
        filter: 3
    };

    var url = rewriteAjaxUrl("../../servlets/workflow?method=getInvocationTable");
    var callString = JSON.stringify(callData);

    var callback = function (o) {
        if (!o.error) {
            this.manager.displayList(o.invocations);
        } else {
            $.jGrowl("Error listing workflow runs: " + o.message);
        }
    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: callback,
        dataType: "json",
        manager: this
    });
};

/** Display the table of invocations */
WorkflowRuns.prototype.displayList = function (invocationData) {
    this.invocationData = invocationData;
    var table = document.getElementById(this.tableDivName);
    var tableId = this.tableDivName + "_table";
    var manager = this;
    table.innerHTML = '<table cellpadding="0" cellspacing="0" border="0" width="100%;" class="display" id="' + tableId + '"></table>';

    $('#' + tableId).dataTable({
        "bJQueryUI": true,
        "bDestroy": true,
        "aaData": invocationData,
        "fnDrawCallback": function(){
            manager.assignCallbacks();
        },        
        "aoColumns": [
            {
                "sTitle": "Status",
                "fnRender": function (obj) {
                    var status = obj.aData[obj.iDataColumn];
                    if (status == 0) {
                        return '<img src="' + rewriteAjaxUrl("../../scripts/wfeditor/images/hourglass.png") + '" alt="waiting"/>';
                    } else if (status == 1) {
                        return '<img src="' + rewriteAjaxUrl("../../scripts/wfeditor/images/cog.png") + '" alt="waiting"/>';
                    } else if (status == 2) {
                        return '<img src="' + rewriteAjaxUrl("../../scripts/wfeditor/images/tick.png") + '" alt="waiting"/>';
                    } else if (status == 3) {
                        return '<img src="' + rewriteAjaxUrl("../../scripts/wfeditor/images/cross.png") + '" alt="waiting"/>';
                    } else {
                        return '<img src="' + rewriteAjaxUrl("../../scripts/wfeditor/images/star.png") + '" alt="waiting"/>';
                    }
                }
            },
            {
                "sTitle": "Name",
                "fnRender": function (obj) {
                    var workflowId = obj.aData[5];
                    var name = obj.aData[obj.iDataColumn];
                    return '<a href="wfeditor.jsp?id=' + workflowId + '">' + name + '</a>';
                }
            },
            {"sTitle": "Date"},
            {
                "sTitle": "Results",
                "sClass": "center",
                "sWidth": "1px",
                "fnRender": function (obj) {
                    var invocationId = obj.aData[ obj.iDataColumn ];
                    var sReturn = '<a id="invocation_cell_' + invocationId + '">Files</a>';
                    return sReturn;
                }
            },
            {
                "sTitle": "Repeat",
                "sClass": "center",
                "sWidth": "1px",
                "fnRender": function (obj) {
                    var invocationId = obj.aData[ obj.iDataColumn ];
                    var sReturn = '<a id="invocation_cell_ex_' + invocationId + '" onclick="reExecute(\'' + invocationId + '\');">Re-Run</a>';
                    return sReturn;
                }
            },
            {
                "sTitle": "InvocationID",
                "bSearchable": false,
                "bVisible": false
            }
        ]
    });

    this.assignCallbacks();
};

WorkflowRuns.prototype.assignCallbacks = function(){
    // Add all the click handlers
    var i;
    var manager = this;
    var cell;
    var invocationId;

    for (i = 0; i < this.invocationData.length; i++) {
        invocationId = this.invocationData[i][3];

        cell = document.getElementById("invocation_cell_" + invocationId);
        if (cell) {
            var onclick = function () {
                manager.showFiles(this.invocationId);
            };
            cell.onclick = onclick;
            cell.invocationId = invocationId;
        }


    }    
};

function reExecute(invocationId){

    var callData = {
        id: invocationId
    };

    var url = rewriteAjaxUrl("../../servlets/workflow?method=reRun");
    var callString = JSON.stringify(callData);

    var callback = function (o) {
        if (!o.error) {
            $.jGrowl("Workflow re-run with invocationId: " + o.invocationId);
        } else {
            $.jGrowl("Error re-running workflow: " + o.message);
        }
    };

    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: callback,
        dataType: "json",
        manager: this
    });

}

WorkflowRuns.prototype.showFiles = function (id) {
    this.folderViewer.openFolder(id);
};

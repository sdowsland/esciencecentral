/* 
 * This script provides a basic dialog that displays the status of a workflow kill
 * operation
 */
//Requires
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>
//<link rel="stylesheet" type="text/css"href="../../scripts/datatables/css/datatable.css"/>

function DrawingTerminationReportDialog(){
    this.divName = null;
    this.terminationReports = new Array();
}

DrawingTerminationReportDialog.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 400,
            width: 600,
            title: "Termination Report",
            modal: true,
            buttons: {
                'Close': function()
                {
                    $(this).dialog('close');
                }

            },
            close: function()
            {

            }            
        });
    }
};

DrawingTerminationReportDialog.prototype.show = function(terminationReports){
    this.terminationReports = terminationReports;
    var div = document.getElementById(this.divName);
    if(div){
        if(terminationReports==null){
            div.innerHTML = "Terminating Workflow...";
        } else {
            div.innerHTML = "";
            var table = document.createElement("table");
            table.setAttribute("id", this.divName + "_table");
            //cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%"
            table.setAttribute("cellpadding","0");
            table.setAttribute("cellspacing", "0");
            table.setAttribute("class", "display");
            table.setAttribute("width", "100%");

            var tr;
            var td1;
            var td2;

            var thead = document.createElement("thead");
            var thr = document.createElement("tr");

            var th1 = document.createElement("th");
            th1.appendChild(document.createTextNode("Workflow Name"));
            thr.appendChild(th1);

            var th2 = document.createElement("th");
            th2.appendChild(document.createTextNode("Status"));
            thr.appendChild(th2);

            thead.appendChild(thr);
            table.appendChild(thead);
            var tbody = document.createElement("tbody");

            var report;
            for(var i=0;i<this.terminationReports.length;i++){
                report = this.terminationReports[i];
                tr = document.createElement("tr");

                td1 = document.createElement("td");
                td1.appendChild(document.createTextNode(report.name));

                td2 = document.createElement("td");
                if(report.terminatedOk){
                    td2.appendChild(document.createTextNode("Terminated Ok"));
                } else {
                    td2.appendChild(document.createTextNode("Termination Failed"));
                }
                tr.appendChild(td1);
                tr.appendChild(td2);
                tbody.appendChild(tr);
            }

            table.appendChild(tbody);
            div.appendChild(table);
            $("#" + this.divName + "_table").dataTable({"bJQueryUI":true});
        }
        $(div).dialog('open');
        
    }
}
/* 
 * This class fetches workflow drawings from the server
 */
function DrawingFetcher(){
    this.documentId = null;         // ID of the drawing document to fetch
    this.versionId = null;          // ID of the version to fetch
    this.useLatest = true;          // Should the latest version be fetched
    this.drawing = null;            // Drawing that will be populated
    this.editor = null;             // Editor that will be used to repaint the drawing
    this.drawingVersionList = new Array();                       // All of the versions
    this.fetchComplete = function(){};  // Callback when drawing fetch has completed
    this.versionsFetchComplete = function(){};  // Callback when versions list has been fetched
    var fetcher = this;
    this.callback = function(o){
        if(!o.error){
            fetcher.parseDrawing(o.drawing);
            if(fetcher.fetchComplete){
                fetcher.fetchComplete();
            }
        } else {
            $.jGrowl("Error fetching workflow: " + o.message);
        }
    };
    

    this.versionFetchCallback = function(o){
        if(!o.error){
            fetcher.drawingVersionList = o.versions;
            if(fetcher.versionsFetchComplete){
                fetcher.versionsFetchComplete();
            }
        } else {
            $.jGrowl("Error listing workflow versions: " + o.message);
        }
    };
}

/** Fetch the drawing data */
DrawingFetcher.prototype.getDrawing = function(){
    var url = "../../servlets/workflow?method=getWorkflowJson";

    if (this.documentId != "") {
        if(this.editor!=null){
            this.editor.repaint();
        }
        var callData;

        if(this.useLatest){
            callData = {
                workflowId: this.documentId
            };
        } else {
            callData = {
                workflowId: this.documentId,
                versionId: this.versionId
            }
        }
        
        jsonCall(callData, url, this.callback);
    }

};

/** Parse a JSON drawing representation */
DrawingFetcher.prototype.parseDrawing = function(drawingJson) {
    this.drawing.clearDrawing();
    this.drawing.resetZoom();
    
    // Basic properties
    this.drawing.documentId = drawingJson.documentId;
    this.drawing.versionId = drawingJson.versionId;
    this.versionId = drawingJson.versionId;
    this.documentId = drawingJson.documentId;
    this.drawing.versionNumber = drawingJson.versionNumber;
    this.drawing.setName(drawingJson.name);
    this.drawing.setExternalDataSupported(drawingJson.externalDataSupported);
    this.drawing.setExternalBlockName(drawingJson.externalBlockName);
    this.drawing.setDescription(drawingJson.description);
    this.drawing.setDeletedOnSuccess(drawingJson.deletedOnSuccess);
    this.drawing.setSingleVMMode(drawingJson.singleVMMode);
    this.drawing.setOnlyFailedOutputsUploaded(drawingJson.onlyFailedOutputsUploaded);
    this.drawing.dynamicEngine = drawingJson.dynamicEngine;
    this.drawing.setExternalService(drawingJson.externalService);
    
    // Create the blocks
    var blocks = drawingJson.blocks;
    var block;
    var blockCount = blocks.blockCount;
    var blockArray = blocks.blockArray;
    var i;
    for(i=0;i<blockCount;i++){
        //block = this.createBlock(blockArray[i]);
        block = new DrawingBlock();
        block.parseJson(blockArray[i], false);
        this.drawing.addBlock(block);
    }

    // Create the connections
    var connections = drawingJson.connections;
    var connectionCount = connections.connectionCount;
    var connectionArray = connections.connectionArray;

    for(i=0;i<connectionCount;i++){
        this.processConnection(connectionArray[i]);
    }

    this.drawing.dirty = false;
    if(this.editor!=null){
        this.editor.repaint();
    }
    
    var d = this.drawing;
    setTimeout(function(){
        d.repaint();
        d.imageCheckFunction();
    }, 10);
};

/** Process a drawing connection */
DrawingFetcher.prototype.processConnection = function(connectionJson){
    var sourceBlockGuid = connectionJson.sourceBlockGuid;
    var destinationBlockGuid = connectionJson.destinationBlockGuid;

    var sourceBlock = this.drawing.getBlockByGuid(sourceBlockGuid);
    var destinationBlock = this.drawing.getBlockByGuid(destinationBlockGuid);
    if(sourceBlock!=null && destinationBlock!=null && sourceBlock!=destinationBlock){
        var sourcePort = sourceBlock.getOutput(connectionJson.sourcePortName);
        var destinationPort = destinationBlock.getInput(connectionJson.destinationPortName);
        if(sourcePort!=null && destinationPort!=null && sourcePort.type!=destinationPort.type){
            if(destinationPort.canAcceptConnections() && sourcePort.canAcceptConnections && destinationPort.canConnectToPort(sourcePort)){
                var connection = this.drawing.joinPorts(sourcePort, destinationPort);
                
                // Load the connection co-ordinates
                if(connection!=null){
                    var pointCount = connectionJson.pointCount;
                    var i;
                    var dp;
                    for(i=0;i<pointCount;i++){
                        dp = new DrawingPoint(connectionJson.xPoints[i], connectionJson.yPoints[i]);
                        connection.pointList.add(dp);
                    }
                }
            }
        }
    }
};

/** List all of the versions of this workflow */
DrawingFetcher.prototype.fetchDrawingVersions = function(){
    if(this.documentId!=null){
        var url = "../../servlets/workflow?method=listWorkflowVersions";
        var callData = {id: this.documentId};
        jsonCall(callData, url, this.versionFetchCallback);
    }
};

/** Find the index of a version */
DrawingFetcher.prototype.findVersionIndex = function(versionId){
    var i;
    var version;
    for(i=0;i<this.drawingVersionList.length;i++){
        version = this.drawingVersionList[i];
        if(version.versionId==versionId){
            return i;
        }
    }
    return -1;
};
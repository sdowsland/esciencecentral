var drawing = new DrawingObject();
var drawingEditor = new DrawingEditor();
var fetcher = new DrawingFetcher();
var invocationsTable = null;
var paletteFetcher = new PaletteFetcher();
var saver = new DrawingSaver();
var executionManager = new DrawingExecutionManager();
var temporaryWorkflowJson = null;
var drawingVersionList = new Array();
var selectedInvocation = null;

globalDrawing = drawing;
globalEditor = drawingEditor;
executionManager.drawing = drawing;
executionManager.invocationsFetched = function(){
    var tableArray = executionManager.getTableData();
    $('#workflowRunsDiv').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="runtable"></table>' );
    invocationsTable = $('#runtable').dataTable( {
            "bPaginate": false,
            "aaData": tableArray,
            "aoColumns": [
                    {"sTitle": "Index"},
                    {"sTitle": "Date"},
                    {"sTitle": "Status"}
            ]
    } );

    

    $('#runtable thead tr').each(function(){
        this.insertBefore(document.createElement('th'), this.childNodes[0]);
        this.insertBefore(document.createElement('th'), this.childNodes[0]);
    });

    $('#runtable tbody tr').each(function(){
        var nCloneTd = document.createElement('td');
        nCloneTd.innerHTML='<img src="images/details_open.png"/>';

        nCloneTd.className="center";
        this.insertBefore(nCloneTd, this.childNodes[0]);

        var nCloneTd2 = document.createElement('td');
        nCloneTd2.className = "center";
        nCloneTd2.innerHTML='<img src="images/cross.png"/>';
        this.insertBefore(nCloneTd2, nCloneTd);
    });

    // Add event listeners to the expand buttons
    $('td img', invocationsTable.fnGetNodes()).each(function(){
        $(this).click(function(){
            var nTr = this.parentNode.parentNode;
            closeAllInvocationDetails(nTr);
                if(this.src.match('cross')){
                    deleteInvocation(nTr);
                    
                } else if(this.src.match('details_close')){
                    this.src = 'images/details_open.png';
                    invocationsTable.fnClose(nTr);
                    globalDrawing.clearExecutionReports();

                    // Replace the drawing with the saved copy
                    if(temporaryWorkflowJson!=null){
                        globalDrawing.clearDrawing();
                        fetcher.parseDrawing(temporaryWorkflowJson);
                    }

                } else if(this.src.match('details_open')){
                    // Save the drawing data to a temporary store - only if it isn't already displaying reports
                    if(!globalDrawing.displayingReports){
                        saver.drawing = globalDrawing;
                        saver.editor = globalEditor;
                        temporaryWorkflowJson = saver.createDrawingJson();
                    }

                    this.src = 'images/details_close.png';
                    var newRow = invocationsTable.fnOpen(nTr, showInvocationDetails(nTr), 'details');
                    newRow.childNodes[0].colSpan = 5;
                }

        });
    });

}

/** Update the selected invocation from the server */
function updateSelectedInvocation(){
    if(selectedInvocation!=null){
        if(selectedInvocation.status===INVOCATION_RUNNING || selectedInvocation.status===INVOCATION_WAITING){
            
        }
    }
}

/** Delete a workflow invocation */
function deleteInvocation(nTr){
    var data = invocationsTable.fnGetData(nTr);
    var invocationIndex = data[0] - 1;
    if(invocationIndex>=0 && invocationIndex<executionManager.getInvocationCount()){
        var deleteCallback = {
            success: function(o){
                listInvocations();
            },
            failure: function(o){

            }
        };
        var invocation = executionManager.getInvocation(invocationIndex);
        var callData = {id: invocation.invocationId};
        var callString = YAHOO.lang.JSON.stringify(callData);
        YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=deleteInvocation", deleteCallback, callString);
    }
}

/** Hide all of the expanded rows in invocations table */
function closeAllInvocationDetails(noCloseRow){
    // Close all of the rows apart from the noCloseRow
    selectedInvocation = null;
    $('td img', invocationsTable.fnGetNodes()).each(function(){
            var nTr = this.parentNode.parentNode;
            if(nTr!=noCloseRow){
                if(!this.src.match('cross')){
                    this.src = 'images/details_open.png';
                    invocationsTable.fnClose(nTr);
                }
            }
    });
}

/** Show the details for an invocation in the table popin */
function showInvocationDetails(nTr){
    var data = invocationsTable.fnGetData(nTr);
    var invocationIndex = data[0] - 1;
    var html;

    if(invocationIndex>=0 && invocationIndex<executionManager.getInvocationCount()){
        selectedInvocation = executionManager.getInvocation(invocationIndex);
        html='<div id="InvocationFiles">'
        html+='<table>';
        html+='<tr><td>Invocation Files: </td><td>Fetching...</td></tr>';
        html+='</table>';
        html+="</div>";
        
        // Clear the drawing
        globalDrawing.clearDrawing();
        
        // Get the actual drawing from the server
        fetchInvocationWorkflowJson(selectedInvocation.invocationId);

        // Get the execution reports - TODO Only do this if the invocation fetched ok
        fetchExecutionReports(selectedInvocation.invocationId);

        // Get the folder contents
        fetchInvocationDirectoryContents(selectedInvocation.invocationId);
        
    } else {
        html = '<h3>No Such Invovcation</h3>';
        globalDrawing.clearExecutionReports();
    }

    return html;
}

/** Get the drawing data for an invocation */
function fetchInvocationWorkflowJson(invocationId){
    var fetchCallback = {
        invocationId: invocationId,
        success: function(o){
            var result = YAHOO.lang.JSON.parse(o.responseText);

            if(!result.error){
                // Parse the drawing
                fetcher.drawing = globalDrawing;
                fetcher.editor = globalEditor;
                fetcher.parseDrawing(result.drawing);
                fetchExecutionReports(this.invocationId);
            } else {
                globalDrawing.clearDrawing();
                fetcher.parseDrawing(temporaryWorkflowJson);
                alert(result.message);
            }
        },
        failure: function(o){
            globalDrawing.clearExecutionReports();
        }
    };
    var callData = {invocationId: invocationId};
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=fetchInvocationWorkflowData", fetchCallback, callString);

}

/** Get the contents of an invocation directory */
function fetchInvocationDirectoryContents(invocationId){
    var fetchCallback = {
        invocationId: invocationId,
        success: function(o){
            var result = YAHOO.lang.JSON.parse(o.responseText);
            var filesDiv = document.getElementById("InvocationFiles");
            if(!result.error){

                var html = '<table>';

                var size = result.documentCount;
                var i;
                var fileJson;

                for(i=0;i<size;i++){
                    fileJson = result.documents[i];
                    if(i==0){
                        html+='<tr><td>Invocation Files: </td><td><a target="blank" href="../secure/dataproperties.jsp?id=' + fileJson.id + '">' + fileJson.name + '</a></td></tr>';
                    } else {
                        html+='<tr><td></td><td><a target="blank" href="../secure/dataproperties.jsp?id=' + fileJson.id + '">' + fileJson.name + '</a></td></tr>';
                    }
                    
                }

                html+='</table>';
                filesDiv.innerHTML = html;
            } else {
                filesDiv.innerHTML = "<h2>Cannot list files</h2>";
            }
        },
        failure: function(o){
            var filesDiv = document.getElementById("InvocationFiles");
            filesDiv.innerHTML = "<h2>Cannot list files</h2>";
        }
    };
    var callData = {invocationId: invocationId};
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=listInvocationFiles", fetchCallback, callString);
}

executionManager.invocationsFetchFailed = function(message){
    alert("Fetch failed: " + message);
}

executionManager.executionSent = function(){
    listInvocations();
}

executionManager.executionSentFailed = function(message){
    alert("Run failed: " + message);
}

function setBlockVersion(blockGuid, versionId, versionNumber){
    var block = globalDrawing.getBlockByGuid(blockGuid);
    if(block!=null){
        block.useLatest = false;
        block.versionId = versionId;
        block.versionNumber = versionNumber;
    }
    $('#versionchooser').dialog('close');
    globalDrawing.repaint();
}

function setBlockToLatestVersion(blockGuid){
    var block = globalDrawing.getBlockByGuid(blockGuid);
    if(block!=null){
        block.useLatest = true;
    }
    $('#versionchooser').dialog('close');
    globalDrawing.repaint();
}

/** Fetch all of the versions of a service */
function fetchServiceVersions(block){
    var fetchCallback = {
        success: function(o){
            var result = YAHOO.lang.JSON.parse(o.responseText);
            if(!result.error){
                var onclick = "setBlockToLatestVersion('" + block.guid + "')";
                var versionChooserContentsDiv = document.getElementById("versionchoosercontents");
                var html='<input type="button" value="Use Latest Version" onclick="' + onclick + '"/>';
                html+='<table style="width: 95%;">';
                var i;
                var version;
                
                for(i=0;i<result.versionCount;i++){
                    version = result.versions[i];
                    html+='<tr>';
                    html+='<td style="width: 50px; align: left;">' + version.versionNumber + '</td>';
                    html+='<td>' + version.timestamp + '</td>';
                    onclick = "setBlockVersion('" + block.guid + "','" + version.id + "','" + version.versionNumber + "')";
                    html+='<td style="width: 60px; align: left;"><a onclick="' + onclick + '">Use</a></td>';
                    html+='</tr>';
                }

                html+="</table>";
                versionChooserContentsDiv.innerHTML = html;
            } else {
                alert(result.message);
            }
        },
        failure: function(o){

        }
    };

    var versionChooserContentsDiv = document.getElementById("versionchoosercontents");
    versionChooserContentsDiv.innerHTML = "Fetching...";
    $('#versionchooser').dialog('open');

    var callData = {id: block.serviceId};
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=listServiceVersions", fetchCallback, callString);

    
}

/** Display the execution output data for a block */
function fetchBlockExecutionLog(invocationId, contextId){
    var fetchCallback = {
        success: function(o){
            var result = YAHOO.lang.JSON.parse(o.responseText);
            if(!result.error){
                var outputTextDiv = document.getElementById("blockoutputtext");
                outputTextDiv.innerHTML = "<pre>" + result.outputText + "</pre>";
            } else {
                alert(result.message);
            }
        },
        failure: function(o){

        }
    };
    var callData = {
        invocationId: invocationId,
        contextId: contextId
    };

    var outputTextDiv = document.getElementById("blockoutputtext");
    outputTextDiv.innerHTML = "Fetching...";
    $('#blockoutput').dialog('open');
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=fetchBlockExecutionOutput", fetchCallback, callString);
}

/** Display the help data for a block */
function displayBlockHelpText(serviceId, versionId){
    var fetchCallback = {
        success: function(o){
            var result = YAHOO.lang.JSON.parse(o.responseText);
            if(!result.error){
                var helpTextDiv = document.getElementById("blockhelptext");
                helpTextDiv.innerHTML = result.html;
            } else {
                alert(result.message);
            }
        },
        failure: function(o){

        }
    };
    var callData;
    if(versionId){
        callData = {
            serviceId: serviceId,
            versionId: versionId
        };
    } else {
        callData = {
            serviceId: serviceId
        };
    }

    var helpTextDiv = document.getElementById("blockhelptext");
    helpTextDiv.innerHTML = "Fetching...";
    
    $('#blockhelp').dialog('open');
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=getWorkflowServiceHelp", fetchCallback, callString);

}

/** Fetch the invocation block reports */
function fetchExecutionReports(invocationId){
    var fetchCallback = {
        success: function(o){
            var result = YAHOO.lang.JSON.parse(o.responseText);
            if(!result.error){
                globalDrawing.setExecutionReports(result);
            } else {

                alert(result.message);
            }
        },
        failure: function(o){
            globalDrawing.clearExecutionReports();
        }
    };

    var callData = {invocationId: invocationId};
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=fetchReports", fetchCallback, callString);
}



function drawingFetchComplete(){
    listInvocations();
    if(fetcher.useLatest){
        fetchWorkflowVersions(fetcher.documentId);
    } else {
        fetchWorkflowVersions(fetcher.documentId, fetcher.versionId);
    }
}

function drawingSaveComplete(){
    fetchWorkflowVersions(saver.documentId, saver.versionId);
}

function buttonClick(left, top, serviceId)
{
    if(serviceId!=null && serviceId!=""){
      var db = new DrawingBlock();
      db.name = "Fetching...";
      db.left = left;
      db.top = top;
      db.serviceId = serviceId;
      db.versionId = null;

      drawing.addBlock(db);
      drawingEditor.repaint();
      db.fetchData();
    }

}

function deleteSelected(){
    drawing.deleteSelected();
    drawingEditor.repaint();
}

function loadWorkflow()
{
    fetcher.useLatest = true;
    fetcher.drawing = drawing;
    fetcher.editor = drawingEditor;
    fetcher.getDrawing();
}

function saveWorkflow(){
    saver.drawing = drawing;
    saver.editor = drawingEditor;
    saver.saveDrawing();
}

function runWorkflow(){
    if(globalDrawing.documentId){
        executionManager.execute();
    }
}

function listInvocations(){
    if(globalDrawing.documentId){
        executionManager.fetchInvocations();
    }
}



function fetchPalette(){
    paletteFetcher.fetchPalette();
}

function initCanvas(canvas)
{
  if (window.G_vmlCanvasManager && window.attachEvent && !window.opera)
  {
    canvas = window.G_vmlCanvasManager.initElement(canvas);
  }
  return canvas;
}


// ======================================================================
/** Document ready function */
// ======================================================================
$(document).ready(function()
{
  drawingEditor.drawing = drawing;
  drawingEditor.canvas = initCanvas(document.getElementById("drawingCanvas"));
  if (window.G_vmlCanvasManager && window.attachEvent && !window.opera)
  {
    drawing.isIE = true;
  }
  drawingEditor.setupMouse();
  setTab("tab_wf", "active");

  $('#palette h3').next().toggle();
  $('#palette').jScrollPane({scrollbarOnLeft:true});

  fetcher.fetchComplete = drawingFetchComplete; // Set up drawing fetch complete listener
  if (fetcher.documentId != null)
  {
    fetcher.drawing = globalDrawing;
    fetcher.editor = globalEditor;
    fetcher.getDrawing();
  }

  saver.saveComplete = drawingSaveComplete;
      
//  $('#search-button').click(function()
//  {
//
//    $('.draggable').each(function()
//    {
//      var searchText = $('#search-text').val();
//      var listText = $(this).text();
//      var pos = listText.indexOf(searchText);
//      if (pos === -1)
//      {
//        $(this).hide();
//      }
//
//    });
//
//    $('#palette h3').each(function()
//    {
//      var ul = $(this).next();
//      var showChildren = $(this).next().children().children(':visible').length;
//      if (showChildren === 0)
//      {
//        $(this).hide();
//      }
//    });
//
//  });
//
//  $('#reset-button').click(function()
//  {
//    $('.draggable').show();
//    $('#palette h3').show();
//  });


  $("#drawingCanvas").droppable({
    drop: function(event, ui)
    {
      var b_context = $(this).get(0).getContext("2d");
      var canvasPos = $("#drawingCanvas").position();
      var centreDraggableLeft = ui.draggable.width() / 2;
      var centreDraggableTop = ui.draggable.height() / 2;
      buttonClick(ui.offset.left - canvasPos.left + centreDraggableLeft - 25, ui.offset.top - canvasPos.top + centreDraggableTop - 25, ui.draggable.attr('id'));
    }
  });

  fetchPalette();
    
});


function palleteInitComplete(){
  $('#palette h3').click(function()
  {
    $(this).next().toggle();
    $('#palette').jScrollPane({scrollbarOnLeft:true});
    return false;
  });

  $('li').click(function()
  {
    $('li').removeClass('selected');
    $(this).addClass('selected');
  });


  $("li").draggable({cursor:"move",
    opacity: 0.7,
    zIndex:9999,
    helper: function(event) {
				return $('<img src="images/button.jpg"/>');
			},
    start: function(event, ui)
    {
      $(this).addClass('moving');
    },
    stop: function(event, ui)
    {
      $(this).removeClass('moving');
    }

  });
    $('#palette h3').next().toggle();
}



/** Show the editor dialog */
function showBlockEditor(block){
    var properties = block.properties;
    var i;
    var property;

    var html = '<form name="_propertyForm"><table>';

    for(i=0;i<properties.getPropertyCount();i++){
        property = properties.getProperty(i);
        html+=property.createHtmlTableRow();
    }
    var editorElement = document.getElementById("dialog");
    html += "<table></form>";

    editorElement.innerHTML = html;
    editorElement.title = "Editing: " + block.name;
    editorElement.properties = properties;
    editorElement.block = block;

    $('#dialog').dialog('open');

}

/** Save the editor state */
function updateEditor(){
    var editorElement = document.getElementById("dialog");

    if(editorElement.block){
        
        var i;
        var property;

        var html = "<form><table>";

        for(i=0;i<editorElement.properties.getPropertyCount();i++){
            property = editorElement.properties.getProperty(i);
            html+=property.createHtmlTableRow();
        }
        html += "<table></form>";

        editorElement.innerHTML = html;
    }
}

/** Edit the properties of the global drawing */
function editDrawingProperties(){
    var properties = globalDrawing.getEditableProperties();
    var i;
    var property;

    var html = '<form name="_propertyForm"><table>';

    for(i=0;i<properties.getPropertyCount();i++){
        property = properties.getProperty(i);
        html+=property.createHtmlTableRow();
    }
    var editorElement = document.getElementById("dialog");
    html += "<table></form>";

    editorElement.innerHTML = html;
    editorElement.title = "Editing: Workflow Properties";
    editorElement.properties = properties;
    editorElement.block = null;

    $('#dialog').dialog('open');
}

/** Display the list of drawing versions */
function fetchWorkflowVersions(id, displayVersionId){
    var fetchCallback = {
        displayVersionId: displayVersionId,
        success : function(o){


            var versionText;
            var result = YAHOO.lang.JSON.parse(o.responseText);
            if(!result.error){
                drawingVersionList = result.versions;
                var versionDiv = document.getElementById("versionlistdiv");
                var versionHtml='<select id="versionselector">';

                var size = result.versionCount;
                var versionJson;
                var i;
                var displayIndex = -1;

                for(i=0;i<size;i++){
                    versionJson = result.versions[i];
                    versionText = versionJson.versionNumber + ": " + versionJson.timestamp;
                    versionHtml+="<option>" + versionText + "</option>";
                    if(this.displayVersionId){
                        if(this.displayVersionId===versionJson.versionId){
                            displayIndex = i;
                        }
                    }
                }
                versionHtml+="</select>";
                versionDiv.innerHTML = versionHtml;

                var selector = document.getElementById("versionselector");
                if(displayIndex!=-1){
                    selector.selectedIndex = displayIndex;
                } else {
                    selector.selectedIndex = size - 1;
                }
                selector.onchange = changeVersion;
            } else {
                drawingVersionList = new Array();
                alert(result.message);
            }
        },

        failure : function(o){
            alert("Could not fetch vesion list");
        }
    };
    var callData = {id: id};
    var callString = YAHOO.lang.JSON.stringify(callData);
    YAHOO.util.Connect.asyncRequest('POST', "../servlets/workflowapi?method=listWorkflowVersions", fetchCallback, callString);
}

/** Change the workflow version being displayed */
function changeVersion(){
    var selector = document.getElementById("versionselector");
    if(selector!=null){
        var index = selector.selectedIndex;
        if(drawingVersionList!=null){
            var versionJson = drawingVersionList[index];
            fetcher.useLatest = false;
            fetcher.versionId = versionJson.versionId;
            fetcher.getDrawing();
        }
    }
}
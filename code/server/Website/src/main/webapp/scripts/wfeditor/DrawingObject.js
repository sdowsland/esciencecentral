// This code provides a drawing object that contains blocks
function DrawingObject(){
    this.blocks = new List();					// All blocks in the drawing
    this.selectedObjects = new List();			// Currently selected objects
    this.dirty = false;                                     // Has the drawing data been modified
    this.documentId = null;
    this.versionId = null;
    this.descriptin = null;
    this.editor = null;
    this.versionNumber = 0;
    this.drawingProperties = new DrawingPropertyList(this);          // Editable properties
    this.drawingProperties.addBoolean("ExternalDataSupported", false, "Does this workflow operate on an external data file");
    this.drawingProperties.addString("ExternalBlockName", "Name of the externally accessible block");
    this.drawingProperties.addString("Name", "New Workflow", "Name of this workflow");
    this.drawingProperties.addBoolean("SingleVMMode", true, "Execute all the services for this workflow in a single VM");
    this.drawingProperties.addString("Description", "", "Description text for this workflow");
    this.drawingProperties.addBoolean("OnlyUploadFailedOutputs", false, "Only upload standard output for blocks that had errors");
    this.drawingProperties.addBoolean("DeletedOnSuccess", false, "Should the workflow invocation folder be deleted if the workflow completes with no errors");
    this.drawingProperties.addBoolean("ExternalService", false, "Should this workflow be marked as an external service");
    this.dynamicEngine = true;
    this.linkStart = null;						// Start location of link
    this.linkStartPort = null;					// Port being linked from 
    this.linkEnd = null;						// End location of link
    this.boxStart = null;						// Start location of box
    this.boxEnd = null;							// End location of box
    this.isIE = false;							// Is this running in Internet Explorer
    this.blockImage = new Image();				// Image for drawing block with
    this.blockImage.src = rewriteAjaxUrl("../../styles/common/images/block.jpg");		// Load block image
    this.okIcon = new Image();
    this.okIcon.src = rewriteAjaxUrl("../../scripts/wfeditor/images/tick.png");
    this.errorIcon = new Image();
    this.errorIcon.src = rewriteAjaxUrl("../../scripts/wfeditor/images/cross.png");
    this.drawingDirtyImage = new Image();                   // Icon to show when drawing needs saving
    this.drawingDirtyImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/drive_disk.png");
    this.inputBlockIcon = new Image();                      // Icon highlighing the input block
    this.inputBlockIcon.src = rewriteAjaxUrl("../../scripts/wfeditor/images/rightarrow.png");
    this.highlightPortDataType = "";  			// Type of port to highlight when connecting
    this.highlightPortType = "input";			// Should inputs be hightlighted when drawing
    this.showName = true;                                   // Should the drawing name be displayed
    this.displayLabel = null;
    this.invocationMessage = null;                          // Message from the selected invocation if there is one
    this.displayingReports = false;                         // Is the drawing currently displaying execution reports
    this.invocationId = null;                               // Invocation ID if displaying reports
    this.menu = new DrawingContextMenu();                   // Menu for editing blocks
    this.menu.drawing = this;
    this.menuVisible = false;                               // Is the menu currently visible
    this.latestVersionImage = new Image();                  // Image to use in a block that uses the latest service version
    this.latestVersionImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/star.png");
    this.specificVersionImage = new Image();                // Image to use for a specific service version
    this.specificVersionImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/clock.png");
    this.blockExecutingImage = new Image();
    this.blockExecutingImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/cog.png");
    this.blockWaitingImage = new Image();
    this.blockWaitingImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/hourglass.png");
    this.upstreamErrorsImage = new Image();
    this.upstreamErrorsImage.src = rewriteAjaxUrl("../../scripts/wfeditor/images/link_break.png");
        
    this.drawingZoom = 1.0;
    this.minimumWidth = 640;
    this.minimumHeight = 480;
}

/** Repaint this drawing */
DrawingObject.prototype.repaint = function() {
    if (this.editor != null) {
        this.editor.repaint();
    }
};

DrawingObject.prototype.allBlockImagesLoaded = function(){
    var allLoaded = true;
    for(var i=0;i<this.blocks.size();i++){
        if(this.blocks.get(i).blockImageLoaded==false){
            allLoaded = false;
        }
    }
    return allLoaded;
};

DrawingObject.prototype.imageCheckFunction = function(){
    if(this.allBlockImagesLoaded()){
        this.repaint();
    } else {
        var d = this;
        setTimeout(function(){d.imageCheckFunction();}, 100);
    }
};


/** Add a block to this drawing */
DrawingObject.prototype.addBlock = function(block){
    block.parentDrawing = this;
    this.blocks.add(block);
    if(block.name==null || block.name==""){
        var name = this.getNextBlockName();
        if(name!=null){
            block.setBlockName(name);
        }
    }
    this.dirty = true;
};

/** Get the total number of connections in this drawing */
DrawingObject.prototype.getOutputConnectionCount = function(){
    var count = 0;
    var i;
    for(i=0;i<this.blocks.size();i++){
        count = count + this.blocks.get(i).getOutputConnectionCount();
    }
    return count;
};

/** Get a block using its GUID */
DrawingObject.prototype.getBlockByGuid = function(guid){
    var block;
    var i;
    for(i=0;i<this.blocks.size();i++){
        block = this.blocks.get(i);
        if(block.guid==guid){
            return block;
        }
    }
    return null;
}

/** Remove a block from this drawing */
DrawingObject.prototype.removeBlock = function(block){
    var index = this.blocks.indexOf(block);
    if(index!=-1){
        block.clearConnections();
        this.blocks.remove(index);
        this.dirty = true;
    }
	
    if(this.selectedObjects.contains(block)){
        index = this.selectedObjects.indexOf(block);
        if(index!=-1){
            this.selectedObjects.remove(index);
            this.dirty = true;
        }
    }

};

/** Remove a connection from this drawing */
DrawingObject.prototype.removeConnection = function(connection){
    connection.sourcePort.removeConnection(connection);
    connection.destinationPort.removeConnection(connection);
	
    if(this.selectedObjects.contains(connection)){
        var index = this.selectedObjects.indexOf(connection);
        if(index!=-1){
            this.selectedObjects.remove(index);
            this.dirty = true;
        }
    }
};

/** Return the size of the drawing area */
DrawingObject.prototype.calculateDrawingBounds = function(){
    var defaultBorder = 40;
    var maxBottom = 0;
    var maxRight = 0;
    var right;
    var bottom;
    var block;
    var i;
    for(i=0;i<this.blocks.size();i++){
        block = this.blocks.get(i);
        right = block.left + block.width;
        bottom = block.top + block.height;
        if(right>maxRight){
            maxRight = right;
        }

        if(bottom>maxBottom){
            maxBottom = bottom;
        }
    }
    var result = {
        width: (maxRight + defaultBorder) * this.drawingZoom,
        height: (maxBottom + defaultBorder) * this.drawingZoom
    }
    return result;
}

/** Render this drawing onto its canvas */
DrawingObject.prototype.renderDrawing = function(canvas, renderState){
    var i;
    var hasExternalInput = this.isExternalDataSupported();
    var externalBlockName = this.getExternalBlockName();
    if(canvas!=null){
        
        var ctx = canvas.getContext("2d");
        ctx.globalAlpha = 1;
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        if(this.displayingReports){
            ctx.fillStyle = DISABLED_DRAWING_BACKGROUND_COLOR;
            ctx.globalAlpha = 0.2;
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.globalAlpha = 1.0;
        }

        // Show display label if there is one
        if(this.showName){
            ctx.font = "16pt arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = BLOCK_TITLE_COLOR;
            ctx.fillText(this.getName(), 30, 20);

            /*ctx.font = "8pt arial";
            ctx.textAlign = 'end';
            ctx.fillStyle = BLOCK_TITLE_COLOR;
            ctx.fillText(this.getDescription(), canvas.width - 10, 20, 200);*/

            wrapText(ctx, this.getDescription(), 'left', 30, 40, 250, '8pt arial', 12, BLOCK_TITLE_COLOR);
                    
        } else if(this.displayLabel!=null){
            ctx.font = "16pt arial";
            ctx.textAlign = 'left';
            ctx.fillStyle = BLOCK_TITLE_COLOR;
            ctx.fillText(this.displayLabel, 30, 20);
        }

        // Display the save icon if the drawing is "dirty"
        if(this.dirty==true){
            ctx.drawImage(this.drawingDirtyImage, 10, 4);
        }

        // Setup the canvas zoom
        ctx.scale(this.drawingZoom, this.drawingZoom);
        ctx.zoom = this.drawingZoom;

        var block;
        for(i=0;i<this.blocks.size();i++){
            block = this.blocks.get(i);
            block.renderBlock(ctx, canvas.width, canvas.height, renderState);
            if(hasExternalInput && block.name==externalBlockName){
                // Render the input connection
                ctx.drawImage(this.inputBlockIcon, block.left + 15, block.top + 15);
            }
        }

        if(this.linkStart!=null && this.linkStartPort!=null && this.linkEnd!=null){
            ctx.lineWidth = CONNECTING_LINE_WIDTH;
            ctx.fillStyle = CONNECTING_LINK_COLOR;
            ctx.strokeStyle = CONNECTING_LINK_COLOR;
            ctx.beginPath();
            ctx.moveTo(this.linkStart.x, this.linkStart.y);
            ctx.lineTo(this.linkEnd.x, this.linkEnd.y);

            ctx.moveTo(this.linkEnd.x, this.linkEnd.y - 10);
            ctx.lineTo(this.linkEnd.x, this.linkEnd.y + 10);
            ctx.moveTo(this.linkEnd.x - 10, this.linkEnd.y);
            ctx.lineTo(this.linkEnd.x + 10, this.linkEnd.y);
            ctx.closePath();
            ctx.stroke();
            
            // Draw the output port icon
            var portIcon = this.linkStartPort.getImageForDataType();
            ctx.drawImage(portIcon, this.linkEnd.x + 12, this.linkEnd.y - 6);
            
            // Draw output port name
            if(ctx.fillText){
                ctx.font = "8pt arial";
                ctx.textAlign = 'left';
                ctx.fillStyle = LINK_LABEL_COLOR;
                ctx.fillText(this.linkStartPort.name, this.linkEnd.x + 25, this.linkEnd.y + 2);
            }
            
            if(this.linkStartPort.sizeModel){
                ctx.font = "8pt arial";
                ctx.textAlign = 'left';
                ctx.fillStyle = LINK_LABEL_COLOR;
                ctx.fillText(this.linkStartPort.sizeModel.predictedValue.toFixed(2), this.linkEnd.x + 25, this.linkEnd.y + 20);                
            }
        }
		
        if(this.boxStart!=null && this.boxEnd!=null){
            ctx.globalAlpha = SELECTION_BOX_ALPHA;
            ctx.fillStyle = SELECTION_BOX_COLOR;
            ctx.fillRect(this.boxStart.x, this.boxStart.y, this.boxEnd.x - this.boxStart.x, this.boxEnd.y - this.boxStart.y);
            ctx.globalAlpha = 1;
        }

        // Display an edit menu if there is one block selected
        if (this.selectedObjects.size() == 1) {
            var selected = this.selectedObjects.get(0);
            if (selected instanceof DrawingBlock) {
                
                // Show the block status text
                if(selected.reportJson && this.displayingReports){
                    ctx.font = "12pt arial";
                    ctx.textAlign = 'left';
                    if(selected.reportJson.executedOk){
                        ctx.fillStyle = "#336600";
                    } else {
                        ctx.fillStyle = "#990000";
                    }
                    if(selected.reportJson.additionalMessage && !selected.reportJson.additionalMessage==""){
                        // Additional message
                        ctx.fillText(selected.reportJson.additionalMessage, 30, canvas.height - 30);
                    } else {
                        // Stanadrd message
                        ctx.fillText(selected.reportJson.message, 30, canvas.height - 30);
                    }
                    
                } else if(selected.description){
                    // Show block description if we are not displaying reports
                    ctx.font = "10pt arial";
                    ctx.textAlign = 'left';                    
                    ctx.fillStyle = "#324c93";
                    ctx.fillText(selected.description, 30, canvas.height - 10);
                }      
                
                if (selected.parentDrawing.invocationId == null) {
                    // Switch off the relevant menu options
                    if (this.displayingReports) {
                        if (this.menu.getCommandByName("output")) {
                            this.menu.getCommandByName("output").enabled = true;
                        }

                        if (this.menu.getCommandByName("version")){
                            this.menu.getCommandByName("version").enabled = false;
                        }

                        if (this.menu.getCommandByName("delete")){
                            this.menu.getCommandByName("delete").enabled = false;
                        }

                    } else {
                        if (this.menu.getCommandByName("output")){
                            this.menu.getCommandByName("output").enabled = true;
                        }

                        if (this.menu.getCommandByName("version")){
                            this.menu.getCommandByName("version").enabled = true;
                        }

                        if (this.menu.getCommandByName("delete")){
                            this.menu.getCommandByName("delete").enabled = true;
                        }
                    }
                }
                this.menu.block = selected;
                this.menu.renderMenu(ctx);
                this.menuVisible = true;        
            } else {
                this.menuVisible = false;
            }
        } else {
            this.menuVisible = false;
            
            // Is there an invocation message to show
            if(this.displayingReports && this.invocationMessage!=null){
                ctx.font = "12pt arial";
                ctx.textAlign = 'left';
                ctx.fillStyle = "#990000";
                ctx.fillText(this.invocationMessage, 30, 45);  
            }
        }
    }
};

/** Get the selected block that contains an edit box under the x and y co-ordinates if there is one */
DrawingObject.prototype.getPropertyEditBlockAtLocation = function(x, y){
    var i;
    var block;

    for(i=0;i<this.selectedObjects.size();i++){
        block = this.selectedObjects.get(i);
        if(block.withinPropertyBoxBounds){
            if(block.withinPropertyBoxBounds(x, y)){
                return block;
            }
        }
    }
    return null;
}

/** Get the selected block that contains an execution log box under the x and y co-ordinates if there is one */
DrawingObject.prototype.getExecutionLogBlockAtLocation = function(x, y){
    var i;
    var block;

    for(i=0;i<this.selectedObjects.size();i++){
        block = this.selectedObjects.get(i);
        if(block.withinExecutionLogBoxBounds){
            if(block.withinExecutionLogBoxBounds(x, y)){
                return block;
            }
        }
    }
    return null;
}

/** Find the object at a location */
DrawingObject.prototype.getObjectAtLocation = function(x, y){
    var obj = this.findBlockAt(x, y);
    if(obj!=null){
        return obj;
    } else {
        obj = this.findConnectionAt(x, y);
        if(obj!=null){
            return obj;
        } else {
            return null;
        }
    }
};

/** Find the connection object at a location */
DrawingObject.prototype.findConnectionAt = function(x, y){
    // Check all of the block output connections
    var i;
    var j;
    var k;
	
    var block;
    var port;
    var connection;
	
    for(i=0;i<this.blocks.size();i++){
        block = this.blocks.get(i);
        for(j=0;j<block.outputList.size();j++){
            port = block.outputList.get(j);
            for(k=0;k<port.connections.size();k++){
                connection = port.connections.get(k);
                if(connection.withinBounds(x, y)){
                    return connection;
                }
            }
        }
    }
    return null;
};

/** Find the block at a co-ordinate */
DrawingObject.prototype.findBlockAt = function(x, y){
    var i;
    for(i=0;i<this.blocks.size();i++){
        if(this.blocks.get(i).withinBounds(x, y)){
            return this.blocks.get(i);
        }
    }
    return null;
};

/** Get the port within the tolerance distance of a point */
DrawingObject.prototype.getPortAtPoint = function(x, y){
    var port = null;
    var locatedPort = null;
    var distance;
    var p;
    var block;
    var minDistance = 100000;
    var i;
    var j;
	
    for(i=0;i<this.blocks.size();i++){
        block = this.blocks.get(i);
		
        // Check inputs
        for(j=0;j<block.inputList.size();j++){
            port = block.inputList.get(j);
            p = block.locatePort(port);
            distance = this.distanceFromPoint(x, y, p.x, p.y);
            if(distance < minDistance){
                locatedPort = port;
                minDistance = distance;
            }
        }
		
        // Check outputs
        for(j=0;j<block.outputList.size();j++){
            port = block.outputList.get(j);
            p = block.locatePort(port);
            distance = this.distanceFromPoint(x, y, p.x, p.y);
            if(distance<minDistance){
                locatedPort = port;
                minDistance = distance;
            }
        }
		

    }
	
	
    // Was anything found
    if(minDistance<=PORT_HIT_DISTANCE){
        return locatedPort;
    } else {
        return null;
    }
	
};

/** Move the selected objects by an ammount */
DrawingObject.prototype.moveSelectedObjects = function(xOffset, yOffset){
    var i;
    if(this.selectedObjects.size()>0){
        this.dirty = true;
    }

    // Move lots of objects
    for(i=0;i<this.selectedObjects.size();i++){
        this.selectedObjects.get(i).moveBy(xOffset, yOffset);
    }

};

/** Is an object selected */
DrawingObject.prototype.isObjectSelected = function(obj){
    return this.selectedObjects.contains(obj);
};

/** Get the number of objects selected */
DrawingObject.prototype.getSelectionCount = function(){
    return this.selectedObjects.size();
};
	
/** Clear the selection list */
DrawingObject.prototype.clearSelectionList = function(){
    while(this.selectedObjects.size()>0){
        this.selectedObjects.remove(0);
    }
};

/** Add an object to the selection list */
DrawingObject.prototype.addToSelectionList = function(obj){
    if(!this.selectedObjects.contains(obj)){
        this.selectedObjects.add(obj);
    }
};

/** Calculate the distance between two points */
DrawingObject.prototype.distanceFromPoint = function(x1, y1, x2, y2){
    return Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
};

/** Join two ports together */
DrawingObject.prototype.joinPorts = function(sourcePort, targetPort){
    var connection = null;
	
    if(sourcePort.type=="input" && targetPort.type=="output"){
        // Only allow one input
        if(sourcePort.connections.size()<1){
            connection = new DrawingConnection(targetPort, sourcePort);
            sourcePort.addConnection(connection);
            targetPort.addConnection(connection);
            this.dirty = true;
        } else {
            window.alert("Only one input allowed");
        }
		
    } else if(targetPort.type=="input" && sourcePort.type=="output"){
        // Only allow one input
        if(targetPort.connections.size()<1){
            connection = new DrawingConnection(sourcePort, targetPort);
            sourcePort.addConnection(connection);
            targetPort.addConnection(connection);
            this.dirty = true;
        } else {
            window.alert("Only one input allowed");
        }
    }
    return connection;
};

/** Remove all of the selected drawing objects */
DrawingObject.prototype.deleteSelected = function(){
    var objectsToDelete = new List();
    var i;
    var obj;
	
    for(i=0;i<this.selectedObjects.size();i++){
        objectsToDelete.add(this.selectedObjects.get(i));
    }
	
    this.selectedObjects.clear();
    for(i=0;i<objectsToDelete.size();i++){
        obj = objectsToDelete.get(i);
        if(this.blocks.contains(obj)){
            // This is a block
            this.removeBlock(obj);
            this.dirty = true;
        } else {
            // Must be a connection
            this.removeConnection(obj);
            this.dirty = true;
        }
    }
};

/** Delete everything from the drawing */
DrawingObject.prototype.clearDrawing = function(){
    var objectsToDelete = new List();
    var i;
    var obj;
	
    for(i=0;i<this.blocks.size();i++){
        objectsToDelete.add(this.blocks.get(i));
    }
	
    this.selectedObjects.clear();
    for(i=0;i<objectsToDelete.size();i++){
        obj = objectsToDelete.get(i);
        if(this.blocks.contains(obj)){
            // This is a block
            this.removeBlock(obj);
        } else {
            // Must be a connection
            this.removeConnection(obj);
        }
    }
    this.dirty = true;
};

/** Clear the selection rectangle */
DrawingObject.prototype.clearSelectionRectangle = function(){
    this.boxEnd = null;
    this.boxStart = null;
};

/** Select all objects within the selection rectangle */
DrawingObject.prototype.selectAllObjectsInSelectionBox = function(){
    if(this.boxStart!=null && this.boxEnd!=null){
        this.selectedObjects.clear();
        var i;
        var block;
		
        for(i=0;i<this.blocks.size();i++){
            block = this.blocks.get(i);
            if(block.isContainedBy(this.boxStart, this.boxEnd)){
                this.selectedObjects.add(block);
            }
        }
    }
};

/** Clear all of the execution reports from the blocks */
DrawingObject.prototype.clearExecutionReports = function() {
    var i;
    this.displayLabel = null;
    this.invocationMessage = null;
    for(i=0;i<this.blocks.size();i++){
        this.blocks.get(i).reportJson = null;
    }
    this.displayingReports = false;
    this.repaint();
};

/** Set the execution reports for all the blocks in this drawing */
DrawingObject.prototype.setExecutionReports = function(reportsJson){
    var size = reportsJson.reportCount;
    var report;
    var i;
    
    var block;

    for(i=0;i<size;i++){
        report = reportsJson.reports[i];
        block = this.getBlockByGuid(report.blockGuid);
        if(block!=null){
            block.reportJson = report;
        }
    }
    this.displayingReports = true;
    this.displayLabel = reportsJson.invocationTime;
    this.invocationId = reportsJson.invocationId;
    this.repaint();
};

/** Create a set of editable properties for this drawing */
DrawingObject.prototype.getEditableProperties = function(){
    return this.drawingProperties;
};

/** Set the drawing name */
DrawingObject.prototype.setName = function(name){
    this.drawingProperties.addString("Name", name);
};

/** Get the drawing name */
DrawingObject.prototype.getName = function(){
    return this.drawingProperties.getPropertyByName("Name").value;
};

/** Set the drawing description */
DrawingObject.prototype.setDescription = function(description){
    this.drawingProperties.addString("Description", description);
};

/** Get the drawing description */
DrawingObject.prototype.getDescription = function(){
    return this.drawingProperties.getPropertyByName("Description").value;
};

/** Set the drawing external block name */
DrawingObject.prototype.setExternalBlockName = function(externalBlockName){
    this.drawingProperties.addString("ExternalBlockName", externalBlockName);
};

/** Is the drawing set to execute in single VM Mode */
DrawingObject.prototype.isSingleVMMode = function(){
    return this.drawingProperties.getPropertyByName("SingleVMMode").value;
};

/** Set whether this drawing executes in single VM Mode */
DrawingObject.prototype.setSingleVMMode = function(value){
    this.drawingProperties.addBoolean("SingleVMMode", value);
};


/** Set whether this drawing is marked as an external service */
DrawingObject.prototype.setExternalService = function(value){
    this.drawingProperties.addBoolean("ExternalService", value);
};

/** Get the drawing external block name */
DrawingObject.prototype.getExternalBlockName = function(){
    return this.drawingProperties.getPropertyByName("ExternalBlockName").value;
};

/** Get whether external data is supported */
DrawingObject.prototype.isExternalDataSupported = function(){
    return this.drawingProperties.getPropertyByName("ExternalDataSupported").value;
};

/** Get whether this drawing is marked as an external service */
DrawingObject.prototype.isExternalService = function(){
    return this.drawingProperties.getPropertyByName("ExternalService").value;
};

/** Get whether external data is supported */
DrawingObject.prototype.setExternalDataSupported = function(externalDataSupported){
    this.drawingProperties.addBoolean("ExternalDataSupported", externalDataSupported);
};

/** Set whether invocations will be deleted if they succeed */
DrawingObject.prototype.setDeletedOnSuccess = function(deletedOnSuccess){
    this.drawingProperties.addBoolean("DeletedOnSuccess", deletedOnSuccess);
};

/** Will successful invocations be automatically deleted */
DrawingObject.prototype.isDeletedOnSuccess = function(){
    return this.drawingProperties.getPropertyByName("DeletedOnSuccess").value;
};

/** Set whether to only uploaded failed blocks output text */
DrawingObject.prototype.setOnlyFailedOutputsUploaded = function(onlyFailedOutputsUploaded){
    this.drawingProperties.addBoolean("OnlyUploadFailedOutputs", onlyFailedOutputsUploaded);
};

DrawingObject.prototype.isOnlyFailedOutputsUploaded = function(){
    return this.drawingProperties.getPropertyByName("OnlyUploadFailedOutputs").value;
};

/** Get a block by name */
DrawingObject.prototype.getBlockByName = function(blockName){
    var block;
    var i;
    for(i=0;i<this.blocks.size();i++){
        block = this.blocks.get(i);
        if(block.name==blockName){
            return block;
        }
    }
    return null;
};

/** Set the executing block */
DrawingObject.prototype.setExecutingBlock = function(blockId, totalBytesToStream, bytesStreamed){
    var i;
    var block;
    for(i=0;i<this.blocks.size();i++){
        block = this.blocks.get(i);
        if(block.guid==blockId){
            block.totalBytesToStream = totalBytesToStream;
            block.bytesStreamed = bytesStreamed;
            block.highlightExecution = true;
        } else {
            block.totalBytesToStream = 0;
            block.bytesStreamed = 0;
            block.highlightExecution = false;
        }
    }
};

/** Get the next available name for a block */
DrawingObject.prototype.getNextBlockName = function(){
    var count = 0;
    var block;

    while(count<1000000){
        block = this.getBlockByName("block_" +  + count);
        if(block==null){
            return "block_" +  count;
        }
        count++;
    }
    return null;
};

/** Reset the zoom */
DrawingObject.prototype.resetZoom = function(){
    this.drawingZoom = 1.0;
    this.repaint();
}

/** Zoom in 10% */
DrawingObject.prototype.zoomIn = function(){
    this.drawingZoom = this.drawingZoom + 0.1;
    this.repaint();
}

/** Zoom out 10% */
DrawingObject.prototype.zoomOut = function(){
    if(this.drawingZoom>0.2){
        this.drawingZoom = this.drawingZoom - 0.1;
        this.repaint();
    }
}

function wrapText(context, text, align, x, y, maxWidth, font, lineHeight, fillStyle) {
    var words = text.split(' ');
    var line = '';

    for(var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
            context.textAlign = align;
            context.font = font;
            context.fillStyle = fillStyle;
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
        }
        else {
            line = testLine;
        }
    }

    context.fillText(line, x, y);
}
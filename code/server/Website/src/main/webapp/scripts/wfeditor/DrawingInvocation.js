/* 
 * This object represents a workflow run
 */
function DrawingInvocation(){
    this.INVOCATION_WAITING = 0;
    this.INVOCATION_RUNNING = 1;
    this.INVOCATION_FINISHED_OK = 2;
    this.INVOCATION_FINISHED_WITH_ERRORS = 3;
    this.INVOCATION_WAITING_FOR_DEBUGGER = 4;

    this.submitTime = "";           // Time that the invocation was sent
    this.queuedTime = "";           // Time that the invocation was placed on a queue
    this.dequeuedTime = "";         // Time that the invocation was placed on a queue
    this.startTime = "";            // Time that the invocation was actually started
    this.endTime = "";              // Time that the invocation completed
    this.workflowId = "";
    this.versionId = "";
    this.versionNum = 0;
    this.statusText = "";           // Status message of the run
    this.status = 0;                // Numerical status flag
    this.invocationId = "";         // Database ID of the invocation
    this.folderId = "";             // Native ID of the invocation folder
    this.currentBlockId = "";       // ID of the current block
    this.hasLocks = false;          // Does this invocation have any locks
    this.workflowsRemaining = 0;    // How many sub-workflows are we waiting for
    this.message = null;            // Message describing invocation errors
    this.invocationName = "";       // Name of the invocation
    this.engineId = "";             // ID of the engine that started processing the invocation
}

/** Parse a JSON representation */
DrawingInvocation.prototype.parseJson = function(invocationJson){
    this.submitTime = invocationJson.submitTime;
    this.queuedTime = invocationJson.queuedTime;
    this.dequeuedTime = invocationJson.queuedTime;
    this.startTime = invocationJson.startTime;
    this.endTime = invocationJson.endTime;
    this.status = invocationJson.status;
    this.statusText = invocationJson.statusText;
    this.invocationId = invocationJson.invocationId;
    this.folderId = invocationJson.folderId;
    this.currentBlockId = invocationJson.currentBlockId;
    this.hasLocks = invocationJson.hasLocks;
    this.workflowsRemaining = invocationJson.workflowsRemaining;
    this.versionId = invocationJson.versionId;
    this.versionNum = invocationJson.versionNum;
    this.workflowId = invocationJson.workflowId;
    this.message = invocationJson.message;
    this.invocationName = invocationJson.invocationName;
    this.engineId = invocationJson.engineId;
};

/** Get a data table friendly object representing a row */
DrawingInvocation.prototype.getTableRow = function(index){
    var data = [
        this.index = index,
        this.submitTime,
        this.statusText
    ];
    return data;
}
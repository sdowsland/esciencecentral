function DrawingBlock(){
    this.inputList = new List();                    // Alternative list
    this.outputList = new List();                   // Alternative output list
    this.guid = "";                                 // Block unique ID
    this.name="";                                   // Block name
    this.caption = "";                              // Block caption
    this.populatedCaption = null;                   // Caption with property replacements
    this.label ="";
    this.description = "";                          // Block description
    this.parentDrawing = null;                      // Container drawing
    this.properties = new DrawingPropertyList(this);    // Block properties
    this.serviceId = "";                            // Workflow service ID
    this.versionId = "";                            // Workflow version ID
    this.useLatest = true;                          // Use the latest service version
    this.versionNumber = 0;                         // Version number
    this.dynamicService = false;                    // Does this block represent a dynamic service
    this.top = 50;                                  // Top of block
    this.left = 50;                                 // Left hand edge of block
    this.width = 60;                                // Block width
    this.height = 60;                               // Height of block;
    this.reportJson = null;                         // JSON object containing execution report
    this.fetching = false;                         // Is the drawing data being fetched from the server
    this.callback = function(o){                    // Block data fetch callback
        if(!o.error){
            this.fetcher.parseJson(o.block, true);
            this.fetcher.parentDrawing.repaint();
            
            // Repaint again
            var d = this.fetcher.parentDrawing;
            setTimeout(function(){d.repaint();}, 100);            
            
        } else {
            $.jGrowl("Error fetching block: " + o.message);
        }
    };

    this.highlightExecution = false;                // Should the block be highlighted as it is being executed
    this.totalBytesToStream = 0;
    this.bytesStreamed = 0;
    this.idempotent = true;                // Is the block idempotent
    this.deterministic = true;                // Is the block deterministic
    this.blockImage = new Image();				// Image for drawing block with
    this.blockImage.src = rewriteAjaxUrl("../../styles/common/images/block.jpg");		// Load block image
    this.blockIcon = null;
    this.blockImageLoaded = false;
}

/** Get an input connection by name */
DrawingBlock.prototype.getInput = function(name){
    var i;
    var inp;

    for(i=0;i<this.inputList.size();i++){
        inp = this.inputList.get(i);
        if(inp.name==name){
            return inp;
        }
    }
    return null;
};

/** Delete all the connections from this block */
DrawingBlock.prototype.clearConnections = function(){
    var i;
    var j;
    var port;
    var connectionsToDelete = new List();
	
    for(i=0;i<this.inputList.size();i++){
        port = this.inputList.get(i);
        connectionsToDelete.clear();
		
        for(j=0;j<port.connections.size();j++){
            connectionsToDelete.add(port.connections.get(j));
        }
		
        for(j=0;j<connectionsToDelete.size();j++){
            this.parentDrawing.removeConnection(connectionsToDelete.get(j));
        }
    }
	
    for(i=0;i<this.outputList.size();i++){
        port = this.outputList.get(i);
        connectionsToDelete.clear();
		
        for(j=0;j<port.connections.size();j++){
            connectionsToDelete.add(port.connections.get(j));
        }
		
        for(j=0;j<connectionsToDelete.size();j++){
            this.parentDrawing.removeConnection(connectionsToDelete.get(j));
        }
    }
};

/** Add a new input connection */
DrawingBlock.prototype.addInput = function(inputObject){
    inputObject.makeInputPort();
    inputObject.parentBlock = this;
    //this.inputs.put(inputObject.name, inputObject);
    this.inputList.add(inputObject);
};

/** Get an output connection by name */
DrawingBlock.prototype.getOutput = function(name){
    var i;
    var output;

    for(i=0;i<this.outputList.size();i++){
        output = this.outputList.get(i);
        if(output.name==name){
            return output;
        }
    }
    return null;
};

/** Add a new output connection */
DrawingBlock.prototype.addOutput = function(outputObject){
    outputObject.makeOutputPort();
    outputObject.parentBlock = this;
    //this.outputs.put(outputObject.name, outputObject);
    this.outputList.add(outputObject);
};

/** Render this block onto a canvas context object */
DrawingBlock.prototype.renderBlock = function(ctx, width, height, renderState){
    ctx.lineWidth = 1;
    var selected = this.parentDrawing.isObjectSelected(this);
    /*
	ctx.fillStyle = BLOCK_SHADOW_COLOR;
	ctx.fillRect(this.left + 4, this.top + 4, this.width - 2, this.height - 2);
	ctx.fillStyle = BLOCK_FILL_COLOR;
	ctx.fillRect(this.left, this.top, this.width, this.height);		
	*/
	
    /*
	ctx.strokeStyle = BLOCK_BORDER_COLOR;
	ctx.strokeRect(this.left, this.top, this.width, this.height);	
	*/
    ctx.drawImage(this.blockImage, this.left, this.top);

    // Render the block icon
    if(this.blockIcon==null && this.serviceId){
        this.blockIcon = new Image();
        var blk = this;
        this.blockIcon.onload = function(){
            blk.parentDrawing.repaint();
            blk.blockImageLoaded = true;
        }
        
        this.blockIcon.onerror = function(){
            blk.parentDrawing.repaint();
            blk.blockImageLoaded = true;
        }
        
        this.blockIcon.onabort = function(){
            blk.parentDrawing.repaint();
            blk.blockImageLoaded = true;
        }
        this.blockIcon.src = rewriteAjaxUrl("../../servlets/image?soid=" + this.serviceId + "&type=blockicon");
    }
    
    if(this.blockIcon.complete){
        try {
            var t = (this.height - (this.blockIcon.height)) / 2;
            var l = (this.width - (this.blockIcon.width)) / 2;
            ctx.drawImage(this.blockIcon, this.left + l, this.top + t + 5);
        } catch (e) {
        }
    }

    // Draw execution icon if present
    if(this.reportJson!=null){
        if(this.reportJson.executedOk){
            ctx.drawImage(this.parentDrawing.okIcon, this.left + this.width - 20, this.top + this.height - 20);
        } else {
            if(this.reportJson.executionStatus==0){
                // Block not executed yet
                ctx.drawImage(this.parentDrawing.blockWaitingImage, this.left + this.width - 20, this.top + this.height - 20);
                
            } else if(this.reportJson.executionStatus==4) {
                // Upstream error somewhere
                ctx.drawImage(this.parentDrawing.upstreamErrorsImage, this.left + this.width - 20, this.top + this.height - 20);
                
            } else {
                // General error
                ctx.drawImage(this.parentDrawing.errorIcon, this.left + this.width - 20, this.top + this.height - 20);
            }
        }
    }

    if(this.highlightExecution) {
        //Add the cog to the block image
        ctx.drawImage(this.parentDrawing.blockExecutingImage, this.left + this.width - 20, this.top + this.height - 20);

        //Display the progress
        if (this.totalBytesToStream != undefined && this.totalBytesToStream != 0 && this.bytesStreamed != undefined && this.bytesStreamed != 0) {

            var startx =  this.left + PROGRESS_PIE_MARGIN;
            var starty  = this.top + this.height - PROGRESS_PIE_MARGIN;
            var segmentWidth  = (this.bytesStreamed / this.totalBytesToStream) * 2 * Math.PI;

            ctx.strokeStyle = '#000000';
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.moveTo(startx + PROGRESS_PIE_RADIUS, starty);
            ctx.arc(startx, starty, PROGRESS_PIE_RADIUS, 0, 2 * Math.PI, false);
            ctx.lineTo(startx + PROGRESS_PIE_RADIUS, starty);
            ctx.closePath();
            ctx.stroke();
            
            ctx.fillStyle = '#3869A5';
            ctx.beginPath();
            ctx.moveTo(startx,starty);
            ctx.arc(startx, starty, PROGRESS_PIE_RADIUS,  1.5*Math.PI, segmentWidth+(1.5*Math.PI));
            ctx.lineTo(startx,starty);
            ctx.closePath();
            ctx.fill();

        }
    }
    // Drawing version
    if(!this.useLatest){
        ctx.font = "8pt arial";
        ctx.textAlign = 'left';
        ctx.fillStyle = BLOCK_VERSION_TEXT_COLOR;
        ctx.fillText("v" + this.versionNumber, this.left + 5, this.top + this.height - 5);
    }

    // Performance data
    if(this.durationModel){
        ctx.font = "8pt arial";
        ctx.textAlign = 'left';
        ctx.fillStyle = BLOCK_VERSION_TEXT_COLOR;
        ctx.fillText((this.durationModel.predictedValue / 1000.0).toFixed(2) + "s", this.left + 30, this.top + this.height - 5);        
    }
    
    ctx.lineWidth = 0.5;
	
    // Only render text in IE if there are no movements going on
    var renderText = true;
    if(this.parentDrawing.isIE){
        // IE render phase check
        if(renderState==DRAGGING_OBJECTS || renderState==DRAWING_BOX || renderState==CONNECTING_PORTS){
            renderText = false;
        }
    }
	
    if(ctx.fillText && renderText){
        var changed = this.properties.listNonDefaultProperties();
        if(changed.length>0){
            ctx.font = "bold 8pt arial";
        } else {
            ctx.font = "8pt arial";
        }
        
        ctx.textAlign = 'left';
        ctx.fillStyle = BLOCK_TITLE_COLOR;


        // Set the clipping area
        if(!selected){
            ctx.save();
            ctx.beginPath();
            ctx.rect(this.left, this.top, this.width, 15);
            ctx.clip();
        }
        ctx.fillText(this.label, this.left + 3, this.top + 12);
        if(!selected){
            ctx.restore();
        }
        
        if(this.populatedCaption){
            if(this.populatedCaption.indexOf(';')!==-1){
                // Multi-line caption
                var topY = this.top + this.height + 14;
                var captionArray = this.populatedCaption.split(';');
                for(var i=0;i<captionArray.length;i++){
                    ctx.fillText(captionArray[i], this.left, topY);
                    topY+=MULTI_ROW_HEIGHT;
                }
            } else {
                // Single-line caption
                ctx.fillText(this.populatedCaption, this.left, this.top + this.height + 14);
            }
        } else if(this.caption){
            ctx.fillText(this.caption, this.left, this.top + this.height + 14);
        }        
    }
	
    // Draw the input and output ports
    this.drawIOs(ctx);

    if(selected){
        ctx.fillStyle = BLOCK_SELECTION_HANDLE_COLOR;
        ctx.fillRect(this.left - SELECTION_HANDLE_SIZE, this.top - SELECTION_HANDLE_SIZE, SELECTION_HANDLE_SIZE, SELECTION_HANDLE_SIZE);
        ctx.fillRect(this.left + this.width, this.top - SELECTION_HANDLE_SIZE, SELECTION_HANDLE_SIZE, SELECTION_HANDLE_SIZE);
        ctx.fillRect(this.left - SELECTION_HANDLE_SIZE, this.top + this.height, SELECTION_HANDLE_SIZE, SELECTION_HANDLE_SIZE);
        ctx.fillRect(this.left + this.width, this.top + this.height, SELECTION_HANDLE_SIZE, SELECTION_HANDLE_SIZE);
    };
    	
};

DrawingBlock.prototype.updateCaption = function(){
    var prop = this.properties.getPropertyByName("Caption");
    if(prop!=null){
        this.caption = prop.value;
        
        // There may be a property to show
        if(this.caption && this.caption.indexOf('$')!=-1){
            var updatedCaption = this.caption;
            var property;
            var size = this.properties.getPropertyCount();
            for(i=0;i<size;i++){
                property = this.properties.getProperty(i);
                updatedCaption = updatedCaption.replace("${" + property.name + "}", property.toString());
            }
            this.populatedCaption = updatedCaption;
        } else {
            this.populatedCaption = null;
        }
    } else {
        this.populatedCaption = null;
    }
};

DrawingBlock.prototype.updateName = function(){
    var prop = this.properties.getPropertyByName("Name");
    if(prop!=null){
        this.name = prop.value;
    }
    
}


/** Is a canvas event within the block bounds */
DrawingBlock.prototype.withinBounds = function(x, y){
    if(x>=this.left && x<=(this.left+this.width) && y>=this.top && y<=(this.top+this.height)){
        return true;
    } else {
        return false;
    }
};

/** Is this block contained by a given box */
DrawingBlock.prototype.isContainedBy = function(p1, p2){
    var x1;
    var x2;
    var y1;
    var y2;
	
    x1 = Math.min(p1.x, p2.x);
    y1 = Math.min(p1.y, p2.y);
    x2 = Math.max(p1.x, p2.x);
    y2 = Math.max(p1.y, p2.y);
	
    if(this.left >= x1 && (this.left + this.width) <= x2 && this.top >= y1 && (this.top + this.height)<=y2){
        return true;
    } else {
        return false;
    }
	
};

/** Draw the inputs and outputs of this block on a canvas context */
DrawingBlock.prototype.drawIOs = function(ctx){
    var inp;
    var p;
    var i;
    var j;

    // Draw the inputs
    for(i=0;i<this.inputList.size();i++){
        inp = this.inputList.get(i);
        p = this.locatePort(inp);
        inp.renderPort(ctx, p);
    }

    // Drawing the outputs and include the output connections
    for(i=0;i<this.outputList.size();i++){
        inp = this.outputList.get(i);
        p = this.locatePort(inp);
        inp.renderPort(ctx, p);

        for(j=0;j<inp.connections.size();j++){
            inp.connections.get(j).renderConnection(ctx);
        }
    }
};

/** Move this block by an offset */
DrawingBlock.prototype.moveBy = function(xOffset, yOffset){
    this.left = this.left + xOffset;
    this.top = this.top + yOffset;

    // Move all of the output connections
    var i;
    var port;
    var j;
    for(i=0;i<this.outputList.size();i++){
        port = this.outputList.get(i);
        for(j=0;j<port.getConnectionCount();j++){
            port.getConnection(j).moveBy(xOffset, yOffset);
        }
    }
    this.parentDrawing.dirty = true;
};

/** Calculate the rendering position of a port */
DrawingBlock.prototype.locatePort = function(drawingPort){
    if(drawingPort.location==LEFT_OF_BLOCK){
        return new DrawingPoint(this.left + 1, this.top + (this.getPortCentrePoint(drawingPort)));
    } else if(drawingPort.location==RIGHT_OF_BLOCK){
        return new DrawingPoint(this.left + this.width + 1, this.top + (this.getPortCentrePoint(drawingPort)));
    } else if(drawingPort.location==TOP_OF_BLOCK){
        return new DrawingPoint(this.left + this.getPortCentrePoint(drawingPort), this.top);
    } else if(drawingPort.location==BOTTOM_OF_BLOCK){
        return new DrawingPoint(this.left + this.getPortCentrePoint(drawingPort), this.top + this.height);
    }
};

/** Get the point on a port that should be used when connecting */
DrawingBlock.prototype.locatePortConnectionPoint  = function(drawingPort) {
    var p = this.locatePort(drawingPort);
    var portHalf;
	
    if(drawingPort.type=="output"){
	
        if(drawingPort.location==LEFT_OF_BLOCK){
            return new DrawingPoint(p.x - OUTPUT_PORT_SIZE, p.y);
        } else if(drawingPort.location==RIGHT_OF_BLOCK){
            return new DrawingPoint(p.x + OUTPUT_PORT_SIZE, p.y);
        } else if(drawingPort.location==TOP_OF_BLOCK){
            return new DrawingPoint(p.x, p.y - OUTPUT_PORT_SIZE);
        } else if(drawingPort.location==BOTTOM_OF_BLOCK){
            return new DrawingPoint(p.x, p.y + OUTPUT_PORT_SIZE);
        }
    } else {
        if(drawingPort.location==LEFT_OF_BLOCK){
            return new DrawingPoint(p.x - INPUT_PORT_SIZE, p.y);
        } else if(drawingPort.location==RIGHT_OF_BLOCK){
            return new DrawingPoint(p.x + INPUT_PORT_SIZE, p.y);
        } else if(drawingPort.location==TOP_OF_BLOCK){
            return new DrawingPoint(p.x, p.y - INPUT_PORT_SIZE);
        } else if(drawingPort.location==BOTTOM_OF_BLOCK){
            return new DrawingPoint(p.x, p.y + INPUT_PORT_SIZE);
        }
    }
};

/** Locate the centre point of a port */
DrawingBlock.prototype.getPortCentrePoint = function(port){
    if(port.portLocation==TOP_OF_BLOCK || port.portLocation==BOTTOM_OF_BLOCK){
        return this.width * (port.portOffset / 100.0);
    } else {
        var h = port.offset;
		
        var oset = h / 100;
        return this.height * oset;
    }
};

/** Get the number of output connections for this block */
DrawingBlock.prototype.getOutputConnectionCount = function(){
    var connectionCount = 0;
    var i;
    for(i=0;i<this.outputList.size();i++){
        connectionCount = connectionCount + this.outputList.get(i).connections.size();
    }
    return connectionCount;
};

/** Fetch the block JSON from the server */
DrawingBlock.prototype.fetchData = function(){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=getServiceJson");

    // Setup the block icon
    this.blockIcon = new Image();
    this.blockIcon.src = rewriteAjaxUrl("../../servlets/image?soid=" + this.serviceId + "&type=blockicon");
    
    if (this.serviceId != "") {
        if(this.editor!=null){
            this.editor.repaint();
        }
        var callData;
        if(this.versionId!=null){
            callData = {
                serviceId: this.serviceId,
                versionId: this.versionId
            }
        } else {
            callData = {
                serviceId: this.serviceId
            };
        }
        var callString = JSON.stringify(callData);
        this.fetching = true;
        $.ajax({
            type: 'POST',
            url: url,
            data: callString,
            xhrFields: {
                withCredentials: true
            },             
            success: this.callback,
            dataType: "json",
            fetcher: this
        });
    }

};

/** Create a block from a JSON block representation */
DrawingBlock.prototype.parseJson = function(blockJson, ignoreLocation){

    // Basic properties
    this.guid = blockJson.guid;
    this.caption = blockJson.caption;
    this.label = blockJson.label;
    this.serviceId = blockJson.serviceId;
    this.useLatest = blockJson.latestVersion;
    this.versionNumber = blockJson.versionNumber;
    this.dynamicService = blockJson.dynamicService;
    this.description = blockJson.description;
    this.idempotent = blockJson.idempotent;
    this.deterministic = blockJson.deterministic;

    // Setup the block icon
    this.blockIcon = new Image();
    var blk = this;
    this.blockImageLoaded = false;
    
    this.blockIcon.onload = function(){
        blk.blockImageLoaded = true;
    }
    
    this.blockIcon.onerror = function(){
        blk.blockImageLoaded = true;
    }
    
    this.blockIcon.onabort = function(){
        blk.blockImageLoaded = true;
    }
    
    this.blockIcon.src = rewriteAjaxUrl("../../servlets/image?soid=" + this.serviceId + "&type=blockicon");
    
    
    if(blockJson.versionId){
        this.versionId = blockJson.versionId;
    }

    // Position
    if(!ignoreLocation){
        var location = blockJson.location;
        this.left = location.left;
        this.top = location.top;
        this.width = location.width;
        this.height = location.height;
    }

    // Inputs
    var inputs = blockJson.inputs;
    var input;
    var inputJson;
    var inputCount = inputs.inputCount;
    var typeCount;
    var i;
    var j;

    for(i=0;i<inputCount;i++){
        inputJson = inputs.inputArray[i];
        input = new DrawingPort(inputJson.name, inputJson.location, inputJson.offset, this);
        input.streamable = inputJson.streamable;
        input.optional = inputJson.optional;
        
        // Set up supported data types
        typeCount = inputJson.typeCount;
        for(j=0;j<typeCount;j++){
            input.addSupportedType(inputJson.typeArray[j]);
        }

        this.addInput(input);
    }

    // Outputs
    var outputs = blockJson.outputs;
    var output;
    var outputJson;
    var outputCount = outputs.outputCount;
    for(i=0;i<outputCount;i++){
        outputJson = outputs.outputArray[i];
        output = new DrawingPort(outputJson.name, outputJson.location, outputJson.offset, this);

        // Set up supported data types
        typeCount = outputJson.typeCount;
        for(j=0;j<typeCount;j++){
            output.addSupportedType(outputJson.typeArray[j]);
        }

        this.addOutput(output);
    }

    // Editable properties
    this.properties.parseJson(blockJson.properties);
    if(this.fetching){
        this.fetching = false;
        this.setBlockName(this.name);   // Overwrite property with name
    } else {
        this.setBlockName(blockJson.name);
    }
    
    var labelProperty = this.properties.getPropertyByName("Label");
    if(labelProperty){
        labelProperty.value = blockJson.label;
    }
    this.updateCaption();
    return this;
};

DrawingBlock.prototype.setBlockName = function(name){
    this.name = name;
    this.properties.addString("Name", name, "Block name");
};
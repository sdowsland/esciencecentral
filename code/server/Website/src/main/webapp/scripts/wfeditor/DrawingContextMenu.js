/* 
 * This file provides a context menu that can sit above a block
 */
function DrawingContextMenu(){
    this.block = null;              // Block being edited
    this.drawing = null;            // Drawing containing block
    this.commands = new List();     // Array list of commands
    this.relativeTop = -30;         // Relative position of top of menu
    this.relativeLeft = -16;        // Relative left hand position
}

/** Is a co-ordinate within the editor bounds */
DrawingContextMenu.prototype.withinBounds = function(x, y){
    var left = this.getLeft();
    var top = this.getTop();
    var height = this.getHeight();
    var width = this.getWidth();

    if(x>=left && y>=top && x<=(left + width) && y<=(top + height)){
        return true;
    } else {
        return false;
    }
};

/** Activate a command using a co-ordinate */
DrawingContextMenu.prototype.executeCommandByCoOrdinate = function(x, y){
    var i;
    var cmd;
    for(i=0;i<this.commands.size();i++){
        cmd = this.commands.get(i);
        if(cmd.withinBounds(x, y)){
            cmd.execute();
        }
    }
}

/** Get the top of the menu */
DrawingContextMenu.prototype.getTop = function(){
    if(this.block!=null){
        return this.block.top + this.relativeTop;
    } else {
        return 0;
    }
};

/** Get the left hand co-ordinate of the menu */
DrawingContextMenu.prototype.getLeft = function(){

    if(this.block!=null){
        //return this.block.left + this.relativeLeft;
        var width = this.getWidth();
        return this.block.left - ((width - this.block.width) / 2);
    } else {
        return 0;
    }
};

/** Calculate the width of this menu based on the number of commands in it */
DrawingContextMenu.prototype.getWidth = function(){
    return 10 + (this.commands.size() * 20);
};

/** Calculate the height of this menu */
DrawingContextMenu.prototype.getHeight = function(){
    return 26;
}

/** Render this menu onto a canvas context */
DrawingContextMenu.prototype.renderMenu = function(ctx){
    ctx.lineWidth = 1;
    var left = this.getLeft();
    var top = this.getTop();
    var height = this.getHeight();
    var width = this.getWidth();

    ctx.fillStyle = DRAWING_CONTEXT_MENU_FILL_COLOR;
    ctx.fillRect(left, top, width, height);
    ctx.strokeStyle = DRAWING_CONTEXT_MENU_BORDER_COLOR;
    ctx.strokeRect(left, top, width, height);

    var i;
    var cmd;
    var pos;
    for(i=0;i<this.commands.size();i++){
        cmd = this.commands.get(i);
        pos = left + 5 + (i * 20);
        ctx.drawImage(cmd.commandIcon, pos, top + 5);
        cmd.x1 = pos - 2;
        cmd.y1 = top + 3;
        cmd.x2 = pos - 2 + 20;
        cmd.y2 = top + 3 + 20;
    }
}

/** Add a command to this menu */
DrawingContextMenu.prototype.addCommand = function(cmd){
    this.commands.add(cmd);
    cmd.parentMenu = this;
}

/** Get a command by name */
DrawingContextMenu.prototype.getCommandByName = function(name){
    var i;
    var cmd;
    for(i=0;i<this.commands.size();i++){
        cmd = this.commands.get(i);
        if(cmd.name===name){
            return cmd;
        }
    }
    return null;
};

/*
 * This object provides a command that sits within the drawing context
 * menu
 */
function DrawingContextMenuCommand(name, callback, imageUrl){
    this.commandIcon = new Image();
    this.parentMenu = null;
    this.callback = callback;
    this.name = name;
    this.setIconUrl(imageUrl);
    this.enabled = true;
    this.x1 = 0;
    this.x2 = 0;
    this.y1 = 0;
    this.y2 = 0;
}

/** Is a co-ordinate within bounds */
DrawingContextMenuCommand.prototype.withinBounds = function(x, y){
    if(x>=this.x1 && y>=this.y1 && x<=this.x2 && y<=this.y2){
        return true;
    } else {
        return false;
    }
}

/** Set the image */
DrawingContextMenuCommand.prototype.setIconUrl = function(url){
    this.commandIcon.src = url;
}

/** Activate this command */
DrawingContextMenuCommand.prototype.execute = function(){
    if(this.callback && this.parentMenu && this.parentMenu.block && this.enabled){
        this.callback(this.parentMenu.block);
    }
}

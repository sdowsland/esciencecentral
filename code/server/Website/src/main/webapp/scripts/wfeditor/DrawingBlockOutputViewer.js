/* 
 * This script provides a dialog that can view the output of a workflow block
 * after it has finished or as it is running.
 */
function DrawingBlockOutputViewer(){
    this.divName = "";
    this.invocationId = null;
    this.contextId = null;    
    this.resultsArea = null;
}

DrawingBlockOutputViewer.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        // Results area
        this.resultsArea = document.createElement("div");
        this.resultsArea.setAttribute("class", "ui-corner-all consolestandard");
        this.resultsArea.setAttribute("style", "");
        this.resultsArea.setAttribute("disabled", "disabled");  
        div.appendChild(this.resultsArea);
        var dialog = this;
        
        $("#" + this.divName).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 550,
            width: 800,
            title: "Block Output",
            modal: true,
            buttons: {
                'Refresh' : function(){
                    dialog.fetchOutput();
                },
                        
                'Close': function()
                {
                    $(this).dialog('close');
                }

            }, 
            open: function(){
                dialog.resizeUI();
            }
        });
        
        $('#' + this.divName).bind("dialogresize", function(){
            dialog.resizeUI();
        });        
    }
};

DrawingBlockOutputViewer.prototype.resizeUI = function(){
    var divHeight;
    var divWidth;
    var div = document.getElementById(this.divName);
    if(div){
        divHeight = div.clientHeight;
        divWidth = div.clientWidth;
        this.resultsArea.style.height = (divHeight - 25) + "px" ;
    }
    
};

DrawingBlockOutputViewer.prototype.showWithOutput = function(invocationId, contextId){
    this.invocationId = invocationId;
    this.contextId = contextId;
    this.setStandardOutputStyle();
    this.resultsArea.innerHTML = "<pre></pre>";
    $("#" + this.divName).dialog('open');
    this.fetchOutput();
};

DrawingBlockOutputViewer.prototype.fetchOutput = function(){
    var callData = {
        invocationId: this.invocationId,
        contextId: this.contextId
    };
    
    var dialog = this;
    var callback = function(o){
        if(!o.error){
            if(o.liveOutput){
                dialog.setLiveOutputStyle();
            } else {
                dialog.setStandardOutputStyle();
            }
            if(o.outputText===""){
                dialog.resultsArea.innerHTML = "<pre>No block output available</pre>";
            } else {
                dialog.resultsArea.innerHTML = "<pre>" + o.outputText + "</pre>";
                dialog.resultsArea.scrollTop = dialog.resultsArea.scrollHeight; //scroll to bottom of output
            }
        } else {
            dialog.resultsArea.innerHTML = "<pre>Error</pre>";
        }
    };
    jsonCall(callData, "../../servlets/workflow?method=fetchLiveBlockOutput", callback);
    this.resultsArea.innerHTML = "<pre>Fetching...</pre>";
};

DrawingBlockOutputViewer.prototype.setLiveOutputStyle = function(){
    $(this.resultsArea).removeClass("consolestandard");
    $(this.resultsArea).addClass("consolelive");
};

DrawingBlockOutputViewer.prototype.setStandardOutputStyle = function(){
    $(this.resultsArea).addClass("consolestandard");
    $(this.resultsArea).removeClass("consolelive");    
};
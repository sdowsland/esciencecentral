<%@page import="com.connexience.server.web.util.ScriptMerger" %>
<%
    ScriptMerger _merger = new ScriptMerger("", "../../scripts");
    String jQueryVersion = "1.8.3";//1.7.2"; //"1.4.2"
    String jQueryUIVersion = "1.9.2"; //"1.8.24";// "1.8.5";
%>
<!DOCTYPE HTML>
<html>
<head>
    <%@include file="../../WEB-INF/jspf/page/ticket.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/theme.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/organisationredirect.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery/<%=jQueryVersion%>/jquery.js"></script>
    <%@include file="../../WEB-INF/jspf/includes/jquery-ui.jspf" %>
    <%@include file="../../WEB-INF/jspf/includes/scriptloader.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery-validate/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $("#regForm").validate({
                rules: {
                    firstname: "required",
                    lastname: "required",
                    email: {
                        required:true,
                        email: true
                    },
                    password1: "required",
                    password2:{
                        equalTo: "#password1"
                    },
                    tandc:"required",
                    cResponse: "required"
                },
                messages: {
                    firstname: "Please enter your first name",
                    lastname: "Please enter your last name",
                    email: {
                        email: "Your email address must be in the format of name@domain.com"
                    },
                    password: "Passwords must match",
                    repeatpassword: "Passwords must match",
                    tandc: "You must agree to the terms and conditions",
                    cResponse: "Please enter the text in the picture"
                }
            });

            $("#captchaimage").click(function() {
                $("#captchaimage").attr("src", "../jcaptcha?" + new Date().getTime())
            });

            $('#promoCode').focus();
        });
    </script>

    <%
        String email = request.getParameter("email");
        String firstName = request.getParameter("firstname");
        String surname = request.getParameter("surname");
        String error = request.getParameter("signupError");
        if (email == null) {
            email = "";
        }
        if (firstName == null) {
            firstName = "";
        }
        if (surname == null) {
            surname = "";
        }
        if (error == null) {
            error = "";
        }
    %>
    <style type="text/css">
        #regForm label.error {
            color: red;
            padding-left: 10px;
            line-height: normal;
        }

        #regForm .error {
            display: block;
        }

        .hyphen {
            width: 6px;
            float: left;
            padding-top: 10px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
<div id="wrapper" class="container clearfix">
    <div class="row-fluid">
        <div class="span6" style="margin:25px 0;">
            <a href="../../pages/front/index.jsp">
                <h1 id="logo" class="grid_6">
                    <img src="../../styles/<%=themeFolder%>/images/logo_head.png"/>
                </h1>
                <h4 id="tagline"><%=tagLine%>
                </h4>
            </a>
        </div>
        <hr style="clear:both;">
        <h2>Sign upto <%=siteName%></h2>

        <div class="row-fluid">
        <%-- Main Content Left Hand Side--%>
            <div class="span12">

                <%
                    if (!error.equals("")) {
                %>
                <div style="padding-top: 20px; padding-bottom: 5px; padding-left: 20px; font-size:15px; color:red">
                    <p><%
                        if (error.equals("captcha")) {
                            out.println("The letters you entered do not match the image, please try again");
                        } else if (error.equals("blankFields")) {
                            out.println("Please fill in all the fields");
                        } else if (error.equals("nopass")) {
                            out.println("The passwords you entered do not match");
                        } else if (error.equals("exists")) {
                    %>
                        That email address already exists on the system. Please choose another or <a
                                href="mailto:s.j.woodman@ncl.ac.uk;h.g.hiden@ncl.ac.uk?subject=PasswordReminder">email</a> us to
                        ask for a password reminder.
                        <%
                        } else if (error.equals("accessCode")) {
                        %>
                        Access Code Error. Access to <%=siteName%> is restricted and an access code is required.  Please contact your
                        System administrator to request access.
                        <%
                        } else if (error.equals("error")) {
                        %>
                        Something has gone wrong, please <a
                                href="mailto:s.j.woodman@ncl.ac.uk;h.g.hiden@ncl.ac.uk?subject=PasswordReminder">email</a> us to
                        and we will investigate.
                        <%
                            }
                        %></p>
                </div>
                <%
                    }
                %>

                <%
                    if ("true".equals(pageContext.getServletContext().getInitParameter("accessCodeEnabled"))) {
                %>
                <div><p>An Access code is required to sign up for e-Science Central.  Please contact your System Administrator for access.</p>
                </div>
                <%
                    }
                %>
                <div class="dialog">
                    <form action="stage2.jsp" method="POST" id="regForm">
                        <%
                            if ("true".equals(pageContext.getServletContext().getInitParameter("accessCodeEnabled"))) {
                        %>
                        <div class="control-group">
                            <label class="control-label">Access Code</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Access Code" id="accessCode" name="accessCode" class="input-xlarge" value=""/>
                            </div>
                        </div>
                                <%
                                    }
                                %>
                        <div class="control-group">
                            <label class="control-label">First Name</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="First Name" id="firstname" name="firstname" class="input-xlarge" value="<%=firstName%>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Last Name</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Last Name" id="lastname" name="lastname" class="input-xlarge" value="<%=surname%>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Email" id="email" name="email" class="input-xlarge" value="<%=email%>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <input type="password" size="75" placeholder="Password" id="password1" name="password1" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Repeat Password</label>
                            <div class="controls">
                                <input type="password" size="75" placeholder="Repeat Password" id="password2" name="password2" class="input-xlarge"/>
                            </div>
                        </div>
                        <%
                            if ("true".equals(pageContext.getServletContext().getInitParameter("jcaptcha_enabled")))
                            {
                        %>
                        <div class="control-group">
                            <label class="control-label">Click to load new image</label>
                            <div class="controls">
                                <img style="margin:10px 0 13px 10px;" id="captchaimage" src="../../jcaptcha"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Enter the characters above</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Security Key" id="cResponse" name="cResponse" class="input-xlarge"/>
                            </div>
                        </div>
                        <%
                            }
                        %>
                        <div class="control-group">
                            <div class="controls">
                                <label class="control-label">
                                    <input type="checkbox" id="tandc" name="tandc"/>
                                    I agree to the <%=siteName%><a href="../../unsecured/signup/tandc.jsp" target="_blank"> terms and conditions</a>
                                </label>
                            </div>
                        </div>
                        <div class="control-group">
                            <input type="submit" value="Register" class="btn" id="regSubmit"/>
                        </div>

                        <input type="hidden" name="register" value="true">

                    </form>
                </div>

            </div>
        </div>

        <div id="push"></div>
    </div>
</div>
<!-- Footer -->
<div id="footer">
    <div class="container">
        <p class="copyright">&copy; Copyright <%=siteName%> </p>

        <div id="partners" class="clearfix">
            <div class="partner-logo">
                <img src="../../styles/esc/images/science-city.png" alt="Newcastle Science City"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/jisc.png" alt="JISC"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/epsrc.png" alt="EPSRC"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/newcastle-university.png" alt="Newcastle University"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/side.png" alt="SiDE"/>
            </div>
        </div>
    </div>
</div>

</body>
</html>

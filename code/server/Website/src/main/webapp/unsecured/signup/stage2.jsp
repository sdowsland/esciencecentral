<%@page contentType="text/html" pageEncoding="UTF-8" %>

<%@page import="com.connexience.server.ejb.util.EJBLocator" %>
<%@page import="com.connexience.server.model.security.User" %>
<%@ page import="com.connexience.server.web.util.WebUtil" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.connexience.server.model.security.Ticket" %>
<%@ page import="com.connexience.server.model.security.WebTicket" %>
<%@ page import="com.connexience.server.util.SessionUtils" %>
<%@ page import="javax.script.ScriptEngineManager" %>
<%@ page import="javax.script.ScriptEngine" %>
<%@ page import="com.sun.jersey.api.client.Client" %>
<%@ page import="com.sun.jersey.api.client.WebResource" %>
<%@ page import="javax.ws.rs.core.MediaType" %>
<%@ page import="com.sun.jersey.api.client.ClientResponse" %>
<%@ page import="com.sun.jersey.api.representation.Form" %>
<%@ page import="javax.ws.rs.core.Response" %>
<%@ page import="com.connexience.server.model.organisation.Organisation" %>
<%@ page import="com.connexience.provenance.client.ProvenanceLoggerClient" %>
<%@ page import="com.connexience.provenance.model.logging.events.LogonEvent" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<%
    String createUser = request.getParameter("register");

    if (createUser != null && createUser.equals("true")) {
        try {

            // Extract the relevant fields from the form
            String email = request.getParameter("email");
            String firstName = request.getParameter("firstname");
            String surname = request.getParameter("lastname");
            String password1 = request.getParameter("password1");
            String password2 = request.getParameter("password2");


            //Figure out the URL the user is coming from
            String originalURL = WebUtil.getHostname(request) + request.getContextPath();

            if ("true".equals(pageContext.getServletContext().getInitParameter("accessCodeEnabled"))) {
                Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(new WebTicket());
                String macAddressAccessKey = org.getAccessKey().replace(":", "-").toUpperCase();
                String submittedAccessKey = request.getParameter("accessCode").replace(":", "-").toUpperCase();
                if(submittedAccessKey.isEmpty() || (!submittedAccessKey.equals(macAddressAccessKey))) {
                    response.sendRedirect("../signup/stage1.jsp?signupError=accessCode&firstname=" + firstName + "&surname=" + surname + "&email=" + email);
                    return;
                }
            }

            if (email == null || email.equals("") || firstName == null || firstName.equals("") || surname == null || surname.equals("") || password1 == null || password1.equals("")) {
                response.sendRedirect("../signup/stage1.jsp?signupError=blankFields&firstname=" + firstName + "&surname=" + surname + "&email=" + email);
                return;
            } else if (!password1.equals(password2)) //passwords match
            {
                response.sendRedirect("../signup/stage1.jsp?signupError=nopass&firstname=" + firstName + "&surname=" + surname + "&email=" + email);
                return;
            } else if (EJBLocator.lookupUserDirectoryBean().getUserFromEmailAddress(email) != null) {
                response.sendRedirect("../signup/stage1.jsp?signupError=exists&firstname=" + firstName + "&surname=" + surname + "&email=" + email);
                return;
            } else {
                User cuser = new User();
                // Create the user
                cuser.setFirstName(firstName);
                cuser.setSurname(surname);
                cuser.setName(firstName + " " + surname);

                try {
                    cuser = EJBLocator.lookupUserDirectoryBean().createAccount(cuser, email, password1, originalURL);

                    /* Log user into website */
                    SessionUtils.login(request, email, password1);

                    LogonEvent logonEvent = new LogonEvent(LogonEvent.REMEMBERED_LOGON, cuser.getDisplayName(), WebUtil.getClientIP(request));
                    ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                    client.log(logonEvent);

                    session.setAttribute("email", email);
                    session.setAttribute("password", password1);

                    //The below URL to rediret to is one level below as it's going to get redirected in a servlet
                    response.sendRedirect("../signup/stage3.jsp?nextPage=" + URLEncoder.encode("../pages/front/index.jsp", "UTF-8"));

                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    response.sendRedirect("../signup/stage1.jsp?signupError=error&firstname=" + firstName + "&surname=" + surname + "&email=" + email);
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("../signup/stage1.jsp?signupError=error");
            return;
        }
    } else {
        response.sendRedirect("../signup/stage1.jsp");
    }
%>
</html>
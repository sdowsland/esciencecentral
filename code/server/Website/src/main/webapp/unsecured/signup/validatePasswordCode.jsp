<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@ page import="com.connexience.server.web.util.FlashUtils" %>
<%@ page import="com.connexience.server.web.util.ScriptMerger" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%
    ScriptMerger _merger = new ScriptMerger("", "../../scripts");
    String jQueryVersion = "1.8.3";//1.7.2"; //"1.4.2"
    String jQueryUIVersion = "1.9.2"; //"1.8.24";// "1.8.5";
%>

<head>
    <%@include file="../../WEB-INF/jspf/page/ticket.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/theme.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/organisationredirect.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery/<%=jQueryVersion%>/jquery.js"></script>
    <%@include file="../../WEB-INF/jspf/includes/jquery-ui.jspf" %>
    <%@include file="../../WEB-INF/jspf/includes/scriptloader.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery-validate/jquery.validate.js"></script>

    <%
        UserDirectoryRemote ud = EJBLocator.lookupUserDirectoryBean();
        String code = request.getParameter("code");
        if (code != null && !code.equals("")) {
            if (!ud.validatePasswordRecoveryCode(code)) {
                FlashUtils.setError(request, "Invalid or expired link to reset password.  Fill in the form to receive another code.");
                response.sendRedirect("../../unsecured/signup/resetPassword.jsp");
                return;
            } else {
                //do nothing - valid code
            }
        } else {
            FlashUtils.setError(request, "Invalid link to reset password");
            response.sendRedirect("../../unsecured/signup/resetPassword.jsp");
            return;
        }

    %>


    <script type="text/javascript">
        $(document).ready(function () {
            clearTabs();

            $("#pwForm").validate({
                rules: {
                    password1: "required",
                    password2: {
                        equalTo: "#password1"
                    }
                },
                messages: {
                    password1: "Passwords must match",
                    password2: "Passwords must match"
                }
            });


            $('#password1').focus();

        });

    </script>

    <style type="text/css">
        #pwForm label.error {
            color: red;
            padding-left: 10px;
            line-height: normal;
        }

        #pwForm .error {
            display: block;
        }

        .hyphen {
            width: 6px;
            float: left;
            padding-top: 10px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
<div id="wrapper" class="container clearfix">
    <div class="row-fluid">
        <div class="span6" style="margin:25px 0;">
            <a href="../../pages/front/index.jsp">
                <h1 id="logo" class="grid_6">
                    <img src="../../styles/<%=themeFolder%>/images/logo_head.png"/>
                </h1>
                <h4 id="tagline"><%=tagLine%>
                </h4>
            </a>
        </div>
        <hr style="clear:both;">

        <!-- Caption Line -->
        <h2 class="grid_12 caption">Reset your <span>password</span></h2>


        <div class="hr grid_12 clearfix">&nbsp;</div>

        <div class="grid_12 content">
            <p>Enter new password</p>

            <div class="dialog">
                <form action="changePasswordFromCode.jsp" method="POST" id="pwForm" autocomplete="off">

                    <div class="formInput">
                        <label for="password1">Password</label>
                        <input type="password" id="password1" name="password1"/>
                    </div>
                    <div class="formInput">
                        <label for="password2">Password again</label>
                        <input type="password" id="password2" name="password2"/>
                    </div>

                    <input type="hidden" name="code" value="<%=code%>" id="code"/>

                    <div class="formInput">
                        <label>&nbsp;</label>

                        <input style="width:518px" id="submit" type="submit" value="Reset">

                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div id="footer">
        <div class="container">
            <p class="copyright">&copy; Copyright <%=siteName%>
            </p>

            <div id="partners" class="clearfix">
                <div class="partner-logo">
                    <img src="../../styles/esc/images/science-city.png" alt="Newcastle Science City"/>
                </div>
                <div class="partner-logo">
                    <img src="../../styles/esc/images/jisc.png" alt="JISC"/>
                </div>
                <div class="partner-logo">
                    <img src="../../styles/esc/images/epsrc.png" alt="EPSRC"/>
                </div>
                <div class="partner-logo">
                    <img src="../../styles/esc/images/newcastle-university.png" alt="Newcastle University"/>
                </div>
                <div class="partner-logo">
                    <img src="../../styles/esc/images/side.png" alt="SiDE"/>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

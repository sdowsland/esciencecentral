<%@ page import="com.connexience.server.ejb.util.EJBLocator" %>
<%
    if (EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(null) != null) {
        response.sendRedirect("../../index.jsp");
    }
%>
<%@page import="com.connexience.server.web.util.ScriptMerger" %>
<%@page import="com.connexience.server.ejb.util.EJBLocator" %>
<%@page import="com.connexience.server.web.util.WebUtil" %>
<%@page import="java.net.URLEncoder" %>
<%@page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@page import="com.connexience.server.model.security.Group" %>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@page import="com.connexience.server.web.util.FlashUtils" %>
<%@ page import="com.connexience.server.model.ServerObject" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");
%>
<%
    ScriptMerger _merger = new ScriptMerger("", "../../scripts");
    String jQueryVersion = "1.8.3";//1.7.2"; //"1.4.2"
    String jQueryUIVersion = "1.9.2"; //"1.8.24";// "1.8.5";

    Study menu_study = null; //needed as the page doesn't have a header but the footer references this.
%>
<!DOCTYPE HTML>
<html>
<head>
    <%@include file="../../WEB-INF/jspf/page/ticket.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/theme.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/organisationredirect.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery/<%=jQueryVersion%>/jquery.js"></script>
    <%@include file="../../WEB-INF/jspf/includes/jquery-ui.jspf" %>
    <%@include file="../../WEB-INF/jspf/includes/scriptloader.jspf" %>
    <%@include file="../../WEB-INF/jspf/includes/analytics.jspf" %>
    <%
        _merger.sendCss(request.getSession().getServletContext(), out);
        _merger.sendScripts(request.getSession().getServletContext(), out);
    %>
    <link rel="icon" href="../../styles/common/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="../../styles/common/images/favicon.ico" type="image/x-icon"/>
    <meta http-equiv="content-type" content="text/html; UTF-8"/>
    <meta name="author" content="<%=siteName%>"/>
    <meta name="keywords" content="collaboration, e-Science, bioinformatics, data analysis"/>
    <meta name="description" content="<%=siteName%> Social Networking site"/>
    <meta name="robots" content="all"/>
    <title><%=siteName%>
    </title>
    <script type="text/javascript" src="../../scripts/jquery-validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery-validate/additional-methods.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#projectDropDownSelect').selectpicker({
                showSubtext: true,
                size: false,
                width: '100%'
            });
        });

    </script>
</head>
<body>
<div id="wrapper" class="container clearfix">
    <div class="row-fluid">
        <div class="span6" style="margin:25px 0;">
            <a href="../../pages/front/index.jsp">
                <h1 id="logo" class="grid_6">
                    <img src="../../styles/<%=themeFolder%>/images/logo_head.png"/>
                </h1>
                <h4 id="tagline"><%=tagLine%>
                </h4>
            </a>
        </div>
        <hr style="clear:both;">
        <h2>Create Organisation</h2>

        <p>Welcome to eScience Central! To begin, please enter your organisation and installation details below.</p>
        <div class="row-fluid">
            <div class="span12">
                <form action="create.jsp" method="POST" class="form-horizontal" id="create-organisation">
                    <fieldset>
                        <legend>Organisation</legend>
                        <div class="control-group">
                            <label class="control-label">Organisation Name</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Organisation Name" id="orgname" name="orgname" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">First Name</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="First Name" id="firstName" name="firstName" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Last Name</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Last Name" id="lastName" name="lastName" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Username</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Username" id="username" name="username" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <input type="password" size="75" placeholder="Password" id="password" name="password" class="input-xlarge"/>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Gmail</legend>
                        <div class="control-group">
                            <label class="control-label">GMail Username</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="GMail Username" id="gmailUsername" name="gmailUsername" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">GMail Password</label>
                            <div class="controls">
                                <input type="password" size="75" placeholder="GMail Password" id="gmailPassword" name="gmailPassword" class="input-xlarge"/>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Groups</legend>
                        <div class="control-group">
                            <label class="control-label">Admin Group</label>
                            <div class="controls">
                                <input type="text" size="75" value="Admins" id="adminGroup" name="adminGroup" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Users Group</label>
                            <div class="controls">
                                <input type="text" size="75" value="Users" id="usersGroup" name="usersGroup" class="input-xlarge"/>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>AWS Storage</legend>
                        <div class="alert">
                            <strong>Warning!</strong> Unarchiving of files will not occur for this organisation until after the server has been restarted.
                        </div>
                        <div class="control-group">
                            <label class="control-label">Storage Folder</label>
                            <div class="controls">
                                <input type="text" size="75" value="/orgdata" id="storageFolder" name="storageFolder" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">AWS Access Key</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Access Key" id="awsAccessKey" name="awsAccessKey" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">AWS Secret Key</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Secret key" id="awsSecretKey" name="awsSecretKey" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">REST Archive URL</label>
                            <div class="controls">
                                <input type="text" size="75" value="http://localhost:8080/beta/servlets/archive/rest" id="awsDomainName" name="awsDomainName" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Glacier Vault Name</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Glacier Vault Name" id="awsGlacierVaultName" name="awsGlacierVaultName" class="input-xlarge"/>
                            </div>
                        </div>
                    </fieldset>
                    <hr>
                    <div class="control-group">
                        <input type="submit" value="Create" class="btn btn-large"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="push"></div>
</div>
<!-- Footer -->
<div id="footer">
    <div class="container">
        <p class="copyright">&copy; Copyright <%=siteName%> </p>

        <div id="partners" class="clearfix">
            <div class="partner-logo">
                <img src="../../styles/esc/images/science-city.png" alt="Newcastle Science City"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/jisc.png" alt="JISC"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/epsrc.png" alt="EPSRC"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/newcastle-university.png" alt="Newcastle University"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/side.png" alt="SiDE"/>
            </div>
        </div>
    </div>
</div>
<!-- Load Javascript -->
<%--<%@include file="../includes/jquery-ui.jspf" %>
<%@include file="../includes/scriptloader.jspf" %>
<%@include file="../includes/analytics.jspf" %>
<%
    _merger.sendCss(request.getServletContext(), out);
    _merger.sendScripts(request.getServletContext(), out);
%>--%>
<script type="text/javascript">
    var all = $.event.props,
            len = all.length,
            res = [];
    while (len--) {
        var el = all[len];
        if (el != 'layerX' && el != 'layerY') res.push(el);
    }
    $.event.props = res;
</script>
<script type="text/javascript">

    $(document).ready(function () {

        jQuery.validator.setDefaults({
            debug: true,
            errorPlacement: function(error, element) {
                // if the input has a prepend or append element, put the validation msg after the parent div
                if(element.parent().hasClass('input-prepend') || element.parent().hasClass('input-append')) {
                    error.insertAfter(element.parent());
                    // else just place the validation message immediatly after the input
                } else {
                    error.insertAfter(element);
                }
            },
            errorClass: "error help-inline",
            errorElement: "small", // contain the error msg in a small tag
            highlight: function(element) {
                $(element).closest('.control-group').addClass('error'); // add the Bootstrap error class to the control group
            },
            success: function(element) {
                $(element).closest('.control-group').removeClass('error'); // remove the Boostrap error class from the control group
            }
        });

        var form = $( "#create-organisation" );
        form.validate({
            rules: {
                orgname: {required: true, maxlength: 255},
                firstName: {required: true, maxlength: 255},
                lastName: {required: true, maxlength: 255},
                username: {required: true, maxlength: 255},
                password: {required: true, maxlength: 255},
                gmailUsername: {required: false, maxlength: 255},
                gmailPassword: {required: false, maxlength: 255},
                adminGroup: {required: false, maxlength: 255},
                usersGroup: {required: false, maxlength: 255},
                storageFolder: {required: true, maxlength: 255},
                awsAccessKey: {required: false, maxlength: 255},
                awsSecretKey: {required: false, maxlength: 255},
                awsDomainName: {required: false, maxlength: 255},
                awsGlacierVaultName: {required: false, maxlength: 255}
            },
            submitHandler: function(form) {
                if ($(form).valid())
                    form.submit();
                return false; // prevent normal form posting
            }
        });

    });

</script>
</body>
</html>

<%@page import="com.connexience.server.ejb.util.EJBLocator"%>
<%@ page import="java.util.*"%>
<%@ page import="com.connexience.server.model.project.study.LoggerConfiguration" %>
<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf"%>
  <%
    try{

        Collection<LoggerConfiguration> configurations = EJBLocator.lookupLoggersBean().getLoggerConfigurations(ticket, 0, Integer.MAX_VALUE);

        Collection<LoggerType> loggerTypes = EJBLocator.lookupLoggersBean().getLoggerTypes(ticket, 0, Integer.MAX_VALUE);
  %>

  <!-- Caption Line -->
  <h2>Logger Configurations</h2>

  <%-- An example List of items--%>
<div class="row-fluid">
  <div class="span9">
      <select id="type-selection" class="show-tick">
          <% for(LoggerType type : loggerTypes){ %>

          <option data-subtext="<%=type.getManufacturer()%>" value="<%=type.getId()%>"><%=type.getName()%></option>

          <% } %>
      </select>

      <%
          System.out.println(configurations.size());

          if(configurations.size()>0){%>

    <ul id="configurations" class="unstyled">

      <%

        for(LoggerConfiguration configuration : configurations)
        {
            System.out.println(configuration.getLoggerType().getId() + " - " + loggerTypes.iterator().next().getId());
      %>

        <li data-type="<%=configuration.getLoggerType().getId()%>" <% if(configuration.getLoggerType().getId() != loggerTypes.iterator().next().getId()) { %> style="display: none" <% } %>>
            <a href="configuration.jsp?id=<%=configuration.getId()%>">
                <h4><%=configuration.getName()%></h4>
                <p><%=configuration.getDescription()%></p>
            </a>
        </li>

      <%
        }
      %>
      <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>

    </ul>
      <p id="no-configurations" style="display:none">This logger type has no configurations yet.</p>
      <%} else {%>
      <p>You have no configurations yet.</p>
      <%}%>
  </div>


  <!-- Right Hand Side -->
        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>

</div>

  <%
    }
    catch(ConnexienceException e)
    {
      e.printStackTrace();
      //response.sendRedirect("../../pages/error.jsp");
      return;
    }
  %>
<script type="text/javascript">

    $('#type-selection').selectpicker({
        showSubtext: true
    });

    $(document).ready(function(){
        var $createConfigLink = $('#create-config-link');
            initialConfigHref = $createConfigLink.attr('href');

        $createConfigLink.attr('href', initialConfigHref + "&loggerConfigType=" + $('#type-selection').val());


        $(document).on('change','#type-selection',function(){

            $('#no-configurations').css('display', 'none');

            $createConfigLink.attr('href', initialConfigHref + "&loggerConfigType=" + $('#type-selection').val());

            $("#configurations li:not([data-type='" + $(this).val() + "'])").css('display','none');
            $("#configurations li[data-type='" + $(this).val() + "']").css('display','block');

            if($("#configurations li[data-type='" + $(this).val() + "']").length == 0){
                $('#no-configurations').css('display', 'block');
            }
        });

        // Trigger change to populate configs list
        $('#type-selection').trigger('change');

    });

</script>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

<%@ page import="com.connexience.server.model.project.study.Logger" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.sun.tools.javac.util.*" %>
<%@ page import="java.util.List" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<%


    try {

        List<LoggerType> loggerTypes = (List<LoggerType>)EJBLocator.lookupLoggersBean().getLoggerTypes(ticket, START, PAGE_SIZE);
        Long loggerTypeCount = EJBLocator.lookupLoggersBean().getLoggerTypeCount(ticket);
        Long loggerConfigurations = EJBLocator.lookupLoggersBean().getLoggerConfigurationCount(ticket);
        Long loggers = EJBLocator.lookupLoggersBean().getLoggerCount(ticket);
        Long fileTypes = EJBLocator.lookupLoggersBean().getFileTypeCount(ticket);
        Long deployments = EJBLocator.lookupLoggersBean().getLoggerDeploymentCount(ticket);
        List<Integer> loggerTypeIdList = new ArrayList<Integer>();

%>


<h2>Loggers</h2>

<div id="avail-summary" class="row-fluid fleet-availability-control-wrapper">
    <div class="span6"><div><span class="badge badge-success" style="border-radius: 3px;">&nbsp;</span>  Availability shown from <strong>today</strong> for <strong>1 month</strong> <a id="editAvailability" class="btn btn-mini" href="#"><i class="icomoon-pencil"></i> Edit</a></div></div>
</div>

<div style="display:none;" class="row-fluid fleet-availability-control-wrapper">
    <div class="span12">
        <form class="form-inline">
            <div class="form-group">
                <label for="startDateAvailability">Availability from:</label>
                <input type="text" class="form-control span2" id="startDateAvailability" placeholder="">

                <label for="dateUnitCount">for</label>
                <select class="span1" name="" id="dateUnitCount">
                    <option selected value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                    <option value="41">41</option>
                    <option value="42">42</option>
                    <option value="43">43</option>
                    <option value="44">44</option>
                    <option value="45">45</option>
                    <option value="46">46</option>
                    <option value="47">47</option>
                    <option value="48">48</option>
                </select>

                <label for="dateUnit"></label>
                <select name="" class="span2" id="dateUnit">
                    <option value="days">Day</option>
                    <option value="weeks">Week</option>
                    <option selected value="months">Month</option>
                </select>
                <label for="cancelAvailabilityEdit"></label>
                <button id="cancelAvailabilityEdit" class="btn btn-mini"><i class="icomoon-close"></i>&nbsp;Close Edit</button>
            </div>
        </form>
    </div>
</div>

<div class="row-fluid">
    <div class="span9">
    <%
        LoggerType loggerType;
        Iterator i = loggerTypes.iterator();
        float iteration = 1;

        while(i.hasNext()) {
            loggerType = (LoggerType) i.next();

            loggerTypeIdList.add(loggerType.getId());

    %>

        <div class="row-fluid logger-type-list-item" data-logger-type-id="<%=loggerType.getId()%>">
            <div class="span12">
                <div class="row-fluid">
                    <div class="span5">
                        <div class="row-fluid">
                            <div class="span12">
                                <h2 class="make-model"><%=loggerType.getName()%> <span class="model"><%=loggerType.getManufacturer()%></span></h2>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <p class="fleet-availability-text">Loading...</p>
                            </div>
                        </div>
                    </div>
                    <div class="span7">
                        <ul class="unstyled inline logger-type-actions pull-right">
                            <li class="text-center">
                                <a href="loggertype.jsp?id=<%=loggerType.getId()%>">
                                    <i class="icomoon-eye"></i>
                                    <p>View <br/>Loggers</p>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#" class="load-edit-loggertype-modal" data-logger-type-id="<%=loggerType.getId()%>" data-logger-type-model="<%=loggerType.getName()%>" data-logger-type-make="<%=loggerType.getManufacturer()%>" data-logger-type-physical="<%=loggerType.isPhysicalDevice()%>">
                                    <i class="icomoon-pencil"></i>
                                    <p>Edit <br/> Type</p>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#" class="load-delete-loggertype-modal" data-logger-type-id="<%=loggerType.getId()%>">
                                    <i class="icomoon-cancel-circle"></i>
                                    <p>Delete <br/> Type</p>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="upload-loggers.jsp?loggerTypeId=<%=loggerType.getId()%>">
                                    <i class="icomoon-plus"></i>
                                    <p>Upload <br/>Logger List</p>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#" class="load-list-sensors-modal" data-logger-type-id="<%=loggerType.getId()%>"">
                                    <i class="icomoon-podcast"></i>
                                    <p>Manage <br/>Sensors</p>
                                </a>
                            </li>
                            <li class="text-center">
                                <a href="#" class="load-list-configurations-modal" data-logger-type-id="<%=loggerType.getId()%>">
                                    <i class="icomoon-settings"></i>
                                    <p>Manage <br/>Configurations</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="fleet-availability span12">
                        <div class="loggers-available" style="width: 0%">
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <%  %>

    <%
            iteration = iteration + 1;
    %>

    <% } %>
        <input type="hidden" id="loggerTypeIdList" value="<%=loggerTypeIdList.toString()%>"/>
        <%
            NUM_OBJECTS = EJBLocator.lookupLoggersBean().getLoggerTypeCount(ticket);
        %>
        <%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>
    </div>
    <div class="span3">
        <%@include file="menu.jspf" %>
    </div>
    </div>




<%--<div class="row-fluid">

        <hr/>

        <div class="row-fluid">
            &lt;%&ndash;<div class="span6">
                <h3>
                    <i class="icomoon-meter"></i>&nbsp;<%=loggerTypeCount%>&nbsp;Logger Types
                </h3>
            </div>&ndash;%&gt;
            <div class="span3">
                <h4>
                    <i class="icomoon-settings"></i>&nbsp;<%=loggerConfigurations%>&nbsp;Configurations
                </h4>
            </div>
            <div class="span3">
                <h4>
                    <i class="icomoon-feed"></i>&nbsp;<%=loggers%>&nbsp;Loggers
                </h4>
            </div>
            <div class="span3">
                <h4>
                    <i class="icomoon-file3"></i>&nbsp;<%=fileTypes%>&nbsp;File Types
                </h4>
            </div>
            <div class="span3">
                <h4>
                    <i class="icomoon-users2"></i>&nbsp;<%=deployments%>&nbsp;Deployments
                </h4>
            </div>
        </div>


    </div>--%>
</div>

<script src="../../scripts/deployment-date-filter/deployment-date-filter.js"></script>

<script>

    var loggerTypeIdsString = $('#loggerTypeIdList').val();

    if (loggerTypeIdsString && loggerTypeIdsString !== '') {

        function updateStatusBars() {
            deploymentDateFilter.loggerTypeIds = JSON.parse(loggerTypeIdsString);
            $('.fleet-availability-text').html('loading...');
        }

        updateStatusBars();

    } else {
        $('.fleet-availability .status-text').html('');
    }

    deploymentDateFilter.eventCallbacks.changeDate = updateStatusBars;

    deploymentDateFilter.eventCallbacks.changeDateUnitOrCount = updateStatusBars;

    deploymentDateFilter.eventCallbacks.ajaxSuccess = function(data) {
        if (data && data.length) {
            $('.fleet-availability .status-text').html('');

            $.each(data, function(i, item){
                // item[0] = loggerId, item[1] = available loggers, item[2] = total loggers
                var $statusBar = $('[data-logger-type-id="'+item[0]+'"]');
                $statusBar.find('.loggers-available').css('width',(item[1]/item[2]*100) + '%');
                $statusBar.find('.fleet-availability-text').html(item[1] + ' of ' + item[2] + ' loggers available');
            });
        }
    };



    deploymentDateFilter.init();

</script>

<%@include file="modals/logger-type.jspf"%>
<%@include file="modals/logger-configurations.jspf"%>
<%@include file="modals/logger-sensors.jspf"%>
<%@include file="createType.jspf" %>
<script>
    $(document).ready(function(){

        $('#create-type').on('hidden', function () {

            document.location = "../../pages/loggers/overview.jsp";

        })

    });
</script>
<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@ page import="com.connexience.server.model.project.FileType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<h2>File Types</h2>
<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">
        <form action="filetypes.jsp">
            <div class="input-append">
                <input id="searchtext" name="searchtext" type="text" placeholder="Search">
                <div name="searchbutton" id="searchbutton" class="btn-group" type="submit">
                    <button class="btn">
                        <i class="icon-search"></i>Search
                    </button>
                </div>
            </div>
        </form>

        <%

            try {
                List fileTypes;
                if (request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()) {
                    fileTypes = (List<FileType>)EJBLocator.lookupLoggersBean().getFileTypes(ticket, 0, 100);
                }
                else
                {
                    fileTypes = (List<FileType>)EJBLocator.lookupLoggersBean().getFileTypes(ticket, 0, 100);
                }
        %>
        <ul id="device-list" class="unstyled clearfix">

            <%
                FileType fileType;
                Iterator i = fileTypes.iterator();
                while (i.hasNext()) {
                    fileType = (FileType) i.next();

            %>

            <li>
                <a href="filetype.jsp?id=<%=fileType.getId()%>">
                    <h4><%=fileType.getName()%></h4>
                    <p><%=fileType.getDescription()%></p>
                </a>
            </li>

            <%

                }

            %>

        </ul>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>

<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
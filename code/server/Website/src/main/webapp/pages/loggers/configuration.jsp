<%@page import="com.connexience.server.ejb.util.EJBLocator"%>
<%@ page import="java.util.*"%>
<%@ page import="com.connexience.server.model.project.study.LoggerConfiguration" %>
<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf"%>
<%
        editing = false;
        int typeId = 0;
        LoggerConfiguration configuration = null;
        Integer additionalPropertyCount = 1;

        if(request.getParameter("id") != null)
        {
            configuration = EJBLocator.lookupLoggersBean().getLoggerConfiguration(ticket, Integer.parseInt(request.getParameter("id")));
        }

        if(request.getParameter("action") !=null && request.getParameter("action").equals("edit"))
        {
            editing = true;
        }

        if(request.getParameter("loggerConfigType") !=null)
        {
            typeId = Integer.parseInt(request.getParameter("loggerConfigType"));
        }

        menu_loggerConfiguration = configuration;

        List<LoggerType> loggerTypes = (List<LoggerType>)EJBLocator.lookupLoggersBean().getLoggerTypes(ticket, 0, Integer.MAX_VALUE);

if(editing && configuration != null)
{ %>
<h2>Edit Configuration</h2>
<% }else if(!editing && configuration != null){ %>
<h2>Logger Configuration</h2>
<% }else{ %>
<h2>Create Configuration</h2>
<%}%>
<div class="btn-group action-menu">
    <% if(!editing && configuration != null) { %>
    <a href="../../pages/loggers/configuration.jsp?id=<%=configuration.getId()%>&action=edit" class="btn">Edit</a>
    <a href="#delete-configuration-dialog" data-toggle="modal" class="btn">Delete</a>
    <% } else if(configuration != null) { %>
    <button id="submit-configuration" class="btn">Save</button>
    <a href="../../pages/loggers/configuration.jsp?id=<%=configuration.getId()%>" class="btn">Cancel</a>
    <a href="#delete-configuration-dialog" data-toggle="modal" class="btn">Delete</a>
    <% } else {
    if(loggerTypes.size() > 0){%>
    <button id="submit-configuration" class="btn">Save</button>
    <%}else{%>
    <button id="submit-configuration" class="btn" disabled="disabled">Save</button>
    <% } %>
    <a href="../../pages/loggers/configurations.jsp" class="btn">Cancel</a>
    <a href="#delete-configuration-dialog" data-toggle="modal" class="btn disabled">Delete</a>
    <% } %>
</div>
<div class="row-fluid">
    <div class="span9">
    <% if(loggerTypes.size() == 0){%>
        <div id="noLoggerTypes" class="alert">
            <h4>Warning!</h4>
            <span>You must create at least one logger type to be able to create a configuration. Click <a href="loggertypes.jsp">here</a> to create one.</span>
        </div>
    <%}%>
    <form id="configuration-form">
    <% if(editing && configuration != null){%>
        <div id="submit-type-message"></div>
        <div class="control-group">
            <label class="control-label">Logger Type</label>
            <div class="controls">
                <select id="logger-type" name="loggerType" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
                    <% for (LoggerType type : loggerTypes )
                    {
                        if(type == configuration.getLoggerType()) { %>

                        <option selected value="<%=type.getId()%>"><%=type.getManufacturer()%> <%=type.getName()%></option>

                    <% } else { %>

                        <option value="<%=type.getId()%>"><%=type.getManufacturer()%> <%=type.getName()%></option>

                   <% } } %>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Name</label>
            <div class="controls">
                <input type="text" id="name" name="name" placeholder="Name" value="<%=configuration.getName()%>" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Description</label>
            <div class="controls">
                <textarea id="description" name="description" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>><%=configuration.getDescription()%></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Sample Frequency</label>
            <div class="controls">
                <input type="text" id="sampleFrequency" name="sampleFrequency" placeholder="Sample Frequency" value="<%=configuration.getSampleFrequency()%>" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Application</label>
            <div class="controls">
                <input type="text" id="application" name="application" placeholder="Application" value="<%=configuration.getApplication()%>" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Minimum Firmware Version</label>
            <div class="controls">
                <input type="text" id="minimumFirmwareVersion" name="minimumFirmwareVersion" placeholder="Minimum Firmware Version" value="<%=configuration.getMinimumFirmwareVersion()%>" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Firmware Location</label>
            <div class="controls">
                <input type="text" id="firmwareLocation" name="firmwareLocation" placeholder="Firmware Location" value="<%=configuration.getFirmwareLocation()%>" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="additional-properties">
            <% for(String key : configuration.getAdditionalProperties().keySet())
            { %>

            <div class="control-group">
                <label class="control-label">Additional Property <%=additionalPropertyCount%></label>
                <div class="controls controls-row">
                    <input type="text" name="key<%=additionalPropertyCount%>" id="key<%=additionalPropertyCount%>" placeholder="Property Name" value="<%=key%>">
                    <input type="text" name="value<%=additionalPropertyCount%>" id="value<%=additionalPropertyCount%>" placeholder="Property Value" value="<%=configuration.getAdditionalProperty(key)%>">
                </div>
            </div>

            <%}%>
        </div>
        <button id="add-property" class="btn" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>Add Property</button>
    <%}else if(!editing && configuration != null){%>
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd><%=configuration.getName()%></dd>
            <dt>Description</dt>
            <dd><%=configuration.getDescription()%></dd>
            <dt>Logger Type</dt>
            <dd><%=configuration.getLoggerType().getManufacturer()%> <%=configuration.getLoggerType().getName()%></dd>
            <dt>Sample Frequency</dt>
            <dd><%=configuration.getSampleFrequency()%></dd>
            <dt>Application</dt>
            <dd><%=configuration.getApplication()%></dd>
            <dt>Min Firmware Version</dt>
            <dd><%=configuration.getMinimumFirmwareVersion()%></dd>
            <dt>Firmware Location</dt>
            <dd><%=configuration.getFirmwareLocation()%></dd>
            <% for(String key : configuration.getAdditionalProperties().keySet())
            { %>
            <dt><%=key%></dt>
            <dd><%=configuration.getAdditionalProperty(key)%></dd>
            <%}%>
        </dl>
    <%}else{%>
        <div class="control-group">
            <label class="control-label">Logger Type</label>
            <div class="controls">
                <select id="logger-type" name="loggerType"<% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>

                    <% for (LoggerType type : loggerTypes )
                    {
                        String displayName = type.getName() + " (" + type.getManufacturer() + ")";

                        System.out.println(displayName);
                    %>

                    <option value="<%=type.getId()%>"><%=displayName%></option>

                    <% } %>

                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Name</label>
            <div class="controls">
                <input type="text" id="name" name="name" placeholder="Name" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Description</label>
            <div class="controls">
                <textarea id="description" name="description" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Sample Frequency</label>
            <div class="controls">
                <input type="text" id="sampleFrequency" name="sampleFrequency" placeholder="Sample Frequency" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Application</label>
            <div class="controls">
                <input type="text" id="application" name="application" placeholder="Application" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Minimum Firmware Version</label>
            <div class="controls">
                <input type="text" id="minimumFirmwareVersion" name="minimumFirmwareVersion" placeholder="Minimum Firmware Version" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Firmware Location</label>
            <div class="controls">
                <input type="text" id="firmwareLocation" name="firmwareLocation" placeholder="Firmware Location" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>
            </div>
        </div>
        <div class="additional-properties">

        </div>
        <button id="add-property" class="btn" <% if(loggerTypes.size() == 0){%>disabled="disabled"<%}%>>Add Property</button>
    </form>
    <%}%>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<div id="delete-configuration-dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="delete-configuration-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="delete-configuration-label">Delete Configuration</h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you wish to delete this configuration?</p>
    </div>
    <div class="modal-footer">
        <button type="submit" id="delete-configuration" class="btn">Confirm</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        // Set type dropdown based on previous selection on configurations.jsp or other source
        var typeID = <%=typeId%>;
        if (typeID) {
            $('#logger-type').val(typeID);
        }


        $('#configuration-form').on('click', '#add-property', function(){

            //Add 1 to avoid 0 start value
            var propertyCount = $('.additional-properties .control-group').size() + 1;

            var html =  '<div class="control-group">' +
                    '<label class="control-label">Additional Property ' + propertyCount + '</label>' +
                    '<div class="controls controls-row">' +
                    '<input type="text" name="key' + propertyCount + '" id="key' + propertyCount + '" placeholder="Property Name">' +
                    '<input type="text" name="value' + propertyCount + '" id="value' + propertyCount + '" placeholder="Property Value">' +
                    '</div>' +
                    '</div>';

            $('.additional-properties').append(html);

        });

        var form = $( "#configuration-form" );
        form.validate({
            rules: {
                name: {required: true, maxlength: 255},
                description: {required: true, maxlength: 255},
                sampleFrequency: {required: false, digits: true, maxlength: 255},
                application: {required: false, maxlength: 255},
                minimumFirmwareVersion: {required: false, maxlength: 255},
                firmwareLocation: {required: false, maxlength: 255}
            }
        });

        $('.btn-group').on("click", "#submit-configuration", function (event) {

            event.preventDefault();

            if(form.valid())
            {
                var configuration = new Object();

                <% if(configuration != null) { %>
                configuration.id = <%=configuration.getId()%>;
                <% } %>
                configuration.name = $("#name").val();
                configuration.description = $("#description").val();
                configuration.sampleFrequency = $('#sampleFrequency').val();
                configuration.application = $("#application").val();
                configuration.minimumFirmwareVersion = $("#minimumFirmwareVersion").val();
                configuration.firmwareLocation = $('#firmwareLocation').val();
                configuration.loggerType = null;

                var additionalProperties = {};

                $.each($('.additional-properties .controls'), function(key, input){

                    var propKey = $(this).children('input').eq(0).val();
                    var propVal = $(this).children('input').eq(1).val();

                    additionalProperties[propKey] = propVal;

                });

                configuration.additionalProperties = additionalProperties;

                var encodedConfiguration = JSON.stringify(configuration);

                $.ajax({
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: '/website-api/rest/logger/' + $('#logger-type').val() + '/configuration/save/',
                    data: encodedConfiguration,
                    success: function (configuration) {

                        <% if(editing && configuration !=null){ %>

                        document.location = "../../pages/loggers/configuration.jsp?id=<%=configuration.getId()%>";

                        <% } else { %>

                        document.location = "../../pages/loggers/configurations.jsp";

                        <% } %>

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    }
                });
            }
            else
            {
                if($('#validation-message').length == 0)
                {
                    if($('.validation-message').length == 0)
                    {
                        var message = "Correct these errors to create this logger type.";
                        var type = "error";
                        var target = $("#submit-type-message");
                        var cssClass = "validation-message";

                        flashMessage(message, type, target, cssClass)
                    }
                }
            }
        });

        <% if(configuration != null) { %>

        $(document).on('click', '#delete-configuration', function(){

            $.ajax({
                type: 'POST',
                url: '/website-api/rest/logger/configuration/<%=configuration.getId()%>/remove',
                success: function (configuration) {
                    console.log(configuration);

                    document.location = "../../pages/loggers/configurations.jsp";

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });

        });

        <% } %>
    });

</script>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%@page import="com.connexience.server.model.security.Permission"%>
<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%
    try
    {
      GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

      String groupId = request.getParameter("id");
      if (groupId == null || groupId.equals(""))
      {
        response.sendRedirect("../../pages/groups/grouplist.jsp?error=noGroupId");
        return;
      }
      Group group = groupEJB.getGroup(ticket, groupId);
      if (group == null)
      {
        response.sendRedirect("../../pages/groups/grouplist.jsp?error=groupNotFound");
        return;
      } else {
          if(EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, group, Permission.WRITE_PERMISSION)){
              EJBLocator.lookupGroupDirectoryBean().removeGroup(ticket, group.getId());
              //todo: project set default ticket items here.  Also check for other users who may have this as a default project
              ticket.setDefaultProjectId(null);
              ticket.setDefaultStorageFolderId(null);
          }
          response.sendRedirect("../../pages/groups/grouplist.jsp");
      }

  %>


<%
  }
  catch (Exception e)
  {
    e.printStackTrace();
    if (!response.isCommitted())
    {
      response.sendRedirect("../../pages/error.jsp");
    }
  }

%>
<%-- 
    Document   : data browser
    Created on : Sep 24, 2010, 11:15:26 AM
    Author     : hugo
--%>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
                              
  <link rel="stylesheet" href="../../scripts/elfinder/css/elfinder.css" type="text/css" media="screen" charset="utf-8">
  <link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen"
        charset="utf-8">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">

  <script type="text/javascript" src="../../scripts/util/list.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>

  <script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
  <script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>

  <script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>

  <script src="../../scripts/elfinder/js/elfinder.full.js" type="text/javascript" charset="utf-8"></script>
  <script src="../../scripts/jquery-treeview/jquery.treeview.js" type="text/javascript"></script>
  <script src="../../scripts/jquery-treeview/jquery.treeview.async.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>

  <script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
  <script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.js"></script>


  <%
    GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

    String groupId = request.getParameter("id");
    if (groupId == null || groupId.equals(""))
    {
      response.sendRedirect("../../pages/groups/grouplist.jsp?error=noGroupId");
      return;
    }
    Group group = groupEJB.getGroup(ticket, groupId);
    if (group == null)
    {
      response.sendRedirect("../../pages/groups/grouplist.jsp?error=groupNotFound");
      return;
    }

    String urlToFinderServlet = "../../servlets/finder?rootid=" + group.getId();

    String folderIdToOpen = request.getParameter("folderId");
    if(folderIdToOpen != null && !folderIdToOpen.equals(""))
    {
      urlToFinderServlet += "&target=" + folderIdToOpen;
    }


  %>

  <script type="text/javascript" charset="utf-8">
    $(document).ready(function()
    {
      clearTabs();

      //create the file browser and plugin components
      $('#finder').elfinder({
        url : '<%=urlToFinderServlet%>',
        places : ""
      });

      var editorChooser = new ViewerChooser();
      editorChooser.init("editorchooser");
      var quickView = new QuickView();
      quickView.init("quickviewdiv");

      jQuery.event.add(window, "load", resizeFinder);
      jQuery.event.add(window, "resize", resizeFinder);

      //Fix elFinder to allow space and other chars to be entered in a dialog box on the page
      //Bug described here: http://elrte.org/redmine/boards/2/topics/950
      $('*').keydown(function(e)
      {
        if ((e.keyCode === 13) || // enter
            (e.keyCode === 32) || // space
            (e.keyCode === 37) || // left
            (e.keyCode === 38) || // up
            (e.keyCode === 39) || // right
            (e.keyCode === 40)){ // down
          //e.preventDefault();
          e.stopPropagation();
        }
        return true;
      });
    });

    //function to resize the file browser when the window size changes
    function resizeFinder()
    {
      var h = $(window).height();
      if (h < 600)
      {
        h = 600;
      }
      $(".el-finder-nav").css('height', h - 320);
      $(".el-finder-cwd").css('height', h - 320);
    }

  </script>

   <!-- Caption Line -->
  <h2 class="grid_12 caption">Documents shared with <span><%=group.getName()%></span>
  </h2>

  <div class="hr grid_12 clearfix">&nbsp;</div>
  
  <div class="grid_9">

    <div id="finder"></div>
    <div id="quickviewdiv"></div>
    <div id="editorchooser"></div>

  </div>
  <%@include file="../../pages/groups/menu.jspf" %>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

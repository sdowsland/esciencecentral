<%@ page import="com.connexience.server.model.security.*" %>
<%--
  Demo Page
  Author: Simon
--%>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<script type="text/javascript" src="../../scripts/util/dialog.js"></script>
<script type="text/javascript">
    var dialog = new ConfirmDialog();
    $(document).ready(function()
    {
        clearTabs();
        dialog.init("confirmdialog");
        $('.listElement img').each(function()
        {
            var boxHeight = $(this).parent().parent().height();
            var pt = (boxHeight - 75) / 2;
            if (pt < 0) pt = 0;
            $(this).css("padding-top", pt);
        });

        $('.listElement').click(function()
        {
            if(!$(this).hasClass("pagination"))
            {
                //window.location = $(this).children('a').first().attr('href');
            }
        });
    });

    function removeGroup(id, name){
        dialog.yesCallback = function(){
            window.location="pages/groups/delete.jsp?id=" + id;
        }
        dialog.noCallback = function(){
            dialog.yesCallback = null;
        }
        dialog.show("Are you sure you want to delete the group: " + name);
    }

</script>

<%
    String filter = request.getParameter("filter");

%>
<!-- Caption Line -->
<%if(filter!=null && filter.equals("all")){%>
<h2>All Groups</h2>
<%} else {%>
<h2>My Groups</h2>
<%}%>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
        <div id="confirmdialog"></div>

        <ul id="group-list" class="unstyled">

            <%

                //        NUM_OBJECTS = EJBLocator.lookupOrganisationDirectoryBean().numberOfOrganisationNonProtectedGroups(ticket);

                List<Group> groups;

                if(filter!=null && filter.equals("all")) {
                    groups = EJBLocator.lookupOrganisationDirectoryBean().listOrganisationNonProtectedGroups(ticket, START, PAGE_SIZE);
                    NUM_OBJECTS = EJBLocator.lookupOrganisationDirectoryBean().numberOfOrganisationNonProtectedGroups(ticket);
                } else {
                    List<Group>allGroups = EJBLocator.lookupUserDirectoryBean().listUserGroups(ticket, ticket.getUserId());
                    ArrayList<Group> filteredGroups = new ArrayList<Group>();
                    for(Group g : allGroups){
                        if(!g.isProtectedGroup()){
                            filteredGroups.add(g);
                        }
                    }
                    NUM_OBJECTS = filteredGroups.size();
                    groups = new ArrayList<Group>();
                    if(START>=0 && START<filteredGroups.size()){
                        for(int i=START;i<START + PAGE_SIZE;i++){
                            if(i<filteredGroups.size()){
                                groups.add(filteredGroups.get(i));
                            }
                        }
                    }
                }

                for (Group g : groups)
                {
                    String groupId = g.getId();
            %>

            <li class="clearfix">
                <a class="group-picture" href="group.jsp?id=<%=g.getId()%>">
                    <img src="../../styles/common/images/profile.100x100.png" alt="group picture" class="img-polaroid img-circle"/>
                </a>
                <div class="group-bio">
                    <h4><%=g.getName()%></h4>
                    <p>
                        <%=g.getDescription()%>
                    </p>
                </div>
                <ul class="inline">

                    <!-- Link to join group-->
                    <%
                        Collection groupMemberships = EJBLocator.lookupUserDirectoryBean().listGroupMembershipForUser(ticket, ticket.getUserId());
                        GroupMembership gm = new GroupMembership();
                        gm.setGroupId(groupId);
                        gm.setUserId(ticket.getUserId());

                        if (EJBLocator.lookupRequestBean().joinGroupRequestExists(ticket, ticket.getUserId(), g.getId()))
                        {
                    %>
                    <li><span>Join Pending</span></li>
                    <%
                    }
                    else if (!groupMemberships.contains(gm))
                    {
                    %>
                    <li><a href="../../pages/groups/group.jsp?action=join&id=<%=groupId%>">Join Group</a></li>
                    <%
                    }
                    else //Link to leave group
                    {
                    %>
                    <li><a href="../../pages/groups/group.jsp?action=leave&id=<%=groupId%>">Leave Group</a></li>
                    <%
                        }

                        if (groupMemberships.contains(gm))
                        {
                    %>
                    <li><a href="../../pages/groups/members.jsp?id=<%=groupId%>">Members</a></li>
                    <li><a href="javascript:return false;">Share a File</a></li>
                    <li><a id="createEvent" href="#">Create Event</a></li>
                    <%
                        }
                    %>

                    <%-- Link to edit group --%>
                    <%
                        boolean admin = EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId());
                        if (ticket.getUserId().equals(g.getCreatorId()) || admin)
                        {
                    %>
                    <li><a href="create.jsp?id=<%=groupId%>">Admin</a></li>
                    <li><a href="#" onclick="removeGroup('<%=groupId%>', '<%=g.getName()%>')">Delete</a></li>
                    <%
                        }
                    %>
                </ul>
            </li>


            <%
                }
            %>

            <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>
        </ul>
    </div>


    <!-- Right Hand Side -->
    <div class="span3">

        <ul class="unstyled sidebar-menu">
            <li>
                <h4><a href="../../pages/groups/grouplist.jsp"><i class="icomoon-users"></i>My Groups</a></h4>
            </li>
            <li>
                <h4><a href="../../pages/groups/grouplist.jsp?filter=all"><i class="icomoon-users2"></i>All Groups</a></h4>
            </li>
            <li>
                <h4><a href="../../pages/groups/create.jsp"><i class="icomoon-plus"></i>Create Group</a></h4>
            </li>
        </ul>
    </div>

</div>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

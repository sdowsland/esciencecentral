<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.model.image.ImageData" %>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>


<%
    try{

        GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

        String groupId = request.getParameter("id");
        if (groupId == null || groupId.equals(""))
        {
            response.sendRedirect("../../pages/groups/grouplist.jsp?error=noGroupId");
            return;
        }
        Group group = groupEJB.getGroup(ticket, groupId);
        if (group == null)
        {
            response.sendRedirect("../../pages/groups/grouplist.jsp?error=groupNotFound");
            return;
        }

        @SuppressWarnings("unchecked")
        List<User> members = groupEJB.listGroupMembers(ticket, group.getId());

%>

<!-- Caption Line -->
<h2><%=group.getName()%></h2>
<h4>Members</h4>

<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">
        <ul id="group-members" class="unstyled">

                <%
        for (User member : members)
        {
          //if there is no photo set, use a default one
          UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, member.getId());
//          String photoURL = "../servlets/image?soid=" + up.getId() + "&type=" + ImageData.SMALL_PROFILE;

          String noHTMLString = up.getText();
          if (noHTMLString == null || noHTMLString.equals(""))
          {
            noHTMLString = member.getDisplayName() + " has no profile information yet.";
          }
          else
          {
            noHTMLString = noHTMLString.replaceAll("<.*?>", "");
            if (noHTMLString.length() > 240)
            {
              noHTMLString = noHTMLString.substring(0, 239) + "... <a href=\"../../pages/profile/profile.jsp?id=" + user.getId() + "\">[Read More]</a>";
            }
          }
      %>

            <li class="clearfix">
                <a class="profile-picture" href="../../pages/profile/profile.jsp?id=<%=member.getId()%>">
                    <img src="../../servlets/image?soid=<%=member.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                    <h6><%=member.getDisplayName()%> </h6>
                </a>
                <div class="user-bio">
                    <p>
                        <%=noHTMLString%>
                    </p>
                </div>
            </li>

                <%
        }
      %>


    </div>
    <div class="span3">

        <%@include file="menu.jspf" %>
    </div>
</div>
</div>

<!-- Right Hand Side -->


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>


<%@ page import="java.util.List" %>
<%@ page import="com.connexience.server.model.dashboard.*"%>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

   <%
    String id = request.getParameter("id");
    Dashboard db = EJBLocator.lookupDashboardBean().getDashboard(ticket, id);
  %>

  <!-- Caption Line -->
  <h2>Edit Dashboard <span><%=db.getName()%></span></h2>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">

      <h1>Dashboard</h1>

    </div>
        <div class="span3">
            <%@include file="menu.jspf" %>
        </div>
  </div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>


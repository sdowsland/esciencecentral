
<%@ page import="java.util.List" %>
<%@ page import="com.connexience.server.model.datasets.*"%>
  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

   <%   
    List datasets = EJBLocator.lookupDatasetsBean().listDatasets(ticket, false);
  %>


    <script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
    <script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
    <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
    <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
    <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>    
    <script type="text/javascript" src="../../scripts/datasets/views/MultiRowTableView.js"></script>
    <script type="text/javascript" src="../../scripts/datasets/views/SingleRowJsonView.js"></script>
    <script type="text/javascript" src="../../scripts/datasets/DatasetManager.js"></script>
    <script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
    
  <script type="text/javascript">
     var datasetManager = new DatasetManager();
     var acl = new AccessControl();
     var deleteConfirm = new ConfirmDialog();
     var resetConfirm = new ConfirmDialog();
     
    $(document).ready(function() {
        datasetManager.init("editdataset");
        deleteConfirm.init("deletediv");
        deleteConfirm.yesCallback = function(){
            datasetManager.deleteDataset(deleteConfirm.datasetId, function(){location.reload();});
            deleteConfirm.datasetId = undefined;
        }
        
        resetConfirm.init("resetdiv");
        resetConfirm.yesCallback = function(){
            datasetManager.resetDataset(resetConfirm.datasetId, function(){$.jGrowl("Dataset has been reset")});
            resetConfirm.datasetId = undefined;
        }        
    });         
    
    function shareDataset(id){
        $('#securitydialog').remove();

        //add a new ACL
        $('body').append('<div id="securitydialog" class="acl"></div>');
        acl.init("securitydialog", _userId, id, _publicUserId, _enablePublic, _usersGroupId);
        acl.open();        
        

    }
    
    function resetDataset(id){
        resetConfirm.datasetId = id;
        resetConfirm.show("Are you sure you want to reset this data set");
    }
    
    function deleteDataset(id){
        deleteConfirm.datasetId = id;
        deleteConfirm.show("Are you sure you want to delete this data set and all of the associated dashboards");
    }    
    
    function editDataset(id){
        datasetManager.editDataset(id);
    }
    
    function createDataset(){
        datasetManager.createDatasetWithDialog();
    }
  </script>
  <!-- Caption Line -->
  <h2>Datasets</h2>
<h4>&nbsp;</h4>
<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
    <div id="editdataset"></div>
    <div id="creatediv" class="dialog"></div>
    <div id="resetdiv" class="dialog"></div>
    <div id="deletediv" class="dialog"></div>
    
    <ul class="unstyled" id="generic-list">
        <%
            Dataset ds;
            for(int i=0;i<datasets.size();i++){
                ds =(Dataset)datasets.get(i);%>
                
                <li class="clearfix">
                    <a class="generic-picture" onclick="editDataset('<%=ds.getId()%>');">
                        <img src="../../scripts/datasets/images/dataset.png" alt="Dataset icon"/>
                        
                    </a>                    
                    <h6><%=ds.getName()%></h6>
                  <div class="generic-description">
                    <p>
                      <%=ds.getDescription()%>
                    </p>
                  </div>
                  <ul class="inline">
                      <li><a style="cursor: pointer;" onclick="editDataset('<%=ds.getId()%>');">Data</a></li>
                      <li><a style="cursor: pointer;" onclick="resetDataset('<%=ds.getId()%>');">Reset</a></li>
                      <li><a style="cursor: pointer;" onclick="deleteDataset('<%=ds.getId()%>');">Delete</a></li>
                      <li><a style="cursor: pointer;" onclick="shareDataset('<%=ds.getId()%>');">Share</a></li>
                  </ul>
                </li>                
            <%}%>
    </ul>

  </div>

  <!-- Right Hand Side -->
<div class="span3">
    <%@include file="menu.jspf" %>
</div>

</div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

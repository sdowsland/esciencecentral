<%@ page import="com.connexience.server.model.folder.TemplateFolder" %>
<%@ page import="java.util.List" %>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

   <%

    String add = request.getParameter("add");
    if (add != null && add.equals("true")) {
      String id = request.getParameter("id");
      if (id != null && !id.equals("")) {
        TemplateFolder templateFolder = EJBLocator.lookupStorageBean().getTemplateFolder(ticket, id);
        EJBLocator.lookupStorageBean().addTemplateFolderToUsersSpace(ticket, ticket.getUserId(), templateFolder);
        response.sendRedirect("../../pages/front/data.jsp");
      }
    }

    List<TemplateFolder> templateFolders = EJBLocator.lookupStorageBean().listTemplateFolders(ticket);

  %>
  <script type="text/javascript">

    function addTemplateFolder(id) {

      if ($("#link" + id).hasClass('disabled')) {
        return false;
      }
      else {
        var link = $("#link" + id);
        link.addClass('disabled');
        link.removeAttr('onclick');
        link.html('Processing');

        window.location.href = '../../pages/settings/templateFolders.jsp?add=true&id=' + id;
        return true;
      }
    }

  </script>

  <!-- Caption Line -->
  <h2>Sample Data</h2>
<h4>&nbsp;</h4>
<div class="row-fluid">
  <%-- Main Content Left Hand Side--%>
  <div class="span9">

    <div class="list">


      <%
        if(templateFolders.size() == 0)
        {
          %>

      <p>Sorry, there is no sample data available for you.</p>
      <%
        }

        for (TemplateFolder t : templateFolders) {
      %>
      <div class="listElement">
        <img src="../../styles/common/images/search/folder.png" alt="profile picture" class="listItemImg"/>

        <div class="listTopText"><%=t.getName()%>
        </div>
        <div class="listMainText">
          <p>
            <%=t.getDescription()%>
          </p>
        </div>
        <div class="listBottomText">
          <a id="link<%=t.getId()%>" style="cursor: pointer" onclick="addTemplateFolder('<%=t.getId()%>')">Add to my
            filespace</a>
        </div>
      </div>

      <%
        }
      %>
    </div>


  </div>

  <!-- Right Hand Side -->
<div class="span3">
    <%@include file="menu.jspf" %>
</div>

</div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

<%@page import="com.connexience.server.model.security.Permission" %>
<%@page import="com.connexience.server.model.workflow.WorkflowDocument" %>
    <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

    <div id="workflowEditor" class="clearfix" style="-webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none;user-select: none;">
        <div id="stats"></div>
        <div id="drawingCanvas" class="clearfix"></div>
        <div id="quickview"></div>
    </div>
    
    <!--div id="positionPush"></div-->

    <%
        String id = request.getParameter("id");
        String versionId = request.getParameter("versionid");
        if(versionId != null && !versionId.equals(""))
        {
          versionId = "'" + versionId + "'";
        }


        boolean editable = false;

        if (id != null && id.equals("new")) {
            editable = true;
        } else {
            WorkflowDocument doc = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, id);
            if (doc != null) {
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, doc, Permission.WRITE_PERMISSION)) {
                    editable = true;
                }
            }
        }

    %>
    <!--[if IE]>
    <script type="text/javascript" src="../../scripts/canvas/excanvas.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>  
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
    <link rel="stylesheet" href="../../scripts/properties/PropertyEditorNoDatatables.css">
    <%
    ScriptMerger merger = new ScriptMerger("", "../../scripts");

    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/canvas/canvas.text.js\"></script>");
    merger.addScriptTag("<script language=\"javascript\" type=\"text/javascript\" src=\"../../scripts/flot/jquery.flot.js\"></script>");
    merger.addScriptTag("<script language=\"javascript\" type=\"text/javascript\" src=\"../../scripts/flot/jquery.flot.pie.js\"></script>");
    merger.addScriptTag("<script language=\"javascript\" type=\"text/javascript\" src=\"../../scripts/flot/jquery.flot.resize.js\"></script>");
    merger.addScriptTag("<script language=\"javascript\" type=\"text/javascript\" src=\"../../scripts/flot/jquery.flot.axislabels.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/jquery-treeview/jquery.treeview.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/filebrowser/filetree.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/filebrowser/filechooser.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/filebrowser/filetree.async.js\"></script>");
    
    merger.addMinifiedScriptTag("<script type=\"text/javascript\" src=\"../../scripts/jquery-csv/jquery.csv-0.71.min.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/util/CSVTable.js\"></script>");
    
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/canvas/canvas.text.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/canvas/optimer-bold-normal.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/canvas/optimer-normal-normal.js\"></script>");
    
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/jscrollpane/jquery.mousewheel.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/jscrollpane/jquery.em.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/jscrollpane/jScrollPane.js\"></script>");
    
    merger.addMinifiedScriptTag("<script type=\"text/javascript\" src=\"../../scripts/jgrowl/jquery.jgrowl_minimized.js\"></script>");
    
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/PerformanceDataFetcher.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/PerformanceDialog.js\"></script>");
    
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/properties/PropertyEditor.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingBlock.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingConnection.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingConstants.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingContextMenu.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingEditor.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingExecutionManager.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingFetcher.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingInvocation.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingObject.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingPoint.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingPort.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingProperty.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingPropertyList.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingSaver.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingUtils.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingLockManager.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/PaletteFetcher.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingInvocationsDialog.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingBlockDebugger.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingBlockOutputViewer.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/DrawingTerminationReportDialog.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/wfeditor/WorkflowEditor.js\"></script>");

    
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/QuickView.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/ViewerPanel.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/MimeType.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/XMLDisplay.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/MimeTypeManager.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/ViewerChooser.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/FolderViewer.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/ViewerPanel.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/MimeType.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/XMLDisplay.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/MimeTypeManager.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/ViewerChooser.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/FolderViewer.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/viewer/QuickView.js\"></script>");
    
    
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/datasets/views/MultiRowTableView.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/datasets/views/SingleRowJsonView.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/datasets/DatasetQueryEditor.js\"></script>");
    merger.addScriptTag("<script type=\"text/javascript\" src=\"../../scripts/datasets/DatasetItemPicker.js\"></script>");



    merger.sendScripts(getServletContext(), out);
    %>

    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/wfeditor/css/WorkflowEditor.css">

    <script type="text/javascript">
        var performanceUrl = "/pserver-web";
        var drawing = new WorkflowEditor();
        var qv = new QuickView();
        
        function saveDrawing() {
            drawing.saveDrawing();
        }

        function addBlock() {
            drawing.addBlock("8a80828129b6930d0129b69d5a33002d", 50, 50);
        }

        function resizeEditor(){
            var h = $(window).height();
            if (h < 200) {
              h = 200;
            }            
            
            var w = $(window).width();
            if(w<200){
                w = 200;
            }

            console.log('resize Editor');
            drawing.resizeUI(w, h);
        }
        
        $(document).ready(function() {
           /* clearTabs();
            setTab('workflowsTab', 'current');*/
            var div = document.getElementById("wrapper");
            div.setAttribute("id", "renamed-wrapper");
            
            $("#_tophr").css("display", "none");
            var settings = {
                divName: "drawingCanvas",
                height: 600,
                width: 1000,
                paletteWidth: 200/*,
                 showPalette: false,
                 showToolbar: false,
                 enableEditor: false*/
            };

            <%if(editable){%>
            drawing.allowSave = true;
            <%} else {%>
            drawing.allowSave = false;
            <%}%>
            qv.init("quickview");
            drawing.invocationsDialog.viewer = qv;
            drawing.quickView = qv;
            drawing.init(settings);
            drawing.fetchPalette();
            <%if(id.equalsIgnoreCase("new")){%>
            drawing.allowSave = true;
            drawing.createDrawing();
            <%}else{%>
            drawing.loadDrawing('<%=id%>', <%=versionId%>);
            <%}%>

            window.onbeforeunload = function(){
                if(drawing.drawing && drawing.drawing.dirty){
                    return "The workflow has not been saved. If you leave this page, you will lose any changes?";
                }
            };

            var doit;
            window.onresize = function(){
                clearTimeout(doit);
                doit = setTimeout(resizeEditor, 20);
            };

            resizeEditor();

        });
    </script>

    <%@ include file="../../WEB-INF/jspf/page/smallfooter.jspf" %>



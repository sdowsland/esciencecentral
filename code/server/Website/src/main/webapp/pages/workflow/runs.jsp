

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">

<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>  

<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>  
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
<script type="text/javascript" src="../../scripts/viewer/FolderViewer.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/WorkflowRuns.js"></script>


<!-- Caption Line -->
<h2 id="runtypelabel">Running Workflows</h2>

<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">        
      <div id="runstable"></div>
      <div class="dialog" id="confirmdialog"></div>        

    </div>

    
    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <li><h6><a href="../../pages/workflow/wfeditor.jsp?id=new">Create Workflow</a></h6></li>
            <li class="divider"/>
            <li><h6><a href="../../pages/workflow/list.jsp">My Workflows</a></h6></li>
            <li><h6><a href="../../pages/workflow/list.jsp?type=shared">Shared Workflows</a></h6></li>
            <li class="divider"/>
            <li><h6><a href="../../pages/workflow/autorun.jsp">Triggered Workflows</a></h6></li>
            <li class="divider"/>


            <li><h6><a onclick="fetchRunning();">Executing runs</a></h6></li>
            <li><h6><a onclick="fetchQueued();">Queued Runs</a></h6></li>
            <li><h6><a onclick="fetchFailed();">Failed runs</a></h6></li>
            <li class="divider"/>
            <li><h6><a onclick="killAll()">Kill ALL workflows</a></h6></li>
            <li><h6><a href="workflowlocks.jsp">Manage Workflow Locks</a></h6></li>

        </ul>
    </div>

</div>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

  <script type="text/javascript">
      var runViewer = new WorkflowRuns();
      var dialog = new ConfirmDialog();
      

      
      $(document).ready(function(){
        runViewer.init("runstable");
        runViewer.fetchRunningInvocations();
        dialog.init("confirmdialog");
      });
      
      function killAll(){
          dialog.yesCallback = function(){
              runViewer.killAllWorkflows();
          };
          dialog.noCallback = null;
          dialog.show("Are you sure you want to kill ALL of your workflows?");
      }
      
      function fetchRunning(){
          runViewer.fetchRunningInvocations();
          document.getElementById("runtypelabel").innerHTML = "Running Workflows";
      }

      function fetchQueued(){
          runViewer.fetchQueuedInvocations();
          document.getElementById("runtypelabel").innerHTML = "Queued Workflows";
      }

      function fetchFailed(){
          runViewer.fetchFailedInvocations();
          document.getElementById("runtypelabel").innerHTML = "Failed Workflows";
      }

  </script>

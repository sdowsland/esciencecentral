<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

<%@page import="com.connexience.server.model.workflow.*"%>
<%@page import="com.connexience.server.model.image.*"%>
<%@page import="com.connexience.server.ejb.util.*"%>
<%@page import="com.connexience.server.util.*"%>
<%@page import="java.util.*"%>
  <%
    List triggers = WorkflowEJBLocator.lookupWorkflowManagementBean().listUserTriggers(ticket, ticket.getUserId());
    NameCache cache = new NameCache(1000);
    
  %>

<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>  
<script type="text/javascript" src="../../scripts/wfeditor/DrawingExecutionManager.js"></script>

<script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
  
<h2>Automatically Triggered Workflows</h2>
<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">       
        <div id="triggerdialog" class="dialog"></div>
    
          <table id="triggerstable" cellpadding="0" cellspacing="0" border="0" width="100%" class="display">
              <thead>
                  <tr>
                      <th>Folder Name</th>
                      <th>Workflow Name</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                  <%
                  WorkflowFolderTrigger trigger;
                  for(int i=0;i<triggers.size();i++){
                      trigger = (WorkflowFolderTrigger)triggers.get(i);%>
                      
                        <tr>
                            <td><%=cache.getObjectName(ticket, trigger.getFolderId())%> </td>
                            <td><%=cache.getObjectName(ticket, trigger.getWorkflowId())%></td>
                            <td>
                                <a onclick="editTrigger('<%=trigger.getId()%>')">Edit</a>
                                <a onclick="deleteTrigger('<%=trigger.getId()%>')">Delete</a>
                            </td>
                        </tr>                      
                  <%}%>

              </tbody>
          </table>
    </div>

    
    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <li><h6><a href="wfeditor.jsp?id=new">Create Workflow</a></h6></li>
            <li class="divider"/>
            <li><h6><a href="list.jsp?type=user">My Workflows</a></h6></li>
            <li><h6><a href="list.jsp?type=shared">Shared Workflows</a></h6></li>
            <li class="divider"/>
            <li><h6><a href="runs.jsp">Running Workflows</a></h6></li>
            <li><h6><a href="autorun.jsp">Triggered Workflows</a></h6></li>            

        </ul>
    </div>

</div>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

  <script type="text/javascript">
      var manager = new DrawingExecutionManager();
      var editor = new PropertyEditor();
      $(document).ready(function(){
        clearTabs();
        setTab('workflowsTab', 'current');
          
          $("#triggerstable").dataTable({"bJQueryUI": true});
          editor.init("triggerdialog");
          editor.okCallback = saveTrigger;
      });
      
      
      function deleteTrigger(id){
          manager.deleteTrigger(id, function(){document.location = "../../pages/workflow/autorun.jsp"});
      }

      function editTrigger(id){
          manager.getTrigger(id, function(trigger){
              editor.triggerId = id;
              editor.editProperties(trigger.properties);
          });
      }
      
      function saveTrigger(properties){
          manager.saveTrigger(this.triggerId, properties, function(){});
      }
  </script>
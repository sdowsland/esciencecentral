<%@ page import="com.connexience.server.web.util.FlashUtils" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>e-Science Central</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

    <%@include file="../../WEB-INF/jspf/page/ticket.jspf"%>

  <link href="../../styles/<%=themeFolder%>/frontpage/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="../../styles/common/bootstrap.css" rel="stylesheet" media="screen">
  <link href="../../styles/<%=themeFolder%>/frontpage/css/style.css" rel="stylesheet" media="screen">
  <script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
  <script type="text/javascript" src="../../scripts/util/list.js"></script>"
  <script type="text/javascript" src="../../scripts/json/json2.js"></script>
  
  <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
      filter: none;
    }
  </style>
  <![endif]-->
  <script type="text/javascript" src="../../styles/<%=themeFolder%>/frontpage/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="../../styles/<%=themeFolder%>/frontpage/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../../scripts/util/flash.js"></script>

  <%-- Get any other Flashes --%>
  <%=FlashUtils.getFrontPageFlash(request)%>

</head>

<body id="splash-background" class="gradient">
    
<div id="wrap">
    <div id="flash-wrapper" class="clearfix">
        <div id="splash-logo">
            <span><%=tagLine%><br><em>LDAP Login</em></span>
        </div>
        <div id="splash-login">
            <form id="login-form" action="../../servlets/ldap" method="POST">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="username" id="username" placeholder="Username">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input type="password" name="password" id="password" placeholder="Password">
                </div>
                <%if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "AllowRememberMe", true)){%>
                    <label class="checkbox">
                        <input type="checkbox" name="rememberMe" value="true"> Remember me
                    </label>
                <%}%>
                <button id="login-button" type="submit" class="">Sign In</button>
            </form>
        </div>
    </div>
    <div id="push"></div>
</div>
<div id="footer">
    <p>This website uses cookies. By logging in you are agreeing to the use of cookies for anonymous tracking and preference saving purposes.</p>
</div>

</body>
</html>


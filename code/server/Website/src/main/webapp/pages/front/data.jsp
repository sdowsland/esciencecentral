<%--
  Demo Page
  Author: Simon
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.connexience.server.model.folder.SearchFolder" %>
<%@ page import="com.connexience.server.model.folder.Folder" %>
<%@ page import="com.connexience.server.model.security.Permission" %>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

<script type="text/javascript" src="../../scripts/jquery-selectbox/jquery.selectBox.js"></script>
<link rel="stylesheet" type="text/css" href="../../scripts/jquery-selectbox/jquery.selectBox.css"/>

<%--QuickView--%>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">

<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>
<script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>


<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>


<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>

<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>

<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>

<!-- elFinder CSS (REQUIRED) -->
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.min.css">


<%--ACL--%>
<link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen"
      charset="utf-8"/>

<!-- elFinder JS (REQUIRED) -->
<script type="text/javascript" src="../../scripts/elfinder-esc/js/jQuery.elFinder.js"></script>
<script type="text/javascript" src="../../scripts/elfinder-esc/js/jQuery.elFinder.esc.js"></script>

<%--ACL--%>
<script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">


<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.esc.css">

<script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
<script type="text/javascript" src="../../scripts/datasets/views/MultiRowTableView.js"></script>
<script type="text/javascript" src="../../scripts/datasets/views/SingleRowJsonView.js"></script>
<script type="text/javascript" src="../../scripts/datasets/DatasetManager.js"></script>
<script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
    
<%
    String folderId = request.getParameter("folderid");
    String volString = "";
    if (folderId != null) {

        volString = "?vol0=" + folderId;
    } else {
        int count;
        //Add the default folder Id first
        String defaultFolderId = ticket.getDefaultStorageFolderId();
        volString = "";
        if (defaultFolderId == null || defaultFolderId.equals("")) {
            volString += "?vol0=" + user.getHomeFolderId() + "&";
            count = 1;
        } else {

            Folder defaultFolder = (Folder) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, ticket.getDefaultStorageFolderId(), Folder.class);

            if (defaultFolder != null) {
                if (EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, defaultFolder, Permission.READ_PERMISSION)) {
                    volString += "?vol0=" + defaultFolderId + "&vol1=" + user.getHomeFolderId() + "&";
                    count = 2;
                } else {
                    ticket.setDefaultStorageFolderId(null);
                    volString += "?vol0=" + user.getHomeFolderId() + "&";
                    count = 1;
                }
            } else {
                ticket.setDefaultStorageFolderId(null);
                volString += "?vol0=" + user.getHomeFolderId() + "&";
                count = 1;
            }


            volString += "?vol0=" + defaultFolderId + "&vol1=" + user.getHomeFolderId() + "&";
            count = 2;
        }


        //add the other oldProjects
        for (Project project : projects) {

            //Don't add the default folder again
            if (!project.getDataFolderId().equals(defaultFolderId)) {
                volString += "vol" + count + "=" + project.getDataFolderId() + "&";
                count++;
            }
        }
        // Add the search folders
        List searchFolders = EJBLocator.lookupObjectDirectoryBean().getOwnedObjects(ticket, ticket.getUserId(), SearchFolder.class, 0, 0);
        SearchFolder f;
        for (Object searchFolder : searchFolders) {
            f = (SearchFolder) searchFolder;
            volString += "vol" + count + "=" + f.getId() + "&";
            count++;
        }
        if (volString.endsWith("&")) {
            volString = volString.substring(0, volString.length() - 1);
        }

    }
%>

<!-- elFinder initialization (REQUIRED) -->
<script type="text/javascript" charset="utf-8">


    $().ready(function () {
                    tinyMCE.init({
                            // General options
                            mode : "inline",
                            theme : "advanced",
                            plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,insertdate,inserttime,|,forecolor,backcolor",
                            theme_advanced_resizing : true,
                            theme_advanced_toolbar_location : "top",
                            theme_advanced_toolbar_align : "left",
                            theme_advanced_statusbar_location : "bottom"
                    });      
                    1
        var myCommands = elFinder.prototype._options.commands;

        <%
           if(!EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
        %>
        var disabled = ['escarchive', 'escrestore'];
        $.each(disabled, function (i, cmd) {
            (idx = $.inArray(cmd, myCommands)) !== -1 && myCommands.splice(idx, 1);
        });

        <%}%>
        var options = {
            commands: myCommands,
            url: '/beta/servlets/elfinder<%=volString%>',
            resizable: false,
            rememberLastDir: false,
            uiOptions: {
                contextmenu: {
                    navbar: ['open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
                    cwd: [/*'reload',*/ 'back', '|', 'escupload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'escinfo'],
                    files: ['getfile', '|', 'open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', '|', 'archive', 'extract', '|', 'info', 'escprovenance']
                },
                toolbar: [
                    ['back', 'forward'],
                    ['netmount'],
                    ['mkdir', 'mkfile', 'escupload', 'escmkdataset','escextractdataset'],
                    ['open', 'getfile'],
                    ['escinfo', 'escprovenance'], //SMW add 'escprovenance'
                    ['escquickview', 'acl', 'escarchive', 'escrestore'],
                    ['copy', 'cut', 'paste'],
                    ['rm'],
                    ['rename', 'edit'],
                    ['extract', 'archive'],
                    ['view', 'sort']
                ]}
        };

        var elf = $('#elfinder').elfinder(options).elfinder('instance');

        elf.bind('dblclick', function (event) {
            alert('hi!')
        });

        $(window).on("load", resizeFinder);
        $(window).on("resize", resizeFinder);

    });

    //function to resize the file browser when the window size changes
    function resizeFinder() {
        var h = $(window).height();
        if (h < 600) {
            h = 600;
        }

        var elfinderHeight = h - 300;
        var workzoneHeight = elfinderHeight - 54;
        var navbarHeight = elfinderHeight - 60;

        $(".elfinder").css('height', elfinderHeight);
        $(".elfinder-workzone").css('height', workzoneHeight);
        $(".elfinder-cwd-wrapper").css('height', workzoneHeight);
        $(".elfinder-navbar").css('height', navbarHeight);

    }
</script>
<style>
    .ui-resizable {
        position: absolute;
    }
</style>

<input type="hidden" id="currentUserId" value="<%=user.getId()%>">
<input type="hidden" id="publicUserId" value="<%=organisation.getDefaultUserId()%>">
<input type="hidden" id="enablePublic"
       value="<%=pageContext.getServletContext().getInitParameter("public_sharing_enabled")%>">
<input type="hidden" id="usersGroupId" value="<%=organisation.getDefaultGroupId()%>">

<div class="row-fluid">
    <div class="span12">
        <div id="elfinder"></div>
        <div id="quickviewdiv"></div>
    </div>
</div>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

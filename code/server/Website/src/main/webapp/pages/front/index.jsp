<%@ page import="com.connexience.server.web.tags.*" %>
<%@ page import="com.connexience.server.model.social.*"%>
<%--
  Index Page
  Author: Simon
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%

    if(publicUser)
    {
      response.sendRedirect("../../pages/front/welcome.jsp");
    }

    String dashboardText = "Dashboard for " + user.getDisplayName();

  %>
    <%if(com.connexience.server.ejb.util.EJBLocator.lookupPreferencesBean().booleanValue("Website", "ShowHomePageDashboard", true)){%>
    <script type="text/javascript" src="../../scripts/dashboard/defaults/DefaultDashboard.js"></script>
    <script type="text/javascript" src="../../scripts/dashboard/DashboardViewManager.js"></script>
    <script type="text/javascript">
        // Set up the view manager to use save to the main users dashboard
        var dashboardViewManager = new DashboardViewManager();
        dashboardViewManager.saveAsMainDashboard = true;
        dashboardViewManager.userId = '<%=ticket.getUserId()%>';
    </script>

    <%@include file="dashboard.jspf" %>
    <hr/>
  <%}%>


<div class="row-fluid" id="_introDiv">
    
</div>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#_introDiv").load("../../styles/<%=themeFolder%>/intro.jspf");
    });
</script>
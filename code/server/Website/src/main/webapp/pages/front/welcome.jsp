<%@ page import="com.connexience.server.web.util.FlashUtils" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>e-Science Central</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

    <%@include file="../../WEB-INF/jspf/page/ticket.jspf"%>

  <%
      //redirect to the home page if we've got a valid ticket.  Gets hit when successful login follows an unsuccessful one
      //as the page being requested after login is welcome.jsp
      if(ticket != null && organisation != null && !ticket.getUserId().equals(organisation.getDefaultUserId()))
      {
          response.sendRedirect("../../pages/front/index.jsp");
          return;
      }

      //deal with login failure
      LoginReport loginReport = null;
      if(session.getAttribute("LOGINREPORT") != null) {
          loginReport = (LoginReport) session.getAttribute("LOGINREPORT");
          System.out.println("report.getReason() = " + loginReport.getReason());
          session.removeAttribute("LOGINREPORT");
      }


      // if web.xml contains ldap.auth == true and ldap.force == true AND
      // if the http request does NOT include "force=true" query param THEN
      // bounce them to ldap.jsp
      if ("true".equals(request.getServletContext().getInitParameter("ldap.enabled")) &&
          "true".equals(request.getServletContext().getInitParameter("ldap.force")) &&
          !"true".equals(request.getParameter("force"))) {
          response.sendRedirect("ldap.jsp");
          return;
      }
  %>

  <link href="../../styles/<%=themeFolder%>/frontpage/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="../../styles/common/bootstrap.css" rel="stylesheet" media="screen">
  <link href="../../styles/<%=themeFolder%>/frontpage/css/style.css" rel="stylesheet" media="screen">
  <script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
  <script type="text/javascript" src="../../scripts/util/list.js"></script>"
  <script type="text/javascript" src="../../scripts/json/json2.js"></script>
  
  <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
      filter: none;
    }
  </style>
  <![endif]-->
  <script type="text/javascript" src="../../styles/<%=themeFolder%>/frontpage/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="../../styles/<%=themeFolder%>/frontpage/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../../scripts/util/flash.js"></script>


  <script type="text/javascript">
      function resetPassword (){
        var currentPassword = $("#currentpassword").val();
        var newPassword1 = $("#newpassword1").val();
        var newPassword2 = $("#newpassword2").val();
        var failedUserId = $("#userid").val();
        var username = $("#username").val();
        if(newPassword1===newPassword2){
            var cb = function(o){
                if(o.error===false){
                    $("#password").val(newPassword1);
                    $("#username").val(username);
                    document.getElementById("login-form").submit();
                }
            };
            
            var errorCb = function(message, o){
                alert("Error changing password: " + message);
            };
            
            jsonCall({currentPassword: currentPassword, newPassword: newPassword1, userId:failedUserId}, "../../unsecured/servlets/profile?method=resetPassword", cb, errorCb);
        } else {
            alert("Passwords do not match");
        }
      }
      
    $(document).ready(function () {
      <%-- Make the register button go to the register page --%>
      <%if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "AllowUserRegistrations", true)){%>              
        $(document).on('click','#register-button',function (e) {
          e.preventDefault();
            window.location.assign("../../unsecured/signup/stage1.jsp");

        });
      <%}%>



<%if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "AllowUserPasswordReminders", true)){%>
        $(document).on('click','#resetPassword-button', function (e) {
            e.preventDefault();
            window.location.assign("../../unsecured/signup/resetPassword.jsp");

        });
<%}%>

      <%-- Enable Shib if necessary--%>
      <%
      if ("true".equals(pageContext.getServletContext().getInitParameter("shibEnabled"))){%>
      $("#alternative-button").show();
      <%}
      %>


    });
  </script>

  <%
    //If there is a failed logon then set the username and show a flash message
    String logonFormUsername = "";
    if (loginReport != null) {
        logonFormUsername = loginReport.getUsername();

      String failMessage;
      if(loginReport.getReason() != null && !loginReport.getReason().equals("")){
          failMessage = loginReport.getReason();
      } else {
          failMessage = "Incorrect username and password combination";
      }

  %>

  <%if(loginReport.getNextAction() != null && loginReport.getNextAction().equals(LoginNextAction.RESET_PASSWORD)){%>
    <script type="text/javascript">
    $(document).ready(function () {
      $("#userid").val('<%=loginReport.getUsername()%>');
      $("#form-content").modal('show');
    });
  </script>    
    
  <%}%>
  
  <%if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "AllowUserPasswordReminders", true)  && loginReport.isAllowPasswordReset()){%>
  <script type="text/javascript">
    $(document).ready(function () {
      flashMessage("<%=failMessage%>. <a href='../../unsecured/signup/resetPassword.jsp'>Click here to reset your password</a>","error",$("#splash-logo span"));
    });
  </script>
 <%} else {%>
    <script type="text/javascript">
    $(document).ready(function () {
      flashMessage("<%=failMessage%>.","error",$("#splash-logo span"));
    });
  </script>
  <%}%>
  <%
    }
  %>

  <%-- Get any other Flashes --%>
  <%=FlashUtils.getFrontPageFlash(request)%>

</head>

<body id="splash-background" class="gradient">
    
<div id="form-content" class="modal hide fade in" style="display: none;">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3>Your password must be reset</h3>
    </div>
    <div class="modal-body">
        <form class="contact" name="contact">
            <label class="label" for="name">Current Password</label><br>
            <input id="currentpassword" type="password" name="name" class="input-xlarge"><br>
            <label class="label" for="password1">New Password</label><br>
            <input id="newpassword1" type="password" name="password1" class="input-xlarge"><br>
            <label class="label" for="password2">New Password Again</label><br>
            <input id="newpassword2" type="password" name="password2" class="input-xlarge"><br>
            <input type="hidden" id="userid" name="userid" value=""/>
        </form>
    </div>
    <div class="modal-footer">
        <input class="btn btn-success" type="submit" id="resetbutton" onclick="resetPassword()" value="Reset Password" />
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
    </div>
</div>
    
    
<div id="wrap">
    <div id="flash-wrapper" class="clearfix">
        <div id="splash-logo">
            <span><%=tagLine%></span>
        </div>
        <div id="splash-login">
            <form id="login-form" method="POST" action="j_security_check">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="j_username" id="username" placeholder="Email" value="<%=logonFormUsername%>">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input type="password" name="j_password" id="password" placeholder="Password">
                </div>
                <%if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "AllowRememberMe", true)){%>
                    <label class="checkbox">
                        <input type="checkbox" name="rememberMe" value="true"> Remember me
                    </label>
                <%}%>
                <button id="login-button" type="submit" class="">Sign In</button>
                <%if(EJBLocator.lookupPreferencesBean().booleanValue("Security", "AllowUserRegistrations", true)){%>
                <button id="register-button" type="submit" class="">Register</button>
                <%}%>

                
                <%
                    if("true".equals(getServletConfig().getInitParameter("shibEnabled"))){
                %>
                <a id="alternative-button" href="#signin-modal" role="button" class="btn" data-toggle="modal">Alternative
                Credentials</a>
                <%
                    }
                %>
            </form>
        </div>
    </div>
    <div id="push"></div>
</div>
<div id="footer">
    <p>This website uses cookies. By logging in you are agreeing to the use of cookies for anonymous tracking and preference saving purposes.</p>
</div>

<%
    if("true".equals(getServletConfig().getInitParameter("shibEnabled"))){
%>
<div id="signin-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="signin-modal-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="signin-modal-label">Alternative Credentials</h3>
    </div>
    <div class="modal-body">
        <a href="https://esc-serv.ncl.ac.uk/beta/servlets/shibboleth" id="login-shibboleth" class="alt-login">Shibboleth</a>
    </div>
    <div class="modal-footer">

    </div>
</div>
<%
    }
%>

</body>
</html>


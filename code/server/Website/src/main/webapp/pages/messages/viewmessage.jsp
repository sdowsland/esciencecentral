<%--
  Demo Page
  Author: Simon
--%>

    <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

    <script type="text/javascript" src="../../scripts/messages/messages.js"></script>
    <script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>


    <script type="text/javascript">
        <%
        String threadId = request.getParameter("id");
        if(!WebUtil.checkParam(threadId))
        {
            response.sendRedirect("../../pages/messages/inbox.jsp");
         }
        %>

        $(document).ready(function()
        {
            clearTabs();

            var messages = new Messages();
            messages.init('messageList', '<%=user.getId()%>', <%=START%>, <%=PAGE_SIZE%>);
            messages.getMessageThread('<%=threadId%>');
            messages.initEditor("editor");

            $("#sendMessageButton").click(function()
            {
                messages.sendMessage("editor", $("#messageTitle").html(), '<%=threadId%>');
            });


        });
    </script>

<h2 id="messageTitle"></h2>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
    <!-- Caption Line -->

        <div id="messageList" class="accordion">

        </div>

        <div class="accordion-group">
            <div class="accordion-heading">
                <div class="accordion-toggle"><i class="icomoon-pencil"></i>&nbsp;&nbsp;Reply</div>
            </div>
            <div id="thread-reply" class="accordion-body">
                <div class="accordion-inner">
                    <textarea id="editor" name="editor">&nbsp;</textarea>
                    <a id="sendMessageButton" class="btn btn-primary" href="#">Send Message</a>
                </div>
            </div>

        </div>

    <%-- Main Content Left Hand Side--%>
    </div>
    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <li>
                <h4><a href="inbox.jsp"><i class="icomoon-list"></i>Inbox</a></h4>
            </li>
        </ul>
    </div>
</div>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
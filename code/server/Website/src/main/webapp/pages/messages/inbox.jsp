<%--
  Demo Page
  Author: Simon
--%>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

  <script type="text/javascript" src="../../scripts/messages/messages.js"></script>

  <%
    NUM_OBJECTS = EJBLocator.lookupMessageBean().getNumberOfMessages(ticket, ticket.getUserId(), user.getInboxFolderId());
  %>

  <script type="text/javascript">
    $(document).ready(function ()
    {
      clearTabs();

      var messages = new Messages();
      messages.init('messageList', '<%=user.getId()%>', '<%=START%>', '<%=PAGE_SIZE%>');
      messages.getMessageList();

    });
  </script>

  <style type="text/css">
    .unread {
      background-color: #f0f8ff;
    }
  </style>

  <!-- Caption Line -->
  <h2>Your Messages</h2>
  <div class="row-fluid">
  <%-- Main Content Left Hand Side--%>
  <div class="span9">
    <div id="messageList" class="accordion">
    </div>
    <div class="list">
      <%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>
    </div>
  </div>
  <!-- Right Hand Side -->
  <div class="span3">

    <ul class="unstyled sidebar-menu">
      <li>
          <h4><a href="../../pages/messages/createmessage.jsp"><i class="icomoon-envelope"></i>New Message</a></h4>
      </li>
    </ul>
  </div>
  </div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
</div>

</body>
</html>

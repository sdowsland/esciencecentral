<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="com.connexience.server.model.document.DocumentRecord" %>
<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<%
    try
    {
        Integer id = Integer.parseInt(request.getParameter("id"));

        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, id);
        menu_study = study;

        List<String> results = null;
        String loggerIdColumn = "D";
        String fileName = "";
        DocumentRecord doc = new DocumentRecord();
        String errorMessage = "";

        String contentType = request.getContentType();

        if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0))
        {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List items = upload.parseRequest(request);

            Iterator iter = items.iterator();

            ArrayList<String> fileNames = new ArrayList<String>();
            ArrayList<Long> sizes = new ArrayList<Long>();

            while (iter.hasNext())
            {
                FileItem item = (FileItem) iter.next();

                System.out.println(item.getFieldName());

                if (item.isFormField())
                {
                    if ("loggerColumn".equals(item.getFieldName()))
                    {
                        loggerIdColumn = item.getString();
                    }
                }
                else
                {
                    if (!item.isFormField())
                    {
                        fileName = item.getName();

                        fileNames.add(fileName);
                        sizes.add(item.getSize());

                        String dataFolderId = study.getDataFolderId();

                        doc.setName(fileName);
                        doc.setContainerId(dataFolderId);
                        doc.setCreatorId(ticket.getUserId());
                        doc.setProjectId(ticket.getDefaultProjectId());
                        doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);

                        StorageUtils.upload(ticket, item.getInputStream(), item.getSize(), doc, "Saved by user: " + user.getDisplayName());
                    }
                }
            }

            System.out.println("LoggerId Column: " + loggerIdColumn);

            results = EJBLocator.lookupStudyParserBean().parseSubjectProperties(ticket, doc.getId(), study.getId());

        }
%>
<div class="row-fluid">
    <div class="span9">
        <div class="row-fluid" id="structure-upload">
            <form id="upload-structure" action="upload-subjects.jsp?id=<%=study.getId()%>" method="POST" enctype="multipart/form-data" class="clearfix">
                <fieldset>
                    <legend>Upload Subject Properties</legend>
                    <ol>
                        <li class="stage-1 current-stage">
                            <div class="control-group">
                                <label class="control-label">Study Subjects File</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new clearfix" data-provides="fileupload">
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select file</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input name="structureFile" id="structureFile" type="file" />
                                        </span>
                                        <div id="file-preview" class="stage-preview">
                                            <h6 class="fileupload-preview"></h6>
                                        </div>
                                    </div>
                                    <span class="help-block">Choose the excel file containing the subject properties you wish to use for this study.</span>
                                </div>
                            </div>
                        </li>
                        <li class="stage-2">
                            <div class="control-group">
                                <div class=controls>
                                    <button type="submit" id="uploadStructure" class="btn" disabled="disabled">
                                        <i class="icomoon-cloud-upload"></i>&nbsp;Upload
                                    </button>
                                </div>
                            </div>
                        </li>
                    </ol>
                </fieldset>
            </form>
        </div>
        <hr>
        <div class="row-fluid" id="upload-results">
            <div class="span12">
                <% if(errorMessage != "") { %>
                   <p><%=errorMessage%></p>
                <% } %>
                <% if(results != null) { %>
                <h5><%=results.size()%> Warnings</h5>
                <ul id="parsingWarnings" class="unstyled">
                    <% for(String problem : results) { %>
                    <li class="alert">
                        <i class="icomoon-warning"></i>
                        <strong>Warning!</strong>&nbsp;<%= problem %>
                    </li>
                    <% } %>
                </ul>
                <% } %>
            </div>
        </div>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(event){

        $(document).on('change', '#structureFile', function(){


            console.log($(this).val());

            if($(this).val() == "")
            {
                $('#structureFile').removeAttr('disabled', 'disabled');
                $('#uploadStructure').attr('disabled');
                $('.stage-1').addClass('current-stage');
                $('.stage-2').removeClass('current-stage');
            }
            else
            {
                $('#structureFile').attr('disabled', 'disabled');
                $('#uploadStructure').removeAttr('disabled');
                $('.stage-1').removeClass('current-stage');
                $('.stage-2').addClass('current-stage');
            }
        });

        <% if(results != null) { %>

        $('#upload-subject-properties h4').html('<i class="icomoon-checkmark" style="color:#009900"></i>Upload Subject Properties');

        <% } %>
    });

</script>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    Integer id = Integer.parseInt(request.getParameter("id"));

    try{

        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, id);
        menu_study = study;
%>
<!-- Wrapping div tag with ID to apply styling to this specific page using the ID -->
<div id="study-deployments">
    <!-- Title block above page content -->
    <h2><%=study.getName() %></h2>
    <h4>Deployments</h4>
    <!-- Bootstrap fluid row is a responsive full with div needed to contain 12 column grid system-->
    <div class="row-fluid">
        <!-- Main content column is 3/4 of width of row, or 9 grid column widths -->
        <div class="span9">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="deployments">
                <thead>
                <tr>
                    <th>Study Group</th>
                    <th>Serial Number</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
        <!-- Sidebar Menu contained in remaining 1/4 of space -->
        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>
    </div>
</div>
<script type="text/javascript">

    var selectedDeployments = [];

    $(document).ready(function(){

        var deploymentsTable;

        $(document).on('click','#undeploy', function(){

            if(selectedDeployments.length == 1)
            {
                $("#deployment-modal-label").text("Undeploy Logger");
                $("#remove-deployment .modal-body").text("Are you sure you want to undeploy this logger?");
            }
            else if(selectedDeployments.length > 1)
            {
                $("#deployment-modal-label").text("Undeploy Loggers");
                $("#remove-deployment .modal-body").text("Are you sure you want to undeploy these " + selectedDeployments.length +  " loggers?");
            }

            $("#deployment-modal").modal("show");

        });

        $('table#deployments').on('click', 'tr', function () {

            var rowData = deploymentsTable.fnGetData(deploymentsTable.fnGetPosition(this));
            var index = jQuery.inArray(rowData, selectedDeployments);

            if ( index === -1 )
            {
                selectedDeployments.push(rowData);
            }
            else
            {
                selectedDeployments.splice(index, 1);
            }

            if(selectedDeployments.length > 0)
            {
                $('#undeploy').prop('disabled', false);
            }
            else
            {
                $('#undeploy').prop('disabled', true);
            }

            $(this).toggleClass('active');
        } );

        deploymentsTable = $('#deployments').dataTable( {
            "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "oLanguage": {
                "sSearch": ""
            },
            "bRetrieve": true,
            "sAjaxSource": "/website-api/rest/logger/<%=study.getId()%>/deployments",
            "sAjaxDataProp": "",
            "aoColumns": [
                {
                    "mData": "subjectGroup.displayName",
                    "sDefaultContent": ''
                },
                {
                    "mData": "logger.serialNumber",
                    "sDefaultContent": ''
                },
                {
                    "mData": "logger.loggerType.name",
                    "sDefaultContent": ''
                }
                ,
                {
                    "mData": "logger.loggerType.manufacturer",
                    "sDefaultContent": ''
                }
            ]
        });

        $('.dataTables_filter input').attr('placeholder', 'Search...');

        var editButtons =   '<button id="undeploy" class="btn" disabled="true">Undeploy</button>';

        $("#deployments_wrapper .btn-group").html(editButtons);
    });


</script>
<%@include file="modals/deployment.jspf" %>
<%--<%@include file="modals/logger-deployment.jspf" %>--%>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
            <%
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@ page import="com.connexience.server.model.project.study.*" %>
<%@ page import="org.apache.commons.collections4.CollectionUtils" %>
<%@ page import="org.apache.commons.collections4.TransformerUtils" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<%
    Integer id = Integer.parseInt(request.getParameter("id"));

    try{

        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, id);
        menu_study = study;

        Collection<String> admins = CollectionUtils.collect(EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, study.getAdminGroupId()), TransformerUtils.invokerTransformer("getId"));

        //Todo: Add tabs to switch phases
        //for now just use the first phase in the study
        List<Phase> phases = EJBLocator.lookupStudyBean().getPhases(ticket, id);
        Phase currentPhase = phases.get(0);
%>
<div id="participants">
    <div class="row-fluid">
        <div class="span9">
            <h2><%=study.getName() %></h2>
            <% if(phases.size() > 0){ %>
            <ul class="nav nav-tabs phase-tabs">
                <li>
                    <span>Phases</span>
                </li>
                <% for(Phase phase : phases){ %>
                <li data-phase="<%=phase.getId()%>"<% if(phase.equals(currentPhase)){%> class="phase active" <%} else {%> class="phase" <%}%>>
                    <a data-toggle="tab"><%=phase.getName()%>
                        <% if(admins.contains(ticket.getUserId())){%>
                        <div class="phase-menu">
                            <i class="editPhase icon-pencil"></i>
                            <% if(phases.size() > 1){ %>
                            <i class="deletePhase icon-trash"></i>
                            <%}%>
                        </div>
                        <%}%>
                    </a>
                </li>
                <% } %>
                <% if(admins.contains(ticket.getUserId())){%>
                <li>
                    <a class="createPhase"><i class="icon-plus"></i></a>
                </li>
                <% } %>
            </ul>
            <table cellpadding="0" cellspacing="0" border="0" class="groups table table-bordered" id="groups-0">
                <thead>
                    <tr>
                        <th width="20px"></th>
                        <th>Name</th>
                        <th>Subjects</th>
                        <th width="14px"></th>
                        <th width="14px"></th>
                        <%--<th width="14px"></th>--%>
                        <%--<th width="14px"></th>--%>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>
            <% } else { %>

            <h4>There are currently no phases. Click <a class="createPhase">here</a> to begin.</h4>

            <% } %>
        </div>
        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>
    </div>
</div>

<script type="text/javascript">

    //Holds the id of the current phase of the active tab
    var phaseId = <%=currentPhase.getId()%>;
    //Holds the ID of the open subject group when building sub groups, set at 0 for root level.
    var currentSubjectGroup = 0;
    //Holds the rowData objects for deletions.
    var deletions = [];
    //holds the deployment for editing
    var currentDeployment;

    //Holds the table objects for any tables with open rows.
    var anOpen = [];
    //Holds an array of group rowData objects for a given tableID.
    var selectedSubjectGroups = [];
    //Holds an array of subject rowData objects for a given tableID.
    var selectedSubjects = [];
    //Holds an array of deployment rowData objects for a given tableID.
    var selectedDeployments = [];

    $(document).ready(function(){

        //Call to build the root level subject groups table.
        buildSubjectGroupTable(0);

        $('.groups').on('click', 'td.control', function () {

            var nTr = this.parentNode;
            var table = $(nTr).closest('table').dataTable();
            var oData = table.fnGetData(nTr);

            var i = $.inArray( nTr, anOpen );

            if ( i === -1 ) {
                $('i', this).attr( 'class', "icon-minus" );
                table.fnOpen(nTr, fnFormatDetails(table, nTr), 'details' );

                buildSubjectGroupTable(oData.id);
                buildSubjectTable(oData.id);
                buildDeploymentsTable(oData.id);

                anOpen.push( nTr );
            }
            else {
                $('i', this).attr( 'class', "icon-plus" );
                table.fnClose( nTr );
                anOpen.splice( i, 1 );
            }
        });

        function fnFormatDetails( table, nTr )
        {
            var oData = table.fnGetData(nTr);

            currentSubjectGroup = oData.id;

            var parentTables = $('#' + table[0].id).parents('table');

            var html = '<div class="innerDetails">';

            html += '<h4 id="groups-' + currentSubjectGroup + '-heading">Level ' + (parentTables.length + 2) + '</h4>';

            html += '<table id="groups-' + currentSubjectGroup + '" class="groups table table-bordered" cellpadding="5" cellspacing="0" border="0">';

            html += '<thead><tr>' +
                    '<th width="20px"></th>' +
                    '<th>Name</th>' +
                    '<th>Subjects</th>' +
                    '<th width="14px"></th>' +
                    '<th width="14px"></th>' +
                    /*'<th width="14px"></th>' +*/
                    /*'<th width="14px"></th>' +*/
                    '</tr></thead>';

            html += '<tbody>' +
                    '</tbody>';

            html += '<tfoot>' +
                    '</tfoot>';

            html += '</table>';

            html += '<h4>Subjects</h4>';

            html += '<table id="subjects-' + currentSubjectGroup + '" class="subjects table table-bordered" cellpadding="5" cellspacing="0" border="0">';

            html += '<thead><tr>' +
                    '<th>Subject ID</th>' +
                    '<th width="14px"></th>' +
                    '</tr></thead>';

            html += '<tbody>' +
                    '</tbody>';

            html += '<tfoot>' +
                    '</tfoot>';

            html += '</table>';

            html += '<h4>Loggers</h4>';

            html += '<table id="deployments-' + currentSubjectGroup + '" class="deployments table table-bordered" cellpadding="5" cellspacing="0" border="0">';

            html += '<thead><tr>' +
                    '<th>Serial Number</th>' +
                    '<th>Type</th>' +
                    '<th>Start Date</th>' +
                    '<th>End Date</th>' +
                    '<th width="14px"></th>' +
                    '</tr></thead>';

            html += '<tbody>' +
                    '</tbody>';

            html += '<tfoot>' +
                    '</tfoot>';

            html += '</table></div>';

            return html;
        }

        $('table.groups').on('click', 'td.subject-selectable', function () {

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');
            var subjectID = (tableID.split('-')[1]);

            if(selectedSubjects[tableID] == undefined)
            {
                selectedSubjects[tableID] = [];
            }

            var row = $(this).closest("tr")[0];
            var rowData = table.fnGetData(table.fnGetPosition(row));
            var index = jQuery.inArray(rowData, selectedSubjects[tableID]);

            if ( index === -1 )
            {
                selectedSubjects[tableID].push(rowData);
            }
            else
            {
                selectedSubjects[tableID].splice(index, 1);
            }

            if(selectedSubjects[tableID].length == 1)
            {
                $('#editSubject_' + subjectID).prop('disabled', false);
                $('#deleteSubject_' + subjectID).prop('disabled', false);
            }
            else if(selectedSubjects[tableID].length > 1)
            {
                $('#editSubject_' + subjectID).prop('disabled', true);
                $('#deleteSubject_' + subjectID).prop('disabled', false);
            }
            else
            {
                $('#editSubject_' + subjectID).prop('disabled', true);
                $('#deleteSubject_' + subjectID).prop('disabled', true);
            }

            $(row).toggleClass('active');
        });

        $('table.groups').on('click', 'td.group-selectable', function () {

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');

            if(selectedSubjectGroups[tableID] == undefined)
            {
                selectedSubjectGroups[tableID] = [];
            }

            var row = $(this).closest("tr")[0];
            var rowData = table.fnGetData(table.fnGetPosition(row));
            var index = jQuery.inArray(rowData, selectedSubjectGroups[tableID]);

            var groupID = (tableID.split('-')[1]);

            if ( index === -1 )
            {
                selectedSubjectGroups[tableID].push(rowData);
            }
            else
            {
                selectedSubjectGroups[tableID].splice(index, 1);
            }

            if(selectedSubjectGroups[tableID].length == 1)
            {
                $('#editSubjectGroup_' + groupID).prop('disabled', false);
                $('#deleteSubjectGroup_' + groupID).prop('disabled', false);
            }
            else if(selectedSubjectGroups[tableID].length > 1)
            {
                $('#editSubjectGroup_' + groupID).prop('disabled', true);
                $('#deleteSubjectGroup_' + groupID).prop('disabled', false);
            }
            else
            {
                $('#editSubjectGroup_' + groupID).prop('disabled', true);
                $('#deleteSubjectGroup_' + groupID).prop('disabled', true);
            }

            $(row).toggleClass('active');
        });

        $('table.groups').on('click', 'td.deployment-selectable', function () {

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');

            if(selectedDeployments[tableID] == undefined)
            {
                selectedDeployments[tableID] = [];
            }

            var row = $(this).closest("tr")[0];
            var rowData = table.fnGetData(table.fnGetPosition(row));
            var index = jQuery.inArray(rowData, selectedDeployments[tableID]);

            var groupID = (tableID.split('-')[1]);

            if ( index === -1 )
            {
                selectedDeployments[tableID].push(rowData);
            }
            else
            {
                selectedDeployments[tableID].splice(index, 1);
            }

            if(selectedDeployments[tableID].length == 1)
            {
                $('#deleteDeployment_' + groupID).prop('disabled', false);
                $('#editDeployment_' + groupID).prop('disabled', false);
            }
            else if(selectedDeployments[tableID].length > 1){
                $('#deleteDeployment_' + groupID).prop('disabled', false);
                $('#editDeployment_' + groupID).prop('disabled', true);
            }
            else
            {
                $('#deleteDeployment_' + groupID).prop('disabled', true);
                $('#editDeployment_' + groupID).prop('disabled', true);
            }

            $(row).toggleClass('active');
        });

        if($("#studyID").length > 0)
        {
            $("#studyID").val('<%=request.getParameter("id")%>');
        }

        $(document).on("click", ".createPhase", function (event) {

            $("#phaseName").val('');

            $("#phase-modal-label").text("Create Phase");
            $("#edit-phase").css('display', 'block');
            $("#delete-phase").css('display', 'none');
            $("#submit-phase").text("Create");

            $("#phase-modal").modal("show");
        });

        $(document).on("click", ".editPhase", function (event) {

            $("#phaseId").val(phaseId);
            $("#phaseName").val($(this).parent().parent().text().trim());

            $("#phase-modal-label").text("Edit Phase");
            $("#edit-phase").css('display', 'block');
            $("#delete-phase").css('display', 'none');
            $("#submit-phase").text("Save");

            $("#phase-modal").modal("show");
        });

        $(document).on("click", ".deletePhase", function (event) {

            $("#phaseName").val('');
            $("#deletePhaseId").val(phaseId);

            $("#phase-modal-label").text("Remove Phase");
            $("#edit-phase").css('display', 'none');
            $("#delete-phase").css('display', 'block');
            $("#submit-delete-phase").text("Confirm");

            $("#phase-modal").modal("show");
        });

        $(document).on("click", ".createGroup", function (event) {

            var parentGroupID = ($(this).attr('id')).split('_')[1];

            $("#parentGroupId").val(parentGroupID);
            $("#phaseId").val(phaseId);
            $("#subjectGroupName").val('');

            $("#subject-group-modal-label").text("Create Subject Group");
            $("#edit-subject-group").css('display', 'block');
            $("#delete-subject-group").css('display', 'none');
            $("#submit-group").text("Create");

            $("#subject-group-modal").modal("show");
        });

        $(document).on("click", ".editGroup", function (event)
        {
            var parentGroupID = ($(this).attr('id')).split('_')[1];
            var data = (selectedSubjectGroups['groups-' + parentGroupID][0]);

            $("#dbId").val(data.id);

            $("#parentGroupId").val(parentGroupID);
            $("#subjectGroupName").val(data.displayName);

            $('#edit-subject-group .additional-properties').html('');

            $.each(data.additionalProperties, function(key, value){

                var propertyCount = 1;

                var html =  '<div class="control-group">' +
                        '<label class="control-label">Additional Property ' + propertyCount + '</label>' +
                        '<div class="controls controls-row">' +
                        '<input type="text" name="key' + propertyCount + '" id="key' + propertyCount + '" value="' + key + '" placeholder="Property Name">' +
                        '<input type="text" name="value' + propertyCount + '" id="value' + propertyCount + '" value="' + value + '" placeholder="Property Value">' +
                        '</div>' +
                        '</div>';

                $('#edit-subject-group .additional-properties').append(html);

                propertyCount = propertyCount + 1;

            });

            $("#subject-group-modal-label").text("Edit Subject Group");
            $("#edit-subject-group").css('display', 'block');
            $("#delete-subject-group").css('display', 'none');
            $("#submit-group").text("Save");

            $("#subject-group-modal").modal("show");
        });

        $(document).on("click", ".deleteGroup", function (event)
        {
            var parentGroupID = ($(this).attr('id')).split('_')[1];

            deletions = selectedSubjectGroups['groups-' + parentGroupID];

            var message;

            if(deletions.length == 1)
            {
                message = "Are you sure you wish to delete this subject group?";
                $("#subject-group-modal-label").text("Delete Subject Group");
                $("#delete-group").text("Ok");
            }
            else if(deletions.length > 1)
            {
                message = "Are you sure you wish to delete these " + deletions.length + " subject groups?";
                $("#subject-group-modal-label").text("Delete Subject Groups");
                $("#delete-group").text("Delete Groups");
            }

            $("#delete-subject-group .modal-body .delete-message").text(message);

            $("#edit-subject-group").css('display', 'none');
            $("#delete-subject-group").css('display', 'block');

            $("#subject-group-modal").modal("show");
        });

        $(document).on("click", ".createSubject", function (event) {

            $("#parentGroupId").val(currentSubjectGroup);
            $("#subjectID").val('');

            $("#subject-modal-label").text("Create Subject");
            $("#edit-subject").css('display', 'block');
            $("#delete-subject").css('display', 'none');
            $("#submit-subject").text("Create");

            $("#subject-modal").modal("show");
        });

        $(document).on("click", ".editSubject", function (event)
        {
            var subjectID = ($(this).attr('id')).split('_')[1];
            var data = (selectedSubjects['subjects-' + subjectID][0]);

            $("#dbId").val(data.id);
            $("#parentGroupId").val(data.subjectGroupId);
            $("#subjectID").val(data.externalId);

            $('#edit-subject .additional-properties').html('');

            $.each(data.additionalProperties, function(key, value){

                var propertyCount = 1;

                var html =  '<div class="control-group">' +
                        '<label class="control-label">Additional Property ' + propertyCount + '</label>' +
                        '<div class="controls controls-row">' +
                        '<input type="text" name="key' + propertyCount + '" id="key' + propertyCount + '" value="' + key + '" placeholder="Property Name">' +
                        '<input type="text" name="value' + propertyCount + '" id="value' + propertyCount + '" value="' + value + '" placeholder="Property Value">' +
                        '</div>' +
                        '</div>';

                $('#edit-subject .additional-properties').append(html);

                propertyCount = propertyCount + 1;

            });

            $("#subject-modal-label").text("Edit Subject");
            $("#edit-subject").css('display', 'block');
            $("#delete-subject").css('display', 'none');
            $("#submit-subject").text("Save");

            $("#subject-modal").modal("show");
        });

        $(document).on("click", ".deleteSubject", function (event)
        {
            var subjectID = ($(this).attr('id')).split('_')[1];
            var data = (selectedSubjects['subjects-' + subjectID][0]);

            $("#parentGroupId").val(data.subjectGroupId);

            deletions = selectedSubjects['subjects-' + subjectID];

            var message;

            if(deletions.length == 1)
            {
                message = "<p>Are you sure you wish to delete this subject?</p>";
                $("#subject-modal-label").text("Delete Subject");
                $("#delete-subjects").text("OK");
            }
            else if(deletions.length > 1)
            {
                message = "<p>Are you sure you wish to delete these " + deletions.size + " subjects?</p>";
                $("#subject-modal-label").text("Delete Subjects");
                $("#delete-subjects").text("Delete Subjects");
            }

            $("#delete-subject .modal-body").html(message);

            $("#edit-subject").css('display', 'none');
            $("#delete-subject").css('display', 'block');

            $("#subject-modal").modal("show");
        });

        $(document).on('click', '.createDeployment', function() {

            $('#logger-deployment #groupId').val(currentSubjectGroup);

            $('#logger-deployment').modal('show');

        });

        $(document).on("click", ".editDeployment", function (event)
        {
            var deploymentID = ($(this).attr('id')).split('_')[1];
            currentDeployment = (selectedDeployments['deployments-' + deploymentID][0]);

            $('#edit-deployment .additional-properties').html('');

            $.each(currentDeployment.additionalProperties, function(key, value){

                var propertyCount = 1;

                var html =  '<div class="control-group">' +
                        '<label class="control-label">Additional Property ' + propertyCount + '</label>' +
                        '<div class="controls controls-row">' +
                        '<input type="text" name="key' + propertyCount + '" id="key' + propertyCount + '" value="' + key + '" placeholder="Property Name">' +
                        '<input type="text" name="value' + propertyCount + '" id="value' + propertyCount + '" value="' + value + '" placeholder="Property Value">' +
                        '</div>' +
                        '</div>';

                $('#edit-deployment .additional-properties').append(html);

                propertyCount = propertyCount + 1;

            });

            $("#deployment-modal-label").text("Edit Deployment");
            $("#edit-deployment").css('display', 'block');
            $("#remove-deployment").css('display', 'none');
            $("#submit-deployment").text("Save");

            $("#deployment-modal").modal("show");
        });

        $(document).on('click','.deleteDeployment', function(){

            var deploymentID = ($(this).attr('id')).split('_')[1];

            deletions = selectedDeployments['deployments-' + deploymentID];

            if(deletions.length == 1)
            {
                $("#deployment-modal-label").text("Undeploy Logger");
                $("#remove-deployment .modal-body").text("Are you sure you want to undeploy this logger?");
            }
            else if(deletions.length > 1)
            {
                $("#deployment-modal-label").text("Undeploy Loggers");
                $("#remove-deployment .modal-body").text("Are you sure you want to undeploy these " + undeployments.length +  " loggers?");
            }

            $("#edit-deployment").css('display', 'none');
            $("#remove-deployment").css('display', 'block');

            $("#deployment-modal").modal("show");

        });

        $(document).on('click', '.group-properties', function() {

            $('#properties-targetId').val($(this).data('id'));

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');
            var rowData = table.fnGetData(table.fnGetPosition($(this).closest("tr")[0]));

            $('#additional-properties .dl-horizontal').html('');
            $('#additional-properties .dl-horizontal').css('display', 'block');
            $('.no-properties').css('display', 'none');

            if(Object.keys(rowData.additionalProperties).length > 0) {
                $.each(rowData.additionalProperties, function(key, value){

                    $('#additional-properties .dl-horizontal').append('<dt>' + key + '</dt><dd>' + value + '</dd>');

                });
            }
            else {
                $('.no-properties').css('display', 'block');
                $('#additional-properties .dl-horizontal').css('display', 'none');
            }

            $('#additional-properties').modal('show');

        });

        /*
        Commented to prevent upload. May also need to remove Website/src/main/webapp/pages/studies/modals/group-data.jspf
        $(document).on('click', '.group-data', function() {

            buildFileUploader($(this).data('url'));

            $('#group-data-label').text('Group ' + $(this).data('id') + ' Data');

            $('#group-data').modal('show');

        });*/

        /*$(document).on('click', '.group-deployment', function() {

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');
            var rowData = table.fnGetData(table.fnGetPosition($(this).closest("tr")[0]));

            $('#logger-deployment #groupId').val(rowData.id);

            $('#logger-deployment').modal('show');

        });*/

        $(document).on('click', '.subject-properties', function() {

            $('#properties-targetId').val($(this).data('id'));

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');
            var rowData = table.fnGetData(table.fnGetPosition($(this).closest("tr")[0]));

            $('#additional-properties .dl-horizontal').html('');
            $('#additional-properties .dl-horizontal').css('display', 'block');
            $('.no-properties').css('display', 'none');

            if(Object.keys(rowData.additionalProperties).length > 0) {
                $.each(rowData.additionalProperties, function(key, value){

                    $('#additional-properties .dl-horizontal').append('<dt>' + key + '</dt><dd>' + value + '</dd>');

                });
            }
            else {
                $('.no-properties').css('display', 'block');
                $('#additional-properties .dl-horizontal').css('display', 'none');
            }

            $('#additional-properties').modal('show');

        });

        $(document).on('click', '.deployment-properties', function() {

            $('#properties-targetId').val($(this).data('id'));

            var table = $(this).closest('table').dataTable();
            var tableID = $(this).closest('table').attr('id');
            var rowData = table.fnGetData(table.fnGetPosition($(this).closest("tr")[0]));

            $('#additional-properties .dl-horizontal').html('');
            $('#additional-properties .dl-horizontal').css('display', 'block');
            $('.no-properties').css('display', 'none');

            if(Object.keys(rowData.additionalProperties).length > 0) {
                $.each(rowData.additionalProperties, function(key, value){

                    $('#additional-properties .dl-horizontal').append('<dt>' + key + '</dt><dd>' + value + '</dd>');

                });
            }
            else {
                $('.no-properties').css('display', 'block');
                $('#additional-properties .dl-horizontal').css('display', 'none');
            }

            $('#additional-properties').modal('show');

        });

        function buildSubjectGroupTable(groupID)
        {
            if($('#groups-' + groupID).hasClass('dataTable')){

                $('#groups-' + groupID).dataTable().fnReloadAjax("/website-api/rest/study/<%=request.getParameter("id")%>/phase/" + phaseId + "/group/" + groupID + "/groups");
            }

            $('#groups-' + groupID).dataTable( {
                "bDestroy": true,
                "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "oLanguage": {
                    "sSearch": ""
                },
                "bRetrieve": true,
                "oTableTools": {
                    "sSwfPath": "../../scripts/datatables-1.9.4/swf/copy_csv_xls_pdf.swf"
                },
                "sAjaxSource": "/website-api/rest/study/<%=request.getParameter("id")%>/phase/" + phaseId + "/group/" + groupID + "/groups",
                "sAjaxDataProp": "",
                "aaSorting": [[ 1, "asc" ]],
                "aoColumns": [
                    {
                        "mDataProp": null,
                        "sClass": "control center",
                        "sDefaultContent": '<i class="icon-plus"></i>',
                        "bSortable": false
                    },
                    {
                        "mData": "displayName",
                        "sClass": "group-selectable",
                        "sDefaultContent": ''
                    },
                    {
                        "mDataProp": null,
                        "bSortable": false,
                        "sClass": "group-selectable",
                        "sDefaultContent": '<span></span>',
                        "fnRender": function(oObj) {

                            var subjectCount = oObj.aData.subjects.length;

                            return '<span>' + subjectCount + '</span>';
                        }
                    },
                    {
                        "mDataProp": null,
                        "bSortable": false,
                        "sClass": "center",
                        "sDefaultContent": '<span class="details"><i class="icomoon-info2"></i></span>',
                        "fnRender": function(oObj) {

                            var groupId = oObj.aData.id;

                            return '<span data-id="' + groupId + '" class="group-properties"><i class="icomoon-info2"></i></span>';
                        }
                    },
                    {
                        "mDataProp": null,
                        "bSortable": false,
                        "sClass": "center",
                        "sDefaultContent": '<a href="data.jsp?id=#" ><i class="icomoon-folder-open"></i></a>',
                        "fnRender": function(oObj) {

                            var folderid = oObj.aData.dataFolderId;
                            var studyId = <%=id%>;

                            return '<a href="data.jsp?id=' + studyId + '&folderid=' + folderid + '" ><i class="icomoon-folder-open"></i></a>';
                        }
                    },
                    /*{
                        "mDataProp": null,
                        "bSortable": false,
                        "sClass": "center",
                        "sDefaultContent": '<span class="group-deployment"><i class="icomoon-meter"></i></span>',
                        "fnRender": function(oObj) {

                            return '<span class="group-deployment"><i class="icomoon-meter"></i></span>';
                        }
                    },
                    {
                        "mDataProp": null,
                        "bSortable": false,
                        "sClass": "center",
                        "sDefaultContent": '<span class="participant-data"><i class="icomoon-cloud-upload"></i></span>',
                        "fnRender": function(oObj) {

                            var studyId = '<%=request.getParameter("id")%>';
                            var groupId = oObj.aData.id;

                            var fullURL = '/website-api/rest/study/' + studyId + '/subjectgroup/' + groupId + '/data/';

                            return '<span data-id="' + groupId + '" data-url="' + fullURL + '" class="group-data"><i class="icomoon-cloud-upload"></i></span>';
                        }
                    }*/
                ],
                "fnInitComplete": function(oSettings, json) {

                    if(json[0].categoryName != null){
                        $("#groups-" + groupID + "-heading").html(json[0].categoryName);
                    }
                }
            });

            $('.dataTables_filter input').attr('placeholder', 'Search...');

            <%if(EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, study)){%>

                var editButtons =   '<button id="createSubjectGroup_' + groupID + '" data-group="' + groupID + '" class="createGroup btn">Add Group</button>' +
                    '<button id="editSubjectGroup_' + groupID + '" data-group="' + groupID + '" class="editGroup btn" disabled="true">Edit</button>' +
                    '<button id="deleteSubjectGroup_' + groupID + '" data-group="' + groupID + '" class="deleteGroup btn" disabled="true">Delete</button>';

            <%} else {%>

                var editButtons =   '<button disabled="disabled" id="createSubjectGroup_' + groupID + '" data-group="' + groupID + '" class="createGroup btn">Add Group</button>' +
                    '<button disabled="disabled" id="editSubjectGroup_' + groupID + '" data-group="' + groupID + '" class="editGroup btn" disabled="true">Edit</button>' +
                    '<button disabled="disabled" id="deleteSubjectGroup_' + groupID + '" data-group="' + groupID + '" class="deleteGroup btn" disabled="true">Delete</button>';

            <%}%>

            $("#groups-" + groupID + "_wrapper .btn-group").html(editButtons);
        }

        function buildSubjectTable(groupID)
        {
            $('#subjects-' + groupID).dataTable( {
                "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "oLanguage": {
                    "sSearch": ""
                },
                "bRetrieve": true,
                "oTableTools": {
                    "sSwfPath": "../../scripts/datatables-1.9.4/swf/copy_csv_xls_pdf.swf"
                },
                "sAjaxSource": "/website-api/rest/study/<%=request.getParameter("id")%>/group/" + groupID + "/subjects",
                "sAjaxDataProp": "",
                "aoColumns": [
                    {
                        "mData": "externalId",
                        "sClass": "subject-selectable"
                    },
                    {
                        "mDataProp": null,
                        "sClass": "center",
                        "sDefaultContent": '<span class="details"><i class="icomoon-info2"></i></span>',
                        "fnRender": function(oObj) {

                            var subjectId = oObj.aData.id;



                            return '<span data-id="' + subjectId + '" class="subject-properties"><i class="icomoon-info2"></i></span>';
                        }
                    }
                ]
            });

            $('.dataTables_filter input').attr('placeholder', 'Search...');

            <%if(EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, study)){%>

            var editButtons =   '<button id="createSubject_' + groupID + '" data-group="' + groupID + '" class="createSubject btn">Add Subject</button>' +
                    '<button id="editSubject_' + groupID + '" data-group="' + groupID + '" class="editSubject btn" disabled="true">Edit</button>' +
                    '<button id="deleteSubject_' + groupID + '" data-group="' + groupID + '" class="deleteSubject btn" disabled="true">Delete</button>';

            <%} else {%>

            var editButtons =   '<button disabled="disabled" id="createSubject_' + groupID + '" data-group="' + groupID + '" class="createSubject btn">Add Subject</button>' +
                    '<button disabled="disabled" id="editSubject_' + groupID + '" data-group="' + groupID + '" class="editSubject btn" disabled="true">Edit</button>' +
                    '<button disabled="disabled" id="deleteSubject_' + groupID + '" data-group="' + groupID + '" class="deleteSubject btn" disabled="true">Delete</button>';

            <%}%>

            $("#subjects-" + groupID + "_wrapper .btn-group").html(editButtons);
        }

        function buildDeploymentsTable(groupID)
        {
            $('#deployments-' + groupID).dataTable( {
                "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                "oLanguage": {
                    "sSearch": ""
                },
                "bRetrieve": true,
                "sAjaxSource": "/website-api/rest/logger/<%=study.getId()%>/" + groupID + "/deployments",
                "sAjaxDataProp": "",
                "aoColumns": [
                    {
                        "mData": "logger.serialNumber",
                        "sClass": "deployment-selectable",
                        "sDefaultContent": ''
                    },
                    {
                        "mDataProp": null,
                        "sClass": "deployment-selectable",
                        "sDefaultContent": '',
                        "fnRender": function(oObj) {
                            return oObj.aData.logger.loggerType.manufacturer + ' ' + oObj.aData.logger.loggerType.name;
                        }
                    },
                    {
                        "mData": "startDate",
                        "sClass": "deployment-selectable",
                        "sDefaultContent": '',
                        "fnRender": function(oObj) {
                            return moment(oObj.aData.startDate).format('DD/MM/YYYY');
                        }
                    },
                    {
                        "mData": "endDate",
                        "sClass": "deployment-selectable",
                        "sDefaultContent": '',
                        "fnRender": function(oObj) {
                            return moment(oObj.aData.endDate).format('DD/MM/YYYY');
                        }
                    },
                    {
                        "mDataProp": null,
                        "sClass": "center",
                        "sDefaultContent": '<span class="details"><i class="icomoon-info2"></i></span>',
                        "fnRender": function(oObj) {

                            var deploymentId = oObj.aData.id;

                            return '<span data-id="' + deploymentId + '" class="deployment-properties"><i class="icomoon-info2"></i></span>';
                        }
                    }
                ]
            });

            $('.dataTables_filter input').attr('placeholder', 'Search...');

            var editButtons =   '<button id="createDeployment_' + groupID + '" data-group="' + groupID + '" class="createDeployment btn">Deploy Logger</button>' +
                    '<button disabled="disabled" id="editDeployment_' + groupID + '" data-group="' + groupID + '" class="editDeployment btn" disabled="true">Edit</button>' +
                    '<button id="deleteDeployment_' + groupID + '" data-group="' + groupID + '" class="deleteDeployment btn" disabled="true">Undeploy</button>';

            $("#deployments-" + groupID + "_wrapper .btn-group").html(editButtons);
        }

        $('.dataTable').on('xhr.dt', function (e, settings, json, xhr) {
            if(json.length == 0){
                $('#' + settings.nTable.id + '_filter').css('display', 'none');
                $('#' + settings.nTable.id).css('display', 'none');
                $('#' + settings.nTable.id + '_info').css('display', 'none');
                $('#' + settings.nTable.id + '_wrapper .pagination').css('display', 'none');
            }
            else {
                $('#' + settings.nTable.id + '_filter').css('display', 'block');
                $('#' + settings.nTable.id).css('display', 'block');
                $('#' + settings.nTable.id + '_info').css('display', 'block');
                $('#' + settings.nTable.id + '_wrapper .pagination').css('display', 'block');
            }
        });

        $('.phase-tabs li.phase').on('click', function(){

            phaseId = $(this).data('phase');

            buildSubjectGroupTable(0);
        });
    });

</script>

<%@include file="modals/subject-group.jspf" %>
<%@include file="modals/create-phase.jspf" %>
<%@include file="modals/subject.jspf" %>
<%@include file="modals/additional-properties.jspf" %>
<%@include file="modals/logger-deployment.jspf" %>
<%@include file="modals/group-data.jspf" %>
<%@include file="modals/deployment.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="com.connexience.server.model.document.DocumentRecord" %>
<%@ page import="com.connexience.server.model.project.study.parser.ParsedStudy" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<%
    try
    {
        Integer id = Integer.parseInt(request.getParameter("id"));

        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, id);
        menu_study = study;

        ParsedStudy parsedStudy = null;
        String loggerIdColumn = "";
        String fileName = "";
        Boolean confirmProcess = false;
        DocumentRecord doc = new DocumentRecord();

        String contentType = request.getContentType();

        if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0))
        {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List items = upload.parseRequest(request);

            Iterator iter = items.iterator();

            ArrayList<String> fileNames = new ArrayList<String>();
            ArrayList<Long> sizes = new ArrayList<Long>();

            while (iter.hasNext())
            {
                FileItem item = (FileItem) iter.next();

                if (item.isFormField())
                {
                    if ("commit".equals(item.getFieldName()))
                    {
                        System.out.println("Confirmation Recieved: " + item.getString());

                        confirmProcess = Boolean.valueOf(item.getString());

                        System.out.println("Confirmation Flag: " + confirmProcess);
                    }
                }
                else
                {
                    if (!item.isFormField())
                    {
                        fileName = item.getName();

                        fileNames.add(fileName);
                        sizes.add(item.getSize());

                        String dataFolderId = study.getDataFolderId();

                        doc.setName(fileName);
                        doc.setContainerId(dataFolderId);
                        doc.setCreatorId(ticket.getUserId());
                        doc.setProjectId(ticket.getDefaultProjectId());
                        doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);

                        StorageUtils.upload(ticket, item.getInputStream(), item.getSize(), doc, "Saved From Study " + study.getName());
                    }
                }
            }

            if(confirmProcess)
            {
                System.out.println("Confirmation Recieved: " + confirmProcess.toString());

                parsedStudy = (ParsedStudy)session.getAttribute("parsedStudy");

                final List<String> feedback = EJBLocator.lookupStudyParserBean().persistParsedStudy(ticket, parsedStudy);

                for (final String s : feedback)
                {
                    System.out.println(s);
                }

//                response.sendRedirect("subjects.jsp?id=" + study.getId());
            }
            else
            {
                parsedStudy = EJBLocator.lookupStudyParserBean().parseStudyStructure(ticket, doc.getId(), study.getId(), loggerIdColumn);
                session.setAttribute("parsedStudy", parsedStudy);
            }

        }
%>
<div class="row-fluid">
    <div class="span9">
        <div class="row-fluid" id="structure-upload">
            <form id="upload-structure" action="upload-structure.jsp?id=<%=study.getId()%>" method="POST" enctype="multipart/form-data" class="clearfix">
                <input type="hidden" id="commit" name="commit" value="false">
                <% if(parsedStudy != null) { %>
                <input type="hidden" id="documentId" name="documentId" value="<%=doc.getId()%>">
                <% } %>
                <fieldset>
                    <legend>Upload Study Structure</legend>
                    <ol>
                        <li class="stage-1 <%if(parsedStudy == null){ %>current-stage<%}%>">
                            <div class="control-group">
                                <label class="control-label">Study Structure File</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new clearfix" data-provides="fileupload">
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select file</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input name="structureFile" id="structureFile" type="file" />
                                        </span>
                                        <div id="file-preview" class="stage-preview">
                                            <% if(parsedStudy != null) { %>
                                            <h6 class="fileupload-preview"><%=fileName%></h6>
                                            <% } else { %>

                                            <h6 class="fileupload-preview"></h6>
                                            <% } %>
                                        </div>
                                    </div>
                                    <span class="help-block">Choose the excel file containing the structure you wish to use for this study.</span>
                                </div>
                            </div>
                        </li>
                        <li class="stage-2">
                            <div class="control-group">
                                <div class=controls>
                                    <button type="submit" id="uploadStructure" class="btn" disabled="disabled">
                                        <i class="icomoon-checkbox-checked"></i>&nbsp;Validate
                                    </button>
                                </div>
                            </div>
                        </li>
                        <li class="stage-3 <%if(parsedStudy != null){ %>current-stage<%}%>">
                            <div class="control-group">
                                <div class=controls>
                                    <% if(parsedStudy != null) { %>
                                    <button id="confirmStructure" class="btn">
                                        <i class="icomoon-cloud-upload"></i>&nbsp;Upload
                                    </button>
                                    <% } else { %>
                                    <button id="confirmStructure" class="btn" disabled="disabled">
                                        <i class="icomoon-cloud-upload"></i>&nbsp;Upload
                                    </button>
                                    <% } %>
                                </div>
                            </div>
                        </li>
                    </ol>
                </fieldset>
            </form>
        </div>
        <hr>
        <div class="row-fluid" id="upload-results">
            <div class="span12">
                <% if(parsedStudy != null) { %>
                    <h5><%=parsedStudy.problems.size()%> Warnings</h5>
                    <ul id="parsingWarnings" class="unstyled">
                    <% for(String problem : parsedStudy.problems) { %>
                        <li class="alert">
                            <i class="icomoon-warning"></i>
                            <strong>Warning!</strong>&nbsp;<%= problem %>
                        </li>
                    <% } %>
                    </ul>
                <% } %>
            </div>
        </div>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(event){

        $(document).on('change', '#structureFile', function(){

            if($(this).val() != "")
            {
                $('#file-preview h2').css('display', 'block');
                $('#uploadStructure').removeAttr('disabled');
                $('.stage-1').removeClass('current-stage');
                $('.stage-2').addClass('current-stage');
            }
            else
            {
                $('#file-preview h2').css('display', 'none');
                $('#uploadStructure').attr('disabled', 'disabled');
                $('.stage-1').addClass('current-stage');
                $('.stage-2').removeClass('current-stage');
            }
        });

        $(document).on('click', '#confirmStructure', function(){

            $('#commit').val('true');

            $('#upload-structure').submit();

            $('#confirmStructure').attr('disabled', 'disabled');
            $('.stage-2').removeClass('current-stage');
            $('.stage-3').addClass('current-stage');
        });

        <% if(confirmProcess) {

         response.sendRedirect("../../pages/studies/subjects.jsp?id=" + id);

         } %>

    });

</script>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
    }
    catch(ConnexienceException e)
    {
    e.printStackTrace();
    response.sendRedirect("../../pages/error.jsp");
    return;
    }
%>
<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.connexience.server.model.folder.SearchFolder" %>
<%@ page import="com.connexience.server.model.folder.Folder" %>
<%@ page import="com.connexience.server.model.security.Permission" %>
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

<%
    Study study = null;
    String volString = "";

    try{

        Integer id = Integer.parseInt(request.getParameter("id"));
        study = EJBLocator.lookupStudyBean().getStudy(ticket, id);

        menu_study = study;

        String folderId = request.getParameter("folderid");

        if (folderId != null) {
            volString = "?vol0=" + folderId;
        } else {
            volString = "?vol0=" + study.getDataFolderId();
        }


%>

<script type="text/javascript" src="../../scripts/jquery-selectbox/jquery.selectBox.js"></script>
<link rel="stylesheet" type="text/css" href="../../scripts/jquery-selectbox/jquery.selectBox.css"/>

<%--QuickView--%>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">

<%--<script type="text/javascript" src="../../scripts/util/list.js"></script>
<script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/json/json2.js"></script>
<script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
<script type="text/javascript" src="../../scripts/jgrowl/jquery.jgrowl_minimized.js"></script>
<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>--%>

<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>

<!--%@ include file="../../pages/data/visualiser.jspf"%-->
<script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>

<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>

<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>

<!-- elFinder CSS (REQUIRED) -->
<%--<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder2/css/elfinder.full.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder2/css/theme.css">--%>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.esc.css">

<%--ACL--%>
<link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen"
    charset="utf-8"/>

<!-- elFinder JS (REQUIRED) -->
<%--<script type="text/javascript" src="../../scripts/elfinder2/js/elfinder.full.js"></script>--%>
<script type="text/javascript" src="../../scripts/elfinder-esc/js/jQuery.elFinder.js"></script>
<script type="text/javascript" src="../../scripts/elfinder-esc/js/jQuery.elFinder.esc.js"></script>

<%--ACL--%>
<script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">

<script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
<script type="text/javascript" src="../../scripts/datasets/views/MultiRowTableView.js"></script>
<script type="text/javascript" src="../../scripts/datasets/views/SingleRowJsonView.js"></script>
<script type="text/javascript" src="../../scripts/datasets/DatasetManager.js"></script>
<script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>

  <!-- elFinder initialization (REQUIRED) -->
  <script type="text/javascript" charset="utf-8">


    $().ready(function () {

      var myCommands = elFinder.prototype._options.commands;

      <%
         if(!EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){
      %>
      var disabled = ['escarchive', 'escrestore'];
      $.each(disabled, function (i, cmd) {
        (idx = $.inArray(cmd, myCommands)) !== -1 && myCommands.splice(idx, 1);
      });

      <%}%>
      var options = {
        commands: myCommands,
        url: '/beta/servlets/elfinder<%=volString%>',
        resizable: false,
        rememberLastDir: false,
        uiOptions : {
          contextmenu : {
              navbar : ['open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
              cwd    : ['reload', 'back', '|', 'escupload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'escinfo'],
              files  : ['getfile', '|','open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', '|', 'archive', 'extract', '|', 'info', 'escprovenance']
          },
          toolbar : [
              ['back', 'forward'],
              ['netmount'],
              ['mkdir', 'mkfile', 'escupload'],
              ['open',  'getfile'],
              ['escinfo', 'escprovenance'], //SMW add 'escprovenance'
              ['escquickview', 'acl', 'escarchive', 'escrestore'],
              ['copy', 'cut', 'paste'],
              ['rm'],
              ['rename', 'edit'],
              ['extract', 'archive'],
              ['view', 'sort']
          ]}
      };

      var elf = $('#elfinder').elfinder(options).elfinder('instance');

        elf.bind('dblclick', function(event){alert('hi!')});

      $(window).on("load", resizeFinder);
      $(window).on("resize", resizeFinder);

    });

    //function to resize the file browser when the window size changes
    function resizeFinder() {
      var h = $(window).height();
      if (h < 600) {
        h = 600;
      }

      var elfinderHeight = h - 300;
      var workzoneHeight = elfinderHeight - 54;
      var navbarHeight = elfinderHeight - 60;

      $(".elfinder").css('height', elfinderHeight);
      $(".elfinder-workzone").css('height', workzoneHeight);
      $(".elfinder-cwd-wrapper").css('height', workzoneHeight);
      $(".elfinder-navbar").css('height', navbarHeight);

    }
  </script>
    <style>
        .ui-resizable {
            position: absolute;
        }
    </style>

    <input type="hidden" id="currentUserId" value="<%=user.getId()%>">
    <input type="hidden" id="publicUserId" value="<%=organisation.getDefaultUserId()%>">
    <input type="hidden" id="enablePublic" value="<%=pageContext.getServletContext().getInitParameter("public_sharing_enabled")%>">
    <input type="hidden" id="usersGroupId" value="<%=organisation.getDefaultGroupId()%>">

    <div class="row-fluid">
        <div class="span9">
            <h2><%=study.getName() %></h2>
            <h4>Data</h4>
            <div id="elfinder"></div>
            <div id="quickviewdiv"></div>
        </div>
        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>
    </div>
    <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%}
catch(ConnexienceException e)
{
e.printStackTrace();
response.sendRedirect("../../pages/error.jsp");
return;
} %>
<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    Integer id = Integer.parseInt(request.getParameter("id"));

    try{

        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, id);
        menu_study = study;
%>
<!-- Wrapping div tag with ID to apply styling to this specific page using the ID -->
<div id="study-deployments">
    <!-- Title block above page content -->
    <!-- Bootstrap fluid row is a responsive full with div needed to contain 12 column grid system-->
    <div class="row-fluid">
        <!-- Main content column is 3/4 of width of row, or 9 grid column widths -->
        <div class="span9">
            <h2><%=study.getName() %></h2>
            <h4>Infield Users</h4>
            <table cellpadding="0" cellspacing="0" border="0" class="infield-users table table-bordered" id="infield-users">
                <thead>
                <tr>
                    <th>Username</th>
                    <%--<th width="30px"></th>
                    <th width="30px"></th>
                    <th width="30px"></th>--%>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
        <!-- Sidebar Menu contained in remaining 1/4 of space -->
        <div class="span3">
            <!-- Unstyled Bootstap class remove bullet points and sets basic list style. Sidebar-menu class sets eSC specific menu styles -->
            <%@include file="menu.jspf"%>
        </div>
    </div>
</div>
<script type="text/javascript">

    var deletions = [];
    var currentUploader;

    $(document).ready(function(){

        var uploadersTable;
        var selectedStudyUploaders = [];

        $('#infield-users').on('click', 'tr', function () {

            var rowData = uploadersTable.fnGetData(uploadersTable.fnGetPosition(this));
            var index = jQuery.inArray(rowData, selectedStudyUploaders);

            if ( index === -1 )
            {
                selectedStudyUploaders.push(rowData);
            }
            else
            {
                selectedStudyUploaders.splice(index, 1);
            }

            if(selectedStudyUploaders.length == 1)
            {
                $('#addUploader').prop('disabled', false);
                $('#removeInFieldUser').prop('disabled', false);
            }
            else if(selectedStudyUploaders.length > 1)
            {
                $('#editUploader').prop('disabled', true);
                $('#removeInFieldUser').prop('disabled', false);
            }
            else
            {
                $('#editUploader').prop('disabled', true);
                $('#removeInFieldUser').prop('disabled', true);
            }

            console.log(selectedStudyUploaders);

            $(this).toggleClass('active');
        });

        $(document).on("click", "#addInFieldUser", function (event) {

            $("#upload-infield-modal-label").text("Add Upload User");
            $("#edit-infield-user").css('display', 'block');
            $("#remove-infield-user").css('display', 'none');
            $("#add-infield-user").text("Add Upload User");

            $("#infield-user-modal").modal("show");
        });

        $(document).on("click", "#editInFieldUser", function (event)
        {

            $("#upload-infield-modal-label").text("Edit Upload User");
            $("#edit-infield-user").css('display', 'block');
            $("#remove-infield-user").css('display', 'none');
            $("#add-infield-user").text("Edit Upload User");

            $("#infield-user-modal").modal("show");
        });

        $(document).on("click", "#removeInFieldUser", function (event)
        {
            deletions = selectedStudyUploaders;

            var message;

            if(deletions.length == 1)
            {
                message = "<p>Are you sure you wish to remove this infield user from this study?</p>";
                $("#infield-user-modal-label").text("Remove Infield User");
                $("#remove-infield-users").text("Remove Infield User");
            }
            else if(deletions.length > 1)
            {
                message = "<p>Are you sure you wish to delete these " + deletions.length + " infield users from this study?</p>";
                $("#infield-user-modal-label").text("Remove Infield Users");
                $("#remove-infield-users").text("Remove Infield Users");
            }

            $("#remove-infield-user .modal-body").html(message);

            $("#edit-infield-user").css('display', 'none');
            $("#remove-infield-user").css('display', 'block');

            $("#infield-user-modal").modal("show");
        });

        uploadersTable = $('#infield-users').dataTable( {
            "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "oLanguage": {
                "sSearch": ""
            },
            "bRetrieve": true,
            "sAjaxSource": "/website-api/rest/study/" + <%=study.getId()%> + "/uploaders",
            "sAjaxDataProp": "",
            "aoColumns": [
                {
                    "mData": "username"
                }/*,
                {
                    "mDataProp": null,
                    "bSortable": false,
                    "sClass": "control center",
                    "sDefaultContent": '<i class="icon-envelope"></i>'
                },
                {
                    "mDataProp": null,
                    "bSortable": false,
                    "sClass": "control center",
                    "sDefaultContent": '<i class="icomoon-stack"></i>'
                },
                {
                    "mDataProp": null,
                    "bSortable": false,
                    "sClass": "control center",
                    "sDefaultContent": '<i class="icomoon-lock"></i>'
                }*/
            ]
        } );

        $('.dataTables_filter input').attr('placeholder', 'Search...');

        var editButtons =   '<button id="addInFieldUser" class="createInFieldUser btn">Add</button>' +
                //'<button id="editUploader" class="editUploader btn" disabled="true">Edit</button>' +
                '<button id="removeInFieldUser" class="deleteInFieldUser btn" disabled="true">Remove</button>';

        $("#infield-users_wrapper .btn-group").html(editButtons);

    });

</script>
<%@include file="modals/infield-user.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
            <%
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
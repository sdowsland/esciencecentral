<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@page import="com.connexience.server.model.scanner.RemoteFilesystemScanner"%>
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@ page import="com.connexience.server.model.folder.Folder"%>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>     
<script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
<script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
<script type="text/javascript" src="../../scripts/admin/RemoteFilesystemScannerAdmin.js"></script>

<%

    SimpleDateFormat DATEFORMAT = new SimpleDateFormat("dd/MM/yyyy");

    try{

        if(request.getParameter("id") == null)
        {
            response.sendRedirect("studies.jsp");
        }
        else
        {
            Integer id = Integer.parseInt(request.getParameter("id"));

            Study study = EJBLocator.lookupStudyBean().getStudy(ticket, id);
            menu_study = study;
            Collection<User> admins = EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, study.getAdminGroupId());
            Collection<User> members = EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, study.getMembersGroupId());

            editing = false;

            for(User admin : admins)
            {
                if(admin.getId().equals(ticket.getUserId()))
                {
                    String action = request.getParameter("action");

                    if(action!=null && action.equals("edit"))
                    {
                        editing = true;
                    }
                }
            }

%>
<div id="study">
    <div class="row-fluid">
        <div class="span9">
            <h2><%=study.getName() %></h2>
            <h4>Overview</h4>
            <div class="btn-group action-menu">
                <% if(!editing && EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, study)) { %>
                <a href="../../pages/studies/study.jsp?id=<%=study.getId()%>&action=edit" class="btn">Edit</a>
                <a href="#delete-study-dialog" data-toggle="modal" class="btn">Delete</a>
                <% } else if(!editing && !EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, study)) { %>
                <a href="#" class="btn" disabled="disabled">Edit</a>
                <a href="#" class="btn" disabled="disabled">Delete</a>
                <% } else if(study != null && EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, study)) { %>
                <a href="#" id="saveChanges" class="btn">Save</a>
                <a href="../../pages/studies/study.jsp?id=<%=id%>" class="btn">Cancel</a>
                <a href="#delete-study-dialog" data-toggle="modal" class="btn">Delete</a>
                <% } else if(EJBLocator.lookupProjectsBean().isProjectAdmin(ticket, study)) { %>
                <a href="#" id="saveChanges" class="btn">Save</a>
                <a href="../../pages/studies/studies.jsp" class="btn" disabled="disabled">Cancel</a>
                <a href="#delete-study-dialog" data-toggle="modal" class="btn" disabled="disabled">Delete</a>
                <% } %>
            </div>
            <% if(editing) { %>
            <form id="edit-study" class="form-inline">
                <div class="row-fluid">
                    <input type="hidden" id="studyCreator" value="<%=study.getOwnerId()%>"/>
                    <input type="hidden" id="studyDataFolderId" value="<%=study.getDataFolderId()%>"/>
                    <input type="hidden" id="studyWorkflowFolderId" value="<%=study.getWorkflowFolderId()%>"/>
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">Study Name</label>
                            <div class="controls">
                                <input type="text" id="studyName" name="studyName" placeholder="Study Name" value="<%=study.getName()%>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Identifier</label>
                            <div class="controls">
                                <input type="text" id="studyID" name="studyID" placeholder="Study ID" value="<%=study.getExternalId()%>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Start Date</label>
                            <div class="controls">
                                <div class="date input-append study-start">
                                    <input name="studyStart" id="studyStart" placeholder="Study Start Date" value="<%=DATEFORMAT.format(study.getStartDate())%>" type="text">
                                    <span class="add-on">
                                        <i class="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">End Date</label>
                            <div class="controls">
                                <div class="date input-append study-end">
                                    <input id="studyEnd" name="studyEnd" placeholder="Study End Date" value="<%=DATEFORMAT.format(study.getEndDate())%>" type="text">
                                    <span class="add-on">
                                        <i class="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <label class="checkbox">
                                    <% if(study.isPrivateProject()) {%>
                                    <input id="studyPrivate" name="studyPrivate" type="checkbox" checked="checked"> Private Study
                                    <% } else { %>
                                    <input id="studyPrivate" name="studyPrivate" type="checkbox"> Private Study
                                    <% } %>
                                </label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <label class="checkbox">
                                    <% if(study.isVisibleOnExternalSite()) {%>
                                    <input id="studyVisibleOnExternalSite" name="studyVisibleOnExternalSite" type="checkbox" checked="checked"> Visible on Upload Site
                                    <% } else { %>
                                    <input id="studyVisibleOnExternalSite" name="studyVisibleOnExternalSite" type="checkbox"> Visible on Upload Site
                                    <% } %>
                                </label>
                            </div>
                        </div>
                        <div class="additional-study-properties">
                        <%

                        Integer i = 1;

                        for(String key : study.getAdditionalProperties().keySet())
                        {

                            if(!key.contains("xslx")) { %>

                            <div class="control-group">
                                <label class="control-label">Additional Property</label>
                                <div class="controls">
                                    <input type="text" name="key<%=i%>" id="key<%=i%>" value="<%=key%>" placeholder="Property Name">
                                    <input type="text" name="value<%=i%>" id="value<%=i%>" value="<%=study.getAdditionalProperty(key)%>" placeholder="Property Value">
                                </div>
                            </div>

                        <%
                            }

                            i++;
                        }

                        %>
                        </div>
                        <span id="add-study-property" class="btn">Add Property</span>
                    </div>
                    <div class="span5">
                        <div class="control-group">
                            <label class="control-label">Description</label>
                            <div class="controls">
                                <%if(study.getDescription() != null && !study.getDescription().isEmpty() && study.getDescription() != "null")
                                { %>
                                <textarea id="studyDescription" name="studyDescription"><%=study.getDescription()%></textarea>
                                <% } else { %>
                                <textarea id="studyDescription" name="studyDescription" placeholder="Description"></textarea>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <h3>Administrators</h3>
                        <ul id="admins" class="inline">
                            <%
                                for(User admin : admins)
                                {
                                    if(admin.getId().equals(ticket.getUserId())){
                            %>

                            <li id="<%= admin.getId()%>">
                                <a class="profile-picture">
                                    <img src="../../servlets/image?soid=<%=admin.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                    <h6><%=admin.getDisplayName()%> </h6>
                                </a>
                            </li>

                            <% }
                               else { %>

                            <li id="<%= admin.getId()%>" class="remove-member">
                                <a class="profile-picture">
                                    <img src="../../servlets/image?soid=<%=admin.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                    <span class="icomoon-close"></span>
                                    <h6><%=admin.getDisplayName()%> </h6>
                                </a>
                            </li>

                            <% } } %>
                            <li class="add-person">
                                <a id="add-admin" class="profile-picture" href="#user-finder">
                                    <img src="../../servlets/image?soid=1&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                    <h6>ADD</h6>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <h3>Members</h3>
                        <ul id="members" class="inline">
                            <%
                            for(User member : members)
                            { %>
                                <li id="<%= member.getId()%>" class="remove-member">
                                    <a class="profile-picture">
                                        <img src="../../servlets/image?soid=<%=member.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                        <span class="icomoon-close"></span>
                                        <h6><%=member.getDisplayName()%> </h6>
                                    </a>
                                </li>

                            <% }%>

                            <li class="add-person">
                                <a id="add-member" class="profile-picture" href="#user-finder">
                                    <img src="../../servlets/image?soid=1&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                    <h6>ADD</h6>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <h3>External Data Scanner</h3>

                        <div class="btn-group">
                            <%if(study.getRemoteScannerId()!=null){%>
                            <a class="btn" onclick="editScanner('<%=study.getRemoteScannerId()%>');">Edit</a>
                            <a class="btn" onclick="scanNow('<%=study.getRemoteScannerId()%>');">Scan Now</a>
                            <a class="btn" onclick="showFiles('<%=study.getRemoteScannerId()%>');">Show Files</a>
                            <a class="btn" onclick="createScanner();">Replace</a>
                            <a class="btn" onclick="deleteScanner('<%=study.getRemoteScannerId()%>');">Delete</a>
                            <a class="btn" onclick="setFolder('<%=study.getRemoteScannerId()%>');">Set Folder</a>
                            
                            <%
                                try {
                                    RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, study.getRemoteScannerId());
                                    if(scanner!=null){%>

                                        <%if(scanner.isEnabled()){%>
                                            <a class="btn" onclick="disableScanner('<%=study.getRemoteScannerId()%>');">Disable</a>
                                        <%} else {%>
                                            <a class="btn" onclick="enableScanner('<%=study.getRemoteScannerId()%>');">Enable</a>
                                        <%}
                                    }
                                  } catch (Exception e){}
                            %>

                            
                            <%} else {%>
                            <a class="btn" onclick="createScanner();">Create</a>
                            <%}%>
                        </div>
                        <%
                            try {
                                RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, study.getRemoteScannerId());
                                if(scanner!=null){%>

                        <%if(scanner.getTargetFolderId()!=null){
                            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, scanner.getTargetFolderId());
                            if(f!=null){
                        %>
                        <h5>Data Upload Path</h5>
                        <p><%=f.getName()%></p>
                        <%} else {%>
                        <h5> No upload folder defined</h5>
                        <%}}%>
                        <%}%>

                        <%} catch (Exception e){%>
                        <h5>No scanner found</h5>
                        <%}%>


                    </div>
                </div>
            </form>
            <% } else { %>
            <div class="row-fluid">
                <div class="span6">
                    <dl class="dl-horizontal">
                        <dt>Study Code</dt>
                        <dd><%= (study.getExternalId() != null) ? study.getExternalId() : "" %></dd>
                        <dt>Start Date</dt>
                        <dd><%= (study.getStartDate() != null) ? DATEFORMAT.format(study.getStartDate()) : "" %></dd>
                        <dt>End Date</dt>
                        <dd><%= (study.getEndDate() != null) ? DATEFORMAT.format(study.getEndDate()) : "" %></dd>
                        <dt>Privacy</dt>
                        <dd><%= (study.isPrivateProject()) ? "Private" : "Public" %></dd>
                        <dt>External Site</dt>
                        <dd><%= (study.isVisibleOnExternalSite()) ? "Visible" : "Invisible" %></dd>
                    <%  for(String key : study.getAdditionalProperties().keySet())
                        {
                            if(!key.contains("xslx")) {
                    %>

                        <dt><%=key%></dt>
                        <dd><%=study.getAdditionalProperty(key)%></dd>

                      <%} }%>
                    </dl>
                    <hr>
                    <h4>Spreadsheet Uploads</h4>
                    <ul id="uploads" class="unstyled">
                        <li>
                            <% if(study.getAdditionalProperties().containsKey("xslx.structure")){ %>
                                <i class="icomoon-checkmark"></i><span>Structure Uploaded</span>
                                <ul class="unstyled">
                                    <li><small><strong>File Used </strong><%=study.getAdditionalProperty("xslx.structure.file")%></small></li>
                                    <li><small><strong>Uploaded </strong><%=study.getAdditionalProperty("xslx.structure.time")%></small></li>
                                </ul>
                            <% } else { %>
                                <i class="icomoon-close"></i><span>Structure Uploaded</span>
                            <% } %>
                        </li>
                        <li>
                            <% if(study.getAdditionalProperties().containsKey("xslx.loggerprops")){ %>
                            <i class="icomoon-checkmark"></i><span>Logger Properties Uploaded</span>
                            <ul class="unstyled">
                                <li><small><strong>File Used </strong><%=study.getAdditionalProperty("xslx.loggerprops.file")%></small></li>
                                <li><small><strong>Uploaded </strong><%=study.getAdditionalProperty("xslx.loggerprops.time")%></small></li>
                            </ul>
                            <% } else { %>
                            <i class="icomoon-close"></i><span>Logger Properties Uploaded</span>
                            <% } %>
                        </li>
                        <li>
                            <% if(study.getAdditionalProperties().containsKey("xslx.subjectprops")){ %>
                            <i class="icomoon-checkmark"></i><span>Subject Properties Uploaded</span>
                            <ul class="unstyled">
                                <li><small><strong>File Used </strong><%=study.getAdditionalProperty("xslx.subjectprops.file")%></small></li>
                                <li><small><strong>Uploaded </strong><%=study.getAdditionalProperty("xslx.subjectprops.time")%></small></li>
                            </ul>
                            <% } else { %>
                            <i class="icomoon-close"></i><span>Subject Properties Uploaded</span>
                            <% } %>
                        </li>
                    </ul>
                </div>
                <div class="span6">
                    <p id="study-description">

                        <%if(study.getDescription() != null && !study.getDescription().isEmpty() && study.getDescription() != "null")
                        {
                            out.println(study.getDescription());
                        }
                        else
                        {
                            out.println("Currently no description");
                        }%>

                    </p>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <h3>Administrators</h3>
                    <ul id="admins" class="inline">
                    <%
                    for(User admin : admins)
                    { %>

                        <li id="<%= admin.getId()%>">
                            <a class="profile-picture" href="../../pages/profile/profile.jsp?id=<%=admin.getId()%>">
                                <img src="../../servlets/image?soid=<%=admin.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                <h6><%=admin.getDisplayName()%> </h6>
                            </a>
                        </li>

                 <% } %>
                    </ul>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <h3>Members</h3>
                    <ul id="members" class="inline">
                <%
                    for(User member : members)
                    {%>

                        <li id="<%= member.getId()%>">
                            <a class="profile-picture" href="../../pages/profile/profile.jsp?id=<%=member.getId()%>">
                                <img src="../../servlets/image?soid=<%=member.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                                <h6><%=member.getDisplayName()%> </h6>
                            </a>
                        </li>

                    <% }%>
                    </ul>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <h3>External Data Scanner</h3>
                    <%
                        try {
                            RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(ticket, study.getRemoteScannerId());
                            if(scanner!=null){%>
                    <%if(scanner.getTargetFolderId()!=null){
                        Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, scanner.getTargetFolderId());
                        if(f!=null){
                    %>
                    <h5>Data Upload Path</h5>
                    <p><%=f.getName()%></p>
                    <%} else {%>
                    <h5> No upload folder defined</h5>
                    <%}}%>
                    <%}%>

                    <%} catch (Exception e){%>
                    <h5>No scanner found</h5>
                    <%}%>


                </div>
            </div>
            <% } %>
        </div>
        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>
    </div>
</div>
<div id="delete-study-dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="delete-study-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="delete-study-label">Delete Study</h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you wish to delete this Study?</p>
        <label class="checkbox">
            <input id="removeData" type="checkbox"> Remove all study data
        </label>

    </div>
    <div class="modal-footer">
        <button type="submit" id="delete-study" class="btn">Confirm</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
    <div class="dialog" id="confirmdialog"></div>
    <div class="dialog" id="filechooser"></div>
    <div class="dialog" id="files"></div>
    <div class="dialog" id="choosedialog"></div>
    <div class="dialog" id="editdialog"></div>    
</div>
<script type="text/javascript">
    var manager = new RemoteFilesystemScannerAdmin(),
        chooser = new FileChooser(),
        newAdmins = [],
        newMembers = [],
        removedAdmins = [],
        removedMembers = [];

    function setFolder(id){
        chooser.showFolder('<%=study.getDataFolderId()%>', false, false);
        chooser.okCallback = function(c){
            if(c.selectedFolderId){
                manager.setScannerFolder(id, c.selectedFolderId, function(){
                   location.reload(); 
                });
            }
        };
    }
    
    function createScanner(){
        var cb = function(){
            location.reload();
        };
        manager.createScannerUsingChooser(cb, <%=id%>);        
    }
    
    function scanNow(id){
        var cb = function(){
            $.jGrowl("Scanner rescan started");
        };
        manager.executeScanner(id, cb);        
    }
    
    function showFiles(id){
        manager.showScannerFiles(id);
    }
            
    function editScanner(id){
        var cb = function(){
            
        };
        manager.editScanner(id, cb);        
    }
    
    function deleteScanner(id){
        var cb = function(){
            location.reload();
        };
        manager.deleteScanner(id, cb, <%=id%>);
    }
    
    function enableScanner(id){
        var cb = function(){
            location.reload();
        };
        manager.enableScanner(id, cb);        
    }
    
    function disableScanner(id){
        var cb = function(){
            location.reload();
        };
        manager.disableScanner(id, cb);
    }

    function saveStudy(){

    }
    
    $(document).ready(function(){
        
        chooser.init("filechooser");
        manager.initChooserDialog("choosedialog");
        manager.initEditDialog("editdialog");
        manager.initFilesDialog("files");        
        
        var form = $("#edit-study");
        form.validate({
            rules: {
                studyName: {required: true, maxlength: 255, uniqueStudyName: true},
                studyID: {required: true, maxlength: 255, uniqueStudyID: true},
                studyStart: {required: true, britishDate: true},
                studyEnd: {required: true, britishDate: true},
                studyDescription: {maxlength: 255}
            }
        });

        $('.study-start').datepicker({
            format: 'dd/mm/yyyy',
            weekStart: 1,
            defaultDate: new Date(),
            pickTime: false,
            autoclose: true,
            todayBtn: "linked",
            todayHighlight: true,
            clearBtn: true
        });

        $('.study-end').datepicker({
            format: 'dd/mm/yyyy',
            weekStart: 1,
            defaultDate: new Date(),
            pickTime: false,
            autoclose: true,
            todayBtn: "linked",
            todayHighlight: false,
            clearBtn: true
        });

        $(document).on('click', '#add-study-property', function(){

            //Add 1 to avoid 0 start value
            var propertyCount = $('.additional-study-properties .control-group').size() + 1;

            console.log(propertyCount);

            var html = '<div class="control-group">' +
                    '<label class="control-label">Additional Property ' + propertyCount + '</label>' +
                    '<div class="controls">' +
                    '<input type="text" name="key' + propertyCount + '" id="key' + propertyCount + '" placeholder="Property Name">' +
                    '<input type="text" name="value' + propertyCount + '" id="value' + propertyCount + '" placeholder="Property Value">' +
                    '</div>' +
                    '</div>';

            $('.additional-study-properties').append(html);

        });

        $("#add-admin").on("click", function(){

            $("#user-finder #group").text("admin");

            $("#user-finder").modal("show");

        });

        $("#add-member").on("click", function(){

            $("#user-finder #group").text("member");

            $("#user-finder").modal("show");

        });

        $('.remove-member').on('click', function(){

            console.log($(this).attr('id'));
            removedMembers.push($(this).attr('id'));

            $(this).remove();
        });

        $("#saveChanges").on("click", function(event){

            event.preventDefault();

            if(form.valid())
            {
                var study = new Object();
                study.id = <%=id%>;
                study.ownerId = $("#studyCreator").val();
                study.name = $("#studyName").val();
                study.externalId = $("#studyID").val();
                study.startDate = moment($("#studyStart").val(), 'DD/MM/YYYY').format();
                study.endDate = moment($("#studyEnd").val(), 'DD/MM/YYYY').format();
                study.privateProject = $("#studyPrivate").is(":checked");
                study.visibleOnExternalSite = $("#studyVisibleOnExternalSite").is(":checked");
                study.description = $("#studyDescription").val();
                study.adminGroupId = <%=study.getAdminGroupId()%>;
                study.membersGroupId = <%=study.getMembersGroupId()%>;
                study.dataFolderId = <%=study.getDataFolderId()%>;
                study.remoteScannerId = <%=study.getRemoteScannerId()%>;
                study.workflowFolderId = <%=study.getWorkflowFolderId()%>;
                study.additionalProperties = {};

                <%

                    for(String key : study.getAdditionalProperties().keySet())
                    {
                        if(key.contains("xslx")) { %>
                        study.additionalProperties["<%=key%>"] = "<%=study.getAdditionalProperty(key)%>";
                       <% }
                    }

                %>

                $.each($('.additional-study-properties .controls'), function(key, input){

                    var propKey = $(this).children('input').eq(0).val();
                    var propVal = $(this).children('input').eq(1).val();

                    console.log(propKey + " - " + propVal);

                    study.additionalProperties[propKey] = propVal;

                    console.log(study.additionalProperties);

                });

                console.log(study);
                console.log(newAdmins);
                console.log(newMembers);
                console.log(removedAdmins);
                console.log(removedMembers);

                var encodedStudy = JSON.stringify(study);

                $.when(

                $.ajax({
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: '/website-api/rest/study/save/',
                    data: encodedStudy,
                    success: function (study) {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    }
                }),

                $.each(newAdmins, function(key, userId){
                    $.ajax({
                        type: 'PUT',
                        url: '/website-api/rest/groups/' + study.adminGroupId + '/' + userId
                    });
                }),

                $.each(newMembers, function(key, userId){
                    $.ajax({
                        type: 'PUT',
                        url: '/website-api/rest/groups/' + study.membersGroupId + '/' + userId
                    });
                }),

                $.each(removedAdmins, function(key, userId){
                    $.ajax({
                        type: 'DELETE',
                        url: '/website-api/rest/groups/' + study.adminGroupId + '/' + userId
                    });
                }),

                $.each(removedMembers, function(key, userId){
                    $.ajax({
                        type: 'DELETE',
                        url: '/website-api/rest/groups/' + study.membersGroupId + '/' + userId
                    });
                })

            ).then(function(){
                window.location.replace("study.jsp?id=" + study.id);
            });

            }
            else
            {
                if($('.validation-message').length == 0)
                {
                    console.log('Building Message...')

                    var message = "Correct these errors to save changes this study.";
                    var type = "error";
                    var target = $("#study h4").first();
                    var cssClass = "validation-message";

                    flashMessage(message, type, target, cssClass)
                }
            }
        });

        <% if(study != null) { %>

        $(document).on('click', '#delete-study', function(){

            var removeData = $('#removeData').is(':checked');

            $.ajax({
                type: 'POST',
                url: '/website-api/rest/study/<%=study.getId()%>/remove',
                data: 'removeData=' + removeData,
                success: function () {

                    console.log("success!");

                    document.location = "../../pages/studies/studies.jsp";

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });

        });

        <% } %>

    });

</script>
<%@include file="modals/create-study-spreadsheet.jspf" %>
<%@include file="../people/user-finder.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
        }
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@page import="com.connexience.server.ejb.util.EJBLocator" %>
<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@ page import="sun.util.calendar.BaseCalendar" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
<script type="text/javascript" src="../../scripts/admin/RemoteFilesystemScannerAdmin.js"></script>
<%
    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

    try {
        List<Study> studies;
        Boolean showInactive = false;
        menu_study = null;

        String searchText = "";

        String queryString = request.getQueryString() != null ? request.getQueryString() : "";

        String active = request.getParameter("inactive");
        String sort = (request.getParameter("sort") == null || request.getParameter("sort").isEmpty()) ? "NAME" : request.getParameter("sort");
        String direction = (request.getParameter("direction") == null || request.getParameter("direction").isEmpty()) ? "ASC" : request.getParameter("direction");

        if (active != null && active.equals("true")) {
            showInactive = true;
        }

        if (request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()) {
            List<Project> projectList = EJBLocator.lookupProjectsBean().searchProjects(ticket, request.getParameter("searchtext").trim());

            studies = new ArrayList<Study>();

            for (Project p : projectList) {
                studies.add(EJBLocator.lookupStudyBean().getStudy(ticket, p.getId()));
            }

            searchText = request.getParameter("searchtext");
        } else if (request.getParameter("browse") != null) {
            studies = EJBLocator.lookupStudyBean().getPublicStudies(ticket, START, PAGE_SIZE, sort, direction);
            NUM_OBJECTS = EJBLocator.lookupStudyBean().getPublicStudyCount(ticket);
        } else {
            if (showInactive) {
                studies = EJBLocator.lookupStudyBean().getMemberStudies(ticket, START, PAGE_SIZE, sort, direction);
                NUM_OBJECTS = EJBLocator.lookupStudyBean().getMemberStudyCount(ticket);
            } else {
                studies = EJBLocator.lookupStudyBean().getMemberActiveStudies(ticket, START, PAGE_SIZE, sort, direction);
                NUM_OBJECTS = EJBLocator.lookupStudyBean().getMemberActiveStudyCount(ticket);
            }
        }
%>

<!-- Caption Line -->

<%
    if (request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()) {
%>
<h2>Search Studies</h2>
<%
} else if (request.getParameter("browse") != null) {
%>
<h2>All Studies</h2>
<%
} else {
%>
<h2>My Studies</h2>
<%
    }
%>

<%-- An example List of items--%>
<div class="row-fluid">
    <div class="span9">
        <form action="studies.jsp">
            <div class="input-append">
                <input id="searchtext" class="studysearchtext" name="searchtext" type="text" placeholder="Search"
                       value="<%=searchText%>"/>
                <% if (showInactive) { %>
                <input type="hidden" name="inactive" value="true"/>
                <% } else { %>
                <input type="hidden" name="inactive" value="false"/>
                <% } %>
                <div name="searchbutton" id="searchbutton" class="btn-group" type="submit">
                    <button title="you little scallywag" id="clear-search" class="btn">
                        <i class="icon-remove-circle"></i>Clear
                    </button>
                    <button class="btn">
                        <i class="icon-search"></i>Search
                    </button>
                </div>
            </div>
        </form>
        <hr>
        <div class="btn-group" style="margin-bottom:20px;">
            <% if (showInactive) { %>
            <a class="btn btn-info"
               href="<%="../../pages/studies/studies.jsp?" + queryString.replace("&inactive=true", "") + "&inactive=false"%>">
                <i class="icon-check icon-white"></i>&nbsp;Include Inactive
            </a>
            <% } else { %>
            <a class="btn"
               href="<%="../../pages/studies/studies.jsp?" + queryString.replace("&inactive=false", "") + "&inactive=true"%>">
                <i class="icon-check"></i>&nbsp;Include Inactive
            </a>
            <% } %>
            <a title="Sort By Name" class="btn <% if(sort == null || !sort.equals("date")){%>btn-info<%}%>"
               href="<%="../../pages/studies/studies.jsp?" + queryString.replace("&sort=date", "") + "&sort=name"%>">
                Name
            </a>
            <a title="Sort By Date" class="btn <% if(sort != null && sort.equals("date")){%>btn-info<%}%>"
               href="<%="../../pages/studies/studies.jsp?" + queryString.replace("&sort=name", "") + "&sort=date"%>">
                Date
            </a>
            <a title="Sort Ascending" class="btn <% if(direction == null || !direction.equals("desc")){%>btn-info<%}%>"
               href="<%="../../pages/studies/studies.jsp?" + queryString.replace("&direction=desc", "") + "&direction=asc"%>">
                <i class="icon-arrow-up"></i>
            </a>
            <a title="Sort Descending" class="btn <% if(direction != null && direction.equals("desc")){%>btn-info<%}%>"
               href="<%="../../pages/studies/studies.jsp?" + queryString.replace("&direction=asc", "") + "&direction=desc"%>">
                <i class="icon-arrow-down"></i>
            </a>
        </div>
        <%
            if (request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()) {
        %>
        <h5>Searching for: "<%=request.getParameter("searchtext")%>"</h5>
        <%
            }

            if (studies.size() > 0) {%>
        <ul id="studies" class="unstyled">

            <%
                for (Study study : studies) {

                    User owner = EJBLocator.lookupUserDirectoryBean().getUser(ticket, study.getOwnerId());

                    if (showInactive || !study.getEndDate().before(new Date())) {
            %>

            <li class="clearfix">
                <a href="../../pages/studies/study.jsp?id=<%=study.getId()%>">
                    <div class="span5 study-overview">

                        <h3><%=study.getName()%>&nbsp;

                            <% if (study.getEndDate().before(new Date()) || study.getStartDate().after(new Date())) { %>

                            <span class="label">Inactive</span>&nbsp;

                            <% } else { %>

                            <span class="label label-success">Active</span>&nbsp;

                            <% } %>

                            <% if (study.isVisibleOnExternalSite()) { %>

                            <span class="label label-success">Visible</span>

                            <% } else { %>

                            <span class="label">Invisible</span>

                            <% } %>

                        </h3>

                        <h5><i class="icomoon-calendar"></i>&nbsp;<%=formatter.format(study.getStartDate())%>
                            - <%=formatter.format(study.getEndDate())%>
                        </h5>
                        <h6 class="owner"><i class="icomoon-user"></i>&nbsp;<%=owner.getDisplayName()%>
                        </h6>
                    </div>
                    <div class="span7 study-description">
                        <p>
                            <%
                                if (study.getDescription() != null && !study.getDescription().isEmpty() && study.getDescription() != "null") {
                                    out.print(study.getDescription());
                                } else {
                                    out.print("No description available");
                                }

                            %>

                        </p>
                    </div>

                </a>
            </li>

            <%
                    }
                }
            %>
            <%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>

        </ul>
        <%} else {%>
        <p>You have no active studies.</p>
        <%}%>
    </div>


    <!-- Right Hand Side -->
    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <li>
                <h4><a href="#create-study" data-toggle="modal"><i class="icomoon-plus"></i>Create Study</a></h4>
            </li>
            <li>
                <h4><a href="studies.jsp"><i class="icomoon-briefcase"></i>My Studies</a></h4>
            </li>
            <li>
                <h4><a href="studies.jsp?browse=true"><i class="icomoon-search"></i>Search Studies</a></h4>
            </li>
            <li>
                <h4><a href="studies.jsp?browse=true"><i class="icomoon-stack"></i>Browse All Studies</a></h4>
            </li>
            <li>
                <h4><a href="uploadusers.jsp"><i class="icomoon-users"></i>In-Field Users</a></h4>
            </li>
            <li class="divider"></li>
            <%
                if (EJBLocator.lookupPreferencesBean().booleanValue("StudyManagement", "EnableSpreadsheetDownload", false)) {
            %>
            <li>
                <h4><a href="/exampleSpreadsheet/example.zip"><i class="icomoon-file-excel"></i>Download Example
                    Spreadsheet</a></h4>
            </li>
            <%
                }
            %>

        </ul>

        <%@include file="modals/create-study.jspf" %>
    </div>

</div>
<script type="text/javascript">

    $(document).ready(function () {

        var substringMatcher = function (studies) {

            return function findMatches(q, cb) {
                var matches, substrRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(studies, function (i, study) {
                    if (substrRegex.test(study.name)) {
                        // the typeahead jQuery plugin expects suggestions to a
                        // JavaScript object, refer to typeahead docs for more info
                        matches.push({id: study.id, name: study.name, externalId: study.externalId});
                    }
                });

                cb(matches);
            };
        };

        $('#searchtext').bind('typeahead:selected', function (obj, datum, name) {
            var targetUrl = "http://" + window.location.host + "/beta/pages/studies/study.jsp?id=" + datum.id;
            window.location.assign(targetUrl);
        });

        var studies = [
            <% for(Study study : studies){ %>

            {
                name: '<%=study.getName()%>',
                externalId: '<%=study.getExternalId()%>'
            },

            <% } %>
        ];

        $('#searchtext').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'studies',
                    displayKey: 'name',
                    //source: studies.ttAdapter(),
                    source: substringMatcher(studies),
                    templates: {
                        empty: [
                            '<div class="empty-message">',
                            'unable to find any studies that match the current query',
                            '</div>'
                        ].join('\n'),
                        suggestion: Handlebars.compile('<p><strong>{{name}}</strong>&nbsp;({{externalId}})</p>')
                    }
                });

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                if (decodeURIComponent(pair[0]) == variable) {
                    return decodeURIComponent(pair[1]);
                }
            }
            console.log('Query variable %s not found', variable);
        }

        $(document).on('click', '#clear-search', function (event) {

            event.preventDefault();

            $('#searchtext').val('');
        });
    });

</script>

<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        //response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

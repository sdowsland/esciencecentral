<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.web.util.SearchUtil" %>
<%@ page import="com.connexience.server.web.util.SearchResult" %>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    String q = request.getParameter("q");
    String x = request.getParameter("x");
    String t = request.getParameter("t");

    //if the query is null, redirect them to the index

    if (t != null)
    {
      response.sendRedirect("../secure/tagsearch.jsp?t=" + t);
    }
    else if (x != null)
    {
      response.sendRedirect("../secure/xpathsearch.jsp?x=" + x);
    }

    if (q == null)
    {
      q = "";
    }
    Vector<String> advOptions = new Vector<String>();

    //get the advanced search parameters
    if (request.getParameter("n") != null)
    {
      advOptions.add(request.getParameter("n"));
    }
    if (request.getParameter("u") != null)
    {
      advOptions.add(request.getParameter("u"));
    }
    if (request.getParameter("b") != null)
    {
      advOptions.add(request.getParameter("b"));
    }
    if (request.getParameter("tg") != null)
    {
      advOptions.add(request.getParameter("tg"));
    }
    if (request.getParameter("m") != null)
    {
      advOptions.add(request.getParameter("m"));
    }
    if (request.getParameter("g") != null)
    {
      advOptions.add(request.getParameter("g"));
    }
    if (request.getParameter("a") != null)
    {
      advOptions.add(request.getParameter("a"));
    }
    if (request.getParameter("w") != null)
    {
      advOptions.add(request.getParameter("w"));
    }
    if (request.getParameter("d") != null)
    {
      advOptions.add(request.getParameter("d"));
    }
    if (request.getParameter("s") != null)
    {
      advOptions.add(request.getParameter("s"));
    }
    if (request.getParameter("f") != null)
    {
      advOptions.add(request.getParameter("f"));
    }
    if (request.getParameter("myFiles") != null)
    {
      advOptions.add(request.getParameter("myFiles"));
    }
    if (request.getParameter("othersFiles") != null)
    {
      advOptions.add(request.getParameter("othersFiles"));
    }


    //get the search results
    List resultObjects = null;
    List<SearchResult> results = new ArrayList<SearchResult>();
    try
    {
      if (!q.equals(""))
      {

        //	http://localhost:8080/InkSpot/jsp/index.jsp
        String thisURL = "http://" + request.getServerName() + ":" + request.getServerPort() + getServletConfig().getServletContext() + "/secure/search.jsp";

        resultObjects = EJBLocator.lookupSearchBean().freeTextSearch(ticket, q, advOptions, START, PAGE_SIZE);
        NUM_OBJECTS = EJBLocator.lookupSearchBean().countFreeTextSearch(ticket, q, advOptions);


        results = SearchUtil.formatSearchResults(ticket, resultObjects);
      }
    }
    catch (ConnexienceException e)
    {
      e.printStackTrace();
      resultObjects = new ArrayList();
      NUM_OBJECTS = 0;
    }


    long numObjOnThisPage = 10;
    long endOfResults = 0;

    if (NUM_OBJECTS < PAGE_SIZE)
    {
      numObjOnThisPage = NUM_OBJECTS;
      endOfResults = NUM_OBJECTS;
    }
    else
    {
      endOfResults = START + PAGE_SIZE;
    }

    if (NUM_OBJECTS == 0)
    {
      START = -1;
    }

  %>


<h2>
    <%
      if (q.equals(""))
      {
        out.print("Search");
      }
      else
      {
        out.print("Results " + (START + 1) + " to " + endOfResults + " of about " + NUM_OBJECTS + " for <span style=\"color:#3399FF\">" + q + "</span>");
      }
    %></h2>


<div class="row-fluid">
    <div class="span9">
        <form action="javascript:" class="searchForm">
            <div class="control-group">
                <div class="controls">
                    <div class="input-append">
                        <input id="searchBox" name="q" value="<%=q%>" type="text">
                        <button class="btn" type="submit" id="submitSearch"><i class="icomoon-search"></i></button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="advSearch" value="false" id="advSearch">
        </form>

        <%
        if (publicUser && !q.equals(""))
        {
        %>

        <div style="padding:15 0 0 200px;color:gray;">
        <h4>
        More results may be available if you Login
        </h4>
        </div>

        <%
        }
        %>

      <%

        for (SearchResult result : results)
        {

      %>
      <ul class="unstlyed">
        <img src="../../styles/common/images/<%=result.getImage()%>" alt="<%=result.getImageAlt()%>"
             class="listItemImg"/>

        <li class="listTopText">
          <%
            if (result.getLink() == null || result.getLink().equals(""))
            {
          %>
          <a onclick="<%=result.getJavascript()%>"><%=result.getName()%>
          </a>
          <%
          }
          else
          {
          %>
          <a href="<%=result.getLink()%>"><%=result.getName()%>
          </a>
          <%
            }
          %>
        </li>
        <div class="listMainText">
          <p>
            <%=result.getDetails()%>
          </p>
        </div>
        <div class="listBottomText">
        </div>


      <div class="hr">&nbsp;</div>
      <%
        }

        if (!q.equals("") && results.size() == 0)
        {
      %>

      <div style="padding:15 0 0 100px;color:gray;">
        <h4>
          Sorry, there are no results
        </h4>
      </div>
      <%
      }
      else if (results.size() == 0)
      {
      %>
      <div style="padding:15 0 0 100px;color:gray;">&nbsp;</div>
      <%
        }
      %>

      <%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>
    </ul>
  </div>
  <div class="span3">

      <%@ include file="../../WEB-INF/jspf/advancedSearch.jspf" %>

  </div>

</div>

<div id="quickview"></div>

<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>
<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>


<script type="text/javascript">
    var qv;
    var applicationManager;
    $(document).ready(function ()
    {
//      clearTabs();
        setTab('searchTab', 'current');

        $("#submitSearch").button().click(function ()
        {
            submitSearch();
        });

        qv = new QuickView();
        qv.init("quickview");

    });

    function submitSearch()
    {
        var queryString = "";

        //function to print the value of each checked checkboxes
        $("#advancedSearchOptions :checked").each(function ()
        {
            queryString += '&' + $(this).val() + '=' + $(this).val();
        });


        window.location = '../../pages/search/search.jsp?q=' + $('#searchBox').val() + queryString + '&advSearch=' + $('#advSearch').val();
    }
</script>

</div>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
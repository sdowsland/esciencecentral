<%@ page import="com.connexience.server.model.social.profile.*"%>
<%@ page import="com.connexience.server.model.image.ImageData" %>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    String id = request.getParameter("id");
    String onMessage = request.getParameter("onMessage");

    if(id==null){
        id = ticket.getUserId();
    }

    UserProfile pageProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, id);
    User pageUser = EJBLocator.lookupUserDirectoryBean().getUser(ticket, id);

    Boolean sendEmailOnMessage = false;

    if(onMessage==null){
        sendEmailOnMessage = EJBLocator.lookupNotificationsBean().getNotificationValue("EMAIL_ON_MESSAGE_RECIEVE", user.getId());
    }
    else {
        sendEmailOnMessage = Boolean.parseBoolean(onMessage);
        EJBLocator.lookupNotificationsBean().setNotificationValue("EMAIL_ON_MESSAGE_RECIEVE", user.getId(), onMessage);
    }

    if(pageProfile==null){
        response.sendRedirect("../../pages/front/index.jsp");
    }

    boolean writable = false;
    if(pageUser.getId().equals(ticket.getUserId())){
        writable = true;
    }

%>
<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">

        <h4>Email Preferences</h4>

        <form id="settingsForm" action="settings.jsp">
            <input type="hidden" name="id" value="<%=id%>">
        <label class="checkbox">
            <input id="onMessageHidden" type='hidden' value='false' name='onMessage'>
            <input id="onMessage" type="checkbox" value='true' name="onMessage" <% if(sendEmailOnMessage){ %> checked <%}%>> Send me an email when I receive a message.
        </label>

        <hr>
            <input class="btn" type="submit" value="Save">
        </form>

    </div>

        <div class="span3">
            <ul class="unstyled sidebar-menu">
                <li>
                    <h4><a href="profile.jsp?id=<%=id%>"><i class="icomoon-user"></i>Profile</a></h4>
                </li>
                <li>
                    <h4><a href="../../data/<%=id%>/info"><i class="icomoon-notebook"></i>Metadata</a></h4>
                </li>
                <li>
                    <h4><a href="profilefiles.jsp?id=<%=id%>"><i class="icomoon-copy"></i>Shared Files</a></h4>
                </li>
                <%if(writable){%>
                <li>
                    <h4><a href="profile.jsp?id=<%=id%>&action=edit"><i class="icomoon-pencil"></i>Edit Profile</a></h4>
                </li>
                <li>
                    <h4><a href="settings.jsp?id=<%=id%>"><i class="icomoon-cogs"></i>Settings</a></h4>
                </li>
                <%}%>
            </ul>
        </div>

</div>
<script type="text/javascript">

    $(document).ready(function(){

        $('#settingsForm').on('submit', function(e){

            if(document.getElementById("onMessage").checked){
                document.getElementById('onMessageHidden').disabled = true;
            }

        });

    });

</script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
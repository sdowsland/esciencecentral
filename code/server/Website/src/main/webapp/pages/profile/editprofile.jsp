<%--
  Lists all of the workflows that a user owns or can view
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>
<%@ page import="com.connexience.server.model.social.profile.*"%>
<%@ page import="com.connexience.server.model.image.ImageData" %>
<%@ page import="java.text.*"%>

      <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">

        <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
        <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
        <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>

        <script type="text/javascript" src="../../scripts/image/jquery.Jcrop.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/image/jquery.Jcrop.css">
        <script type="text/javascript" src="../../scripts/image/ImageCropperDialog.js"></script>
        
        <script type="text/javascript">
            var imageDialog = new ImageCropperDialog();
            
            $(document).ready(function()
            
            {
                clearTabs();
                imageDialog.init("imagedialog");

            });
            
            function showImageDialog(){
                imageDialog.show();
            }
        </script>

        <%
            String id = request.getParameter("id");
            UserProfile pageProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, id);
            User pageUser = EJBLocator.lookupUserDirectoryBean().getUser(ticket, id);
            if(pageProfile==null){
                response.sendRedirect("../../pages/front/index.jsp");
            }
            
            boolean writable = false;
            if(pageUser.getId().equals(ticket.getUserId())){
                writable = true;
            }

        %>


            <!-- Caption Line -->
            <h2 class="grid_12 caption">Editing: 

                <span><%=pageUser.getDisplayName()%></span></h2>

            <div class="hr grid_12 clearfix">&nbsp;</div>

            <%-- Main Content Left Hand Side--%>
            <div class="grid_8 content">
                <div id="imagedialog"></div>
                <h1>User Profile</h1>

            </div>
            


            <!-- Right Hand Side -->
            <div class="grid_4">


                <%if(writable){%>
                <ul class="unstyled sidebar-menu">
                    <li>
                        <h4><a href="profile.jsp?id=<%=id%>"><i class="icomoon-user"></i>Profile</a></h4>
                    </li>
                    <li>
                        <h4><a href="../../data/<%=id%>/info"><i class="icomoon-notebook"></i>Metadata</a></h4>
                    </li>
                    <%if(!editing){%>
                    <li>
                        <h4><a href="profilefiles.jsp?id=<%=id%>"><i class="icomoon-copy"></i>Shared Files</a></h4>
                    </li>
                    <%}%>
                    <%if(writable){%>
                    <%if(editing){%>
                    <li>
                        <h4><a href="#" onclick="showImageDialog()"><i class="icomoon-image"></i>Change Picture</a></h4>
                    </li>
                    <li>
                        <h4><a href="#" onclick="cancelEdit(event)"><i class="icomoon-close"></i>Cancel Edit</a></h4>
                    </li>
                    <li>
                        <h4><a href="#" onclick="saveChanges()"><i class="icomoon-checkmark"></i>Save Changes</a></h4>
                    </li>
                    <%} else {%>
                    <li>
                        <h4><a href="profile.jsp?id=<%=id%>&action=edit"><i class="icomoon-pencil"></i>Edit Profile</a></h4>
                    </li>
                    <%}%>
                    <li>
                        <h4><a href="settings.jsp?id=<%=id%>"><i class="icomoon-cogs"></i>Settings</a></h4>
                    </li>
                    <%}%>
                </ul>
                <%}%>
            </div>


            <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>


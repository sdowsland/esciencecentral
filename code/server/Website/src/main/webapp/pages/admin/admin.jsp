<%--
  System Admin Menu
  Author: Hugo
--%>

<%@page import="java.text.NumberFormat"%>
<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>
<%@ page import="com.connexience.server.model.storage.*" %>
            <%@include file="../../WEB-INF/jspf/page/header.jspf" %>

            <!-- Caption Line -->
            <h2>System Administration</h2>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9" id="admin-stats">
                <%
                    long userCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("USER") - 2;
                    long workflowCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("WORKFLOWDOCUMENTRECORD");
                    long invocationCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("WORKFLOWINVOCATION");
                    long documentCount = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjects("DOCUMENTRECORD");
                %>
                
                <h3><span class="icomoon-user"></span><%=userCount%> Users</h3>
                <h3><span class="icomoon-file"></span><%=documentCount%> Documents</h3>
                <h3><span class="icomoon-share"></span><%=workflowCount%> Workflows</h3>
                <h3><span class="icomoon-play-3"></span><%=invocationCount%> Workflow Runs</h3>
                <%
                    DataStore store = EJBLocator.lookupStorageBean().getOrganisationDataStore(ticket, ticket.getOrganisationId());
                    NumberFormat fmt = NumberFormat.getInstance();
                    fmt.setMaximumFractionDigits(2);
                    
                    if(store!=null && store.isSizeLimited() && store.isSpaceReportingSupported()){
                        double percent = 100.0 - (((double)store.getAvailableStoreSize() / (double)store.getTotalStoreSize()) * 100.0);
                
                    %>
                        
                    
                    <h3><span class="icomoon-storage"></span><%=fmt.format(percent)%>% Storage Used</h3>
                    <h3><span class="icomoon-key"></span>User Registration Key: <%=EJBLocator.lookupPreferencesBean().getMacAddress()%></h3>
                <%}%>
            </div>



<div class="span3">
    <%@include file="menu.jspf" %>
</div>

</div>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
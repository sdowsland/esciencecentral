<%--
  User Admin Menu
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>
<%@ page import="com.connexience.server.ejb.admin.AdminBean" %>

<%@ include file="../../WEB-INF/jspf/page/header.jspf"%>

<%
    EJBLocator.lookupAdminBean().migrateProjects(ticket);
%>
            <!-- Caption Line -->
            <h2>System Administration</h2>
            <h4>User Management</h4>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9" id="admin-users">

    </div>


    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
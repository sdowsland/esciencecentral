<%--
  System Admin Menu
  Author: Hugo
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<%
    //code here
    String action = request.getParameter("action");
    if (action != null && !action.isEmpty()) {
        if (action.equalsIgnoreCase("addPhaseToStudies")) {

            EJBLocator.lookupAdminBean().addPhaseToStudy(ticket);
        }


    }
%>

<!-- Caption Line -->
<h2>System Administration</h2>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
        <p><a href="../../pages/admin/migrations.jsp?action=addPhaseToStudies">17.06.2015: Add Phases to Studies</a></p>
    </div>


    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>

</div>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%--
  User Admin Menu
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>

        <%@ include file="../../WEB-INF/jspf/page/header.jspf"%>
        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>        
        <script type="text/javascript" src="../../scripts/admin/WorkflowEngineAdmin.js"></script>

            <!-- Caption Line -->
            <h2>System Administration</h2>
            <h4>Workflow Engine Management</h4>

            <div class="row-fluid">
                <%-- Main Content Left Hand Side--%>
                <div class="span9">
                    <div id="enginediv">

                    </div>
                    <div class="dialog" id="confirmdialog"></div>
                    <div class="dialog" id="exitdialog"></div>
                </div>
                <div class="span3">
                    <%@include file="menu.jspf"%>
                </div>

            </div>

        <script type="text/javascript">
            
            var workflowAdmin = new WorkflowEngineAdmin();
            var dialog = new ConfirmDialog();
            var exitDialog = new MultiValueSelectDialog();
            var exitOptions = ["Restart Now", "Restart when workflow finish", "Exit Now", "Exit when workflows finish"];
            
            workflowAdmin.init("enginediv");
            
            $(document).ready(function(){
                var cb = function(){
                    workflowAdmin.displayTable();
                };
                workflowAdmin.fetchEngineList(cb);
                dialog.init("confirmdialog");
                exitDialog.init("exitdialog");
                exitDialog.title = "Select reset option";
                exitDialog.okCallback = function(value, index){
                    var cb = function(){
                        $.jGrowl("Engine reset message sent");
                    };
                    
                    switch(index){
                        case 0:
                            workflowAdmin.exitAllEngines(99, false, cb);
                            break;
                            
                        case 1:
                            workflowAdmin.exitAllEngines(99, true, cb);
                            break;
                        case 2:
                            workflowAdmin.exitAllEngines(0, false, cb);
                            break;
                        case 3:
                            workflowAdmin.exitAllEngines(0, true, cb);
                            break;
                    }
                };
            });
            
            function resetWorkflowEngines(){
                exitDialog.show(exitOptions, "Restart Now");
            };
            
            function reRegisterWorkflowEngines(){
                var cb = function(){$.jGrowl("Re-register message sent");};
                workflowAdmin.reRegisterEngines(cb);
            };
            
            function flushEngineInformationCaches(){
                var cb = function(){$.jGrowl("Flush information caches message sent");};
                workflowAdmin.flushEngineInformationCaches(cb);
            };
            
            function resetEngineRmiCommunications(){
                var cb = function(){$.jGrowl("Reset RMI Comms message sent");};
                workflowAdmin.resetEngineRmiCommunications(cb);
            };
            
            function notifyAllFinishedLockHolders(){
                var cb = function(){$.jGrowl("Lock reset messages sent");};
                workflowAdmin.notifyAllFinishedLockHolders(cb);
            };
            
            function rebuildCoreLibrary(){
                var cb = function(){$.jGrowl("Library rebuild done");};
                workflowAdmin.rebuildCoreLibrary(cb);
            }
            
            function killAllWorkflows(){
          dialog.yesCallback = function(){
              var cb = function(){$.jGrowl("All Workflows terminated");};
              workflowAdmin.killAllWorkflows(cb);
          };
          dialog.noCallback = null;
          dialog.show("Are you sure you want to kill EVERY running workflow?");                
            }
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
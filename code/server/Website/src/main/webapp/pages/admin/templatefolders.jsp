
  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">

  <script type="text/javascript" src="../../scripts/admin/TemplateFolderAdmin.js"></script>
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>    
  <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>



  <!-- Caption Line -->
  <h2>System Administration</h2>
    <h4>Template Folders</h4>

  <div class="row-fluid">
  <%-- Main Content Left Hand Side--%>
  <div class="span9">

    <div id="filechooser"></div>

    <div id="descriptionDialog" class="dialog">
      <p>Please add a description of the contents of the Template Folder</p>

      <form action="javascript:createTemplateFolder();">
        <div class="formInput">
          <label for="description">Description</label>
          <input type="text" name="description" id="description"/>
        </div>
        <input type="hidden" name="folderId" id="folderId">
      </form>
    </div>
    <table id="templateFoldersTable" cellpadding="0" cellspacing="0" border="0" width="100%" class="display"></table>
  </div>

  <!-- Right Hand Side -->

      <div class="span3">
          <%@include file="menu.jspf"%>
      </div>

  </div>


<script type="text/javascript">
  var templateFolderAdmin = new TemplateFolderAdmin();
  var templateFoldersArray;
  var filechooser;

  $(document).ready(function () {

    filechooser = new FileChooser();
    filechooser.init("filechooser");
    fetchTemplateFolders();

    $("#descriptionDialog").dialog({
      bgiframe:true,
      autoOpen:false,
      height:200,
      width:500,
      title:"Add Template Folder",
      buttons:{
        'Ok':function () {
          createTemplateFolder();
        },
        'Cancel':function () {
          $(this).dialog('close');
        }
      }
    });
  });

  function createTemplateFolder() {

    $("#descriptionDialog").dialog("close");
    var folderId = $("#folderId").val();
    var description = $("#description").val();
    $("#description").val("");

    templateFolderAdmin.createTemplateFolder(folderId, description, function () {
      fetchTemplateFolders();
    });
  }


  function chooseTemplateFolder() {

    filechooser.showDialog();
    filechooser.showHomeFolder();
    filechooser.okCallback = function () {
      var folderId = filechooser.selectedFolderId;
      $("#folderId").val(folderId);


      $("#descriptionDialog").dialog("open");
    };
  }


  function fetchTemplateFolders() {
    var cb = function () {
      templateFoldersArray = templateFolderAdmin.buildTemplateFolderArray();
      $('#templateFoldersTable').dataTable({
        "bJQueryUI":true,
        "bDestroy":true,
        "fnDrawCallback":function () {
          assignCallbacks();
        },
        "aaData":templateFoldersArray,
        "aoColumns":[

          {
            "sTitle":"ID",
            "bVisible":false,
            "bSearchable":false
          },

          {
            "sTitle":"FolderId"
          },

          {
            "sTitle":"Name"
          },

          {
            "sTitle":"Description"
          },

          {
            "sTitle":"",
            "bSearchable":false,
            "sWidth":"25px",
            "bSortable":false,
            "fnRender":function (obj) {
              return '<img style="cursor: pointer;" src="cross.png" id="delete_' + obj.aData[0] + '"></img>';
            }
          }

        ]
      });
      assignCallbacks();

    };
    templateFolderAdmin.listTemplateFolders(cb);
  }

  function assignCallbacks() {
    var obj;
    var mimeId;
    var length = templateFoldersArray.length;
    for (var i = 0; i < length; i++) {
      var tf = templateFoldersArray[i];
      var tfId = tf[0];
      obj = document.getElementById("delete_" + tfId);
      if (obj) {
        obj.tfId = tfId;
        obj.onclick = function () {
          deleteTemplateFolder(this.tfId);
        }
      }
    }
  }


  function deleteTemplateFolder(id) {
    templateFolderAdmin.deleteTemplateFolder(id);
    fetchTemplateFolders();

  }

</script>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

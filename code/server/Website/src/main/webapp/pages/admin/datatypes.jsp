<%--
  User Admin Menu
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>


        <%@ include file="../../WEB-INF/jspf/page/header.jspf"%>
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>        
        <script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
        <script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>


            <!-- Caption Line -->
            <h2>System Administration</h2>

                <h4>Data Types</h4>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
                <div id="mimedialog" class="dialog">
                    <form>
                        <div class="formInput">
                            <label for="mimetype">Mimetype</label>
                            <input type="text" name="mimetype" id="mimetype"/>
                        </div>
                        <div class="formInput">
                            <label for="extension">Extension</label>
                            <input type="text" name="extension" id="extension"/>
                        </div>
                        <div class="formInput">
                            <label for="description">Description</label>
                            <input type="text" name="description" id="description"/>
                        </div>                        
                    </form>                 
                </div>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="datatypestable"></table>
            </div>

        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>

</div>


        <script type="text/javascript">
            var mimeManager = new MimeTypeManager();
            var mimeArray;
            var selectedMimeId;
            var selectedMimeType;
            var selectedMimeExtension;
            var selectedMimeDescription;
            function newDataType(){
                
            }
            $(document).ready(function(){
                fetchDataTypes();
                
                // Set up the mime type dialog
                $('#mimedialog').dialog({
                    bgiframe: true,
                    autoOpen: false,
                    height: 250,
                    width: 500,
                    title: "Edit Mime Type",
                    modal: true,
                    buttons: {
                        'Cancel' : function(){
                            $(this).dialog('close');
                        },
                        'Ok': function()
                        {
                           selectedMimeDescription = document.getElementById("description").value;
                           selectedMimeExtension = document.getElementById("extension").value;
                           selectedMimeType = document.getElementById("mimetype").value;
                           var mt = new MimeType(null);
                           mt.description = selectedMimeDescription;
                           mt.extension = selectedMimeExtension;
                           mt.id = selectedMimeId;
                           mt.mimeType = selectedMimeType;
                           mimeManager.saveMimeType(mt, function(){
                               $('#mimedialog').dialog('close');
                               fetchDataTypes();
                           });
                        }
                    }
                });                  
            });
            
            function fetchDataTypes(){
                var cb = function(){
                    mimeArray = mimeManager.getServerMimeTypeTable();
                    $('#datatypestable').dataTable( {
                            "bJQueryUI": true,
                            "bDestroy":  true,
                            "fnDrawCallback": function(){
                                assignCallbacks();
                            },
                            "aaData": mimeArray,
                            "aoColumns": [

                                    {
                                        "sTitle": "MimeType"
                                    },
                                    
                                    {
                                        "sTitle": "Extension",
                                        "sWidth": "30px",
                                        "fnRender": function(obj){
                                            return '<a style="cursor: pointer;" id="ext_edit_' + obj.aData[3] + '">' + obj.aData[1] + '</a>';
                                        }
                                    },
                                    
                                    {
                                        "sTitle" : "",
                                        "bSearchable": false,
                                        "sWidth": "25px",
                                        "bSortable": false,
                                        "fnRender" : function(obj){
                                            return '<img style="cursor: pointer;" src="cross.png" id="delete_' + obj.aData[3] + '"></img>';
                                        }                                        
                                    },
                                    {
                                        "sTitle": "Description",
                                        "bSearchable": false,
                                        "bVisible" : false                                        
                                    }
                            ]
                    } );
                    assignCallbacks();
                    
                }
                mimeManager.fetchMimeTypeData(cb);                
            }
            
            function assignCallbacks(){
                var obj;
                var mimeId;
                var length = mimeArray.length;
                for(var i=0;i<length;i++){
                    var smt = mimeArray[i];
                    mimeId = smt[3];
                    obj = document.getElementById("delete_" + mimeId);
                    if(obj){
                        obj.mimeTypeId = mimeId;
                        obj.onclick = function(){
                            deleteMimeType(this.mimeTypeId);
                        }
                    }

                    obj = document.getElementById("ext_edit_" + mimeId);
                    if(obj){
                        obj.mimeTypeId = mimeId;
                        obj.mimeIndex = i;
                        obj.onclick = function(){
                            editMimeType(this.mimeIndex);
                        }
                    }
                }                
            }
            
            function deleteMimeType(id){
                mimeManager.deleteMimeType(id, function(){
                    fetchDataTypes();
                })
            }
            
            function editMimeType(index){
                if(mimeArray){
                    var selectedMime = mimeArray[index];
                    selectedMimeType = selectedMime[0];
                    selectedMimeExtension = selectedMime[1];
                    selectedMimeDescription = selectedMime[2];
                    selectedMimeId = selectedMime[3];

                    $('#mimetype').val(selectedMimeType);
                    $('#extension').val(selectedMimeExtension);
                    $('#description').val(selectedMimeDescription);

                    $('#mimedialog').dialog('open');
                }
            }
            
            function createMimeType(){
                selectedMimeType = "";
                selectedMimeExtension = "";
                selectedMimeDescription = "";
                selectedMimeId = "";
                $('#mimetype').val(selectedMimeType);
                $('#extension').val(selectedMimeExtension);
                $('#description').val(selectedMimeDescription);          
                $('#mimedialog').dialog('open');
            }
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
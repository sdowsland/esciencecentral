

<%@ page import="java.util.List" %>
<%@ page import="com.connexience.server.model.dashboard.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
<script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
<script type="text/javascript" src="../../scripts/dashboard/DashboardViewManager.js"></script>
    
<%
    List dashboards;
    String viewType = request.getParameter("type");
    if("shared".equals(viewType)){
        dashboards = EJBLocator.lookupDashboardBean().listSharedDashboards(ticket);
    } else {
        viewType = "mine";
        dashboards = EJBLocator.lookupDashboardBean().listUserDashboards(ticket);
    }
%>

<%if("shared".equals(viewType)){%>
    <h2>Shared Dashboards</h2>
<%} else {%>
    <h2>My Dashboards</h2>
<%}%>

<h4>&nbsp;</h4>
<div class="row-fluid">
    <div id="confirmdialog" class="dialog"></div>
    <div id="dashboardeditor" class="dialog"></div>
    
    <div class="span9">
  
        <ul class="unstyled" id="generic-list">
        <%
            Dashboard db;
            for(int i=0;i<dashboards.size();i++){
                db = (Dashboard)dashboards.get(i);
        %>
                <li class="clearfix">
                    <a class="generic-picture" onclick="openDashboard('<%=db.getId()%>');">
                        <img src="../../scripts/dashboard/images/dashboard.png" alt="Dashboad icon"/>
                    </a>                    
                    <h6><%=db.getName()%></h6>
                  <div class="generic-description">
                    <p>
                      <%=db.getDescription()%>
                    </p>
                  </div>
                  <ul class="inline">
                      <li><a style="cursor: pointer;" onclick="editDashboard('<%=db.getId()%>');">Properties</a></li>
                      <li><a style="cursor: pointer;" onclick="openDashboard('<%=db.getId()%>');">View</a></li>
                      <li><a style="cursor: pointer;" onclick="deleteDashboard('<%=db.getId()%>');">Delete</a></li>
                      <li><a style="cursor: pointer;" onclick="shareDashboard('<%=db.getId()%>');">Share</a></li>
                  </ul>
                </li>   
            
            
       <%}%>
        

        </ul>
    </div>
    
    <!-- Menu -->
    <div class="span3">
    <ul class="unstyled sidebar-menu">
        <li>
            <h5><a href="../../pages/settings/settings.jsp">General Settings</a></h5>
        </li>
        <li>
            <h5>Sample Data</h5>
        </li>
        <li>
            <h5><a href="../../pages/settings/datasets.jsp">Datasets</a></h5>
        </li>
        <li>
            <h5>Dashboards</h5>
            <ul class="unstyled sidebar-menu">
                <li>
                    <h6><a href="../../pages/dashboard/viewdashboard.jsp">Create new Dashboard</a></h6>
                    <%if("shared".equals(viewType)){%>                    
                    <h6><a href="../../pages/dashboard/dashboards.jsp?type=mine">View my Dashboards</a></h6>
                    <%} else {%>
                    <h6><a href="../../pages/dashboard/dashboards.jsp?type=shared">View shared Dashboards</a></h6>
                    <%}%>
                </li>
            </ul>            
        </li>         
        <li>
            <h5><a href="../../pages/settings/searchfolders.jsp">Search Folders</a></h5>
        </li>
        <%if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){%>
        <li class="divider"></li>
        <li>
            <h5><a href="../../pages/admin/admin.jsp">System Administration</a></h5>
        </li>
        <%}%>
    </ul>        

    </div>
    
</div>

<script type="text/javascript">
    var confirmDialog = new ConfirmDialog();
    var dashboardViewManager = new DashboardViewManager();
    var dashboardEditor = new NameDescriptionDialog();
    
    var acl = new AccessControl();
    $(document).ready(function(){
        confirmDialog.init("confirmdialog");
        dashboardEditor.init("dashboardeditor");
    });
    
    function openDashboard(id){
        document.location = "../../pages/dashboard/viewdashboard.jsp?id=" + id;
    }
    
    function editDashboard(id){
        dashboardEditor.okCallback = function(name, description){
            dashboardViewManager.setDashboardDetails(id, name, description, function(){
                location.reload();
            });
        };
        
        var cb = function(name, description){
            dashboardEditor.show(name, description);
        };
        dashboardViewManager.getDashboardDetails(id, cb);
    }
    
    function deleteDashboard(id){
        confirmDialog.yesCallback = function(){
            var cb = function(){
                location.reload();
            };
            
            jsonCall({id: id}, rewriteAjaxUrl("../../servlets/dataset?method=deleteDashboard"), cb);
        };
        confirmDialog.show("Are you sure you want to delete the dashboard");
    }
    
    function shareDashboard(id){
        $('#securitydialog').remove();

        //add a new ACL
        $('body').append('<div id="securitydialog" class="acl"></div>');
        acl.init("securitydialog", _userId, id, _publicUserId, _enablePublic, _usersGroupId);
        acl.open();        
    }
    

</script>


  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>




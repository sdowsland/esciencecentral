<%@ page import="com.connexience.server.web.tags.*" %>
<%@ page import="com.connexience.server.model.dashboard.*"%>

<%--
  Index Page
  Author: Hugo
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%

    if(publicUser)
    {
      response.sendRedirect("../../pages/front/welcome.jsp");
    }
%>
<script type="text/javascript" src="../../scripts/dashboard/DashboardViewManager.js"></script>
<script type="text/javascript">
    var dashboardViewManager = new DashboardViewManager();
    dashboardViewManager.saveAsMainDashboard = false;
    dashboardViewManager.userId = '<%=ticket.getUserId()%>';      
      
<%
    String id = request.getParameter("id");
    
    String dashboardText;
    if(id==null || id.isEmpty()){
        dashboardText = "New Dashboard";
    %>
        dashboardViewManager.newDashboard = true;
    <%} else {
        Dashboard db = EJBLocator.lookupDashboardBean().getDashboard(ticket, id);
        dashboardText = db.getName();
        %>
        dashboardViewManager.newDashboard = false;
        dashboardViewManager.dashboardId = '<%=id%>';    
    <%}%>
</script>

 
    
  
<%@include file="../../pages/front/dashboard.jspf" %>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

<%@include file="header.jspf" %>
<!-- Wrapping div tag with ID to apply styling to this specific page using the ID -->
<div id="template">
    <!-- Title block above page content -->
    <h2>Title</h2>
    <h4>Sub Title</h4>
    <!-- Bootstrap fluid row is a responsive full with div needed to contain 12 column grid system-->
    <div class="row-fluid">
        <!-- Main content column is 3/4 of width of row, or 9 grid column widths -->
        <div class="span9">
            <p>Main content goes here</p>
        <div>
        <!-- Sidebar Menu contained in remaining 1/4 of space -->
        <div class="span3">
            <!-- Unstyled Bootstap class remove bullet points and sets basic list style. Sidebar-menu class sets eSC specific menu styles -->
            <ul class="unstyled sidebar-menu">
                <!-- All sidebar menu items are anchor tags inside h6 tags -->
                <li>
                    <h6><a href="#">Item 1</a></h6>
                </li>
                <li>
                    <h6><a href="#">Item 2</a></h6>
                </li>
                <li>
                    <h6><a href="#">Item 3</a></h6>
                </li>
                <li>
                    <h6><a href="#">Item 4</a></h6>
                </li>
            </ul>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
<%
    String themeFolder = request.getParameter("themeFolder");
    String siteName = request.getParameter("siteName");
%>

<style type="text/css">

    #logoBox {
        margin-right: 20px;
        padding-top: 35px
    }

    #logoBox img {
        padding-bottom: 10px;
        width: 143px;
        margin: 0;
    }

        /*#logoBox :first-child {*/
        /*padding-right: 0;*/
        /*}*/

        /*#logoBox :last-child {*/
        /*padding-left: 0;*/
        /*}*/


</style>

<div class="grid_5 content rightBox" id="logoBox">
    <img src="../../styles/xps/images/epsrc.jpg" style="width:170px" alt="EPSRC"/>
    <img src="../../styles/xps/images/ncl.jpg" style="width: 210px; float: right;" alt="NCL"/>
</div>

<div class="grid_7 content leftBox">
    <p> Analysis of X-ray photoelectron spectrocopy (XPS) data requires collaboration between those planning XPS data
        acquisition, those processing the data, and those interpreting it in the context of a scientific application.
    </p>

    <p>
        NEXUS has developed this collaborative platform to meet these needs. Our cloud-based system combines powerful
        data management and analysis, workflow process definition and automation, and the ability to share and
        collaborate in a secure, reliable and scalable environment.
    </p>


</div>
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;

/**
 * Simple representation of a permission.
 * @author hugo
 */
public class EscPermission implements JsonSerializable {
    public static final String READ = "read";
    public static final String WRITE = "write";

    private String permissionType = READ;
    private String principalId;
    
    public EscPermission() {
    }
    
    public EscPermission(String permissionType){
        this.permissionType = permissionType;
    }
    
    public EscPermission(JSONObject json) {
        parseJsonObject(json);
    }
    
    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public String getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(String principalId) {
        this.principalId = principalId;
    }

    @Override
    public JSONObject toJsonObject() {
        return new JSONObject(this);
    }

    @Override
    public void parseJsonObject(JSONObject json) {
       permissionType = json.getString("permissionType", READ);
       principalId = json.getString("principalId", null);
    }    
}
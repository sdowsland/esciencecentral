/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;

import javax.xml.bind.annotation.XmlType;

/**
 * Simple workflow invocation record for soap web service
 * @author hugo
 */
@XmlType
public class EscWorkflowInvocation extends EscObject implements JsonSerializable {
    public static final String INVOCATION_FINISHED_OK = "Finished";
    public static final String INVOCATION_FINISHED_WITH_ERRORS = "ExecutionError";
    public static final String INVOCATION_RUNNING = "Running";
    public static final String INVOCATION_WAITING = "Queued";
    public static final String INVOCATION_WAITING_FOR_DEBUGGER = "Debugging";
    public static final String INVOCATION_STATE_UNKNOWN = "Unknown";
    
    private String status;
    private String statusMessage;
    private String workflowId;
    private String workflowVersionId;
    private long queuedTimestamp;
    private long dequeuedTimestamp;
    private long startTimestamp;
    private long endTimestamp;
    private int percentComplete;
    private String workflowName;
    private String engineId;

    public EscWorkflowInvocation() {
    }

    public EscWorkflowInvocation(JSONObject json) {
        parseJsonObject(json);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getWorkflowVersionId() {
        return workflowVersionId;
    }

    public void setWorkflowVersionId(String workflowVersionId) {
        this.workflowVersionId = workflowVersionId;
    }

    @Override
    public JSONObject toJsonObject() {
        return new JSONObject(this);
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public long getQueuedTimestamp() {
        return queuedTimestamp;
    }

    public void setQueuedTimestamp(long timestamp) {
        this.queuedTimestamp = timestamp;
    }

    public long getDequeuedTimestamp() {
        return dequeuedTimestamp;
    }

    public void setDequeuedTimestamp(long timestamp) {
        this.dequeuedTimestamp = timestamp;
    }

    public int getPercentComplete() {
        return percentComplete;
    }

    public void setPercentComplete(int percentComplete) {
        this.percentComplete = percentComplete;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getEngineId() { return engineId; }

    public void setEngineId(String engineId) { this.engineId = engineId; }

    @Override
    public final void parseJsonObject(JSONObject json) {
        super.parseJsonObject(json);
        status = json.getString("status", null);
        statusMessage = json.getString("statusMessage", null);
        workflowId = json.getString("workflowId", null);
        workflowVersionId = json.getString("workflowVersionId", null);
        queuedTimestamp = json.getLong("queuedTimestamp", 0);
        dequeuedTimestamp = json.getLong("dequeuedTimestamp", 0);
        startTimestamp = json.getLong("startTimestamp", 0);
        endTimestamp = json.getLong("endTimestamp", 0);
        percentComplete = json.getInt("percentComplete", 0);
        workflowName = json.getString("workflowName", "");
        engineId = json.getString("engineId", "");
    }

    @Override
    public String getObjectType() {
        return getClass().getSimpleName();
    }    

    /** Check to see if this workflow is in one of its running states */
    public boolean isInProgress(){
        if(status.equals(INVOCATION_FINISHED_OK) || status.equals(INVOCATION_FINISHED_WITH_ERRORS) || status.equals(INVOCATION_STATE_UNKNOWN)){
            return false;
        } else {
            return true;
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

/**
 * Docs back-references
 * /documentation/wiki/devs-guide/rest-api/java-rest-client-r2317.md 
 */
package com.connexience.api;

import com.connexience.api.model.*;
import com.connexience.api.model.json.JSONArray;
import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.net.GenericClient;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class provides a client for the REST workflow service.
 * @author hugo
 */
public class WorkflowClient extends GenericClient implements WorkflowInterface
{
    /**
     * <p>Creates a reference to the workflow API client with parameters provided explicitly.</p>
     *
     * <p>Note: this operation does not communicate with the server to authenticate.
     * The authentication will occur on the first attempt to use the client.</p>
     * 
     * @param hostname -- the address of an e-Science Central server (IP or DNS name)
     * @param port -- port number on which the server listens on. Usually, 8080 or 80 for HTTP, 443 or 4343 for HTTPS
     * @param secure -- <code>true</code> if the connection should be made to a HTTPS endpoint, <code>false</code> if the server should be accessed using HTTP 
     * @param username -- logon name of the user to authenticate with
     * @param password -- password to authenticate with
     */
    public WorkflowClient(String hostname, int port, boolean secure, String username, String password) {
        super(hostname, port, secure, "/api/public/rest/v1/workflow", username, password);
    }


    /**
     * <p>Creates a reference to the workflow API client. The client will authenticate using information from 
     * the properties file at ~/.inkspot/api.properties.</p>
     * 
     * <p>Note: this operation does not communicate with the server to authenticate.
     * The authentication will occur on the first attempt to use the client.</p>
     * 
     * @throws Exception -- in the case an exception occurs when reading properties file. 
     */
    public WorkflowClient() throws Exception {
        super("/api/public/rest/v1/workflow");
    }


    /**
     * <p>Creates a reference to the workflow API client based on the existing client
     * reference (e.g. storage or dataset client). It will use the same 
     * authentication information as the given reference.</p>
     * 
     * <p>Note: this operation does not communicate with the server to authenticate.
     * The authentication will occur on the first attempt to use the client.</p>
     * 
     * @param existingClient -- a reference to an existing API client like workflow or dataset client.
     */
    public WorkflowClient(GenericClient existingClient) throws Exception {
        existingClient.configureClient(this);
        this.setUrlBase("/api/public/rest/v1/workflow");
    }


    /**
     * <p>Creates a reference to the workflow API client. The client will authenticate using information from 
     * the given properties file. The file should include five properties (displayed are also default values):</p>
     *
     * <pre>
     *    hostname = localhost
     *    port = 8080
     *    username =
     *    password = 
     *    secure = false
     * </pre>
     * 
     * <p>Note: this operation does not communicate with the server to authenticate.
     * The authentication will occur on the first attempt to use the client.</p>
     * 
     * @throws Exception -- in the case an exception occurs when reading the properties file. 
     */
    public WorkflowClient(File apiProperties) throws Exception {
        super("/api/public/rest/v1/workflow", apiProperties);
    }
    
    @Override
    public EscWorkflow[] listWorkflows() throws Exception {
        JSONArray json = retrieveJsonArray("/workflows");
        EscWorkflow[] results = new EscWorkflow[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflow(json.getJSONObject(i));
        }
        return results;
    }

    @Override
    public EscWorkflow[] listSharedWorkflows() throws Exception {
        JSONArray json = retrieveJsonArray("/workflows/shared");
        EscWorkflow[] results = new EscWorkflow[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflow(json.getJSONObject(i));
        }
        return results;
    }

    @Override
    public EscWorkflowService[] listSharedServices() throws Exception {
        JSONArray json = retrieveJsonArray("/services");
        EscWorkflowService[] results = new EscWorkflowService[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflowService(json.getJSONObject(i));
        }
        return results;
    }

    
    @Override
    public EscWorkflow[] listProjectWorkflows(String projectId) throws Exception {
        JSONArray json = retrieveJsonArray("/project/" + projectId + "/workflows");
        EscWorkflow[] results = new EscWorkflow[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflow(json.getJSONObject(i));
        }
        return results;
    }

    @Override
    public EscWorkflow[] listAllWorkflows() throws Exception {
        JSONArray json = retrieveJsonArray("/workflows/all");
        EscWorkflow[] results = new EscWorkflow[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflow(json.getJSONObject(i));
        }
        return results;
    }

    @Override
    public EscWorkflowInvocation[] listInvocationsRelatedToDocument(String documentId) throws Exception {
        JSONArray results = retrieveJsonArray("/relatedinvocations/" + documentId);
        EscWorkflowInvocation[] invocations = new EscWorkflowInvocation[results.length()];
        for(int i=0;i<results.length();i++){
            invocations[i] = new EscWorkflowInvocation(results.getJSONObject(i));
        }
        return invocations;        
    }

    
    @Override
    public EscWorkflow[] listCallableWorkflows() throws Exception {
        JSONArray json = retrieveJsonArray("/callableworkflows");
        EscWorkflow[] results = new EscWorkflow[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflow(json.getJSONObject(i));
        }
        return results;
    }


    @Override
    public EscWorkflow[] listAllCallableWorkflows() throws Exception {
        JSONArray json = retrieveJsonArray("/allcallableworkflows");
        EscWorkflow[] results = new EscWorkflow[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = new EscWorkflow(json.getJSONObject(i));
        }
        return results;
    }


    @Override
    public EscWorkflow getWorkflow(String workflowId) throws Exception {
        JSONObject workflowJson = retrieveJson("/workflows/" + workflowId);
        if(workflowJson!=null){
            return new EscWorkflow(workflowJson);
        } else {
            return null;
        }
    }

    @Override
    public EscWorkflow saveWorkflow(EscWorkflow workflow) throws Exception {
        return new EscWorkflow(postJsonRetrieveJson("/workflows", workflow.toJsonObject()));
    }
    
    @Override
    public void deleteWorkflow(String workflowId) throws Exception {
        deleteResource("/workflows/" + workflowId);
    }

    @Override
    public EscWorkflowInvocation executeWorkflow(String workflowId) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/invoke", "EXECUTE"));
    }

    @Override
    public EscWorkflowInvocation executeWorkflow(String workflowId, String versionId) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/" + versionId + "/invoke", "EXECUTE"));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowOnDocument(String workflowId, String documentId) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/docinvoke", documentId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowOnDocument(String workflowId, String versionId, String documentId) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/" + versionId + "/docinvoke", documentId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowWithParameters(String workflowId, EscWorkflowParameterList parameters) throws Exception {
        return new EscWorkflowInvocation(postJsonRetrieveJson("/workflows/" + workflowId + "/paraminvoke", parameters.toJsonObject()));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowWithParameters(String workflowId, String versionId, EscWorkflowParameterList parameters) throws Exception {
        return new EscWorkflowInvocation(postJsonRetrieveJson("/workflows/" + workflowId + "/" + versionId + "/paraminvoke", parameters.toJsonObject()));
    }

    @Override
    public EscWorkflowInvocation[] listInvocationsOfWorkflow(String workflowId) throws Exception {
        JSONArray results = retrieveJsonArray("/workflows/" + workflowId + "/invocations");
        EscWorkflowInvocation[] invocations = new EscWorkflowInvocation[results.length()];
        for(int i=0;i<results.length();i++){
            invocations[i] = new EscWorkflowInvocation(results.getJSONObject(i));
        }
        return invocations;
    }

    @Override
    public EscWorkflowInvocation getInvocation(String invocationId) throws Exception {
        return new EscWorkflowInvocation(retrieveJson("/invocations/" + invocationId));
    }

    @Override
    public EscWorkflowInvocation terminateInvocation(String invocationId) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/invocations/" + invocationId + "/terminate", "TERMINATE"));
    }


    @Override
    public HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(String workflowId) throws Exception {
        return listCallableWorkflowParametersEx(workflowId, null);
    }


    @Override
    public HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(String workflowId, String versionId) throws Exception {
        JSONObject results = (versionId == null || "".equals(versionId) || "latest".equals(versionId)) ?
                retrieveJson("/callableworkflows/" + workflowId + "/parametersex") :
                retrieveJson("/callableworkflows/" + workflowId + "/" + versionId + "/parametersex");

        HashMap<String, EscWorkflowParameterDesc> params = new HashMap<>();
        Iterator keys = results.keys();
        String key;
        while(keys.hasNext()){
            key = keys.next().toString();
            params.put(key, new EscWorkflowParameterDesc(results.getJSONObject(key)));
        }
        return params;
    }


    @Override
    public HashMap<String, String> listCallableWorkflowParameters(String workflowId) throws Exception {
        JSONObject results = retrieveJson("/callableworkflows/" + workflowId + "/parameters");
        HashMap<String, String> params = new HashMap<>();
        Iterator keys = results.keys();
        String key, value;
        while(keys.hasNext()){
            key = keys.next().toString();
            value = results.getString(key);
            params.put(key, value);
        }
        return params;
    }

    @Override
    public HashMap<String, String> listCallableWorkflowParameters(String workflowId, String versionId) throws Exception {
        JSONObject results = retrieveJson("/callableworkflows/" + workflowId + "/" + versionId + "/parameters");
        HashMap<String, String> params = new HashMap<>();
        Iterator keys = results.keys();
        String key, value;
        while(keys.hasNext()){
            key = keys.next().toString();
            value = results.getString(key);
            params.put(key, value);
        }
        return params;
    }

    @Override
    public EscWorkflowService getService(String serviceId) throws Exception {
        JSONObject json = retrieveJson("/services/" + serviceId);
        if(json!=null){
            return new EscWorkflowService(json);
        } else {
            return null;
        }
    }

    @Override
    public EscWorkflowService saveService(EscWorkflowService service) throws Exception {
        return new EscWorkflowService(postJsonRetrieveJson("/services", service.toJsonObject()));
    }

    @Override
    public EscWorkflowLibrary getLibraryByName(String name) throws Exception {
        JSONObject json = retrieveJson("/librariesbyname/" + name);
        if(json!=null){
            return new EscWorkflowLibrary(json);
        } else {
            return null;
        }
    }

    @Override
    public EscWorkflowLibrary saveLibrary(EscWorkflowLibrary library) throws Exception {
        return new EscWorkflowLibrary(postJsonRetrieveJson("/libraries", library.toJsonObject()));
    }

    @Override
    public EscWorkflowInvocation executeCallableWorkflow(String workflowId, String parametersJson) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/externalinvoke", parametersJson));
    }

    @Override
    public EscWorkflowInvocation executeCallableWorkflow(String workflowId, String versionId, String parametersJson) throws Exception {
        return new EscWorkflowInvocation(postTextRetrieveJson("/workflows/" + workflowId + "/" + versionId + "/externalinvoke", parametersJson));
    }

    /** Convenience method with JSONObject */
    public EscWorkflowInvocation executeCallableWorkflow(String workflowId, JSONObject parameters) throws Exception {
        return executeCallableWorkflow(workflowId, parameters.toString());
    }

    /** Convenience method with JSONObject */
    public EscWorkflowInvocation executeCallableWorkflow(String workflowId, String versionId, JSONObject parameters) throws Exception {
        return executeCallableWorkflow(workflowId, versionId, parameters.toString());
    }
    
    @Override
    public void enableWorkflowExternalDataSupport(String workflowId, String blockName) throws Exception {
        postTextRetrieveText("/workflows/" + workflowId + "/enableexternaldata", blockName);
    }
    
    @Override
    public void disableWorkflowExternalDataSupport(String workflowId) throws Exception {
        postTextRetrieveText("/workflows/" + workflowId + "/disableexternaldata", "");
    }
}

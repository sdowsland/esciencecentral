/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONArray;
import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;

import javax.xml.bind.annotation.XmlType;

/**
 * This class represents a descriptor for a callable workflow parameter
 * @author Jacek
 */
@XmlType
public class EscWorkflowParameterDesc implements JsonSerializable {
    private String name;
    private String type;
    private String value;
    private String description;
    private String[] options;

    public EscWorkflowParameterDesc(String name, String type, String value, String description, String[] options) {
        this.name = name;
        this.type = type;
        this.value = value;
        this.description = description;
        this.options = options;
    }

    public EscWorkflowParameterDesc() {
    }

    public EscWorkflowParameterDesc(JSONObject json) {
        parseJsonObject(json);
    }
    
    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public JSONObject toJsonObject() {
        return new JSONObject(this);
    }

    @Override
    public final void parseJsonObject(JSONObject json) {
        name = json.getString("name", null);
        type = json.getString("type", null);
        value = json.getString("value", null);
        description = json.getString("description", null);

        JSONArray array = json.optJSONArray("options");
        if (array != null) {
            int len = array.length();
            options = new String[len];
            for (int i = 0; i < len; i++) {
                options[i] = array.getString(i);
            }
        } else {
            options = null;
        }
    }

    @Override
    public String toString() {
        return toJsonObject().toString(2);
    }
}
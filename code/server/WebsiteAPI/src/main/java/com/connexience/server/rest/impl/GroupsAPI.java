/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.rest.api.IGroupsAPI;
import com.connexience.server.rest.exceptions.ConnexienceWebApplicationException;
import com.connexience.server.rest.model.WebsiteGroup;
import com.connexience.server.rest.util.TransitionSessionUtils;


import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by markturner on 31/07/2014.
 */
@SuppressWarnings("unchecked")
@Path("/rest/groups")
public class GroupsAPI implements IGroupsAPI {
    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/GroupDirectoryBean")
    GroupDirectoryRemote groupDirectoryBean;

    @Override
    public List<WebsiteGroup> getGroups(@DefaultValue("") String query) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteGroup> groups = new ArrayList<>();

        try {

            List<Group> connexienceGroups;

            if(query != "") {
                connexienceGroups = groupDirectoryBean.searchForGroups(ticket, query);
            }
            else {
                connexienceGroups = groupDirectoryBean.listGroups(ticket);
            }

            for(Group connexienceGroup : connexienceGroups) {
                groups.add(new WebsiteGroup(connexienceGroup, false, ticket));
            }
        }
        catch (ConnexienceException ex) {

            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }

        return groups;
    }

    @Override
    public WebsiteGroup createGroup(WebsiteGroup group) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            return WebsiteGroup.saveGroup(group, ticket);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }
    }

    @Override
    public WebsiteGroup getGroup(String groupId, Boolean userMetadata) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WebsiteGroup group;

        try {
            group = new WebsiteGroup(groupDirectoryBean.getGroup(ticket, groupId), userMetadata, ticket);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }

        return group;
    }

    @Override
    public WebsiteGroup updateGroup(String groupId, WebsiteGroup group) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            return WebsiteGroup.saveGroup(group, ticket);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }
    }

    @Override
    public Response deleteGroup(String groupId) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            groupDirectoryBean.removeGroup(ticket, groupId);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }

        return Response.status(Response.Status.OK).entity("{\"message\":\" Successfully removed group: " + groupId + "\"}").build();
    }

    @Override
    public WebsiteGroup getGroupByName(String groupName, Boolean userMetadata) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WebsiteGroup group;

        try {
            group = new WebsiteGroup(groupDirectoryBean.getGroupByName(ticket, groupName), userMetadata, ticket);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }

        return group;
    }

    @Override
    public WebsiteGroup addUserToGroup(String groupId, String userId) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WebsiteGroup group;

        try {
            groupDirectoryBean.addUserToGroup(ticket, userId, groupId);
            group = new WebsiteGroup(groupDirectoryBean.getGroup(ticket, groupId), false, ticket);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }

        return group;
    }

    @Override
    public WebsiteGroup removeUserFromGroup(String groupId, String userId) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WebsiteGroup group;

        try {
            groupDirectoryBean.removeUserFromGroup(ticket, userId, groupId);
            group = new WebsiteGroup(groupDirectoryBean.getGroup(ticket, groupId), false, ticket);
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }

        return group;
    }
}

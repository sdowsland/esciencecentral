/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.LibrarySaveOperation;
import com.connexience.provenance.model.logging.events.ServiceSaveOperation;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.document.UncommittedVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.SearchFolder;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.rest.api.IDocumentsAPI;
import com.connexience.server.rest.model.*;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.StorageUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.spi.NotFoundException;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Path("/rest/documents")
public class DocumentsAPI implements IDocumentsAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

    @Override
    public WebsiteDocument getDocument(String id) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, id);

        WebsiteDocument websiteDoc = new WebsiteDocument();
        websiteDoc.setId(doc.getId());
        websiteDoc.setName(doc.getName());
        websiteDoc.setContainerId(doc.getContainerId());
        websiteDoc.setDescription(doc.getDescription());
        websiteDoc.setCreator(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, doc.getCreatorId()), false, ticket));
        websiteDoc.setCurrentVersionSize(doc.getCurrentVersionSize());
        websiteDoc.setCurrentVersionNumber(doc.getCurrentVersionNumber());
        websiteDoc.setDownloadPath("/data/" + doc.getId() + "/latest");
        websiteDoc.setUploadPath("/data/" + doc.getId());
        websiteDoc.setProjectId(doc.getProjectId());

        return websiteDoc;
    }

    @Override
    public DocumentRecord createDocument(@PathParam(value = "id") String id, @FormParam(value = "name") String name) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        return StorageUtils.getOrCreateDocumentRecord(ticket, id, name);
    }

    @Override
    public WebsiteDocument updateDocument(String documentId, WebsiteDocument document) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);

        if (doc != null) {
            doc.setContainerId(document.getContainerId());
            doc.setName(document.getName());
            doc.setDescription(document.getDescription());
            doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);

            WebsiteDocument websiteDoc = new WebsiteDocument();
            websiteDoc.setId(doc.getId());
            websiteDoc.setName(doc.getName());
            websiteDoc.setContainerId(doc.getContainerId());
            websiteDoc.setDescription(doc.getDescription());
            websiteDoc.setCreator(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, doc.getCreatorId()), false, ticket));
            websiteDoc.setCurrentVersionSize(doc.getCurrentVersionSize());
            websiteDoc.setCurrentVersionNumber(doc.getCurrentVersionNumber());
            websiteDoc.setDownloadPath("/data/" + doc.getId() + "/latest");
            websiteDoc.setUploadPath("/data/" + doc.getId());
            websiteDoc.setProjectId(doc.getProjectId());

            return websiteDoc;
        } else {
            throw new Exception("No such document");
        }
    }

    @Override
    public Response moveDocuments(@PathParam("folderId") String folderId, String[] documentIds, Boolean copy) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        
        if (!isSearchFolder(ticket, folderId)) {

            for (int i = 0; i < documentIds.length; i++) {
                ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, getObjectId(documentIds[i]), ServerObject.class);

                String newProjectId = null;
                String destination = request.getParameter("dst");

                if (folderId != null) {
                    //will be of the form volId_docId
                    if (!getVolumeId(documentIds[i]).equals(getVolumeId(destination))) {
                        String newProjectDataFolderId = getVolumeId(destination);

                        //if the new project volume == 0 this is the user's home folder.  Leave the newProjectId as null
                        //so that it gets unset in the save
                        if (!newProjectDataFolderId.equals("0")) {
                            Folder newProjectDataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, newProjectDataFolderId);
                            newProjectId = newProjectDataFolder.getProjectId();
                        }
                    }
                }

                if (!copy) {
                    // Move files
                    if (obj instanceof DocumentRecord) {
                        // Move document
                        EJBLocator.lookupStorageBean().moveDocument(ticket, obj.getId(), folderId, newProjectId);
                    } else if (obj instanceof Folder) {
                        // Move folder
                        EJBLocator.lookupStorageBean().moveFolder(ticket, obj.getId(), folderId, newProjectId);
                    }

                } else {
                    // Copy files
                    if (obj instanceof DocumentRecord) {

                        DocumentRecord sourceDocument = (DocumentRecord) obj;
                        DocumentRecord targetDocument = new DocumentRecord();

                        if (obj instanceof WorkflowDocument) {
                            sourceDocument = (WorkflowDocument) obj;
                            targetDocument = new WorkflowDocument();
                        }

                        sourceDocument.populateCopy(targetDocument);
                        targetDocument.setContainerId(folderId);
                        targetDocument = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, targetDocument);

                        EJBLocator.lookupStorageBean().checkProjectId(ticket, targetDocument, newProjectId);

                        StorageUtils.copyDocumentData(ticket, sourceDocument, null, targetDocument);

                        ImageData image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, obj.getId(), ImageData.WORKFLOW_PREVIEW);
                        if (obj instanceof WorkflowDocument) {
                            if (image != null) {
                                ImageData copyImage = image.getCopy();
                                copyImage.setServerObjectId(targetDocument.getId());
                                EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, targetDocument.getId(), copyImage.getData(), copyImage.getType());
                            }
                        }


                    } else {
                        // TODO: Cannot copy folders (yet)
                        throw new Exception("Cannot copy folders or datasets yet");
                    }
                }
            }

            return Response.status(Response.Status.OK).build();
        }

        return Response.status(Response.Status.FORBIDDEN).build();
    }

    @Override
    public Response deleteDocument(String documentId) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        try {
            EJBLocator.lookupStorageBean().removeDocumentRecord(ticket, documentId);

            return Response.status(Response.Status.OK).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }

    @Override
    public Response batchUpload(String id, MultipartFormDataInput form) throws ConnexienceException {
        System.out.println("batchUpload, id: " + id);

        for (Map.Entry<String, List<InputPart>> inputParts : form.getFormDataMap().entrySet()) {
            System.out.println("inputParts.getKey() = " + inputParts.getKey());
            System.out.println("inputParts.getValue().size() = " + inputParts.getValue().size());

            for (InputPart inputPart : inputParts.getValue()) {
                System.out.println("inputPart.getMediaType().getType() = " + inputPart.getMediaType().getType());

                for (Map.Entry<String, List<String>> headers : inputPart.getHeaders().entrySet()) {
                    System.out.println("headers.getKey() = " + headers.getKey());

                    for (String header : headers.getValue()) {
                        System.out.println("header = " + header);
                    }
                }
            }
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public WebsiteDocument getNamedDocument(String name, String folderId) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        DocumentRecord doc = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, folderId, name);

        if (doc != null) {

            WebsiteDocument websiteDoc = new WebsiteDocument();
            websiteDoc.setId(doc.getId());
            websiteDoc.setName(doc.getName());
            websiteDoc.setContainerId(doc.getContainerId());
            websiteDoc.setDescription(doc.getDescription());
            websiteDoc.setCreator(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, doc.getCreatorId()), false, ticket));
            websiteDoc.setCurrentVersionSize(doc.getCurrentVersionSize());
            websiteDoc.setCurrentVersionNumber(doc.getCurrentVersionNumber());
            websiteDoc.setDownloadPath("/data/" + doc.getId() + "/latest");
            websiteDoc.setUploadPath("/data/" + doc.getId());
            websiteDoc.setProjectId(doc.getProjectId());

            return websiteDoc;
        } else {
            throw new Exception("No such document");
        }
    }

    @Override
	public WebsiteDocumentVersion[] listDocumentVersions(String id) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		List versions = EJBLocator.lookupStorageBean().listVersions(ticket, id);
		WebsiteDocumentVersion[] results = new WebsiteDocumentVersion[versions.size()];
		for (int i = 0; i < versions.size(); i++) {
			DocumentVersion docVersion = (DocumentVersion) versions.get(i);

			WebsiteDocumentVersion version = new WebsiteDocumentVersion();
			version.setId(docVersion.getId());
			version.setDocumentRecordId(docVersion.getDocumentRecordId());
			version.setComments(docVersion.getComments());
			version.setUserId(docVersion.getUserId());
			version.setVersionNumber(docVersion.getVersionNumber());
			version.setSize(docVersion.getSize());
			version.setTimestamp(docVersion.getTimestamp());
			version.setDownloadPath("/data/" + docVersion.getDocumentRecordId() + "/" + docVersion.getId());

			results[i] = version;
		}
		return results;
	}

    @Override
	public WebsiteDocumentVersion getDocumentVersion(String id) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		DocumentVersion docVersion = EJBLocator.lookupStorageBean().getVersion(ticket, id);

		WebsiteDocumentVersion version = new WebsiteDocumentVersion();
		version.setId(docVersion.getId());
		version.setDocumentRecordId(docVersion.getDocumentRecordId());
		version.setComments(docVersion.getComments());
		version.setUserId(docVersion.getUserId());
		version.setVersionNumber(docVersion.getVersionNumber());
		version.setSize(docVersion.getSize());
		version.setTimestamp(docVersion.getTimestamp());
		version.setDownloadPath("/data/" + docVersion.getDocumentRecordId() + "/" + docVersion.getId());

		return version;
	}

    @Override
	public WebsiteDocumentVersion getLatestDocumentVersion(String documentId) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
		if (doc != null) {
			DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, documentId);

			if (version != null) {
				WebsiteDocumentVersion docVersion = new WebsiteDocumentVersion();
				docVersion.setId(version.getId());
				docVersion.setDocumentRecordId(version.getDocumentRecordId());
				docVersion.setComments(version.getComments());
				docVersion.setUserId(version.getUserId());
				docVersion.setVersionNumber(version.getVersionNumber());
				docVersion.setSize(version.getSize());
				docVersion.setTimestamp(version.getTimestamp());
				docVersion.setDownloadPath("/data/" + docVersion.getDocumentRecordId() + "/" + docVersion.getId());

				return docVersion;
			} else {
				throw new Exception("Document has no versions");
			}
		} else {
			throw new Exception("No such document");
		}
	}

    @Override
    public Response uploadVersion(String documentId) throws ConnexienceException {

        try {

            final Ticket ticket = TransitionSessionUtils.getTicket(request);

            ServerObject docObject = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, documentId, ServerObject.class);
            DocumentVersion version = null;
            UserTransaction tx = null;
            long contentLength = Long.parseLong(request.getHeader("Content-Length"));

            if(docObject instanceof DynamicWorkflowService){
                try {
                    // Workflow service upload
                    tx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                    tx.begin();
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    DocumentVersion v = StorageUtils.upload(ticket, request.getInputStream(), contentLength, doc, "Uploaded service");
                    WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceXml(ticket, documentId, v.getId());

                    //log that the library has been saved in the graphDb
                    DynamicWorkflowService service = (DynamicWorkflowService) doc;
                    DocumentVersion serviceVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, service.getId());
                    ServiceSaveOperation saveOp = new ServiceSaveOperation(service.getId(), serviceVersion.getId(),serviceVersion.getVersionNumber(), service.getName(),  ticket.getUserId(), new Date(System.currentTimeMillis()));
                    saveOp.setProjectId(ticket.getDefaultProjectId());
                    ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                    provClient.log(saveOp);
                    tx.commit();

                    PrintWriter writer = response.getWriter();
                    writer.println(v.getId());
                    writer.flush();

                } catch (Exception e){
                    if(tx!=null){
                        tx.rollback();
                    }
                    throw new Exception("Error storing document: " + e.getMessage(), e);
                }

            } else if(docObject instanceof DynamicWorkflowLibrary){
                // Workflow library upload
                try {
                    tx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                    tx.begin();
                    DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
                    DocumentVersion v = StorageUtils.upload(ticket, request.getInputStream(), contentLength, doc, "Uploaded library");
                    WorkflowEJBLocator.lookupWorkflowManagementBean().updateLibraryXml(ticket, documentId, v.getId());

                    //log that the library has been saved in the graphDb
                    DynamicWorkflowLibrary library = (DynamicWorkflowLibrary) doc;
                    DocumentVersion libVersion = EJBLocator.lookupStorageBean().getLatestVersion(ticket, library.getId());
                    LibrarySaveOperation saveOp = new LibrarySaveOperation(library.getId(), libVersion.getId(), library.getName(), libVersion.getVersionNumber(), ticket.getUserId(), new Date(System.currentTimeMillis()));
                    saveOp.setProjectId(ticket.getDefaultProjectId());
                    ProvenanceLoggerClient provClient = new ProvenanceLoggerClient();
                    provClient.log(saveOp);

                    tx.commit();

                    PrintWriter writer = response.getWriter();
                    writer.println(v.getId());
                    writer.flush();

                } catch (Exception e){
                    if(tx!=null){
                        tx.rollback();
                    }
                    throw new Exception("Error storing document: " + e.getMessage(), e);
                }

            } else if(docObject instanceof DocumentRecord){
                // Plain document last
                DocumentRecord doc = (DocumentRecord)docObject;
                try {
                    tx = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

                    // Default to an hour for the transaction
                    int transactionTimeout = 3600;

                    // Do some content-length based calculations assuming 500kb/second
                    long estimatedUploadTime = contentLength / 500000L;
                    if(estimatedUploadTime>transactionTimeout){
                        tx.setTransactionTimeout((int)estimatedUploadTime);
                    } else {
                        tx.setTransactionTimeout(transactionTimeout);
                    }

                    tx.begin();
                    version = StorageUtils.upload(ticket, request.getInputStream(), contentLength,  doc, "Uploaded by API");

                    //re-get the document so that we can set the project id
                    doc = (DocumentRecord) EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, doc.getId(), DocumentRecord.class);
                    if(doc.getProjectId()!=null && !doc.getProjectId().isEmpty()){
                        EJBLocator.lookupStorageBean().checkProjectId(ticket, doc, doc.getProjectId());

                    } else if(request.getHeader("ProjectID")!=null && !request.getHeader("ProjectID").isEmpty()){
                        EJBLocator.lookupStorageBean().checkProjectId(ticket, doc, request.getHeader("ProjectID"));

                    }

                    tx.commit();
                } catch (Exception e){
                    if(tx!=null){
                        tx.rollback();
                    }
                    throw new Exception("Error storing document: " + e.getMessage(), e);
                }

                PrintWriter writer = response.getWriter();
                writer.println(version.getId());
                writer.flush();

                // Run workflow triggers
                WorkflowEJBLocator.lookupWorkflowManagementBean().runTriggersForDocument(ticket, doc);

            } else {
                if(!response.isCommitted()){
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } catch (Exception e){
            if(!response.isCommitted()){
                try {
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error sending data: " + e.getMessage());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
	public WebsiteMetadataItem[] getDocumentMetadata(String id) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		List results = EJBLocator.lookupMetaDataBean().getObjectMetadata(ticket, id);
		WebsiteMetadataItem[] metadata = new WebsiteMetadataItem[results.size()];

		for (int i = 0; i < results.size(); i++) {
			MetadataItem md = (MetadataItem) results.get(i);

			WebsiteMetadataItem metadataItem = new WebsiteMetadataItem();
			metadataItem.setObjectId(md.getObjectId());
			metadataItem.setName(md.getName());
			metadataItem.setCategory(md.getCategory());
			metadataItem.setStringValue(md.getStringValue());
			metadataItem.setId(String.valueOf(md.getId()));

			if (md instanceof TextMetadata) {
				metadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
			} else if (md instanceof BooleanMetadata) {
				metadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.BOOLEAN);
			} else if (md instanceof NumericalMetadata) {
				metadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.NUMERICAL);
			} else if (md instanceof DateMetadata) {
				metadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.DATE);
			} else {
				metadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
			}

			metadata[i] = metadataItem;
		}

		return metadata;
	}

    @Override
	public WebsiteMetadataItem addMetadataToDocument(String id, WebsiteMetadataItem metadataItem) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		MetadataItem md;

		switch (metadataItem.getMetadataType()) {
			case BOOLEAN:
				md = new BooleanMetadata();
				((BooleanMetadata) md).setBooleanValue(Boolean.parseBoolean(metadataItem.getStringValue()));
				break;

			case DATE:

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

				md = new DateMetadata();
				((DateMetadata) md).setDateValue(dateFormat.parse(metadataItem.getStringValue()));
				break;

			case NUMERICAL:
				md = new NumericalMetadata();
				((NumericalMetadata) md).setDoubleValue(Double.parseDouble(metadataItem.getStringValue()));
				break;

			case TEXT:
				md = new TextMetadata();
				((TextMetadata) md).setTextValue(metadataItem.getStringValue());
				break;

			default:
				md = new TextMetadata();
				((TextMetadata) md).setTextValue(metadataItem.getStringValue());
				break;
		}

		md.setCategory(metadataItem.getCategory());
		md.setName(metadataItem.getName());
		md.setObjectId(metadataItem.getObjectId());
		md.setUserId(ticket.getUserId());
		md = EJBLocator.lookupMetaDataBean().addMetadata(ticket, id, md);

		WebsiteMetadataItem websiteMetaData = new WebsiteMetadataItem();
		websiteMetaData.setObjectId(md.getObjectId());
		websiteMetaData.setName(md.getName());
		websiteMetaData.setCategory(md.getCategory());
		websiteMetaData.setStringValue(md.getStringValue());
		websiteMetaData.setId(String.valueOf(md.getId()));

		if (md instanceof TextMetadata) {
			websiteMetaData.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
		} else if (md instanceof BooleanMetadata) {
			websiteMetaData.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.BOOLEAN);
		} else if (md instanceof NumericalMetadata) {
			websiteMetaData.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.NUMERICAL);
		} else if (md instanceof DateMetadata) {
			websiteMetaData.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.DATE);
		} else {
			websiteMetaData.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
		}

		return websiteMetaData;
	}

    private String getObjectId(String targetParam) {
        int location = targetParam.indexOf("_");
        if (location != -1) {
            return targetParam.substring(location + 1, targetParam.length());
        } else {
            return targetParam;
        }
    }

    private String getVolumeId(String targetParam) {
        int location = targetParam.indexOf("_");
        if (location != -1) {
            return targetParam.substring(0, location);
        } else {
            return targetParam;
        }
    }

    private boolean isSearchFolder(Ticket ticket, String folderId) throws Exception {
        Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, folderId);
        if (f instanceof SearchFolder) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.project.LoggersRemote;
import com.connexience.server.ejb.project.StudyRemote;
import com.connexience.server.ejb.project.SubjectsRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.model.project.FileType;
import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerConfiguration;
import com.connexience.server.model.project.study.LoggerDeployment;
import com.connexience.server.model.project.study.LoggerType;
import com.connexience.server.model.project.study.Sensor;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.rest.api.ILoggerAPI;
import com.connexience.server.rest.model.project.WebsiteConversionWorkflow;
import com.connexience.server.rest.model.project.WebsiteFileType;
import com.connexience.server.rest.model.project.study.*;
import com.connexience.server.rest.util.TransitionSessionUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created with IntelliJ IDEA. User: nmdt3 Date: 27/08/2013 Time: 20:41
 */
@SuppressWarnings("unchecked")
@Path("/rest/logger")
@Stateless
public class LoggerAPI implements ILoggerAPI {
    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    StorageRemote storageBean;

    @EJB(lookup = "java:global/esc/server-beans/LoggersBean")
    LoggersRemote loggerBean;

    @EJB(lookup = "java:global/esc/server-beans/StudyBean")
    StudyRemote studyBean;

    @EJB(lookup = "java:global/esc/server-beans/SubjectsBean")
    SubjectsRemote subjectBean;

    @PersistenceContext
    private EntityManager em;

    @Override
    public Collection<WebsiteFileType> getLoggerFileTypes(@FormParam("start") int start, @FormParam("size") int size) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Collection<FileType> fileTypes = loggerBean.getFileTypes(ticket, start, size);

        Collection<WebsiteFileType> websiteFileTypes = new ArrayList<>();

        for (FileType fileType : fileTypes) {
            websiteFileTypes.add(new WebsiteFileType(em.merge(fileType)));
        }

        return websiteFileTypes;
    }

    @Override
    public WebsiteFileType saveLoggerFileType(WebsiteFileType websiteFileType) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (websiteFileType.getId() != null) {
            FileType fileType = em.merge(loggerBean.getFileType(ticket, websiteFileType.getId()));

            fileType.setId(websiteFileType.getId());
            fileType.setName(websiteFileType.getName());
            fileType.setDescription(websiteFileType.getDescription());

            for (String workflowID : websiteFileType.getConversionWorkflows()) {
                fileType.addConversionWorkflow(loggerBean.createConversionWorkflow(ticket, fileType.getId(), workflowID));
            }

            return new WebsiteFileType(em.merge(fileType));
        } else {
            FileType fileType = loggerBean.createFileType(ticket, websiteFileType.getName());
            fileType.setDescription(websiteFileType.getDescription());

            for (String workflowID : websiteFileType.getConversionWorkflows()) {
                fileType.addConversionWorkflow(loggerBean.createConversionWorkflow(ticket, fileType.getId(), workflowID));
            }

            return new WebsiteFileType(em.merge(loggerBean.saveFileType(ticket, fileType)));
        }
    }

    @Override
    public Response removeLoggerFileType(@PathParam("typeId") int typeId) {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            loggerBean.deleteFileType(ticket, typeId);
        } catch (ConnexienceException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Collection<WebsiteConversionWorkflow> getConversionWorkflows() throws ConnexienceException {
        return null;
    }

    @Override
    public WebsiteConversionWorkflow saveConversionWorkflows(WebsiteConversionWorkflow websiteConversionWorkflow) throws ConnexienceException {
        return null;
    }

    @Override
    public Collection<WebsiteLoggerTypeMin> getLoggerTypes(int start, int end) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Collection<LoggerType> loggerTypes = loggerBean.getLoggerTypes(ticket, start, end);

        List<WebsiteLoggerTypeMin> websiteLoggerTypes = new ArrayList<>();

        for (LoggerType loggerType : loggerTypes) {
            websiteLoggerTypes.add(new WebsiteLoggerTypeMin(em.merge(loggerType)));
        }

        return websiteLoggerTypes;
    }

    @Override
    public WebsiteLoggerType saveLoggerType(WebsiteLoggerType websiteLoggerType) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (websiteLoggerType.getId() != null) {
            LoggerType loggerType = em.merge(loggerBean.getLoggerType(ticket, websiteLoggerType.getId()));

            loggerType.setManufacturer(websiteLoggerType.getManufacturer());
            loggerType.setName(websiteLoggerType.getName());
            loggerType.setPhysicalDevice(websiteLoggerType.isPhysicalDevice());

            for (WebsiteSensor websiteSensor : websiteLoggerType.getSensors()) {
                if (websiteSensor.getId() != null) {
                    Sensor sensor = em.merge(loggerBean.getSensor(ticket, websiteSensor.getId()));
                    sensor.setName(websiteSensor.getName());
                    sensor.setFileType(em.merge(loggerBean.getFileType(ticket, websiteSensor.getFileType().getId())));

                    loggerType.addSensor(sensor);
                } else {
                    Sensor sensor = em.merge(loggerBean.createSensor(ticket, loggerType.getId(), websiteSensor.getName()));
                    sensor = em.merge(loggerBean.addFileTypeToSensor(ticket, sensor.getId(), websiteSensor.getFileType().getId()));

                    loggerType.addSensor(sensor);
                }
            }

            return new WebsiteLoggerType(em.merge(loggerBean.saveLoggerType(ticket, loggerType)));
        } else {
            LoggerType loggerType = em.merge(loggerBean.createLoggerType(ticket, websiteLoggerType.getManufacturer(), websiteLoggerType.getName()));

            for (WebsiteSensor websiteSensor : websiteLoggerType.getSensors()) {
                if (websiteSensor.getId() != null) {
                    Sensor sensor = em.merge(loggerBean.getSensor(ticket, websiteSensor.getId()));
                    sensor.setName(websiteSensor.getName());
                    sensor.setFileType(em.merge(loggerBean.getFileType(ticket, websiteSensor.getFileType().getId())));

                    loggerType.addSensor(sensor);
                } else {
                    Sensor sensor = em.merge(loggerBean.createSensor(ticket, loggerType.getId(), websiteSensor.getName()));
                    sensor = em.merge(loggerBean.addFileTypeToSensor(ticket, sensor.getId(), websiteSensor.getFileType().getId()));

                    loggerType.addSensor(sensor);
                }
            }

            return new WebsiteLoggerType(em.merge(loggerBean.saveLoggerType(ticket, loggerType)));
        }
    }

    @Override
    public Response deleteLoggerType(@PathParam("typeId") int typeId) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            loggerBean.deleteLoggerType(ticket, typeId);
        } catch (ConnexienceException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Collection<WebsiteSensor> getLoggerTypeSensors(@PathParam("typeId") String typeId) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);
        final Collection<Sensor> sensors = em.merge(loggerBean.getLoggerType(ticket, Integer.parseInt(typeId))).getSensors();

        final Collection<WebsiteSensor> websiteSensors = new ArrayList<>();

        for (final Sensor sensor : sensors) {
            websiteSensors.add(new WebsiteSensor(em.merge(sensor)));
        }

        return websiteSensors;
    }

    @Override
    public WebsiteSensor saveLoggerTypeSensor(@PathParam("typeId") String typeId, WebsiteSensor websiteSensor) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        LoggerType loggerType = em.merge(loggerBean.getLoggerType(ticket, Integer.parseInt(typeId)));

        if (websiteSensor.getId() != null) {
            Sensor sensor = em.merge(loggerBean.getSensor(ticket, websiteSensor.getId()));
            sensor.setName(websiteSensor.getName());
            sensor.setFileType(em.merge(loggerBean.getFileType(ticket, websiteSensor.getFileType().getId())));

            loggerType.addSensor(sensor);
        } else {
            Sensor sensor = em.merge(loggerBean.createSensor(ticket, loggerType.getId(), websiteSensor.getName()));
            sensor = em.merge(loggerBean.addFileTypeToSensor(ticket, sensor.getId(), websiteSensor.getFileType().getId()));

            loggerType.addSensor(sensor);
        }

        return websiteSensor;
    }

    @Override
    public Response removeLoggerTypeSensor(@PathParam("typeId") String typeId, @PathParam("sensorId") int sensorId) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        LoggerType loggerType = em.merge(loggerBean.getLoggerType(ticket, Integer.parseInt(typeId)));

        loggerType.removeSensor(em.merge(loggerBean.getSensor(ticket, sensorId)));

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Collection<WebsiteLogger> getLoggers(@FormParam("start") int start, @FormParam("end") int end) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        final Collection<Logger> loggers = loggerBean.getLoggers(ticket, start, end);

        final Collection<WebsiteLogger> websiteLoggers = new ArrayList<>();

        for (final Logger logger : loggers) {
            websiteLoggers.add(new WebsiteLogger(em.merge(logger)));
        }

        return websiteLoggers;
    }

    @Override
    public Collection<WebsiteLoggerMin> getAllLoggersOfType(@PathParam("typeId") int typeId) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        Collection<Logger> loggers = loggerBean.getLoggersByType(ticket, typeId);

        final Collection<WebsiteLoggerMin> websiteLoggers = new ArrayList<>();

        for (final Logger logger : loggers) {
            websiteLoggers.add(new WebsiteLoggerMin(em.merge(logger)));
        }

        return websiteLoggers;
    }

    @Override
    public Collection<WebsiteLoggerMin> getUndeployedLoggersOfType(@PathParam("typeId") int typeId, @FormParam("startDate") long startDate, @FormParam("endDate") long endDate) throws ConnexienceException {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        Collection<Logger> loggers = loggerBean.getAvailableLoggersByType(ticket, typeId, new Date(startDate), new Date(endDate));

        final Collection<WebsiteLoggerMin> websiteLoggers = new ArrayList<>();

        for (final Logger logger : loggers) {
            websiteLoggers.add(new WebsiteLoggerMin(em.merge(logger)));
        }

        return websiteLoggers;
    }

    @Override
    public ArrayList<ArrayList<Integer>> getDeployedLoggersCountOfType(@QueryParam("typeIdList") ArrayList<Integer> typeIdList, @QueryParam("startDate") long startDate, @QueryParam("startDate") long endDate) throws ConnexienceException {
        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        ArrayList<ArrayList<Integer>> loggerTypeCounts = new ArrayList<>();

        for (final Integer loggerType : typeIdList) {

            Integer totalLoggersOfType = loggerBean.getLoggerCount(ticket, loggerType).intValue();

            ArrayList<Integer> item = new ArrayList<>();
            item.add(loggerType);
            item.add(totalLoggersOfType - loggerBean.getDeployedLoggersCountByType(ticket, loggerType, new Date(startDate), new Date(endDate)));
            item.add(totalLoggersOfType);
            loggerTypeCounts.add(item);
        }

        return loggerTypeCounts;
    }

    @Override
    public WebsiteLogger getLogger(@PathParam("loggerId") int loggerId) throws ConnexienceException {
        return null;
    }

    @Override
    public WebsiteLogger saveLogger(@PathParam("typeId") int typeId, WebsiteLogger websiteLogger) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (websiteLogger.getId() != null) {
            Logger logger = em.merge(loggerBean.getLogger(ticket, websiteLogger.getId()));

            logger.setLocation(websiteLogger.getLocation());
            logger.setLoggerType(em.merge(loggerBean.getLoggerType(ticket, typeId)));
            logger.setSerialNumber(websiteLogger.getSerialNumber());
            logger.setAdditionalProperties(websiteLogger.getAdditionalProperties());

            return new WebsiteLogger(em.merge(loggerBean.saveLogger(ticket, logger)));
        } else {
            Logger newLogger = em.merge(loggerBean.createLogger(ticket, em.merge(loggerBean.getLoggerType(ticket, typeId)).getId(), websiteLogger.getSerialNumber()));
            newLogger.setLocation(websiteLogger.getLocation());
            newLogger.setAdditionalProperties(websiteLogger.getAdditionalProperties());

            em.merge(loggerBean.saveLogger(ticket, newLogger));

            return new WebsiteLogger(newLogger);
        }
    }

    @Override
    public Response saveLoggers(@PathParam("typeId") int typeId, @Context HttpServletRequest request) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (request != null) {
            StringBuilder files = new StringBuilder();
            String username = null;
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List items = upload.parseRequest(request);
                Iterator iter = items.iterator();

                ArrayList<String> fileNames = new ArrayList<>();
                ArrayList<Long> sizes = new ArrayList<>();

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (item.isFormField()) {
                        if ("uname".equals(item.getFieldName())) {
                            username = item.getString();
                        }
                    } else {
                        if (!item.isFormField()) {
                            fileNames.add(item.getName());
                            sizes.add(item.getSize());
                        }
                    }
                }

                System.out.println(fileNames.size());
            } catch (Exception ex) {
                ex.printStackTrace();

                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }

        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response deleteLogger(@PathParam("typeId") int typeId, @PathParam("loggerId") int loggerId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        loggerBean.deleteLogger(ticket, loggerId);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Collection<WebsiteLoggerConfigurationMin> getAllConfigurations() throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteLoggerConfigurationMin> websiteConfigurations = new ArrayList<>();

        for (LoggerConfiguration configuration : loggerBean.getLoggerConfigurations(ticket, 0, Integer.MAX_VALUE)) {
            websiteConfigurations.add(new WebsiteLoggerConfigurationMin(configuration));
        }

        return websiteConfigurations;
    }

    @Override
    public Collection<WebsiteLoggerConfigurationMin> getLoggerConfigurations(@PathParam("typeId") int typeId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteLoggerConfigurationMin> websiteConfigurations = new ArrayList<>();

        for (LoggerConfiguration configuration : loggerBean.getLoggerConfigurationsByType(ticket, typeId)) {

            //needed to lazily load sensor list on logger type property
            configuration = em.merge(configuration);
            LoggerType t = configuration.getLoggerType();
            t = em.merge(t);

            //convert Configuration to WebsiteConfiguration
            websiteConfigurations.add(new WebsiteLoggerConfigurationMin(configuration));
        }

        return websiteConfigurations;
    }

    @Override
    public WebsiteLoggerConfiguration getLoggerConfiguration(@PathParam("loggerConfigurationId") int loggerConfigurationId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        return new WebsiteLoggerConfiguration(loggerBean.getLoggerConfiguration(ticket, loggerConfigurationId));
    }

    @Override
    public WebsiteLoggerConfiguration saveLoggerConfiguration(@PathParam("loggerTypeId") int loggerTypeId, WebsiteLoggerConfiguration websiteLoggerConfiguration) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (websiteLoggerConfiguration.getId() != null) {
            LoggerConfiguration configuration = em.merge(loggerBean.getLoggerConfiguration(ticket, websiteLoggerConfiguration.getId()));
            configuration.setName(websiteLoggerConfiguration.getName());
            configuration.setDescription(websiteLoggerConfiguration.getDescription());
            configuration.setSampleFrequency(websiteLoggerConfiguration.getSampleFrequency());
            configuration.setApplication(websiteLoggerConfiguration.getApplication());
            configuration.setMinimumFirmwareVersion(websiteLoggerConfiguration.getMinimumFirmwareVersion());
            configuration.setFirmwareLocation(websiteLoggerConfiguration.getFirmwareLocation());
            configuration.setLoggerType(em.merge(loggerBean.getLoggerType(ticket, loggerTypeId)));
            configuration.setAdditionalProperties(websiteLoggerConfiguration.getAdditionalProperties());

            return new WebsiteLoggerConfiguration(em.merge(loggerBean.saveLoggerConfiguration(ticket, configuration)));
        } else {
            LoggerConfiguration configuration = new LoggerConfiguration(websiteLoggerConfiguration.getName());
            configuration.setDescription(websiteLoggerConfiguration.getDescription());
            configuration.setSampleFrequency(websiteLoggerConfiguration.getSampleFrequency());
            configuration.setApplication(websiteLoggerConfiguration.getApplication());
            configuration.setMinimumFirmwareVersion(websiteLoggerConfiguration.getMinimumFirmwareVersion());
            configuration.setFirmwareLocation(websiteLoggerConfiguration.getFirmwareLocation());
            configuration.setLoggerType(em.merge(loggerBean.getLoggerType(ticket, loggerTypeId)));
            configuration.setAdditionalProperties(websiteLoggerConfiguration.getAdditionalProperties());

            return new WebsiteLoggerConfiguration(em.merge(loggerBean.saveLoggerConfiguration(ticket, configuration)));
        }
    }

    @Override
    public Response removeLoggerConfiguration(@PathParam("configurationId") int configurationId) {

        final Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            loggerBean.deleteLoggerConfiguration(ticket, configurationId);
        } catch (ConnexienceException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public WebsiteLoggerDeployment getLoggerDeployment(@PathParam("deploymentId") int deploymentId) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        return (new WebsiteLoggerDeployment(em.merge(loggerBean.getLoggerDeployment(ticket, deploymentId))));
    }

    @Override
    public Collection<WebsiteLoggerDeploymentMin> getLoggerDeployments(@PathParam("studyId") int studyId) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        final Collection<WebsiteLoggerDeploymentMin> deployments = new ArrayList<>();

        for (LoggerDeployment deployment : loggerBean.getLoggerDeploymentsByStudy(ticket, studyId, true)) {
            deployments.add(new WebsiteLoggerDeploymentMin(em.merge(deployment)));
        }

        return deployments;
    }

    @Override
    public Collection<WebsiteLoggerDeployment> getSubjectGroupLoggerDeployments(@PathParam("studyId") int studyId, @PathParam("groupId") int groupId, @DefaultValue("false") @QueryParam("includeInactive") Boolean includeInactive) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        final Collection<WebsiteLoggerDeployment> deployments = new ArrayList<>();

        for (LoggerDeployment deployment : em.merge(subjectBean.getSubjectGroup(ticket, groupId)).getLoggerDeployments()) {

            if (includeInactive) {
                deployments.add(new WebsiteLoggerDeployment(em.merge(deployment)));
            } else if (deployment.isActive()) {
                deployments.add(new WebsiteLoggerDeployment(em.merge(deployment)));
            }
        }

        return deployments;
    }

    @Override
    public WebsiteLoggerDeployment saveLoggerDeployment(@PathParam("studyId") int studyId, @PathParam("subjectGroupId") int subjectGroupId, WebsiteLoggerDeployment websiteLoggerDeployment) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        LoggerDeployment deployment = null;

        Integer deploymentId = websiteLoggerDeployment.getId();

        if (websiteLoggerDeployment.getId() != null) {
            deployment = em.merge(loggerBean.updateLoggerDeploymentAdditionalProperties(ticket, websiteLoggerDeployment.getId(), websiteLoggerDeployment.getAdditionalProperties()));
        } else {
            deployment = em.merge(loggerBean.createLoggerDeployment(ticket, studyId, websiteLoggerDeployment.getLogger().getId(), websiteLoggerDeployment.getLoggerConfigurationId(), subjectGroupId));
        }

        System.out.println(deployment);

        return new WebsiteLoggerDeployment(deployment);
    }

    @Override
    public Response removeLoggerDeployment(@PathParam("deploymentId") int deploymentId) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        LoggerDeployment deployment = em.merge(loggerBean.getLoggerDeployment(ticket, deploymentId));

        deployment.setActive(false);

        loggerBean.saveLoggerDeployment(ticket, deployment);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Collection<WebsiteLoggerDeploymentMin> getLoggerDeploymentsByLoggerId(@PathParam("loggerId") int loggerId) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        final Collection<WebsiteLoggerDeploymentMin> deployments = new ArrayList<>();

        Collection<LoggerDeployment> dbDeployments = loggerBean.getLoggerDeploymentsByLoggerId(ticket, loggerId);

        if (dbDeployments != null){
            for (LoggerDeployment deployment : dbDeployments) {
                deployments.add(new WebsiteLoggerDeploymentMin(em.merge(deployment)));
            }
        }


        return deployments;
    }

}

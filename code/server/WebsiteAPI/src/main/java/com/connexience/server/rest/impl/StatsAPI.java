/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.workflow.WorkflowManagementRemote;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.rest.FileTypeStats;
import com.connexience.server.rest.QuotaStats;
import com.connexience.server.rest.WorkflowStats;
import com.connexience.server.rest.api.IStatsAPI;
import com.connexience.server.rest.util.TransitionSessionUtils;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.HashMap;

/**
 * User: nsjw7 Date: 28/05/2013 Time: 10:20
 * <p/>
 * To access lazily fetched members use
 */
@SuppressWarnings("unchecked")
@Path("/rest/stats")
public class StatsAPI implements IStatsAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@EJB(lookup = "java:global/esc/server-beans/WorkflowManagementBean")
	WorkflowManagementRemote wfManagementBean;

	@Override
	public WorkflowStats getWorkflowStats() throws ConnexienceException {

		Ticket ticket = TransitionSessionUtils.getTicket(request);
		Organisation org = TransitionSessionUtils.getOrganisation(request);

		//don't do anything for the public user
		if (org.getDefaultUserId().equals(ticket.getUserId())) {
			return new WorkflowStats();
		} else {
			HashMap<String, Long> stats = wfManagementBean.getWorkflowStats(ticket);
			WorkflowStats websitestats = new WorkflowStats();

			for (String key : stats.keySet()) {
				if (key.equals("Success")) {
					websitestats.setSuccess(stats.get(key));
				} else if (key.equals("Failed")) {
					websitestats.setFailed(stats.get(key));
				} else if (key.equals("Running")) {
					websitestats.setRunning(stats.get(key));
				} else if (key.equals("Waiting")) {
					websitestats.setWaiting(stats.get(key));
				}
			}
			return websitestats;
		}
	}

	@Override
	public QuotaStats getQuotaStats() throws ConnexienceException {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		Organisation org = TransitionSessionUtils.getOrganisation(request);

		//don't do anything for the public user
		if (org.getDefaultUserId().equals(ticket.getUserId())) {
			return new QuotaStats();
		} else {
			//convert from bytes to MB
			long dataAvailable = EJBLocator.lookupQuotaBean().getAvailableStorageQuota(ticket, ticket.getUserId()) / 1048576;

			if (dataAvailable < 0) {
				dataAvailable = 0;
			}

			long dataUsed = EJBLocator.lookupQuotaBean().getStorageQuotaUsed(ticket, ticket.getUserId()) / 1048576;

			QuotaStats stats = new QuotaStats();
			stats.setFree(dataAvailable);
			stats.setUsed(dataUsed);

			return stats;
		}
	}

	@Override
	public FileTypeStats getFileTypes() throws ConnexienceException {
		Ticket ticket = TransitionSessionUtils.getTicket(request);
		Organisation org = TransitionSessionUtils.getOrganisation(request);

		//don't do anything for the public user
		if (org.getDefaultUserId().equals(ticket.getUserId())) {
			return new FileTypeStats();
		} else {
			HashMap<String, Long> fileTypes = EJBLocator.lookupStorageBean().getFileTypesForUser(ticket);

			FileTypeStats stats = new FileTypeStats();
			stats.setFiletypes(fileTypes);
			return stats;
		}
	}
}

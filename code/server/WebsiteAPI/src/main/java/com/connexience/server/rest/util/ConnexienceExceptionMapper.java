/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.util;

import com.connexience.server.ConnexienceException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * User: nsjw7 Date: 30/05/2013 Time: 13:12
 */
@Provider
public class ConnexienceExceptionMapper implements ExceptionMapper<ConnexienceException> {
	public Response toResponse(ConnexienceException exception) {
		// TODO: JSON escape this string
		final String response = String.format("{error:'%s'}", exception.getMessage());

		return Response.status(500).entity(response).type(MediaType.APPLICATION_JSON_TYPE).build();
	}
}

/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;


import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.rest.api.IFoldersAPI;
import com.connexience.server.rest.model.*;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.ZipFileCreator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;

@SuppressWarnings("unchecked")
@Path("/rest/folders")
public class FoldersAPI implements IFoldersAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

    @Override
    public WebsiteFolder[] listChildFolders(String id) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List folders = EJBLocator.lookupStorageBean().getChildFolders(ticket, id);
        WebsiteFolder[] results = new WebsiteFolder[folders.size()];
        for (int i = 0; i < folders.size(); i++) {

            Folder folder = (Folder) folders.get(i);

            WebsiteFolder websiteFolder = new WebsiteFolder();
            websiteFolder.setId(folder.getId());
            websiteFolder.setName(folder.getName());
            websiteFolder.setDescription(folder.getDescription());
            websiteFolder.setCreatorId(folder.getCreatorId());
            websiteFolder.setContainerId(folder.getContainerId());
            websiteFolder.setProjectId(folder.getProjectId());

            results[i] = websiteFolder;
        }

        return results;
    }

    @Override
    public WebsiteDocument[] folderDocuments(String id) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List docs = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, id);
        WebsiteDocument[] results = new WebsiteDocument[docs.size()];
        int count = 0;
        for (Object doc : docs) {
            results[count] = new WebsiteDocument((DocumentRecord) doc, ticket, id);
            count++;
        }

        return results;
    }

    @Override
    public WebsiteFolder getFolder(String id) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Folder folder = EJBLocator.lookupStorageBean().getFolder(ticket, id);

        WebsiteFolder websiteFolder = new WebsiteFolder();
        websiteFolder.setId(folder.getId());
        websiteFolder.setName(folder.getName());
        websiteFolder.setDescription(folder.getDescription());
        websiteFolder.setCreatorId(folder.getCreatorId());
        websiteFolder.setContainerId(folder.getContainerId());
        websiteFolder.setProjectId(folder.getProjectId());

        return websiteFolder;
    }

    @Override
    public WebsiteFolder homeFolder() throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        Folder folder = EJBLocator.lookupStorageBean().getHomeFolder(ticket, ticket.getUserId());

        WebsiteFolder websiteFolder = new WebsiteFolder();
        websiteFolder.setId(folder.getId());
        websiteFolder.setName(folder.getName());
        websiteFolder.setDescription(folder.getDescription());
        websiteFolder.setCreatorId(folder.getCreatorId());
        websiteFolder.setContainerId(folder.getContainerId());
        websiteFolder.setProjectId(folder.getProjectId());

        return websiteFolder;
    }

    @Override
	public WebsiteFolder createChildFolder(String id, String name) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		Folder parent = EJBLocator.lookupStorageBean().getFolder(ticket, id);

		if (parent != null) {
			Folder child = new Folder();
			child.setName(name);
			child.setContainerId(parent.getId());
			child = EJBLocator.lookupStorageBean().addChildFolder(ticket, id, child);

			WebsiteFolder websiteFolder = new WebsiteFolder();
			websiteFolder.setId(child.getId());
			websiteFolder.setName(child.getName());
			websiteFolder.setDescription(child.getDescription());
			websiteFolder.setCreatorId(child.getCreatorId());
			websiteFolder.setContainerId(child.getContainerId());
			websiteFolder.setProjectId(child.getProjectId());

			return websiteFolder;
		} else {
			throw new Exception("No such parent folder");
		}
	}

    @Override
	public WebsiteFolder updateFolder(String folderId, WebsiteFolder folder) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		Folder existing = EJBLocator.lookupStorageBean().getFolder(ticket, folder.getId());

		if (existing != null) {
			existing.setName(folder.getName());
			existing.setDescription(folder.getDescription());
			existing.setContainerId(folder.getContainerId());
			existing = EJBLocator.lookupStorageBean().updateFolder(ticket, existing);

			WebsiteFolder websiteFolder = new WebsiteFolder();
			websiteFolder.setId(existing.getId());
			websiteFolder.setName(existing.getName());
			websiteFolder.setDescription(existing.getDescription());
			websiteFolder.setCreatorId(existing.getCreatorId());
			websiteFolder.setContainerId(existing.getContainerId());
			websiteFolder.setProjectId(existing.getProjectId());

			return websiteFolder;
		} else {
			throw new Exception("No such folder");
		}
	}

    @Override
    public Response archiveFolder(@PathParam("folderIds") String[] folderIds) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (folderIds != null && folderIds.length > 0) {
            ZipFileCreator zipper = new ZipFileCreator(ticket);
            for (int i = 0; i < folderIds.length; i++) {
                zipper.addTopLevelItem(getObjectId(folderIds[i]));
            }

            String fileName = EJBLocator.lookupObjectInfoBean().getObjectName(ticket, getObjectId(folderIds[0])) + ".zip";
            String targetFolderId = getVolumeId(folderIds[0]);

            // Begin the zip process
            DocumentRecord doc = zipper.compressData(targetFolderId, fileName);

        } else {
            throw new Exception("No targets specified in archive");
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
	public void deleteFolder(String folderId) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		EJBLocator.lookupStorageBean().removeFolderTree(ticket, folderId);
	}

    private String getObjectId(String targetParam) {
        int location = targetParam.indexOf("_");
        if (location != -1) {
            return targetParam.substring(location + 1, targetParam.length());
        } else {
            return targetParam;
        }
    }

    private String getVolumeId(String targetParam) {
        int location = targetParam.indexOf("_");
        if (location != -1) {
            return targetParam.substring(0, location);
        } else {
            return targetParam;
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.project.ProjectsRemote;
import com.connexience.server.ejb.project.StudyRemote;
import com.connexience.server.ejb.project.SubjectsRemote;
import com.connexience.server.ejb.project.UploaderRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.project.Uploader;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.rest.api.IStudyAPI;
import com.connexience.server.rest.model.WebsiteData;
import com.connexience.server.rest.model.WebsiteDataVersion;
import com.connexience.server.rest.model.project.WebsiteUploader;
import com.connexience.server.rest.model.project.study.*;
import com.connexience.server.rest.util.APIUtils;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.SecureHashUtils;
import com.connexience.server.util.StorageUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * User: nsjw7 Date: 28/05/2013 Time: 10:20
 * <p/>
 * To access lazily fetched members use
 *
 * @ Resource UserTransaction tx;
 * <p/>
 * tx.begin(); em.joinTransaction(); s = em.merge(s); WebsiteStudy ws = new WebsiteStudy(s); tx.commit();
 */
@Path("/rest/study")
@SuppressWarnings("unchecked")
@Stateless
public class StudyAPI implements IStudyAPI {
    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    StorageRemote storageBean;

    @EJB(lookup = "java:global/esc/server-beans/ProjectsBean")
    ProjectsRemote projectsBean;

    @EJB(lookup = "java:global/esc/server-beans/UploaderBean")
    UploaderRemote uploaderBean;

    @EJB(lookup = "java:global/esc/server-beans/StudyBean")
    StudyRemote studyBean;

    @EJB(lookup = "java:global/esc/server-beans/SubjectsBean")
    SubjectsRemote subjectBean;

    @PersistenceContext
    private EntityManager em;

    @Override
    public WebsiteStudy getStudy(int studyId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        return new WebsiteStudy(em.merge(studyBean.getStudy(ticket, studyId)));
    }

    @Override
    public WebsiteStudy saveStudy(WebsiteStudy websiteStudy) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Study study = websiteStudy.toStudy();
        study = em.merge(studyBean.saveStudy(ticket, study));

        return new WebsiteStudy(study);
    }

    @Override
    public Response removeStudy(@PathParam("studyId") int studyId, @FormParam("removeData") Boolean removeData) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            studyBean.deleteStudy(ticket, studyId, removeData);
        } catch (ConnexienceException ex) {
            ex.printStackTrace();

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response saveStudyStructure(@PathParam("studyId") int studyId, @Context HttpServletRequest request) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (request != null) {
            String loggerIdColumn = null;
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List items = upload.parseRequest(request);
                Iterator iter = items.iterator();

                ArrayList<String> fileNames = new ArrayList<>();
                ArrayList<Long> sizes = new ArrayList<>();

                long contentLength = Long.parseLong(request.getHeader("Content-Length"));

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (item.isFormField()) {
                        if ("loggerColumn".equals(item.getFieldName())) {
                            loggerIdColumn = item.getString();
                        }
                    } else {

                            fileNames.add(item.getName());
                            sizes.add(item.getSize());

                            Study study = studyBean.getStudy(ticket, studyId);
                            String dataFolderId = study.getDataFolderId();

                            DocumentRecord doc = new DocumentRecord();
                            doc.setName(item.getName());
                            doc.setContainerId(dataFolderId);
                            doc.setCreatorId(ticket.getUserId());
                            doc.setProjectId(ticket.getDefaultProjectId());
                            doc = storageBean.saveDocumentRecord(ticket, doc);

                            StorageUtils.upload(ticket, item.getInputStream(), contentLength, doc, "Saved From Study " + study.getName());

                            EJBLocator.lookupStudyParserBean().parseStudyStructure(ticket, doc.getId(), studyId, loggerIdColumn);

                            return Response.status(Response.Status.OK).entity(response).build();
                        }

                }

                String response = "{\"files\": [";

                for (int i = 0; i < fileNames.size(); i++) {
                    String filename = fileNames.get(i);
                    long size = sizes.get(i);
                    response += "{" +
                            "\"name\": \"" + filename + "\"," +
                            "\"size\": " + size + "," +
                            "\"url\": \"http://example.org/files/picture1.jpg\"," +
                            "\"thumbnailUrl\": \"http://example.org/files/thumbnail/picture1.jpg\"," +
                            "\"deleteUrl\": \"http://example.org/files/picture1.jpg\"," +
                            "\"deleteType\": \"DELETE\"" +
                            "},";
                }

                response = response.substring(0, response.length() - 1);

                response += "]}";

                return Response.status(Response.Status.OK).entity(response).build();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else {
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response saveStudySubjectProperties(@PathParam("studyId") int studyId, @Context HttpServletRequest request) throws ConnexienceException {

        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response saveStudyDeploymentProperties(@PathParam("studyId") int studyId, @Context HttpServletRequest request) throws ConnexienceException {

        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public List<WebsiteStudy> getStudies(@QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize, @QueryParam("orderBy") String orderBy, @QueryParam("direction") String direction) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<Study> studies = studyBean.getPublicStudies(ticket, APIUtils.getFirstResult(pageNum, pageSize), pageSize, orderBy, direction);
        List<WebsiteStudy> websiteStudies = new ArrayList<>();

        for (Study study : studies) {
            websiteStudies.add(new WebsiteStudy(em.merge(study)));
        }

        return websiteStudies;
    }

    @Override
    public List<WebsiteStudy> getUserStudies(@QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize, @QueryParam("orderBy") String orderBy, @QueryParam("direction") String direction) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<Study> studies = studyBean.getMemberStudies(ticket, APIUtils.getFirstResult(pageNum, pageSize), pageSize, orderBy, direction);

        List<WebsiteStudy> websiteStudies = new ArrayList<>();

        for (Study study : studies) {
            websiteStudies.add(new WebsiteStudy(em.merge(study)));
        }

        return websiteStudies;
    }

    @Override
    public Long getStudyCount() throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        return studyBean.getPublicStudyCount(ticket);
    }

    @Override
    public Long getUserStudyCount() throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        return studyBean.getMemberStudyCount(ticket);
    }

    @Override
    public Response addAdminToStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        projectsBean.addProjectAdmin(ticket, studyId, userId);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response addMemberToStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        projectsBean.addProjectMember(ticket, studyId, userId);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response removeMemberFromStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        projectsBean.removeProjectMember(ticket, studyId, userId);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response removeAdminFromStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        projectsBean.removeProjectAdmin(ticket, studyId, userId);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response deleteStudy(@PathParam("id") int studyId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        studyBean.deleteStudy(ticket, studyId);

        return Response.status(Response.Status.OK).build();
    }

    //	@Override
    //	public Response addDataToStudy(@FormParam("studyId") int studyId, @FormParam("escId") String escId) throws ConnexienceException
    //	{
    //		Ticket ticket = TransitionSessionUtils.getTicket(request);
    //		studyBean.addDataToStudy(ticket, studyId, escId);
    //
    //		return Response.status(Response.Status.OK).build();
    //	}

    @Override
    public List<WebsiteData> getStudyData(@QueryParam("studyId") int studyId, @QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        //Get all the data associated with this study
        Collection<LoggerData> loggerData = studyBean.getStudyData(ticket, studyId, APIUtils.getFirstResult(pageNum, pageSize), pageSize);
        List<WebsiteData> dataToReturn = new ArrayList<>();

        for (LoggerData loggerDatum : loggerData) {
            //Find the document record
            DocumentRecord documentRecord = storageBean.getDocumentRecord(ticket, loggerDatum.getDocumentRecordId());
            WebsiteData data = new WebsiteData();

            data.setName(documentRecord.getName());
            data.setId(loggerDatum.getDocumentRecordId());
            data.setMimeType(storageBean.getDocumentType(ticket, documentRecord.getDocumentTypeId()).getMimeType());

            //Get all the versions
            List<WebsiteDataVersion> versions = new ArrayList<>();
            List<DocumentVersion> documentVersions = storageBean.listVersions(ticket, documentRecord.getId());

            for (DocumentVersion documentVersion : documentVersions) {
                WebsiteDataVersion version = new WebsiteDataVersion();

                version.setId(documentVersion.getId());
                version.setDocumentRecordId(documentRecord.getId());
                version.setSize(documentVersion.getSize());
                version.setTimestamp(documentVersion.getTimestamp());
                version.setUserId(documentVersion.getUserId());
                version.setVersionNumber(version.getVersionNumber());
                versions.add(version);
            }

            data.setVersions(versions);
            dataToReturn.add(data);
        }

        return dataToReturn;
    }

    @Override
    public List<WebsiteSubjectGroup> getSubjectGroups(@PathParam("studyId") int studyId, @PathParam("phaseId") int phaseId, @PathParam("groupId") int groupId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteSubjectGroup> subjectGroups = new ArrayList<>();

        if (groupId == 0) {

            Phase phase = em.find(Phase.class, phaseId);

            for (SubjectGroup subjectGroup : phase.getSubjectGroups()) {
                if (subjectGroup.getParent() == null) {
                    subjectGroups.add(new WebsiteSubjectGroup(subjectGroup));
                }
            }
        } else {
            SubjectGroup group = em.merge(subjectBean.getSubjectGroup(ticket, groupId));

            for (SubjectGroup subjectGroup : group.getChildren()) {
                subjectGroups.add(new WebsiteSubjectGroup(subjectGroup));
            }
        }

        return subjectGroups;
    }

    @Override
    public List<WebsiteSubject> getSubjects(@PathParam("studyId") int studyId, @PathParam("groupId") int groupId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteSubject> subjects = new ArrayList<>();

        SubjectGroup group = em.merge(subjectBean.getSubjectGroup(ticket, groupId));

        for (Subject subject : group.getSubjects()) {
            subjects.add(new WebsiteSubject(subject));
        }

        return subjects;
    }

    @Override
    public WebsiteSubjectGroup saveSubjectGroup(@PathParam("studyId") int studyId, WebsiteSubjectGroup websiteSubjectGroup) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (websiteSubjectGroup.getId() != null) {
            SubjectGroup subjectGroup = em.merge(subjectBean.getSubjectGroup(ticket, websiteSubjectGroup.getId()));

            subjectGroup.setId(websiteSubjectGroup.getId());
            subjectGroup.setDisplayName(websiteSubjectGroup.getDisplayName());

            if (websiteSubjectGroup.getParentId().equals(0)) {
                subjectGroup.setParent(null);
            } else {
                subjectGroup.setParent(em.merge(subjectBean.getSubjectGroup(ticket, websiteSubjectGroup.getParentId())));
            }

            subjectGroup.setAdditionalProperties(websiteSubjectGroup.getAdditionalProperties());

            SubjectGroup savedGroup = em.merge(subjectBean.saveSubjectGroup(ticket, subjectGroup));

            return new WebsiteSubjectGroup(savedGroup);
        } else {
            SubjectGroup newSubjectGroup;

            if (websiteSubjectGroup.getParentId() > 0) {
                // createChildGroup
                newSubjectGroup = em.merge(subjectBean.createChildGroup(ticket, studyId, websiteSubjectGroup.getParentId(), websiteSubjectGroup.getDisplayName(), websiteSubjectGroup.getPhaseId()));
            } else {
                // createSubjectGroup
                newSubjectGroup = em.merge(subjectBean.createSubjectGroup(ticket, studyId, websiteSubjectGroup.getDisplayName(), websiteSubjectGroup.getPhaseId()));
            }

            return new WebsiteSubjectGroup(newSubjectGroup);
        }
    }

    @Override
    public Response deleteSubjectGroup(@FormParam("deleteData") boolean deleteData, @PathParam("studyId") int studyId, @PathParam("id") int subjectGroupId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        subjectBean.deleteSubjectGroup(ticket, subjectGroupId, deleteData);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public WebsiteSubject saveSubject(@PathParam("studyId") int studyId, @PathParam("groupId") int subjectGroupId, WebsiteSubject websiteSubject) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (websiteSubject.getId() != null) {
            Subject subject = em.merge(subjectBean.getSubject(ticket, websiteSubject.getId()));

            subject.setId(websiteSubject.getId());
            subject.setExternalId(websiteSubject.getExternalId());
            //subject.setSubjectGroup(subjectBean.getSubjectGroup(ticket, subjectGroupId));
            subject.setAdditionalProperties(websiteSubject.getAdditionalProperties());

            subject = em.merge(subjectBean.saveSubject(ticket, subject));

            return new WebsiteSubject(subject);
        } else {
            Subject newSubject = (em.merge(subjectBean.createSubject(ticket, subjectGroupId, websiteSubject.getExternalId())));
            newSubject.setAdditionalProperties(websiteSubject.getAdditionalProperties());

            newSubject = em.merge(subjectBean.saveSubject(ticket, newSubject));

            return new WebsiteSubject(newSubject);
        }
    }

    @Override
    public Response deleteSubject(@PathParam("studyId") int studyId, @PathParam("groupId") int subjectGroupId, @PathParam("id") int subjectId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        subjectBean.deleteSubject(ticket, subjectId);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public String getSubjectData(@PathParam("studyId") int studyId, @PathParam("id") int groupId) throws ConnexienceException {
        return "{\"files\": []}";
    }

    @Override
    public Response saveSubjectData(@PathParam("studyId") int studyId, @PathParam("id") int groupId, @Context HttpServletRequest request) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (request != null) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List items = upload.parseRequest(request);
                Iterator iter = items.iterator();

                ArrayList<String> fileNames = new ArrayList<>();
                ArrayList<Long> sizes = new ArrayList<>();

                long contentLength = Long.parseLong(request.getHeader("Content-Length"));

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                        if (!item.isFormField()) {
                            fileNames.add(item.getName());
                            sizes.add(item.getSize());

                            SubjectGroup group = subjectBean.getSubjectGroup(ticket, groupId);
                            String dataFolderId = group.getDataFolderId();

                            DocumentRecord doc = new DocumentRecord();
                            doc.setName(item.getName());
                            doc.setContainerId(dataFolderId);
                            doc.setCreatorId(ticket.getUserId());
                            doc.setProjectId(ticket.getDefaultProjectId());
                            doc = storageBean.saveDocumentRecord(ticket, doc);

                            StorageUtils.upload(ticket, item.getInputStream(), contentLength, doc, "Saved From Study " + group.getPhase().getStudy().getName());
                        }

                }

                String response = "{\"files\": [";

                for (int i = 0; i < fileNames.size(); i++) {
                    String filename = fileNames.get(i);
                    long size = sizes.get(i);
                    response += "{" +
                            "\"name\": \"" + filename + "\"," +
                            "\"size\": " + size + "," +
                            "\"url\": \"http://example.org/files/picture1.jpg\"," +
                            "\"thumbnailUrl\": \"http://example.org/files/thumbnail/picture1.jpg\"," +
                            "\"deleteUrl\": \"http://example.org/files/picture1.jpg\"," +
                            "\"deleteType\": \"DELETE\"" +
                            "},";
                }

                response = response.substring(0, response.length() - 1);

                response += "]}";

                return Response.status(Response.Status.OK).entity(response).build();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else {
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public List<WebsiteUploader> getUploadUsers(@FormParam("start") int start, @FormParam("end") int end) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        end = (end == -1) ? Integer.MAX_VALUE : end;

        Collection<Uploader> uploaders = uploaderBean.getUploaders(ticket, start, end);
        List<WebsiteUploader> websiteUploaders = new ArrayList<>();

        for (Uploader uploader : uploaders) {
            websiteUploaders.add(new WebsiteUploader(uploader));
        }

        return websiteUploaders;
    }

    @Override
    public WebsiteUploader saveUploadUser(WebsiteUploader uploadUser) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if (uploadUser.getId() != null) {
            Uploader uploader = em.merge(uploaderBean.getUploader(ticket, uploadUser.getId()));
            uploader.setUsername(uploadUser.getUsername());
            uploader.setHashedPassword(SecureHashUtils.generateSaltedHash(uploadUser.getPassword()));

            System.out.println(uploader.getHashedPassword());

            return new WebsiteUploader(em.merge(uploaderBean.saveUploader(ticket, uploader)));
        } else {
            Uploader uploader = em.merge(uploaderBean.createUploader(ticket, uploadUser.getUsername(), uploadUser.getPassword()));

            return new WebsiteUploader(uploader);
        }
    }

    @Override
    public Response deleteUploadUser(@PathParam("uploaderId") String uploadUserId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        uploaderBean.deleteUploader(ticket, Integer.parseInt(uploadUserId));

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public List<WebsiteUploader> getStudyUploadUsers(@PathParam("studyId") int studyId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Collection<Uploader> uploaders = em.merge(studyBean.getStudy(ticket, studyId)).getUploaders();
        List<WebsiteUploader> websiteUploaders = new ArrayList<>();

        for (Uploader uploader : uploaders) {
            websiteUploaders.add(new WebsiteUploader(uploader));
        }

        return websiteUploaders;
    }

    @Override
    public WebsiteUploader addUploadUser(@PathParam("studyId") int studyId, @PathParam("uploaderId") int uploaderId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Uploader uploader = em.merge(uploaderBean.addUploader(ticket, studyId, uploaderId));

        return new WebsiteUploader(uploader);
    }

    @Override
    public Response removeUploadUser(@PathParam("studyId") int studyId, @PathParam("uploaderId") int uploaderId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        em.merge(uploaderBean.removeUploader(ticket, studyId, uploaderId));

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public void sendMessageToAdmins(@PathParam("studyId") int studyId, @FormParam("title") String title, @FormParam("message") String message) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        studyBean.sendMessageToAdmins(ticket, studyId, title, message);
    }

    @Override
    public void notifyUploadFailure(@PathParam("studyId") int studyId,
                                    @FormParam("username") String username,
                                    @FormParam("filename") String filename,
                                    @FormParam("errorMessage") String errorMessage,
                                    @FormParam("expectedSize") Long expectedSize,
                                    @FormParam("stagedSize") Long stagedSize,
                                    @FormParam("blobSize") Long blobSize)
            throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        studyBean.notifyUploadFailure(ticket, studyId, username, filename, errorMessage, expectedSize, stagedSize, blobSize);

    }

    @Override
    public long countProjectsWithMatchingName(@PathParam("name") String name) throws ConnexienceException {
        return EJBLocator.lookupProjectsBean().countProjectsWithMatchingName(name);
    }

    @Override
    public long countProjectsWithMatchingExternalId(@PathParam("externalId") String externalId) throws ConnexienceException {
        return EJBLocator.lookupProjectsBean().countProjectsWithMatchingExternalId(externalId);
    }

    @Override
    public List<WebsitePhaseMin> getStudyPhases(@PathParam("studyId") int studyId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        ArrayList<WebsitePhaseMin> websitePhases = new ArrayList<>();

        List<Phase> phases = studyBean.getPhases(ticket, studyId);
        for (Phase phase : phases) {
            websitePhases.add(new WebsitePhaseMin(em.merge(phase)));
        }
        return websitePhases;
    }

    @Override
    public WebsiteStudy addPhaseToStudy(@PathParam("studyId") int studyId, WebsitePhase phase) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        if(phase.getId() != null){
            Phase updatedPhase = studyBean.editPhase(ticket, phase.getId(), phase.getName());
        }
        else {
            Phase newPhase = studyBean.addPhaseToStudy(ticket, studyId, phase.getName());
        }


        return new WebsiteStudy(em.merge(studyBean.getStudy(ticket, studyId)));
    }

    @Override
    public WebsiteStudy removePhase(@PathParam("studyId") int studyId, @PathParam("studyId") int phaseId) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        boolean removeFolders = true;
        studyBean.removePhase(ticket, phaseId, removeFolders);

        return new WebsiteStudy(em.merge(studyBean.getStudy(ticket, studyId)));
    }

    @Override
    public boolean isPhaseUnique(final int studyId, final String phasename) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        return studyBean.isPhaseUnique(ticket, studyId, phasename);
    }

    @Override
    public List<WebsiteSubject> getSubjectFromProperty(@PathParam("propertyName") String propertyName, @PathParam("propertyValue") String propertyValue) throws ConnexienceException{

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Collection<Subject> subjects = subjectBean.getSubjectFromProperty(ticket, propertyName, propertyValue);
        List<WebsiteSubject> websiteSubjects = new ArrayList<>();
        for(Subject subject: subjects){
            websiteSubjects.add(new WebsiteSubject(subject));
        }

        return websiteSubjects;
    }

    @Override
    public boolean isPropertyUnique(@PathParam("propertyName") String propertyName, @PathParam("propertyValue") String propertyValue) throws ConnexienceException{
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        return subjectBean.isPropertyUnique(ticket, propertyName, propertyValue);
    }
}

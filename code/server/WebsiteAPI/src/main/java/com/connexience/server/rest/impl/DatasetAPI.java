/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.datasets.DatasetsRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.model.datasets.items.SingleValueItem;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.datasets.items.single.DoubleValueItem;
import com.connexience.server.model.datasets.items.single.SingleJsonRowItem;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.rest.api.IDatasetAPI;
import com.connexience.server.rest.model.WebsiteDataset;
import com.connexience.server.rest.model.WebsiteDatasetItem;
import com.connexience.server.rest.model.WebsiteDatasetKeyList;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.rest.util.WebsiteObjectFactory;
import com.connexience.server.util.JSONContainer;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.List;

/**
 * User: nsjw7 Date: 28/05/2013 Time: 10:20
 * <p/>
 * To access lazily fetched members use
 */
@SuppressWarnings("unchecked")
@Path("/rest/datasets")
public class DatasetAPI implements IDatasetAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@EJB(lookup = "java:global/esc/server-beans/DatasetsBean")
	DatasetsRemote datasetsBean;

	@Path(value = "/{id}/items/nameditems/{name}/json")
    @GET
	@Produces(value = "application/json")
    @Override
	public String getDashboardItemJsonValue(@PathParam(value = "id") String dashboardId, @PathParam(value = "name") String name) throws ConnexienceException {
		Object value = datasetsBean.getDatasetItemValue(TransitionSessionUtils.getTicket(request), dashboardId, name);
		if (value instanceof JSONContainer) {
			// Need to send JSONObject back as a string
			try {
				return ((JSONContainer) value).getJSONObject().toString();
			} catch (JSONException e) {
				throw new ConnexienceException(e);
			}
		} else {
			throw new ConnexienceException("Object is not available as JSON data");
		}
	}


    //Dataset Operations


    @Path("/")
    @GET
    @Produces("application/json")
    @Override
    /**
     * Returns a list of all the datasets the current user as access to.
     * @return A list of the accessible datasets
     */
	public WebsiteDataset[] listDatasets() throws Exception {
		Ticket t = TransitionSessionUtils.getTicket(request);

        List datasets = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(t, t.getUserId(), Dataset.class, 0, Integer.MAX_VALUE);

		WebsiteDataset[] results = new WebsiteDataset[datasets.size()];
		for (int i = 0; i < datasets.size(); i++) {
			results[i] = WebsiteObjectFactory.createEscDataset((Dataset) datasets.get(i));
		}
		return results;
	}

    @Path("/{id}")
    @GET
    @Produces("application/json")
    @Override
    /**
     * @param id the ID of the dataset being retrieved.
     * @return A dataset object.
     *
     */
    public WebsiteDataset getDataset(@PathParam(value = "id") String id) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        return WebsiteObjectFactory.createEscDataset(EJBLocator.lookupDatasetsBean().getDataset(t, id));
    }

    @Path("/find")
    @GET
    @Produces("application/json")
    @Override
    public WebsiteDataset findDataset(@QueryParam(value = "name") String name, @QueryParam(value = "userId") String userId) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);

        Dataset dataset = null;

        if(userId != null)
        {
            dataset = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, userId, name);
        }
        else
        {
            dataset = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, name);
        }

        if(dataset != null)
        {
            return WebsiteObjectFactory.createEscDataset(dataset);
        }
        else
        {
            throw new Exception("No dataset with name '" + name + "' exists");
        }
    }

    @Path("/create")
    @POST
    @Consumes("text/plain")
    @Produces("application/json")
    @Override
    public WebsiteDataset createDataset(String name) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset ds = new Dataset();
        ds.setName(name);
        ds.setProjectId(t.getDefaultProjectId());
        ds.setDescription("Dataset created by API");
        ds = EJBLocator.lookupDatasetsBean().saveDataset(t, ds);
        return WebsiteObjectFactory.createEscDataset(ds);
    }

    @Path("/{id}")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public WebsiteDataset updateDataset(@PathParam(value = "id") String id, WebsiteDataset ds) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset existing = EJBLocator.lookupDatasetsBean().getDataset(t, ds.getId());
        if (existing != null) {
            existing.setName(ds.getName());
            existing.setDescription(ds.getDescription());
            existing = EJBLocator.lookupDatasetsBean().saveDataset(t, existing);
            return WebsiteObjectFactory.createEscDataset(existing);
        } else {
            throw new Exception("No such dataset");
        }
    }

    @Path("/{id}")
    @DELETE
    @Override
    public void deleteDataset(@PathParam(value = "id") String id) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        EJBLocator.lookupDatasetsBean().removeDataset(t, id);
    }

    @Path("/{id}/reset")
    @GET
    @Override
    public void resetDataset(@PathParam(value = "id") String id) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        EJBLocator.lookupDatasetsBean().resetDataset(t, id);
    }

    @Path("/{id}/items")
    @GET
    @Produces("application/json")
    @Override
    public WebsiteDatasetItem[] getDatasetContents(@PathParam(value = "id") String id) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        List items = EJBLocator.lookupDatasetsBean().getDatasetItems(t, id);
        WebsiteDatasetItem[] results = new WebsiteDatasetItem[items.size()];

        for (int i = 0; i < items.size(); i++) {
            results[i] = WebsiteObjectFactory.createEscDatasetItem((DatasetItem) items.get(i));
        }
        return results;
    }

    @Path("/aggregate/{group}/{name}")
    @GET
    @Produces("application/json")
    @Override
    public WebsiteDatasetItem[] aggregateDatasetContents(@PathParam("group") String groupId, @PathParam("name") String name) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);

        List users = EJBLocator.lookupGroupDirectoryBean().listGroupMembers(t, groupId);

        // Get dataset for each user
        User u;
        Dataset ds;
        ArrayList<String> ids = new ArrayList<>();

        for (Object o : users)
        {
            u = (User) o;
            ds = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, u.getId(), name);

            if (ds != null)
            {
                ids.add(ds.getId());
            }
        }

        // Join into uberdataset
        List items = EJBLocator.lookupDatasetsBean().aggregateDatasetItems(t, ids);
        WebsiteDatasetItem[] results = new WebsiteDatasetItem[items.size()];

        for (int i = 0; i < items.size(); i++)
        {
            results[i] = WebsiteObjectFactory.createEscDatasetItem((DatasetItem) items.get(i));
        }

        return results;
    }


    //Dataset Item Operations


    @Path(value = "/{id}/items/{itemname}")
    @GET
    @Produces("application/json")
    @Override
    public WebsiteDatasetItem getDatasetItem(@PathParam("id") String id, @PathParam("itemname")String itemName) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset ds = EJBLocator.lookupDatasetsBean().getDataset(t, id);
        if (ds != null) {
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, ds.getId(), itemName);
            if (item != null) {
                return WebsiteObjectFactory.createEscDatasetItem(item);
            } else {
                throw new Exception("No item: " + itemName + " in specified dataset.");
            }
        } else {
            throw new Exception("No such dataset with ID " + id);
        }
    }

    @Path(value = "/find/{name}/items/{itemname}")
    @GET
    @Produces("application/json")
    @Override
    public WebsiteDatasetItem findDatasetItem(@PathParam("name") String datasetName, @PathParam("itemname")String itemName) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset ds = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, datasetName);
        if (ds != null) {
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, ds.getId(), itemName);
            if (item != null) {
                return WebsiteObjectFactory.createEscDatasetItem(item);
            } else {
                throw new Exception("No item: " + itemName + " in dataset: " + datasetName);
            }
        } else {
            throw new Exception("No such dataset: " + datasetName);
        }
    }

    @Path("/{id}/items/{itemname}")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    @Override
    public String updateDatasetItem(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName, @FormParam(value = "rowId") Long rowId, @FormParam(value = "jsonData") String jsonData) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);

        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, datasetId, itemName);

        if (item != null)
        {
            if (item instanceof SingleJsonRowItem)
            {
                EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(t, item.getId(), jsonData);
                return "OK";
            }
            else if (item instanceof JsonMultipleValueItem)
            {
                if(rowId != null)
                {
                    EJBLocator.lookupDatasetsBean().updateExistingMultipleValueItemRow(t, datasetId, itemName, rowId, jsonData);
                    return "OK";
                }
                else
                {
                    throw new Exception("Update failed. Missing row ID.");
                }
            } else if (item instanceof DoubleValueItem)
            {
                Double doubleValue;
                try {
                    doubleValue = Double.parseDouble(jsonData);
                } catch (NumberFormatException e) {
                    throw new NumberFormatException("Non double value passed in when trying to update DoubleValueItem: " + jsonData);
                }
                EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(t, item.getId(), doubleValue);
                return "OK";
            }
            else
            {
                throw new Exception("Item: " + itemName + " is not a valid item type.");
            }
        }
        else
        {
            throw new Exception("No such item: " + itemName + " in dataset: " + datasetId);
        }
    }

    @Path("/{id}/items/{name}")
    @DELETE
    @Override
    public void deleteDatasetItem(@PathParam(value = "id") String datasetId, @PathParam(value = "name") String itemName) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, datasetId, itemName);
        if (item != null) {
            EJBLocator.lookupDatasetsBean().removeDatasetItem(t, item.getId());
        } else {
            throw new Exception("No such item: " + itemName);
        }
    }

    @Path("/{id}/items/{itemname}/size")
    @GET
    @Produces("text/plain")
    @Override
    public Integer getDatasetItemSize(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        return EJBLocator.lookupDatasetsBean().getMultipleValueItemSize(t, datasetId, itemName);
    }

    @Path("/{id}/items/{itemname}/contents")
    @GET
    @Produces("application/json")
    @Override
    public String getDatasetItemContents(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName, @DefaultValue("0") @QueryParam("start") int start, @DefaultValue("50") @QueryParam("pagesize") int pagesize) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, datasetId, itemName);
        if (item instanceof SingleValueItem) {
            SingleValueItem svi = (SingleValueItem) item;
            Object value = svi.getObjectValue();
            if (value != null) {
                return value.toString();
            } else {
                return "";
            }
        } else if (item instanceof MultipleValueItem) {
            JSONContainer c = EJBLocator.lookupDatasetsBean().queryMultipleValueItem(t, datasetId, itemName, start, pagesize);
            return c.toString();
        } else {
            throw new Exception("Item: " + itemName + " is not a valid item type");
        }
    }

    @Path("/find/{datasetname}/items/{itemname}/contents")
    @GET
    @Produces("application/json")
    @Override
    public String getDatasetItemContentsByName(@PathParam(value = "datasetname") String datasetName, @PathParam(value = "itemname") String itemName, @DefaultValue("0") @QueryParam("start") int start, @DefaultValue("50") @QueryParam("pagesize") int pagesize, @QueryParam("userid") String userId) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset dataset = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, userId, datasetName);
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, dataset.getId(), itemName);

        if (item instanceof SingleValueItem) {
            SingleValueItem svi = (SingleValueItem) item;
            Object value = svi.getObjectValue();
            if (value != null) {
                return value.toString();
            } else {
                return "";
            }
        } else if (item instanceof MultipleValueItem) {
            JSONContainer c = EJBLocator.lookupDatasetsBean().queryMultipleValueItem(t, item.getDatasetId(), itemName, start, pagesize);
            return c.toString();
        } else {
            throw new Exception("Item: " + itemName + " is not a valid item type");
        }
    }

    @Path("/{id}/items")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public WebsiteDatasetItem addItemToDataset(@PathParam(value = "id") String id, WebsiteDatasetItem item) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset ds = EJBLocator.lookupDatasetsBean().getDataset(t, id);
        if (ds != null) {
            DatasetItem incomingItem = WebsiteObjectFactory.createDatasetItem(item);
            incomingItem.setDatasetId(id);
            incomingItem.setId(-1);

            // Only add the item if a pre-existing one doesn't exist
            DatasetItem existingItem = EJBLocator.lookupDatasetsBean().getDatasetItem(t, id, item.getName());
            if (existingItem == null) {
                // Just add - no existing item
                DatasetItem savedItem = EJBLocator.lookupDatasetsBean().saveDatasetItem(t, incomingItem);
                return WebsiteObjectFactory.createEscDatasetItem(savedItem);
            } else {
                if (WebsiteObjectFactory.datasetItemsMatch(existingItem, incomingItem)) {
                    return WebsiteObjectFactory.createEscDatasetItem(existingItem);
                } else {
                    throw new Exception("A different item with the same name already exists");
                }
            }
        } else {
            throw new Exception("No such dataset: " + id);
        }
    }


    //Dataset Multi Value Item Operations


    @Path("/{id}/items/{itemname}/rows")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public String getMultiValueDatasetItemRows(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName, @DefaultValue("0") @QueryParam("start") int startRow, @DefaultValue("50") @QueryParam("pagesize") int pageSize, WebsiteDatasetKeyList keys) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);

        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, datasetId, itemName);

        if (item instanceof MultipleValueItem)
        {
            if(keys != null && keys.getKeys().length != 0)
            {
                JSONContainer c = EJBLocator.lookupDatasetsBean().queryMultipleValueItem(t, datasetId, itemName, startRow, pageSize, keys.getKeys());
                return c.toString();
            }
            else
            {
                JSONContainer c = EJBLocator.lookupDatasetsBean().queryMultipleValueItem(t, datasetId, itemName, startRow, pageSize);

                return c.toString();
            }
        }
        else
        {
            throw new Exception("Item: " + itemName + " is not a multiple value item");
        }
    }

    @Path("/find/{datasetname}/items/{itemname}/rows")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public List<Object> findMultiValueDatasetItemRows(@PathParam(value = "datasetname") String datasetName, @PathParam(value = "itemname") String itemName, @DefaultValue("0") @QueryParam("start") int startRow, @DefaultValue("50") @QueryParam("pagesize") int pageSize, List<String> userIds) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);
        List<Object> data = new ArrayList<>();

        for(String userId : userIds)
        {
            Dataset dataset = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, userId, datasetName);
            DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, dataset.getId(), itemName);

            if (item instanceof MultipleValueItem)
            {
                data.add(new ObjectMapper().readValue(EJBLocator.lookupDatasetsBean().queryMultipleValueItem(t, dataset.getId(), itemName, startRow, pageSize).getStringData(), Object.class));
            }
            else
            {
                throw new Exception("Item: " + itemName + " is not a multiple value item");
            }
        }

        return data;
    }

    @Path("/{id}/items/{itemName}/rows/{rowId}")
    @GET
    @Produces("application/json")
    @Override
    public String getMultiValueDatasetItemRow(@PathParam(value="id")String datasetId, @PathParam(value="itemName")String itemName, @PathParam(value="rowId") Long rowId) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        JSONContainer c = EJBLocator.lookupDatasetsBean().getMultipleValueDataRow(t, datasetId, itemName, rowId);
        if(c!=null){
            return c.getStringData();
        } else {
            throw new Exception("Specified row does not exist");
        }
    }

    @Path("find/{datasetname}/items/{itemName}/rows/{rowId}")
    @GET
    @Produces("application/json")
    @Override
    public String findMultiValueDatasetItemRow(@PathParam(value="datasetname")String datasetName, @PathParam(value="itemName")String itemName, @PathParam(value="rowId") Long rowId, @QueryParam(value="userId") String userId) throws Exception {
        Ticket t = TransitionSessionUtils.getTicket(request);
        Dataset dataset = null;

        if(userId != null)
        {
            dataset = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, userId, datasetName);
        }
        else
        {
            dataset = EJBLocator.lookupDatasetsBean().getUserDatasetByName(t, datasetName);
        }

        if(dataset != null)
        {
            JSONContainer c = EJBLocator.lookupDatasetsBean().getMultipleValueDataRow(t, dataset.getId(), itemName, rowId);
            if(c!=null){
            return c.getStringData();
            } else {
                throw new Exception("Specified row does not exist");
            }
        }
        else
        {
            throw new Exception("No dataset with name '" + datasetName + "' exists");
        }


    }

    @Path("/{id}/items/{itemname}/append")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public WebsiteDatasetItem[] appendToMultipleValueDatasetItem(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName, String jsonData) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);
        DatasetItem item = EJBLocator.lookupDatasetsBean().getDatasetItem(t, datasetId, itemName);

        if (item != null) {
            if (item instanceof JsonMultipleValueItem) {

                JSONArray jsonArray = new JSONArray(jsonData);

                WebsiteDatasetItem[] addedItems = new WebsiteDatasetItem[jsonArray.length()];

                for(int i=0; i<jsonArray.length(); i++){

                    System.out.println(jsonArray.getJSONObject(i).toString());

                    addedItems[i] = WebsiteObjectFactory.createEscDatasetItem(EJBLocator.lookupDatasetsBean().updateDatasetItemWithValue(t, item.getId(), jsonArray.getJSONObject(i).toString()));
                }

                return addedItems;

            } else {
                throw new Exception("Item: " + itemName + " is not a multi-row JSON item");
            }
        } else {
            throw new Exception("No such item: " + itemName + " in dataset: " + datasetId);
        }
    }

    @Path("/{id}/items/{itemname}/{rowId}")
    @PUT
    @Consumes("application/json")
    @Override
    public void updateMultipleValueDatasetItem(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName, @PathParam(value = "rowId") Long rowId, String jsonData) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);

        Dataset dataset = EJBLocator.lookupDatasetsBean().getDataset(t, datasetId);

        if(dataset!=null)
        {
            EJBLocator.lookupDatasetsBean().updateExistingMultipleValueItemRow(t, datasetId, itemName, rowId, jsonData);
        }
        else
        {
            throw new Exception("Specified dataset does not exist");
        }
    }

    @Path("/{id}/items/{itemname}/{rowId}")
    @DELETE
    @Consumes("text/plain")
    @Produces("text/plain")
    @Override
    public void removeFromMultipleValueDatasetItem(@PathParam(value = "id") String datasetId, @PathParam(value = "itemname") String itemName, @PathParam(value = "rowId") Long rowId) throws Exception {

        Ticket t = TransitionSessionUtils.getTicket(request);

        Dataset dataset = EJBLocator.lookupDatasetsBean().getDataset(t, datasetId);

        if(dataset!=null)
        {
            EJBLocator.lookupDatasetsBean().removeExistingMultipleValueItemRow(t, datasetId, itemName, rowId);
        }
        else
        {
            throw new Exception("Specified dataset does not exist");
        }
    }

}
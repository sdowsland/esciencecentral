/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowInvocationMessage;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.WorkflowTerminationReport;
import com.connexience.server.rest.api.IWorkflowsAPI;
import com.connexience.server.rest.model.WebsiteWorkflow;
import com.connexience.server.rest.model.WebsiteWorkflowInvocation;
import com.connexience.server.rest.model.WebsiteWorkflowParameterList;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.rest.util.WebsiteObjectFactory;
import com.connexience.server.util.SerializationUtils;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/rest/workflows")
@Stateless
public class WorkflowsAPI implements IWorkflowsAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@Override
	public WebsiteWorkflow[] listWorkflows() throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		List workflows = WorkflowEJBLocator.lookupWorkflowManagementBean().listWorkflows(ticket);
		WebsiteWorkflow[] results = new WebsiteWorkflow[workflows.size()];

		for (int i = 0; i < workflows.size(); i++) {
			results[i] = WebsiteObjectFactory.createEscWorkflow((WorkflowDocument) workflows.get(i));
		}

		return results;
	}

	@Override
	public WebsiteWorkflow getWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		return WebsiteObjectFactory.createEscWorkflow(WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, workflowId));
	}

	@Override
	public Response deleteWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		try {
			WorkflowEJBLocator.lookupWorkflowManagementBean().deleteWorkflowDocument(ticket, workflowId);
		} catch (ConnexienceException ex) {
			ex.printStackTrace();

			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
		}

		return Response.status(Response.Status.OK).build();
	}

	@Override
	public WebsiteWorkflowInvocation executeWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);
		String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

	@Override
	public WebsiteWorkflowInvocation executeWorkflow(@PathParam(value = "id") String workflowId, @PathParam(value = "versionid") String versionId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId, versionId);
		String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

	@Override
	public WebsiteWorkflowInvocation executeWorkflowOnDocument(@PathParam(value = "id") String workflowId, String documentId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);
		msg.setTargetFileId(documentId);
		String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

	@Override
	public WebsiteWorkflowInvocation executeWorkflowOnDocument(@PathParam(value = "id") String workflowId, @PathParam(value = "versionid") String versionId, String documentId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId, versionId);
		msg.setTargetFileId(documentId);
		String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

	@Override
	public WebsiteWorkflowInvocation executeWorkflowWithParameters(@PathParam(value = "id") String workflowId, WebsiteWorkflowParameterList parameters) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);
		WorkflowParameterList params = WebsiteObjectFactory.createEscWorkflowParameterList(parameters);
		msg.setParameterXmlData(SerializationUtils.serialize(params));
		String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

    /*@Override
	public String executeWorkflowWithParameters(@PathParam(value = "id") String workflowId, String foo) throws Exception {

        System.out.println("Method Hit");

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);

        //JSONObject object = new JSONObject(parameters);

        //WorkflowParameterList params = WebsiteObjectFactory.createEscWorkflowParameterList(parameters);
        //msg.setParameterXmlData(SerializationUtils.serialize(params));
        //String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        //return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));

        return "Success";
    }*/

	@Override
	public WebsiteWorkflowInvocation executeWorkflowWithParameters(@PathParam(value = "id") String workflowId, @PathParam(value = "versionid") String versionId, WebsiteWorkflowParameterList parameters) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId, versionId);

		WorkflowParameterList params = WebsiteObjectFactory.createEscWorkflowParameterList(parameters);

		msg.setParameterXmlData(SerializationUtils.serialize(params));
		String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

	@Override
	public WebsiteWorkflowInvocation[] listInvocationsOfWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		List results = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(ticket, workflowId);
		WebsiteWorkflowInvocation[] invocations = new WebsiteWorkflowInvocation[results.size()];

		for (int i = 0; i < results.size(); i++) {
			invocations[i] = WebsiteObjectFactory.createEscWorkflowInvocation((WorkflowInvocationFolder) results.get(i));
		}

		return invocations;
	}

	@Override
	public WebsiteWorkflowInvocation getInvocation(@PathParam(value = "id") String invocationId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}

	@Override
	public WebsiteWorkflowInvocation terminateInvocation(@PathParam(value = "id") String invocationId) throws Exception {

		Ticket ticket = TransitionSessionUtils.getTicket(request);

		WorkflowTerminationReport report = WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, invocationId);

		return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
	}
}

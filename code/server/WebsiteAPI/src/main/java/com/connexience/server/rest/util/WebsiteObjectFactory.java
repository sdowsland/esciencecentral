/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.util;

import com.connexience.server.model.datasets.Dataset;
import com.connexience.server.model.datasets.DatasetConstants;
import com.connexience.server.model.datasets.DatasetItem;
import com.connexience.server.model.datasets.items.MultipleValueItem;
import com.connexience.server.model.datasets.items.SingleValueItem;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;
import com.connexience.server.model.datasets.items.single.DoubleValueItem;
import com.connexience.server.model.datasets.items.single.SingleJsonRowItem;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameter;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.rest.model.*;

/**
 * This class creates WebsiteXX objects from the standard database model objects
 *
 * @author hugo
 */
public class WebsiteObjectFactory {
	public static WebsiteDocumentVersion createEscDocumentVersion(DocumentVersion v) {
		WebsiteDocumentVersion escVersion = new WebsiteDocumentVersion();
		escVersion.setId(v.getId());
		escVersion.setDocumentRecordId(v.getDocumentRecordId());
		escVersion.setComments(v.getComments());
		escVersion.setUserId(v.getUserId());
		escVersion.setVersionNumber(v.getVersionNumber());
		escVersion.setSize(v.getSize());
		escVersion.setTimestamp(v.getTimestamp());
		escVersion.setDownloadPath("/data/" + v.getDocumentRecordId() + "/" + v.getId());
		return escVersion;
	}

	/*public static WebsiteGroup createEscGroupWithoutUsers(Group g) {
		WebsiteGroup group = new WebsiteGroup();
		group.setDescription(g.getDescription());
		group.setId(g.getId());
		group.setMembers(new ArrayList<WebsiteUser>());
		group.setName(g.getName());
		return group;
	}*/

	public static WebsiteFolder createEscFolder(Folder f) {
		WebsiteFolder escFolder = new WebsiteFolder();
		escFolder.setId(f.getId());
		escFolder.setName(f.getName());
		escFolder.setDescription(f.getDescription());
		escFolder.setCreatorId(f.getCreatorId());
		escFolder.setContainerId(f.getContainerId());
		escFolder.setProjectId(f.getProjectId());
		return escFolder;
	}

	/*public static WebsiteUser createEscUser(User u) {
		WebsiteUser escUser = new WebsiteUser();
		escUser.setId(u.getId());
		escUser.setFirstname(u.getName());
		escUser.setLastname(u.getSurname());
		return escUser;
	}*/

	public static WebsiteMetadataItem createMetadataItem(MetadataItem md) {
		WebsiteMetadataItem escMetadataItem = new WebsiteMetadataItem();
		escMetadataItem.setObjectId(md.getObjectId());
		escMetadataItem.setName(md.getName());
		escMetadataItem.setCategory(md.getCategory());
		escMetadataItem.setStringValue(md.getStringValue());
		escMetadataItem.setId(String.valueOf(md.getId()));

		if (md instanceof TextMetadata) {
			escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
		} else if (md instanceof BooleanMetadata) {
			escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.BOOLEAN);
		} else if (md instanceof NumericalMetadata) {
			escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.NUMERICAL);
		} else if (md instanceof DateMetadata) {
			escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.DATE);
		} else {
			escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
		}

		return escMetadataItem;
	}

	public static WebsiteDataset createEscDataset(Dataset ds) {
		WebsiteDataset escDataset = new WebsiteDataset();
		escDataset.setCreatorId(ds.getCreatorId());
		escDataset.setDescription(ds.getDescription());
		escDataset.setId(ds.getId());
		escDataset.setName(ds.getName());
		escDataset.setProjectId(ds.getProjectId());
		return escDataset;
	}

	public static DatasetItem createDatasetItem(WebsiteDatasetItem escDatasetItem) {
		DatasetItem item = null;
		if (escDatasetItem.getItemType() == WebsiteDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW) {
			item = new JsonMultipleValueItem();
		} else if (escDatasetItem.getItemType() == WebsiteDatasetItem.DATASET_ITEM_TYPE.SINGLE_ROW) {
			item = new SingleJsonRowItem();
		} else {
			item = new JsonMultipleValueItem();
		}

		if (escDatasetItem.getId() != -1) {
			item.setId(escDatasetItem.getId());
		} else {
			item.setId(0);
		}

		item.setDatasetId(escDatasetItem.getDatasetId());
		item.setName(escDatasetItem.getName());

		if (item instanceof SingleValueItem) {
			SingleValueItem svi = (SingleValueItem) item;
			switch (escDatasetItem.getUpdateStrategy()) {
				case AVERAGE:
					svi.setUpdateStrategy(DatasetItem.UPDATE_CALCULATES_AVERAGE);
					break;

				case MAXIMUM:
					svi.setUpdateStrategy(DatasetItem.UPDATE_CALCULATES_MAXIMUM);
					break;

				case MINIMUM:
					svi.setUpdateStrategy(DatasetItem.UPDATE_CALCULATES_MINIMUM);
					break;

				case REPLACE:
					svi.setUpdateStrategy(DatasetItem.UPDATE_REPLACES_VALUES);
					break;

				case SUM:
					svi.setUpdateStrategy(DatasetItem.UPDATE_CALCULATES_SUM);
					break;

				default:
					svi.setUpdateStrategy(DatasetItem.UPDATE_REPLACES_VALUES);
					break;
			}
		}
		return item;
	}

	public static WebsiteDatasetItem createEscDatasetItem(DatasetItem item) {
		WebsiteDatasetItem escDatasetItem = new WebsiteDatasetItem();
		escDatasetItem.setDatasetId(item.getDatasetId());
		escDatasetItem.setId(item.getId());
		escDatasetItem.setName(item.getName());

		// Update strategy
		if (item instanceof SingleValueItem) {
			SingleValueItem svi = (SingleValueItem) item;
			escDatasetItem.setStringValue(svi.getObjectValue().toString());
			if (svi.getUpdateStrategy() != null && !svi.getUpdateStrategy().isEmpty()) {
				if (DatasetConstants.UPDATE_CALCULATES_AVERAGE.equals(svi.getUpdateStrategy())) {
					escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.AVERAGE);
				} else if (DatasetConstants.UPDATE_CALCULATES_MAXIMUM.equals(svi.getUpdateStrategy())) {
					escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.MAXIMUM);
				} else if (DatasetConstants.UPDATE_CALCULATES_MINIMUM.equals(svi.getUpdateStrategy())) {
					escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.MINIMUM);
				} else if (DatasetConstants.UPDATE_CALCULATES_SUM.equals(svi.getUpdateStrategy())) {
					escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.SUM);
				} else if (DatasetConstants.UPDATE_REPLACES_VALUES.equals(svi.getUpdateStrategy())) {
					escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.REPLACE);
				} else {
					escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.REPLACE);
				}
			} else {
				escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.REPLACE);
			}
		} else {
			// No update strategy for multirow items
			escDatasetItem.setUpdateStrategy(WebsiteDatasetItem.DATASET_ITEM_UPDATE_STRATEGY.REPLACE);
		}

		if (item instanceof SingleJsonRowItem) {
			escDatasetItem.setItemType(WebsiteDatasetItem.DATASET_ITEM_TYPE.SINGLE_ROW);
			escDatasetItem.setStringValue(((SingleJsonRowItem) item).getStringValue());
		} else if (item instanceof DoubleValueItem) {
			escDatasetItem.setItemType(WebsiteDatasetItem.DATASET_ITEM_TYPE.SINGLE_ROW);
			escDatasetItem.setStringValue(Double.toString(((DoubleValueItem) item).getDoubleValue()));
		} else if (item instanceof JsonMultipleValueItem) {
			escDatasetItem.setItemType(WebsiteDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW);
		} else if (item instanceof MultipleValueItem) {
			escDatasetItem.setItemType(WebsiteDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW);
		} else {
			escDatasetItem.setItemType(WebsiteDatasetItem.DATASET_ITEM_TYPE.MULTI_ROW);
		}

		return escDatasetItem;
	}

	public static boolean datasetItemsMatch(DatasetItem item1, DatasetItem item2) {
		if (item1.getDatasetId().equals(item2.getDatasetId())) {
			if (item1.getName().equals(item2.getName())) {
				if (item1.getTypeLabel().equals(item2.getTypeLabel())) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static WebsiteWorkflow createEscWorkflow(WorkflowDocument wf) {
		WebsiteWorkflow websiteWorkflow = new WebsiteWorkflow();
		websiteWorkflow.setId(wf.getId());
		websiteWorkflow.setName(wf.getName());
		websiteWorkflow.setContainerId(wf.getContainerId());
		websiteWorkflow.setDescription(wf.getDescription());
		websiteWorkflow.setCreatorId(wf.getCreatorId());
		websiteWorkflow.setCurrentVersionNumber(wf.getCurrentVersionNumber());
		websiteWorkflow.setCurrentVersionSize(wf.getCurrentVersionSize());
		websiteWorkflow.setProjectId(wf.getProjectId());
		return websiteWorkflow;
	}

	public static WebsiteWorkflowInvocation createEscWorkflowInvocation(WorkflowInvocationFolder folder) {
		WebsiteWorkflowInvocation websiteWorkflowInvocation = new WebsiteWorkflowInvocation();
		websiteWorkflowInvocation.setId(folder.getId());
		websiteWorkflowInvocation.setName(folder.getName());
		websiteWorkflowInvocation.setContainerId(folder.getContainerId());
		websiteWorkflowInvocation.setDescription(folder.getDescription());
		websiteWorkflowInvocation.setCreatorId(folder.getCreatorId());
		websiteWorkflowInvocation.setWorkflowId(folder.getWorkflowId());
		websiteWorkflowInvocation.setWorkflowVersionId(folder.getVersionId());
		websiteWorkflowInvocation.setProjectId(folder.getProjectId());
		switch (folder.getInvocationStatus()) {
			case WorkflowInvocationFolder.INVOCATION_FINISHED_OK:
				websiteWorkflowInvocation.setStatus("Finished");
				break;

			case WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS:
				websiteWorkflowInvocation.setStatus("ExecutionError");
				break;

			case WorkflowInvocationFolder.INVOCATION_RUNNING:
				websiteWorkflowInvocation.setStatus("Running");
				break;

			case WorkflowInvocationFolder.INVOCATION_WAITING:
				websiteWorkflowInvocation.setStatus("Queued");
				break;

			case WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER:
				websiteWorkflowInvocation.setStatus("Debugging");
				break;

			default:
				websiteWorkflowInvocation.setStatus("Unknown");
		}

		return websiteWorkflowInvocation;
	}

	public static WorkflowParameter createEscWorkflowParameter(WebsiteWorkflowParameter websiteWorkflowParameter) {
		WorkflowParameter p = new WorkflowParameter();
		p.setBlockName(websiteWorkflowParameter.getBlockName());
		p.setName(websiteWorkflowParameter.getName());
		p.setValue(websiteWorkflowParameter.getValue());
		return p;
	}

	public static WorkflowParameterList createEscWorkflowParameterList(WebsiteWorkflowParameterList websiteWorkflowParameterList) {
		WorkflowParameterList p = new WorkflowParameterList();
		WebsiteWorkflowParameter[] values = websiteWorkflowParameterList.getValues();
		for (int i = 0; i < values.length; i++) {
			p.add(values[i].getBlockName(), values[i].getName(), values[i].getValue());
		}
		return p;
	}
}

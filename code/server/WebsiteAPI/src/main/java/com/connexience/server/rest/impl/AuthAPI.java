/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.storage.MetaDataRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.Uploader;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.scanner.filesystems.AzureScanner;
import com.connexience.server.model.scanner.filesystems.S3Scanner;
import com.connexience.server.model.security.StoredCredentials;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.credentials.AmazonCredentials;
import com.connexience.server.model.security.credentials.AzureCredentials;
import com.connexience.server.rest.api.IAuthAPI;
import com.connexience.server.rest.api.IUsersAPI;
import com.connexience.server.rest.exceptions.ConnexienceWebApplicationException;
import com.connexience.server.rest.model.WebsiteSignupUser;
import com.connexience.server.rest.model.WebsiteUser;
import com.connexience.server.rest.model.project.UploadUserProject;
import com.connexience.server.rest.util.APIUtils;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.StorageUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * This class implements the user management in the website API. Listing,
 * updating, etc.
 * <p/>
 * User: nsjw7 Date: 29/11/2012 Time: 15:20
 */
@SuppressWarnings("unchecked")
@Path("/auth")
public class AuthAPI implements IAuthAPI {

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/MetaDataBean")
    MetaDataRemote metaDataBean;

    @EJB(lookup = "java:global/esc/server-beans/GroupDirectoryBean")
    GroupDirectoryRemote groupDirectoryBean;

    @EJB(lookup = "java:global/esc/server-beans/UserDirectoryBean")
    UserDirectoryRemote userDirectoryBean;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    StorageRemote storageBean;




    @Override
    public List<UploadUserProject> authenticateUploadUser(@FormParam("userId") String username, @FormParam("password") String password) {
        System.out.println("UsersAPI.authenticateUploadUser");

        try {
            //try to get and authenticate the upload user.  Will throw an exception if either the user doesn't exist or
            //the password is incorrect
            Uploader uploader = EJBLocator.lookupUploaderBean().authenticateUploader(username, password);

            //get the list of projects for that upload user
            List<UploadUserProject> uProjects = new ArrayList<>();

            //Only show studies which are supposed to be visible on the external site.
            for (Project p : uploader.getProjects()) {
                if (p instanceof Study) {
                    Study s = (Study) p;
                    if (s.isVisibleOnExternalSite()) {

                        //check that the study is active at present
                        Date startDate = s.getStartDate();
                        Date endDate = s.getEndDate();
                        Date now = new Date();
                        if (now.after(startDate) && now.before(endDate)) {

                            //Get the scanner which is associated with the project
                            //Use a ticket from the project owner to be able to retrieve the scanner
                            Long scannerId = p.getRemoteScannerId();
                            if (scannerId != null) {
                                Ticket projectOwnerTicket = EJBLocator.lookupTicketBean().createWebTicketForDatabaseId(p.getOwnerId());
                                RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(projectOwnerTicket, scannerId);

                                if (scanner instanceof AzureScanner) {
                                    AzureScanner azureScanner = (AzureScanner) scanner;

                                    //Get the credentials object for this scanner
                                    String credentialsId = azureScanner.getCredentialsId();

                                    if (credentialsId != null && !credentialsId.isEmpty()) {
                                        StoredCredentials credentials = EJBLocator.lookupCredentialsDirectoryBean().getCredentials(projectOwnerTicket, credentialsId);

                                        //Get the key, name and container from the credentials
                                        AzureCredentials azureCredentials = (AzureCredentials) credentials;
                                        String key = azureCredentials.getAccountKey();
                                        String accountName = azureCredentials.getAccountName();
                                        String container = azureScanner.getContainerName();

                                        //Get the list of Admins for the group
                                        List<User> projectAdmins = groupDirectoryBean.listGroupMembers(projectOwnerTicket, p.getAdminGroupId());
                                        List<String> projectAdminsIds = new ArrayList<>();
                                        for (User admin : projectAdmins) {
                                            projectAdminsIds.add(admin.getId());
                                        }

                                        //Put the credentials etc into the DTO for the project and add it to the list
                                        UploadUserProject up = new UploadUserProject(String.valueOf(p.getId()), p.getName(), accountName, key, container, p.getDescription(), projectAdminsIds);
                                        up.setScannerType("azure");
                                        uProjects.add(up);
                                    } else {

                                        //Todo: How to pass this error back to the user?
                                        System.err.println("Project: " + p.getName() + "[" + p.getId() + "] Has no cloud credentials set on the scanner");
                                    }

                                } else if (scanner instanceof S3Scanner) {
                                    S3Scanner s3Scanner = (S3Scanner) scanner;

                                    String credentialsId = s3Scanner.getCredentialsId();
                                    if (credentialsId != null && !credentialsId.isEmpty()) {
                                        StoredCredentials credentials = EJBLocator.lookupCredentialsDirectoryBean().getCredentials(projectOwnerTicket, credentialsId);

                                        //Get the key, name and container from the credentials
                                        AmazonCredentials amazonCredentials = (AmazonCredentials) credentials;
                                        String key = amazonCredentials.getAccessKey();
                                        String accountName = amazonCredentials.getAccessKeyId();
                                        String container = s3Scanner.getBucketName();

                                        //Get the list of Admins for the group
                                        List<User> projectAdmins = groupDirectoryBean.listGroupMembers(projectOwnerTicket, p.getAdminGroupId());
                                        List<String> projectAdminsIds = new ArrayList<>();
                                        for (User admin : projectAdmins) {
                                            projectAdminsIds.add(admin.getId());
                                        }

                                        //Put the credentials etc into the DTO for the project and add it to the list
                                        UploadUserProject up = new UploadUserProject(String.valueOf(p.getId()), p.getName(), accountName, key, container, p.getDescription(), projectAdminsIds);
                                        up.setScannerType("s3");
                                        uProjects.add(up);

                                    } else {
                                        System.err.println("Project: " + p.getName() + "[" + p.getId() + "] Has no cloud credentials set on the scanner");
                                    }
                                } else {
                                    throw new ConnexienceException("Unsupported Scanner type: " + scanner.getTypeName());
                                }
                            } else {
                                System.out.println("Upload user trying to log in but Project " + p.getName() + " has no scanner");
                            }
                        }
                    }
                }

            }
            return uProjects;
        } catch (ConnexienceException e) {
            e.printStackTrace();
            throw new WebApplicationException(HttpURLConnection.HTTP_UNAUTHORIZED);
        }
    }
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.provenance.client.ProvenanceLoggerClient;
import com.connexience.provenance.model.logging.events.LogonEvent;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.RememberMeLogin;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.rest.WebUtil;
import com.connexience.server.rest.api.ILogonAPI;
import com.connexience.server.rest.JsonWebToken;
import com.connexience.server.rest.SignatureAlgorithm;
import com.connexience.server.rest.TokenSigner;
import com.connexience.server.rest.util.AuthUtils;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.SessionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/rest/account")
public class LogonAPI implements ILogonAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@Override
	public Response token(@FormParam("username") String username, @FormParam("password") String password, @QueryParam("expires") @DefaultValue("86400") Long expires) {

        System.out.println(username + " : " + password);

        try {
			final User user = EJBLocator.lookupUserDirectoryBean().authenticateUser(username, password);
			return generateJwtResponse(user, expires);
		} catch (ConnexienceException e) {
			final String response = "{\"msg\":\"Could not authenticate credentials\",\"cause\": \"" + e.getMessage() + "\"}";
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}
	}

	@Override
	public Response token(@PathParam("obo") String oboId, @QueryParam("expires") @DefaultValue("86400") Long expires) {
		try {
			final Ticket ticket = TransitionSessionUtils.getTicket(request);

			// Check the requester is an admin before issuing a token on behalf of (obo) another user
			if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())) {
				final User oboUser = EJBLocator.lookupUserDirectoryBean().getUser(ticket, oboId);
				return generateJwtResponse(oboUser, expires);
			} else {
				final String response = "{\"msg\":\"You must be an admin to acquire a token on behalf of another User.\"}";
				return Response.status(Response.Status.FORBIDDEN).entity(response).build();
			}
		} catch (ConnexienceException e) {
			final String response = "{\"msg\":\"Could not build a token on behalf of another User.\",cause: \"" + e.getMessage() + "\"}";
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}
	}

	@Override
	public Response token() {
		if (request.getAttribute("com.connexience.jwt") != null) {
			JsonWebToken token = (JsonWebToken) request.getAttribute("com.connexience.jwt");
			return Response.status(Response.Status.OK).entity(token.getPayloadJson()).build();
		}

		return Response.status(Response.Status.FORBIDDEN).entity("{\"msg\":\"You must provide a valid Authorization header.\"}").build();
	}

	@Override
	public Response login(@FormParam("username") String username, @FormParam("password") String password) throws Exception {

		if (SessionUtils.login(request, username, password)) {
			String sessionId = request.getSession().getId();
            LogonEvent logonEvent = new LogonEvent(LogonEvent.API_LOGON, username, WebUtil.getClientIP(request));
            ProvenanceLoggerClient client = new ProvenanceLoggerClient();
            client.log(logonEvent);

			return Response.status(Response.Status.OK).entity("{\"sessionId\":\"" + sessionId + "\"}").build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).entity("Incorrect Username or Password").build();
		}
	}

        @Override
        public Response switchSession(@PathParam("userid")String userId) throws Exception {
            Ticket t = SessionUtils.getTicket(request);
            if(t.isAssociatedWithNonRootOrg() && EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(t, t.getUserId(), t.getOrganisationId())){
                SessionUtils.login(request, userId);
                String sessionId = request.getSession().getId();
                LogonEvent logonEvent = new LogonEvent(LogonEvent.API_LOGON, userId, WebUtil.getClientIP(request));
                ProvenanceLoggerClient client = new ProvenanceLoggerClient();
                client.log(logonEvent);
                return Response.status(Response.Status.OK).entity("{\"sessionId\":\"" + sessionId + "\"}").build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("Cannot switch session").build();
            }
        }

	@Override
	public Response loginWithRememberMe(@FormParam("rememberMeCookieValue") String rememberMeCookie) throws Exception {
		RememberMeLogin r = EJBLocator.lookupTicketBean().checkRememberMe(rememberMeCookie);
		if (r != null) {
			//log the user in
			SessionUtils.login(request, r.getUserId());
			String sessionId = request.getSession().getId();

            LogonEvent logonEvent = new LogonEvent(LogonEvent.REMEMBERED_LOGON, ((User)request.getSession().getAttribute("USER")).getDisplayName(), WebUtil.getClientIP(request));
            ProvenanceLoggerClient client = new ProvenanceLoggerClient();
            client.log(logonEvent);

			NewCookie cookie = new NewCookie("JSESSIONID", sessionId, "/website-api", "localhost", "", 0, false);

			return Response.status(Response.Status.OK).entity("{\"sessionId\":\"" + sessionId + "\"}").cookie(cookie).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).entity("Incorrect RememberMe Login").build();
		}
	}

	@Override
	public Response logout() throws Exception {
		SessionUtils.logout(request);
		return Response.status(Response.Status.OK).build();
	}

	@Override
	public Response isNonPublicUser() throws Exception {
		String publicUserId = TransitionSessionUtils.getOrganisation(request).getDefaultUserId();
		String thisUserId = TransitionSessionUtils.getTicket(request).getUserId();

		if (!publicUserId.equals(thisUserId)) {
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	private Response generateJwtResponse(User userId, Long expires) throws ConnexienceException {
		final JsonWebToken token = generatePlainToken(userId.getId(), expires);
		final String response = generateSignedToken(token);

		return Response.status(Response.Status.OK).entity("{\"jwt\":\"" + response + "\"}").build();
	}

	private JsonWebToken generatePlainToken(final String userId, final Long expires) {
		JsonWebToken token = new JsonWebToken(SignatureAlgorithm.HS256);

		token.setIssuer("esc");
		token.setAudience(request.getServerName());
		token.setSubject(userId);

		// TODO: Potential to invalidate token by ID
		token.setJwtId(UUID.randomUUID().toString());

		token.setExpiration((System.currentTimeMillis() / 1000 + expires));
		token.setNotBefore(System.currentTimeMillis() / 1000);

		return token;
	}

	private String generateSignedToken(JsonWebToken token) throws ConnexienceException {
		final byte[] key = AuthUtils.getOrganisationKey();

		return new TokenSigner(SignatureAlgorithm.HS256, key).sign(token).toString();
	}
}

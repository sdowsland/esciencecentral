/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.social.MessageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.rest.api.IMessagesAPI;
import com.connexience.server.rest.model.WebsiteMessage;
import com.connexience.server.rest.model.WebsiteMessageSummary;
import com.connexience.server.rest.model.WebsiteUserMin;
import com.connexience.server.rest.util.TransitionSessionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Methods of the API relating to messaging and notifications
 * <p/>
 * User: nsjw7 Date: 06/12/2012 Time: 14:58
 */
@Path("/rest/messages")
public class MessagesAPI implements IMessagesAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	private static final String MSG_SUMMARY = "MSG_SUMMARY";

	public WebsiteMessageSummary getSummary() throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);
		HttpSession session = request.getSession();
		if (session.getAttribute(MSG_SUMMARY) == null) {
			WebsiteMessageSummary summary = new WebsiteMessageSummary();

			MessageRemote msgBean = EJBLocator.lookupMessageBean();
			Long numMessages = msgBean.getNumberOfUnreadMessages(ticket);
			summary.setNumNewMessages(numMessages.intValue());

			session.setAttribute(MSG_SUMMARY, summary);
			//todo: implement other message types
			return summary;
		} else {
			return (WebsiteMessageSummary) session.getAttribute(MSG_SUMMARY);
		}
	}

	public List<WebsiteMessage> getMessages(int start, int numResults) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		//remove the summary from the session as we're reading messages
		HttpSession session = request.getSession();
		if (session.getAttribute(MSG_SUMMARY) != null) {
			session.removeAttribute(MSG_SUMMARY);
		}

		User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
		Collection<Message> messages = EJBLocator.lookupMessageBean().getMessages(ticket, ticket.getUserId(), user.getInboxFolderId(), start, numResults);

		List<WebsiteMessage> wsMessages = new ArrayList<>();
		for (Message msg : messages) {
			WebsiteMessage wsMessage = new WebsiteMessage();
			wsMessage.setId(msg.getId());
			wsMessage.setTimestamp(msg.getTimestamp());
			wsMessage.setSender(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, msg.getSenderId()), false, ticket));
            //todo: Deal with potentila multiple recipients
			wsMessage.setRecipient(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, msg.getRecipientId().split(" ")[0]), false, ticket));

			wsMessages.add(wsMessage);
		}

		return wsMessages;
	}

    //todo the following method probably needs to be simplified and more logic pushed into the lower layers
    /**
     *
     * @param userId
     * @param threadId
     * @param otherUsersIds Space separated list of other users to send the message to
     * @param title
     * @param message
     */
    @Override
    public void sendMessage(String userId, String threadId, String otherUsersIds, String title, String message){
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            EJBLocator.lookupMessageBean().createTextMessage(ticket, userId, otherUsersIds, threadId, title, message);
        } catch (ConnexienceException e) {
            e.printStackTrace();
        }
    }
}

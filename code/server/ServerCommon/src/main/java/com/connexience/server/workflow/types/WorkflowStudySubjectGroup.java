/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.types;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * Workflow accessible subject group
 * @author hugo
 */
public class WorkflowStudySubjectGroup extends WorkflowStudyObject {
    private String displayName = "";
    private String categoryName = "";
    private String dataFolderId = "";

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public WorkflowStudySubjectGroup() {
        super();
    }

    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("DisplayName", displayName);
        store.add("CategoryName", categoryName);
        store.add("DataFolderID", dataFolderId);
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store);
        displayName = store.stringValue("DisplayName", "");
        categoryName = store.stringValue("CategoryName", "");
        dataFolderId = store.stringValue("DataFolderID", "");
    }
}
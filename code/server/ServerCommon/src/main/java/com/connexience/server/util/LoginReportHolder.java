package com.connexience.server.util;

import sun.rmi.runtime.Log;

/**
 *  Hold a LoginReport in a ThreadLocal so that it can be passed back to the
 *  login page following a failed login
 * User: nsjw7
 * Date: 31/03/15
 * Time: 11:25
 */
public class LoginReportHolder {

    //Threadlocal to pass back the reason to the calling code
    private static ThreadLocal<LoginReport> loginReportHolder = new ThreadLocal();

    public static void setLoginReport (LoginReport loginReport){
        loginReportHolder.set(loginReport);
    }

    public static LoginReport getLoginReport(){
        return loginReportHolder.get();
    }

    public static void clearLoginReport(){
        loginReportHolder.remove();
    }

}

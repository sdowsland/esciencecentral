/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.workflow.types.WorkflowStudy;
import com.connexience.server.workflow.types.WorkflowStudyDeployment;
import com.connexience.server.workflow.types.WorkflowStudySubjectGroup;
import com.connexience.server.workflow.types.WorkflowStudyLogger;
import com.connexience.server.workflow.types.WorkflowStudyObject;
import com.connexience.server.workflow.types.WorkflowStudyPhase;
import com.connexience.server.workflow.types.WorkflowStudySubject;
import java.util.List;
import javax.ejb.Remote;

/**
 * Provides access to study data for the workflow engine
 * @author hugo
 */
@Remote
public interface WorkflowStudyRemote {
    public WorkflowStudy getStudy(Ticket ticket, int studyId) throws ConnexienceException;
    public List<WorkflowStudyPhase> listStudyPhases(Ticket ticket, int studyId) throws ConnexienceException;
    public List<WorkflowStudySubjectGroup> listPhaseGroups(Ticket ticket, int phaseId) throws ConnexienceException;
    public List<WorkflowStudySubjectGroup> listChildGroups(Ticket ticket, int parentGroupId) throws ConnexienceException;
    public List<WorkflowStudySubject> listGroupSubjects(Ticket ticket, int parentGroupId) throws ConnexienceException;
    public List<WorkflowStudyDeployment> listGroupDeployments(Ticket ticket, int parentGroupId) throws ConnexienceException;
    public WorkflowStudyLogger getLogger(Ticket ticket, int loggerId) throws ConnexienceException;
    public WorkflowStudyObject getStudyObjectAssociatedWithFolder(Ticket ticket, String folderId) throws ConnexienceException;
    public void updateStudyObjectProperties(Ticket ticket, WorkflowStudyObject object) throws ConnexienceException;
}
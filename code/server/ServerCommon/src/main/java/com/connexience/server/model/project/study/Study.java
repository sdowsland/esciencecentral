/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.study;

import com.connexience.server.model.project.Project;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("STUDY")
@NamedQueries({
	// All studies whose visibility is public
	@NamedQuery(name = "Study.public.name.asc", query = "SELECT s FROM Study s WHERE s.privateProject = false ORDER BY s.name ASC"),
    @NamedQuery(name = "Study.public.name.desc", query = "SELECT s FROM Study s WHERE s.privateProject = false ORDER BY s.name DESC"),
    @NamedQuery(name = "Study.public.date.asc", query = "SELECT s FROM Study s WHERE s.privateProject = false ORDER BY s.startDate ASC"),
    @NamedQuery(name = "Study.public.date.desc", query = "SELECT s FROM Study s WHERE s.privateProject = false ORDER BY s.startDate DESC"),
	@NamedQuery(name = "Study.public.count", query = "SELECT COUNT(*) FROM Study s WHERE s.privateProject = false"),

	// All studies whose associated groups are in the specified list
	@NamedQuery(name = "Study.member.name.asc", query = "SELECT s FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships ORDER BY s.name ASC"),
    @NamedQuery(name = "Study.member.name.desc", query = "SELECT s FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships ORDER BY s.name DESC"),
    @NamedQuery(name = "Study.member.date.asc", query = "SELECT s FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships ORDER BY s.startDate ASC"),
    @NamedQuery(name = "Study.member.date.desc", query = "SELECT s FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships ORDER BY s.startDate DESC"),
	@NamedQuery(name = "Study.member.count", query = "SELECT COUNT(*) FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships"),

	//Active studies are in the right date range
	@NamedQuery(name = "Study.member.active.name.asc", query = "SELECT s FROM Study s WHERE (s.adminGroupId IN (:groupMemberships) OR s.membersGroupId IN (:groupMemberships)) AND s.startDate <= CURRENT_DATE AND s.endDate > CURRENT_DATE ORDER BY s.name ASC"),
    @NamedQuery(name = "Study.member.active.name.desc", query = "SELECT s FROM Study s WHERE (s.adminGroupId IN (:groupMemberships) OR s.membersGroupId IN (:groupMemberships)) AND s.startDate <= CURRENT_DATE AND s.endDate > CURRENT_DATE ORDER BY s.name DESC"),
    @NamedQuery(name = "Study.member.active.date.asc", query = "SELECT s FROM Study s WHERE (s.adminGroupId IN (:groupMemberships) OR s.membersGroupId IN (:groupMemberships)) AND s.startDate <= CURRENT_DATE AND s.endDate > CURRENT_DATE ORDER BY s.startDate ASC"),
    @NamedQuery(name = "Study.member.active.date.desc", query = "SELECT s FROM Study s WHERE (s.adminGroupId IN (:groupMemberships) OR s.membersGroupId IN (:groupMemberships)) AND s.startDate <= CURRENT_DATE AND s.endDate > CURRENT_DATE ORDER BY s.startDate DESC"),
    @NamedQuery(name = "Study.member.count.active", query = "SELECT COUNT(*) FROM Study s WHERE (s.adminGroupId IN (:groupMemberships) OR s.membersGroupId IN (:groupMemberships)) AND s.startDate <= CURRENT_DATE AND s.endDate > CURRENT_DATE"),

	// All studies whose associated groups are in the specified list or are public
	@NamedQuery(name = "Study.visible", query = "SELECT s FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships OR s.privateProject = false"),
        @NamedQuery(name = "Study.all.studies", query = "SELECT s FROM Study s"),
	@NamedQuery(name = "Study.visible.count", query = "SELECT COUNT(*) FROM Study s WHERE s.adminGroupId IN :groupMemberships OR s.membersGroupId IN :groupMemberships OR s.privateProject = false")
})
public class Study extends Project implements Serializable
{
	private Date startDate;

	private Date endDate;

    private boolean visibleOnExternalSite = false;

	@OneToMany(mappedBy = "study", cascade = CascadeType.REMOVE)
	private List<Phase> phases = new ArrayList<>();

//	@OneToMany(mappedBy = "study", cascade = CascadeType.REMOVE)
//	private List<SubjectGroup> subjectGroups = new ArrayList<>();

	@OneToMany(mappedBy = "study", cascade = CascadeType.REMOVE)
	private List<LoggerDeployment> loggerDeployments = new ArrayList<>();

	protected Study()
	{
	}

	public Study(final String name, final String ownerId)
	{
		super(name, ownerId);
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public List<Phase> getPhases() {
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	public void addPhase(final Phase phase)
	{
		if (!phases.contains(phase))
		{
			phases.add(phase);
		}

		phase.setStudy(this);
	}

	public void removePhase(final Phase phase)
	{
		phases.remove(phase);
		phase.setStudy(null);
	}

//	public List<SubjectGroup> getSubjectGroups()
//	{
//		return subjectGroups;
//	}

    public boolean isVisibleOnExternalSite() {
        return visibleOnExternalSite;
    }

    public void setVisibleOnExternalSite(boolean visibleOnExternalSite) {
        this.visibleOnExternalSite = visibleOnExternalSite;
    }

//    public void addSubjectGroup(final SubjectGroup subjectGroup)
//	{
//		if (!subjectGroups.contains(subjectGroup))
//		{
//			subjectGroups.add(subjectGroup);
//		}
//
//		subjectGroup.setStudy(this);
//	}
//
//	public void removeSubjectGroup(final SubjectGroup subjectGroup)
//	{
//		subjectGroups.remove(subjectGroup);
//		subjectGroup.setStudy(null);
//	}

	public List<LoggerDeployment> getLoggerDeployments()
	{
		return loggerDeployments;
	}

	public void addLoggerDeployment(final LoggerDeployment loggerDeployment)
	{
		if (!loggerDeployments.contains(loggerDeployment))
		{
			loggerDeployments.add(loggerDeployment);
		}

		loggerDeployment.setStudy(this);
	}

	public void removeLoggerDeployment(final LoggerDeployment loggerDeployment)
	{
		loggerDeployments.remove(loggerDeployment);
		loggerDeployment.setStudy(null);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Study)) return false;

		Study study = (Study) o;

		if (visibleOnExternalSite != study.visibleOnExternalSite) return false;
		if (startDate != null ? !startDate.equals(study.startDate) : study.startDate != null) return false;
		if (endDate != null ? !endDate.equals(study.endDate) : study.endDate != null) return false;
		if (phases != null ? !phases.equals(study.phases) : study.phases != null) return false;
		return !(loggerDeployments != null ? !loggerDeployments.equals(study.loggerDeployments) : study.loggerDeployments != null);

	}

	@Override
	public int hashCode() {
		int result = startDate != null ? startDate.hashCode() : 0;
		result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
		result = 31 * result + (visibleOnExternalSite ? 1 : 0);
		result = 31 * result + (phases != null ? phases.hashCode() : 0);
		result = 31 * result + (loggerDeployments != null ? loggerDeployments.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "Study{" +
			"startDate=" + startDate +
			", endDate=" + endDate +
            ", visibleOnExternalSite" + visibleOnExternalSite +
			"} " + super.toString();
	}
}

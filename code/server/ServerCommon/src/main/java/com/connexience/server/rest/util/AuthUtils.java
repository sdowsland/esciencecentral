package com.connexience.server.rest.util;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;

public class AuthUtils {
	private static byte[] organisationKey = null;

	private AuthUtils() {}

	public static byte[] getOrganisationKey() {
		try {
			if (organisationKey == null) {
				organisationKey = EJBLocator.lookupCertificateBean().getOrganisationKey();
			}
		} catch (ConnexienceException e) {
			return null;
		}

		return organisationKey;
	}
}

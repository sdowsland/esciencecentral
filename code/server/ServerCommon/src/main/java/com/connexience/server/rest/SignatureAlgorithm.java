package com.connexience.server.rest;


public enum SignatureAlgorithm
{
	HS256 ("HmacSHA256");

	private final String algorithmName;

	SignatureAlgorithm(final String algorithmName)
	{
		this.algorithmName = algorithmName;
	}

	public String getAlgorithmName()
	{
		return algorithmName;
	}
}

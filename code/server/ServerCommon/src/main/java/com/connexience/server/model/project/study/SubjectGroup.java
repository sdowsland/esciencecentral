/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.study;

import com.connexience.server.util.SignatureUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * User: nsjw7 Date: 11/07/2013 Time: 11:48
 */
@Entity
@Table(name = "subjectgroups")
@NamedQueries({
    @NamedQuery(name="SubjectGroup.findByFolderId", query="SELECT s FROM SubjectGroup s WHERE s.dataFolderId = :folderId"),
		@NamedQuery(name="SubjectGroup.findTopLevelGroups", query="SELECT s FROM SubjectGroup s WHERE s.phase.id = :phaseId AND s.parent IS NULL")
})
public class SubjectGroup implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
	@GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
	private Integer id;

	private String displayName;

    private String categoryName;

	private String dataFolderId;

	@ManyToOne(fetch =  FetchType.LAZY)
	private Phase phase;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private SubjectGroup parent;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.REMOVE)
	private List<SubjectGroup> children = new ArrayList<>();

	@OneToMany(mappedBy = "subjectGroup", cascade = CascadeType.REMOVE)
	private List<LoggerDeployment> loggerDeployments = new ArrayList<>();

	@OneToMany(mappedBy = "subjectGroup", cascade = CascadeType.REMOVE)
	private List<Subject> subjects = new ArrayList<>();

	@ElementCollection(fetch = FetchType.LAZY)
	private Map<String, String> additionalProperties = new HashMap<>();

	protected SubjectGroup()
	{
	}

	public SubjectGroup(final String displayName)
	{
		this.displayName = displayName;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

    public String getCategoryName()
    {
        return categoryName;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

	public String getDataFolderId()
	{
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId)
	{
		this.dataFolderId = dataFolderId;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties)
	{
		//hash passwords if they contain both "password" and "hash"
		for(String propName: additionalProperties.keySet()){

			//Hash any fields that look like passwords.
			String value = SignatureUtils.hashPropertyIfPasswordField(propName, additionalProperties.get(propName));
			additionalProperties.put(propName, value);
		}
		this.additionalProperties = additionalProperties;
	}

	public String getAdditionalProperty(String key)
	{
		return additionalProperties.get(key);
	}

	public String putAdditionalProperty(String key, String value)
	{
		//Hash any fields that look like passwords.
		value = SignatureUtils.hashPropertyIfPasswordField(key, value);
		return additionalProperties.put(key, value);
	}

	public String removeAdditionalProperty(String key)
	{
		return additionalProperties.remove(key);
	}

	public SubjectGroup getParent()
	{
		return parent;
	}

	public void setParent(SubjectGroup parent)
	{
		// un-associate, if required
		if (this.parent != null && this.parent.getChildren().contains(this))
		{
			this.parent.getChildren().remove(this);
		}

		this.parent = parent;

		// re-associate, if required
		if (parent != null && !parent.getChildren().contains(this))
		{
			parent.getChildren().add(this);
		}
	}

	public List<SubjectGroup> getChildren()
	{
		return children;
	}

	public void addChild(final SubjectGroup child)
	{
		if (!children.contains(this))
		{
			children.add(child);
		}

		child.setParent(this);
	}

	public void removeChild(final SubjectGroup child)
	{
		children.remove(child);
		child.setParent(null);
	}

	public List<Subject> getSubjects()
	{
		return subjects;
	}

	public void addSubject(final Subject subject)
	{
		if (!subjects.contains(subject))
		{
			subjects.add(subject);
		}

		subject.setSubjectGroup(this);
	}

	public void removeSubject(final Subject subject)
	{
		subjects.remove(subject);
		subject.setSubjectGroup(null);
	}

	public List<LoggerDeployment> getLoggerDeployments()
	{
		return loggerDeployments;
	}

	public void addLoggerDeployment(final LoggerDeployment loggerDeployment)
	{
		if (!loggerDeployments.contains(loggerDeployment))
		{
			loggerDeployments.add(loggerDeployment);
		}

		loggerDeployment.setSubjectGroup(this);
	}

	public void removeLoggerDeployment(final LoggerDeployment loggerDeployment)
	{
		loggerDeployments.remove(loggerDeployment);
		loggerDeployment.setSubjectGroup(null);
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof SubjectGroup)) return false;

		SubjectGroup that = (SubjectGroup) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (dataFolderId != null ? !dataFolderId.equals(that.dataFolderId) : that.dataFolderId != null) return false;
		if (phase != null ? !phase.equals(that.phase) : that.phase != null) return false;
		return !(parent != null ? !parent.equals(that.parent) : that.parent != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
		result = 31 * result + (dataFolderId != null ? dataFolderId.hashCode() : 0);
		result = 31 * result + (phase != null ? phase.hashCode() : 0);
		result = 31 * result + (parent != null ? parent.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
			.append("id", id)
			.append("displayName", displayName)
			.append("dataFolderId", dataFolderId)
			.append("phase", phase)
			.append("parent", parent)
			.append("additionalProperties", additionalProperties)
			.toString();
	}
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.datasets;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.datasets.items.multiple.JsonMultipleValueItem;

import java.util.HashMap;

/**
 * This class provides a factory that can create storage engines for different
 * classes of dataset item
 * @author hugo
 */
public class DatasetItemStorageFactory {
    private static final HashMap<Class,Class> storageProviders = new HashMap<>();

    public static final void register(Class itemType, Class storageImplementation){
        storageProviders.put(itemType, storageImplementation);
    }
    
    public static final void unregister(Class itemType){
        storageProviders.remove(itemType);
    }
    
    public static boolean containsEngineForClass(Class itemType){
        return storageProviders.containsKey(itemType);
    }
    
    public static final DatasetItemStorage createStorage(Class itemType) throws ConnexienceException {
        if(storageProviders.containsKey(itemType)){
            try {
                return (DatasetItemStorage)storageProviders.get(itemType).newInstance();
            } catch (Exception e){
                throw new ConnexienceException("Error creating DatasetItem storage: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException("Cannot create storage for class: " + itemType.getClass().getName());
        }
    }
}
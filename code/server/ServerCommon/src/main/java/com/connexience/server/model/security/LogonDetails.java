/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.security;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represents a logon configuration for a User
 *
 * @author hugo
 */
public class LogonDetails implements Serializable {
    public static final String LOCKED_DUE_TO_INACTIVITY = "Inactivity";
    public static final String LOCKED_DUE_TO_PASSWORD_EXPIRY = "Expired";
    public static final String LOCKED_DUE_TO_FAILED_LOGONS = "MultipleFailedLogons";
    public static final String LOCKED_BY_ADMINISTRATOR = "LockedByAdmin";
    public static final String NOT_LOCKED = "";
    
    /**
     * ID of logon object
     */
    private long id;

    /**
     * Logon name. This is used as the database id
     */
    private String logonName;

    /**
     * User ID
     */
    private String userId;

    /**
     * Password
     */
    private String hashedPassword;

    /**
     * Is this account enabled
     */
    private boolean enabled = true;

    /**
     * Number of times this logon has failed
     */
    private int failureCount = 0;
    
    /**
     * Last failed logon time
     */
    private Date lastFailedLogon = null;
    
    /**
     * Last sucessful logon
     */
    private Date lastSucessfulLogon = null;
    
    /**
     * Last time logon was attempted
     */
    private Date lastLogonAttempt = null;
    
    /**
     * Time these credentials were created
     */
    private Date creationTime = new Date();
    
    /** 
     * Reason for locking
     */
    private String lockReason = NOT_LOCKED;
    
    /**
     * Time that this logon was locked
     */
    private Date lockTime = null;
    
    /**
     * Creates a new instance of LogonDetails
     */
    public LogonDetails() {
    }

    /**
     * Get the id of this logon object
     */
    public long getId() {
        return id;
    }

    /**
     * Set the id of this logon object
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the user logon name
     */
    public String getLogonName() {
        return logonName;
    }

    /**
     * Set the user logon name
     */
    public void setLogonName(String logonName) {
        this.logonName = logonName;
    }

    /**
     * Get the database id of the user
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Set the database id of the user
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Get the hashed password of this logon
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * Set the hashed password of this logon
     */
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(int failureCount) {
        this.failureCount = failureCount;
    }

    public Date getLastFailedLogon() {
        return lastFailedLogon;
    }

    public void setLastFailedLogon(Date lastFailedLogon) {
        this.lastFailedLogon = lastFailedLogon;
    }

    public Date getLastSucessfulLogon() {
        return lastSucessfulLogon;
    }

    public void setLastSucessfulLogon(Date lastSucessfulLogon) {
        this.lastSucessfulLogon = lastSucessfulLogon;
    }

    public Date getLastLogonAttempt() {
        return lastLogonAttempt;
    }

    public void setLastLogonAttempt(Date lastLogonAttempt) {
        this.lastLogonAttempt = lastLogonAttempt;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getLockReason() {
        return lockReason;
    }

    public void setLockReason(String lockReason) {
        this.lockReason = lockReason;
    }

    public Date getLockTime() {
        return lockTime;
    }

    public void setLockTime(Date lockTime) {
        this.lockTime = lockTime;
    }
}

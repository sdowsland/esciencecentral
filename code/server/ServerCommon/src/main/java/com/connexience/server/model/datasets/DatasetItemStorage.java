/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.datasets;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.datasets.items.MultipleValueItem;


/**
 * This interface defines a class that can persist and retrieve data set items
 * from an underlying storage system.
 * @author hugo
 */
public abstract class DatasetItemStorage {
    /** Does this storage require a Hibernate session */
    protected boolean requiresHibernateSession = false;
    
    public abstract void appendMultipleValueData(Object session, MultipleValueItem item, Object data) throws ConnexienceException;
    public abstract Object getMultipleValueData(Object session, MultipleValueItem item, int startIndex, int maxResults, String[] keys) throws ConnexienceException;
    public abstract Object getMultipleValueData(Object session, MultipleValueItem item, int startIndex, int maxResults) throws ConnexienceException;
    public abstract Object getMultipleValueDataRow(Object session, MultipleValueItem item, String rowId) throws ConnexienceException;
    public abstract Object getMultipleValueData(Object session, MultipleValueItem item) throws ConnexienceException;
    public abstract void updateMultipleValueData(Object session, MultipleValueItem item, String rowId, Object data) throws ConnexienceException;
    public abstract void removeMultipleValueDataRow(Object session, MultipleValueItem item, String rowId) throws ConnexienceException;
    public abstract void removeMultipleValueData(Object session, MultipleValueItem item) throws ConnexienceException;
    public abstract long getMultipleValueDataSize(Object session, MultipleValueItem item) throws ConnexienceException;
    public abstract void itemSaved(MultipleValueItem item) throws ConnexienceException;
    public abstract void itemDeleted(MultipleValueItem item) throws ConnexienceException;
}
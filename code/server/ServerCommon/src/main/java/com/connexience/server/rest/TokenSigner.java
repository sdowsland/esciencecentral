package com.connexience.server.rest;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class TokenSigner {
	private final SignatureAlgorithm signatureAlgorithm;

	private final Mac mac;

	public TokenSigner(final SignatureAlgorithm signatureAlgorithm, final byte[] secret) {
		this.signatureAlgorithm = signatureAlgorithm;

		try {
			mac = Mac.getInstance(signatureAlgorithm.getAlgorithmName());
			SecretKeySpec key = new SecretKeySpec(secret, signatureAlgorithm.getAlgorithmName());
			mac.init(key);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(String.format("Could not get %s Algorithm", signatureAlgorithm.getAlgorithmName()));
		} catch (InvalidKeyException e) {
			throw new IllegalArgumentException("Specified key was invalid.");
		}
	}

	public SignatureAlgorithm getSignatureAlgorithm() {
		return signatureAlgorithm;
	}

	public SignedJsonWebToken sign(JsonWebToken token) {
		final byte[] header = Base64.encodeBase64URLSafe(token.getHeaderJson().getBytes());
		final byte[] payload = Base64.encodeBase64URLSafe(token.getPayloadJson().getBytes());
		final byte[] signature = Base64.encodeBase64URLSafe(mac.doFinal(payload));
		mac.reset();

		return new SignedJsonWebToken(header, payload, signature);
	}

	public boolean isValid(SignedJsonWebToken token) {
		final byte[] signature = mac.doFinal(token.getEncodedPayload().getBytes());
		mac.reset();

		return Base64.encodeBase64URLSafeString(signature).equals(token.getEncodedSignature());
	}

	public JsonWebToken extractToken(SignedJsonWebToken token) {
		if (isValid(token)) {
			try {
				JSONObject header = new JSONObject(new String(Base64.decodeBase64(token.getHeader())));
				JSONObject payload = new JSONObject(new String(Base64.decodeBase64(token.getPayload())));

				return new JsonWebToken(header, payload);
			} catch (JSONException e) {
				throw new IllegalArgumentException("Could not parse header and payload into JSONObjects.", e);
			}
		} else {
			throw new IllegalArgumentException("JsonWebToken could not be verified.");
		}
	}
}

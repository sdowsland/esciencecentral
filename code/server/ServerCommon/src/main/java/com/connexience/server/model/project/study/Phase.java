/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.study;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: nsjw7 Date: 11/07/2013 Time: 11:48
 */
@Entity
@Table(name = "phases")
@NamedQueries({
		// All phases in a study
		@NamedQuery(name = "Phase.phasesInStudy", query = "SELECT p FROM Phase p WHERE p.study.id = :studyId "),
                @NamedQuery(name = "Phase.findFromFolderId", query = "SELECT p FROM Phase p WHERE p.dataFolderId = :folderId"),
		@NamedQuery(name = "Phase.namedPhase", query = "SELECT p FROM Phase p WHERE p.study.id = :studyId AND lower(p.name) = lower(:name)")
})
public class Phase implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
	@GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
	private Integer id;

	private String name;

	private String dataFolderId;

	@ManyToOne(fetch = FetchType.LAZY)
	private Study study;

	@OneToMany(mappedBy = "phase", cascade = CascadeType.REMOVE)
	private List<SubjectGroup> subjectGroups = new ArrayList<>();


	public Phase()
	{
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Study getStudy()
	{
		return study;
	}

	public void setStudy(Study study)
	{
		if (this.study != null && this.study.getPhases().contains(this))
		{
			this.study.getPhases().remove(this);
		}

		this.study = study;

		if (!study.getPhases().contains(this))
		{
			study.getPhases().add(this);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SubjectGroup> getSubjectGroups() {
		return subjectGroups;
	}

	public void setSubjectGroups(List<SubjectGroup> subjectGroups) {
		this.subjectGroups = subjectGroups;
	}

	public void addSubjectGroup(final SubjectGroup subjectGroup)
	{
		if (!subjectGroups.contains(subjectGroup))
		{
			subjectGroups.add(subjectGroup);
		}

		subjectGroup.setPhase(this);
	}

	public void removeSubjectGroup(final SubjectGroup subjectGroup)
	{
		subjectGroups.remove(subjectGroup);
		subjectGroup.setPhase(null);
	}

	public String getDataFolderId()
	{
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId)
	{
		this.dataFolderId = dataFolderId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Phase)) return false;

		Phase phase = (Phase) o;

		if (name != null ? !name.equals(phase.name) : phase.name != null) return false;
		return !(study != null ? !study.equals(phase.study) : phase.study != null);

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (study != null ? study.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
			.append("id", id)
			.append("name", name)
			.toString();
	}
}

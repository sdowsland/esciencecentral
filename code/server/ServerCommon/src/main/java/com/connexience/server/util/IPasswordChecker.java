package com.connexience.server.util;

import com.connexience.server.ConnexienceException;

/**
 * User: nsjw7
 * Date: 07/04/15
 * Time: 10:16
 */
public interface IPasswordChecker {

    boolean isHistoryCheckSupported();

    boolean isAcceptable() throws ConnexienceException;

    void setProposedPassword(String proposedPassword);

    void setUserId(String userId);

    String getRejectionMessage();

}

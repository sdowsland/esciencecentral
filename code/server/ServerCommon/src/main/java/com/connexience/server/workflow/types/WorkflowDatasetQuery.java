/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */


package com.connexience.server.workflow.types;

import com.connexience.server.model.datasets.DatasetQuery;
import com.connexience.server.model.datasets.DatasetQueryFactory;
import org.json.JSONObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class can wrap a dataset query into one that can be saved with workflows
 * without having to be on the classpath.
 * @author hugo
 */
public class WorkflowDatasetQuery extends DatasetQuery {
    private String queryClassname = "";
    private String queryJson;
    
    public WorkflowDatasetQuery() {
    }
    
    public void setQuery(DatasetQuery query) throws Exception{
        queryClassname = query.getClass().getName();
        queryJson = query.toJson().toString();
        setDatasetId(query.getDatasetId());
        setItemName(query.getItemName());
    }
    
    public DatasetQuery instantiateQuery() throws Exception {
        DatasetQuery q = (DatasetQuery)Class.forName(queryClassname).newInstance();
        q.readJson(new JSONObject(queryJson));
        return q;
    }

    @Override
    public JSONObject toJson() throws Exception {
        return new JSONObject(queryJson);
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        queryJson = json.toString();
    }

    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("InnerQueryJSON", queryJson);
        store.add("QueryClassname", queryClassname);
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store); 
        queryJson = store.stringValue("InnerQueryJSON", "");
        queryClassname = store.stringValue("QueryClassname", "");
    }
}
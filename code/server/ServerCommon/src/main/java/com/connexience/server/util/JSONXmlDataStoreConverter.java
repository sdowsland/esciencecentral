/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONObject;
import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlBooleanDataObject;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlDateDataObject;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlDoubleDataObject;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlIntegerDataObject;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlLongDataObject;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlStringDataObject;

/**
 * This class provides a JSONEditable interface to an XmlDataStore object
 * @author hugo
 */
public class JSONXmlDataStoreConverter implements JSONEditable {
    private XmlDataStore dataStore;
    private boolean dataChanged = false;
    
    public JSONXmlDataStoreConverter(XmlDataStore dataStore) {
        this.dataStore = dataStore;
    }
    
    @Override
    public JSONObject toJson() throws Exception {
        JSONObject properties = new JSONObject();
        String name;
        XmlDataObject value;
        for(Object n : dataStore.getNames()){
            name = (String)n;
            value = dataStore.get(name);
            if(value instanceof XmlStringDataObject){
                properties.put(name, ((XmlStringDataObject)value).stringValue());
                
            } else if(value instanceof XmlBooleanDataObject){
                properties.put(name, ((XmlBooleanDataObject)value).booleanValue());
                
            } else if(value instanceof XmlLongDataObject){
                properties.put(name, ((XmlLongDataObject)value).longValue());
                
            } else if(value instanceof XmlIntegerDataObject){
                properties.put(name, ((XmlIntegerDataObject)value).intValue());
                
            } else if(value instanceof XmlDoubleDataObject){
                properties.put(name, ((XmlDoubleDataObject)value).doubleValue());
                
            } else if(value instanceof XmlDateDataObject){
                properties.put(name, new JSONDate(((XmlDateDataObject)value).dateValue()));
                
            }
        }
        
        return properties;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        Iterator names = json.keys();
        String name;
        Object value;
        XmlDataObject existingObject;
        dataChanged = false;
        
        while(names.hasNext()){
            name = names.next().toString();

            if(dataStore.containsName(name)){
                existingObject = dataStore.get(name);
                if(existingObject instanceof XmlStringDataObject){
                    if(!((XmlStringDataObject)existingObject).stringValue().equals(json.getString(name))){
                        ((XmlStringDataObject)existingObject).setStringValue(json.getString(name));
                        dataChanged = true;
                    }
                        
                } else if(existingObject instanceof XmlBooleanDataObject){
                    if(((XmlBooleanDataObject)existingObject).booleanValue()!=json.getBoolean(name)){
                        ((XmlBooleanDataObject)existingObject).setBooleanValue(json.getBoolean(name));
                        dataChanged = true;
                    }

                } else if(existingObject instanceof XmlLongDataObject){
                    if(((XmlLongDataObject)existingObject).longValue()!=json.getLong(name)){
                        ((XmlLongDataObject)existingObject).setLongValue(json.getLong(name));
                        dataChanged = true;
                    }

                } else if(existingObject instanceof XmlIntegerDataObject){
                    if(((XmlIntegerDataObject)existingObject).intValue()!=json.getInt(name)){
                        ((XmlIntegerDataObject)existingObject).setIntValue(json.getInt(name));
                        dataChanged = true;
                    }

                } else if(existingObject instanceof XmlDoubleDataObject){
                    if(((XmlDoubleDataObject)existingObject).doubleValue()!=json.getDouble(name)){
                        ((XmlDoubleDataObject)existingObject).setDoubleValue(json.getDouble(name));
                        dataChanged = true;
                    }

                } else if(existingObject instanceof XmlDateDataObject){
                    Date newDate = JSONDate.createDate(json.getJSONObject(name));
                    if(((XmlDateDataObject)existingObject).dateValue().getTime()!=newDate.getTime()){
                        ((XmlDateDataObject)existingObject).setDateValue(newDate);
                        dataChanged = true;
                    }
                }                
            }
        }
    }

    public boolean isDataChanged() {
        return dataChanged;
    }
}
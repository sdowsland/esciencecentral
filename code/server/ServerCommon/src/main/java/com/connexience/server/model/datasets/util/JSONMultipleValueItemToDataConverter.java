/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.datasets.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;

/**
 * This class converts data returned by a dataset query into a standard Data
 * object.
 *
 * @author hugo
 */
public class JSONMultipleValueItemToDataConverter {

    private JSONObject json;

    public JSONMultipleValueItemToDataConverter(JSONObject json) {
        this.json = json;
    }

    public Data convert() throws Exception {
        Data results = new Data();
        JSONArray dataArray = json.getJSONArray("data");

        int rows = dataArray.length();
        Iterator keys;
        String stringValue;
        double doubleValue;
        Column col;
        String key;
        JSONObject rowJson;
        if (rows > 0) {
            // Get a sorted set of keys
            rowJson = dataArray.getJSONObject(0);
            keys = rowJson.keys();
            ArrayList<String> sortedKeys = new ArrayList<String>();
            while (keys.hasNext()) {
                sortedKeys.add(keys.next().toString());
            }
            Collections.sort(sortedKeys);
            keys = sortedKeys.iterator();

            // Create the columns with the correct names                
            while (keys.hasNext()) {
                key = keys.next().toString();
                if (!key.startsWith("_")) {
                    stringValue = rowJson.getString(key);
                    try {
                        doubleValue = Double.parseDouble(stringValue);
                        col = new DoubleColumn(key);
                        ((DoubleColumn) col).appendDoubleValue(doubleValue);

                    } catch (Exception e) {
                        col = new StringColumn(key);
                        ((StringColumn) col).appendStringValue(stringValue);
                    }
                    results.addColumn(col);
                }
            }

            // Now work through the rest of the data
            if (rows > 1) {
                for (int i = 0; i < rows; i++) {
                    rowJson = dataArray.getJSONObject(i);
                    keys = rowJson.keys();
                    while (keys.hasNext()) {
                        key = keys.next().toString();
                        if (!key.startsWith("_")) {
                            if (results.containsColumn(key)) {
                                col = results.column(key);
                                stringValue = rowJson.getString(key);
                                if (col instanceof NumericalColumn) {
                                    try {
                                        ((DoubleColumn) col).appendDoubleValue(Double.parseDouble(stringValue));
                                    } catch (Exception e) {
                                        col.appendObjectValue(MissingValue.get());
                                    }
                                } else {
                                    col.appendStringValue(stringValue);
                                }
                            }
                        }
                    }

                }
            }
            return results;
        } else {
            return results;
        }
    }
}

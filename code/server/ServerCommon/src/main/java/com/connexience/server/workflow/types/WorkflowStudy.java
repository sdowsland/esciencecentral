/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.types;

import java.util.Date;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * Provides a workflow accessible representation of a study
 * @author hugo
 */
public class WorkflowStudy extends WorkflowStudyObject{
    private String externalId = "";
    private String name = "";
    private String description = "";
    private String ownerId = "";
    private String adminGroupId = "";
    private String membersGroupId = ""; 
    private String dataFolderId = "";
    private String workflowFolderId = "";
    private Date startDate = new Date();
    private Date endDate = new Date();

    public WorkflowStudy() {
        super();
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getAdminGroupId() {
        return adminGroupId;
    }

    public void setAdminGroupId(String adminGroupId) {
        this.adminGroupId = adminGroupId;
    }

    public String getMembersGroupId() {
        return membersGroupId;
    }

    public void setMembersGroupId(String membersGroupId) {
        this.membersGroupId = membersGroupId;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public String getWorkflowFolderId() {
        return workflowFolderId;
    }

    public void setWorkflowFolderId(String workflowFolderId) {
        this.workflowFolderId = workflowFolderId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject(); 
        store.add("StartDate", startDate);
        store.add("EndDate", endDate);
        store.add("Description", description);
        store.add("Name", name);
        store.add("AdminGroupID", adminGroupId);
        store.add("DataFolderID", dataFolderId);
        store.add("ExternalID", externalId);
        store.add("MembersGroupID", membersGroupId);
        store.add("OwnerID", ownerId);
        store.add("WorkflowFolderID", workflowFolderId);        
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store);
        startDate = store.dateValue("StartDate", new Date());
        endDate = store.dateValue("EndDate", new Date());        
        description = store.stringValue("Description", "");
        name = store.stringValue("Name", "");
        adminGroupId = store.stringValue("AdminGroupID", "");
        dataFolderId = store.stringValue("DataFolderID", "");
        externalId = store.stringValue("ExternalID", "");
        membersGroupId = store.stringValue("MembersGroupID", "");
        ownerId = store.stringValue("OwnerID", "");
        workflowFolderId = store.stringValue("WorkflowFolderID", "");                
    }
}
/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.security.login;

import com.connexience.server.util.LdapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

import javax.naming.InitialContext;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import java.security.acl.Group;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

/**
 * Login module provides eSC authentication powered by an LDAP server for checking credentials.
 * <p/>
 * It differs from the JBoss provided LDAP Auth module because the role sets returned by {@link #getRoleSets()} correspond to eSC groups/roles, not domain ones.
 */
public class LdapLoginModule extends UsernamePasswordLoginModule {

    // Configure the below values in standalone.xml.
    // You'll find a commented out connexience-ldap security domain.

    // Default to localhost LDAP server
    private String host = "localhost:389";

    // Default domain of USERS
    private String defaultDomain = "USERS";

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
        super.initialize(subject, callbackHandler, sharedState, options);

        final String host = (String) options.get("ldap.host");
        if (StringUtils.isNotBlank(host)) {
            this.host = host;
        }

        final String domain = (String) options.get("ldap.domain");
        if (StringUtils.isNotBlank(domain)) {
            this.defaultDomain = domain;
        }

    }

    @Override
    protected String getUsersPassword() throws LoginException {
        return "";
    }

    @Override
    protected boolean validatePassword(String inputPassword, String expectedPassword) {
        // getUsername() may contain username and DOMAIN
        final String username = LdapUtils.extractUsername(getUsername());
        final String domain = LdapUtils.extractDomain(getUsername(), defaultDomain);

        return LdapUtils.validateCredentials(username, inputPassword, domain, host);
    }

    @Override
    protected Group[] getRoleSets() throws LoginException {
        // select name from objectsflat where id in(select groupid from groupmembership where userid='8a808282328c961d01328c9a99130000')
        SimpleGroup group = new SimpleGroup("Roles");

        // this has to sync up with the servlet
        // getUsername() may contain username and DOMAIN
        final String username = LdapUtils.extractUsername(getUsername());
        final String domain = LdapUtils.extractDomain(getUsername(), defaultDomain);

        final String id = LdapUtils.generateExternalLogonId(username, domain);

        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;

        try {
            c = getSQLConnection();
            s = c.prepareStatement("select name from objectsflat where id in(select groupid from groupmembership where userid in (select userid from externallogondetails where logonname=?))");
            s.setString(1, id);
            r = s.executeQuery();
            while (r.next()) {
                group.addMember(new SimplePrincipal(r.getString("name")));
            }
        } catch (Exception e) {
        } finally {
            try {
                r.close();
            } catch (Exception e) {
            }
            try {
                s.close();
            } catch (Exception e) {
            }
            try {
                c.close();
            } catch (Exception e) {
            }
        }


        return new Group[]{group};
    }

    /**
     * Get a database connection
     */
    private Connection getSQLConnection() throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup("java:jboss/datasources/ConnexienceDB");
        return source.getConnection();
    }
}

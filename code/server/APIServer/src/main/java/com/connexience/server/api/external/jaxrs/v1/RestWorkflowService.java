/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.api.external.jaxrs.v1;

import com.connexience.api.model.*;
import com.connexience.api.model.json.JSONObject;
import com.connexience.server.ConnexienceException;
import com.connexience.server.api.external.helpers.WorkflowHelper;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Ticket;
import java.util.HashMap;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import org.jboss.resteasy.annotations.Body;

/**
 * This class provides the REST endpoint for the publically accessible workflow
 * service.
 * @author hugo
 */
@Path("/public/rest/v1/workflow")
public class RestWorkflowService implements WorkflowInterface {
    @Context SecurityContext secContext;

    /** Create a ticket for the current security context */
    private Ticket getTicket() throws ConnexienceException {
        return EJBLocator.lookupTicketBean().createWebTicket(secContext.getUserPrincipal().getName());
    }

    @GET
    @Path("/workflows")
    @Produces("application/json")
    @Override
    public EscWorkflow[] listWorkflows() throws Exception {
        return new WorkflowHelper(getTicket()).listWorkflows();
    }

    @GET
    @Path("/workflows/shared")
    @Produces("application/json")
    @Override
    public EscWorkflow[] listSharedWorkflows() throws Exception {
        return new WorkflowHelper(getTicket()).listSharedWorkflows();
    }

    @GET
    @Path("/services")
    @Produces("application/json")
    @Override
    public EscWorkflowService[] listSharedServices() throws Exception {
        return new WorkflowHelper(getTicket()).listSharedServices();
    }

    
    @GET
    @Path("/project/{projectId}/workflows")
    @Produces("application/json")
    @Override
    public EscWorkflow[] listProjectWorkflows(@PathParam(value="projectId")String projectId) throws Exception {
        return new WorkflowHelper(getTicket()).listProjectWorkflows(projectId);
    }

    @GET
    @Path("/workflows/all")
    @Produces("application/json")
    @Override
    public EscWorkflow[] listAllWorkflows() throws Exception {
        return new WorkflowHelper(getTicket()).listAllWorkflows();
    }

    @GET
    @Path("/relatedinvocations/{documentId}")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation[] listInvocationsRelatedToDocument(@PathParam(value="documentId")String documentId) throws Exception {
        return new WorkflowHelper(getTicket()).listInvocationsRelatedToDocument(documentId);
    }

    @GET
    @Path("/callableworkflows")
    @Produces("application/json")
    @Override
    public EscWorkflow[] listCallableWorkflows() throws Exception {
        return new WorkflowHelper(getTicket()).listCallableWorkflows();
    }

    @GET
    @Path("/allcallableworkflows")
    @Produces("application/json")
    @Override
    public EscWorkflow[] listAllCallableWorkflows() throws Exception {
        return new WorkflowHelper(getTicket()).listAllCallableWorkflows();
    }

    @GET
    @Path("/callableworkflows/{id}/parameters")
    @Produces("application/json")
    @Override
    public HashMap<String, String> listCallableWorkflowParameters(@PathParam(value="id")String workflowId) throws Exception {
        return new WorkflowHelper(getTicket()).listCallableWorkflowParameters(workflowId);
    }

    @GET
    @Path("/callableworkflows/{id}/{versionid}/parameters")
    @Produces("application/json")
    @Override
    public HashMap<String, String> listCallableWorkflowParameters(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId) throws Exception {
        return new WorkflowHelper(getTicket()).listCallableWorkflowParameters(workflowId, versionId);
    }

    @GET
    @Path("/callableworkflows/{id}/parametersex")
    @Produces("application/json")
    @Override
    public HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(@PathParam(value="id")String workflowId) throws Exception {
        return new WorkflowHelper(getTicket()).listCallableWorkflowParametersEx(workflowId, null);
    }

    @GET
    @Path("/callableworkflows/{id}/{versionid}/parametersex")
    @Produces("application/json")
    @Override
    public HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId) throws Exception {
        return new WorkflowHelper(getTicket()).listCallableWorkflowParametersEx(workflowId, versionId);
    }

    @GET
    @Path("/workflows/{id}")
    @Produces("application/json")
    @Override
    public EscWorkflow getWorkflow(@PathParam(value="id")String workflowId) throws Exception {
        return new WorkflowHelper(getTicket()).getWorkflow(workflowId);
    }

    @POST
    @Path("/workflows")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public EscWorkflow saveWorkflow(EscWorkflow workflow) throws Exception {
        return new WorkflowHelper(getTicket()).saveWorkflow(workflow);
    }

    @DELETE
    @Path("/workflows/{id}")
    @Override
    public void deleteWorkflow(@PathParam(value="id")String workflowId) throws Exception {
        new WorkflowHelper(getTicket()).deleteWorkflow(workflowId);
    }

    @POST
    @Path("/deleteworkflow")
    @Consumes("text/plain")
    public void deleteWorkflowUsingPOST(@FormParam(value="id")String workflowId) throws Exception {
        new WorkflowHelper(getTicket()).deleteWorkflow(workflowId);
    }

    @POST
    @Path("/workflows/{id}/invoke")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation executeWorkflow(@PathParam(value="id")String workflowId) throws Exception {
        return new WorkflowHelper(getTicket()).executeWorkflow(workflowId);
    }

    @POST
    @Path("/workflows/{id}/{versionid}/invoke")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation executeWorkflow(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId) throws Exception {
        return new WorkflowHelper(getTicket()).executeWorkflow(workflowId, versionId);
    }

    @POST
    @Path("/workflows/{id}/docinvoke")
    @Produces("application/json")
    @Consumes("text/plain")
    @Override
    public EscWorkflowInvocation executeWorkflowOnDocument(@PathParam(value="id")String workflowId, String documentId) throws Exception {
        return new WorkflowHelper(getTicket()).executeWorkflowOnDocument(workflowId, documentId);
    }

    @POST
    @Path("/workflows/{id}/{versionid}/docinvoke")
    @Produces("application/json")
    @Consumes("text/plain")
    @Override
    public EscWorkflowInvocation executeWorkflowOnDocument(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId, String documentId) throws Exception {
        return new WorkflowHelper(getTicket()).executeWorkflowOnDocument(workflowId, versionId, documentId);
    }

    @POST
    @Path("/workflows/{id}/paraminvoke")
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation executeWorkflowWithParameters(@PathParam(value="id")String workflowId, EscWorkflowParameterList parameters) throws Exception {
        return new WorkflowHelper(getTicket()).executeWorkflowWithParameters(workflowId, parameters);
    }

    @POST
    @Path("/workflows/{id}/ptparaminvoke")
    @Consumes("text/plain")
    @Produces("application/json")
    public EscWorkflowInvocation executeWorkflowWithParameters(@PathParam(value="id")String workflowId, String parametersJson) throws Exception {
        EscWorkflowParameterList params = new EscWorkflowParameterList(new JSONObject(parametersJson));
        return new WorkflowHelper(getTicket()).executeWorkflowWithParameters(workflowId, params);
    }

    @POST
    @Path("/workflows/{id}/{versionid}/paraminvoke")
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation executeWorkflowWithParameters(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId, EscWorkflowParameterList parameters) throws Exception {
        return new WorkflowHelper(getTicket()).executeWorkflowWithParameters(workflowId, versionId, parameters);
    }

    @POST
    @Path("/workflows/{id}/{versionid}/ptparaminvoke")
    @Consumes("text/plain")
    @Produces("application/json")
    public EscWorkflowInvocation executeWorkflowWithParameters(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId, String parametersJson) throws Exception {
        EscWorkflowParameterList params = new EscWorkflowParameterList(new JSONObject(parametersJson));
        return new WorkflowHelper(getTicket()).executeWorkflowWithParameters(workflowId, versionId, params);
    }

    @POST
    @Path("/workflows/{id}/externalinvoke")
    @Consumes("text/plain")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation executeCallableWorkflow(@PathParam(value="id")String workflowId, String parametersJson) throws Exception {
        return new WorkflowHelper(getTicket()).executeCallableWorkflow(workflowId, null, parametersJson);
    }

    @POST
    @Path("/workflows/{id}/{versionid}/externalinvoke")
    @Consumes("text/plain")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation executeCallableWorkflow(@PathParam(value="id")String workflowId, @PathParam(value="versionid")String versionId, String parametersJson) throws Exception {
        return new WorkflowHelper(getTicket()).executeCallableWorkflow(workflowId, versionId, parametersJson);
    }

    @GET
    @Path("/workflows/{id}/invocations")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation[] listInvocationsOfWorkflow(@PathParam(value="id")String workflowId) throws Exception {
        return new WorkflowHelper(getTicket()).listInvocationsOfWorkflow(workflowId);
    }

    @GET
    @Path("/invocations/{id}")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation getInvocation(@PathParam(value="id")String invocationId) throws Exception {
        return new WorkflowHelper(getTicket()).getInvocation(invocationId);
    }

    @POST
    @Path("/invocations/{id}/terminate")
    @Produces("application/json")
    @Override
    public EscWorkflowInvocation terminateInvocation(@PathParam(value="id")String invocationId) throws Exception {
        return new WorkflowHelper(getTicket()).terminateInvocation(invocationId);
    }

    @GET
    @Path("/services/{id}")
    @Produces("application/json")
    @Override
    public EscWorkflowService getService(@PathParam(value="id")String serviceId) throws Exception {
        return new WorkflowHelper(getTicket()).getService(serviceId);
    }

    @POST
    @Path("/services")
    @Produces("application/json")
    @Consumes("application/json")
    @Override
    public EscWorkflowService saveService(EscWorkflowService service) throws Exception{
        return new WorkflowHelper(getTicket()).saveService(service);
    }

    @POST
    @Path("/ptservices")
    @Produces("application/json")
    @Consumes("text/plain")
    public EscWorkflowService saveService(String serviceJson) throws Exception {
        JSONObject json = new JSONObject(serviceJson);
        EscWorkflowService service = new EscWorkflowService(json);
        return new WorkflowHelper(getTicket()).saveService(service);
    }

    @GET
    @Path("/librariesbyname/{name}")
    @Produces("application/json")
    @Override
    public EscWorkflowLibrary getLibraryByName(@PathParam(value="name")String name) throws Exception {
        return new WorkflowHelper(getTicket()).getLibraryByName(name);
    }

    @POST
    @Path("/libraries")
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public EscWorkflowLibrary saveLibrary(EscWorkflowLibrary library) throws Exception {
        return new WorkflowHelper(getTicket()).saveLibrary(library);
    }

    @POST
    @Path("/ptlibraries")
    @Consumes("text/plain")
    @Produces("application/json")
    public EscWorkflowLibrary saveLibrary(String libraryJson) throws Exception {
        JSONObject json = new JSONObject(libraryJson);
        EscWorkflowLibrary library = new EscWorkflowLibrary(json);
        return new WorkflowHelper(getTicket()).saveLibrary(library);
    }

    @POST
    @Path("/workflows/{id}/enableexternaldata")
    @Consumes("text/plain")
    @Override
    public void enableWorkflowExternalDataSupport(@PathParam(value="id")String workflowId, String blockName) throws Exception {
        new WorkflowHelper(getTicket()).enableWorkflowExternalDataSupport(workflowId, blockName);
    }

    @POST
    @Path("/workflows/{id}/disableexternaldata")
    @Consumes("text/plain")
    @Override
    public void disableWorkflowExternalDataSupport(@PathParam(value="id")String workflowId) throws Exception {
        new WorkflowHelper(getTicket()).disableWorkflowExternalDataSupport(workflowId);
    }
}

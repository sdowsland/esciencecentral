/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.api.external.helpers;

import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import com.connexience.api.model.EscWorkflowLibrary;
import com.connexience.api.model.EscWorkflowParameterDesc;
import com.connexience.api.model.EscWorkflowParameterList;
import com.connexience.api.model.EscWorkflowService;
import com.connexience.api.model.WorkflowInterface;
import com.connexience.api.model.json.JSONArray;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.*;
import com.connexience.server.util.SerializationUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.json.CallableWorkflowParameterCreator;

import java.util.*;

import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import org.pipeline.core.drawing.model.DefaultDrawingModel;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.io.XmlDataStoreByteArrayIO;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamReader;

/**
 * This class provides a helper that supports the various workflow services.
 * @author hugo
 */
public class WorkflowHelper implements WorkflowInterface {
    Ticket t;

    public WorkflowHelper(Ticket t) {
        this.t = t;
    }

    @Override
    public EscWorkflow[] listWorkflows() throws Exception {
        return toEscWorkflows(WorkflowEJBLocator.lookupWorkflowManagementBean().listWorkflows(t));
    }

    @Override
    public EscWorkflow[] listSharedWorkflows() throws Exception {
        return toEscWorkflows(EJBLocator
                .lookupObjectDirectoryBean()
                .getExplicitlySharedObjectsUserHasAccesTo(t, WorkflowDocument.class, 0, -1));
    }

    @Override
    public EscWorkflowService[] listSharedServices() throws Exception {
        List results = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(t, t.getUserId(), DynamicWorkflowService.class, 0, -1);
        EscWorkflowService[] services = new EscWorkflowService[results.size()];
        for(int i=0;i<results.size();i++){
            services[i] = EscObjectFactory.createEscWorkflowService((DynamicWorkflowService)results.get(i));
        }
        return services;
    }

    
    @Override
    public EscWorkflow[] listProjectWorkflows(String projectId) throws Exception {
        return toEscWorkflows(EJBLocator
                .lookupObjectDirectoryBean()
                .getProjectObjectsOrderByTimeDesc(t, projectId, WorkflowDocument.class, 0, -1));
    }

    @Override
    public EscWorkflow[] listAllWorkflows() throws Exception {
        return toEscWorkflows(EJBLocator
                .lookupObjectDirectoryBean()
                .getAllObjectsUserHasAccessTo(t, t.getUserId(), WorkflowDocument.class, 0, -1));
    }


    @Override
    public EscWorkflowInvocation[] listInvocationsRelatedToDocument(String documentId) throws Exception {
        DocumentRecord source = EJBLocator.lookupStorageBean().getDocumentRecord(t, documentId);

        if (source != null) {
            Collection<ServerObject> results = EJBLocator.lookupLinkBean().getLinkedSourceObjects(t, source);
            ArrayList<EscWorkflowInvocation> wfs = new ArrayList<>();

            for (ServerObject o : results) {
                if (o instanceof WorkflowInvocationFolder) {
                    wfs.add(EscObjectFactory.createEscWorkflowInvocation( (WorkflowInvocationFolder)o, EJBLocator.lookupObjectInfoBean().getObjectName(t, ((WorkflowInvocationFolder)o).getWorkflowId())));
                }
            }

            return wfs.toArray(new EscWorkflowInvocation[0]);
        } else {
            return new EscWorkflowInvocation[0];
        }
    }


    @Override
    public EscWorkflow[] listCallableWorkflows() throws Exception {
        return toEscWorkflows(WorkflowEJBLocator.lookupWorkflowManagementBean().listExternallyCallableWorkflows(t));
    }


    @Override
    public EscWorkflow[] listAllCallableWorkflows() throws Exception {
        return toEscWorkflows(WorkflowEJBLocator.lookupWorkflowManagementBean().listAllVisibleExternallyCallableWorkflows(t));
    }


    @Override
    public HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(String workflowId) throws Exception {
        return listCallableWorkflowParametersEx(workflowId, null);
    }


    @Override
    public HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(String workflowId, String versionId) throws Exception {
        // [CALA] The next two lines will need a fix when isExternalService is moved to a class that can
        // hold a workflow document version (WorkflowDocumentVersion).
        WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
        if(workflow.isExternalService()){
            HashMap<String, WorkflowExtParameterDesc> paramDescs = WorkflowEJBLocator.lookupWorkflowManagementBean().getExternallyCallableWorkflowParametersEx(t, workflowId, versionId);
            HashMap<String, EscWorkflowParameterDesc> results = new HashMap<>();
            // Convert values from the internal to the API model
            for(Map.Entry<String, WorkflowExtParameterDesc> entry : paramDescs.entrySet()){
                WorkflowExtParameterDesc desc = entry.getValue();
                String[] tv = paramToString(desc.getType(), desc.getValue());

                EscWorkflowParameterDesc extDesc = new EscWorkflowParameterDesc(desc.getName(), tv[0], tv[1], desc.getDescription(), desc.getOptions());

                results.put(entry.getKey(), extDesc);
            }
            return results;
        } else {
            throw new Exception("Workflow: " + workflowId + " is not marked as externally callable");
        }
    }


    /**
     * Converts type and value of a parameter object into a String representation.
     * The return value is a two-element array where [0] is type name and [1] is the parameter value converted
     * into String.
     *
     * @param type
     * @param value
     * @return
     */
    private static String[] paramToString(Class<?> type, Object value)
    {
        if (ServerObject.class.isAssignableFrom(type)) {
            // Server objects get referred to by ID -- String
            return new String[]{String.class.getName(), ((ServerObject) value).getId()};
        }

        if (type.equals(StringListWrapper.class)) {
            // String array
            return new String[]{"String[]", new JSONArray(((StringListWrapper) value).toStringArray()).toString()};
        }

        if (type.equals(StringPairListWrapper.class)) {
            // String 2d array
            return new String[]{"String[][]", new JSONArray(((StringPairListWrapper) value).toStringArray()).toString()};
        }

        // Simple type
        return new String[]{type.getName(), value.toString()};
    }


    @Override
    public HashMap<String, String> listCallableWorkflowParameters(String workflowId, String versionId) throws Exception {
        // [CALA] The next two lines will need a fix when isExternalService is moved to a class that can
        // hold a workflow document version (WorkflowDocumentVersion).
        WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
        if(workflow.isExternalService()){
            HashMap<String, Class> paramClasses = WorkflowEJBLocator.lookupWorkflowManagementBean().getExternallyCallableWorkflowParameters(t, workflowId, versionId);
            HashMap<String, String> results = new HashMap<>();
            Class value;
            for(String key : paramClasses.keySet()){
                value = paramClasses.get(key);
                if(com.connexience.server.model.ServerObject.class.isAssignableFrom(value)){
                    // Server objects get referred to by ID
                    results.put(key, String.class.getName());
                } else {
                    if(value.equals(com.connexience.server.workflow.xmlstorage.StringListWrapper.class)){
                        // String array
                        results.put(key, ArrayList.class.getName());

                    } else if(value.equals(com.connexience.server.workflow.xmlstorage.StringPairListWrapper.class)){
                        // Hashmap
                        results.put(key, HashMap.class.getName());

                    } else {
                        // Simple type
                        results.put(key, paramClasses.get(key).getName());
                    }
                }
            }
            return results;
        } else {
            throw new Exception("Workflow: " + workflowId + " is not marked as externally callable");
        }
    }


    @Override
    public HashMap<String, String> listCallableWorkflowParameters(String workflowId) throws Exception {
        WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
        if(workflow.isExternalService()){
            HashMap<String, Class> paramClasses = WorkflowEJBLocator.lookupWorkflowManagementBean().getExternallyCallableWorkflowParameters(t, workflowId);
            HashMap<String, String> results = new HashMap<>();
            Class value;
            for(String key : paramClasses.keySet()){
                value = paramClasses.get(key);
                if(com.connexience.server.model.ServerObject.class.isAssignableFrom(value)){
                    // Server objects get referred to by ID
                    results.put(key, String.class.getName());
                } else {
                    if(value.equals(com.connexience.server.workflow.xmlstorage.StringListWrapper.class)){
                        // String array
                        results.put(key, ArrayList.class.getName());
                    
                    } else if(value.equals(com.connexience.server.workflow.xmlstorage.StringPairListWrapper.class)){
                        // Hashmap
                        results.put(key, HashMap.class.getName());
                        
                    } else {
                        // Simple type
                        results.put(key, paramClasses.get(key).getName());
                    }
                }
                
            }
            return results;
        } else {
            throw new Exception("Workflow: " + workflowId + " is not marked as externally callable");
        }
    }
    
    
    @Override
    public EscWorkflow getWorkflow(String workflowId) throws Exception {
        WorkflowDocument wf = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
        if(wf!=null){
            return EscObjectFactory.createEscWorkflow(wf);
        } else {
            return null;
        }
    }

    @Override
    public EscWorkflow saveWorkflow(EscWorkflow workflow) throws Exception {
        WorkflowDocument existing = null;
        if(workflow.getId()!=null && !workflow.getId().isEmpty()){
            existing = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflow.getId());
        }
        
        if(existing==null){
            // New document
            existing = new WorkflowDocument();
            existing.setContainerId(workflow.getContainerId());
            existing.setCreatorId(t.getUserId());
            existing.setOrganisationId(t.getOrganisationId());
            existing = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(t, existing);
        }
        
        existing.setContainerId(workflow.getContainerId());
        existing.setName(workflow.getName());
        existing.setDescription(workflow.getDescription());
        return EscObjectFactory.createEscWorkflow(WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(t, existing));
    }

    @Override
    public void deleteWorkflow(String workflowId) throws Exception {
        WorkflowEJBLocator.lookupWorkflowManagementBean().deleteWorkflowDocument(t, workflowId);
    }

    @Override
    public EscWorkflowInvocation executeWorkflow(String workflowId) throws Exception {
        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(t, workflowId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
        return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflow(String workflowId, String versionId) throws Exception {
        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(t, workflowId, versionId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
        return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowOnDocument(String workflowId, String documentId) throws Exception {
        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(t, workflowId);
        msg.setTargetFileId(documentId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
        return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowOnDocument(String workflowId, String versionId, String documentId) throws Exception {
        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(t, workflowId, versionId);
        msg.setTargetFileId(documentId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
        return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowWithParameters(String workflowId, EscWorkflowParameterList parameters) throws Exception {
        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(t, workflowId);
        WorkflowParameterList params = EscObjectFactory.createEscWorkflowParameterList(parameters);
        msg.setParameterXmlData(SerializationUtils.serialize(params));
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
        return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId));
    }

    @Override
    public EscWorkflowInvocation executeWorkflowWithParameters(String workflowId, String versionId, EscWorkflowParameterList parameters) throws Exception {
        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(t, workflowId, versionId);
        WorkflowParameterList params = EscObjectFactory.createEscWorkflowParameterList(parameters);
        msg.setParameterXmlData(SerializationUtils.serialize(params));
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
        return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId)); 
    }

    @Override
    public EscWorkflowInvocation executeCallableWorkflow(String workflowId, String parametersJson) throws Exception {
        return executeCallableWorkflow(workflowId, null, parametersJson);
    }

    @Override
    public EscWorkflowInvocation executeCallableWorkflow(String workflowId, String versionId, String parametersJson) throws Exception {
        try {
            //Get an in memory representation of the workflow
            XmlDataStore drawingData;
            if (versionId == null || "latest".equals(versionId)) {
                WorkflowDocument doc = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
                XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(StorageUtils.getInputStream(t, doc, null));
                drawingData = reader.read();
            } else {
                byte[] workflowData = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocumentData(t, workflowId, versionId);
                drawingData = new XmlDataStoreByteArrayIO(workflowData).toXmlDataStore();
            }
            DefaultDrawingModel drawing = new DefaultDrawingModel();
            drawing.recreateObject(drawingData);

            CallableWorkflowParameterCreator generator = new CallableWorkflowParameterCreator(drawing, parametersJson);
            WorkflowParameterList parametersList = generator.createParametersList();
            WorkflowInvocationMessage msg = (versionId == null || "latest".equals(versionId))?
                    new WorkflowInvocationMessage(t, workflowId) :
                    new WorkflowInvocationMessage(t, workflowId, versionId);
            msg.setParameterXmlData(SerializationUtils.serialize(parametersList));
            String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(t, msg);
            return EscObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId), EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId));
        } catch (Exception e){
            throw new Exception("Error parsing workflow data: " + e.getMessage(), e);
        }        
    }
    
    @Override
    public EscWorkflowInvocation[] listInvocationsOfWorkflow(String workflowId) throws Exception {
        List results = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(t, workflowId);
        EscWorkflowInvocation[] invocations = new EscWorkflowInvocation[results.size()];
        String workflowName = EJBLocator.lookupObjectInfoBean().getObjectName(t, workflowId);
        for(int i=0;i<results.size();i++){
            invocations[i] = EscObjectFactory.createEscWorkflowInvocation((WorkflowInvocationFolder)results.get(i), workflowName);
        }
        return invocations;
    }

    @Override
    public EscWorkflowInvocation getInvocation(String invocationId) throws Exception {
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId);
        return EscObjectFactory.createEscWorkflowInvocation(invocation, EJBLocator.lookupObjectInfoBean().getObjectName(t, invocation.getWorkflowId()));
    }

    @Override
    public EscWorkflowInvocation terminateInvocation(String invocationId) throws Exception {
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId);
        WorkflowTerminationReport report = WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(t, invocationId);
        return EscObjectFactory.createEscWorkflowInvocation(invocation, EJBLocator.lookupObjectInfoBean().getObjectName(t, invocation.getWorkflowId()));
    }

    @Override
    public EscWorkflowService getService(String serviceId) throws Exception {
        DynamicWorkflowService service = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowService(t, serviceId);
        if(service!=null){
            return EscObjectFactory.createEscWorkflowService(service);
        } else {
            return null;
        }
    }

    public EscWorkflowService saveService(EscWorkflowService service) throws Exception {
        DynamicWorkflowService existing = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowService(t, service.getId());
        if(existing!=null){
            existing.setName(service.getName());
            existing.setDescription(service.getDescription());
            existing.setCategory(service.getCategory());
            existing.setContainerId(service.getContainerId());
            existing = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(t, existing);
            return EscObjectFactory.createEscWorkflowService(existing);
        } else {
            DynamicWorkflowService svc = new DynamicWorkflowService();
            svc.setName(service.getName());
            svc.setDescription(service.getDescription());
            svc.setCategory(service.getCategory());
            svc.setContainerId(service.getContainerId());
            svc = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowService(t, svc);
            return EscObjectFactory.createEscWorkflowService(svc);        
        }
    }

    @Override
    public EscWorkflowLibrary getLibraryByName(String name) throws Exception {
        DynamicWorkflowLibrary library = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowLibraryByLibraryName(t, name);
        if(library!=null){
            return EscObjectFactory.createEscWorkflowLibrary(library);
        } else {
            return null;
        }
    }

    @Override
    public EscWorkflowLibrary saveLibrary(EscWorkflowLibrary library) throws Exception {
        DynamicWorkflowLibrary existing = null;
        if(library.getId()!=null && !library.getId().isEmpty()){
            existing = WorkflowEJBLocator.lookupWorkflowManagementBean().getDynamicWorkflowLibrary(t, library.getId());
        } 
        
        if(existing!=null){
            existing.setName(library.getName());
            existing.setLibraryName(library.getLibraryName());
            existing.setDescription(library.getDescription());
            existing.setContainerId(library.getContainerId());
            existing = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(t, existing);
            return EscObjectFactory.createEscWorkflowLibrary(existing);
                    
        } else {
            DynamicWorkflowLibrary lib = new DynamicWorkflowLibrary();
            lib.setName(library.getName());
            lib.setLibraryName(library.getLibraryName());
            lib.setDescription(library.getDescription());
            lib.setContainerId(library.getContainerId());
            lib.setOrganisationId(t.getOrganisationId());
            lib = WorkflowEJBLocator.lookupWorkflowManagementBean().saveDynamicWorkflowLibrary(t, lib);
            return EscObjectFactory.createEscWorkflowLibrary(lib);                    
        }
    }


    private EscWorkflow[] toEscWorkflows(List workflowList) {
        EscWorkflow[] results = new EscWorkflow[workflowList.size()];
        for(int i=0;i<workflowList.size();i++){
            results[i] = EscObjectFactory.createEscWorkflow((WorkflowDocument)workflowList.get(i));
        }
        return results;
    }

    @Override
    public void enableWorkflowExternalDataSupport(String workflowId, String blockName) throws Exception {
        WorkflowDocument wf = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
        if(wf!=null){
            wf.setExternalDataSupported(true);
            wf.setExternalDataBlockName(blockName);
            wf = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(t, wf);
        } else {
            throw new Exception("No such workflow");
        }        
    }

    @Override
    public void disableWorkflowExternalDataSupport(String workflowId) throws Exception {
        WorkflowDocument wf = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(t, workflowId);
        if(wf!=null){
            wf.setExternalDataSupported(false);
            wf.setExternalDataBlockName("");
            wf = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(t, wf);
        } else {
            throw new Exception("No such workflow");
        }   
    }
    
    
}
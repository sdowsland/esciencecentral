/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.storage;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.document.UncommittedVersion;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.connexience.server.util.Base64;
import com.connexience.server.util.ZipUtils;
import java.io.FileInputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This data store uses the AWS official SDK.
 * @author hugo
 */
public class S3DataStore extends DataStore {
    /** Amazon Access Key ID */
    private String accessKeyId = "";
    
    /** Amazon Secret Key ID */
    private String accessKey = "";
    
    /** Organisation Bucket Name */
    private String organisationBucket = "";
    
    /** Encrypt data on the server */
    private boolean serverSideEncrypted = false;
    
    public S3DataStore() {
        sizeLimited = false;
        directAccessSupported = true;
        bulkDeleteSupported = false;
        spaceReportingSupported = false;        
    }

    private final AmazonS3 getClient() throws Exception {
        BasicAWSCredentials creds = new BasicAWSCredentials(accessKeyId, accessKey);
        ClientConfiguration config = new ClientConfiguration();
        config.setUseGzip(true);
        config.setSocketTimeout(600000);
        config.setConnectionTimeout(600000);
        config.setMaxConnections(1);
        return new AmazonS3Client(creds, config);
    }
    
    @Override
    public InputStream getInputStream(DocumentRecord document, DocumentVersion version) throws ConnexienceException {
        try {
            AmazonS3 client = getClient();
            GetObjectRequest request = new GetObjectRequest(organisationBucket, version.getId());
            S3Object object = client.getObject(request);
            return object.getObjectContent();
        } catch (Exception e){
            throw new ConnexienceException("Error getting S3 data stream: " + e.getMessage(), e);
        }
    }

    @Override
    public DocumentVersion readFromFile(DocumentRecord document, DocumentVersion record, File file) throws ConnexienceException {
        FileInputStream inStream = null;
        try {
            inStream = new FileInputStream(file);
            return readFromStream(document, record, inStream, file.length());
        } catch (Exception e){
            throw new ConnexienceException("Error reading from file: " + e.getMessage(), e);
        } finally {
            try {inStream.close();}catch(Exception e){}
        }
    }

    @Override
    public DocumentVersion readFromStream(DocumentRecord document, DocumentVersion record, InputStream stream, long contentLength) throws ConnexienceException {
        assertWritable();
        DigestInputStream digester = null;
        try {
            // Get the S3 Service
            AmazonS3 client = getClient();
            
            digester = new DigestInputStream(stream, MessageDigest.getInstance("MD5"));
            ObjectMetadata md = new ObjectMetadata();
            md.setContentLength(contentLength);
            
            // Set encryption properties
            if(serverSideEncrypted){
                md.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);
            }
            
            PutObjectRequest request = new PutObjectRequest(organisationBucket, record.getId(), digester, md);
            client.putObject(request);
            digester.close();
            record.setSize(contentLength);
            record.setMd5(Base64.encodeBytes(digester.getMessageDigest().digest()));
            return record;
        } catch (Exception e){
            e.printStackTrace();
            throw new ConnexienceException("Error uploading data to S3: " + e.getMessage());
        }
    }

    @Override
    public void writeToStream(DocumentRecord document, DocumentVersion record, OutputStream stream) throws ConnexienceException {
        InputStream inputStream = null;
        try {
            inputStream = getInputStream(document, record);
            ZipUtils.copyInputStream(inputStream, stream);
            stream.flush();
        } catch (Exception e){
            throw new ConnexienceException("Error writing to stream: " + e.getMessage(), e);
        } finally {
            try {inputStream.close();}catch(Exception e){}
        }
    }

    @Override
    public void writeToStream(DocumentRecord document, DocumentVersion record, OutputStream stream, long sizeLimit) throws ConnexienceException {
        InputStream inputStream = null;
        try {
            inputStream = getInputStream(document, record);
            ZipUtils.copyInputStream(inputStream, stream, (int)sizeLimit);
            stream.flush();
        } catch (Exception e){
            throw new ConnexienceException("Error writing to stream: " + e.getMessage(), e);
        } finally {
            try {inputStream.close();}catch(Exception e){}
        }
    }

    @Override
    public void removeRecord(DocumentRecord document, DocumentVersion record) throws ConnexienceException {
        try {
            AmazonS3 client = getClient();
            DeleteObjectRequest request = new DeleteObjectRequest(organisationBucket, record.getId());
            client.deleteObject(request);
        } catch (Exception e){
            throw new ConnexienceException("Error removing record: " + e.getMessage());
        }
    }

    @Override
    public long getRecordSize(DocumentRecord document, DocumentVersion record) throws ConnexienceException {
        try {
            AmazonS3 client = getClient();
            GetObjectRequest request = new GetObjectRequest(organisationBucket, record.getId());
            S3Object object = client.getObject(request);
            return object.getObjectMetadata().getContentLength();
        } catch (Exception e){
            throw new ConnexienceException("Error getting record size: " + e.getMessage());
        }
    }

    @Override
    public void bulkDelete(String organisationId, ArrayList<String> documentIds) throws ConnexienceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void uploadBlock(DocumentRecord document, UncommittedVersion version, int blockId, byte[] blockContent) throws ConnexienceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DocumentVersion commitBlockList(DocumentRecord document, UncommittedVersion version, List<Integer> blockList, DocumentVersion versionToCommit) throws ConnexienceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Integer> getBlockList(DocumentRecord document, String versionId) throws ConnexienceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     @Override
    public JSONObject toJson() throws Exception {
        JSONObject json = super.toJson();
        json.put("AccessKeyID", accessKeyId);
        json.put("AccessKey", accessKey);
        json.put("BucketName", organisationBucket);
        json.put("ServerSideEncrypted", serverSideEncrypted);
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        super.readJson(json);
        accessKey = json.getString("AccessKey");
        accessKeyId = json.getString("AccessKeyID");
        organisationBucket = json.getString("BucketName");
        serverSideEncrypted = json.getBoolean("ServerSideEncrypted");
    }

    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("AccessKey", accessKey);
        store.add("AccessKeyID", accessKeyId);
        store.add("OrganisationBucket", organisationBucket);
        store.add("ServerSideEncrypted", serverSideEncrypted);
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        super.recreateObject(store);
        accessKey = store.stringValue("AccessKey", "");
        accessKeyId = store.stringValue("AccessKeyID", "");
        organisationBucket = store.stringValue("OrganisationBucket", "");
        serverSideEncrypted = store.booleanValue("ServerSideEncrypted", false);
    }
    
    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getOrganisationBucket() {
        return organisationBucket;
    }

    public void setOrganisationBucket(String organisationBucket) {
        this.organisationBucket = organisationBucket;
    }

    public boolean isServerSideEncrypted() {
        return serverSideEncrypted;
    }

    public void setServerSideEncrypted(boolean serverSideEncrypted) {
        this.serverSideEncrypted = serverSideEncrypted;
    }
}
package com.connexience.server.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * User: nsjw7
 * Date: 18/06/2013
 * Time: 10:14
 */
public class FileTypeStats {

    private Map<String, Long> filetypes = new HashMap<>();

    public FileTypeStats() {
    }

    public Long get(Object o) {
        return filetypes.get(o);
    }

    public Long put(String s, Long aLong) {
        return filetypes.put(s, aLong);
    }

    public Set<String> keySet() {
        return filetypes.keySet();
    }

    public Map<String, Long> getFiletypes() {
        return filetypes;
    }

    public void setFiletypes(Map<String, Long> filetypes) {
        this.filetypes = filetypes;
    }
}

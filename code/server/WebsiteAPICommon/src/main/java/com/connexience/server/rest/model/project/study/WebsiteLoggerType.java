package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerConfiguration;
import com.connexience.server.model.project.study.LoggerType;
import com.connexience.server.model.project.study.Sensor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WebsiteLoggerType
{
	private Integer id;

	private String name;

	private String manufacturer;

	private boolean physicalDevice = true;

	private List<WebsiteSensor> sensors = new ArrayList<>();

	private List<Integer> loggers = new ArrayList<>();

	private List<Integer> loggerConfigurations = new ArrayList<>();

	protected WebsiteLoggerType()
	{
	}

	public WebsiteLoggerType(LoggerType loggerType)
	{
		this.setId(loggerType.getId());
		this.name = loggerType.getName();
		this.manufacturer = loggerType.getManufacturer();
		this.physicalDevice = loggerType.isPhysicalDevice();

		// cyclic, use ID at OTHER side
		for (Sensor sensor : loggerType.getSensors())
		{
			if(sensor.getName() != null)
            {
                this.sensors.add(new WebsiteSensor(sensor));
            }
		}

		// cyclic, use IDs at this side
		for (Logger logger : loggerType.getLoggers())
		{
			this.loggers.add(logger.getId());
		}

		// cyclic, use IDs at this side
		for (LoggerConfiguration configuration : loggerType.getLoggerConfigurations())
		{
			this.loggerConfigurations.add(configuration.getId());
		}
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getManufacturer()
	{
		return manufacturer;
	}

	public void setManufacturer(String manufacturer)
	{
		this.manufacturer = manufacturer;
	}

	public boolean isPhysicalDevice()
	{
		return physicalDevice;
	}

	public void setPhysicalDevice(boolean physicalDevice)
	{
		this.physicalDevice = physicalDevice;
	}

	public List<WebsiteSensor> getSensors()
	{
		return sensors;
	}

	public void setSensors(List<WebsiteSensor> sensors)
	{
		this.sensors = sensors;
	}

	public List<Integer> getLoggers()
	{
		return loggers;
	}

	public void setLoggers(List<Integer> loggers)
	{
		this.loggers = loggers;
	}

	public List<Integer> getLoggerConfigurations()
	{
		return loggerConfigurations;
	}

	public void setLoggerConfigurations(List<Integer> loggerConfigurations)
	{
		this.loggerConfigurations = loggerConfigurations;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

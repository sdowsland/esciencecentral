package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.LoggerData;
import com.connexience.server.rest.model.project.WebsiteFileType;

public class WebsiteLoggerData
{
	private Integer id;

	private String documentRecordId;

	private WebsiteFileType fileType;

	private WebsiteLoggerDeployment loggerDeployment;

	protected WebsiteLoggerData()
	{

	}

	public WebsiteLoggerData(LoggerData loggerData)
	{
		// Basic fields
		this.setId(loggerData.getId());

		// Reference fields
		// not-cyclic
		this.fileType = new WebsiteFileType(loggerData.getFileType());

		// cyclic, IDs used at other side
        this.loggerDeployment = new WebsiteLoggerDeployment(loggerData.getLoggerDeployment());
	}

	public String getDocumentRecordId()
	{
		return documentRecordId;
	}

	public void setDocumentRecordId(String documentRecordId)
	{
		this.documentRecordId = documentRecordId;
	}

	public WebsiteFileType getFileType()
	{
		return fileType;
	}

	public void setFileType(WebsiteFileType fileType)
	{
		this.fileType = fileType;
	}

	public WebsiteLoggerDeployment getLoggerDeployment()
	{
		return loggerDeployment;
	}

	public void setLoggerDeployment(WebsiteLoggerDeployment loggerDeployment)
	{
		this.loggerDeployment = loggerDeployment;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

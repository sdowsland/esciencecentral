package com.connexience.server.rest.model.project;

import com.connexience.server.model.project.ConversionWorkflow;
import com.connexience.server.model.project.FileType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WebsiteConversionWorkflow
{
	private Integer id;

	private String workflowDocumentId;

	private List<WebsiteFileType> fileTypes = new ArrayList<>();

	protected WebsiteConversionWorkflow()
	{
	}

	public WebsiteConversionWorkflow(ConversionWorkflow conversionWorkflow)
	{
		this.setId(conversionWorkflow.getId());
		this.workflowDocumentId = conversionWorkflow.getWorkflowDocumentId();

		// cyclic, use ID list at OTHER side
		for (FileType fileType : conversionWorkflow.getFileTypes())
		{
			this.fileTypes.add(new WebsiteFileType(fileType));
		}
	}

	public String getWorkflowDocumentId()
	{
		return workflowDocumentId;
	}

	public void setWorkflowDocumentId(String workflowDocumentId)
	{
		this.workflowDocumentId = workflowDocumentId;
	}

	public List<WebsiteFileType> getFileTypes()
	{
		return fileTypes;
	}

	public void setFileTypes(List<WebsiteFileType> fileTypes)
	{
		this.fileTypes = fileTypes;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

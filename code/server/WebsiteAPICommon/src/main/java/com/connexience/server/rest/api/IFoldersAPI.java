package com.connexience.server.rest.api;
import com.connexience.server.rest.model.WebsiteDocument;
import com.connexience.server.rest.model.WebsiteFolder;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/folders
 * <p/>
 * User: nmdt3 Date: 30/11/2012 Time: 09:00
 */
public interface IFoldersAPI {

    @GET
    @Path("/{folderId}/children")
    @Produces("application/json")
    WebsiteFolder[] listChildFolders(@PathParam("folderId") String folderId) throws Exception;

    @GET
    @Path("/{folderId}/documents")
    @Produces("application/json")
    WebsiteDocument[] folderDocuments(@PathParam("folderId") String folderId) throws Exception;

    @GET
    @Path("/{folderId}")
    @Produces("application/json")
    WebsiteFolder getFolder(@PathParam("folderId") String folderId) throws Exception;

    @GET
    @Path("/home")
    @Produces("application/json")
    WebsiteFolder homeFolder() throws Exception;

    @POST
    @Path("/{folderId}")
    @Produces("application/json")
    WebsiteFolder createChildFolder(@PathParam("folderId") String id, @FormParam("folderName") String name) throws Exception;

    @PUT
    @Path("/{folderId}")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteFolder updateFolder(@PathParam("folderId") String folderId, WebsiteFolder folder) throws Exception;

    @GET
    @Path("/{folderId}/archive")
    Response archiveFolder(@PathParam("folderId") String[] folderIds) throws Exception;

    @DELETE
    @Path("/{folderId}")
    void deleteFolder(@PathParam("folderId") String folderId) throws Exception;
}

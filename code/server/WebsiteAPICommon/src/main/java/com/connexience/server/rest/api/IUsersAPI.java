/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.rest.model.WebsiteSignupUser;
import com.connexience.server.rest.model.WebsiteUser;
import com.connexience.server.rest.model.project.UploadUserProject;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/users
 * <p/>
 * User: nsjw7
 * Date: 30/11/2012
 * Time: 09:00
 */
public interface IUsersAPI {

    /**
     * Get the current user.  Returns HTTP 401 if the current user is the public user to allow
     * the website to redirect to splash screen
     *
     * @return WebsiteUser representation of the currently logged in user
     * @throws Exception WebApplicationException = 401 if the public user is logged in
     */
    @GET
    @Path("/")
    @Produces("application/json")
    List<WebsiteUser> getUsers(@DefaultValue("") @QueryParam("q") String query,
                               @DefaultValue("id") @QueryParam("orderBy") OrderBy orderBy,
                               @DefaultValue("ASC") @QueryParam("ascDesc") SearchOrder ascDesc,
                               @DefaultValue("1") @QueryParam("pageNum") int pageNum,
                               @DefaultValue("10") @QueryParam("pageSize") int pageSize,
                               @DefaultValue("true") @QueryParam("metadata") Boolean metadata,
                               @QueryParam("userId") List<String> userId);

    @PUT
    @Path("/")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteUser registerUser(WebsiteSignupUser user);

    @GET
    @Path("/{userId}")
    @Produces("application/json")
    WebsiteUser getUser(@PathParam("userId") String userId, @DefaultValue("true") @QueryParam("metadata") Boolean metadata);

    @POST
    @Path("/{userId}")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteUser saveUser(@PathParam("userId") String userId, WebsiteUser user);

    @GET
    @Path("username/{username}")
    @Produces("application/json")
    WebsiteUser findUser(@PathParam("username") String username, @DefaultValue("true") @QueryParam("metadata") Boolean metadata);

    @GET
    @Path("/current")
    @Produces("application/json")
    WebsiteUser getCurrentUser(@DefaultValue("true") @QueryParam("metadata") Boolean metadata);

    @POST
    @Path("/{userId}/profilePicture")
    @Produces("application/json")
    @Consumes("multipart/form-data")
    Response saveProfilePicture(@PathParam("userId") String userId, @Context HttpServletRequest request) throws ConnexienceException;

}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.security.Ticket;

public class WebsiteDocument extends WebsiteObject {
    private String id;
    private String name;
    private String containerId;
    private String description;
    private WebsiteUserMin creator;
    private long created;
    private long currentVersionSize;
    private int currentVersionNumber;
    private Boolean read;
    private Boolean write;
    private String archiveStatus;
    private String downloadPath;
    private String uploadPath;
    private String projectId;
    private String hash;
    private String phash;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WebsiteUserMin getCreator() {
        return creator;
    }

    public void setCreator(WebsiteUserMin creator) {
        this.creator = creator;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getCurrentVersionSize() {
        return currentVersionSize;
    }

    public void setCurrentVersionSize(long currentVersionSize) {
        this.currentVersionSize = currentVersionSize;
    }

    public int getCurrentVersionNumber() {
        return currentVersionNumber;
    }

    public void setCurrentVersionNumber(int currentVersionNumber) {
        this.currentVersionNumber = currentVersionNumber;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getWrite() {
        return write;
    }

    public void setWrite(Boolean write) {
        this.write = write;
    }

    public String getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(String archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPhash() {
        return phash;
    }

    public void setPhash(String phash) {
        this.phash = phash;
    }

    public WebsiteDocument() {
    }
    
    public WebsiteDocument(DocumentRecord document, Ticket ticket, String volumeId) throws ConnexienceException {
        
        this.setId(document.getId());
        this.setName(document.getName());
        this.setContainerId(document.getContainerId());
        this.setDescription(document.getDescription());
        this.setCreator(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, document.getCreatorId()), false, ticket));
        this.setCreated(document.getCreationDate().getTime() / 1000);
        this.setCurrentVersionSize(document.getCurrentVersionSize());
        this.setCurrentVersionNumber(document.getCurrentVersionNumber());
        this.setRead(true);
        this.setWrite(true);
        this.setArchiveStatus(document.getCurrentArchiveStatusAsString());
        this.setDownloadPath("/data/" + document.getId() + "/latest");
        this.setUploadPath("/data/" + document.getId());
        this.setProjectId(document.getProjectId());
        this.setHash(volumeId + "_" + document.getId());
        this.setPhash(volumeId + "_" + document.getContainerId());
        
    }
}
package com.connexience.server.rest.model.project;

import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.Uploader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WebsiteUploader
{
	private Integer id;

	private String username;

	private String password;

	private List<Integer> projects = new ArrayList<>();

	public WebsiteUploader()
	{

	}

	public WebsiteUploader(Uploader uploader)
	{
		this.setId(uploader.getId());
		this.username = uploader.getUsername();
		//this.password = uploader.getHashedPassword();

		for (final Project project : uploader.getProjects())
		{
			projects.add(project.getId());
		}
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public List<Integer> getProjects()
	{
		return projects;
	}

	public void setProjects(final List<Integer> projects)
	{
		this.projects = projects;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

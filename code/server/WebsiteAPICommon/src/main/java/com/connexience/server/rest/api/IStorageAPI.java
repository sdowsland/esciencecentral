package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.rest.model.WebsiteDocument;
import com.connexience.server.rest.model.WebsiteDocumentVersion;
import com.connexience.server.rest.model.WebsiteFolder;
import com.connexience.server.rest.model.WebsiteMetadataItem;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/xstorage
 * <p/>
 * User: nmdt3 Date: 30/11/2012 Time: 09:00
 */
public interface IStorageAPI {

    /*** Folder Methods ***/

    @GET
    @Path("/folders/{id}/documents")
    @Produces("application/json")
    WebsiteDocument[] folderDocuments(@PathParam("id") String id) throws Exception;

    @GET
    @Path("/folders/{id}/children")
    @Produces("application/json")
    WebsiteFolder[] listChildFolders(@PathParam("id") String id) throws Exception;

    @GET
    @Path("/folders/{id}")
    @Produces("application/json")
    WebsiteFolder getFolder(@PathParam("id") String id) throws Exception;

    @GET
    @Path("/folders/home")
    @Produces("application/json")
    WebsiteFolder homeFolder() throws Exception;

    @POST
    @Path("/folders/{id}")
    @Produces("application/json")
    @Consumes("text/plain")
    WebsiteFolder createChildFolder(@PathParam("id") String id, @FormParam("name") String name) throws Exception;

    @POST
    @Path("/folders/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteFolder updateFolder(WebsiteFolder folder) throws Exception;

    @DELETE
    @Path("/folders/{id}")
    void deleteFolder(@PathParam("id") String folderId) throws Exception;

    /*** Document Record Methods ***/

    @GET
    @Path("/documents/{id}")
    @Produces("application/json")
    WebsiteDocument getDocument(@PathParam("id") String id) throws Exception;

    @GET
    @Path("/folders/{folderId}/nameddocuments/{name}")
    @Produces("application/json")
    WebsiteDocument getNamedDocumentInFolder(@PathParam("folderId") String folderId, @PathParam("name") String name) throws Exception;

    @GET
    @Path("/documents/{id}/versions")
    @Produces("application/json")
    WebsiteDocumentVersion[] listDocumentVersions(@PathParam("id") String id) throws Exception;

    @GET
    @Path("/documentversions/{id}")
    @Produces("application/json")
    WebsiteDocumentVersion getDocumentVersion(@PathParam("id") String id) throws Exception;

    @GET
    @Path("/documents/{id}/versions/latest")
    @Produces("application/json")
    WebsiteDocumentVersion getLatestDocumentVersion(@PathParam("id") String documentId) throws Exception;

    @POST
    @Path("/folders/{id}/contents")
    @Produces("application/json")
    DocumentRecord createDocument(@PathParam("id") String id, @FormParam("name") String name) throws ConnexienceException;

    @POST
    @Path("/folders/{id}/documents/create")
    @Produces("application/json")
    @Consumes("text/plain")
    WebsiteDocument createDocumentInFolder(@PathParam("id") String id, @FormParam("name") String name) throws Exception;

	@POST
	@Path("/documents/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	WebsiteDocument updateDocument(WebsiteDocument document) throws Exception;

	@DELETE
	@Path("/documents/{id}")
	void deleteDocument(@PathParam("id") String documentId) throws Exception;

	@GET
	@Path("/documents/{id}/metadata")
	@Produces("application/json")
	WebsiteMetadataItem[] getDocumentMetadata(@PathParam("id") String id) throws Exception;

	@POST
	@Path("/documents/{id}/metadata")
	@Produces("application/json")
	@Consumes("application/json")
	WebsiteMetadataItem addMetadataToDocument(@PathParam("id") String id, WebsiteMetadataItem metadataItem) throws Exception;


    /*** Blocks ***/

	@PUT
	@Path("/documents/{documentId}/{versionId}/{blockNo}")
	@Consumes("application/octet-stream")
	@Produces("application/json")
	String uploadBlock(@PathParam("documentId") String documentId, @PathParam("versionId") String versionId, @PathParam("blockNo") Integer blockNo, byte[] blockData) throws Exception;

	@GET
	@Path("/documents/{documentId}/{versionId}/blocklist")
	@Produces("application/json")
	List<Integer> getBlockList(@PathParam("documentId") String documentId, @PathParam("versionId") String versionId) throws Exception;

	@PUT
	@Path("/documents/{documentId}/{versionId}/blocklist")
	@Consumes("application/json")
	@Produces("application/json")
	DocumentVersion commitBlockList(@PathParam("documentId") String documentId, @PathParam("versionId") String versionId, List<Integer> blockList) throws Exception;

	@POST
	@Path("/batch/{id}")
	@Consumes("multipart/form-data")
	Response batchUpload(@PathParam("id") String id, MultipartFormDataInput form) throws ConnexienceException;

    @POST
    @Path("/upload/{documentId}")
    Response uploadVersion(@PathParam("documentId") String documentId) throws ConnexienceException;

    @GET
    @Path("/metadata/{userId}/{category}/{name}")
    @Produces("application/json")
    WebsiteMetadataItem getObjectMetadata(@PathParam("userId") String userId, @PathParam("category") String category, @PathParam("name") String name) throws Exception;

}

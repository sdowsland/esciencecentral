package com.connexience.server.rest.model.project;

import com.connexience.server.model.project.ConversionWorkflow;
import com.connexience.server.model.project.FileType;
import com.connexience.server.model.project.study.Sensor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WebsiteFileType
{
	private Integer id;

	private String name;

	private String description;

	private List<String> conversionWorkflows = new ArrayList<>();

	private List<String> sensors = new ArrayList<>();

	protected WebsiteFileType()
	{
	}

	public WebsiteFileType(FileType fileType)
	{
		// Basic members
		this.setId(fileType.getId());
		this.name = fileType.getName();
		this.description = fileType.getDescription();

		// cyclic, use IDs at this side
		for (ConversionWorkflow conversionWorkflow : fileType.getConversionWorkflows())
		{
			this.conversionWorkflows.add(conversionWorkflow.getId().toString());
		}

		// cyclic, use IDs at this side
		for (Sensor sensor : fileType.getSensors())
		{
			this.sensors.add(sensor.getId().toString());
		}
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public List<String> getConversionWorkflows()
	{
		return conversionWorkflows;
	}

	public void setConversionWorkflows(List<String> conversionWorkflows)
	{
		this.conversionWorkflows = conversionWorkflows;
	}

	public List<String> getSensors()
	{
		return sensors;
	}

	public void setSensors(List<String> sensors)
	{
		this.sensors = sensors;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

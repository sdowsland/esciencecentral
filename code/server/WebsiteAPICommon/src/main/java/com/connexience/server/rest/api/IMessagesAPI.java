/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.api;

import com.connexience.server.rest.model.WebsiteMessage;
import com.connexience.server.rest.model.WebsiteMessageSummary;

import javax.ejb.Local;
import javax.ws.rs.*;
import java.util.List;

/**
 * User: nsjw7
 * Date: 06/12/2012
 * Time: 15:08
 */
@Local
public interface IMessagesAPI {

    @GET
    @Path("/summary")
    @Produces("application/json")
    public WebsiteMessageSummary getSummary() throws Exception;

    @GET
    @Path("/")
    @Produces("application/json")
    public List<WebsiteMessage> getMessages(@QueryParam("start") int start, @QueryParam("numResults") int numResults) throws Exception;

    @POST
    @Path("/{userId}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    void sendMessage(@PathParam("userId")String userId, @FormParam("threadId")String threadId, @FormParam("otherUsersIds") String otherUsersIds, @FormParam("title")String title, @FormParam("message")String message);
}

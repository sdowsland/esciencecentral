/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.rest.model.WebsiteData;
import com.connexience.server.rest.model.project.WebsiteUploader;
import com.connexience.server.rest.model.project.study.*;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import java.util.Collection;
import java.util.List;

@Local
public interface IStudyAPI {
    @GET
    @Path("/{studyId}")
    @Produces("application/json")
    WebsiteStudy getStudy(@PathParam("studyId") int studyId) throws ConnexienceException;

    @POST
    @Path("/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteStudy saveStudy(WebsiteStudy websiteStudy) throws ConnexienceException;

    @POST
    @Path("/{studyId}/remove")
    @Produces("application/json")
    Response removeStudy(@PathParam("studyId") int studyId, @FormParam("removeData") Boolean removeData) throws ConnexienceException;

    @POST
    @Path("/{studyId}/structure")
    @Produces("application/json")
    @Consumes("multipart/form-data")
    Response saveStudyStructure(@PathParam("studyId") int studyId, @Context HttpServletRequest request) throws ConnexienceException;

    @POST
    @Path("/{studyId}/subjectproperties")
    @Produces("application/json")
    @Consumes("multipart/form-data")
    Response saveStudySubjectProperties(@PathParam("studyId") int studyId, @Context HttpServletRequest request) throws ConnexienceException;

    @POST
    @Path("/{studyId}/deploymentproperties")
    @Produces("application/json")
    @Consumes("multipart/form-data")
    Response saveStudyDeploymentProperties(@PathParam("studyId") int studyId, @Context HttpServletRequest request) throws ConnexienceException;

    @GET
    @Path("/all")
    @Produces("application/json")
    List<WebsiteStudy> getStudies(@QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize, @QueryParam("orderBy") String orderBy, @QueryParam("direction") String direction) throws Exception;

    @GET
    @Path("/all/count")
    @Produces("application/json")
    Long getStudyCount() throws ConnexienceException;

    @GET
    @Path("/user")
    @Produces("application/json")
    List<WebsiteStudy> getUserStudies(@QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize, @QueryParam("orderBy") String orderBy, @QueryParam("direction") String direction) throws Exception;

    @GET
    @Path("/user/count")
    @Produces("application/json")
    Long getUserStudyCount() throws ConnexienceException;

    @POST
    @Path("/admin/add")
    @Produces("application/json")
    Response addAdminToStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException;

    @POST
    @Path("/member/add")
    @Produces("application/json")
    Response addMemberToStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException;

    @POST
    @Path("/admin/remove")
    @Produces("application/json")
    Response removeAdminFromStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException;

    @POST
    @Path("/member/remove")
    @Produces("application/json")
    Response removeMemberFromStudy(@FormParam("studyId") int studyId, @FormParam("userId") String userId) throws ConnexienceException;

    @POST
    @Path("/{id}/delete")
    @Produces("application/json")
    Response deleteStudy(@PathParam("id") int studyId) throws ConnexienceException;

    //
//	@POST
//	@Path("/data/add")
//	@Produces("application/json")
//	Response addDataToStudy(@FormParam("studyId") int studyId, @FormParam("escId") String escId) throws ConnexienceException;
//
    @GET
    @Path("/data")
    @Produces("application/json")
    List<WebsiteData> getStudyData(@QueryParam("studyId") int studyId, @QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize) throws ConnexienceException;

    @GET
    @Path("/{studyId}/phase/{phaseId}/group/{groupId}/groups")
    @Produces("application/json")
    List<WebsiteSubjectGroup> getSubjectGroups(@PathParam("studyId") int studyId, @PathParam("phaseId") int phaseId, @PathParam("groupId") int groupId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/group/{groupId}/subjects")
    @Produces("application/json")
    List<WebsiteSubject> getSubjects(@PathParam("studyId") int studyId, @PathParam("groupId") int groupId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/subjectgroup/save")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteSubjectGroup saveSubjectGroup(@PathParam("studyId") int studyId, WebsiteSubjectGroup subjectGroup) throws ConnexienceException;

    @POST
    @Path("/{studyId}/subjectgroup/{id}/delete")
    @Produces("application/json")
    Response deleteSubjectGroup(@FormParam("deleteData") boolean deleteData, @PathParam("studyId") int studyId, @PathParam("id") int subjectGroupId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/subjectgroup/{groupId}/subject/save")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteSubject saveSubject(@PathParam("studyId") int studyId, @PathParam("groupId") int subjectGroupId, WebsiteSubject subject) throws ConnexienceException;

    @POST
    @Path("/{studyId}/subjectgroup/{groupId}/subject/{id}/delete")
    @Produces("application/json")
    Response deleteSubject(@PathParam("studyId") int studyId, @PathParam("groupId") int subjectGroupId, @PathParam("id") int subjectId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/subjectgroup/{id}/data")
    @Produces("application/json")
    String getSubjectData(@PathParam("studyId") int studyId, @PathParam("id") int groupId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/subjectgroup/{id}/data")
    @Produces("application/json")
    @Consumes("multipart/form-data")
    Response saveSubjectData(@PathParam("studyId") int studyId, @PathParam("id") int groupId, @Context HttpServletRequest request) throws ConnexienceException;

    @GET
    @Path("/uploaders")
    @Produces("application/json")
    List<WebsiteUploader> getUploadUsers(@FormParam("start") int start, @FormParam("end") int end) throws ConnexienceException;

    @POST
    @Path("/uploader/save")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteUploader saveUploadUser(WebsiteUploader uploadUser) throws ConnexienceException;

    @POST
    @Path("/uploader/{uploaderId}/delete")
    @Produces("application/json")
    Response deleteUploadUser(@PathParam("uploaderId") String uploadUserId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/uploaders")
    @Produces("application/json")
    List<WebsiteUploader> getStudyUploadUsers(@PathParam("studyId") int studyId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/uploader/{uploaderId}/save")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteUploader addUploadUser(@PathParam("studyId") int studyId, @PathParam("uploaderId") int uploaderId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/uploader/{uploaderId}/remove")
    @Produces("application/json")
    Response removeUploadUser(@PathParam("studyId") int studyId, @PathParam("uploaderId") int uploadUserId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/message")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    void sendMessageToAdmins(@PathParam("studyId") int studyId, @FormParam("title") String title, @FormParam("message") String message) throws ConnexienceException;

    @POST
    @Path("/{studyId}/failedUpload")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    void notifyUploadFailure(@PathParam("studyId") int studyId,
                             @FormParam("username") String username,
                             @FormParam("filename") String filename,
                             @FormParam("errorMessage") String errorMessage,
                             @FormParam("expectedSize") Long expectedSize,
                             @FormParam("stagedSize") Long stagedSize,
                             @FormParam("blobSize") Long blobSize)
            throws ConnexienceException;


    @GET
    @Path("/count/studynames/{name}")
    @Produces("application/json")
    long countProjectsWithMatchingName(@PathParam("name") String name) throws ConnexienceException;

    @GET
    @Path("/count/studyids/{externalId}")
    @Produces("application/json")
    long countProjectsWithMatchingExternalId(@PathParam("externalId") String externalId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/phases")
    @Produces("application/json")
    List<WebsitePhaseMin> getStudyPhases(@PathParam("studyId") int studyId) throws ConnexienceException;

    @POST
    @Path("/{studyId}/phases")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteStudy addPhaseToStudy(@PathParam("studyId") int studyId, WebsitePhase phase) throws ConnexienceException;

    @DELETE
    @Path("/{studyId}/phases/{phaseId}")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteStudy removePhase(@PathParam("studyId") int studyId, @PathParam("phaseId") int phaseId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/{phasename}/unique")
    @Produces("text/plain")
    boolean isPhaseUnique(@PathParam("studyId") int studyId, @PathParam("phasename") String phasename);

    @GET
    @Path("/subjects/property/{propertyName}/{propertyValue}")
    @Produces("application/json")
    List<WebsiteSubject> getSubjectFromProperty(@PathParam("propertyName") String propertyName,
                                          @PathParam("propertyValue") String propertyValue) throws ConnexienceException;

    @GET
    @Path("/subjects/property/{propertyName}/{propertyValue}/unique")
    @Produces("text/plain")
    boolean isPropertyUnique(@PathParam("propertyName") String propertyName,
                             @PathParam("propertyValue") String propertyValue) throws ConnexienceException;
}

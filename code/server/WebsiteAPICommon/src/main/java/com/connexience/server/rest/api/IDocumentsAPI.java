package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.rest.model.WebsiteDocument;
import com.connexience.server.rest.model.WebsiteDocumentVersion;
import com.connexience.server.rest.model.WebsiteMetadataItem;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/documents
 * <p/>
 * User: nmdt3 Date: 30/11/2012 Time: 09:00
 */
public interface IDocumentsAPI {

    @GET
    @Path("/{documentId}")
    @Produces("application/json")
    WebsiteDocument getDocument(@PathParam("documentId") String documentId) throws Exception;

    @POST
    @Path("/")
    @Produces("application/json")
    DocumentRecord createDocument(@FormParam("folderId") String folderId, @FormParam("name") String name) throws ConnexienceException;

    @PUT
    @Path("/{documentId}")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteDocument updateDocument(@PathParam("documentId") String documentId, WebsiteDocument document) throws Exception;

    @POST
    @Path("/move/{folderId}")
    Response moveDocuments(@PathParam("folderId") String folderId, @FormParam("documentId") String[] documentIds, @FormParam("copy") Boolean copy) throws Exception;

    @DELETE
    @Path("/{documentId}")
    Response deleteDocument(@PathParam("documentId") String documentId) throws Exception;

    @POST
    @Path("/batch/{documentId}")
    @Consumes("multipart/form-data")
    Response batchUpload(@PathParam("documentId") String documentId, MultipartFormDataInput form) throws ConnexienceException;

    @GET
    @Path("/find/{name}")
    @Produces("application/json")
    WebsiteDocument getNamedDocument(@PathParam("name") String name, @FormParam("folderId") String folderId) throws Exception;

    @GET
    @Path("/{documentId}/versions")
    @Produces("application/json")
    WebsiteDocumentVersion[] listDocumentVersions(@PathParam("documentId") String documentId) throws Exception;

    @GET
    @Path("/{documentId}/versions")
    @Produces("application/json")
    WebsiteDocumentVersion getDocumentVersion(@PathParam("documentId") String documentId) throws Exception;

    @GET
    @Path("/{documentId}/versions/latest")
    @Produces("application/json")
    WebsiteDocumentVersion getLatestDocumentVersion(@PathParam("documentId") String documentId) throws Exception;

    @POST
    @Path("/upload/{documentId}")
    Response uploadVersion(@PathParam("documentId") String documentId) throws ConnexienceException;

    @GET
    @Path("/{documentId}/metadata")
    @Produces("application/json")
    WebsiteMetadataItem[] getDocumentMetadata(@PathParam("documentId") String documentId) throws Exception;

    @POST
    @Path("/{documentId}/metadata")
    @Produces("application/json")
    @Consumes("application/json")
    WebsiteMetadataItem addMetadataToDocument(@PathParam("documentId") String documentId, WebsiteMetadataItem metadataItem) throws Exception;
}

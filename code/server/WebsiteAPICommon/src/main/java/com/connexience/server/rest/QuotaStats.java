package com.connexience.server.rest;

/**
 * User: nsjw7
 * Date: 18/06/2013
 * Time: 09:44
 */
public class QuotaStats {
    private long used;
    private long free;

    public QuotaStats() {
    }

    public long getUsed() {
        return used;
    }

    public void setUsed(long used) {
        this.used = used;
    }

    public long getFree() {
        return free;
    }

    public void setFree(long free) {
        this.free = free;
    }
}

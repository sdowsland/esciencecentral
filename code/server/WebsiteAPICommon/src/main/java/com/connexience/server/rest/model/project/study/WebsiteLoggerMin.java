package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerDeployment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebsiteLoggerMin
{
	private Integer id;

	private String serialNumber;

	private String location;

	private WebsiteLoggerTypeMin loggerType;

	private Integer loggerDeployments;

	private Map<String, String> additionalProperties = new HashMap<>();

	protected WebsiteLoggerMin()
	{

	}

	public WebsiteLoggerMin(Logger logger)
	{
		this.setId(logger.getId());
		this.serialNumber = logger.getSerialNumber();
		this.location = logger.getLocation();
		this.loggerType = new WebsiteLoggerTypeMin(logger.getLoggerType());
        this.loggerDeployments = logger.getLoggerDeployments().size();

		for (String key : logger.getAdditionalProperties().keySet())
		{
			this.additionalProperties.put(key, logger.getAdditionalProperties().get(key));
		}
	}

	public String getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public WebsiteLoggerTypeMin getLoggerType()
	{
		return loggerType;
	}

	public void setLoggerType(WebsiteLoggerTypeMin loggerType)
	{
		this.loggerType = loggerType;
	}

	public Integer getLoggerDeployments()
	{
		return loggerDeployments;
	}

	public void setLoggerDeployments(Integer loggerDeployments)
	{
		this.loggerDeployments = loggerDeployments;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties)
	{
		this.additionalProperties = additionalProperties;
	}
}

package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Subject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class WebsiteSubject
{
	private Integer id;
	private String externalId;
	private Integer subjectGroupId;
	private Map<String, String> additionalProperties = new HashMap<>();

	public WebsiteSubject()
	{

	}

	public WebsiteSubject(Subject subject)
	{
		if (subject.getId() != null)
		{
			this.setId(subject.getId());
			this.externalId = subject.getExternalId();

			// cyclic, use ID here
			this.subjectGroupId = subject.getSubjectGroup().getId();

			// not-cyclic
			for (String key : subject.getAdditionalProperties().keySet())
			{
				this.additionalProperties.put(key, subject.getAdditionalProperties().get(key));
			}
		}
	}

	public String getExternalId()
	{
		return externalId;
	}

	public void setExternalId(String externalId)
	{
		this.externalId = externalId;
	}

	public Integer getSubjectGroupId()
	{
		return subjectGroupId;
	}

	public void setSubjectGroupId(Integer subjectGroupId)
	{
		this.subjectGroupId = subjectGroupId;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties)
	{
		this.additionalProperties = additionalProperties;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

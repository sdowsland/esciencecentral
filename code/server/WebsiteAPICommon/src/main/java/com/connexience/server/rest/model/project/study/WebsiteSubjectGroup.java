package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.LoggerDeployment;
import com.connexience.server.model.project.study.Subject;
import com.connexience.server.model.project.study.SubjectGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebsiteSubjectGroup
{
	private Integer id;

	private Integer phaseId;

	private Integer parentId;

	private List<Integer> children = new ArrayList<>();

	private String displayName;

    private String categoryName;

	private String dataFolderId;

	private List<WebsiteSubject> subjects = new ArrayList<>();

	private List<Integer> loggerDeployments = new ArrayList<>();

	private Map<String, String> additionalProperties = new HashMap<>();

	public WebsiteSubjectGroup()
	{

	}

	public WebsiteSubjectGroup(SubjectGroup subjectGroup)
	{

		this.setId(subjectGroup.getId());
		this.displayName = subjectGroup.getDisplayName();
        this.categoryName = subjectGroup.getCategoryName();
		this.dataFolderId = subjectGroup.getDataFolderId();

		if(subjectGroup.getPhase() != null){
			this.phaseId = subjectGroup.getPhase().getId();
		}

		if (subjectGroup.getParent() != null)
		{
			this.parentId = subjectGroup.getParent().getId();
		}
		else
		{
			this.parentId = null;
		}

		for (SubjectGroup child : subjectGroup.getChildren())
		{
			this.children.add(child.getId());
		}

		// cyclic, use ID at OTHER side
		for (Subject subject : subjectGroup.getSubjects())
		{
			this.subjects.add(new WebsiteSubject(subject));
		}

		// cyclic, use ID here
		for (final LoggerDeployment loggerDeployment : subjectGroup.getLoggerDeployments())
		{
			loggerDeployments.add(loggerDeployment.getId());
		}

		// not-cyclic
		for (String key : subjectGroup.getAdditionalProperties().keySet())
		{
			this.additionalProperties.put(key, subjectGroup.getAdditionalProperties().get(key));
		}
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

    public String getCategoryName()
    {
        return categoryName;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

	public String getDataFolderId()
	{
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId)
	{
		this.dataFolderId = dataFolderId;
	}

	public List<Integer> getChildren()
	{
		return children;
	}

	public void setChildren(List<Integer> children)
	{
		this.children = children;
	}

	public List<WebsiteSubject> getSubjects()
	{
		return subjects;
	}

	public void setSubjects(List<WebsiteSubject> subjects)
	{
		this.subjects = subjects;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties)
	{
		this.additionalProperties = additionalProperties;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	public List<Integer> getLoggerDeployments()
	{
		return loggerDeployments;
	}

	public void setLoggerDeployments(final List<Integer> loggerDeployments)
	{
		this.loggerDeployments = loggerDeployments;
	}

	public Integer getParentId()
	{
		return parentId;
	}

	public void setParentId(final Integer parentId)
	{
		this.parentId = parentId;
	}

	public Integer getPhaseId() {
		return phaseId;
	}

	public void setPhaseId(Integer phaseId) {
		this.phaseId = phaseId;
	}
}

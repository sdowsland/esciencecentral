/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.profile.UserProfile;
import org.codehaus.jackson.annotate.JsonSubTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: nsjw7
 * Date: 28/11/2012
 * Time: 13:48
 */
@JsonSubTypes({
        @JsonSubTypes.Type(value = WebsiteUser.class, name = "user")
})
public class WebsiteUserMin extends WebsiteObject {

    private String email;
    private String firstname;
    private String lastname;
    private String bio;
    private Boolean admin;
    private Integer friends;
    private Integer groups;
    private Map<String, WebsiteMetadataItem> metadata = new HashMap<>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Boolean isAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Integer getFriends() {
        return friends;
    }

    public void setFriends(Integer friends) {
        this.friends = friends;
    }

    public Integer getGroups() {
        return groups;
    }

    public void setGroups(Integer groups) {
        this.groups = groups;
    }

    public Map<String, WebsiteMetadataItem> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, WebsiteMetadataItem> metadata) {
        this.metadata = metadata;
    }

    public WebsiteUserMin() {}

    public WebsiteUserMin(User user, Boolean metadata, Ticket ticket) throws ConnexienceException {

        UserProfile userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, user.getId());
        List<MetadataItem> metaDataItems = EJBLocator.lookupMetaDataBean().getObjectMetadata(ticket, user.getId());

        this.setId(user.getId());
        this.email = userProfile.getEmailAddress();
        this.firstname = user.getFirstName();
        this.lastname = user.getSurname();
        this.bio = userProfile.getText();
        this.admin = EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, user.getId(), ticket.getOrganisationId());
        this.friends = EJBLocator.lookupLinkBean().getLinkedSinkObjects(ticket, user, User.class).size();
        this.groups = EJBLocator.lookupGroupDirectoryBean().listNonProtectedGroups(ticket).size();

        if(metadata){
            for(MetadataItem metaDataItem : metaDataItems) {
                this.metadata.put(metaDataItem.getName(), new WebsiteMetadataItem(metaDataItem));
            }
        }
    }

    @Override
    public String toString() {
        return "WebsiteUserMin{" +
                "firstname:'" + firstname + '\'' +
                ", lastname:'" + lastname + '\'' +
                "} " + super.toString();
    }
}

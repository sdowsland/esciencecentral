/*
 * e-Science Central
 * Copyright (C) 2008-2014 Digital Institute, School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * This interface is responsible for handling authentication attempts against the eScience Central API and, where
 * requested, presenting Json Web Tokens (JWT) that can be used to authorize stateless requests.
 * <p/>
 * The {@link #token(String, String, Long)} method allows the caller to acquire a JWT to be presented in subsequent API
 * calls (in the <code>Authorization</code> header) to act as an authorization claim which can be verified by the server
 * (using digital signatures).
 * <p/>
 * The {@link #token(String, Long)} and {@link #token()} methods provide additional functionality such as masquerading
 * as another user and verifying an existing token.
 * <p/>
 * The {@link #login(String, String)} and {@link #loginWithRememberMe(String)} methods are used to establish a stateful
 * session relying on an {@link javax.servlet.http.HttpSession} object. This mechanism is currently deprecated and will
 * eventually be removed.
 *
 * @author Derek Mortimer
 */
public interface ILogonAPI {
	/**
	 * Authenticates the presented credentials and returns a JWT token (representing a {@link
	 * com.connexience.server.model.security.User}) to be used as the <code>Authorization</code> header in subsequent
	 * API calls.
	 * <p/>
	 * Subsequent API calls with a valid JWT contained in the <code>Authorization</code> header will result in the
	 * availability of a {@link com.connexience.server.rest.api.jwt.JsonWebToken} and {@link
	 * com.connexience.server.model.security.WebTicket} object store in the {@link javax.servlet.http.HttpServletRequest}
	 * object, accessible through its {@link javax.servlet.http.HttpServletRequest#getAttribute(String)} method with
	 * <code>&quot;com.connexience.jwt&quot;</code> and <code>&quot;com.connexience.ticket&quot;</code> parameters,
	 * respectively.
	 *
	 * @param username The {@link com.connexience.server.model.security.User}'s username.
	 * @param password The {@link com.connexience.server.model.security.User}'s password
	 * @param expires The length (in seconds) that the token should be valid for, defaults to 1 day (86400 seconds)
	 *
	 * @return An OK response containing a JSON object whose <code>jwt</code> property is an encoded and signed JWT
	 * token to be supplied to subsequent API calls. An error response will be returned if the credentials are invalid
	 * or an exception occurs.
	 */
	@POST
	@Path("/token")
	@Produces("application/json")
	Response token(@FormParam("username") String username, @FormParam("password") String password, @QueryParam("expires") @DefaultValue("86400") Long expires);

	/**
	 * Allows an authorized eScience Central administrator to acquire a {@link com.connexience.server.rest.api.jwt.JsonWebToken}
	 * representing another eScience Central {@link com.connexience.server.model.security.User}. This token can be
	 * passed to subsequent API calls and the API will respond as if that user had called the API themselves.
	 *
	 * @param oboId The {@link com.connexience.server.model.security.User} to issue a {@link
	 * com.connexience.server.rest.api.jwt.JsonWebToken} on behalf of.
	 * @param expires The length (in seconds) that the token should be valid for, defaults to 1 day (86400 seconds).
	 *
	 * @return An HTTP OK response whose body represents the encoded JWT to be used in subsequent HTTP requests or HTTP
	 * Forbidden or Error if the requested token cannot be issued due to permissions or an exception.
	 */
	@GET
	@Path("/token/{obo}")
	@Produces("application/json")
	Response token(@PathParam("obo") String oboId, @QueryParam("expires") @DefaultValue("86400") Long expires);

	/**
	 * Verifies the JWT token submitted in the <code>Authorization</code> header and returns a response whose body is
	 * the decoded payload of the JWT token (i.e., a plain JSON object representing the JWT's claims).
	 * <p/>
	 * This is used for debugging/testing purposes as the payload can be Base64URL on the client side although it may
	 * still be useful as it will validate both the signature and expiration time of the JWT.
	 *
	 * @return An HTTP OK response plus the JSON payload of the JWT (if the header is present and valid) or HTTP
	 * Forbidden if the token could not be extracted and verified from the Authorization header.
	 */
	@GET
	@Path("/token")
	@Produces("application/json")
	Response token();

	/**
	 * Validate the provided credentials and establish an {@link javax.servlet.http.HttpSession} containing the
	 * authenticated {@link com.connexience.server.model.security.User}'s {@link com.connexience.server.model.security.WebTicket}.
	 * The stored objects will be detected and used in subsequent API calls where the session can be re-established
	 * (usually by a cookie containing a <code>JSESSIONID</code> value).
	 *
	 * @param username The {@link com.connexience.server.model.security.User}'s username.
	 * @param password The {@link com.connexience.server.model.security.User}'s password.
	 *
	 * @return HTTP OK if successful and also the value of the JSESSIONID cookie in the body of the response. 400 BAD
	 * REQUEST if the login was unsuccessful
	 *
	 * @throws Exception If something goes wrong.
	 */
	@POST
	@Path("/login")
	@Produces("application/json")
	Response login(@FormParam("username") String username, @FormParam("password") String password) throws Exception;

        /**
         * Switch user ID for the session. This requires the user already to be authenticated as an Admin. It is used in
         * conjunction with the AdminServlet to impersonate Users.
         * 
         * @param userId The UserID to switch this session to
         * 
         * @return HTTP OK if the switch was successful and also the value of the JSESSIONID cookie in the body. 400
         * if login was unsuccessful.
         * 
         * @throws Exception if something goes wrong
         */
        @GET
        @Path("/switchsession/{userid}")
        @Produces("application/json")
        Response switchSession(@PathParam("userid")String userId) throws Exception;
        
	/**
	 * Logs the user in using via the rememberMe cookie mechanism.
	 *
	 * @param rememberMeCookie the UUID of the rememberMe in the DB
	 *
	 * @return 200 OK if successful and also the value of the JSESSIONID cookie in the body of the response. 400 BAD
	 * REQUEST  if the login was unsuccessful
	 *
	 * @throws Exception If something goes wrong.
	 */
	@POST
	@Path("/login/rememberMe/{rememberMeCookieValue}")
	@Produces("application/json")
	Response loginWithRememberMe(@PathParam("rememberMeCookieValue") String rememberMeCookie) throws Exception;

	/**
	 * Logs the current user out by terminating their current {@link javax.servlet.http.HttpSession}.
	 *
	 * @return 200 OK
	 *
	 * @throws Exception If something goes wrong.
	 */
	@GET
	@Path("/logout")
	@Produces("application/json")
	Response logout() throws Exception;

	/**
	 * Check if the currently logged in user is the public User.
	 * <p/>
	 * Called very frequently so doesn't touch the database.
	 *
	 * @return HTTP OK if there is a user logged in, HTTP BAD REQUEST if there is no user logged in.
	 *
	 * @throws Exception If something goes wrong.
	 */
	@GET
	@Path("/check")
	Response isNonPublicUser() throws Exception;
}

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.rest.api;

import com.connexience.server.rest.model.WebsiteWorkflow;
import com.connexience.server.rest.model.WebsiteWorkflowInvocation;
import com.connexience.server.rest.model.WebsiteWorkflowParameterList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/workflows
 * <p/>
 * User: nsjw7 Date: 30/11/2012 Time: 09:00
 */
public interface IWorkflowsAPI
{

	@GET
	@Path("")
	@Produces("application/json")
	WebsiteWorkflow[] listWorkflows() throws Exception;

	@GET
	@Path("/{id}")
	@Produces("application/json")
	WebsiteWorkflow getWorkflow(@PathParam("id") String workflowId) throws Exception;

	@DELETE
	@Path("/{id}")
    Response deleteWorkflow(@PathParam("id") String workflowId) throws Exception;

	@POST
	@Path("/{id}/invoke")
	@Produces("application/json")
	WebsiteWorkflowInvocation executeWorkflow(@PathParam("id") String workflowId) throws Exception;

	@POST
	@Path("/{id}/{versionid}/invoke")
	@Produces("application/json")
	WebsiteWorkflowInvocation executeWorkflow(@PathParam("id") String workflowId, @PathParam("versionid") String versionId) throws Exception;

	@POST
	@Path("/{id}/docinvoke")
	@Produces("application/json")
	@Consumes("text/plain")
	WebsiteWorkflowInvocation executeWorkflowOnDocument(@PathParam("id") String workflowId, String documentId) throws Exception;

	@POST
	@Path("/{id}/{versionid}/docinvoke")
	@Produces("application/json")
	@Consumes("text/plain")
	WebsiteWorkflowInvocation executeWorkflowOnDocument(@PathParam("id") String workflowId, @PathParam("versionid") String versionId, String documentId) throws Exception;

	@POST
	@Path("/{id}/paraminvoke")
	@Consumes("application/json")
	@Produces("application/json")
	WebsiteWorkflowInvocation executeWorkflowWithParameters(@PathParam("id") String workflowId, WebsiteWorkflowParameterList parameters) throws Exception;

	@POST
	@Path("/{id}/{versionid}/paraminvoke")
	@Consumes("application/json")
	@Produces("application/json")
	WebsiteWorkflowInvocation executeWorkflowWithParameters(@PathParam("id") String workflowId, @PathParam("versionid") String versionId, WebsiteWorkflowParameterList parameters) throws Exception;

    @GET
	@Path("/{id}/invocations")
	@Produces("application/json")
	WebsiteWorkflowInvocation[] listInvocationsOfWorkflow(@PathParam("id") String workflowId) throws Exception;

	@GET
	@Path("/invocations/{id}")
	@Produces("application/json")
	WebsiteWorkflowInvocation getInvocation(@PathParam("id") String invocationId) throws Exception;

	@POST
	@Path("/invocations/{id}/terminate")
	@Produces("application/json")
	WebsiteWorkflowInvocation terminateInvocation(@PathParam("id") String invocationId) throws Exception;
}

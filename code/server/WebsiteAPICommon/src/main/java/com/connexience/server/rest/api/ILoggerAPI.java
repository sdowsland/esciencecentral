package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.rest.model.project.WebsiteConversionWorkflow;
import com.connexience.server.rest.model.project.WebsiteFileType;
import com.connexience.server.rest.model.project.study.*;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/logger
 * <p/>
 * User: nsjw7
 * Date: 30/11/2012
 * Time: 09:00
 */
@Local
public interface ILoggerAPI {

    @GET
    @Path("/filetype/all")
    @Produces("application/json")
    Collection<WebsiteFileType> getLoggerFileTypes(@FormParam("start") int start, @FormParam("size") int size) throws ConnexienceException;

    @POST
    @Path("/filetype/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteFileType saveLoggerFileType(WebsiteFileType websiteFileType) throws ConnexienceException;

    @POST
    @Path("/filetype/{typeId}/remove")
    @Produces("application/json")
    Response removeLoggerFileType(@PathParam("typeId") int typeId) throws ConnexienceException;

    @GET
    @Path("/filetype/conversionworkflow/all")
    @Consumes("application/json")
    @Produces("application/json")
    Collection<WebsiteConversionWorkflow> getConversionWorkflows() throws ConnexienceException;

    @POST
    @Path("/filetype/conversionworkflow/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteConversionWorkflow saveConversionWorkflows(WebsiteConversionWorkflow websiteConversionWorkflow) throws ConnexienceException;

    @GET
    @Path("/type/all")
    @Produces("application/json")
    Collection<WebsiteLoggerTypeMin> getLoggerTypes(@FormParam("start") int start, @FormParam("end") int end) throws ConnexienceException;

    @POST
    @Path("/type/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteLoggerType saveLoggerType(WebsiteLoggerType websiteLoggerType) throws ConnexienceException;

    @POST
    @Path("/type/{typeId}/remove")
    Response deleteLoggerType(@PathParam("typeId") int typeId) throws ConnexienceException;

    @GET
    @Path("/type/{typeId}/sensor/all")
    @Produces("application/json")
    Collection<WebsiteSensor> getLoggerTypeSensors(@PathParam("typeId") String typeId) throws ConnexienceException;

    @POST
    @Path("/type/{typeId}/sensor/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteSensor saveLoggerTypeSensor(@PathParam("typeId") String typeId, WebsiteSensor websiteSensor) throws ConnexienceException;

    @POST
    @Path("/type/{typeId}/sensor/{sensorId}/remove")
    @Produces("application/json")
    Response removeLoggerTypeSensor(@PathParam("typeId") String typeId, @PathParam("sensorId") int sensorId) throws ConnexienceException;

    @GET
    @Path("/all")
    @Produces("application/json")
    Collection<WebsiteLogger> getLoggers(@FormParam("start") int start, @FormParam("end") int end) throws ConnexienceException;

    @GET
	@Path("/type/{typeId}/loggers/all")
	@Produces("application/json")
	Collection<WebsiteLoggerMin> getAllLoggersOfType(@PathParam("typeId") int typeId) throws ConnexienceException;

    @GET
    @Path("/type/{typeId}/loggers/available")
    @Produces("application/json")
    Collection<WebsiteLoggerMin> getUndeployedLoggersOfType(@PathParam("typeId") int typeId, @QueryParam("startDate") long startDate, @QueryParam("endDate") long endDate) throws ConnexienceException;

    @GET
    @Path("/type/loggers/available")
    @Produces("application/json")
    ArrayList<ArrayList<Integer>> getDeployedLoggersCountOfType(@QueryParam("typeIdList") ArrayList<Integer> typeIdList, @QueryParam("startDate") long startDate, @QueryParam("endDate") long endDate) throws ConnexienceException;

    @GET
    @Path("/{loggerId}")
    @Produces("application/json")
    WebsiteLogger getLogger(@PathParam("loggerId") int loggerId) throws ConnexienceException;

    @POST
    @Path("/type/{typeId}/logger/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteLogger saveLogger(@PathParam("typeId") int typeId, WebsiteLogger websiteLogger) throws ConnexienceException;

    @POST
    @Path("/type/{typeId}/loggers/upload")
    @Produces("application/json")
    @Consumes("multipart/form-data")
    Response saveLoggers(@PathParam("typeId") int typeId, @Context HttpServletRequest request) throws ConnexienceException;

    @POST
    @Path("/type/{typeId}/logger/{loggerId}/remove")
    @Produces("application/json")
    Response deleteLogger(@PathParam("typeId") int typeId, @PathParam("loggerId") int loggerId) throws ConnexienceException;

    @GET
    @Path("/configurations/")
    @Produces("application/json")
    Collection<WebsiteLoggerConfigurationMin> getAllConfigurations() throws ConnexienceException;

    @GET
    @Path("/configurations/{typeId}")
    @Produces("application/json")
    Collection<WebsiteLoggerConfigurationMin> getLoggerConfigurations(@PathParam("typeId") int typeId) throws ConnexienceException;

    @GET
    @Path("/configuration/{loggerConfigurationId}")
    @Produces("application/json")
    WebsiteLoggerConfiguration getLoggerConfiguration(@PathParam("loggerConfigurationId") int loggerConfigurationId) throws ConnexienceException;

    @POST
    @Path("/{loggerTypeId}/configuration/save")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteLoggerConfiguration saveLoggerConfiguration(@PathParam("loggerTypeId") int loggerTypeId, WebsiteLoggerConfiguration websiteLoggerConfiguration) throws ConnexienceException;

    @POST
    @Path("/configuration/{configurationId}/remove")
    @Produces("application/json")
    Response removeLoggerConfiguration(@PathParam("configurationId") int configurationId) throws ConnexienceException;

    @GET
    @Path("/{deploymentId}/deployment")
    @Produces("application/json")
	WebsiteLoggerDeployment getLoggerDeployment(@PathParam("deploymentId") int deploymentId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/deployments")
    @Produces("application/json")
    Collection<WebsiteLoggerDeploymentMin> getLoggerDeployments(@PathParam("studyId") int studyId) throws ConnexienceException;

    @GET
    @Path("/{studyId}/{groupId}/deployments")
    @Produces("application/json")
    Collection<WebsiteLoggerDeployment> getSubjectGroupLoggerDeployments(@PathParam("studyId") int studyId, @PathParam("groupId") int groupId, @DefaultValue("false") @QueryParam("includeInactive") Boolean includeInactive) throws ConnexienceException;

    @POST
    @Path("/{studyId}/{subjectGroupId}/deployment")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteLoggerDeployment saveLoggerDeployment(@PathParam("studyId") int studyId, @PathParam("subjectGroupId") int subjectGroupId, WebsiteLoggerDeployment websiteLoggerDeployment) throws ConnexienceException;

    @POST
    @Path("/deployment/{deploymentId}/remove")
    @Produces("application/json")
    Response removeLoggerDeployment(@PathParam("deploymentId") int deploymentId) throws ConnexienceException;

    @GET
    @Path("/logger/{loggerId}/deployments")
    @Produces("application/json")
    Collection<WebsiteLoggerDeploymentMin> getLoggerDeploymentsByLoggerId(@PathParam("loggerId") int loggerId) throws ConnexienceException;
}
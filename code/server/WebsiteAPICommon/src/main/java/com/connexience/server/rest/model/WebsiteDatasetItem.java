/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;


import javax.xml.bind.annotation.XmlType;

/**
 * This class contains a simplified representation of an item that is contained
 * within a Dataset.
 * @author hugo
 */
@XmlType
public class WebsiteDatasetItem  {
    /** The basic format of the item */
    public enum DATASET_ITEM_TYPE {
        MULTI_ROW, SINGLE_ROW
    }
    
    /** The strategy that will be applied to updates */
    public enum DATASET_ITEM_UPDATE_STRATEGY {
        REPLACE, MINIMUM, MAXIMUM, AVERAGE, SUM
    }
    
    private long id;
    private String name;
    private String datasetId;
    private DATASET_ITEM_TYPE itemType = DATASET_ITEM_TYPE.MULTI_ROW;
    private DATASET_ITEM_UPDATE_STRATEGY updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.REPLACE;
    private String stringValue;
    
    public WebsiteDatasetItem() {
    }

    public String getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(String datasetId) {
        this.datasetId = datasetId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setItemType(DATASET_ITEM_TYPE itemType) {
        this.itemType = itemType;
    }

    public DATASET_ITEM_TYPE getItemType() {
        return itemType;
    }

    public void setUpdateStrategy(DATASET_ITEM_UPDATE_STRATEGY updateStrategy) {
        this.updateStrategy = updateStrategy;
    }

    public DATASET_ITEM_UPDATE_STRATEGY getUpdateStrategy() {
        return updateStrategy;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }
}
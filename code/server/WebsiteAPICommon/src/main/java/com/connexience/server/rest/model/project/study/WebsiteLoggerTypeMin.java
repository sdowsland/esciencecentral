package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerConfiguration;
import com.connexience.server.model.project.study.LoggerType;
import com.connexience.server.model.project.study.Sensor;

import java.util.ArrayList;
import java.util.List;

public class WebsiteLoggerTypeMin
{
	private Integer id;

	private String name;

	private String manufacturer;

	private boolean physicalDevice = true;

	private Integer sensors;

	private Integer loggers;

	private Integer loggerConfigurations;

	protected WebsiteLoggerTypeMin()
	{
	}

	public WebsiteLoggerTypeMin(LoggerType loggerType)
	{
		this.setId(loggerType.getId());
		this.name = loggerType.getName();
		this.manufacturer = loggerType.getManufacturer();
		this.physicalDevice = loggerType.isPhysicalDevice();
        this.sensors = loggerType.getSensors().size();
        this.loggers = loggerType.getLoggers().size();
        this.loggerConfigurations = loggerType.getLoggerConfigurations().size();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getManufacturer()
	{
		return manufacturer;
	}

	public void setManufacturer(String manufacturer)
	{
		this.manufacturer = manufacturer;
	}

	public boolean isPhysicalDevice()
	{
		return physicalDevice;
	}

	public void setPhysicalDevice(boolean physicalDevice)
	{
		this.physicalDevice = physicalDevice;
	}

	public Integer getSensors()
	{
		return sensors;
	}

	public void setSensors(Integer sensors)
	{
		this.sensors = sensors;
	}

	public Integer getLoggers()
	{
		return loggers;
	}

	public void setLoggers(Integer loggers)
	{
		this.loggers = loggers;
	}

	public Integer getLoggerConfigurations()
	{
		return loggerConfigurations;
	}

	public void setLoggerConfigurations(Integer loggerConfigurations)
	{
		this.loggerConfigurations = loggerConfigurations;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

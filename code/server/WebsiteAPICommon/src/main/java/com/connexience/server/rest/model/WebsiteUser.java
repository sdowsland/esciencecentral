/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.profile.UserProfile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Wrapper for internal User object
 *
 * @author hugo
 */

public class WebsiteUser extends WebsiteObject
{
    private String email;
    private String firstname;
    private String lastname;
    private String bio;
    private Boolean admin;
    private String homeFolderId;
    private List<WebsiteUserMin> friends = new ArrayList<>();
    private List<WebsiteGroupMin> groups = new ArrayList<>();
    private Map<String, WebsiteMetadataItem> metadata = new HashMap<>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Boolean isAdmin() { return admin; }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public List<WebsiteUserMin> getFriends() {
        return friends;
    }

    public void setFriends(List<WebsiteUserMin> friends) {
        this.friends = friends;
    }

    public List<WebsiteGroupMin> getGroups() {
        return groups;
    }

    public void setGroups(List<WebsiteGroupMin> groups) {
        this.groups = groups;
    }

    public String getHomeFolderId() {
        return homeFolderId;
    }

    public void setHomeFolderId(String homeFolderId) {
        this.homeFolderId = homeFolderId;
    }

    public Map<String, WebsiteMetadataItem> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, WebsiteMetadataItem> metadata) {
        this.metadata = metadata;
    }

    public WebsiteUser() {}

    public WebsiteUser(User user, Boolean metadata, Ticket ticket) throws ConnexienceException {

        UserProfile userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, user.getId());
        Collection<User> userConnections = EJBLocator.lookupLinkBean().getLinkedSinkObjects(ticket, user, User.class);
        List<Group> userGroups = EJBLocator.lookupUserDirectoryBean().listUserGroups(ticket, user.getId());
        List<MetadataItem> metaDataItems = EJBLocator.lookupMetaDataBean().getObjectMetadata(ticket, user.getId());

        this.setId(user.getId());
        this.email = userProfile.getEmailAddress();
        this.firstname = user.getFirstName();
        this.lastname = user.getSurname();
        this.bio = userProfile.getText();
        this.admin = EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, user.getId(), ticket.getOrganisationId());
        this.homeFolderId = user.getHomeFolderId();

        for (User connection : userConnections) {
            this.friends.add(new WebsiteUserMin(connection, false, ticket));
        }

        for (Group group : userGroups) {
            if(!group.isProtectedGroup())
            {
                this.groups.add(new WebsiteGroupMin(group));
            }
        }

        if(metadata){
            for(MetadataItem metaDataItem : metaDataItems) {
                this.metadata.put(metaDataItem.getName(), new WebsiteMetadataItem(metaDataItem));
            }
        }
    }

    public static WebsiteUser saveUser(WebsiteUser user, Ticket ticket) throws ConnexienceException, ParseException {

        UserDirectoryRemote userDirectoryRemote = EJBLocator.lookupUserDirectoryBean();

        User connexienceUser = userDirectoryRemote.getUser(ticket, user.getId());
        UserProfile userProfile = userDirectoryRemote.getUserProfile(ticket, user.getId());

        if(connexienceUser != null){

            connexienceUser.setFirstName(user.getFirstname());
            connexienceUser.setSurname(user.getLastname());
            userDirectoryRemote.saveUser(ticket, connexienceUser);

            userProfile.setText(user.getBio());
            userProfile.setEmailAddress(user.getEmail());
            userDirectoryRemote.saveUserProfile(ticket, user.getId(), userProfile);

            for (String metadataKey : user.getMetadata().keySet()) {

                WebsiteMetadataItem metadataItem = user.getMetadata().get(metadataKey);

                switch (metadataItem.getMetadataType()) {
                    case BOOLEAN:
                        BooleanMetadata booleanMetadata = new BooleanMetadata();
                        booleanMetadata.setCategory(metadataItem.getCategory());
                        booleanMetadata.setName(metadataItem.getName());
                        booleanMetadata.setBooleanValue(Boolean.parseBoolean(metadataItem.getStringValue()));
                        EJBLocator.lookupMetaDataBean().addMetadata(ticket, user.getId(), booleanMetadata);
                        break;
                    case DATE:

                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

                        DateMetadata dateMetadata = new DateMetadata();
                        dateMetadata.setCategory(metadataItem.getCategory());
                        dateMetadata.setName(metadataItem.getName());
                        dateMetadata.setDateValue(dateFormat.parse(metadataItem.getStringValue()));
                        EJBLocator.lookupMetaDataBean().addMetadata(ticket, user.getId(), dateMetadata);
                        break;
                    case NUMERICAL:
                        NumericalMetadata numericalMetadata = new NumericalMetadata();
                        numericalMetadata.setCategory(metadataItem.getCategory());
                        numericalMetadata.setName(metadataItem.getName());
                        if (metadataItem.getStringValue() != null && !metadataItem.getStringValue().equals("")) {
                            numericalMetadata.setDoubleValue(Double.parseDouble(metadataItem.getStringValue()));
                        }
                        EJBLocator.lookupMetaDataBean().addMetadata(ticket, user.getId(), numericalMetadata);
                        break;
                    case TEXT:
                        TextMetadata textMetadata = new TextMetadata();
                        textMetadata.setCategory(metadataItem.getCategory());
                        textMetadata.setName(metadataItem.getName());
                        textMetadata.setTextValue(metadataItem.getStringValue());
                        EJBLocator.lookupMetaDataBean().addMetadata(ticket, user.getId(), textMetadata);
                }
            }

        }
        else {
            throw new ConnexienceException("User not found");
        }

        return user;
    }

}
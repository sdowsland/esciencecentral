package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.LoggerData;
import com.connexience.server.model.project.study.LoggerDeployment;

import java.util.*;

public class WebsiteLoggerDeployment
{
	private Integer id;

	private String dataFolderId;

	private boolean active = false;

	private Integer studyId;

	private Integer subjectGroupId;

	private WebsiteLoggerMin logger;

	private Integer loggerConfigurationId;

	private String loggerConfigurationName;

	private List<Integer> loggerData = new ArrayList<>();

	private Map<String, String> additionalProperties = new HashMap<>();

    private Date startDate;

    private Date endDate;

	protected WebsiteLoggerDeployment()
	{
	}

	public WebsiteLoggerDeployment(LoggerDeployment deployment)
	{
		this.setId(deployment.getId());
		this.dataFolderId = deployment.getDataFolderId();
		this.active = deployment.isActive();

		// not cyclic (no reverse reference)
		this.studyId = deployment.getStudy().getId();

		// cyclic, use instance here
		this.logger = new WebsiteLoggerMin(deployment.getLogger());

		// cyclic, use ID here instead
		this.loggerConfigurationId = deployment.getLoggerConfiguration().getId();

		this.loggerConfigurationName = deployment.getLoggerConfiguration().getName();

		// cyclic, use IDs at other side
		this.subjectGroupId = deployment.getSubjectGroup().getId();

		// cyclic, use IDs here instead
		for (LoggerData data : deployment.getLoggerData())
		{
			loggerData.add(data.getId());
		}

        this.startDate = deployment.getStartDate();

        this.endDate = deployment.getEndDate();

		// not cyclic
		for (String key : deployment.getAdditionalProperties().keySet())
		{
			this.additionalProperties.put(key, deployment.getAdditionalProperties().get(key));
		}
	}

	public String getDataFolderId()
	{
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId)
	{
		this.dataFolderId = dataFolderId;
	}

	public boolean isActive()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public Integer getStudyId()
	{
		return studyId;
	}

	public void setStudyId(Integer studyId)
	{
		this.studyId = studyId;
	}

    public Integer getSubjectGroupId()
    {
        return subjectGroupId;
    }

    public void setSubjectGroupId(Integer subjectGroupId)
    {
        this.subjectGroupId = subjectGroupId;
    }

	public Integer getLoggerConfigurationId()
	{
		return loggerConfigurationId;
	}

	public void setLoggerConfigurationId(Integer loggerConfigurationId) {
		this.loggerConfigurationId = loggerConfigurationId;
	}

    public String getLoggerConfigurationName() {
        return loggerConfigurationName;
    }

    public void setLoggerConfigurationName(String loggerConfigurationName) {
        this.loggerConfigurationName = loggerConfigurationName;
    }

    public List<Integer> getLoggerData()
	{
		return loggerData;
	}

	public void setLoggerData(List<Integer> loggerData)
	{
		this.loggerData = loggerData;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public WebsiteLoggerMin getLogger()
	{
		return logger;
	}

	public void setLogger(final WebsiteLoggerMin logger)
	{
		this.logger = logger;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}

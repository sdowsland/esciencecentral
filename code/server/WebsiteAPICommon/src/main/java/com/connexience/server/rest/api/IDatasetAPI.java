/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.rest.model.WebsiteDataset;
import com.connexience.server.rest.model.WebsiteDatasetItem;
import com.connexience.server.rest.model.WebsiteDatasetKeyList;

import java.util.List;


public interface IDatasetAPI {

    public String getDashboardItemJsonValue(String dashboardId, String name) throws ConnexienceException;


    //Dataset Operations

    public WebsiteDataset[] listDatasets() throws Exception;

    public WebsiteDataset getDataset(String id) throws Exception;

    public WebsiteDataset findDataset(String name, String userId) throws Exception;

    public WebsiteDataset updateDataset(String id, WebsiteDataset ds) throws Exception;

    public WebsiteDataset createDataset(String name) throws Exception;

    public void deleteDataset(String id) throws Exception;

    public void resetDataset(String id) throws Exception;

    public WebsiteDatasetItem[] getDatasetContents(String id) throws Exception;

    public WebsiteDatasetItem[] aggregateDatasetContents(String groupId, String name) throws Exception;


    //Dataset Item Operations

    public WebsiteDatasetItem getDatasetItem(String id, String itemName) throws Exception;

    public WebsiteDatasetItem findDatasetItem(String datasetName, String itemName) throws Exception;

    public String updateDatasetItem(String datasetId, String itemName, Long rowId, String jsonData) throws Exception;

    public void deleteDatasetItem(String datasetId, String itemName) throws Exception;

    public Integer getDatasetItemSize(String datasetId, String itemName) throws Exception;

    public String getDatasetItemContents(String datasetId, String itemName, int start, int pagesize) throws Exception;

    public String getDatasetItemContentsByName(String datasetName, String itemName, int start, int pagesize, String userId) throws Exception;

    public WebsiteDatasetItem addItemToDataset(String id, WebsiteDatasetItem item) throws Exception;


    //Dataset Multi Value Item Operations

    public String getMultiValueDatasetItemRows(String datasetId, String itemName, int startRow, int maxResults, WebsiteDatasetKeyList keys) throws Exception;

    public List<Object> findMultiValueDatasetItemRows(String datasetName, String itemName, int startRow, int maxResults, List<String> userIds) throws Exception;

    public String getMultiValueDatasetItemRow(String datasetId, String itemName, Long rowId) throws Exception;

    public String findMultiValueDatasetItemRow(String datasetName, String itemName, Long rowId, String userId) throws Exception;

    public WebsiteDatasetItem[] appendToMultipleValueDatasetItem(String datasetId, String itemName, String jsonData) throws Exception;

    public void updateMultipleValueDatasetItem(String datasetId, String itemName, Long rowId, String jsonData) throws Exception;

    public void removeFromMultipleValueDatasetItem(String datasetId, String itemName, Long rowId) throws Exception;
}
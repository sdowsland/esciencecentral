/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import java.util.Date;

/**
 * User: nsjw7
 * Date: 06/12/2012
 * Time: 15:35
 */
public class WebsiteMessage extends WebsiteObject {

    /** Time that this request was made */
    private Date timestamp;

    /** Text of the message */
    private String message;

    /** The sender */
    private WebsiteUserMin sender;

    /** The recipient */
    private WebsiteUserMin recipient;

    public WebsiteMessage() {
    }

    public WebsiteMessage(Date timestamp, String message, WebsiteUserMin sender, WebsiteUserMin recipient) {
        this.timestamp = timestamp;
        this.message = message;
        this.sender = sender;
        this.recipient = recipient;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WebsiteUserMin getSender() {
        return sender;
    }

    public void setSender(WebsiteUserMin sender) {
        this.sender = sender;
    }

    public WebsiteUserMin getRecipient() {
        return recipient;
    }

    public void setRecipient(WebsiteUserMin recipient) {
        this.recipient = recipient;
    }
}

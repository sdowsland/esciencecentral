package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Phase;

@SuppressWarnings("unused")
public class WebsitePhaseMin {
    private Integer id;

    private Integer studyId;

    private String name;

    public WebsitePhaseMin() {
    }

    public WebsitePhaseMin(Phase phase) {
        this.setId(phase.getId());
        this.setName(phase.getName());

        if (phase.getStudy() != null) {
            this.setStudyId(phase.getStudy().getId());
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudyId() {
        return studyId;
    }

    public void setStudyId(Integer studyId) {
        this.studyId = studyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

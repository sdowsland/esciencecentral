/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;


import javax.xml.bind.annotation.XmlType;

/**
 * This class holds a list of String keys for a query
 * @author hugo
 */
@XmlType
public class WebsiteDatasetKeyList {
    /** List of keys */
    private String[] keys;

    public WebsiteDatasetKeyList() {
    }


    public void addKey(String key){
        if(keys==null){
            keys = new String[1];
            keys[0] = key;
        } else {
            String[] newValues = new String[keys.length + 1];
            for(int i=0;i<keys.length;i++){
                newValues[i] = keys[i];
            }
            newValues[newValues.length - 1] = key;
            keys = newValues;
        }
    }

    public String[] getKeys() {
        return keys;
    }

    public void setKeys(String[] keys) {
        this.keys = keys;
    }

}

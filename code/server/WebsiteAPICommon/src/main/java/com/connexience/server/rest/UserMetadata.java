/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest;

/**
 * This contains constants which are used as keys for the Metadata within
 * MoveECloud
 *
 * User: nsjw7
 * Date: 06/12/2012
 * Time: 14:09
 */
public interface UserMetadata {
    public static final String METADATA_CATEGORY = "user";

    public static final String JOB_TITLE = "jobtitle";

    public static final String ORGANISATION = "organisation";

    public static final String HOMEPAGE = "homepage";

    public static final String DASHBOARD_PREFS = "dashboardprefs";

}

package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Phase;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.rest.model.project.WebsiteProject;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WebsiteStudy extends WebsiteProject implements Serializable
{

	private Date startDate = null;

	private Date endDate = null;

    private boolean visibleOnExternalSite = false;

    private List<WebsitePhase> phases = new ArrayList();

	public WebsiteStudy()
	{
	}

	public WebsiteStudy(Study study)
	{
		super(study);

		this.startDate = study.getStartDate();
		this.endDate = study.getEndDate();

        for(Phase phase : study.getPhases()){
            this.phases.add(new WebsitePhase(phase));
        }
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

    public List<WebsitePhase> getPhases() { return phases;}

    public void setPhases(List<WebsitePhase> phases) {
        this.phases = phases;
    }

    public boolean isVisibleOnExternalSite() {
        return visibleOnExternalSite;
    }

    public void setVisibleOnExternalSite(boolean visibleOnExternalSite) {
        this.visibleOnExternalSite = visibleOnExternalSite;
    }

    @Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof WebsiteStudy))
		{
			return false;
		}

		WebsiteStudy study = (WebsiteStudy) o;

		if (!(getId().equals(study.getId())))
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		return getId();
	}

	public Study toStudy()
	{
		Study study = new Study(getName(), getOwnerId());

		if (getId() != null)
		{
			study.setId(getId());
		}

        if (getExternalId() != null)
        {
            study.setExternalId(getExternalId());
        }

		if (getDescription() != null)
		{
			study.setDescription(getDescription());
		}

		if (this.startDate != null)
		{
			study.setStartDate(this.startDate);
		}

		if (this.endDate != null)
		{
			study.setEndDate(this.endDate);
		}

        if (this.phases != null)
        {
            List<Phase> phases = new ArrayList<>();

            for(WebsitePhase phase : this.phases){
                Phase p = new Phase();

                p.setId(p.getId());
                p.setName(p.getName());
                p.setStudy(study);
                p.setSubjectGroups(p.getSubjectGroups());

                phases.add(p);
            }

            study.setPhases(phases);
        }

		study.setPrivateProject(isPrivateProject());

        study.setVisibleOnExternalSite(visibleOnExternalSite);

		if (getAdminGroupId() != null)
		{
			study.setAdminGroupId(getAdminGroupId());
		}

		if (getMembersGroupId() != null)
		{
			study.setMembersGroupId(getMembersGroupId());
		}

		if (getWorkflowFolderId() != null)
		{
			study.setWorkflowFolderId(getWorkflowFolderId());
		}

		if (getDataFolderId() != null)
		{
			study.setDataFolderId(getDataFolderId());
		}

		for (String key : this.getAdditionalProperties().keySet())
		{
			study.getAdditionalProperties().put(key, this.getAdditionalProperties().get(key));
		}

        if(getRemoteScannerId() != null){
            study.setRemoteScannerId(Long.parseLong(getRemoteScannerId()));
        }


		return study;
	}
}

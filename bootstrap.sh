#!/usr/bin/env bash


# Update all repositories
aptitude -y update


# Install Open JDK 7, Postgres 9,1, unzip
aptitude install -y openjdk-7-jdk postgresql-9.1  unzip


# Enable localhost connections to Postgres (and restart)
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = 'localhost'/g" /etc/postgresql/9.1/main/postgresql.conf
/etc/init.d/postgresql restart


# Configure Postgres
su - postgres -c "psql -c \"CREATE ROLE connexience WITH LOGIN UNENCRYPTED PASSWORD 'connexience'\""
su - postgres -c "psql -c \"CREATE DATABASE connexience WITH OWNER connexience\""
su - postgres -c "psql -c \"CREATE DATABASE logeventsdb WITH OWNER connexience\""
su - postgres -c "psql -c \"CREATE DATABASE performancedb WITH OWNER connexience\""


# Extract JBoss 7.1.3.final to /home/vagrant/jboss
unzip /vagrant/documentation/buildAndInstall/jboss-as-7.1.3.final.zip -d /home/vagrant/jboss


# Apply eSC configuration to JBoss standalone
rsync -rv /vagrant/documentation/buildAndInstall/jboss7/* /home/vagrant/jboss/


# CHOWN everything in home dir
chown -R vagrant:vagrant /home/vagrant/*


# Make an /orgdata directory and give it to vagrant user
mkdir /orgdata
chown vagrant:vagrant /orgdata


# Deploy inkspot.ear to jboss if a built version exists
if [ -f /vagrant/code/server/Ear/target/inkspot.ear ]
then
	echo 'Found a built inkspot.ear, copying to /home/vagrant/jboss/standalone/deployments/'
	cp -p /vagrant/code/server/Ear/target/inkspot.ear /home/vagrant/jboss/standalone/deployments/
fi


# Configure JBoss 7 to launch in BG and write PID file and
# create aliases for deploy, jboss up/down/tail
(
cat <<'EOF'

export LAUNCH_JBOSS_IN_BACKGROUND='true'
export JBOSS_PIDFILE='/home/vagrant/jboss.pid'

alias deploy='cp -p /vagrant/code/server/Ear/target/inkspot.ear /home/vagrant/jboss/standalone/deployments/'
alias up='nohup /home/vagrant/jboss/bin/standalone.sh -Djboss.bind.address.management=0.0.0.0 -Djboss.bind.address=0.0.0.0 &> /dev/null &'
alias down='kill `cat /home/vagrant/jboss.pid`'
alias jtail='tail -f /home/vagrant/jboss/standalone/log/server.log'
alias clean='rm -rf /home/vagrant/jboss/standalone/{data,log,tmp}'
EOF
) >> /home/vagrant/.profile

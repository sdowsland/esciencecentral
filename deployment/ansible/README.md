This directory includes a set of ansible playbooks, roles and a template of an inventory file that may be used to deploy e-SC instance.

To use it you'll need:

- a linux machine to run the ansible playbooks, which includes:
    - ansible from http://www.ansible.com (works best on branch `stable-1.9`). See installation manual
      at: http://docs.ansible.com/intro_installation.html
    - compiled and built code of e-Science Central (check whether
      `code/server/Ear/target/esc.ear` exists. If it doesn't, run
      `mvn clean install` in the `code` directory)
- one or more target linux machines (currently only Ubuntu is supported)

Before you can run the ansible playbook you'll need to configure access to your
target systems. The easiest way is to use `ssh-copy-id` to exchange keys, so
that you can log in to a remote machine without being asked for password.

Once you can log in without password to all target remote systems, you'll need
to make a copy of [`inventory-template.ini`](inventory-template.ini) and customise it according to your needs; look inside the file, read comments and change accordingly. You'll need to set at least three things:

* remote user name,
* password when doing sudo from the remote user account on the target machine; can be commented out if not needed,
* address of the target machine(s) where the database, the main server and
  workflow engine are going to be located.

Regarding the target machines, the simplest case is to set all of these to
the same address. Then all three components will be running on the same host
(all-in-one deployment).

Also, it is recommended to change settings in [`server-standalone.yml`](server-standalone.yml) and [`group_vars/esc_server'](group_vars/esc_server) and perhaps ['roles/esc-server/vars/main.yml`](roles/esc-server/vars/main.yml).
You can find there settings starting with `RegInfo_` used to register an e-SC organisation under which the system will operate. You should change at least administrator password (the `RegInfo_Password` property).

After you adjusted all the settings you are ready to go. Just run:

~~~~~
ansible-playbook site-standalone.yml -i my_inventory.ini
~~~~~

Note that it may take some time to prepare a clean Ubuntu OS. However, most of
the time is spent on installing dependency packages like java and postgres.
To make things quicker you can consider using as a baseline a VM image that
already includes JRE-7. It'll be needed by the server and engines.

Once the all-in-one deployment works, you can play with more sophisticated
configurations, e.g. add more engines to the inventory, remove engine from
the server (an ansible role to do this will be delivered soon), move database
to another host.

Further steps to make the system usable is to install core workflow libraries
and blocks. You should be able to find information about this in the building
manual, though.


Please send any issues and questions to:
`esciencecentral-users@lists.sourceforge.net`

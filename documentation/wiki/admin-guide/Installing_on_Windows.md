<h1>Installing on Windows</h1>

This guide is for Microsoft Windows system. It was tested on Windows 7, 8, Server 2012 R2 but it's likely to work on most of other flavours.

[TOC]

## Server Installation ##

### Database ###

From <http://www.postgresql.org> you can get the latest postgres 9.x. Binary packages are available directly from [here](http://www.enterprisedb.com/products-services-training/pgdownload#windows).

Download the version appropriate for your system (32- or 64-bit) and follow the installation wizard. Remember superuser password and the port number the server will listen on (default 5432). Use default locale.

The installation takes a few minutes...

... and then do not run Stack Builder unless you need it for some other purposes. e-Science Central does not need any extra PostgreSQL packages.

#### to be continued... ####

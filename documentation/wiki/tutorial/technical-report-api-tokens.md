# eScience Central API Tokens

## Introduction

A new endpoint has been added to the REST api allowing you to acquire tokens for authorisation against API calls.

`POST /website-api/rest/account/token` with form parameters username and password will return a signed, encoded token (called a JSON Web Token).

Running:

`curl --data "username=user&password=pass" http://localhost:8080/website-api/rest/account/token`

Returns something like:


	{ jwt: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzOTYwMTAwNzEsInN1YiI6IjU1IiwibmJmIjoxMzk1OTIzNjcxLCJhdWQiOiJweW8ubmdyb2suY29tIiwiaXNzIjoiZXNjIiwianRpIjoiYzc4OGMyMzctMzk4Yi00ZTM3LTkzNjUtYzMxMTQxODkyMzU4IiwiaWF0IjoxMzk1OTIzNjcxfQ.T35F7OMXECNZXvGtJ0J0k3ST6-sEr8QPhBhk-0aEptI' }

The string value of the `jwt` property is an encoded and signed Json Web Token.

To use the token when you make an API call, pass the value in the `Authorization` header.

For example:

    curl --header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzOTYwMDg4NjEsInN1YiI6IjU1IiwibmJmIjoxMzk1OTIyNDYxLCJhdWQiOiJweW8ubmdyb2suY29tIiwiaXNzIjoiZXNjIiwianRpIjoiYTBiODEzMWMtMzQ1Yy00YjExLWI2YjMtODYzZmZjOGQ0ZWU5IiwiaWF0IjoxMzk1OTIyNDYxfQ.fvYhx7SWdcUOoC7GXpRkO-apC5NC9xlYSX-p88GTyR8' http://localhost:8080/website-api/rest/stats/workflow

## Masquerading as another User

If you are an admin and you acquire a token through `/website-api/rest/account/token` you can also acquire tokens representing other eSC users by doing `GET /website-api/rest/account/token/:escId` -- you **must** provide your Admin token in the `Authorization` header to this request.

For example:

    curl --header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzOTYwMDg4NjEsInN1YiI6IjU1IiwibmJmIjoxMzk1OTIyNDYxLCJhdWQiOiJweW8ubmdyb2suY29tIiwiaXNzIjoiZXNjIiwianRpIjoiYTBiODEzMWMtMzQ1Yy00YjExLWI2YjMtODYzZmZjOGQ0ZWU5IiwiaWF0IjoxMzk1OTIyNDYxfQ.fvYhx7SWdcUOoC7GXpRkO-apC5NC9xlYSX-p88GTyR8' http://localhost:8080/website-api/rest/account/token/3106
    
Would check that you are an administrator (using the `Authorization` header) and then issue a token representing the eSC user with ID `3106`. You then have two tokens you can use against API requests depending on who you wish to masquerade as.

## Token Expiration Time

Tokens are valid for **1 day** by default, you can extend or shorten this through the initial token request by adding the `expires` query string parameter, which specifies *how long the token should be valid for in **seconds***.

For example: 

    POST /website-api/rest/account/token?expires=3600

Would give you a token valid for **1 hour**. The parameter works when acquiring a token on behalf of another user, for example:

    GET /website-api/rest/account/token/3106?expires=3600
    
## Compatibility

The only change to the existing WebsiteAPI classes are calls to `SessionUtils.getTicket(...)` (and `getOrganisation()`) were replaced by corresponding calls to `TransitionSessionUtils`, this allows all API methods to use the Token (stateless) and Session (stafeful) modes of Auth/Authz.

Ensure you use `TransitionSessionUtils` instead of `SessionUtils` and everything will work seamlessly.

## Summary

1. New API methods
	1. `POST /website-api/rest/account/token` (acquire a token)
	2. `GET /website-api/rest/account/token` (inspect your token)
	3. `GET /website-api/rest/account/token/:escId` (acquire a token o.b.o another user)
2. New `TransitionSessionUtils` class to allow API methods to use old and new mechanisms for auth/authz.

## Future Work

Each token current has a randomly generated and completely transient token identifier (`jti`) property. In future it may be desirable to track these identifiers in the database such that tokens can be invalidated before their expiry time, if desired.


## Under the Hood

Classes representing tokens and their ability to sign were implemented in the Website API common project (under the `com.connexience.server.rest.api.jwt` package). 

A `WebFilter` class has been implemented to detect if the `Authorization` header is present and if so, verify the token and inject a decoded token (and corresponding `WebTicket`) into the `HttpServletRequest` object. These can be accessed through:

- `request.getAttribute("com.connexience.ticket")`
- `request.getAttribute("com.connexience.jwt")`

`TransitionSessionUtils` has been added to the Website API project, implementing `getTicket(HttpServletRequest)` and `getOrganisation(HttpServletRequest)` methods. These methods check first if the injected attributes are present and if not, falls back on the underlying `HttpSession` based methods of `SessionUtils`.

The digital signatures are generated and validated using each users specific key. Accessed through `EJBLocator.lookupCertificateBean()`.

**NOTE:** We really need to address the use of the hardcoded KeyStore password soon, especially with potential FDA approval looming.



## JWT Token Structure

A token looking like

    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzOTYwMTAwNzEsInN1YiI6IjU1IiwibmJmIjoxMzk1OTIzNjcxLCJhdWQiOiJweW8ubmdyb2suY29tIiwiaXNzIjoiZXNjIiwianRpIjoiYzc4OGMyMzctMzk4Yi00ZTM3LTkzNjUtYzMxMTQxODkyMzU4IiwiaWF0IjoxMzk1OTIzNjcxfQ.T35F7OMXECNZXvGtJ0J0k3ST6-sEr8QPhBhk-0aEptI
    
Is 3 pieces of information, separated by the `.` character. The format is:

    <Base 64 URL Encoded Header>.<Base 64 URL Encoded Payload>.<Digital Signature>

The signature ensures the token cannot be tampered with and remain valid to the server. This also means you can manually break apart the string and Base64 decode the middle token to see the Json Web Token's information. Decoding the example's middle fragment yields:

    {
      "exp":1396010071,
      "sub":"55",
      "nbf":1395923671,
      "aud":"pyo.ngrok.com",
      "iss":"esc",
      "jti":"c788c237-398b-4e37-9365-c31141892358",
      "iat":1395923671
    }

The key fields are `sub` and `exp`, these refer to the eSC user ID and token expiration time respectively.

There is also a convenience method in the API for returning the unencoded payload of your JWT token, under the URI `GET /website-api/rest/account/token`. This method will also verify the signature and expiration time of the token. For example:

    curl --header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzOTYwMDg5NzAsInN1YiI6IjMxMDYiLCJuYmYiOjEzOTU5MjI1NzAsImF1ZCI6InB5by5uZ3Jvay5jb20iLCJpc3MiOiJlc2MiLCJqdGkiOiI2N2UxODA4My1lOTE1LTQzYzctOTczZS1iODc0MjlmYjUzODgiLCJpYXQiOjEzOTU5MjI1NzB9.SDrKV_hLkN23t5q8_gYspjrl8kph_hapdvSE7ygQZzI' http://pyo.ngrok.com/website-api/rest/account/token

Will return:

    {"exp":1396008970,"sub":"3106","iss":"esc","aud":"pyo.ngrok.com","nbf":1395922570,"jti":"67e18083-e915-43c7-973e-b87429fb5388","iat":1395922570}


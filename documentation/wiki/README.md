Welcome to the e-Science Central Wiki.  Whilst this is work in progress you may find some useful information about building, installing and running e-Science Central on here.

Please visit [http://www.esciencecentral.co.uk](http://www.esciencecentral.co.uk) for a demo installation or download the code and run it yourself.

For help on installing e-Science Central please see the [manuals](../manuals) directory or take a look at an automated way to install system using [ansible playbooks](../../deployment/ansible).

To get information on how to develop your own workflow blocks or how to integrate with e-Science Central via its API have a look at the [developers guide](devs-guide).

You may also want to look at the [tutorial](tutorial) to get some basic help with usage and development with the system. 

The above documents are in the preliminary stage and will be extended based on the current user needs. Please report any issues or questions to [esciencecentral-users@lists.sourceforge.net](mailto:esciencecentral-users@lists.sourceforge.net)

The e-Science Central developers currently include:

Simon Woodman  
Hugo Hiden  
Jacek Cała  
Mark Turner  
Dominic Searson  
Stephen Dowsland  

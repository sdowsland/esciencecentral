<h1>Getting started with the REST API</h1>

The e-Science Central REST API is broken into a number of parts, each of which allows you to access a specific piece of system functionality. The following interfaces are currently provided:

1. [Storage](Storage-REST-API-r2317): allows access and manipulation of data files and folders stored within the system
*  [Workflow](Workflow-REST-API-r2317): allows execution of workflows stored in e-SC
*  [Datasets](Datasets-REST-API-r2317): allows access and manipulation of data objects like scalars, vectors and matrices

Using the links above you can refer to the details about each of these interfaces. This will give you basic information about how to access the interface via HTTP.

As the interfaces produce and consume JSON, you can easily try using them with **JavaScript**. However, if you use **Java** or **C#**, you may want to use our client libraries instead:

* [The Java client library](Java-REST-client-r2317)
    * [Storage API client](Java-REST-client-r2317/#storage-api-client)
    * [Workflow API client](Java-REST-client-r2317/#workflow-api-client)
    * [Datasets API client](Java-REST-client-r2317/#dataset-api-client)
* [The C# client library](CSharp-REST-client-r2317)
    * [Storage API client](CSharp-REST-client-r2317/#storage-api-client)
    * [Workflow API client](CSharp-REST-client-r2317/#workflow-api-client)
    * [Datasets API client](CSharp-REST-client-r2317/#dataset-api-client)


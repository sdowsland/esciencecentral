-- Index: objflat_containerid

-- DROP INDEX objflat_containerid;

CREATE INDEX objflat_containerid
  ON objectsflat
  USING hash
  (containerid);

-- Index: objflat_creatorid

-- DROP INDEX objflat_creatorid;

CREATE INDEX objflat_creatorid
  ON objectsflat
  USING hash
  (creatorid);

-- Index: objflat_id

-- DROP INDEX objflat_id;

CREATE INDEX objflat_id
  ON objectsflat
  USING hash
  (id);

-- Index: perms_principalid

-- DROP INDEX perms_principalid;

CREATE INDEX perms_principalid
  ON permissions
  USING hash
  (principalid);

-- Index: perms_targetid

-- DROP INDEX perms_targetid;

CREATE INDEX perms_targetid
  ON permissions
  USING hash
  (targetobjectid);

  CREATE INDEX ON loggerconfigurations(id);

CREATE INDEX ON loggerdeployments(id);
CREATE INDEX ON loggerdeployments(logger_id);
CREATE INDEX ON loggerdeployments(study_id);
CREATE INDEX ON loggerdeployments(subjectgroup_id);
CREATE INDEX ON loggerdeployments(loggerconfiguration_id);

CREATE INDEX ON loggers(id);
CREATE INDEX ON loggers(loggertype_id);
CREATE INDEX ON loggers(serialnumber);

CREATE INDEX ON loggertypes(id);

CREATE INDEX ON phases(id);
CREATE INDEX ON phases(study_id);

CREATE INDEX ON projects(id);

CREATE INDEX ON subjectgroups(id);
CREATE INDEX ON subjectgroups(study_id);
CREATE INDEX ON subjectgroups(phase_id);
CREATE INDEX ON subjectgroups(parent_id);

CREATE INDEX ON subjects(id);
CREATE INDEX ON subjects(subjectgroup_id);




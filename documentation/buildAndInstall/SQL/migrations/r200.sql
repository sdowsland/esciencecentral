--The name of the owner may need to be changed

CREATE SEQUENCE esc_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10
  CACHE 1;
ALTER TABLE esc_sequence
  OWNER TO connexience;

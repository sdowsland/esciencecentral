#!/bin/bash

source conf

fuser $JBOSS_HOME/jboss-modules.jar | xargs kill -9

rm -rf $JBOSS_HOME || exit 1

#!/bin/bash
#
# This script installs the required local jars into the maven repository
#

#Neo4j JCA Connector
mvn install:install-file -DgroupId=com.netoprise -DartifactId=neo4j-connector-api -Dversion=0.3 -Dpackaging=jar -Dfile=../code/jars/neo4j-connector-api-0.3.jar

# JCaptcha
mvn install:install-file -DgroupId=jcaptcha -DartifactId=jcaptcha -Dversion=1.0 -Dpackaging=jar -Dfile=../code/jars/jcaptcha-1.0.jar

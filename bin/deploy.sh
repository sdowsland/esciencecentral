#!/bin/sh

#Change to new release number
ESC_RELEASE_NUM=2.3;

EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` {SERVER_NAME}"
  exit $E_BADARGS
fi

SERVER_NAME=$1;
JBOSS_HOME=/inkspot/jboss-as-7.1.1.Final;
CONNEXIENCE_HOME=/Users/nsjw7/inkspot;
WORKING_DIR=~/esc-${ESC_RELEASE_NUM};
ZIP_NAME=${WORKING_DIR}.zip;

#remove temp directory
if [ -e ${WORKING_DIR} ] ;	then
  rm -rf ${WORKING_DIR}
fi

mkdir ${WORKING_DIR};
cd ${WORKING_DIR};

#move the code
cp -R ${JBOSS_HOME} . || exit 1;
cp -R ${CONNEXIENCE_HOME}/code/server/ServiceHost . || exit 1;
cp -R ${CONNEXIENCE_HOME}/documentation/buildAndInstall/SQL/migrations . || exit 1;

#zip and exclude .svn directories
zip -r ${ZIP_NAME} -x*/*.svn/* * || exit 1;

scp ${ZIP_NAME} ${SERVER_NAME}: || exit 1;
scp ${CONNEXIENCE_HOME}/bin/install-release.sh ${SERVER_NAME}:

rm -rf ${WORKING_DIR}

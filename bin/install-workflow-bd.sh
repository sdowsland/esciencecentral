#!/bin/bash

DIR=$(readlink -f $(dirname $0))
. $DIR/conf

HOST=$1

if [ "$HOST" == "" ]; then
	echo "usage $0 <target host>"
	exit 1
fi

#ssh $HOST 'rm -rf .inkspot'
ssh $HOST 'rm -rf WorkflowCloud'

scp -r $CONNEXIENCE_HOME/code/webflow/WorkflowCloud $HOST: || exit 1
#ssh $HOST 'mkdir .inkspot' || exit 1
cat $DIR/engine.xml | sed "s|@SERVER_HOSTNAME@|bd01|g" | sed "s|@JMS_PORT@|5445|g" | ssh $HOST 'cat - > .inkspot/engine.xml' || exit 1
ssh $HOST "echo 'completionServer=bd03' > .inkspot/performance.properties"
ssh $HOST 'mkdir -p /workflow/invocations /workflow/library /workflow/static' || exit 1
#ssh $HOST 'cd WorkflowCloud; nohup bin/startserver.sh' || exit 1

#!/bin/bash
# Lists 5 most recently logged user events. Can help to say whether anyone is using the system at the moment.

sudo -u postgres psql -d logeventsdb -c "select timestamp, eventtype, userid, username from graphoperations where timestamp is not null and eventtype like 'USER%' or eventtype like 'LOG%' order by timestamp desc limit 5"
